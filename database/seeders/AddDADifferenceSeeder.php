<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Setting;

class AddDADifferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Wards Seeder
        $wards = [
            [
                'id' => 3,
                'name' => 'da_difference',
                'value' => '1',
            ],
            [
                'id' => 4,
                'name' => 'is_da_execute',
                'value' => '1',
            ],
            [
                'id' => 5,
                'name' => 'pension_da_difference',
                'value' => '1',
            ],
            [
                'id' => 6,
                'name' => 'is_pension_da_execute',
                'value' => '1',
            ],
        ];

        foreach ($wards as $ward) {
            Setting::updateOrCreate([
                'id' => $ward['id']
            ], [
                'id' => $ward['id'],
                'name' => $ward['name'],
                'value' => $ward['value']
            ]);
        }
    }
}
