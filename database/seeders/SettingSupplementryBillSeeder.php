<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSupplementryBillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $supplimentaries = [
            [
                'id' => 1,
                'name' => 'supplimentary_bill_no',
                'value' => '1',
            ]
        ];

        foreach ($supplimentaries as $supplimentary) {
            Setting::updateOrCreate([
                'id' => $supplimentary['id']
            ], [
                'id' => $supplimentary['id'],
                'name' => $supplimentary['name'],
                'value' => $supplimentary['value']
            ]);
        }
    }
}
