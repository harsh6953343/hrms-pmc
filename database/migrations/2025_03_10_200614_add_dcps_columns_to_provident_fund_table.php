<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('employee_provident_funds', function (Blueprint $table) {
            $table->integer('dcps_employee')->after('pf_contribution')->nullable();
            $table->integer('dcps_corp')->after('dcps_employee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('employee_provident_funds', function (Blueprint $table) {
            $table->dropColumn('dcps_employee');
            $table->dropColumn('dcps_corp');
        });
    }
};
