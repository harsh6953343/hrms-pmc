<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->dropColumn('allowance_dcp_corporation');
            $table->dropColumn('deduction_dcp_corporation');
            $table->dropColumn('deduction_dcp_employee');

            $table->string('freeze_attendance_id')->nullable()->after('deduction_type');
            $table->string('freeze_attendance_present_day')->nullable()->after('freeze_attendance_id');
            $table->string('freeze_attendance_basic_salary')->nullable()->after('freeze_attendance_id');
            $table->string('freeze_attendance_net_salary')->nullable()->after('freeze_attendance_basic_salary');
            $table->string('freeze_attendance_da')->nullable()->after('freeze_attendance_net_salary');
            $table->string('freeze_attendance_allowance_id')->nullable()->after('freeze_attendance_da');
            $table->string('freeze_attendance_allowance_amount')->nullable()->after('freeze_attendance_allowance_id');
            $table->string('freeze_attendance_allowance_type')->nullable()->after('freeze_attendance_allowance_amount');
            $table->string('freeze_attendance_deduction_id')->nullable()->after('freeze_attendance_allowance_type');
            $table->string('freeze_attendance_deduction_amount')->nullable()->after('freeze_attendance_deduction_id');
            $table->string('freeze_attendance_deduction_type')->nullable()->after('freeze_attendance_deduction_amount');

            $table->string('employee_salary_id')->nullable()->after('freeze_attendance_deduction_type');
            $table->string('employee_salary_present_day')->nullable()->after('employee_salary_id');
            $table->string('employee_salary_basic_salary')->nullable()->after('employee_salary_id');
            $table->string('employee_salary_net_salary')->nullable()->after('employee_salary_basic_salary');
            $table->string('employee_salary_da')->nullable()->after('employee_salary_net_salary');
            $table->string('employee_salary_allowance_id')->nullable()->after('employee_salary_da');
            $table->string('employee_salary_allowance_amount')->nullable()->after('employee_salary_allowance_id');
            $table->string('employee_salary_allowance_type')->nullable()->after('employee_salary_allowance_amount');
            $table->string('employee_salary_deduction_id')->nullable()->after('employee_salary_allowance_type');
            $table->string('employee_salary_deduction_amount')->nullable()->after('employee_salary_deduction_id');
            $table->string('employee_salary_deduction_type')->nullable()->after('employee_salary_deduction_amount');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->string('allowance_dcp_corporation');
            $table->string('deduction_dcp_corporation');
            $table->string('deduction_dcp_employee');

            $table->dropColumn('freeze_attendance_id');
            $table->dropColumn('freeze_attendance_present_day');
            $table->dropColumn('freeze_attendance_basic_salary');
            $table->dropColumn('freeze_attendance_net_salary');
            $table->dropColumn('freeze_attendance_da');
            $table->dropColumn('freeze_attendance_allowance_id');
            $table->dropColumn('freeze_attendance_allowance_amount');
            $table->dropColumn('freeze_attendance_allowance_type');
            $table->dropColumn('freeze_attendance_deduction_id');
            $table->dropColumn('freeze_attendance_deduction_amount');
            $table->dropColumn('freeze_attendance_deduction_type');

            $table->dropColumn('employee_salary_id');
            $table->dropColumn('employee_salary_present_day');
            $table->dropColumn('employee_salary_basic_salary');
            $table->dropColumn('employee_salary_net_salary');
            $table->dropColumn('employee_salary_da');
            $table->dropColumn('employee_salary_allowance_id');
            $table->dropColumn('employee_salary_allowance_amount');
            $table->dropColumn('employee_salary_allowance_type');
            $table->dropColumn('employee_salary_deduction_id');
            $table->dropColumn('employee_salary_deduction_amount');
            $table->dropColumn('employee_salary_deduction_type');
        });
    }
};
