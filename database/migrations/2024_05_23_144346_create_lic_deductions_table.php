<?php

use App\Models\Employee;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lic_deductions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->string('lic_name');
            $table->string('lic_number');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('total_amt');
            $table->integer('installment_amt');
            $table->integer('total_installment');
            $table->integer('deducted_installment')->default(0);
            $table->integer('pending_installment')->default(0);
            $table->integer('status')->default('1')->nullable()->comment('1) Start 2) Stop');
            $table->integer('status_by')->nullable();
            $table->datetime('status_date')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lic_deductions');
    }
};
