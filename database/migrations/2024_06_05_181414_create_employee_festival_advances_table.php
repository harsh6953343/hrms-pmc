<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_festival_advances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->string('festival_name', 50);
            $table->integer('total_amount');
            $table->integer('total_instalment');
            $table->integer('deducted_instalment')->default(0);
            $table->integer('pending_instalment')->default(0);
            $table->integer('instalment_amount');
            $table->integer('applicable_month');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('apply_status')->default('1')->nullable()->comment('1) Pending 2) Applied');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_festival_advances');
    }
};
