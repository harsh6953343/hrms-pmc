<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pf_opening_balances', function (Blueprint $table) {
            $table->unsignedBigInteger('nps_opening_balance')->after('remark')->nullable();
            $table->string('opening_balance', 30)->nullable()->change();
            $table->string('pf_no', 30)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pf_opening_balances', function (Blueprint $table) {
            $table->dropColumn('nps_opening_balance');
            $table->unsignedBigInteger('opening_balance')->nullable()->change();
            $table->string('pf_no', 30)->change();
        });
    }
};
