<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->string('lic_deduction_id')->after('total_loan_deduction');
            $table->string('lic_deduction_amt')->after('lic_deduction_id');
            $table->integer('total_lic_deduction')->after('lic_deduction_amt');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('lic_deduction_id');
            $table->dropColumn('lic_deduction_amt');
            $table->dropColumn('total_lic_deduction');
        });
    }
};
