<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pension_item_masters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('basis')->comment("1 => Percentage, 2 => Amount, 3 => Other");
            $table->integer('based_on')->nullable()->comment("same table id like foreign key");
            $table->integer('amount')->default(0);
            $table->boolean('type_of_pay')->comment("1 => Earning, 0 => Deduction");
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pension_item_masters');
    }
};
