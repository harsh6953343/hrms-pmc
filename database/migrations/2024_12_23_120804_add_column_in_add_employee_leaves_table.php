<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->integer('amc_status')->default(0)->after('financial_year_id')->comment("0 => Pending, 1 => Approve, 2 => Reject");
            $table->text('amc_reject_remark')->nullable()->after('amc_status');
            $table->integer('dmc_status')->default(0)->after('amc_reject_remark')->comment("0 => Pending, 1 => Approve, 2 => Reject");
            $table->text('dmc_reject_remark')->nullable()->after('dmc_status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->dropColumn('amc_status');
            $table->dropColumn('amc_reject_remark');
            $table->dropColumn('dmc_status');
            $table->dropColumn('dmc_reject_remark');
        });
    }
};
