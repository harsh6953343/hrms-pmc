<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pf_opening_balances', function (Blueprint $table) {
            $table->integer('status')->after('closing_balance')->default(0)->comment('1) Report generated based on this opening balance');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pf_opening_balances', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
