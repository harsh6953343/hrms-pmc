<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\PensionBank;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freeze_pension_news', function (Blueprint $table) {
            $table->id();
            $table->string('pension_id')->nullable();

            $table->date('from_date');
            $table->date('to_date');
            $table->integer('month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->foreignIdFor(PensionBank::class)->nullable()->constrained();

            $table->string('pensioner_name');
            $table->string('account_no');
            $table->string('basic');
            $table->string('dearness_relief');
            $table->string('pension_arrears');
            $table->string('total_earning');
            $table->string('miscellaneous_deduction');
            $table->string('commutation');
            $table->string('recovery');
            $table->string('total_deduction');
            $table->string('net_salary');

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('freeze_pension_news');
    }
};
