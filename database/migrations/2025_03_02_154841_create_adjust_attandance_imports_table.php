<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adjust_attandance_imports', function (Blueprint $table) {
            $table->id();
            $table->string('month')->nullable();
            $table->string('from_date')->nullable();
            $table->string('to_date');
            $table->string('no_of_days')->nullable();
            $table->string('file')->nullable();
            $table->string('financial_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adjust_attandance_imports');
    }
};
