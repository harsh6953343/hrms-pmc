<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->integer('earned_basic')->default(0)->after('basic_salary');
            $table->integer('leave_pay')->default(0)->after('earned_basic');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('earned_basic');
            $table->dropColumn('leave_pay');
        });
    }
};
