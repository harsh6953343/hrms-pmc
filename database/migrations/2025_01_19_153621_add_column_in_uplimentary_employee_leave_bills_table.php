<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->string('allowance_dcp_corporation')->nullable()->after('deduction_type');
            $table->string('deduction_dcp_corporation')->nullable()->after('allowance_dcp_corporation');
            $table->string('deduction_dcp_employee')->nullable()->after('deduction_dcp_corporation');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('uplimentary_employee_leave_bills', function (Blueprint $table) {
            //
        });
    }
};
