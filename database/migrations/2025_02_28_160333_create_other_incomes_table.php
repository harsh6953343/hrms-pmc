<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\IncomeTax;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('other_incomes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('income_tax_id')->nullable();
            $table->foreign('income_tax_id')->references('id')->on('income_tax');
            $table->string('un_furnished')->nullable();
            $table->string('personal_attendant')->nullable();
            $table->string('perk_furnished_value')->nullable();
            $table->string('gas_electricity_water')->nullable();
            $table->string('interest_free_or_concessional_loans')->nullable();
            $table->string('cost_of_furniture')->nullable();
            $table->string('furniture_rentals')->nullable();
            $table->string('holiday_expenses')->nullable();
            $table->string('perquisite_value_of_furniture')->nullable();
            $table->string('free_meals')->nullable();
            $table->string('free_education')->nullable();
            $table->string('perk_furnished_total')->nullable();
            $table->string('gifts_vouchers_etc')->nullable();
            $table->string('rent_paid_by_employee')->nullable();
            $table->string('credit_card_expenses')->nullable();
            $table->string('value_of_perquisites')->nullable();
            $table->string('club_expenses')->nullable();
            $table->string('conveyance')->nullable();
            $table->string('use_of_movable_assets_by_employees')->nullable();
            $table->string('remuneration_paid_on_behalf_of_employee')->nullable();
            $table->string('transfer_of_assets_to_employees')->nullable();
            $table->string('taxable_ltc')->nullable();
            $table->string('stock_option_non_qualified_options')->nullable();
            $table->string('other_benefits')->nullable();
            $table->string('stock_options_referred_80_iac')->nullable();
            $table->string('pf_in_excess_of_12')->nullable();
            $table->string('contribution_by_employer')->nullable();
            $table->string('excess_iterest_credited')->nullable();
            $table->string('annual_accretion_taxable')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('other_incomes');
    }
};
