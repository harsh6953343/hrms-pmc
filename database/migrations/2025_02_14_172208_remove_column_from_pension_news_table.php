<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pension_news', function (Blueprint $table) {
            $table->dropColumn('basic');
            $table->dropColumn('basic_date');
            $table->dropColumn('after_basic');
            $table->dropColumn('after_basic_date');
            $table->dropColumn('dearness_relief');
            $table->dropColumn('dearness_relief_date');
            $table->dropColumn('relief_fund');
            $table->dropColumn('relief_fund_date');
            $table->dropColumn('pensioner_arrears');
            $table->dropColumn('pensioner_arrears_date');
            $table->dropColumn('special_allowance');
            $table->dropColumn('special_allowance_date');
            $table->dropColumn('miscellaneous_deduction');
            $table->dropColumn('miscellaneous_deduction_date');
            $table->dropColumn('miscellaneous_arrears');
            $table->dropColumn('miscellaneous_arrears_date');
            $table->dropColumn('recovery');
            $table->dropColumn('recovery_date');
            $table->dropColumn('commutation');
            $table->dropColumn('commutation_date');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pension_news', function (Blueprint $table) {
            $table->string('basic')->default(0)->after('pension_end_date');
            $table->date('basic_date')->nullable()->after('basic');
            $table->string('after_basic')->default(0)->after('basic_date');
            $table->date('after_basic_date')->nullable()->after('after_basic');
            $table->string('dearness_relief')->default(0)->after('after_basic_date');
            $table->date('dearness_relief_date')->nullable()->after('dearness_relief');
            $table->string('relief_fund')->default(0)->after('dearness_relief_date');
            $table->date('relief_fund_date')->nullable()->after('relief_fund');
            $table->string('pensioner_arrears')->default(0)->after('relief_fund_date');
            $table->date('pensioner_arrears_date')->nullable()->after('pensioner_arrears');
            $table->string('special_allowance')->default(0)->after('pensioner_arrears_date');
            $table->date('special_allowance_date')->nullable()->after('special_allowance');
            $table->string('miscellaneous_deduction')->default(0)->after('special_allowance_date');
            $table->date('miscellaneous_deduction_date')->nullable()->after('miscellaneous_deduction');
            $table->string('miscellaneous_arrears')->default(0)->after('miscellaneous_deduction_date');
            $table->date('miscellaneous_arrears_date')->nullable()->after('miscellaneous_arrears');
            $table->string('recovery')->default(0)->after('miscellaneous_arrears_date');
            $table->date('recovery_date')->nullable()->after('recovery');
            $table->string('commutation')->default(0)->after('recovery_date');
            $table->date('commutation_date')->nullable()->after('commutation');
        });
    }
};
