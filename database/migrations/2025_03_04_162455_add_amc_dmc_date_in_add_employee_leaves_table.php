<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->datetime('amc_datetime')->nullable()->after('amc_reject_remark');
            $table->datetime('dmc_datetime')->nullable()->after('dmc_reject_remark');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            //
        });
    }
};
