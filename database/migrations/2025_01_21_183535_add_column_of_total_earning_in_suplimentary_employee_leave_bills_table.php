<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->string('employee_total_earning')->nullable();
            $table->string('freeze_total_earning')->nullable();
            $table->string('suplimentary_total_earning')->nullable();
            $table->string('employee_total_deduction')->nullable();
            $table->string('freeze_total_deduction')->nullable();
            $table->string('suplimentary_total_deduction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            //
        });
    }
};
