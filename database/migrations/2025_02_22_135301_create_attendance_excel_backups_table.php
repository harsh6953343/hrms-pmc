<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Employee;
use App\Models\Attendance;
use App\Models\FinancialYear;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attendance_excel_backups', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Attendance::class)->nullable()->constrained();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->string('emp_code');
            $table->string('emp_name');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('main_present_days')->default(0);
            $table->string('total_present_days')->default(0);
            $table->string('total_absent_days')->default(0);
            $table->string('total_leave')->default(0);
            $table->string('total_half_days')->default(0);
            $table->string('month')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attendance_excel_backups');
    }
};
