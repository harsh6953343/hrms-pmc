<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Ward;
use App\Models\FinancialYear;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('supplimentary_bills');
        Schema::create('supplimentary_bills', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->string('bill_no')->nullable();
            $table->text('bill_description')->nullable();
            $table->date('cheque_date')->nullable();
            $table->integer('payment_status')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('supplimentary_bills');
    }
};
