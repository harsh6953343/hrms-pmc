<?php

use App\Models\Bank;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\PayScale;
use App\Models\PensionBank;
use App\Models\Ward;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pension_news', function (Blueprint $table) {
            $table->id();
            $table->string('pension_id')->nullable();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();

            $table->string('emp_title');
            $table->string('emp_fname');
            $table->string('emp_fname_marathi');
            $table->string('emp_mname');
            $table->string('emp_mname_marathi');
            $table->string('emp_lname');
            $table->string('emp_lname_marathi');

            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(Designation::class)->nullable()->constrained();
            $table->foreignIdFor(PayScale::class)->nullable()->constrained();

            $table->string('last_basic');
            $table->string('last_grade_pay');
            $table->date('dob');
            $table->date('doj');
            $table->date('dor');
            $table->date('dod')->nullable();

            $table->string('percent_share')->nullable();
            $table->string('pensioner_title')->nullable();
            $table->string('pensioner_type')->nullable();

            $table->string('pensioner_fname');
            $table->string('pensioner_fname_marathi');
            $table->string('pensioner_mname');
            $table->string('pensioner_mname_marathi');
            $table->string('pensioner_lname');
            $table->string('pensioner_lname_marathi');

            $table->string('mobile')->nullable();
            $table->string('aadhar')->nullable();
            $table->string('address')->nullable();
            $table->string('address_marathi')->nullable();

            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->foreignIdFor(PensionBank::class)->nullable()->constrained();
            $table->string('branch')->nullable();

            $table->string('account_no');
            $table->integer('pensionser_status')->default(1)->comment('1) Active 2) Inactive');
            $table->date('status_change_date')->nullable();
            $table->string('remarks')->nullable();

            $table->date('pension_start_date');
            $table->integer('da_applicabe')->default('1')->comment('1) yes 2) no');
            $table->string('pay_commission')->nullable();
            $table->date('pension_end_date')->nullable();

            $table->string('basic')->default(0)->nullable();
            $table->date('basic_date')->nullable();

            $table->string('after_basic')->default(0)->nullable();
            $table->date('after_basic_date')->nullable();

            $table->string('dearness_relief')->default(0)->nullable();
            $table->date('dearness_relief_date')->nullable();

            $table->string('relief_fund')->default(0)->nullable();
            $table->date('relief_fund_date')->nullable();

            $table->string('pensioner_arrears')->default(0)->nullable();
            $table->date('pensioner_arrears_date')->nullable();

            $table->string('special_allowance')->default(0)->nullable();
            $table->date('special_allowance_date')->nullable();

            $table->string('miscellaneous_deduction')->default(0)->nullable();
            $table->date('miscellaneous_deduction_date')->nullable();

            $table->string('miscellaneous_arrears')->default(0)->nullable();
            $table->date('miscellaneous_arrears_date')->nullable();

            $table->string('recovery')->default(0)->nullable();
            $table->date('recovery_date')->nullable();

            $table->string('commutation')->default(0)->nullable();
            $table->date('commutation_date')->nullable();


            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pension_news');
    }
};
