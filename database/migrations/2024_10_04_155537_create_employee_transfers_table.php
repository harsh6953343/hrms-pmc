<?php

use App\Models\Clas;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\Ward;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_transfers', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('Emp_Code');

            $table->string('current_ward');
            $table->string('current_department');


            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(Department::class)->nullable()->constrained();
            $table->foreignIdFor(Clas::class)->nullable()->constrained();
            $table->foreignIdFor(Designation::class)->nullable()->constrained();
            $table->string('transfer_order');
            $table->string('transfer_date');
            $table->string('remark');

            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_transfers');
    }
};
