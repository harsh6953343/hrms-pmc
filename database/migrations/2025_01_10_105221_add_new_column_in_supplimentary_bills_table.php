<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            $table->double('actual_basic_salary', 8, 2)->default(0)->after('status');
            $table->double('basic_salary', 8, 2)->default(0)->after('actual_basic_salary');
            $table->string('allowance_id')->nullable()->after('basic_salary');
            $table->string('allowance_amount')->nullable()->after('allowance_id');
            $table->string('allowance_type')->nullable()->after('allowance_amount');
            $table->string('deduction_id')->nullable()->after('allowance_type');
            $table->string('deduction_amount')->nullable()->after('deduction_id');
            $table->string('deduction_type')->nullable()->after('deduction_amount');
            $table->double('net_salary, 8, 2')->nullable()->after('deduction_type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            //
        });
    }
};
