<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->string('actual_present_day')->default(0)->after('present_day');
            $table->string('total_leave')->default(0)->after('actual_present_day');


        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('actual_present_day');
            $table->dropColumn('total_leave');


        });
    }
};
