<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Employee;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->id();
            $table->foreignId('suplimentry_leave_id')->nullable()->constrained('suplimentary_leave_bills');
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('emp_code')->nullable();
            $table->string('month')->nullable();
            $table->string('attandance_pr')->nullable();
            $table->string('attandance_ul')->nullable();
            $table->string('attandance_ml')->nullable();
            $table->string('attandance_el')->nullable();
            $table->string('supplimentry_pr')->nullable();
            $table->string('supplimentry_ul')->nullable();
            $table->string('supplimentry_ml')->nullable();
            $table->string('supplimentry_el')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('suplimentary_employee_leave_bills');
    }
};
