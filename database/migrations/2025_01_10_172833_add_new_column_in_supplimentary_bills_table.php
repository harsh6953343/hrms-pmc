<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            $table->double('da', 8, 2)->default(0)->after('basic_salary');
            $table->double('allowance_dcp_corporation', 8, 2)->default(0)->after('da');
            $table->double('deduction_dcp_corporation', 8, 2)->default(0)->after('allowance_dcp_corporation');
            $table->double('deduction_dcp_employee', 8, 2)->default(0)->after('deduction_dcp_corporation');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            //
        });
    }
};
