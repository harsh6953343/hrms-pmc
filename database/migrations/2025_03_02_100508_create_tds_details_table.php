<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tds_details', function (Blueprint $table) {
            $table->id();
            $table->string('Emp_Code');
            $table->integer('prev_emp_tax')->nullable();
            $table->integer('prev_emp_surcharge')->nullable();
            $table->integer('wodt_surcharge')->nullable();
            $table->integer('wdt_surcharge')->nullable();
            $table->integer('prev_emp_cess')->nullable();
            $table->integer('wodt_cess')->nullable();
            $table->integer('wdt_cess')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tds_details');
    }
};
