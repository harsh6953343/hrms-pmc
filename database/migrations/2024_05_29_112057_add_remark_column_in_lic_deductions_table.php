<?php

use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('lic_deductions', function (Blueprint $table) {
            $table->string('remark', 50)->after('status_date')->nullable();
            $table->foreignIdFor(FinancialYear::class)->after('remark')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('lic_deductions', function (Blueprint $table) {
            $table->dropColumn('remark');
            $table->dropForeign(['financial_year_id']);
            $table->dropColumn('financial_year_id');
        });
    }
};
