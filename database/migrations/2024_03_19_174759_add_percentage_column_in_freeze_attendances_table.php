<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->integer('salary_percentage')->comment('if status and salary applicable')->after('corporation_share_da')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('salary_percentage');
        });
    }
};
