<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('m_fname', 50)->after('lname')->nullable();
            $table->string('m_mname', 50)->after('m_fname')->nullable();
            $table->string('m_lname', 50)->after('m_mname')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('m_fname');
            $table->dropColumn('m_mname');
            $table->dropColumn('m_lname');
        });
    }
};
