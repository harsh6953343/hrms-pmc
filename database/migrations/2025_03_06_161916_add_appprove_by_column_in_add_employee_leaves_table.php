<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->integer('amc_approved_by')->nullable()->after('amc_datetime');
            $table->integer('dmc_approved_by')->nullable()->after('dmc_datetime');
            $table->integer('cancel_status')->nullable()->after('dmc_approved_by');
            $table->datetime('cancel_date')->nullable()->after('cancel_status');
            $table->text('cancel_remark')->nullable()->after('cancel_date');
            $table->integer('cancel_by')->nullable()->after('cancel_remark');
            $table->integer('amc_cancel_status')->nullable()->after('cancel_remark');
            $table->datetime('amc_cancel_datetime')->nullable()->after('amc_cancel_status');
            $table->text('amc_cancel_remark')->nullable()->after('amc_cancel_datetime');
            $table->integer('amc_cancel_by')->nullable()->after('amc_cancel_remark');
            $table->integer('dmc_cancel_status')->nullable()->after('amc_cancel_by');
            $table->datetime('dmc_cancel_datetime')->nullable()->after('dmc_cancel_status');
            $table->text('dmc_cancel_remark')->nullable()->after('dmc_cancel_datetime');
            $table->integer('dmc_cancel_by')->nullable()->after('dmc_cancel_remark');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->dropColumn('amc_approved_by');
            $table->dropColumn('dmc_approved_by');
            $table->dropColumn('cancel_status');
            $table->dropColumn('cancel_date');
            $table->dropColumn('cancel_remark');
            $table->dropColumn('cancel_by');
            $table->dropColumn('amc_cancel_status');
            $table->dropColumn('amc_cancel_datetime');
            $table->dropColumn('amc_cancel_remark');
            $table->dropColumn('amc_cancel_by');
            $table->dropColumn('dmc_cancel_status');
            $table->dropColumn('dmc_cancel_datetime');
            $table->dropColumn('dmc_cancel_remark');
            $table->dropColumn('dmc_cancel_by');
        });
    }
};
