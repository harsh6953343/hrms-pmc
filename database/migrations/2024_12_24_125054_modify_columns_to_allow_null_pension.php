<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pension_news', function (Blueprint $table) {
            $table->string('pensioner_fname')->nullable()->change();
            $table->string('pensioner_fname_marathi')->nullable()->change();
            $table->string('pensioner_mname')->nullable()->change();
            $table->string('pensioner_mname_marathi')->nullable()->change();
            $table->string('pensioner_lname')->nullable()->change();
            $table->string('pensioner_lname_marathi')->nullable()->change();

            $table->string('account_no')->nullable()->change();
            $table->integer('pensionser_status')->default(1)->comment('1) Active 2) Inactive')->nullable()->change();
            $table->text('pension_start_date')->nullable()->change();
            $table->integer('da_applicabe')->default('1')->comment('1) yes 2) no')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pension_news', function (Blueprint $table) {
            $table->string('pensioner_fname')->nullable(false)->change();
            $table->string('pensioner_fname_marathi')->nullable(false)->change();
            $table->string('pensioner_mname')->nullable(false)->change();
            $table->string('pensioner_mname_marathi')->nullable(false)->change();
            $table->string('pensioner_lname')->nullable(false)->change();
            $table->string('pensioner_lname_marathi')->nullable(false)->change();

            $table->string('account_no')->nullable(false)->change();
            $table->integer('pensionser_status')->default(1)->comment('1) Active 2) Inactive')->nullable(false)->change();
            $table->text('pension_start_date')->nullable(false)->change();
            $table->integer('da_applicabe')->default('1')->comment('1) yes 2) no')->nullable(false)->change();
        });
    }
};
