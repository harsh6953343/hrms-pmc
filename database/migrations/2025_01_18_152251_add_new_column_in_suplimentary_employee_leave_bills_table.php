<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->integer('financial_year_id')->nullable()->after('id');
            $table->string('basic_salary')->nullable()->after('supplimentry_el');
            $table->string('net_salary')->nullable()->after('basic_salary');
            $table->string('da')->nullable()->after('net_salary');
            $table->string('allowance_id')->nullable()->after('da');
            $table->string('allowance_amount')->nullable()->after('allowance_id');
            $table->string('allowance_type')->nullable()->after('allowance_amount');
            $table->string('deduction_id')->nullable()->after('allowance_type');
            $table->string('deduction_amount')->nullable()->after('deduction_id');
            $table->string('deduction_type')->nullable()->after('deduction_amount');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('suplimentary_employee_leave_bills', function (Blueprint $table) {
            $table->dropColumn('financial_year_id');
            $table->dropColumn('basic_salary');
            $table->dropColumn('net_salary');
            $table->dropColumn('da');
            $table->dropColumn('allowance_id');
            $table->dropColumn('allowance_amount');
            $table->dropColumn('allowance_type');
            $table->dropColumn('deduction_id');
            $table->dropColumn('deduction_amount');
            $table->dropColumn('deduction_type');
        });
    }
};
