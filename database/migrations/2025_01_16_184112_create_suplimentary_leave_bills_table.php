<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\SupplimentaryBill;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('suplimentary_leave_bills', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(SupplimentaryBill::class)->nullable()->constrained();
            $table->string('bill_no')->nullable();
            $table->date('bill_date')->nullable();
            $table->text('bill_description')->nullable();
            $table->integer('payment_status')->nullable();
            $table->string('cheque_no')->nullable();
            $table->date('cheque_date')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('suplimentary_leave_bills');
    }
};
