<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ten_day_attendnces', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->string('Emp_Code');
            $table->string('emp_name');
            $table->string('P');
            $table->string('A');
            $table->string('H');
            $table->string('HP');
            $table->string('WO');
            $table->string('WOP');

            $table->string('CL');
            $table->string('PL');
            $table->string('SL');
            $table->string('other_leave');
            $table->string('total_present');
            $table->string('total_pay_days');
            $table->string('total_ot_in_hrs');
            $table->string('total_late_by');
            $table->string('total_early_by');

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ten_day_attendnces');
    }
};
