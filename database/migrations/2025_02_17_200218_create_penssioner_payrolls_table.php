<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\PensionNew;
use App\Models\PensionItemMaster;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penssioner_payrolls', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(PensionNew::class)->nullable()->constrained();
            $table->foreignIdFor(PensionItemMaster::class)->nullable()->constrained();
            $table->string('basis')->nullable();
            $table->string('based_on')->nullable();
            $table->string('type_of_pay')->nullable();
            $table->string('amount')->nullable();
            $table->date('effective_date')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penssioner_payrolls');
    }
};
