<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\DaBill;
use App\Models\Ward;
use App\Models\Department;
use App\Models\Designation;
use App\Models\FreezeAttendance;
use App\Models\FinancialYear;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('da_bill_calculates', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignIdFor(DaBill::class)->nullable()->constrained();
            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(Department::class)->nullable()->constrained();
            $table->foreignIdFor(Designation::class)->nullable()->constrained();
            $table->foreignIdFor(FreezeAttendance::class)->nullable()->constrained();
            $table->string('emp_code');
            $table->string('month');
            $table->date('from_date');
            $table->date('to_date');
            $table->string('basic')->nullable();
            $table->string('grade_pay')->nullable();
            $table->string('da_payable')->nullable();
            $table->string('da_given')->nullable();
            $table->string('da_difference')->nullable();
            $table->string('hra_payable')->nullable();
            $table->string('hra_given')->nullable();
            $table->string('hra_difference')->nullable();
            $table->string('dcps_corporation_allowance')->nullable();
            $table->string('dcps_employee_deduction')->nullable();
            $table->string('dcps_corporation_deduction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('da_bill_calculates');
    }
};
