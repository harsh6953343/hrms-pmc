<?php

namespace App\Repositories;

use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\EmployeeAllowance;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeSalary;
use App\Models\OldEmployeeAllowance;
use App\Models\OldEmployeeDeduction;
use App\Models\OldEmployeeSalary;
use App\Models\PayScale;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;




class EmployeeSalaryStructure
{

    public function store($input)
    {
        DB::beginTransaction();

        $employeeSalary = EmployeeSalary::create(Arr::only($input, EmployeeSalary::getFillables()));

        $input['applicable_date'] = date('Y-m-d');

        $oldEmployeeSalary = OldEmployeeSalary::create(Arr::only($input, OldEmployeeSalary::getFillables()));

        // Handle Allowances
        $allowedAllowanceIds = $input['allowance_is_active']; // Specify the allowed allowance IDs

        foreach ($input['allowance_id'] as $key => $allowance_id) {
            if (in_array($allowance_id, $allowedAllowanceIds)) {
                $allowanceData = [
                    'employee_id' => $employeeSalary->employee_id,
                    'Emp_Code' => $employeeSalary->Emp_Code,
                    'employee_salary_id' => $employeeSalary->id,
                    'allowance_id' => $allowance_id,
                    'allowance_amt' => isset($input['allowance_amt'][$key]) ? $input['allowance_amt'][$key] : null,
                    'allowance_type' => isset($input['allowance_type'][$key]) ? $input['allowance_type'][$key] : null,
                    'is_active' => '1',
                ];

                EmployeeAllowance::create($allowanceData);

                $allowanceData['applicable_date'] = date('Y-m-d');
                $allowanceData['old_employee_salary_id'] = $oldEmployeeSalary->id;

                OldEmployeeAllowance::create($allowanceData);
            }
        }

        // Handle Deductions
        $allowedDedcutionIds = $input['deduction_is_active']; // Specify the allowed allowance IDs

        foreach ($input['deduction_id'] as $key => $deduction_id) {
            if (in_array($deduction_id, $allowedDedcutionIds)) {
                $deductionData = [
                    'employee_id' => $employeeSalary->employee_id,
                    'Emp_Code' => $employeeSalary->Emp_Code,
                    'employee_salary_id' => $employeeSalary->id,
                    'deduction_id' => $deduction_id,
                    'deduction_amt' => isset($input['deduction_amt'][$key]) ? $input['deduction_amt'][$key] : null,
                    'deduction_type' => isset($input['deduction_type'][$key]) ? $input['deduction_type'][$key] : null,
                    'is_active' => '1',
                ];

                EmployeeDeduction::create($deductionData);

                $deductionData['applicable_date'] = date('Y-m-d');
                $deductionData['old_employee_salary_id'] = $oldEmployeeSalary->id;

                OldEmployeeDeduction::create($deductionData);
            }
        }
        // Commit the transaction
        DB::commit();
    }

    public function editEmployeeSalary($employeeSalary)
    {
        if ($employeeSalary) {

            $pay_scales = PayScale::latest()->get();

            $allowances = Allowance::latest()->get();

            $deductions = Deduction::latest()->get();

            return [
                'employeeSalary'        => $employeeSalary,
                'pay_scales'            => $pay_scales,
                'allowances'            => $allowances,
                'deductions'            => $deductions,
            ];
        }
    }

    public function updateEmployeeSalary($input, $emp)
    {
        DB::beginTransaction();

        $authUser = Auth::user();

        $employeeSalary = $emp->update(Arr::only($input, EmployeeSalary::getFillables()));
        $get_old_employee_salary = OldEmployeeSalary::with('employee')->where('employee_id', $input['employee_id'])->latest()->first();

        if (($get_old_employee_salary->pay_scale_id != $emp->pay_scale_id) ||
            ($get_old_employee_salary->basic_salary != $emp->basic_salary) ||
            ($get_old_employee_salary->grade_pay != $emp->grade_pay)
        ) {
            $get_old_employee_salary->update([
                'end_date' => now(),
                'updated_by' => $authUser->id,
            ]);

            $input['applicable_date'] = date('Y-m-d');

            $oldEmployeeSalary = OldEmployeeSalary::create(Arr::only($input, OldEmployeeSalary::getFillables()));
        }

        $all_allowance_id       = $input['allowance_id'];
        $all_allowance_amt      = $input['allowance_amt'];
        $all_allowance_type     = $input['allowance_type'];
        $active_allowance_ids  = $input['allowance_is_active'];

        $employeeAllowances = $emp->employee_allowances;

        foreach ($all_allowance_id as $key => $allowance_id) {

            $matchingAllowance = $employeeAllowances->where('allowance_id', $allowance_id)->first();

            if (in_array($allowance_id, $active_allowance_ids)) {

                if ($matchingAllowance) {
                    $matchingAllowance->update([
                        'is_active' => 1,
                        'allowance_amt' => $all_allowance_amt[$key],
                        'updated_by' => $authUser->id,
                        'updated_at' => now(),
                    ]);
                } else {

                    $allowanceData = [
                        'employee_salary_id' => $emp->id,
                        'allowance_id' => $allowance_id,
                        'is_active' => 1,
                        'allowance_amt' => $all_allowance_amt[$key],
                        'allowance_type' => $all_allowance_type[$key],
                        'employee_id' => $emp->employee_id,
                        'Emp_Code' => $emp->Emp_Code,
                    ];

                    EmployeeAllowance::create($allowanceData);
                }
            } else {
                if ($matchingAllowance) {
                    $matchingAllowance->update([
                        'is_active' => 0,
                        'updated_by' => $authUser->id,
                        'updated_at' => now(),
                    ]);
                }
            }
        }

        // Old Employee Allowance start
        foreach ($all_allowance_id as $key => $allowance_id) {

            if (!empty($oldEmployeeSalary)) {
                $get_old_allowance_data = OldEmployeeAllowance::where('employee_id', $oldEmployeeSalary->employee_id)
                    ->where('allowance_id', $allowance_id)->latest()->first();
            } else {
                $oldEmployeeSalary = $get_old_employee_salary;
                $get_old_allowance_data = OldEmployeeAllowance::where('employee_id', $emp->employee_id)
                    ->where('allowance_id', $allowance_id)->latest()->first();
            }

            if (in_array($allowance_id, $active_allowance_ids)) {
                if ($get_old_allowance_data) {

                    if ($get_old_allowance_data->allowance_amt != $all_allowance_amt[$key] || $get_old_allowance_data->is_active != 1) {
                        $get_old_allowance_data->update([
                            'is_active' => 0,
                            'end_date' => date('Y-m-d'),
                            'updated_by' => $authUser->id,
                        ]);

                        $allowanceData = [
                            'old_employee_salary_id' => $oldEmployeeSalary->id,
                            'allowance_id' => $allowance_id,
                            'is_active' => 1,
                            'allowance_amt' => $all_allowance_amt[$key],
                            'allowance_type' => $all_allowance_type[$key],
                            'employee_id' => $emp->employee_id,
                            'Emp_Code' => $emp->Emp_Code,
                            'applicable_date' => date('Y-m-d'),
                        ];

                        OldEmployeeAllowance::create($allowanceData);
                    }
                } else {

                    $allowanceData = [
                        'old_employee_salary_id' => $oldEmployeeSalary->id,
                        'allowance_id' => $allowance_id,
                        'is_active' => 1,
                        'allowance_amt' => $all_allowance_amt[$key],
                        'allowance_type' => $all_allowance_type[$key],
                        'employee_id' => $emp->employee_id,
                        'Emp_Code' => $emp->Emp_Code,
                        'applicable_date' => date('Y-m-d'),
                    ];

                    OldEmployeeAllowance::create($allowanceData);
                }
            } else {
                if ($get_old_allowance_data) {
                    $get_old_allowance_data->update([
                        'is_active' => 0,
                        'end_date' => date('Y-m-d'),
                        'updated_by' => $authUser->id,
                    ]);
                }
            }
        }
        // Old Employee Allowance End

        // Deduction Start

        $all_deduction_id       = $input['deduction_id'];
        $all_deduction_amt      = $input['deduction_amt'];
        $all_deduction_type     = $input['deduction_type'];
        $active_deduction_ids  = $input['deduction_is_active'];

        $employeeDeductions = $emp->employee_deductions;

        foreach ($all_deduction_id as $key => $deduction_id) {

            $matchingDeduction = $employeeDeductions->where('deduction_id', $deduction_id)->first();

            if (in_array($deduction_id, $active_deduction_ids)) {

                if ($matchingDeduction) {
                    $matchingDeduction->update([
                        'is_active' => 1,
                        'deduction_amt' => $all_deduction_amt[$key],
                        'updated_by' => $authUser->id,
                        'updated_at' => now(),
                    ]);
                } else {

                    EmployeeDeduction::create([
                        'employee_salary_id' => $emp->id,
                        'deduction_id' => $deduction_id,
                        'is_active' => 1,
                        'deduction_amt' => $all_deduction_amt[$key],
                        'deduction_type' => $all_deduction_type[$key],
                        'employee_id' => $emp->employee_id,
                        'Emp_Code' => $emp->Emp_Code,
                    ]);
                }
            } else {
                if ($matchingDeduction) {
                    $matchingDeduction->update([
                        'is_active' => 0,
                        'updated_by' => $authUser->id,
                        'updated_at' => now(),
                    ]);
                }
            }
        }

        // Deduction End

        // Old Deduction Start

        foreach ($all_deduction_id as $key => $deduction_id) {

            if (!empty($oldEmployeeSalary)) {
                $get_old_deduction_data = OldEmployeeDeduction::where('employee_id', $oldEmployeeSalary->employee_id)
                    ->where('deduction_id', $deduction_id)->latest()->first();
            } else {
                $oldEmployeeSalary = $get_old_employee_salary;
                $get_old_deduction_data = OldEmployeeDeduction::where('employee_id', $emp->employee_id)
                    ->where('deduction_id', $deduction_id)->latest()->first();
            }

            if (in_array($deduction_id, $active_deduction_ids)) {
                if ($get_old_deduction_data) {

                    if (($get_old_deduction_data->deduction_amt != $all_deduction_amt[$key]) || $get_old_deduction_data->is_active != 1) {
                        $get_old_deduction_data->update([
                            'is_active' => 0,
                            'end_date' => date('Y-m-d'),
                            'updated_by' => $authUser->id,
                        ]);

                        $deductionData = [
                            'old_employee_salary_id' => $oldEmployeeSalary->id,
                            'deduction_id' => $deduction_id,
                            'is_active' => 1,
                            'deduction_amt' => $all_deduction_amt[$key],
                            'deduction_type' => $all_deduction_type[$key],
                            'employee_id' => $emp->employee_id,
                            'Emp_Code' => $emp->Emp_Code,
                            'applicable_date' => date('Y-m-d'),
                        ];

                        OldEmployeeDeduction::create($deductionData);
                    }
                } else {

                    $deductionData = [
                        'old_employee_salary_id' => $oldEmployeeSalary->id,
                        'deduction_id' => $deduction_id,
                        'is_active' => 1,
                        'deduction_amt' => $all_deduction_amt[$key],
                        'deduction_type' => $all_deduction_type[$key],
                        'employee_id' => $emp->employee_id,
                        'Emp_Code' => $emp->Emp_Code,
                        'applicable_date' => date('Y-m-d'),
                    ];

                    OldEmployeeDeduction::create($deductionData);
                }
            } else {
                if ($get_old_deduction_data) {
                    $get_old_deduction_data->update([
                        'is_active' => 0,
                        'end_date' => date('Y-m-d'),
                        'updated_by' => $authUser->id,
                    ]);
                }
            }
        }

        // Old Deduction End


        // Commit the transaction
        DB::commit();
    }
}
