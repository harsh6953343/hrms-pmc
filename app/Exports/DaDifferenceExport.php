<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DaDifferenceExport implements FromView
{
    protected $daBillCalculates;
    protected $daBillDate;

    public function __construct($daBillCalculates, $daBillDate)
    {
        $this->daBillCalculates = $daBillCalculates;
        $this->daBillDate = $daBillDate;
    }

    public function view(): View
    {
        return view('admin.employee.da-differance-excel', [
            'daBillCalculates' => $this->daBillCalculates,
            'daBillDate' => $this->daBillDate
        ]);
    }
}
