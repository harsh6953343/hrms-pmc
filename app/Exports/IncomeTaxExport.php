<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class IncomeTaxExport implements FromArray, WithHeadings
{
    protected $headings;
    protected $rows;

    /**
     * Constructor to pass data
     *
     * @param array $headings
     * @param array $rows
     */

    public function __construct(array $headings, array $rows)
    {
        $this->headings = $headings;
        $this->rows = $rows;
    }

    /**
     * Return the data to export (rows data)
     *
     * @return array
     */
    public function array(): array
    {
        return $this->rows;
    }

    /**
     * Return the headings for the export
     *
     * @return array
     */
    public function headings(): array
    {
        return $this->headings;
    }
}
