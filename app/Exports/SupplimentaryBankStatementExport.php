<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SupplimentaryBankStatementExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $bankStatements;

    public function __construct($bankStatements)
    {
        $this->bankStatements = $bankStatements;
    }
    public function view(): View
    {
        return view('admin.reports.suplimentry.bank-statement-excel', [
            'bankStatements' => $this->bankStatements
        ]);
    }
}
