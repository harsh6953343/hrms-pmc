<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class IncomeTaxExports implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $incomeTax;

    public function __construct($incomeTax)
    {
        $this->incomeTax = $incomeTax;
    }

    public function view(): View
    {
        return view('admin.incomeTax.report.excel', [
            'incomeTaxs' => $this->incomeTax,
        ]);
    }
}
