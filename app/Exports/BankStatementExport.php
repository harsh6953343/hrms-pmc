<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BankStatementExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $bankStatements;
    protected $monthName;

    public function __construct($bankStatements, $monthName)
    {
        $this->bankStatements = $bankStatements;
        $this->monthName = $monthName;
    }
    public function view(): View
    {
        return view('admin.reports.bank-statement-excel', [
            'bankStatements' => $this->bankStatements,
            'monthName' => $this->monthName,
        ]);
    }
}
