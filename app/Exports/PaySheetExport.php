<?php

namespace App\Exports;

use App\Models\FreezeAttendance;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\EmployeeMonthlyLoan;
use App\Models\RemainingFreezeSalary;
use App\Models\SupplimentaryBill;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PaySheetExport implements FromCollection, WithHeadings
{
    protected $freezeAttendanceData;

    public function __construct(Collection $freezeAttendanceData)
    {
        $this->freezeAttendanceData = $freezeAttendanceData;
    }

    public function collection()
    {
        // Add allowance headings dynamically
        $allowances = Allowance::get();
        $deductions = Deduction::get();

        $data = [];
        $grand_total_basic_salary = 0;
        $grand_total_leave_pay = 0;
        $grand_total_earn = 0;
        $allowanceTotals = array_fill_keys($allowances->pluck('id')->toArray(), 0);
        $deductionTotals = array_fill_keys($deductions->pluck('id')->toArray(), 0);

        $grand_total_pf = 0;
        $grand_total_bank_loan = 0;
        $grand_total_stamp_duty = 0;
        $grand_total_deductions = 0;
        $grand_total_net_salary = 0;
        $grand_total_corporation_share = 0;
        $grand_total_lic = 0;
        $grand_total_festival_allowance = 0;
        $grand_total_festival_deduction = 0;
        $grand_total_employee_share = 0;



        foreach ($this->freezeAttendanceData as $key => $freeze_attendance) {
            if ($freeze_attendance?->employee?->employee_status?->is_salary_applicable == "0") {
                continue;
            }
            $explode_allowance_ids = explode(',', $freeze_attendance->allowance_Id);
            $explode_allowance_amt = explode(',', $freeze_attendance->allowance_Amt);
            $explode_deduction_ids = explode(',', $freeze_attendance->deduction_Id);
            $explode_deduction_amt = explode(',', $freeze_attendance->deduction_Amt);
            $explode_loan_ids = explode(',', $freeze_attendance->loan_deduction_id);
            $explode_bank_ids = explode(',', $freeze_attendance->loan_deduction_bank_id);

            // Grand Total Calculations
            $grand_total_basic_salary += $freeze_attendance->earned_basic;
            $grand_total_leave_pay += $freeze_attendance->leave_pay;

            $grand_total_earn += ($freeze_attendance->earned_basic + $freeze_attendance->leave_pay + $freeze_attendance->total_allowance);
            $grand_total_festival_allowance += $freeze_attendance->festival_allowance;

            $fromDate = Carbon::parse($freeze_attendance->from_date);
            $toDate = Carbon::parse($freeze_attendance->to_date);

            // Calculate the difference in days
            $total_days = $fromDate->diffInDays($toDate) + 1;

            $present_days = $freeze_attendance->present_day;

            // $actual_basic = ($freeze_attendance->present_day > 0 && $freeze_attendance->present_day < $total_days) ?
            //     ($freeze_attendance->basic_salary / $freeze_attendance->present_day) * $total_days : $freeze_attendance->basic_salary;

            $rowData = [
                $key + 1, // Sr No.
                $freeze_attendance->Emp_Code,
                $present_days,
                $freeze_attendance->emp_name,
                // $freeze_attendance->pay_band_scale . " " . $freeze_attendance->grade_pay_scale,
                $freeze_attendance->date_of_appointment,
                optional($freeze_attendance->designation)->name,
                $freeze_attendance->employee?->pan,
                $freeze_attendance->actual_basic,
                $freeze_attendance->earned_basic,
                $freeze_attendance->leave_pay,
            ];


            foreach ($allowances as $allowance) {
                $index = array_search($allowance->id, $explode_allowance_ids);
                if ($index !== false) {
                    if (array_key_exists($allowance->id, $allowanceTotals)) {
                        $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                    } else {
                        $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                    }
                    $rowData[] = $explode_allowance_amt[$index];
                } else {
                    $rowData[] = '';
                }
            }

            $rowData[] = ($freeze_attendance->festival_allowance);
            $rowData[] = ($freeze_attendance->corporation_share_da);
            // Add total earning
            $rowData[] = ($freeze_attendance->basic_salary + $freeze_attendance->total_allowance + $freeze_attendance->corporation_share_da);

            foreach ($deductions as $deduction) {
                $index = array_search($deduction->id, $explode_deduction_ids);
                if ($index !== false) {
                    if (array_key_exists($deduction->id, $deductionTotals)) {
                        $deductionTotals[$deduction->id] += $explode_deduction_amt[$index];
                    } else {
                        $deductionTotals[$deduction->id] = $explode_deduction_amt[$index];
                    }
                    $rowData[] = $explode_deduction_amt[$index];
                } else {
                    $rowData[] = '';
                }
            }


            // Calculate PF Loan and Bank Loan
            $pf_amt = 0;
            $bank_loan = 0;

            if ($freeze_attendance->loan_deduction_id) {
                foreach ($explode_loan_ids as $loan_id) {
                    $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();
                    if ($emp_loan && $emp_loan->loan_id == 1) {

                        $pf_amt = $emp_loan->installment_amount;
                        $grand_total_pf += $pf_amt;
                    } else {
                        $bank_loan = $emp_loan->installment_amount;
                    }
                }
            }

            $grand_total_bank_loan += $bank_loan;
            $grand_total_stamp_duty += $freeze_attendance->stamp_duty;
            $grand_total_lic += $freeze_attendance->total_lic_deduction;

            $grand_total_deductions += $freeze_attendance->total_deduction;

            $grand_total_net_salary += $freeze_attendance->net_salary;
            $grand_total_corporation_share += $freeze_attendance->corporation_share_da;

            $grand_total_festival_deduction += $freeze_attendance->total_festival_deduction;

            $grand_total_employee_share += $freeze_attendance->employee_share_da;


            $rowData[] = $pf_amt;
            $rowData[] = $bank_loan;
            $rowData[] = $freeze_attendance->total_lic_deduction;
            $rowData[] = $freeze_attendance->total_festival_deduction;
            $rowData[] = $freeze_attendance->stamp_duty;
            $rowData[] = $freeze_attendance->employee_share_da;
            $rowData[] = $freeze_attendance->corporation_share_da;
            $rowData[] = ($freeze_attendance->total_deduction + $freeze_attendance->corporation_share_da);
            $rowData[] = $freeze_attendance->net_salary;
            $rowData[] = optional($freeze_attendance->employee?->employee_status)->remark ?? $freeze_attendance->remark;

            $data[] = $rowData;
        }

        // Add grand total row with allowance totals
        $grandTotalRow = ['Grand Total:', '', '', '', '', '', '', '', $grand_total_basic_salary, $grand_total_leave_pay];
        foreach ($allowances as $allowance) {
            if (isset($allowanceTotals[$allowance->id])) {
                $grandTotalRow[] = $allowanceTotals[$allowance->id];
            }
        }

        $grandTotalRow[] = $grand_total_festival_allowance;
        $grandTotalRow[] = $grand_total_corporation_share;
        $grandTotalRow[] = ($grand_total_earn + $grand_total_corporation_share);

        foreach ($deductions as $deduction) {
            if (isset($deductionTotals[$deduction->id])) {
                $grandTotalRow[] = $deductionTotals[$deduction->id];
            }
        }

        $grandTotalRow[] = $grand_total_pf;
        $grandTotalRow[] = $grand_total_bank_loan;
        $grandTotalRow[] = $grand_total_lic;
        $grandTotalRow[] = $grand_total_festival_deduction;
        $grandTotalRow[] = $grand_total_stamp_duty;
        $grandTotalRow[] = $grand_total_employee_share;
        $grandTotalRow[] = $grand_total_corporation_share;
        $grandTotalRow[] = ($grand_total_deductions + $grand_total_corporation_share);
        $grandTotalRow[] = $grand_total_net_salary;


        $data[] = $grandTotalRow;

        return collect($data);
    }



    public function headings(): array
    {
        $headings = [
            'Sr No.',
            'Employee Code',
            'Total Present Days',
            'Name',
            // 'Pay Scale',
            'Date of Appointment',
            'Designation',
            'PAN Card',
            'Actual Basic',
            'Earned_Pay',
            'LEAVEPAY'
        ];

        // Add allowance headings dynamically
        $allowances = Allowance::get();
        foreach ($allowances->chunk(5) as $chunk) {
            foreach ($chunk as $allowance) {
                $headings[] = substr($allowance->allowance, 0, 5);
            }
        }

        $headings[] = 'Fest Adv.';
        $headings[] = 'DCPS I CORPORATION CONTRIBUTION ALLOWANCE';
        $headings[] = 'Total_Earn';

        $deductions = Deduction::get();
        foreach ($deductions->chunk(5) as $chunk) {
            foreach ($chunk as $deduction) {
                $headings[] = substr($deduction->deduction, 0, 5);
            }
        }

        $headings[] = 'PF Loan';
        $headings[] = 'Bank Loan';
        $headings[] = 'LIC';
        $headings[] = 'Festival';
        $headings[] = 'Revenue Stamp';
        $headings[] = 'DCPS I EMPLOYEE CONTRIBUTION';
        $headings[] = 'DCPS I CORPORATIONCONTRIBUTIONDEDUCTION';
        $headings[] = 'Total Deduct';

        $headings[] = 'Net Salary';
        $headings[] = 'Remark';

        return $headings;
    }
}
