<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SupplimentaryExport implements FromView
{
    protected $suplimentries;
    protected $employeeSalary;

    public function __construct($suplimentries, $employeeSalary)
    {
        $this->suplimentries = $suplimentries;
        $this->employeeSalary = $employeeSalary;
    }

    public function view(): View
    {
        return view('admin.reports.suplimentry.excel', [
            'suplimentries' => $this->suplimentries,
            'employeeSalary' => $this->employeeSalary,
        ]);
    }
}
