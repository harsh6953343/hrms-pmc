<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeeAttendanceExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $attendances;

    public function __construct($attendances)
    {
        $this->attendances = $attendances;
    }

    public function view(): View
    {
        return view('exports.attendance', [
            'attendances' => $this->attendances
        ]);
    }
}
