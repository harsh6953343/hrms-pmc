<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Employee;
use App\Models\Allowance;
use App\Models\FreezeAttendance;
use Illuminate\Support\Facades\DB;

class IncomeTaxExportFile implements FromCollection, WithHeadings
{
    protected $financial_year;

    public function __construct($financial_year)
    {
        $this->financial_year = $financial_year;
    }

    public function collection()
    {
        $financial_year = $this->financial_year;
        // $financial_year_id = 1;
        $excel_row = array();
        // $employees = Employee::with('ward')->where('employee_id',3220)->get();
        $employees = Employee::with('ward')->get();

        foreach ($employees as $emp) {
            // echo $emp->employee_id . "\n";
            //--Get current gross salary and supplementary bill-------------------//
            $data['allowances'] = Allowance::get();

            // Extract the start year from the current fiscal year string
            $startYear = intval(substr($financial_year, 2, 4));
            // Calculate the previous fiscal year range 
            $previousEndYear = $startYear;
            $endYear = $startYear + 1;

            $currentYearRec = FreezeAttendance::with('employee')
                ->where('from_date', '>=', "$previousEndYear-05-01")
                ->where('to_date', '<', "$endYear-02-30")
                ->where('freeze_status', 1)
                ->where('Emp_code', $emp->employee_id)
                ->get();

            $months_remaining = 10 - $currentYearRec->count();
            $lastRec = $currentYearRec->last();
            if ($lastRec) {
                $last_gross = $lastRec->basic_salary + $lastRec->total_allowance;
                $estimated_salary = $last_gross * $months_remaining;
            } else {
                $excel_row[] = array($emp->employee_id, $emp->fname, 0, 0, 0, 0, 0);
                continue;
            }

            $data['freezeAttendances'] = $currentYearRec;
            $income = $this->getGrossSalary($data);

            //Supplementary bill
            $supplementary_bill = 0;
            // $supp_bill = SuplimentaryEmployeeLeaveBill::where('financial_year_id', $financial_year_id)
            //     ->where('Emp_code', $emp->employee_id)
            //     ->selectRaw('SUM(suplimentary_total_earning) as suppbill')
            //     ->groupBy('Emp_code')
            //     ->get();
            // if ($supp_bill->count()) {
            //     $supplementary_bill = $supp_bill[0]->suppbill;
            // }

            $supp_bill_csv = DB::table('csv_imported_supplementary')
                ->where('employeeid', $emp->employee_id)
                ->selectRaw('SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
                ->groupBy('employeeid')
                ->get();

            //------------------------------------------------------------------------------------//

            $dcps_emp = $dcps_empr = $gpf = $income_tax = $total_lic_deduction = 0;
            foreach ($currentYearRec as $ded) {
                $ded_ids = explode(',', $ded->deduction_Id);
                $ded_amts = explode(',', $ded->deduction_Amt);

                foreach ($ded_ids as $key => $id) {
                    if ($id == 11) {     //LIC
                        $total_lic_deduction += $ded_amts[$key];
                    } elseif ($id == 7) {   //dcps employee
                        $dcps_emp += $ded_amts[$key];
                    } elseif ($id == 8) {   //dcps employer
                        $dcps_empr += $ded_amts[$key];
                    } elseif ($id == 5) {     //gpf
                        $gpf += $ded_amts[$key];
                    } elseif ($id == 2) {     //income tax
                        $income_tax += $ded_amts[$key];
                    }
                }
                $dcps_emp += $ded->employee_share_da;
                $dcps_empr += $ded->corporation_share_da;
            }

            if ($supp_bill_csv->count()) {
                $supplementary_bill += $supp_bill_csv[0]->suppbill;
                $total_lic_deduction += $supp_bill_csv[0]->lic;
                $dcps_emp += $supp_bill_csv[0]->dcpsemployee;
                $dcps_empr += $supp_bill_csv[0]->dcpsemployer;
                $gpf += $supp_bill_csv[0]->gpf;
            }

            //Process excel imported records
            $deductionsFromExcel = DB::table('csv_imported_salary')
                ->select('Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX')
                ->where('Emp_Code', $emp->employee_id)
                ->get();
            $gross_salary_mar_april = 0;
            foreach ($deductionsFromExcel as $ded) {

                $gross_salary_mar_april += $ded->Total_Earning;
                $total_lic_deduction += $ded->LIC;
                $dcps_emp += $ded->DCPS_I_EMPLOYEE_CONTRIBUTION;
                $dcps_empr += $ded->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION;
                $gpf += $ded->GENERAL_PROVIDENT_FUND;
                $income_tax += $ded->INCOME_TAX;
            }

            //Dearness allowance
            $da_csv = DB::table('csv_imported_da')
                ->where('EmployeeId', $emp->employee_id)
                ->selectRaw('SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
                ->groupBy('EmployeeId')
                ->get();
            $da_gross = 0;
            if ($da_csv->count()) {
                $da_gross = $da_csv[0]->da;
                $dcps_emp += $da_csv[0]->dcpsemployee;
                $dcps_empr += $da_csv[0]->dcpsemployer;
            }

            //7th pay difference
            $seventh_pay_diff_csv = DB::table('csv_imported_7th_difference')
                ->where('Employee ID', $emp->employee_id)
                ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
                ->groupBy('Employee ID')
                ->get();
            $seventh_pay_diff_gross = 0;
            if ($seventh_pay_diff_csv->count()) {
                $seventh_pay_diff_gross = $seventh_pay_diff_csv[0]->fifth_installment;
            }
            //====================================================================

            $totalgross = $income['gross_salary'] + $gross_salary_mar_april + $da_gross + $seventh_pay_diff_gross;
            $gross_income = $totalgross + $estimated_salary + $supplementary_bill;
            $net_salary = $gross_income - 52500;
            if ($gpf) {
                $total_taxable_salary = $net_salary - $total_lic_deduction - $gpf;
            } else {
                $total_taxable_salary = $net_salary - $total_lic_deduction - $dcps_emp - $dcps_empr;
            }

            $tax_details = $this->calculateTax($total_taxable_salary, $income_tax);
            $excel_row[] = array_merge(array($emp->employee_id, $emp->fname . ' ' . $emp->mname . ' ' . $emp->lname, $emp->ward->name, $totalgross, $estimated_salary, $total_lic_deduction, $dcps_emp, $dcps_empr, $gpf, $total_taxable_salary), $tax_details);
        }
        $data[] = $excel_row;
        return collect($excel_row);
    }

    public function headings(): array
    {
        return ['Emp_code', 'Name', 'GroupName', 'Gross Salary', 'Estimated Salary', 'LIC', 'DCPS Emp', 'DCPS Corporation', 'GPF', 'Net Salary', 'Total tax', 'Rebate', 'Tax deducted', 'Tax pending'];
    }

    public function calculateTax($taxable_income, $tax_deducted)
    {

        $tax = 0;

        if ($taxable_income <= 250000) {
            $tax = 0;
        } else if ($taxable_income <= 500000) {
            $tax = ($taxable_income - 250000) * 0.05;
        } else if ($taxable_income <= 1000000) {
            $tax = 12500 + ($taxable_income - 500000) * 0.2;
        } else {
            $tax = 112500 + ($taxable_income - 1000000) * 0.3;
        }

        $net_tax = round($tax, 2);
        $edu_cess = round($net_tax * 0.04);

        //Apply rebate Section 87a
        $rebated_amount = 0;
        if ($taxable_income <= 500000) {
            if ($net_tax < 12500) {
                $rebated_amount = $net_tax;
            } else {
                $rebated_amount = 12500;
            }
            $net_tax = $net_tax - $rebated_amount;
        }
        $rebate = $rebated_amount;

        if ($net_tax == 0) {
            $edu_cess = 0;
        }
        $total_tax = round($net_tax + $edu_cess);

        // $('#tax_payable').val(net_tax)
        // $('#tax_before_cess').val(Math.round(net_tax));
        // $('#tax_after_cess').val(total_tax);
        // $('#edu_cess').val(edu_cess);

        $tax_pending = $total_tax - $tax_deducted;

        return array($total_tax, $rebate,  $tax_deducted, $tax_pending);
    }

    //--Get the current gross salary
    public function getGrossSalary($data)
    {
        $grand_total_basic_salary = 0;
        $grand_total_da_differance = 0;
        $grand_total_earn = 0;
        $grand_total_festival_allowance = 0;

        foreach ($data['freezeAttendances'] as $index_key => $freezeAttendance) {

            $grand_total_basic_salary += $freezeAttendance->basic_salary;
            $grand_total_da_differance += $freezeAttendance->da_differance;

            $grand_total_earn += ($freezeAttendance->basic_salary + $freezeAttendance->total_allowance);
            $grand_total_festival_allowance += $freezeAttendance->festival_allowance;
        }
        $grand_total_earn += $grand_total_da_differance;

        return array('gross_salary' => $grand_total_earn);
    }
}
