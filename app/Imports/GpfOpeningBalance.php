<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\PfOpeningBalance;
use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GpfOpeningBalance implements ToCollection, WithHeadingRow
{


    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $employee = Employee::where('employee_id', $row['emp_id'])->first();

            if ($employee && $row['bal'] && $employee->pf_account_no)
            {
                PfOpeningBalance::updateOrCreate([
                    'employee_id' => $employee->id,
                    'Emp_Code' => $employee->employee_id,
                ],[
                    'pf_no' => $employee->pf_account_no,
                    'financial_year_id' => 1,
                    'opening_balance' => $row['bal'],
                ]);
            }
        }
    }

}
