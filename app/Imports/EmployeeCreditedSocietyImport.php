<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\EmployeeSalary;
use App\Models\EmployeeDeduction;
use App\Models\OldEmployeeSalary;
use App\Models\OldEmployeeDeduction;
use Illuminate\Support\Facades\DB;


class EmployeeCreditedSocietyImport implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        if (isset($row['emp_id']) && $row['emp_id'] != "") {
            try {
                DB::beginTransaction();
                $employeeSalary = EmployeeSalary::where('Emp_Code', $row['emp_id'])->first();

                $oldEmployeeSalary = OldEmployeeSalary::where('Emp_Code', $row['emp_id'])->latest()->first();

                $deductionData = [
                    'employee_id' => $employeeSalary->employee_id,
                    'Emp_Code' => $employeeSalary->Emp_Code,
                    'employee_salary_id' => $employeeSalary->id,
                    'deduction_id' => 10,
                    'deduction_amt' => isset($row['amount']) ? $row['amount'] : null,
                    'deduction_type' => 1,
                    'is_active' => '1',
                ];

                EmployeeDeduction::updateOrCreate([
                    'Emp_Code' => $employeeSalary->Emp_Code,
                    'deduction_id' => 10
                ], $deductionData);

                $deductionData['applicable_date'] = date('Y-m-d');
                $deductionData['old_employee_salary_id'] = $oldEmployeeSalary->id;

                OldEmployeeDeduction::updateOrCreate([
                    'Emp_Code' => $employeeSalary->Emp_Code,
                    'old_employee_salary_id' => $oldEmployeeSalary->id,
                    'deduction_id' => 10
                ], $deductionData);

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
    }
}
