<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Attendance;
use App\Models\FinancialYear;
use App\Models\OldEmployeeSalary;
use App\Models\EmployeeSalary;
use App\Models\OldEmployeeDeduction;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeAllowance;
use Carbon\Carbon;

class DeleteAbsentAttendanceImport implements ToModel, WithChunkReading
{
    /**
     * @param Collection $collection
     */
    protected $period;
    protected $month;

    public function __construct($period, $month)
    {
        $this->period = $period;
        $this->month = $month;
    }

    public function model(array $row)
    {
        if ($row[5] > 0) {
            DB::beginTransaction();
            $financial_year = FinancialYear::where('is_active', 1)->first();
            try {
                if ($this->period == "1") {
                    $attendance = Attendance::where([
                        'Emp_Code' => $row[1],
                        'month' => $this->month,
                        'financial_year_id' => $financial_year->id
                    ])->first();

                    if ($attendance) {
                        $attendance->total_absent_days = $attendance->total_absent_days - $row[5] + $attendance->total_leave;
                        $attendance->total_present_days = $attendance->total_present_days - $row[5] + $attendance->total_leave;
                        $attendance->save();
                    }
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
    }
    public function chunkSize(): int
    {
        return 200;
    }
}
