<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\DB;
use App\Models\Attendance;

class EmployeeExcelAdjustment implements ToModel, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    protected $noOfDays;
    protected $month;
    protected $fromDate;
    protected $toDate;
    protected $monthDay;

    public function __construct($month, $fromDate, $toDate, $noOfDays, $monthDay)
    {
        $this->month = $month;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->noOfDays = $noOfDays;
        $this->monthDay = $monthDay;
    }

    public function model(array $row)
    {
        if($row[1] && $row[1] != ""){
            DB::beginTransaction();
            try {
                $attendance = Attendance::where([
                    'Emp_Code' => $row[1],
                    'month' => $this->month,
                    'from_date' => $this->fromDate,
                    'to_date' => $this->toDate
                ])->first();

                if ($attendance) {
                    $days = $row[17] + $this->noOfDays;
                    if($days > $this->monthDay){
                        $days = $this->monthDay;
                    }
                    $attendance->total_present_days = $days;
                    $attendance->total_absent_days = $this->monthDay - $days;
                    $attendance->save();
                }

                DB::commit();
            } catch (\Exception $e) {
                \Log::info($e);
                DB::rollback();
            }
        }
    }

    public function chunkSize(): int
    {
        return 200;
    }
}
