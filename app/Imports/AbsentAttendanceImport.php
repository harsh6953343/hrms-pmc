<?php

namespace App\Imports;


use Illuminate\Support\Facades\DB;
use App\Models\Attendance;
use App\Models\FinancialYear;
use App\Models\OldEmployeeSalary;
use App\Models\EmployeeSalary;
use App\Models\OldEmployeeDeduction;
use App\Models\EmployeeDeduction;
use App\Models\EmployeeAllowance;
use App\Models\AddEmployeeLeave;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;

class AbsentAttendanceImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public $empCodes = [];

    public function collection(Collection $rows)
    {
        $financial_year = FinancialYear::where('id', session('financial_year'))->first();

        foreach($rows as $row){
            try {
                DB::beginTransaction();
                if($row[1]){
                    $this->empCodes[] = $row[1];
                }
                if ($row[5] > 0) {

                    if (request()->period == "1") {
                        $attendance = Attendance::where([
                            'Emp_Code' => $row[1],
                            'month' => request()->month,
                            'financial_year_id' => session('financial_year')
                        ])->first();

                        if ($attendance) {
                            $attendance->total_absent_days = $attendance->total_absent_days + $row[5];
                            $attendance->total_present_days = $attendance->total_present_days - $row[5];
                            $attendance->save();
                        }
                    }

                    if (request()->period == "2") {

                        $employeeSalary = EmployeeSalary::with(['employee'])->where('Emp_Code', $row[1])->first();

                        $fromDate = Carbon::parse(request()->from_date);
                        $toDate = Carbon::parse(request()->to_date);

                        $totalDays = $fromDate->diffInDays($toDate) + 1; // +1 to include the last day


                        if ($financial_year) {
                            $month = request()->month;
                            if ($month <= 3) {
                                $year = date('Y', strtotime($financial_year->to_date));
                            } else {
                                $year = date('Y', strtotime($financial_year->from_date));
                            }
                            $month = $month ?? 1;

                            $fromDate = Carbon::parse($year . '-' . ($month) . '-' . 01);
                            $toDate = clone ($fromDate);

                            // $fromDate = (string) $fromDate->startOfMonth()->toDateString();
                            // $toDate = (string) $toDate->endOfMonth()->toDateString();

                            $fromDate = Carbon::parse("$year-$month-21")->toDateString();

                            $toDate = Carbon::parse($fromDate)->addMonth()->startOfMonth()->addDays(19)->toDateString();

                            $attendancess = AddEmployeeLeave::where('from_date', '>=', $fromDate)
                            ->where('to_date', '<=', $toDate)
                            ->whereIn('status', ['1'])
                            ->where('Emp_Code', $employeeSalary->Emp_Code)
                            ->get();

                            $leaveDaysCount  = 0;
                            foreach ($attendancess as $leave) {
                                $leaveStart = Carbon::parse($leave->from_date);
                                $leaveEnd = Carbon::parse($leave->to_date);

                                // Ensure leave days are within the period
                                while ($leaveStart->lte($leaveEnd) && $leaveStart->lte(Carbon::parse($toDate))) {
                                    if ($leaveStart->day >= 21) { // Count only if the date is 21 or later
                                        $leaveDaysCount++;
                                    }
                                    $leaveStart->addDay(); // Move to next day
                                }
                            }

                            $noOfDays = $row[5] - $leaveDaysCount;

                            // \Log::info($noOfDays);

                            if ($employeeSalary && $noOfDays > 0) {
                                $basicSalary = ($employeeSalary->basic_salary / $totalDays) * $noOfDays;

                                $allowances = EmployeeAllowance::where('Emp_Code', $employeeSalary->Emp_Code)
                                    ->select('allowance_id', 'allowance_amt', 'is_active', 'employee_id', 'Emp_Code', 'allowance_type', 'id')
                                    ->get();

                                $total = $basicSalary;

                                $da = 0;

                                foreach ($allowances as $allowance) {
                                    if ($allowance->allowance_id == 6 || $allowance->is_active == 0) {
                                        continue;
                                    }

                                    if ($allowance->allowance_type == "1") {
                                        $amount = ($allowance->allowance_amt / $totalDays) * $noOfDays;
                                        $total = $total + $amount;
                                    } else {
                                        $amount = ((($employeeSalary->basic_salary * $allowance->allowance_amt) / 100) / $totalDays) * $noOfDays;
                                        $total = $total + $amount;
                                    }

                                    if ($allowance->allowance_id == "1") {
                                        if ($allowance->allowance_type == "1") {
                                            $da = ($allowance->allowance_amt);
                                        } else {
                                            $da = (($employeeSalary->basic_salary * $allowance->allowance_amt) / 100);
                                        }
                                    }
                                }

                                if ($employeeSalary->employee?->doj > '2005-11-01' && $employeeSalary->employee?->department_id != 3) {
                                    $amount = (((($da + $employeeSalary->basic_salary) * 14) / 100) / $totalDays) * $noOfDays;
                                    $total = $total + $amount;
                                }

                                $row['amount'] = $total;

                                $oldEmployeeSalary = OldEmployeeSalary::where('Emp_Code', $row[1])->latest()->first();



                                $deductionData = [
                                    'employee_id' => $employeeSalary->employee_id,
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'employee_salary_id' => $employeeSalary->id,
                                    'deduction_id' => 12,
                                    'deduction_amt' => isset($row['amount']) ? $row['amount'] : null,
                                    'deduction_type' => 1,
                                    'is_active' => '1',
                                ];

                                EmployeeDeduction::updateOrCreate([
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'deduction_id' => 12
                                ], $deductionData);

                                $deductionData['applicable_date'] = date('Y-m-d');
                                $deductionData['old_employee_salary_id'] = $oldEmployeeSalary->id;

                                OldEmployeeDeduction::updateOrCreate([
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'old_employee_salary_id' => $oldEmployeeSalary->id,
                                    'deduction_id' => 12
                                ], $deductionData);
                            }else{
                                $oldEmployeeSalary = OldEmployeeSalary::where('Emp_Code', $row[1])->latest()->first();
                                $deductionData = [
                                    'employee_id' => $employeeSalary->employee_id,
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'employee_salary_id' => $employeeSalary->id,
                                    'deduction_id' => 12,
                                    'deduction_amt' => 0,
                                    'deduction_type' => 1,
                                    'is_active' => '1',
                                ];

                                EmployeeDeduction::updateOrCreate([
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'deduction_id' => 12
                                ], $deductionData);

                                $deductionData['applicable_date'] = date('Y-m-d');
                                $deductionData['old_employee_salary_id'] = $oldEmployeeSalary->id;

                                OldEmployeeDeduction::updateOrCreate([
                                    'Emp_Code' => $employeeSalary->Emp_Code,
                                    'old_employee_salary_id' => $oldEmployeeSalary->id,
                                    'deduction_id' => 12
                                ], $deductionData);
                            }
                        }
                    }


                }


                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
    }

    public function getEmpCodes()
    {
        return $this->empCodes;
    }
}
