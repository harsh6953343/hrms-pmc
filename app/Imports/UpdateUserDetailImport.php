<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class UpdateUserDetailImport implements ToModel, WithChunkReading, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        
        DB::beginTransaction();
        try{
            Employee::where('employee_id', $row['emp_id'])->update([
                'account_no' => $row['account_number'],
                'ifsc' => $row['ifsc_code'],
            ]);
            
            DB::commit();
        }catch(\Exception $e){

            // DB::rollback();
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
