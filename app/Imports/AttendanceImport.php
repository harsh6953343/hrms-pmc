<?php

namespace App\Imports;

use App\Models\TenDayAttendnce;
use Maatwebsite\Excel\Concerns\ToModel;

class AttendanceImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($this->isHeader($row)) {
            return null;
        }

        return new TenDayAttendnce([
            'emp_code' => $row[0],             // Employee Code
            'employee_name' => $row[1],       // Employee Name
            'p' => $row[2],                   // Present Days
            'a' => $row[3],                   // Absent Days
            'h' => $row[4],                   // Holidays
            'hp' => $row[5],                  // Half Present
            'wo' => $row[6],                  // Weekly Off
            'wop' => $row[7],                 // Weekly Off Present
            'cl' => $row[8],                  // Casual Leave
            'pl' => $row[9],                  // Privilege Leave
            'sl' => $row[10],                 // Sick Leave
            'other_leave' => $row[11],        // Other Leaves
            'total_leave' => $row[12],        // Total Leaves
            'total_present' => $row[13],      // Total Present Days
            'total_pay_days' => $row[14],     // Total Pay Days
            'total_ot_in_hrs' => $row[15],    // Total Overtime Hours
            'total_late_by' => $row[16],      // Total Late By (minutes/hours)
            'total_early_by' => $row[17],     // Total Early By (minutes/hours)
        ]);
    }

    protected function isHeader(array $row): bool
    {
        return isset($row[0]) && $row[0] === 'Emp. Code';
    }
}
