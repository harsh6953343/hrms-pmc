<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\EmployeeLeaves;
use App\Models\Employee;
use App\Models\AddEmployeeLeave;
use App\Models\FinancialYear;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class PendingEmployeeLeaveImport implements ToModel, WithChunkReading
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        try {
            if ($row[15] > 0) {
                $employee = Employee::where('employee_id', $row[1])->first();

                if ($employee && $employee->id && $row[1] && $row[1] != "") {
                    $financial_year = FinancialYear::where('is_active', 1)->first();


                    // to get to date
                    $toStartDate = AddEmployeeLeave::where([
                        'employee_id' => $employee->id,
                        'month' => request()->month,
                    ])->orderBy('to_date', 'desc')->select('to_date')->first();

                    if ($toStartDate && $toStartDate->to_date && $toStartDate->to_date != "") {
                        $fromDate = date('Y-m-d', strtotime('+1 days', strtotime($toStartDate->to_date)));
                    } else {
                        $date = date('Y') . '-' . request()->month . '-01';
                        $fromDate = date('Y-m-d', strtotime($date));
                    }
                    // end of code
                    $leaveType = 2;
                    $noOfLeave = $row[15];
                    $noOfLeaves = (int)($row[15] - 1);
                    // if ($noOfLeaves > 1) {
                    //     $noOfLeaves = $noOfLeaves - 1;
                    // }

                    $toDate = date('Y-m-d', strtotime("+$noOfLeaves days", strtotime($fromDate)));

                    $data = [
                        'employee_id' => $employee->id,
                        'Emp_Code' => $employee->employee_id,
                        'leave_type_id' => $leaveType,
                        'from_date' => $fromDate,
                        'to_date' => $toDate,
                        'rejoin_date' => date('Y-m-d', strtotime('+1 days', strtotime($toDate))),
                        'no_of_days' => $noOfLeave,
                        'month' => request()->month,
                        'financial_year_id' => $financial_year->id,
                    ];

                    AddEmployeeLeave::create($data);
                }
            }
        } catch (\Exception $e) {
            \Log::info('Something went wrong');
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
