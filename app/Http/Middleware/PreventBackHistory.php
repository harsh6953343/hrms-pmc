<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PreventBackHistory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!$request->routeIs('pay-sheet-excel.store') && !$request->routeIs('reports.muster') && !$request->routeIs('reports.suplimentry-slip.excel') && !$request->routeIs('reports.suplimentry-bank-statement-excel') && !$request->routeIs('reports.bank-statement-excel') && !$request->routeIs('income-tax.report-excel') && !$request->routeIs('da-differance.index')) {
            $response = $next($request);
            return $response->header('Cache-Control', 'nocache,no-store,max-age=0;must-revalidate')
                ->header('Pragma', 'no-cache')
                ->header('Expires', 'Sun, 02 Jan 1990 00:00:00 GMT');
        }
        return $response = $next($request);
    }
}
