<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateDesignationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $designationId = $this->edit_model_id ?? null;

        return [
            'ward_id' => 'required',
            'department_id' => 'nullable',
            'clas_id' => 'nullable',
            'name' => ['required', Rule::unique('designations')->ignore($designationId)->whereNull('deleted_at')],
            'marathi_name' => ['required', Rule::unique('designations')->ignore($designationId)->whereNull('deleted_at')],
            'initial' => 'nullable',
        ];
    }
}
