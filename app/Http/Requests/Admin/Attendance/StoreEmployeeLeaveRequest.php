<?php

namespace App\Http\Requests\Admin\Attendance;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeLeaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'   =>  'required',
            'Emp_Code'      =>  'required',
            'leave_type_id' =>  'required',
            'from_date'     =>  'required',
            'to_date'       =>  'required',
            'rejoin_date'   =>  'required',
            'no_of_days'    =>  'required',
            'remark'        =>  'nullable',
            'financial_year_id' => 'nullable',
            'month'         =>  'required',
            'leave_files'   =>  'required'
        ];
    }
}
