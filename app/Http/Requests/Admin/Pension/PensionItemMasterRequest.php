<?php

namespace App\Http\Requests\Admin\Pension;

use Illuminate\Foundation\Http\FormRequest;

class PensionItemMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        if ($this->edit_model_id) {
            $rule = [
                'name' => "required|unique:pension_item_masters,name,$this->edit_model_id,id",
                'basis' => 'required',
                'based_on' => 'nullable',
                'amount' => 'required',
                'type_of_pay' => 'required',
                'status' => 'required',
            ];
        } else {
            $rule = [
                'name' => 'required|unique:pension_item_masters,name,NULL,NULL',
                'basis' => 'required',
                'based_on' => 'nullable',
                'amount' => 'required',
                'type_of_pay' => 'required',
                'status' => 'required',
            ];
        }
        return $rule;
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter name',
            'basis.required' => 'Please select basis',
            'amount.required' => 'Please enter amount',
            'type_of_pay.required' => 'Please select pay type',
            'status.required' => 'Please select status',
        ];
    }
}
