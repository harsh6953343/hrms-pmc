<?php

namespace App\Http\Requests\Admin\Pension;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewPensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'Emp_Code'   => 'required',
            'employee_id'   => 'required',
            'emp_title' => 'required',
            'emp_fname' => 'required',
            'emp_fname_marathi' => 'required',
            'emp_mname' => 'required',
            'emp_mname_marathi' => 'required',
            'emp_lname' => 'required',
            'emp_lname_marathi' => 'required',
            'pay_scale_id' => 'nullable',
            'dob' => 'nullable',
            'age' => 'nullable',
            'doj' => 'nullable',
            'dor' => 'nullable',
            'dod' => 'nullable',
            'percent_share' => 'nullable',
            'pensioner_title' => 'nullable',
            'pensioner_type' => 'nullable',
            'pensioner_fname' => 'nullable',
            'pensioner_fname_marathi' => 'nullable',
            'pensioner_mname' => 'nullable',
            'pensioner_mname_marathi' => 'nullable',
            'pensioner_lname' => 'nullable',
            'pensioner_lname_marathi' => 'nullable',
            'mobile' => 'nullable|digits:10',
            'aadhar' => 'nullable|digits:12',
            'address' => 'nullable',
            'address_marathi' => 'nullable',
            'city' => 'nullable',
            'state' => 'nullable',
            'pension_bank_id' => 'nullable',
            'branch' => 'nullable',
            'account_no' => 'nullable|numeric',
            'ifsc_code' => 'nullable',
            'pensionser_status' => 'nullable',
            'status_change_date' => 'nullable',
            'remarks' => 'nullable',
            'pension_start_date' => 'nullable',
            'da_applicabe' => 'nullable',
            'pay_commission' => 'nullable',
            'basic'     => 'nullable',
            'basic_date'     => 'nullable',
            'after_basic'     => 'nullable',
            'after_basic_date'     => 'nullable',
            'dearness_relief'     => 'nullable',
            'dearness_relief_date'     => 'nullable',
            'relief_fund'     => 'nullable',
            'relief_fund_date'     => 'nullable',
            'pensioner_arrears'     => 'nullable',
            'pensioner_arrears_date'     => 'nullable',
            'special_allowance'     => 'nullable',
            'special_allowance_date'     => 'nullable',
            'miscellaneous_deduction'     => 'nullable',
            'miscellaneous_deduction_date'     => 'nullable',
            'miscellaneous_arrears'     => 'nullable',
            'miscellaneous_arrears_date'     => 'nullable',
            'recovery'     => 'nullable',
            'recovery_date'     => 'nullable',
            'commutation'     => 'nullable',
            'commutation_date'     => 'nullable',
            'ward_id' => 'nullable',
            'ward_name' => 'nullable',
            'department_id' => 'nullable',
            'designation_id' => 'nullable',
            'designation' => 'nullable',
            'last_basic' => 'nullable',
            'last_grade_pay' => 'nullable',
            'pension_end_date' => 'nullable',
        ];
    }
}
