<?php

namespace App\Http\Requests\Admin\Attendances;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttendanceExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'month' => 'required',
            'period' => 'required',
            'file'  => 'required|file|mimes:xlsx,csv|max:2048',
            'financial_year_id' => 'nullable',
        ];
    }
}
