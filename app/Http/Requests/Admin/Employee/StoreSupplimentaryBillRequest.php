<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreSupplimentaryBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'           =>  'required',
            'Emp_Code'              =>  'required',
            'month'                 =>  'required',
            'from_date'             =>  'nullable',
            'to_date'               =>  'nullable',
            'remaining_freeze_id'  =>   'nullable',
            'financial_year_id'     =>  'nullable',
            'type'                  =>  'nullable',
        ];
    }
}
