<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncrementBasicSalaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'increment_type'    => 'required',
            'department_id'     => 'required',
            'month'             => 'required',
            'applicable_date'   => 'required',
            'emp_id'            => 'nullable',
            'incrementted_basic' => 'nullable',
            'salary_unique_id'  => 'nullable',
        ];
    }
}
