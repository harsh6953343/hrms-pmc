<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'           =>  'required',
            'Emp_Code'              =>  'required',
            'status_id'             =>  'required',
            'remark'                =>  'required',
            'applicable_date'       =>  'required',
            'is_salary_applicable'  => 'required|in:0,1',
            'salary_percent'       => [
                                        'required_if:is_salary_applicable,1',
                                        'nullable',
                                        'numeric',
                                        'min:0',
                                        'max:100',
                                    ],
        ];
    }

    public function messages(): array
    {
        return [
            'salary_percent.required_if' => 'The salary percent field is required when salary is applicable.',
        ];
    }
}
