<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmployeeAttendanceExport;

class ExportController extends Controller
{
    public function exportAttendanceWithLeaveApplied()
    {
        $attendnace = Attendance::with(['employee' => fn($q) => $q->with('ward', 'department')])->where('month', 2)->where('financial_year_id', 1)->paginate(400);

        return Excel::download(new EmployeeAttendanceExport($attendnace), 'attendance.xlsx');
    }
}
