<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\EmployeeExcelAdjustment;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\AdjustAttandanceImport;
use Carbon\Carbon;

class AttandanceAdjustmentController extends Controller
{
    public function index(){
        return view('attandance.adjustment.index');
    }

    public function save(Request $request){
        set_time_limit(0);

        $request->validate([
            'import' => 'required|mimes:xlsx,xls',
            'month' => 'required',
            'no_of_days' => 'required',
        ], [
            'import.required' => 'Please select file.',
            'import.mimes' => 'Only xlsx and xls file is allowed.',
            'month.required' => 'Please select month',
            'month.import' => 'Please select file',
            'no_of_days.required' => 'Please enter no of days',
        ]);

        try {
            $from = Carbon::parse($request->from_date);
            $to = Carbon::parse($request->to_date);

            $monthDays = $from->diffInDays($to) + 1;

            Excel::import(new EmployeeExcelAdjustment($request->month, $request->from_date, $request->to_date, $request->no_of_days, $monthDays), $request->file('import'));

            if ($request->hasFile('import')) {
                $request['file'] = $request->import->store('export-attandance-adjustment-excel');
            }
            $request['financial_year'] = session('financial_year');
            AdjustAttandanceImport::create($request->all());

            return redirect()->back()->with(['success' => 'File import successfully']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => 'Something went wrong, please try again']);
        }
    }
}
