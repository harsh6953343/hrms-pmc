<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FinancialYear;
use App\Imports\AttandanceSheetExcelImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportAttendanceController extends Controller
{
    public function index()
    {
        $financialYears = FinancialYear::get();

        return view('attandance.import.excel')->with(['financialYears' => $financialYears]);
    }


    public function store(Request $request)
    {
        set_time_limit(0);

        $request->validate([
            'import' => 'required|mimes:xlsx,xls',
            'month' => 'required'
        ], [
            'import.required' => 'Please select file.',
            'import.mimes' => 'Only xlsx and xls file is allowed.',
            'month.required' => 'Please select month'
        ]);

        try {

            Excel::import(new AttandanceSheetExcelImport, $request->file('import'));

            // if ($request->hasFile('import')) {
            //     $request['file'] = $request->import->store('export-excel');
            // }
            // $request['financial_year'] = session('financial_year');
            // ImportExcelFile::create($request->all());

            return redirect()->back()->with(['success' => 'File import successfully']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => 'Something went wrong, please try again']);
        }
    }
}
