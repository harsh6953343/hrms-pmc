<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UpdateUserDetailImport;

class ImportExcelController extends Controller
{
    public function updateEmployeeDetails(Request $request){
        return view('admin.import.update-user-details');
    }

    public function storeEmployeeUpdateDetails(Request $request){
        set_time_limit(0);

        Excel::import(new UpdateUserDetailImport, $request->file('import'));

        return redirect()->back()->with('success', "User details updated successfully");
    }
}
