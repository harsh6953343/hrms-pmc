<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Models\FreezePension;
use App\Models\FreezePensionNew;
use App\Models\PensionBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use Barryvdh\Snappy\Facades\SnappyPdf;

class PensionBillNewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $bank_id            = $request->bank ?? '';
        $month              = $request->month ?? '';
        $from_date          = $request->from_date ?? '';
        $to_date            = $request->to_date ?? '';
        $pensioner_type     = $request->pensioner_type ?? '';
        $ward_id            = $request->ward_id ?? '';


        $freeze_pension_data = FreezePensionNew::query()->with('bank')
            ->when($bank_id != 'all', function ($query) use ($bank_id) {
                return $query->where('pension_bank_id', $bank_id);
            })
            ->whereHas('pensionNew', function ($q) {
                $q->where('pensionser_status', '!=', 5);
            })
            ->when($pensioner_type != 'all', function ($query) use ($pensioner_type) {
                return $query->where('pensioner_type', $pensioner_type);
            })
            ->when($pensioner_type == 'all', function ($query) use ($pensioner_type) {
                return $query->where('pensioner_type', '!=', 'family');
            })
            ->when($ward_id != 'all', function ($query) use ($ward_id) {
                return $query->where('ward_id', $ward_id);
            })
            ->when($month, function ($query, $month) {
                return $query->where('month', $month);
            })
            ->when($from_date, function ($query, $from_date) {
                return $query->where('from_date', $from_date);
            })
            ->when($to_date, function ($query, $to_date) {
                return $query->where('to_date', $to_date);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->orderBy('pension_id', 'asc')
            ->get();



        $banks = PensionBank::latest()->get();

        $getPensionerType = FreezePensionNew::select('pensioner_type')->whereNotNull('pensioner_type')
            ->groupBy('pensioner_type')->get();

        $getWards = FreezePensionNew::with(['ward:id,name'])
            ->select('ward_id')
            ->groupBy('ward_id')
            ->get();


        return view('admin.pension.pensioner-bill-new')->with([
            'banks' => $banks,
            'freeze_pensions' => $freeze_pension_data,
            'bank_id' => $bank_id,
            'month'             => $month,
            'from_date'         => $from_date,
            'to_date'           => $to_date,
            'getPensionerType'  => $getPensionerType,
            'pensioner_type'    => $pensioner_type,
            'getWards'          => $getWards,
            'ward_id'           => $ward_id
        ]);
    }

    public function pdf(Request $request)
    {
        $bank_id            = $request->bank ?? '';
        $month              = $request->month ?? '';
        $from_date          = $request->from_date ?? '';
        $to_date            = $request->to_date ?? '';
        $pensioner_type     = $request->pensioner_type ?? '';
        $ward_id            = $request->ward_id ?? '';


        $freeze_pension_datas = FreezePensionNew::query()->with('bank')
            ->when($bank_id != 'all', function ($query) use ($bank_id) {
                return $query->where('pension_bank_id', $bank_id);
            })
            ->whereHas('pensionNew', function ($q) {
                $q->where('pensionser_status', '!=', 5);
            })
            ->when($pensioner_type != 'all', function ($query) use ($pensioner_type) {
                return $query->where('pensioner_type', $pensioner_type);
            })
            ->when($pensioner_type == 'all', function ($query) use ($pensioner_type) {
                return $query->where('pensioner_type', '!=', 'family');
            })
            ->when($ward_id != 'all', function ($query) use ($ward_id) {
                return $query->where('ward_id', $ward_id);
            })
            ->when($month, function ($query, $month) {
                return $query->where('month', $month);
            })
            ->when($from_date, function ($query, $from_date) {
                return $query->where('from_date', $from_date);
            })
            ->when($to_date, function ($query, $to_date) {
                return $query->where('to_date', $to_date);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->orderBy('pension_id', 'asc')
            ->get();


        $filename = "Supplimentary Slip.pdf";
        $pdf = SnappyPdf::loadView('admin.pension.pensioner-bill-new.pdf', compact('freeze_pension_datas'))
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('footer-right', now()->format('F d, Y h:i A'))
            ->setOption('footer-left', 'Page: [page] of [toPage]')
            ->setOption('margin-bottom', 8)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 5)
            ->setOption('margin-right', 5);

        return $pdf->inline($filename);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
