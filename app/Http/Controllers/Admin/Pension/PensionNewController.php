<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Pension\StoreNewPensionRequest;
use App\Http\Requests\Admin\Pension\UpdateNewPensionRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Employee;
use App\Models\PayScale;
use App\Models\PensionBank;
use App\Models\PensionItemMaster;
use App\Models\PensionNew;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;
use DataTables;

class PensionNewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pension.pension-list-new');
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {

            $pensions = PensionNew::when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereHas('employee', function ($q) {
                        $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                    });
                });


            return DataTables::of($pensions)
                ->addIndexColumn()
                ->addColumn('employee_full_name', function ($data) {
                    return $data->emp_fname . ' ' . $data->emp_mname . ' ' . $data->emp_lname;
                })
                ->addColumn('pensioner_full_name', function ($data) {
                    return $data->pensioner_fname . ' ' . $data->pensioner_mname . ' ' . $data->pensioner_lname;
                })
                ->addColumn('pension_status', function ($data) {
                    return [1 => 'Active', 2 => 'Stop', 3 => 'Backdated', 4 => 'Deceased', 5 => 'Hold', 6 => 'Release'][$data->pensionser_status] ?? '-';
                })
                ->addColumn('pensioner_type', function ($data) {
                    return ["regular" => 'Regular Retirement', "family" => 'Family Pension', "inservice" => 'Inservice', "vol_ret" => 'Voluntary Retirement', "ret_med" => 'Retirement Due to Medical Reasons'][$data->pensioner_type] ?? '-';
                })
                ->filterColumn('employee_full_name', function ($query, $keyword) {
                    $query->whereRaw("CONCAT(emp_fname, ' ', emp_mname,' ', emp_lname) LIKE ?", ["%$keyword%"]);
                })
                ->filterColumn('pensioner_full_name', function ($query, $keyword) {
                    $query->whereRaw("CONCAT(pensioner_fname, ' ', pensioner_mname,' ', pensioner_lname) LIKE ?", ["%$keyword%"]);
                })
                ->filterColumn('pensioner_type', function ($query, $keyword) {
                    $query->whereRaw("
                        CASE
                            WHEN pensioner_type = 'regular' THEN 'Regular Retirement'
                            WHEN pensioner_type = 'family' THEN 'Family Pension'
                            WHEN pensioner_type = 'inservice' THEN 'Inservice'
                            WHEN pensioner_type = 'vol_ret' THEN 'Voluntary Retirement'
                            WHEN pensioner_type = 'ret_med' THEN 'Retirement Due to Medical Reasons'
                            ELSE '-'
                        END LIKE ?", ["%{$keyword}%"]);
                })
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $authUser = Auth::user();

        $userDepartment = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');

        $employees = Employee::leftJoin('employee_statuses', 'employees.employee_id', '=', 'employee_statuses.Emp_Code')
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->select('employees.*')
            ->latest('employees.created_at')
            ->where('retirement_date', '<=', date('Y-m-d'))
            ->whereNull('employee_statuses.deleted_at')
            ->get();

        $pension_banks  = PensionBank::latest()->get();

        $payScales = PayScale::latest()->get();

        $wards = Ward::whereHas('departments', function ($q) use ($userDepartment) {
            return $q->whereIn('id', $userDepartment);
        })->latest()
        ->get();
        
        $da_rate        = Allowance::where('id', 1)->first();

        return view('admin.pension.add-pension-new')->with(['pension_banks' => $pension_banks, 'employees' => $employees, 'payScales' => $payScales, 'wards' => $wards, 'da_rate' => $da_rate]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNewPensionRequest $request)
    {
        try {
            $corporation_name = Corporation::first();
            DB::beginTransaction();
            $input = $request->validated();
            $pension = PensionNew::create(Arr::only($input, PensionNew::getFillables()));
            $pension->save();
            $num = rand(10, 10000);
            $pension_id = $corporation_name->initial . 'PEN' . $num . $pension->id;
            $pension->pension_id = $pension_id;
            $pension->save();
            DB::commit();
            return response()->json(['success' => 'Pension created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Pension');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, string $id)
    {
        if($request->ajax()){
            $pension = PensionItemMaster::with('pensionItemMaster')->find($id);

            return response()->json([
                'pension' => $pension,
                'row' => $request->row
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PensionNew $pensionNew)
    {
        $pension_banks  = PensionBank::latest()->get();

        $payScales = PayScale::latest()->get();

        $da_rate = Allowance::where('id', 1)->first();

        return view('admin.pension.edit-pension-new')->with(['pensionNew' => $pensionNew, 'pension_banks' => $pension_banks, 'payScales' => $payScales, 'da_rate' => $da_rate]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNewPensionRequest $request, PensionNew $pensionNew)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $pensionNew->update(Arr::only($input, PensionNew::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Pension Update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Pension Update');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PensionNew $pensionNew)
    {
        try {
            DB::beginTransaction();
            $pensionNew->delete();
            DB::commit();
            return response()->json(['success' => 'Pension deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Pension');
        }
    }
}
