<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Pension\PensionItemMasterRequest;
use App\Models\PensionItemMaster;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PensionItemMasterController extends Controller
{
    public function index()
    {
        $pensionItemMasters = PensionItemMaster::with('pensionItemMaster')->get();

        $basedOns = PensionItemMaster::where('status', 1)->get();

        return view('admin.pension.pension-item-master.index')->with([
            'pensionItemMasters' => $pensionItemMasters,
            'basedOns' => $basedOns
        ]);
    }

    public function store(PensionItemMasterRequest $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $input = $request->validated();
                PensionItemMaster::create($input);
                DB::commit();

                return response()->json(['success' => 'Pension item master created successfully!']);
            } catch (\Exception $e) {
                DB::rollback();
                // return $this->respondWithAjax($e, 'creating', 'Pension Item Master');
            }
        }
    }

    public function edit(PensionItemMaster $pensionItemMaster)
    {

        if ($pensionItemMaster) {

            $pensionItemMasters = PensionItemMaster::where('id', '!=', $pensionItemMaster->id)->where('status', 1)->get();
            $pensionItemMasterHtml = '<span>
            <option value="">--Select Option--</option>';
            foreach ($pensionItemMasters as $pensionItem) :
                $is_select = $pensionItem->id == $pensionItemMaster->based_on ? "selected" : "";
                $pensionItemMasterHtml .= '<option value="' . $pensionItem->id . '" ' . $is_select . '>' . $pensionItem->name . '</option>';
            endforeach;
            $pensionItemMasterHtml .= '</span>';

            $response = [
                'result' => 1,
                'pensionItemMaster' => $pensionItemMaster,
                'pensionItemMasterHtml' => $pensionItemMasterHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PensionItemMasterRequest $request, PensionItemMaster $pensionItemMaster)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $pensionItemMaster->update($input);
            DB::commit();

            return response()->json(['success' => 'Pension item master  updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Pension Item Master ');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
}
