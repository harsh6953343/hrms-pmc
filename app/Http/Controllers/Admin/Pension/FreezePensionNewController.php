<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pension\StoreFreezePensionRequest;
use App\Models\Allowance;
use App\Models\FreezePensionNew;
use App\Models\PensionBank;
use App\Models\PensionNew;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;

class FreezePensionNewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $freeze_pensions =  FreezePensionNew::when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
            $q->whereHas('employee', function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            });
        })
            ->groupBy('from_date', 'to_date', 'month')
            ->select('from_date', 'to_date', 'month')
            ->get();

        return view('admin.pension.freeze-pension-list-new')->with(['freeze_pensions' => $freeze_pensions]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFreezePensionRequest $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->validated();
            $financial_year_id = session('financial_year');
            $from_date = $input['from_date'];
            $to_date = $input['to_date'];
            $month = $input['month'];

            $check_already_added = FreezePensionNew::where('from_date', $from_date)
                ->where('to_date', $to_date)
                ->where('month', $month)
                ->first();
            if ($check_already_added) {
                return response()->json(['error' => 'Already Generated!']);
            } else {

                $da_rate = Allowance::where('id', 1)->value('amount');
                // $pension_banks = PensionBank::latest()->get();

                // foreach ($pension_banks as $bank) {
                // $all_pension_freeze = PensionNew::where('basic', '!=', 0)->where('pension_bank_id', $bank->id)->latest()->get();
                $all_pension_freeze = PensionNew::where('basic', '!=', 0)->where('pensionser_status', 1)->latest()->get();

                foreach ($all_pension_freeze as $row) {
                    $payable_pension_amt = $row->basic;
                    $da_amt = round($row->da_applicabe == 1 ? ($payable_pension_amt * $da_rate / 100) : 0);
                    $total = $payable_pension_amt + $da_amt + $row->pensioner_arrears;
                    $total_deduction = $row->miscellaneous_deduction + $row->commutation + $row->recovery;
                    $net_salary = $total - $total_deduction;

                    FreezePensionNew::create([
                        'pension_id' => $row->pension_id,
                        'pensioner_type' => $row->pensioner_type,
                        'ward_id' => $row->ward_id,
                        'employee_id' => $row->employee_id,
                        'from_date' => $from_date,
                        'to_date' => $to_date,
                        'month' => $month,
                        'financial_year_id' => $financial_year_id,
                        'employee_id' => $row->employee_id,
                        'employee_name' => $row->emp_fname . " " . $row->emp_mname . " " . $row->emp_lname,
                        'pensioner_name' => $row->pensioner_fname . " " . $row->pensioner_mname . " " . $row->pensioner_lname,
                        'account_no' => $row->account_no ?? 0,
                        'ifsc_code' => $row->ifsc_code,
                        'basic' => $row->basic ?? 0,
                        'dearness_relief' => $da_amt ?? 0,
                        'pension_arrears' => $row->pensioner_arrears ?? 0,
                        'total_earning' => $total ?? 0,
                        'miscellaneous_deduction' => $row->miscellaneous_deduction ?? 0,
                        'commutation'             => $row->commutation ?? 0,
                        'recovery'                => $row->recovery ?? 0,
                        'total_deduction'         => $total_deduction ?? 0,
                        'net_salary'              => $net_salary ?? 0,
                        'pension_bank_id'         => $row->pension_bank_id,
                    ]);
                }
                // }
                DB::commit();
            }
            return response()->json(['success' => 'Freeze Pension successfully.']);
        } catch (\Exception $e) {
            DB::rollBack();
            // Log the error message for debugging
            \Log::error('Failed to freeze pension:', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);
            // Return the error message in the response
            return response()->json([
                'error' => 'Failed to freeze pension.',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $from_date)
    {
        // Soft delete the records based on the provided criteria
        $recordsToDelete = FreezePensionNew::where('from_date', $from_date)->get();
        $deleteCount = $recordsToDelete->count();
        $recordsToDelete->each->delete();
        return response()->json([
            'success' => "$deleteCount records deleted successfully.",
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
