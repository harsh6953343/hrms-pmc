<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Models\PensionNew;
use App\Models\PensionItemMaster;
use App\Models\PenssionerPayroll;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ManagePensionerPayrollControler extends Controller
{
    public function index(Request $request) {
        if ($request->ajax()) {

            $pensions = PensionNew::with('designation', 'pay_scale', 'department');

            return DataTables::of($pensions)
                ->addIndexColumn()
                ->addColumn('employee_full_name', function ($data) {
                    return $data->emp_fname . ' ' . $data->emp_mname . ' ' . $data->emp_lname;
                })
                ->addColumn('employee_full_mname', function ($data) {
                    return $data->emp_fname . ' ' . $data->emp_mname . ' ' . $data->emp_lname;
                })
                ->addColumn('pensioner_full_mname', function ($data) {
                    return $data->pensioner_fname . ' ' . $data->pensioner_mname . ' ' . $data->pensioner_lname;
                })
                ->addColumn('designation_name', function ($data) {
                    return $data?->designation?->name;
                })
                ->addColumn('department_name', function ($data) {
                    return $data?->department?->name;
                })
                ->addColumn('grade_pay', function ($pension) {
                    if (isset($pension->pay_scale->grade_pay_name)) {
                        return $pension->pay_scale->grade_pay_name . " " . "7th Pay Comission /" . $pension->pay_scale->pay_band_scale . " /" . $pension->pay_scale->amount;
                    } else {
                        return "-";
                    }
                })
                ->filterColumn('employee_full_name', function ($query, $keyword) {
                    $query->whereRaw("CONCAT(emp_fname, ' ', emp_mname,' ', emp_lname) LIKE ?", ["%$keyword%"]);
                })
                ->filterColumn('pensioner_full_mname', function ($query, $keyword) {
                    $query->whereRaw("CONCAT(pensioner_fname, ' ', pensioner_mname,' ', pensioner_lname) LIKE ?", ["%$keyword%"]);
                })
                ->filterColumn('designation_name', function ($query, $keyword) {
                    $query->whereHas('designation', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->filterColumn('department_name', function ($query, $keyword) {
                    $query->whereHas('department', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->filterColumn('grade_pay', function ($query, $keyword) {
                    $query->whereHas('pay_scale', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("CONCAT(grade_pay_name, ' 7th Pay Comission /', pay_band_scale,' /', amount) LIKE ?", ["%$keyword%"]);
                    });
                })
                ->make(true);
        }

        return view('admin.pension.manage-pension-payroll.index');
    }

    public function edit($id){
        $pensionNew = PensionNew::with(['pensionPayrolls', 'department', 'designation', 'pay_scale'])->find($id);

        $pensionItemMasters = PensionItemMaster::where('status', 1)->get();

        return view('admin.pension.manage-pension-payroll.edit')->with([
            'pensionItemMasters' => $pensionItemMasters,
            'pensionNew' => $pensionNew
        ]);
    }

    public function update(Request $request, $id){
        // return $id;
        if(isset($request->pension_item_master_id) && count($request->pension_item_master_id) > 0){
            // return $request->all();
            try{
                DB::beginTransaction();
                for($i = 0; $i < count($request->pension_item_master_id); $i++){
                    PenssionerPayroll::updateOrCreate([
                        'pension_new_id' => $id,
                    ], [
                        'pension_new_id' => $id,
                        'pension_item_master_id' => $request->pension_item_master_id[$i],
                        'basis' => $request->basisId[$i],
                        'based_on' => $request->basedOnId[$i],
                        'type_of_pay' => $request->typeOfPayId[$i],
                        'amount' => $request->amount[$i],
                        'effective_date' => ($request->effective_date[$i]) ? date('Y-m-d', strtotime($request->effective_date[$i])) : null,
                        'reason' => $request->reason[$i],
                    ]);
                }
                DB::commit();

                return redirect()->back()->with('success', 'Pensioner Details upddated or created successfully');
            }catch(\Exception $e){
                DB::rollback();
                Log::info($e);
                return redirect()->back()->with('error', 'Something went wrong, please try again later');
            }
        }else{
            return redirect()->back()->with('error', 'Something went wrong, please try again later');
        }
    }
}
