<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\FreezeAttendance;
use App\Models\Ward;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\Models\UserDepartment;
use App\Models\PensionNew;
use Illuminate\Support\Facades\DB;
use App\Models\Attendance;

class DashboardController extends Controller
{

    public function index()
    {
        $userDepartment = [];
        if (Auth::user()->hasRole(['AMC', 'DMC'])) {
            $userDepartment = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');
        }
        $authUser = Auth::user();

        $emp_statuses = EmployeeStatus::when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
            $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                $employeeQuery->where('ward_id', $authUser->ward_id);
            });
        })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('department_id', $authUser->department_id);
                });
            })

            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereHas('employee', function ($q) use ($userDepartment) {
                    $q->whereIn('department_id', $userDepartment);
                });
            })

            ->latest()
            ->count();

        $wards = Ward::latest()->count();

        $departments = Department::when($authUser->hasRole(['Ward HOD']), fn($q) => $q->where('ward_id', $authUser->ward_id))
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('id', $userDepartment);
            })
            ->latest()->count();

        $banks = Bank::latest()->count();

        $retire_employees = Employee::with('ward', 'department')
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('retirement_date', '>=', Carbon::now())
            ->where('retirement_date', '<', Carbon::now()->addMonths(6)->startOfDay())
            ->latest()
            ->get();


        $employee_salary = FreezeAttendance::with('employee')->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->where('net_salary', '<', 0)
            ->get();

        $employees = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->latest()
            ->count();

        $pf_employees_count = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('doj', '<', '2005-01-01')
            ->count();

        $nps_employees_count = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('doj', '>=', '2005-01-01')
            ->count();

        $pensioners_count = PensionNew::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($q) use ($authUser) {
                    $q->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->count();

        $group_wise_employee = Employee::with('ward')->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->selectRaw('COUNT(*) as total_employees, ward_id, COUNT(ward_id) as employees_per_ward')
            ->groupBy('ward_id')
            ->latest('ward_id') // Optional: Sort by ward_id or another column
            ->get();

        $current_month = date('m');
        $retire_emps_count = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->selectRaw('count(*) as ret_count, DATE_FORMAT(retirement_date, "%m") as ret_month, MONTHNAME(retirement_date) as ret_month_name')
            ->groupBy('ret_month', 'ret_month_name')
            ->having('ret_month', '>=', $current_month)
            ->limit($group_wise_employee->count())
            ->get();

        $current_year = Carbon::now()->year;
        $start_month = $current_month - 5; // Last 5 months
        if ($start_month <= 0) {
            $start_month += 12;
            $start_year = $current_year - 1;
        } else {
            $start_year = $current_year;
        }
        $from_date = "$start_year-$start_month-01";
        $to_date = "$current_year-$current_month-01";

        $attendances_presents = Attendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            $query->whereHas('employee', function ($q) use ($authUser) {
                $q->where('ward_id', $authUser->ward_id);
            });
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($q) use ($authUser) {
                    $q->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereHas('employee', function ($q) use ($userDepartment) {
                    $q->whereIn('department_id', $userDepartment);
                });
            })
            ->where('from_date', '>=', $from_date)
            ->where('to_date', '<', $to_date)
            ->whereColumn('total_present_days', 'main_present_days')
            ->selectRaw('MONTHNAME(STR_TO_DATE(CONCAT(month, "-01", "-2025"), "%m-%d-%Y")) AS month_name, count(1) as total_present')
            ->groupBy('month')
            ->get();

        // dd($attendances_presents);
        $attendances_leaves = Attendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            $query->whereHas('employee', function ($q) use ($authUser) {
                $q->where('ward_id', $authUser->ward_id);
            });
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($q) use ($authUser) {
                    $q->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereHas('employee', function ($q) use ($userDepartment) {
                    $q->whereIn('department_id', $userDepartment);
                });
            })
            ->where('from_date', '>=', $from_date)
            ->where('to_date', '<', $to_date)
            ->where('total_leave', '!=', 0)
            ->selectRaw('count(1) as total_leaves')
            ->groupBy('month')
            ->get();

        $attendances_absents = Attendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            $query->whereHas('employee', function ($q) use ($authUser) {
                $q->where('ward_id', $authUser->ward_id);
            });
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($q) use ($authUser) {
                    $q->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereHas('employee', function ($q) use ($userDepartment) {
                    $q->whereIn('department_id', $userDepartment);
                });
            })
            ->where('from_date', '>=', $from_date)
            ->where('to_date', '<', $to_date)
            ->where('total_absent_days', '!=', 0)
            ->selectRaw('count(1) as total_absents')
            ->groupBy('month')
            ->get();

        $bar_chart_months = $presents = $leaves = $absents = '';
        foreach ($attendances_presents as $a) {
            $bar_chart_months .= substr($a->month_name, 0, 3) . ',';
            $presents .= $a->total_present . ',';
        }
        foreach ($attendances_leaves as $a) {
            $leaves .= $a->total_leaves . ',';
        }
        foreach ($attendances_absents as $a) {
            $absents .= $a->total_absents . ',';
        }
        $bar_chart_months = rtrim($bar_chart_months, ",");
        $presents = rtrim($presents, ",");
        $leaves = rtrim($leaves, ",");
        $absents = rtrim($absents, ",");

        $net_salary = FreezeAttendance::when($authUser->hasRole(['Ward HOD']), function ($q) use ($authUser) {
            $q->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($q) use ($authUser) {
                $q->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('from_date', '>=', $from_date)
            ->where('to_date', '<', $to_date)
            ->selectRaw('SUM(net_salary) as salarypaid')
            ->groupBy('month')
            ->get();

        $salary_paid = '';
        foreach ($net_salary as $n) {
            $salary_paid .= $n->salarypaid . ',';
        }
        $salary_paid = rtrim($salary_paid, ",");

        return view('admin.dashboard')->with([
            'employees'       => $employees,
            'emp_statuses'  => $emp_statuses,
            'wards'         => $wards,
            'departments'   => $departments,
            'banks'         => $banks,
            'retire_employees' => $retire_employees,
            'employee_salarys' => $employee_salary,
            'group_wise_employees' => $group_wise_employee,
            'pf_employees_count' => $pf_employees_count,
            'nps_employees_count' => $nps_employees_count,
            'pensioners_count' => $pensioners_count,
            'retire_emps_count' => $retire_emps_count,
            'bar_chart_months' => $bar_chart_months,
            'presents' => $presents,
            'leaves' => $leaves,
            'absents' => $absents,
            'salary_paid' => $salary_paid,
        ]);
    }

    public function changeThemeMode()
    {
        $mode = request()->cookie('theme-mode');

        if ($mode == 'dark')
            Cookie::queue('theme-mode', 'light', 43800);
        else
            Cookie::queue('theme-mode', 'dark', 43800);

        return true;
    }
}
