<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\Ward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Exports\BankStatementExport;
use Maatwebsite\Excel\Facades\Excel;

class BankStatementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $authUser = Auth::user();
            $userDepartment = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');
            $from_date = $request->from_date ?? null;
            $to_date = $request->to_date ?? null;
            $month = $request->month ?? date('m');
            $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
            $ward = $request->ward ?? null;


            // If from date and to date are not provided, calculate current month's from date and to date
            $financial_year = FinancialYear::where('id', session('financial_year'))->first();

            $wards = Ward::when(Auth::user()->hasRole('AMC', 'DMC'), function ($q) use ($userDepartment) {
                whereHas('departments', function ($q) use ($userDepartment) {
                    return $q->whereIn('id', $userDepartment);
                });
            })
                ->when($authUser->hasRole(['Ward HOD']), fn($q) => $q->where('id', $authUser->ward_id))
                ->latest()
                ->get();


            if (!$from_date || !$to_date) {
                if ($financial_year) {
                    if ($month <= 3) {
                        $year = date('Y', strtotime($financial_year->to_date));
                    } else {
                        $year = date('Y', strtotime($financial_year->from_date));
                    }
                    $month = $month ?? 1;

                    $from_date = Carbon::parse($year . '-' . ($month) . '-' . 01);
                    $to_date = clone ($from_date);
                    $from_date = (string) $from_date->startOfMonth()->toDateString();
                    $to_date = (string) $to_date->endOfMonth()->toDateString();
                }
            }

            $bank_statements = FreezeAttendance::query()->with(['employee.bank', 'employee.employee_status']);

            // Filter by ward if selected
            if ($ward) {

                $bank_statements->where('ward_id', $ward);
            }

            $bank_statements = $bank_statements->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                    $q->whereIn('department_id', $userDepartment);
                })

                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                ->get();
            // return $bank_statements;
            return view('admin.reports.bank-statement')->with(['bank_statements' => $bank_statements, 'from_date' => $from_date, 'to_date' => $to_date, 'month' => $month, 'monthName' => $monthName, 'wards' => $wards, 'ward_id' => $ward]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }


    public function bankStatementPdf(Request $request){
        $authUser = Auth::user();

        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;

        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');

        $bankStatements = FreezeAttendance::query()->with(['employee.bank', 'employee.employee_status'])
        // ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
        //     return $query->where('ward_id', $authUser->ward_id);
        // })
        //     ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
        //         return $query->where('department_id', $authUser->department_id);
        //     })
        //     ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
        //         $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
        //     })
        ->when(isset($request->ward) && $request->ward, fn($q) => $q->where('ward_id', $request->ward))

            ->where('freeze_status', 1)
            ->where('from_date', $from_date)
            ->where('to_date', $to_date)
            ->get();

            $pdf = SnappyPdf::loadView('admin.reports.bank-statement-pdf', compact('bankStatements', 'monthName'))
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('footer-right', now()->format('F d, Y h:i A'))
            ->setOption('footer-left', 'Page: [page] of [toPage]')
            ->setOption('margin-bottom', 8)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 5)
            ->setOption('margin-right', 5);
            $filename = "Bank Statement";
            return $pdf->inline($filename);
    }

    public function bankStatementExcel(Request $request){
        $authUser = Auth::user();

        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;

        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');

        $bankStatements = FreezeAttendance::query()->with(['employee.bank', 'employee.employee_status'])
        // ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
        //     return $query->where('ward_id', $authUser->ward_id);
        // })
        //     ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
        //         return $query->where('department_id', $authUser->department_id);
        //     })
        //     ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
        //         $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
        //     })

        ->when(isset($request->ward) && $request->ward, fn($q) => $q->where('ward_id', $request->ward))

            ->where('freeze_status', 1)
            ->where('from_date', $from_date)
            ->where('to_date', $to_date)
            ->get();

        return Excel::download(new BankStatementExport($bankStatements, $monthName), 'bank-statement.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
