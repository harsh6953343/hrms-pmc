<?php

namespace App\Http\Controllers\Admin\Report;

use App\Exports\PaySheetExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\PaySheetExcelRequest;
use App\Models\Department;
use App\Models\FreezeAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\UserDepartment;
use App\Models\Ward;

class PaySheetExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        // $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
        //     return $query->where('ward_id', $authUser->ward_id);
        // })
        //     ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
        //         $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
        //     })
        //     ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
        //         return $query->where('id', $authUser->department_id);
        //     })->latest()->get();

        $wards = Ward::latest()->get();

        $userDepartment = [];
        if ($authUser->hasRole(['AMC', 'DMC'])) {
            $userDepartment = UserDepartment::where('user_id', $authUser->id)->pluck('department_id');
        }
        $employee_salaries = FreezeAttendance::with('employee')->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('net_salary', '<', 0)
            ->where('basic_salary', '!=', 0)
            ->get();

        return view('admin.reports.pay-sheet-excel')->with(['wards' => $wards, 'employee_salaries' => $employee_salaries, 'authUser' => $authUser]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaySheetExcelRequest $request)
    {
        $input = $request->validated();
        $department = $request->department_id;
        $ward = $request->ward_id;

        $freezeAttendances = FreezeAttendance::with('employee.employee_status', 'designation')
            ->when($department != '', function ($query) use ($department) {
                return $query->where('department_id', $department);
            })
            ->when($ward != '', function ($query) use ($ward) {
                return $query->where('ward_id', $ward);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->where('from_date', $input['from_date'])
            ->where('to_date', $input['to_date'])
            ->where('month', $input['month'])
            ->where('freeze_status', 1)
            ->orderBy('clas_id', 'ASC')
            ->get();

        return Excel::download(new PaySheetExport($freezeAttendances), 'pay-sheet.xlsx');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
