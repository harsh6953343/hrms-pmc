<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\UserDepartment;
use Illuminate\Support\Facades\Auth;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Models\SupplimentaryBill;
use App\Models\EmployeeSalary;
use App\Models\FreezeAttendance;
use App\Models\SuplimentaryLeaveBill;
use App\Models\SuplimentaryEmployeeLeaveBill;
use App\Exports\SupplimentaryExport;
use App\Exports\SupplimentaryBankStatementExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Models\Ward;

class SuplimentryReportController extends Controller
{
    public function index(Request $request)
    {
        $suplimentaryLeaveBills = SuplimentaryLeaveBill::where('is_generate', 1)->select('bill_no', 'id')->latest()->get();

        if (isset($request->bill_no) && $request->bill_no != "") {
            $suplimentries = SuplimentaryEmployeeLeaveBill::with(['employee.bank', 'suplimentryLeaveBill.supplimentaryBill.ward'])
                ->where('suplimentry_leave_id', $request->bill_no)
                ->orderBy('emp_code', 'asc')
                ->get();
            // return $suplimentries;
            $empCodes = $suplimentries->pluck('emp_code')->toArray();

            $employeeSalary = EmployeeSalary::with(['employee_allowances', 'employee_deductions', 'employee'])
                ->whereIn('Emp_Code', $empCodes)
                ->get();

            $filename = "Supplimentary Slip.pdf";
            $pdf = SnappyPdf::loadView('admin.reports.pdf.suplimentry-pdf', compact('suplimentries', 'employeeSalary'))
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('footer-right', now()->format('F d, Y h:i A'))
                ->setOption('footer-left', 'Page: [page] of [toPage]')
                ->setOption('margin-bottom', 8)
                ->setOption('margin-top', 5)
                ->setOption('margin-left', 5)
                ->setOption('margin-right', 5);

            return $pdf->inline($filename);
        }

        return view('admin.reports.suplimentry')->with([
            'suplimentaryLeaveBills' => $suplimentaryLeaveBills
        ]);
    }

    public function excel(Request $request)
    {
        $suplimentries = SuplimentaryEmployeeLeaveBill::with(['employee.bank', 'suplimentryLeaveBill'])
            ->where('suplimentry_leave_id', $request->bill_no)
            ->orderBy('emp_code', 'asc')
            ->get();

        $empCodes = $suplimentries->pluck('emp_code')->toArray();

        $employeeSalary = EmployeeSalary::with(['employee_allowances', 'employee_deductions', 'employee'])
            ->whereIn('Emp_Code', $empCodes)
            ->get();

        return Excel::download(new SupplimentaryExport($suplimentries, $employeeSalary), 'suplimentary.xlsx');
    }

    public function bankStatement(Request $request){
        try {
            $suplimentryLeaveBills = SuplimentaryLeaveBill::where('is_generate', 1)->select('id', 'bill_no')->latest()->get();

            $bankStatements = [];
            if(isset($request->bill_no) && $request->bill_no != ""){
                $bankStatements = SuplimentaryEmployeeLeaveBill::query()
                    ->with(['employee'])
                    ->where('suplimentry_leave_id', $request->bill_no)
                    ->selectRaw('employee_id, emp_code, SUM(ROUND(net_salary)) as total_salary')
                    ->groupBy('employee_id', 'emp_code')
                    ->get();
            }

            return view('admin.reports.suplimentry.bank-statement')->with(['bankStatements' => $bankStatements, 'suplimentryLeaveBills' => $suplimentryLeaveBills]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }

    public function bankStatementPdf(Request $request){
        $bankStatements = [];
        if(isset($request->bill_no) && $request->bill_no != ""){
            $bankStatements = SuplimentaryEmployeeLeaveBill::query()
                ->with(['employee'])
                ->where('suplimentry_leave_id', $request->bill_no)
                ->selectRaw('employee_id, emp_code, SUM(ROUND(net_salary)) as total_salary')
                ->groupBy('employee_id', 'emp_code')
                ->get();
        }

        $pdf = SnappyPdf::loadView('admin.reports.suplimentry.bank-statement-pdf', compact('bankStatements'))
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('footer-right', now()->format('F d, Y h:i A'))
                ->setOption('footer-left', 'Page: [page] of [toPage]')
                ->setOption('margin-bottom', 8)
                ->setOption('margin-top', 5)
                ->setOption('margin-left', 5)
                ->setOption('margin-right', 5);
        $filename = "Suplimentry Bank Statement";
        return $pdf->inline($filename);
    }

    public function bankStatementExcel(Request $request){
        $bankStatements = [];
        if(isset($request->bill_no) && $request->bill_no != ""){
            $bankStatements = SuplimentaryEmployeeLeaveBill::query()
                ->with(['employee'])
                ->where('suplimentry_leave_id', $request->bill_no)
                ->selectRaw('employee_id, emp_code, SUM(ROUND(net_salary)) as total_salary')
                ->groupBy('employee_id', 'emp_code')
                ->get();
        }

        return Excel::download(new SupplimentaryBankStatementExport($bankStatements), 'suplimentary-bank-statement.xlsx');
    }
}
