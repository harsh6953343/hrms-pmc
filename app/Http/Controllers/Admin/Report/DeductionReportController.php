<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Deduction;
use App\Models\Department;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use App\Models\Ward;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Models\FreezePensionNew;

class DeductionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();
        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;
        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
        $department = $request->department ?? null;
        $ward = $request->ward_id ?? null;
        $deduction_id = $request->deduction ?? null;

        $departments = [];

        if ($ward && $ward != "") {
            $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                })
                ->where('ward_id', $ward)
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('id', $authUser->department_id);
                })->latest()->get();
        }


        $deductions = Deduction::latest()->get();

        $financial_year = FinancialYear::where('id', session('financial_year'))->first();

        if (!$from_date || !$to_date) {
            if ($financial_year) {
                if ($month <= 3) {
                    $year = date('Y', strtotime($financial_year->to_date));
                } else {
                    $year = date('Y', strtotime($financial_year->from_date));
                }
                $month = $month ?? 1;

                $from_date = Carbon::parse($year . '-' . ($month) . '-' . 01);
                $to_date = clone ($from_date);
                $from_date = (string) $from_date->startOfMonth()->toDateString();
                $to_date = (string) $to_date->endOfMonth()->toDateString();
            }
        }


        $deduction_report = FreezeAttendance::query();

        // Filter by department if selected
        if ($department) {
            $deduction_report->where('department_id', $department);
        }

        // Filter by allowance ID if provided
        if ($deduction_id) {

            // Apply user role-based filters
            $deduction_report = $deduction_report->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                })
                ->when($ward != "", function ($q) use ($ward) {
                    $q->where('ward_id', $ward);
                })
                ->when($department != "", function ($q) use ($department) {
                    $q->where('department_id', $department);
                })
                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                // ->whereRaw("FIND_IN_SET($deduction_id, deduction_Id) > 0")
                ->with('employee')
                ->get();

            // return $deduction_report;
        }

        $wards = Ward::latest()->get();

        return view('admin.reports.deduction-report')->with([
            'wards'   => $wards,
            'from_date'     => $from_date,
            'to_date'       => $to_date,
            'month'         => $month,
            'monthName'     => $monthName,
            'department_id' => $department,
            'deductions'    => $deductions,
            'deduction_id'  => $deduction_id,
            'deduction_reports' => $deduction_report,
            'departments' => $departments
        ]);
    }

    public function deductionReportPdf(Request $request)
    {
        $authUser = Auth::user();
        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;
        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
        $department = $request->department ?? null;
        $ward = $request->ward_id ?? null;
        $deduction_id = $request->deduction ?? null;

        if ($request->deduction == "1") {
        }

        // return view('admin.reports.deduction-report.pdf.patsanstha');
        $deduction_report = FreezeAttendance::query();

        // Filter by department if selected
        if ($department) {
            $deduction_report->where('department_id', $department);
        }

        if ($deduction_id == 4) {
            $deduction_reports = FreezePensionNew::when($ward && $ward != "", function ($query) use ($ward) {
                return $query->where('ward_id', $ward);
            })->withWhereHas('employee', function ($q) use ($department) {
                $q->when($department && $department != "", function ($q) use ($department) {
                    $q->where('department_id', $department);
                });
            })->when($month && $month != "", function ($q) use ($month) {
                $q->where('month', $month);
            })->when($from_date && $from_date != "", function ($q) use ($from_date) {
                $q->where('from_date', $from_date);
            })->when($to_date && $to_date != "", function ($q) use ($to_date) {
                $q->where('to_date', $to_date);
            })->get();
        } else {

            $deduction_reports = $deduction_report->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                })
                ->when($ward != "", function ($q) use ($ward) {
                    $q->where('ward_id', $ward);
                })
                ->when($department != "", function ($q) use ($department) {
                    $q->where('department_id', $department);
                })
                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                // ->whereRaw("FIND_IN_SET($deduction_id, deduction_Id) > 0")
                ->with('employee.bank', 'designation')
                ->get();
        }

        // return $deduction_reports;
        $viewArray = ['1' => 'professional-tax', '2' => 'income-tax', '3' => 'pf-contribution', '4' => 'pension-contribution', '5' => 'general-provident-fund', '6' => 'general-provident-fund-advance', '7' => 'dcps-i-employee-contribution', '8' => 'dcps-i-corporation-contribution-deduction', '9' => 'festival-advance', '10' => 'employee-credit-society', '11' => 'lic', '12' => 'salary-recovery', '13' => 'lic', '14' => 'bank-recovery', '15' => 'bank-recovery', '16' => 'service-charge', '17' => 'accidental-insurance-premium', '18' => 'maharashtra-labour-welfare-board-employee-deduction', '19' => 'maharashtra-labour-welfare-board-corporation-deduction', '20' => 'home-loan', '21' => 'group-insurance-scheme', '22' => 'revenue-stamp', '23' => 'income-tax-remuneration', '24' => 'patsanstha', '25' => 'salary-increment-atjustment'];

        // return view('admin.reports.deduction-report.pdf.' . $viewArray[$deduction_id], compact('deduction_reports', 'deduction_id', 'month', 'to_date'));
        $pdf = SnappyPdf::loadView('admin.reports.deduction-report.pdf.' . $viewArray[$deduction_id], compact('deduction_reports', 'deduction_id', 'month', 'to_date'))
            ->setPaper('a4');
        if (!in_array($deduction_id, [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 20, 17, 18, 19, 21, 22, 23, 24, 25])) {
            $pdf = $pdf->setOrientation('landscape');
        }
        $pdf = $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->setOption('footer-right', now()->format('d-m-Y h:i:s'))
            ->setOption('footer-left', 'Page: [page] of [toPage]')
            ->setOption('footer-font-size', 8);

        return $pdf->inline("Deduction report.pdf");
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
