<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\PaySheetExcelRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Department;
use App\Models\FreezeAttendance;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Exports\PaySheetExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\UserDepartment;
use App\Models\Ward;

class PaySheetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        // $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
        //     return $query->where('ward_id', $authUser->ward_id);
        // })
        //     ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
        //         return $query->where('id', $authUser->department_id);
        //     })->when($authUser->hasRole(['AMC', 'DMC']), function ($query) use ($authUser) {
        //         return $query->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
        //     })->latest()->get();

        $wards = Ward::latest()->get();

        $userDepartment = [];
        if ($authUser->hasRole(['AMC', 'DMC'])) {
            $userDepartment = UserDepartment::where('user_id', $authUser->id)->pluck('department_id');
        }
        $employee_salaries = FreezeAttendance::with('employee')->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->where('net_salary', '<', 0)
            ->where('basic_salary', '!=', 0)
            ->get();

        return view('admin.reports.pay-sheet')->with(['wards' => $wards, 'employee_salaries' => $employee_salaries]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaySheetExcelRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            return Excel::download(new PaySheetExport, 'pay_sheet.xlsx');

            // Allowance::create(Arr::only($input, Allowance::getFillables()));
            // DB::commit();

            return response()->json(['success' => 'Excel Export created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Excel Export');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showPaySheetPDF(PaySheetExcelRequest $request)
    {
        $authUser = Auth::user();
        $department = $request->department_id;
        $ward = $request->ward_id;
        $month = $request->month;
        $data['from_date'] = $request->from_date;
        $data['to_date'] = $request->to_date;

        $fromDate = Carbon::parse($data['from_date']);
        $toDate = Carbon::parse($data['to_date']);

        // Calculate the difference in days
        $data['total_days'] = $fromDate->diffInDays($toDate) + 1;

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();

        $data['freezeAttendances'] = FreezeAttendance::with('employee.employee_status', 'designation')
            ->when($department != '', function ($query) use ($department) {
                return $query->where('department_id', $department);
            })
            ->when($ward != '', function ($query) use ($ward) {
                return $query->where('ward_id', $ward);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']) && $department == "", function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->where('from_date', $data['from_date'])
            ->where('to_date', $data['to_date'])
            ->where('month', $month)
            ->where('freeze_status', 1)
            // ->where('employee_id', 1)
            ->orderBy('clas_id', 'ASC')
            ->get();

        if ($department == '') {
            $data['department_name'] = 'all';
        } else {
            $department_name = Department::where('id', $department)->first();
            $data['department_name'] = $department_name->name;
        }

        $filename = "Pay Sheet " . $data['department_name'] . " Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.pay-sheet-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 5)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->setOption('footer-center', '[page] of [toPage]') // Page number format
            ->setOption('footer-font-size', 8);

        return $pdf->inline($filename);
    }
}
