<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\YearlyPaySheetRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\FinancialYear;
use App\Models\Employee;
use App\Models\FreezeAttendance;
use App\Models\SuplimentaryEmployeeLeaveBill;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class YearlyPaySheetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $employees = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->employee_id);
            })->latest()->get();

        $finYears = FinancialYear::all()->pluck('title');

        return view('admin.reports.yearly-pay-sheet')->with(['employees' => $employees, 'finYears' => $finYears]);
    }

    public function showPaySheetPDF($employee_id, $financial_year_range)
    {
        if ($employee_id == '') {
            echo 'Please check URL. Employee ID not present.';
            die;
        } elseif ($financial_year_range == '') {
            echo 'Please check URL. Financial Year not present.';
            die;
        }
        $employee = Employee::where('id', $employee_id)->first();

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();

        // Extract the start year from the current fiscal year string
        $startYear = intval(substr($financial_year_range, 2, 4));
        // Calculate the previous fiscal year range
        $previousEndYear = $startYear;
        $endYear = $startYear + 1;

        if ($financial_year_range == 'FY2024-2025') {
            $currentYearRec = FreezeAttendance::with('employee', 'designation', 'department')
                ->where('from_date', '>=', "$previousEndYear-05-01")
                ->where('to_date', '<', "$endYear-02-30")
                ->where('freeze_status', 1)
                ->where('employee_id', $employee_id)
                ->get();

            $dayCountOfMonths = [13 => 31, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];

            $lastRec = $currentYearRec->last();
            if ($lastRec) {
                if ($lastRec->month < 3) {
                    $assumptionRecordCount = 2 - $lastRec->month;
                } else {
                    $assumptionRecordCount = 14 - $lastRec->month;
                }
                $lastMonth = $lastRec->month;
                for ($i = 0; $i < $assumptionRecordCount; $i++) {
                    $newRec = $lastRec->replicate();
                    $lastMonth = $newRec->month = $lastMonth + 1;
                    if ($lastMonth == 14) {
                        if ($endYear % 4 == 0) {
                            $newRec->present_day = 29;
                        } else {
                            $newRec->present_day = 28;
                        }
                    } else {
                        if ($lastMonth == 2) {
                            if ($endYear % 4 == 0) {
                                $newRec->present_day = 29;
                            } else {
                                $newRec->present_day = 28;
                            }
                        } else {
                            $newRec->present_day = $dayCountOfMonths[$lastMonth];
                        }
                    }
                    $currentYearRec->push($newRec);
                }
            } else {
                echo "<b>Data not present.</b>";
                die;
            }

            $dcps_emp = $dcps_empr = $gpf = $income_tax = $total_lic_deduction = 0;
            $supplementary_bill = $dcps_emp_supp = $dcps_empr_supp = $gpf_supp = 0;
            $supp_bill_csv = DB::table('csv_imported_supplementary')
                ->where('employeeid', $employee->employee_id)
                ->selectRaw('SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
                ->groupBy('employeeid')
                ->get();

            if ($supp_bill_csv->count()) {
                $supplementary_bill += $supp_bill_csv[0]->suppbill;
                $total_lic_deduction += $supp_bill_csv[0]->lic;
                $dcps_emp_supp += $supp_bill_csv[0]->dcpsemployee;
                $dcps_empr_supp += $supp_bill_csv[0]->dcpsemployer;
                $gpf_supp += $supp_bill_csv[0]->gpf;
            }
            $data['supplementary_bill'] = $supplementary_bill;
            $data['dcps_emp_supplementary'] = $dcps_emp_supp;
            $data['dcps_empr_supplementary'] = $dcps_empr_supp;
            $data['gpf_supplementary'] = $gpf_supp;

            //Process excel imported records
            $deductionsFromExcel = DB::table('csv_imported_salary')
                                    ->select('Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX', 'DEARNESS_ALLOWANCE', 'HOUSERENT_ALLOWANCE', 'CITY_COMPENSATORY_ALLOWANCE', 'DEPUTATION_ALLOWANCE', 'GROUP_INSURANCE_SCHEME', 'Actual Basic as Actual_Basic')
                                    ->where('Emp_Code', $employee->employee_id)
                                    ->get();
            $actual_basic_salary_mar_april_may = $gross_salary_mar_april = $hra = $dearness_aw = $city_c_aw = $depu_aw = $gr_in_scheme = 0;
            foreach ($deductionsFromExcel as $ded) {

                $actual_basic_salary_mar_april_may = $ded->Actual_Basic;
                $gross_salary_mar_april += $ded->Total_Earning;
                $total_lic_deduction += $ded->LIC;
                $dcps_emp += $ded->DCPS_I_EMPLOYEE_CONTRIBUTION;
                $dcps_empr += $ded->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION;
                $gpf += $ded->GENERAL_PROVIDENT_FUND;
                $income_tax += $ded->INCOME_TAX;

                $dearness_aw += $ded->DEARNESS_ALLOWANCE;
                $hra += $ded->HOUSERENT_ALLOWANCE;
                $city_c_aw += $ded->CITY_COMPENSATORY_ALLOWANCE;
                $depu_aw += $ded->DEPUTATION_ALLOWANCE;
                $gr_in_scheme += $ded->GROUP_INSURANCE_SCHEME;
            }

            //Dearness allowance
            $da_csv = DB::table('csv_imported_da')
                ->where('EmployeeId', $employee->employee_id)
                ->selectRaw('SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
                ->groupBy('EmployeeId')
                ->get();
            $da_gross = 0;
            if ($da_csv->count()) {
                $da_gross = $da_csv[0]->da;
                $dcps_emp += $da_csv[0]->dcpsemployee;
                $dcps_empr += $da_csv[0]->dcpsemployer;
            }

            //7th pay difference
            $seventh_pay_diff_csv = DB::table('csv_imported_7th_difference')
                ->where('Employee ID', $employee->employee_id)
                ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
                ->groupBy('Employee ID')
                ->get();
            $seventh_pay_diff_gross = 0;
            if ($seventh_pay_diff_csv->count()) {
                $seventh_pay_diff_gross = $seventh_pay_diff_csv[0]->fifth_installment;
            }

            $data['seventh_pay_fifth_installment'] = $seventh_pay_diff_gross;
            //====================================================================

            $month = [5, 4, 3];
            $supp_bill_s = round($supplementary_bill / 3);
            $total_lic_deduction_s = round($total_lic_deduction / 3);
            $dcps_emp_s = round($dcps_emp / 3);
            $dcps_empr_s = round($dcps_empr / 3);
            $gpf_s = round($gpf / 3);
            $gross_salary_mar_april_s = round($gross_salary_mar_april / 3);
            $actual_basic_salary_mar_april_may_s = round($actual_basic_salary_mar_april_may / 3);
            $income_tax_s = round($income_tax / 3);
            $da_gross_s = round($da_gross / 3);

            $dearness_aw_s = round($dearness_aw / 3);
            $hra_s = round($hra / 3);
            $city_c_aw_s = round($city_c_aw / 3);
            $depu_aw_s = round($depu_aw / 3);
            $gr_in_scheme_s = round($gr_in_scheme / 3);

            $data_values = array('supp_bill' => $supp_bill_s, 'total_lic_deduction' => $total_lic_deduction_s, 'dcps_emp' => $dcps_emp_s, 'dcps_empr' => $dcps_empr_s, 'gpf' => $gpf_s, 'gross_salary_mar_april' => $gross_salary_mar_april_s, 'income_tax' => $income_tax_s, 'da_gross' => $da_gross_s, 'dearness_aw' => $dearness_aw_s, 'hra' => $hra_s, 'city_c_aw' => $city_c_aw_s, 'depu_aw' => $depu_aw_s, 'gr_in_scheme' => $gr_in_scheme_s, 'actual_basic_salary_mar_april_may' => $actual_basic_salary_mar_april_may_s);

            foreach ($month as $m) {

                $newRec = $this->getYearlyPaysheetRow($m, $data_values, $employee);
                $currentYearRec->prepend($newRec);
            }

            $data['freezeAttendances'] = $currentYearRec;

            $data['ward_name'] = $employee->ward->name;
            $data['department_name'] = $employee->department->name;
            $data['designation_name'] = $employee->designation->name;
            $data['yearRange'] = "(Mar $previousEndYear - Feb $endYear)";
            $filename = "Yearly Salary Slip" . " " . $employee->employee_id . '.pdf';

            $pdf = SnappyPdf::loadView('admin.reports.pdf.yearly-pay-sheet-pdf', $data)
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 3)
                ->setOption('margin-left', 0)
                ->setOption('margin-right', 0);

            return $pdf->inline($filename);
        }
    }

    public function generateYearlyPaysheetPdf($employee_id, $financial_year_range)
    {
        // $employee_id is the employee code
        if ($employee_id == '')
            return 'Please check URL. Employee ID not present.';
        if ($financial_year_range == '')
            return 'Please check URL. Financial Year not present.';

        $employee = Employee::where('id', $employee_id)->first();
        $corporation = Corporation::first();
        $logoPath = public_path($corporation->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $startYear = intval(substr($financial_year_range, 2, 4));
        $previousYear = $startYear;
        $endYear = $startYear + 1;



        $salaries = FreezeAttendance::with('employee', 'designation', 'department')
                                        ->where('from_date', '>=', ($endYear == '2025') ? $previousYear."-06-01" : $previousYear."-03-01")
                                        ->where('to_date', '<=', $endYear . "-02-29")
                                        ->where('freeze_status', 1)
                                        ->where('employee_id', $employee_id)
                                        ->get();

        $supplementary = SuplimentaryEmployeeLeaveBill::query()
                                    ->where('month', '>=', ($endYear == '2025') ? $previousYear."-06" : $previousYear."-04")
                                    ->where('month', '<=', $endYear . "-03")
                                    ->where('employee_id', $employee_id)
                                    ->get();

        $tillMaySupplementary = ($endYear == '2025') ? DB::table('csv_imported_supplementary')
                                    ->where('employeeid', $employee->employee_id)
                                    ->selectRaw('Period, SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
                                    // ->groupBy('employeeid')
                                    ->groupBy('Period')
                                    ->get() : null;

        $tillMaySalary = ($endYear == '2025') ? DB::table('csv_imported_salary')
                                    ->select('month', 'Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX', 'DEARNESS_ALLOWANCE', 'HOUSERENT_ALLOWANCE', 'CITY_COMPENSATORY_ALLOWANCE', 'DEPUTATION_ALLOWANCE', 'GROUP_INSURANCE_SCHEME', 'Actual Basic as Actual_Basic', 'EARNED BASIC as Earned_Basic', 'LEAVEPAY', 'DCPS I CORPORATION CONTRIBUTION ALLOWANCE as DCPSCorpAllw')
                                    ->where('Emp_Code', $employee->employee_id)
                                    ->where('year', '2024')
                                    ->whereIn('month', [3,4,5])
                                    // ->groupBy('month')
                                    ->get() : null;

        $tillMayDa = ($endYear == '2025') ? DB::table('csv_imported_da')
                                ->selectRaw('Period, SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
                                ->where('EmployeeId', $employee->employee_id)
                                ->whereRaw("STR_TO_DATE(CONCAT('01-', `Period`), '%d-%b-%y') > STR_TO_DATE('01-Mar-24', '%d-%b-%y')")
                                ->whereRaw("STR_TO_DATE(CONCAT('01-', `Period`), '%d-%b-%y') < STR_TO_DATE('01-Jun-24', '%d-%b-%y')")
                                ->groupBy('Period')
                                ->get() : null;

        $tillMay7thDiff = ($endYear == '2025') ? DB::table('csv_imported_7th_difference')
                            ->where('Employee ID', $employee->employee_id)
                            ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
                            ->groupBy('Employee ID')
                            ->get() : null;



        $data['base64Logo'] = $base64Logo;
        $data['employee'] = $employee;
        $data['startYear'] = $startYear;
        $data['endYear'] = $endYear;
        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();
        $data['corporation'] = $corporation;
        $data['salaries'] = $salaries;
        $data['tillMaySupplementary'] = $tillMaySupplementary;
        $data['supplementary'] = $supplementary;
        $data['tillMaySalary'] = $tillMaySalary;
        $data['tillMayDa'] = $tillMayDa;
        $data['tillMay7thDiff'] = $tillMay7thDiff;
        $filename = "Yearly Salary Slip" . " " . $employee->employee_id . '.pdf';

        $pdf = SnappyPdf::loadView('admin.reports.pdf.yearly-pay-sheet-marathi-pdf', $data)
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 3)
                ->setOption('margin-left', 0)
                ->setOption('margin-right', 0);

            return $pdf->inline($filename);
    }





    public function showPaySheetMarathiPDFOld($employee_id, $financial_year_range)
    {
        // if ($employee_id == '') {
        //     echo 'Please check URL. Employee ID not present.';
        //     die;
        // } elseif ($financial_year_range == '') {
        //     echo 'Please check URL. Financial Year not present.';
        //     die;
        // }
        // $employee = Employee::where('id', $employee_id)->first();

        // $data['corporation'] = Corporation::first();

        // $logoPath = public_path($data['corporation']->logo);

        // if (file_exists($logoPath)) {
        //     $logoData = file_get_contents($logoPath);
        //     $base64Logo = base64_encode($logoData);
        // } else {
        //     $base64Logo = null;
        // }

        // $data['base64Logo'] = $base64Logo;

        // $data['allowances'] = Allowance::get();
        // $data['deductions'] = Deduction::get();


        // $startYear = intval(substr($financial_year_range, 2, 4));
        // $previousEndYear = $startYear;
        // $endYear = $startYear + 1;

        // // if ($financial_year_range == 'FY2024-2025') {
        //     $currentYearRec = FreezeAttendance::with('employee', 'designation', 'department')
        //         ->where('from_date', '>=', $previousEndYear . "-05-01")
        //         ->where('to_date', '<', $endYear . "-02-30")
        //         ->where('freeze_status', 1)
        //         ->where('employee_id', $employee_id)
        //         ->get();

        //     $lastRec = $currentYearRec->last();

        //     if(!$lastRec)
        //         return "<b>Data not present.</b>";

        //     if($lastRec)
        //     {
        //         if ($lastRec->month < 3) {
        //             $assumptionRecordCount = 2 - $lastRec->month;
        //         } else {
        //             $assumptionRecordCount = 14 - $lastRec->month;
        //         }
        //         $lastMonth = $lastRec->month;
        //         for ($i = 0; $i < $assumptionRecordCount; $i++) {
        //             $newRec = $lastRec->replicate();
        //             $lastMonth = $newRec->month = $lastMonth + 1;
        //             if ($lastMonth == 14) {
        //                 if ($endYear % 4 == 0) {
        //                     $newRec->present_day = 29;
        //                 } else {
        //                     $newRec->present_day = 28;
        //                 }
        //             } else {
        //                 if ($lastMonth == 2) {
        //                     if ($endYear % 4 == 0) {
        //                         $newRec->present_day = 29;
        //                     } else {
        //                         $newRec->present_day = 28;
        //                     }
        //                 } else {
        //                     $newRec->present_day = $dayCountOfMonths[$lastMonth];
        //                 }
        //             }
        //             $currentYearRec->push($newRec);
        //         }
        //     }

        //     $marathi_name = $currentYearRec[0]->employee->fullname;

        //     $dayCountOfMonths = [13 => 31, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];

        //     $dcps_emp = $dcps_empr = $gpf = $income_tax = $total_lic_deduction = 0;
        //     $supplementary_bill = $dcps_emp_supp = $dcps_empr_supp = $gpf_supp = 0;
        //     $supp_bill_csv = DB::table('csv_imported_supplementary')
        //         ->where('employeeid', $employee->employee_id)
        //         ->selectRaw('SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
        //         ->groupBy('employeeid')
        //         ->get();

            // if ($supp_bill_csv->count()) {
            //     $supplementary_bill += $supp_bill_csv[0]->suppbill;
            //     $total_lic_deduction += $supp_bill_csv[0]->lic;
            //     $dcps_emp_supp += $supp_bill_csv[0]->dcpsemployee;
            //     $dcps_empr_supp += $supp_bill_csv[0]->dcpsemployer;
            //     $gpf_supp += $supp_bill_csv[0]->gpf;
            // }
            // $data['supplementary_bill'] = $supplementary_bill;
            // $data['till_may_supplementary_bill'] = $supplementary_bill;
            // $data['dcps_emp_supplementary'] = $dcps_emp_supp;
            // $data['till_may_dcps_emp_supplementary'] = $dcps_emp_supp;
            // $data['dcps_empr_supplementary'] = $dcps_empr_supp;
            // $data['till_may_dcps_empr_supplementary'] = $dcps_empr_supp;
            // $data['gpf_supplementary'] = $gpf_supp;
            // $data['till_may_gpf_supplementary'] = $gpf_supp;
            // $data['supplementary_total_deduction'] = $dcps_emp_supp + $dcps_empr_supp + $gpf_supp;
            // $data['till_may_supplementary_total_deduction'] = $dcps_emp_supp + $dcps_empr_supp + $gpf_supp;

            //Supplementary from May month
            // $data['dearness_allowance_supplementary'] = $data['hra_supplementary'] = $data['city_allowance_supplementary'] = $data['deputation_allowance_supplementary'] = $data['dcps_corp_allowance_supplementary'] = $data['professional_tax_supplementary'] = $data['income_tax_supplementary'] = $data['gis_ded_supplementary'] = $data['supplementary_total_allowances'] = $data['dcps_corp_ded_supplementary'] = $data['stamp_duty_supplementary'] = 0;
            // $supplementary_allowances = array();
            // $supplementary_deductions = array();

            // $supp_bill_from_may = SuplimentaryEmployeeLeaveBill::query()
            //     ->where('month', '>=', $previousEndYear . "-04")
            //     ->where('month', '<=', $endYear . "-03")
            //     ->where('employee_id', $employee_id)
            //     ->get();

            // zafeer
            // $data['supp_bill_from_may'] = $supp_bill_from_may;
            // // dd($supp_bill_from_may);

            // foreach ($supp_bill_from_may as $supp) {

            //     $data['supplementary_bill'] += $supp->basic_salary;
            //     $exploded_allowance_ids = explode(',', $supp->allowance_id);
            //     $exploded_allowance_amount = explode(',', $supp->allowance_amount);

            //     foreach ($exploded_allowance_ids as $key => $aw_id) {
            //         if (in_array($aw_id, array(1, 2, 4, 6, 13))) {
            //             if ($aw_id == 1) {
            //                 $data['dearness_allowance_supplementary'] += $exploded_allowance_amount[$key];
            //             } elseif ($aw_id == 2) {
            //                 $data['hra_supplementary'] += $exploded_allowance_amount[$key];
            //             } elseif ($aw_id == 4) {
            //                 $data['city_allowance_supplementary'] += $exploded_allowance_amount[$key];
            //             } elseif ($aw_id == 13) {
            //                 $data['deputation_allowance_supplementary'] += $exploded_allowance_amount[$key];
            //             } elseif ($aw_id == 6) {
            //                 $data['dcps_corp_allowance_supplementary'] += $exploded_allowance_amount[$key];
            //             }
            //             $data['supplementary_total_allowances'] += (float)$exploded_allowance_amount[$key];
            //         }
            //     }

            //     $exploded_deduction_ids = explode(',', $supp->deduction_id);
            //     $exploded_deduction_amount = explode(',', $supp->deduction_amount);
            //     foreach ($exploded_deduction_ids as $key => $ded_id) {
            //         if (in_array($ded_id, array(5, 7, 8, 1, 2, 21, 22))) {
            //             if ($ded_id == 5) {
            //                 $data['gpf_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 7) {
            //                 $data['dcps_emp_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 8) {
            //                 $data['dcps_empr_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 1) {
            //                 $data['professional_tax_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 2) {
            //                 $data['income_tax_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 21) {
            //                 $data['gis_ded_supplementary'] += $exploded_deduction_amount[$key];
            //             } elseif ($ded_id == 22) {
            //                 $data['stamp_duty_supplementary'] += $exploded_deduction_amount[$key];
            //             }
            //             $data['supplementary_total_deduction'] += (float)$exploded_deduction_amount[$key];
            //         }
            //     }
            // }
            // $data['dcps_emp_supplementary'] = round($data['dcps_emp_supplementary']);
            // $data['dcps_empr_supplementary'] = round($data['dcps_empr_supplementary']);
            // $data['dcps_corp_allowance_supplementary'] = round($data['dcps_corp_allowance_supplementary']);
            // $data['supplementary_total_allowances'] = round($data['supplementary_total_allowances']);
            // $data['supplementary_total_deduction'] = round($data['supplementary_total_deduction']);
            // $data['supplementary_net_salary'] = round($data['supplementary_bill'] + $data['supplementary_total_allowances'] - $data['supplementary_total_deduction']);

            // $supplementary_allowances[1] = $data['dearness_allowance_supplementary'];
            // $supplementary_allowances[2] = $data['hra_supplementary'];
            // $supplementary_allowances[4] = $data['city_allowance_supplementary'];
            // $supplementary_allowances[6] = $data['dcps_corp_allowance_supplementary'];
            // $supplementary_allowances[13] = $data['deputation_allowance_supplementary'];
            // $data['supplementary_allowances'] = $supplementary_allowances;

            // $supplementary_deductions[5] = $data['gpf_supplementary'];
            // $supplementary_deductions[7] = $data['dcps_emp_supplementary'];
            // $supplementary_deductions[8] = $data['dcps_empr_supplementary'];
            // $supplementary_deductions[1] = $data['professional_tax_supplementary'];
            // $supplementary_deductions[2] = $data['income_tax_supplementary'];
            // $supplementary_deductions[21] = $data['gis_ded_supplementary'];
            // $supplementary_deductions[22] = $data['stamp_duty_supplementary'];
            // $data['supplementary_deductions'] = $supplementary_deductions;

            // //Process excel imported records
            // $deductionsFromExcel = DB::table('csv_imported_salary')
            //     ->select('Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX', 'DEARNESS_ALLOWANCE', 'HOUSERENT_ALLOWANCE', 'CITY_COMPENSATORY_ALLOWANCE', 'DEPUTATION_ALLOWANCE', 'GROUP_INSURANCE_SCHEME', 'Actual Basic as Actual_Basic', 'EARNED BASIC as Earned_Basic', 'LEAVEPAY', 'DCPS I CORPORATION CONTRIBUTION ALLOWANCE as DCPSCorpAllw')
            //     ->where('Emp_Code', $employee->employee_id)
            //     ->get();
            // $actual_basic_salary_mar_april_may = $gross_salary_mar_april = $hra = $dearness_aw = $city_c_aw = $depu_aw = $gr_in_scheme = $dcps_corp_aw = 0;
            // foreach ($deductionsFromExcel as $ded) {

            //     $actual_basic_salary_mar_april_may += $ded->Earned_Basic + $ded->LEAVEPAY;
            //     $gross_salary_mar_april += $ded->Actual_Basic;
            //     // $gross_salary_mar_april += $ded->Total_Earning;
            //     $total_lic_deduction += $ded->LIC;
            //     $dcps_emp += $ded->DCPS_I_EMPLOYEE_CONTRIBUTION;
            //     $dcps_empr += $ded->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION;
            //     $gpf += $ded->GENERAL_PROVIDENT_FUND;
            //     $income_tax += $ded->INCOME_TAX;

            //     $dearness_aw += $ded->DEARNESS_ALLOWANCE;
            //     $hra += $ded->HOUSERENT_ALLOWANCE;
            //     $city_c_aw += $ded->CITY_COMPENSATORY_ALLOWANCE;
            //     $depu_aw += $ded->DEPUTATION_ALLOWANCE;
            //     $gr_in_scheme += $ded->GROUP_INSURANCE_SCHEME;
            //     $dcps_corp_aw += $ded->DCPSCorpAllw;
            // }

            //Dearness allowance
            // $da_csv = DB::table('csv_imported_da')
            //     ->where('EmployeeId', $employee->employee_id)
            //     ->selectRaw('SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
            //     ->groupBy('EmployeeId')
            //     ->get();
            // $da_gross = 0;
            // if ($da_csv->count()) {
            //     $da_gross = $da_csv[0]->da;
            //     // $dcps_emp += $da_csv[0]->dcpsemployee;
            //     // $dcps_empr += $da_csv[0]->dcpsemployer;
            // }

            //7th pay difference
            // $seventh_pay_diff_csv = DB::table('csv_imported_7th_difference')
            //     ->where('Employee ID', $employee->employee_id)
            //     ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
            //     ->groupBy('Employee ID')
            //     ->get();
            // $seventh_pay_diff_gross = 0;
            // if ($seventh_pay_diff_csv->count()) {
            //     $seventh_pay_diff_gross = $seventh_pay_diff_csv[0]->fifth_installment;
            // }

            // $data['seventh_pay_fifth_installment'] = $seventh_pay_diff_gross;
            //====================================================================

            // $month = [5, 4, 3];
            // $supp_bill_s = round($supplementary_bill / 3);
            // $total_lic_deduction_s = round($total_lic_deduction / 3);
            // $dcps_emp_s = round($dcps_emp / 3);
            // $dcps_empr_s = round($dcps_empr / 3);
            // $gpf_s = round($gpf / 3);
            // $gross_salary_mar_april_s = round($gross_salary_mar_april / 3);
            // $actual_basic_salary_mar_april_may_s = round($actual_basic_salary_mar_april_may / 3);
            // $income_tax_s = round($income_tax / 3);
            // $da_gross_s = round($da_gross / 3);

            // $dearness_aw_s = round($dearness_aw / 3);
            // $hra_s = round($hra / 3);
            // $city_c_aw_s = round($city_c_aw / 3);
            // $depu_aw_s = round($depu_aw / 3);
            // $gr_in_scheme_s = round($gr_in_scheme / 3);

            // $data_values = array('supp_bill' => $supp_bill_s, 'total_lic_deduction' => $total_lic_deduction_s, 'dcps_emp' => $dcps_emp_s, 'dcps_empr' => $dcps_empr_s, 'gpf' => $gpf_s, 'gross_salary_mar_april' => $gross_salary_mar_april_s, 'income_tax' => $income_tax_s, 'da_gross' => $da_gross_s, 'dearness_aw' => $dearness_aw_s, 'hra' => $hra_s, 'city_c_aw' => $city_c_aw_s, 'depu_aw' => $depu_aw_s, 'gr_in_scheme' => $gr_in_scheme_s, 'actual_basic_salary_mar_april_may' => $actual_basic_salary_mar_april_may_s, 'dcps_corp_aw' => round($dcps_corp_aw / 3));

            // foreach ($month as $m) {

            //     $newRec = $this->getYearlyPaysheetRow($m, $data_values, $employee);
            //     $currentYearRec->prepend($newRec);
            // }

            // $data['freezeAttendances'] = $currentYearRec;

            // $data['marathi_name'] = $marathi_name;
            // $data['ward_name'] = $employee->ward->name;
            // $data['department_name'] = $employee->department->name;
            // $data['designation_name'] = $employee->designation->name;
            // $data['yearRange'] = "(Mar $previousEndYear - Feb $endYear)";
            // $data['startYear'] = "$previousEndYear";
            // $data['endYear'] = "$endYear";
            // $filename = "Yearly Salary Slip" . " " . $employee->employee_id . '.pdf';

            $pdf = SnappyPdf::loadView('admin.reports.pdf.yearly-pay-sheet-marathi-pdf', $data)
                ->setPaper('a4')
                ->setOrientation('landscape')
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 3)
                ->setOption('margin-left', 0)
                ->setOption('margin-right', 0);

            return $pdf->inline($filename);
        // }
    }

    public function getYearlyPaysheetRow($month, $data, $employee)
    {
        if ($month == 3) {
            $fromdate = '2024-03-01';
            $todate = '2024-03-31';
            $present_day = 31;
        } elseif ($month == 4) {
            $fromdate = '2024-04-01';
            $todate = '2024-04-30';
            $present_day = 30;
        } else {
            $fromdate = '2024-05-01';
            $todate = '2024-05-31';
            $present_day = 31;
        }
        $gpf = $data['gpf'];
        $total_lic_deduction = $data['total_lic_deduction'];
        $supplementary_bill = $data['supp_bill'];
        $gross_salary_mar_april = $data['gross_salary_mar_april'];
        $da_gross = $data['da_gross'];
        $dcps_emp = $data['dcps_emp'];
        $dcps_empr = $data['dcps_empr'];
        $income_tax = $data['income_tax'];

        $dearness_aw = $data['dearness_aw'];
        $hra = $data['hra'];
        $city_c_aw = $data['city_c_aw'];
        $depu_aw = $data['depu_aw'];
        $gr_in_scheme = $data['gr_in_scheme'];
        $dcps_corp_aw = $data['dcps_corp_aw'];

        $actual_basic_salary_mar_april_may = $data['actual_basic_salary_mar_april_may'];

        $professional_tax = 200;

        //Create a new row to prepend to freeze attendance records
        $newRec = new FreezeAttendance();
        $newRec->Emp_Code = $employee->employee_id;
        $newRec->from_date = $fromdate;
        $newRec->to_date = $todate;
        $newRec->month = $month;
        $newRec->present_day = $present_day;
        $newRec->grade_pay = 0;
        $newRec->allowance_Id = '1,2,4,13';
        $newRec->allowance_Amt = "$dearness_aw,$hra,$city_c_aw,$depu_aw";
        $newRec->allowance_Type = 1;
        $newRec->deduction_Id = '1,2,5,7,8,11,21';
        $newRec->deduction_Amt = "$professional_tax,$income_tax,$gpf,$dcps_emp,$dcps_empr,$total_lic_deduction,$gr_in_scheme";
        $newRec->deduction_Type = 1;
        $newRec->stamp_duty = 1;
        $newRec->loan_deduction_id = 0;
        $newRec->loan_deduction_amt = 0;
        $newRec->loan_deduction_bank_id = 0;
        $newRec->total_loan_deduction = 0;
        $newRec->lic_deduction_id = 0;
        $newRec->lic_deduction_amt = 0;
        $newRec->emp_name = $employee->fname . ' ' . $employee->mname . ' ' . $employee->lname;
        $newRec->pf_account_no = 0;
        $newRec->pay_band_scale = $employee->salary->pay_scale->pay_band_scale;
        $newRec->grade_pay_scale = $employee->salary->pay_scale->grade_pay_name;
        $newRec->date_of_birth = $employee->dob;
        $newRec->date_of_appointment = $employee->doj;
        $newRec->date_of_retirement = $employee->retirement_date;
        $newRec->bank_account_number = 0;

        $newRec->basic_salary = $actual_basic_salary_mar_april_may;
        $newRec->earned_basic = $actual_basic_salary_mar_april_may;
        // $newRec->total_allowance = $dearness_aw + $hra + $city_c_aw + $depu_aw + $da_gross;
        $newRec->total_allowance = $dearness_aw + $hra + $city_c_aw + $depu_aw;
        $newRec->total_lic_deduction = 0;
        $newRec->employee_share_da = 0;
        $newRec->corporation_share_da = $dcps_corp_aw;
        $newRec->total_deduction = $total_lic_deduction + $dcps_emp + $dcps_empr + $gpf + $income_tax + $newRec->stamp_duty + $professional_tax + $gr_in_scheme;
        $newRec->net_salary = $newRec->basic_salary + $newRec->total_allowance + $dcps_corp_aw - $newRec->total_deduction;
        $newRec->save();

        return $newRec;
    }
}
