<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\Loan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;

class BankDeductionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();

        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;
        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
        $department = $request->department ?? null;

        $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->latest()->get();

        $loans = Loan::latest()->get();

        // If from date and to date are not provided, calculate current month's from date and to date
        $financial_year = FinancialYear::where('id', session('financial_year'))->first();

        if (!$from_date || !$to_date) {
            if ($financial_year) {
                if ($month <= 3) {
                    $year = date('Y', strtotime($financial_year->to_date));
                } else {
                    $year = date('Y', strtotime($financial_year->from_date));
                }
                $month = $month ?? 1;

                $from_date = Carbon::parse($year . '-' . ($month) . '-' . 01);
                $to_date = clone ($from_date);
                $from_date = (string) $from_date->startOfMonth()->toDateString();
                $to_date = (string) $to_date->endOfMonth()->toDateString();
            }
        }


        return view('admin.reports.bank-deduction')->with([
            'loans' => $loans,
            'departments' => $departments,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'month' => $month,
            'monthName' => $monthName,
            'department_id' => $department
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
