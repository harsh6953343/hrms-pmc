<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\RemainingFreezeSalary;
use App\Models\SupplimentaryBill;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Http\Response;
use App\Models\UserDepartment;
use App\Models\Ward;

class SalarySlipController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $authUser = Auth::user();
            $from_date = $request->from_date ?? null;
            $to_date = $request->to_date ?? null;
            $month = $request->month ?? date('m');

            // If from date and to date are not provided, calculate current month's from date and to date
            $financial_year = FinancialYear::where('id', session('financial_year'))->first();
            if (!$from_date || !$to_date) {
                if ($financial_year) {
                    if ($month <= 3) {
                        $year = date('Y', strtotime($financial_year->to_date));
                    } else {
                        $year = date('Y', strtotime($financial_year->from_date));
                    }
                    $month = $month ?? 1;

                    // $from_date = Carbon::parse($year . '-' . ($month) . '-' . 16)->subMonth()->toDateString();
                    $from_date = Carbon::parse($year . '-' . ($month) . '-' . 01);
                    $to_date = clone ($from_date);
                    $from_date = (string) $from_date->startOfMonth()->toDateString();
                    $to_date = (string) $to_date->endOfMonth()->toDateString();
                }
            }

            $current_month = date('n');

            $salary_slips = FreezeAttendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
                    return $query->where('employee_id', $authUser->employee_id);
                })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                })
                ->when(isset($request->ward_id) && $request->ward_id != "", function ($q) use ($request) {
                    $q->where('ward_id', $request->ward_id);
                })
                ->when(($current_month == $month && $authUser->id != 1), function ($query) {
                    return $query->where('show_salary_slips', 1);
                })
                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                ->orderBy('Emp_Code')
                ->get();

            $wards = Ward::get();

            return view('admin.reports.salary-slips')->with(['salary_slips' => $salary_slips, 'from_date' => $from_date, 'to_date' => $to_date, 'month' => $month, 'wards' => $wards]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store() {}

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $data['freezeAttendance'] = FreezeAttendance::where('id', $id)->first();

        $data['employee_details'] = Employee::with('ward', 'department', 'designation')->where('id', $data['freezeAttendance']->employee_id)->first();

        $data['allowances'] = Allowance::latest()->get();
        $data['deductions'] = Deduction::latest()->get();

        $data['explode_allowance_ids'] = explode(',', $data['freezeAttendance']->allowance_Id);

        $data['explode_allowance_type'] = explode(',', $data['freezeAttendance']->allowance_Type);
        $data['explode_allowance_amt'] = explode(',', $data['freezeAttendance']->allowance_Amt);


        $data['explode_deduction_ids'] = explode(',', $data['freezeAttendance']->deduction_Id);
        $data['explode_deduction_amt'] = explode(',', $data['freezeAttendance']->deduction_Amt);
        $data['explode_deduction_type'] = explode(',', $data['freezeAttendance']->deduction_Type);

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $filename = "Salary Slip" . " " . $data['freezeAttendance']->Emp_Code . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.salary-slip-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }

    public function salarySlipMarathi($id)
    {
        $data['freezeAttendance'] = FreezeAttendance::where('id', $id)->first();

        $data['employee_details'] = Employee::with('ward', 'department', 'designation')->where('id', $data['freezeAttendance']->employee_id)->first();
        $data['allowances'] = Allowance::latest()->get();
        $data['deductions'] = Deduction::latest()->get();

        $data['explode_allowance_ids'] = explode(',', $data['freezeAttendance']->allowance_Id);

        $data['explode_allowance_type'] = explode(',', $data['freezeAttendance']->allowance_Type);
        $data['explode_allowance_amt'] = explode(',', $data['freezeAttendance']->allowance_Amt);


        $data['explode_deduction_ids'] = explode(',', $data['freezeAttendance']->deduction_Id);
        $data['explode_deduction_amt'] = explode(',', $data['freezeAttendance']->deduction_Amt);
        $data['explode_deduction_type'] = explode(',', $data['freezeAttendance']->deduction_Type);

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $filename = "Salary Slip" . " " . $data['freezeAttendance']->Emp_Code . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.salary-slip-marathi-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('encoding', 'UTF-8');


        return $pdf->inline($filename);
    }

    public function showSalarySlips()
    {

        FreezeAttendance::where('show_salary_slips', 0)->update(['show_salary_slips' => 1]);
        return response()->json(['success' => 'Salary slips are visible.']);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }


    public function viewSalaryInGroup(Request $request)
    {
        set_time_limit(0);
        $authUser = Auth::user();
        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;
        $month = $request->month ?? date('m');

        $salary_slips = FreezeAttendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
                return $query->where('employee_id', $authUser->employee_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->when(isset($request->ward_id) && $request->ward_id != "", function ($q) use ($request) {
                $q->where('ward_id', $request->ward_id);
            })
            ->with(['employee.ward', 'employee.department', 'employee.designation', 'employee.employee_status'])
            ->where('freeze_status', 1)
            ->where('from_date', $from_date)
            ->where('to_date', $to_date)
            ->orderBy('Emp_Code')
            ->get();


        $allowances = Allowance::latest()->get();
        $deductions = Deduction::latest()->get();
        $corporation = Corporation::first();

        $filename = "Salary Slip.pdf";
        if($request->lang == 'mr'){
            $pdf = SnappyPdf::loadView('admin.reports.pdf.ward-salary-slip-marathi-pdf', compact('salary_slips', 'allowances', 'deductions', 'corporation'));
        }else{
            $pdf = SnappyPdf::loadView('admin.reports.pdf.ward-salary-slip-pdf', compact('salary_slips', 'allowances', 'deductions', 'corporation'));
        }
        $pdf = $pdf->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }
}
