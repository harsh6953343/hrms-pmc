<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Controller;
use App\Models\AddEmployeeLeave;
use App\Models\Attendance;
use App\Models\EmployeeLeaves;
use App\Models\LeaveType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PendingEmployeeLeaveImport;
use App\Models\FreezeCron;
use Illuminate\Support\Facades\DB;
use App\Models\ImportPendingEmployeeLeave;

class LeaveApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employee_leave = AddEmployeeLeave::with(['leaveType'])->where('status', 0)
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->when(Auth::user()->hasRole('DMC'), function ($q) {
                $q->whereHas('leaveType', function ($q) {
                    $q->where('is_approve_by', 2);
                })->where('amc_status', 1);
            })
            ->when(Auth::user()->hasRole('AMC'), function ($q) {
                $q->where('amc_status', 0);
            })
            ->latest()->get();

        // return $employee_leave;

        $leaveType = LeaveType::latest()->get();

        return view('admin.attendance.pending-employee-leave')->with([
            'employee_leaves' => $employee_leave,
            'leaveTypes' => $leaveType
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $importLeaves = ImportPendingEmployeeLeave::select('excel_file', 'month', 'period')->latest()->get();

        return view('admin.attendance.import-pending-employee-leave')->with([
            'importLeaves' => $importLeaves
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        set_time_limit(0);

        $request->validate([
            'month' => 'required',
            'leave_files' => 'required'
        ]);

        Excel::import(new PendingEmployeeLeaveImport, $request->file('leave_files'));

        if ($request->hasFile('leave_files')) {
            $request['excel_file'] = $request->leave_files->store('import-pending-employee-leaves');
        }
        ImportPendingEmployeeLeave::create($request->all());

        return redirect()->back()->with('success', "Import leave approval successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasRole(['AMC', 'DMC'])) {
            return response()->json(['error' => 'Not Applicable For Reject The Application']);
        }

        $check_applied_leave = AddEmployeeLeave::with(['leaveType'])->where('id', $id)->first();

        if ($check_applied_leave) {
            if (Auth::user()->hasRole('AMC')) {
                $check_applied_leave->amc_status = 2;
                $check_applied_leave->amc_reject_remark = $request->remark;
                $check_applied_leave->status = 2;
                $check_applied_leave->status_by = Auth::user()->id;
                $check_applied_leave->status_dt = now();
                $check_applied_leave->reject_remark = $request->remark;
                $check_applied_leave->amc_datetime = now();
                $check_applied_leave->save();
            }
            if (Auth::user()->hasRole('DMC')) {
                $check_applied_leave->dmc_status = 2;
                $check_applied_leave->dmc_reject_remark = $request->remark;
                $check_applied_leave->status = 2;
                $check_applied_leave->status_by = Auth::user()->id;
                $check_applied_leave->status_dt = now();
                $check_applied_leave->reject_remark = $request->remark;
                $check_applied_leave->dmc_datetime = now();
                $check_applied_leave->save();
            }


            return response()->json(['success' => 'Employee Leave rejected successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function ApproveLeave($id)
    {
        if (!Auth::user()->hasRole(['AMC', 'DMC'])) {
            return response()->json(['error' => 'Not Applicable For Approved The Application']);
        }
        $check_applied_leave = AddEmployeeLeave::with(['leaveType'])->where('id', $id)->first();
        if ($check_applied_leave) {

            // Get the attendance record for the employee for the specific month and financial year
            $get_attendance = Attendance::where('employee_id', $check_applied_leave->employee_id)
                ->where('month', $check_applied_leave->month)
                ->where('financial_year_id', $check_applied_leave->financial_year_id)
                ->first();

            // if(!empty($get_attendance) && $get_attendance->main_present_days == $get_attendance->total_present_days){
            //     return response()->json(['error' => 'No Absent Days Found']);
            // }

            $check_leave_available = EmployeeLeaves::where('employee_id', $check_applied_leave->employee_id)
                ->when($check_applied_leave->leave_type_id == 4, function ($q) {
                    $q->where('leave_type_id', 2);
                })
                ->when($check_applied_leave->leave_type_id != 4, function ($q) use ($check_applied_leave) {
                    $q->where('leave_type_id', $check_applied_leave->leave_type_id);
                })
                ->where('no_of_leaves', '>=', $check_applied_leave->no_of_days)->first();

            if (empty($check_leave_available)) {
                return response()->json(['error' => 'No Leaves Found']);
            }

            if (!empty($get_attendance) && !empty($check_leave_available)) {
                $isUpdate = false;
                if (Auth::user()->hasRole('AMC')) {
                    if ($check_applied_leave->leaveType?->is_approve_by == "1") {
                        $isUpdate = true;
                    }
                    $check_applied_leave->amc_status = 1;
                    $check_applied_leave->amc_datetime = now();
                } else if (Auth::user()->hasRole('DMC')) {
                    if ($check_applied_leave->leaveType?->is_approve_by == "2") {
                        $isUpdate = true;
                    }
                    $check_applied_leave->dmc_status = 1;
                    $check_applied_leave->dmc_datetime = now();
                }

                if ($isUpdate) {
                    // Update the attendance record
                    if ($check_applied_leave->leave_type_id == 4) {
                        $get_attendance->total_leave += 0.5;
                        $get_attendance->total_half_days += 1;
                        $get_attendance->total_present_days = $get_attendance->main_present_days - $get_attendance->total_absent_days - 0.5;
                    } else {
                        $get_attendance->total_leave += $check_applied_leave->no_of_days;
                        $get_attendance->total_absent_days += $check_applied_leave->no_of_days;

                        $totalhalfdays = $get_attendance->total_half_days * 0.5;
                        $get_attendance->total_present_days = $get_attendance->main_present_days - $get_attendance->total_absent_days - $totalhalfdays;
                    }
                    // $get_attendance->total_present_days -= $check_applied_leave->no_of_days;
                    $get_attendance->save(); // Make sure to save the updated attendance record

                    $check_leave_available->no_of_leaves -= $check_applied_leave->no_of_days;
                    $check_leave_available->save();


                    $check_applied_leave->status = 1;
                    $check_applied_leave->status_by = Auth::user()->id;
                    $check_applied_leave->status_dt = now();
                }
                $check_applied_leave->save();



                return response()->json(['success' => 'Employee Leave approved successfully!']);
            }
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    public function leaveApproveStatic(Request $request)
    {
        return view('admin.attendance.approve-employee-leave');
    }

    public function storeLeaveApproveStatic(Request $request)
    {
        set_time_limit(0);
        $check_applied_leaves = AddEmployeeLeave::with(['leaveType'])->where('month', $request->month)->get();

        DB::beginTransaction();
        try {
            foreach ($check_applied_leaves as $check_applied_leave) {
                if ($check_applied_leave) {

                    // Get the attendance record for the employee for the specific month and financial year
                    $get_attendance = Attendance::where('employee_id', $check_applied_leave->employee_id)
                        ->where('month', $check_applied_leave->month)
                        ->where('financial_year_id', $check_applied_leave->financial_year_id)
                        ->first();

                    $check_leave_available = EmployeeLeaves::where('employee_id', $check_applied_leave->employee_id)
                        ->where('leave_type_id', $check_applied_leave->leave_type_id)
                        ->where('no_of_leaves', '>=', $check_applied_leave->no_of_days)->first();

                    if (!empty($get_attendance)) {

                        // Update the attendance record
                        if ($check_applied_leave->leave_type_id == 4) {
                            $get_attendance->total_leave += 0.5;
                            $get_attendance->total_half_days += 1;
                            $get_attendance->total_present_days = $get_attendance->main_present_days - $get_attendance->total_absent_days - 0.5;
                        } else {
                            $get_attendance->total_leave += $check_applied_leave->no_of_days;
                            $get_attendance->total_absent_days += $check_applied_leave->no_of_days;

                            $totalhalfdays = $get_attendance->total_half_days * 0.5;
                            $get_attendance->total_present_days = $get_attendance->main_present_days - $get_attendance->total_absent_days - $totalhalfdays;
                        }
                        // $get_attendance->total_present_days -= $check_applied_leave->no_of_days;
                        $get_attendance->save(); // Make sure to save the updated attendance record

                        $check_leave_available->no_of_leaves -= $check_applied_leave->no_of_days;
                        $check_leave_available->save();


                        $check_applied_leave->status = 1;
                        $check_applied_leave->status_by = Auth::user()->id;
                        $check_applied_leave->status_dt = now();

                        $check_applied_leave->save();
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('success',  'Leave approved successfully!');
        } catch (\Exception  $e) {
            DB::rollback();
            return redirect()->back()->with('error', "Something went wrong, please try again!");
        }
    }
}
