<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Attendances\StoreAttendanceExcelRequest;
use App\Models\AttendanceExcelCron;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Imports\AttendanceImport;
use App\Imports\AbsentAttendanceImport;
use App\Imports\DeleteAbsentAttendanceImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\EmployeeDeduction;
use App\Models\AbsentAttendanceExcel;
use App\Models\FinancialYear;
use App\Models\Attendance;
use App\Models\AttendanceExcelBackup;
use App\Models\Ward;

class AttendanceExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $absentAttendances = AbsentAttendanceExcel::latest()->get();

        $wards = Ward::where('is_corporation', 0)->select('id', 'name')->get();

        return view('admin.attendance.add-absent-attendance-excel')->with([
            'absentAttendances' => $absentAttendances,
            'wards' => $wards
        ]);
        // return view('admin.attendance.add-attendance-excel');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAttendanceExcelRequest $request)
    {
        // return redirect()->back()->with('error', "Something went wrong, please try again!");
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $input['financial_year_id'] = Session('financial_year');

            $path = $request->file('file')->store('attendance_files', 'public');
            $input['file'] = $path;
            AttendanceExcelCron::create(Arr::only($input, AttendanceExcelCron::getFillables()));
            Excel::import(new AttendanceImport, $request->file('file'));

            DB::commit();

            return response()->json(['success' => 'Attendance Excel created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Attendance Excel');
        }
    }

    public function storeImportAbsent(Request $request)
    {
        // return redirect()->back()->with('error', "Something went wrong, please try again!");
        set_time_limit(0);

        $request->validate([
            'ward' => 'required',
            'month' => 'required',
            'period' => 'required',
            'file' => 'required'
        ]);

        DB::beginTransaction();
        try {
            // if ($request->period == "2") {
            //     EmployeeDeduction::where('deduction_id', 12)->update(['deduction_amt' => 0]);
            // }
            $ward = "";
            if ($request->ward == "All") {
                $ward = Ward::where('is_corporation', 1)->pluck('id')->toArray();
            } else {
                $ward = [$request->ward];
            }
            if ($request->period == "1") {
                $attendances = Attendance::where('month', $request->month)->where('financial_year_id', session('financial_year'))->get();

                $data = [];
                $mainPresentDay = 0;
                foreach ($attendances as $attendance) {
                    $data[] = [
                        'attendance_id' => $attendance->id,
                        'employee_id' => $attendance->employee_id,
                        'financial_year_id' => $attendance->financial_year_id,
                        'emp_code' => $attendance->Emp_Code,
                        'emp_name' => $attendance->emp_name,
                        'from_date' => $attendance->from_date,
                        'to_date' => $attendance->to_date,
                        'main_present_days' => $attendance->main_present_days,
                        'total_present_days' => $attendance->total_present_days,
                        'total_absent_days' => $attendance->total_absent_days,
                        'total_leave' => $attendance->total_leave,
                        'total_half_days' => $attendance->total_half_days,
                        'month' => $attendance->month,
                    ];
                    $mainPresentDay = $attendance->main_present_days;
                }

                if (count($data) > 0) {
                    AttendanceExcelBackup::insert($data);
                }
            }
            $import = new AbsentAttendanceImport();
            Excel::import($import, $request->file('file'));
            $data1 = $import->getEmpCodes();

            if ($request->ward == "All" && $request->period == "1") {
                $import2 = new AbsentAttendanceImport();
                Excel::import($import2, $request->file('file2'));
                $data2 = $import2->getEmpCodes();
                $mergeEmp = array_merge($data1, $data2);
            } else {
                $mergeEmp = $data1;
            }


            // Excel::import(new AbsentAttendanceImport, $request->file('file'));

            if ($request->hasFile('file')) {
                $request['excel_file'] = $request->file->store('absent-attendance-excel');
            }

            AbsentAttendanceExcel::create($request->all());

            if ($request->hasFile('file2')) {
                $request['excel_file'] = $request->file2->store('absent-attendance-excel');
            }
            AbsentAttendanceExcel::create($request->all());

            if ($request->period == "1") {
                Attendance::whereHas('employee', function ($q) use ($ward) {
                    $q->whereIn('ward_id', $ward);
                })->where('month', $request->month)->where('financial_year_id', session('financial_year'))
                    ->whereNotIn('Emp_Code', $mergeEmp)->update([
                        'total_present_days' => 0,
                        'total_absent_days' => $mainPresentDay,
                        'total_leave' => 0,
                        'total_half_days' => 0
                    ]);
            }

            DB::commit();
            return redirect()->back()->with('success', "Absent Employee Attendance Uploaded Successfully");
        } catch (\Exception $e) {
            \Log::info($e);
            DB::rollback();
            return redirect()->back()->with('error', "Something went wrong, please try again!");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        /*set_time_limit(0);
        DB::beginTransaction();
        try {
            $absentExcel = AbsentAttendanceExcel::find($id);

            if ($absentExcel->period == "1") {
                if (Storage::disk('public')->exists($absentExcel->excel_file)) {
                    $fullPath = Storage::disk('public')->path($absentExcel->excel_file);

                    Excel::import(new DeleteAbsentAttendanceImport($absentExcel->period, $absentExcel->month), $fullPath);

                    // Storage::disk('public')->delete($absentExcel->excel_file);
                }
            }
            $absentExcel->delete();
            DB::commit();
            return redirect()->back()->with('success', "Delete Absent Employee Attendance Uploaded Excel File Successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', "Something went wrong, please try again!");
        } */
    }


    public function deleteAllImportAbsent(Request $request)
    {
        return view('admin.attendance.add-absent-attendance-delete');
    }

    public function postDeleteAllImportAbsent(Request $request)
    {
        // return redirect()->back()->with('error', "Something went wrong, please try again!");
        set_time_limit(0);
        $financial_year = FinancialYear::where('is_active', 1)->first();
        DB::beginTransaction();

        try {
            $attendances = Attendance::where('month', $request->month)->where('financial_year_id', $financial_year->id)->get();

            foreach ($attendances as $attendance) {
                $total = $attendance->main_present_days - ($attendance->total_half_days / 2) - $attendance->total_leave;
                $attendance->total_present_days = $total;

                $attendance->total_absent_days = 0;
                $attendance->save();
            }

            AbsentAttendanceExcel::where('month', $request->month)->delete();
            DB::commit();
            return redirect()->back()->with('success', "Delete Absent Employee Attendance");
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', "Something went wrong, please try again!");
        }
    }
}
