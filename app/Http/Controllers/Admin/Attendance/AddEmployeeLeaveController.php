<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Attendance\StoreEmployeeLeaveRequest;
use App\Models\AddEmployeeLeave;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\EmployeeLeaves;
use App\Models\FinancialYear;
use App\Models\FreezeCron;
use App\Models\LeaveType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DataTables;

class AddEmployeeLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $leaveType = LeaveType::latest()->get();

        return view('admin.attendance.employee_leave')->with([
            'leaveTypes' => $leaveType
        ]);
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {
            $authUser = Auth::user();

            $employeeLeave = AddEmployeeLeave::with('employee.department', 'leaveType')
                ->whereHas('employee', function ($query) use ($authUser) {
                    $query->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                        return $query->where('ward_id', $authUser->ward_id);
                    })
                        ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                            return $query->where('department_id', $authUser->department_id);
                        });
                })
                ->latest();

            return DataTables::of($employeeLeave)
                ->addIndexColumn()
                ->addColumn('full_name', function ($data) {
                    return $data?->employee->fname . ' ' . $data?->employee->mname . ' ' . $data?->employee->lname;
                })
                ->filterColumn('full_name', function ($query, $keyword) {
                    $query->whereHas('employee', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("CONCAT(fname, ' ', mname,' ', lname) LIKE ?", ["%$keyword%"]);
                    });
                })
                ->addColumn('department_name', function ($data) {
                    return $data?->employee?->department?->name;
                })
                ->filterColumn('department_name', function ($query, $keyword) {
                    $query->whereHas('employee.department', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->addColumn('leave_type_name', function ($data) {
                    return $data?->leaveType?->name;
                })
                ->filterColumn('leave_type_name', function ($query, $keyword) {
                    $query->whereHas('leaveType', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->editColumn('apply_date', function ($data) {
                    return ($data?->created_at) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->created_at)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('apply_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('amc_datetime', function ($data) {
                    return ($data?->amc_datetime) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->amc_datetime)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('amc_datetime', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('dmc_datetime', function ($data) {
                    return ($data?->dmc_datetime) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->dmc_datetime)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('dmc_datetime', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('from_date', function ($data) {
                    return Carbon::createFromFormat('Y-m-d', $data?->from_date)->format('d-m-Y');
                })
                ->filterColumn('from_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('to_date', function ($data) {
                    return Carbon::createFromFormat('Y-m-d', $data?->to_date)->format('d-m-Y');
                })
                ->filterColumn('to_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(to_date,'%d-%m-%Y') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('leave_file', function ($data) {
                    return asset('storage/' . $data?->leave_file);
                })
                ->editColumn('amc_status', function ($data) {
                    return $data?->amc_status == 1 ? 'Approved' : 'Pending';
                })
                ->filterColumn('amc_status', function ($query, $keyword) {
                    $query->whereRaw("
                        CASE
                            WHEN amc_status = '1' THEN 'Approved'
                            ELSE 'Pending'
                        END LIKE ?", ["%{$keyword}%"]);
                })
                ->editColumn('dmc_status', function ($data) {
                    if ($data?->leaveType->is_approve_by == 2) {
                        return $data?->dmc_status == 1 ? 'Approved' : 'Pending';
                    }
                    return '-';
                })
                ->filterColumn('dmc_status', function ($query, $keyword) {
                    $query->whereRaw("
                        CASE
                            WHEN dmc_status = '1' THEN 'Approved'
                            ELSE 'Pending'
                        END LIKE ?", ["%{$keyword}%"]);
                })
                ->editColumn('id', function ($data) {
                    if (Auth::user()->hasRole('Super Admin')) {
                        return $data->id;
                    } else {
                        if ($data->status != "") {
                            return $data->id;
                        }
                    }
                    return "";
                })
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeLeaveRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            if ($input['leave_type_id'] == 4) {
                $input['no_of_days'] = $input['no_of_days'] * 0.5;
            }
            // dd($input);
            if ($input['leave_type_id'] != 4) {
                $check_leave_available = EmployeeLeaves::where('employee_id', $input['employee_id'])
                    ->where('leave_type_id', $input['leave_type_id'])
                    ->where('no_of_leaves', '>=', $input['no_of_days'])->first();

                if (empty($check_leave_available)) {
                    return response()->json(['error2' => 'No Leaves Available!']);
                }
            }

            // Check if leave already exists for the given dates
            $checkAlreadyExist = AddEmployeeLeave::where('employee_id', $input['employee_id'])
                ->where('from_date', '>=', $input['from_date'])
                ->where('to_date', '<=', $input['to_date'])
                ->whereIn('status', ['0', '1'])
                ->exists();

            if ($checkAlreadyExist) {
                return response()->json(['error2' => 'Leave already exists for the given dates!']);
            }

            // Get the active financial year
            $financial_year = FinancialYear::where('is_active', 1)->first();
            $input['financial_year_id'] = $financial_year->id;


            // Create the employee leave record
            if ($request->hasFile('leave_files')) {
                $input['leave_file'] = $request->leave_files->store('employee-leave');
            }
            AddEmployeeLeave::create(Arr::only($input, AddEmployeeLeave::getFillables()));

            DB::commit();

            return response()->json(['success' => 'Employee Leave created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Employee Leave');
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $addEmployeeLeave = AddEmployeeLeave::find($id);

        return view('admin.attendance.leave.show')->with([
            'addEmployeeLeave' => $addEmployeeLeave
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(AddEmployeeLeave $add_leave)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $add_leave->delete();
    //         DB::commit();
    //         return response()->json(['success' => 'Employee Leave deleted successfully!']);
    //     } catch (\Exception $e) {
    //         return $this->respondWithAjax($e, 'deleting', 'Employee Leave');
    //     }
    // }

    public function destroy(AddEmployeeLeave $add_leave)
    {
        try {
            DB::beginTransaction();

            // Delete the leave record
            // dd($add_leave->status);


            // Find the freeze record for the specified date range with salary_generated = 0
            $get_freeze_record = FreezeCron::where('to_date', '<=', $add_leave->from_date)
                // ->where('to_date', '<=', $add_leave->to_date)
                ->where('salary_generated', 0)
                ->first();

            if ($get_freeze_record && $add_leave->status == 1) {


                // Find the attendance record for the employee in the specified month and financial year
                $check_attendance = Attendance::where('employee_id', $add_leave->employee_id)
                    ->where('month', $add_leave->month)
                    ->where('financial_year_id', $add_leave->financial_year_id)
                    ->first();

                $employeeLeaves = EmployeeLeaves::where('employee_id', $add_leave->employee_id)->where('leave_type_id', $add_leave->leave_type_id)->first();

                if ($check_attendance) {
                    // Update attendance with adjusted leave and present days
                    if ($add_leave->leave_type_id == 4) {
                        $no_of_days = 0.5;
                    } else {
                        $no_of_days = $add_leave->no_of_days;
                    }

                    $new_total_leave = $check_attendance->total_leave - $no_of_days;
                    $new_total_present_days = min($check_attendance->main_present_days, $check_attendance->total_present_days + $no_of_days);

                    $attendance_details = array(
                        'total_leave' => $new_total_leave,
                        'total_present_days' => $new_total_present_days,
                    );

                    if ($add_leave->leave_type_id == 4) {
                        $attendance_details['total_half_days'] = $check_attendance->total_half_days - $add_leave->no_of_days;
                    } else {
                        $attendance_details['total_absent_days'] = $check_attendance->total_absent_days - $no_of_days;
                    }

                    $check_attendance->update($attendance_details);

                    $employeeLeaves->update([
                        'no_of_leaves' => $employeeLeaves->no_of_leaves +  $add_leave->no_of_days,
                    ]);
                }
            }

            if ($add_leave->leave_file != "") {
                if (Storage::exists($add_leave->leave_file)) {
                    Storage::delete($add_leave->leave_file);
                }
            }

            $add_leave->delete();
            DB::commit();
            return response()->json(['success' => 'Employee Leave deleted successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Error deleting Employee Leave: ' . $e->getMessage()], 500);
        }
    }

    public function fetchEmployeeDetails($emp_id, $month)
    {
        $authUser = Auth::user();

        // Fetch employee details with necessary conditions
        $employee_details = Employee::with('ward', 'department', 'designation', 'class')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->whereNotExists(function ($query) use ($emp_id) {
                $query->select(DB::raw(1))
                    ->from('employee_statuses')
                    ->whereColumn('employee_statuses.employee_id', 'employees.id')
                    ->where('employee_statuses.Emp_Code', $emp_id)
                    ->whereNull('employee_statuses.deleted_at');
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id', 'pf_account_no', 'dob')
            ->first();

        // Fetch active financial year
        $financial_year = FinancialYear::where('is_active', 1)->first();

        if ($financial_year) {
            $year = ($month <= 3) ? date('Y', strtotime($financial_year->to_date)) : date('Y', strtotime($financial_year->from_date));
            $fromDate = Carbon::parse("$year-$month-21")->subMonth()->toDateString();
            $toDate = Carbon::parse("$year-$month-20")->toDateString();
        }
        if ($employee_details) {
            $response = [
                'result'                => 1,
                'employee_details'      => $employee_details,
                'from_date'             => $fromDate,
                'to_date'               => $toDate,
            ];
        } else {
            $response = [
                'result' => 0,
                'message' => 'Employee details or attendance details not found.'
            ];
        }

        return response()->json($response);
    }

    public function removePendingLeave(Request $request)
    {
        return view('admin.attendance.remove-pending-employee-leave');
    }

    public function storeRemovePendingLeave(Request $request)
    {
        set_time_limit(0);
        $request->validate([
            'month' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $empLeaves = AddEmployeeLeave::where('month', $request->month)->get();

            foreach ($empLeaves as $add_leave) {

                // Find the freeze record for the specified date range with salary_generated = 0
                $get_freeze_record = FreezeCron::where('to_date', '<=', $add_leave->from_date)
                    // ->where('to_date', '<=', $add_leave->to_date)
                    ->where('salary_generated', 0)
                    ->first();

                if ($get_freeze_record && $add_leave->status == 1) {


                    // Find the attendance record for the employee in the specified month and financial year
                    $check_attendance = Attendance::where('employee_id', $add_leave->employee_id)
                        ->where('month', $add_leave->month)
                        ->where('financial_year_id', $add_leave->financial_year_id)
                        ->first();

                    $employeeLeaves = EmployeeLeaves::where('employee_id', $add_leave->employee_id)->where('leave_type_id', $add_leave->leave_type_id)->first();

                    if ($check_attendance) {
                        // Update attendance with adjusted leave and present days
                        if ($add_leave->leave_type_id == 4) {
                            $no_of_days = 0.5;
                        } else {
                            $no_of_days = $add_leave->no_of_days;
                        }

                        $new_total_leave = $check_attendance->total_leave - $no_of_days;
                        $new_total_present_days = min($check_attendance->main_present_days, $check_attendance->total_present_days + $no_of_days);

                        $attendance_details = array(
                            'total_leave' => $new_total_leave,
                            'total_present_days' => $new_total_present_days,
                        );

                        if ($add_leave->leave_type_id == 4) {
                            $attendance_details['total_half_days'] = $check_attendance->total_half_days - $add_leave->no_of_days;
                        } else {
                            $attendance_details['total_absent_days'] = $check_attendance->total_absent_days - $no_of_days;
                        }

                        $check_attendance->update($attendance_details);

                        $employeeLeaves->update([
                            'no_of_leaves' => $employeeLeaves->no_of_leaves +  $add_leave->no_of_days,
                        ]);
                    }
                }

                if ($add_leave->leave_file != "") {
                    if (Storage::exists($add_leave->leave_file)) {
                        Storage::delete($add_leave->leave_file);
                    }
                }

                $add_leave->delete();
            }

            DB::commit();
            return redirect()->back()->with('success', "Employee leave remove successffully");
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', "Something went wrong");
        }
    }
}
