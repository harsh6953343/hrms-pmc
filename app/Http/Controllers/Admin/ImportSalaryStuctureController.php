<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\EmployeeCreditedSocietyImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ImportExcelFile;
use App\Models\EmployeeDeduction;

class ImportSalaryStuctureController extends Controller
{
    public function employeeCreditedSociety()
    {
        $files = ImportExcelFile::select('month', 'file')->latest()->limit(1)->get();

        return view('admin.import.employee-credited-society')->with([
            'files' => $files
        ]);
    }

    public function storeEmployeeCreditedSociety(Request $request)
    {
        set_time_limit(0);

        $request->validate([
            'import' => 'required|mimes:xlsx,xls',
            'month' => 'required'
        ], [
            'import.required' => 'Please select file.',
            'import.mimes' => 'Only xlsx and xls file is allowed.',
            'month.required' => 'Please select month'
        ]);

        try {
            EmployeeDeduction::where('deduction_id', 10)->update([
                'deduction_amt' => 0
            ]);

            Excel::import(new EmployeeCreditedSocietyImport, $request->file('import'));

            if ($request->hasFile('import')) {
                $request['file'] = $request->import->store('export-excel');
            }
            $request['financial_year'] = session('financial_year');
            ImportExcelFile::create($request->all());

            return redirect()->back()->with(['success' => 'File import successfully']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => 'Something went wrong, please try again']);
        }
    }
}
