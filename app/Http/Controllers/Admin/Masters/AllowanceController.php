<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Models\Allowance;
use App\Http\Requests\Admin\Masters\StoreAllowanceRequest;
use App\Http\Requests\Admin\Masters\UpdateAllowanceRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class AllowanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $allowance = Allowance::latest()->get();

        return view('admin.masters.allowance')->with(['allowances' => $allowance]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAllowanceRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Allowance::create(Arr::only($input, Allowance::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Allowance created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Allowance');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Allowance $allowance)
    {
        if ($allowance) {

            $typeHtml = '<span>
            <option value="">--Select Type--</option>';
            $typeHtml .= '<option value="1" ' . ($allowance->type == '1' ? 'selected' : '') . '>Amount</option>';
            $typeHtml .= '<option value="2" ' . ($allowance->type == '2' ? 'selected' : '') . '>Percentage</option>';
            $typeHtml .= '</span>';

            $calculationHtml = '<span>
            <option value="">--Select Calculation--</option>';
            $calculationHtml .= '<option value="1" ' . ($allowance->calculation == '1' ? 'selected' : '') . '>Fixed</option>';
            $calculationHtml .= '<option value="2" ' . ($allowance->calculation == '2' ? 'selected' : '') . '>Dynamic</option>';
            $calculationHtml .= '</span>';

            $response = [
                'result'            => 1,
                'allowance'         => $allowance,
                'typeHtml'          => $typeHtml,
                'calculationHtml'   => $calculationHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAllowanceRequest $request, Allowance $allowance)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $allowance->update(Arr::only($input, Allowance::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Allowance updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Allowance');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Allowance $allowance)
    {
        try {
            DB::beginTransaction();
            $allowance->delete();
            DB::commit();
            return response()->json(['success' => 'Allowance deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Allowance');
        }
    }
}
