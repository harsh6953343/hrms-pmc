<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Models\LeaveType;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Admin\Masters\StoreLeaveTypeRequest;


class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $leaveTypes = LeaveType::latest()->get();

        return view('admin.masters.leaveType')->with(['leaveTypes' => $leaveTypes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLeaveTypeRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            LeaveType::create(Arr::only($input, LeaveType::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Leave type created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Leave type');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(LeaveType $leaveType)
    {
        if ($leaveType) {

            $leavetypeHtml = '<span>
            <option value="">--Select Type--</option>';
            $leavetypeHtml .= '<option value="0" ' . ($leaveType->type == 0 ? 'selected' : '') . '>Monthly</option>';
            $leavetypeHtml .= '<option value="1" ' . ($leaveType->type == 1 ? 'selected' : '') . '>Yearly</option>';
            $leavetypeHtml .= '</span>';

            $response = [
                'result' => 1,
                'leaveType' => $leaveType,
                'leavetypeHtml' => $leavetypeHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreLeaveTypeRequest $request, LeaveType $leaveType)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $leaveType->update(Arr::only($input, LeaveType::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Leave Type updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Leave Type');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LeaveType $leaveType)
    {
        try {
            DB::beginTransaction();
            $leaveType->delete();
            DB::commit();
            return response()->json(['success' => 'Leave Type deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Leave Type');
        }
    }
}
