<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Ward;
use App\Models\Clas;
use App\Models\Designation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use App\Http\Requests\Admin\Masters\StoreDesignationRequest;
use App\Http\Requests\Admin\Masters\UpdateDesignationRequest;
use App\Models\UserDepartment;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $designations = Designation::with('ward','department','clas')->latest()->get();
        $wards = Ward::latest()->get();
        $class = Clas::latest()->get();

        return view('admin.masters.designations')->with(['designations' => $designations, 'wards' => $wards,'class' => $class]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDesignationRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Designation::create(Arr::only($input, Designation::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Designation created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Designation');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Designation $designation)
    {
        $wards = Ward::latest()->get();
        $depts = Department::where('ward_id',$designation->ward_id)->latest()->get();
        $class = Clas::latest()->get();

        if ($designation) {

            $wardHtml = '<span>
            <option value="">--Select Ward--</option>';
            foreach ($wards as $ward) :
                $is_select = $ward->id == $designation->ward_id ? "selected" : "";
                $wardHtml .= '<option value="' . $ward->id . '" ' . $is_select . '>' . $ward->name . '</option>';
            endforeach;
            $wardHtml .= '</span>';

            $deptHtml = '<span>
            <option value="">--Select Department--</option>';
            foreach ($depts as $dept) :
                $is_select = $dept->id == $designation->department_id ? "selected" : "";
                $deptHtml .= '<option value="' . $dept->id . '" ' . $is_select . '>' . $dept->name . '</option>';
            endforeach;
            $deptHtml .= '</span>';

            $classHtml = '<span>
            <option value="">--Select Class--</option>';
            foreach ($class as $clas) :
                $is_select = $clas->id == $designation->clas_id ? "selected" : "";
                $classHtml .= '<option value="' . $clas->id . '" ' . $is_select . '>' . $clas->name . '</option>';
            endforeach;
            $classHtml .= '</span>';

            $response = [
                'result' => 1,
                'designation' => $designation,
                'wardHtml' => $wardHtml,
                'deptHtml' => $deptHtml,
                'classHtml' => $classHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDesignationRequest $request, Designation $designation)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $designation->update(Arr::only($input, Designation::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Designation updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Designation');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Designation $designation)
    {
        try {
            DB::beginTransaction();
            $designation->delete();
            DB::commit();
            return response()->json(['success' => 'Designation deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Designation');
        }
    }

    // Fetch Department on basis of ward
    public function fetchDepartment($ward_id)
    {
        $dept_arr = Department::where('ward_id',$ward_id)->get();

        if ($dept_arr) {
            $deptHtml = '<span>
            <option value="">--Select Department--</option>';
            foreach ($dept_arr as $dept) :
                $deptHtml .= '<option value="' . $dept->id . '">' . $dept->name . '</option>';
            endforeach;
            $deptHtml .= '</span>';

            $response = [
                'result' => 1,
                'deptHtml' => $deptHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;

    }


    public function fetchDepartments($ward_id)
    {
        $authUser = Auth::user();

        $dept_arr = Department::where('ward_id',$ward_id)
        ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
            return $query->where('id', $authUser->department_id);
        })->when($authUser->hasRole(['AMC', 'DMC']), function ($query) use ($authUser) {
            return $query->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
        })->get();

        if ($dept_arr) {
            $deptHtml = '
            <option value="">--Select All Department--</option>';
            foreach ($dept_arr as $dept) :
                $deptHtml .= '<option value="' . $dept->id . '">' . $dept->name . '</option>';
            endforeach;

            $response = [
                'result' => 1,
                'deptHtml' => $deptHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;

    }
}
