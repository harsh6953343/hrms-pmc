<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DaBill;
use App\Models\DaBillCalculate;
use Illuminate\Support\Facades\DB;
use App\Models\Ward;
use App\Models\Setting;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Exports\DaDifferenceExport;
use Maatwebsite\Excel\Facades\Excel;

class DADifferanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if(isset($request->bill_no) && $request->bill_no != ""){
            $daBillCalculates = DaBillCalculate::with('employee')->where('da_bill_id', $request->bill_no)
                        ->where('ward_id', $request->ward_id)
                        ->when(isset($request->department_id) && $request->department_id != "", function($q) use($request){
                        $q->where('department_id', $request->department_id);
                        })
                        ->orderBy('emp_code', 'asc')
                        ->orderBy('from_date', 'asc')
                        ->get();
            $daBillCalculates = $daBillCalculates->groupBy('emp_code');

            $daBillDate = DaBill::find($request->bill_no);

            if($request->type == "pdf"){
                $pdf = SnappyPdf::loadView('admin.employee.da-differance-pdf', compact('daBillCalculates', 'daBillDate'))
                    ->setPaper('a4')
                    ->setOrientation('landscape')
                    ->setOption('margin-bottom', 8)
                    ->setOption('margin-top', 5)
                    ->setOption('margin-left', 5)
                    ->setOption('margin-right', 5)
                    ->setOption('footer-right', now()->format('F d, Y h:i A'))
                    ->setOption('footer-left', 'Page: [page] of [toPage]') // Page number format
                    ->setOption('footer-font-size', 8);

                return $pdf->inline('Da difference');
            }else{
                return Excel::download(new DaDifferenceExport($daBillCalculates, $daBillDate), 'da_difference.xlsx');
            }
        }
        $daBills = DaBill::select('id', 'bill_no')->where('status', 1)->latest()->get();

        $wards = Ward::latest()->get();

        return view('admin.employee.da-differance')->with([
            'daBills' => $daBills,
            'wards' => $wards
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $setting = Setting::where('name', 'da_difference')->value('value');
            $request['bill_no'] = $setting;
            $request['bill_date'] = now();
            DaBill::create($request->all());

            Setting::where('name', 'da_difference')->increment('value');
            DB::commit();

            return response()->json(['success' => 'Da Difference sended successfully']);
        }catch(\Exception $e){
            \Log::info($e);
            DB::rollback();
            return response()->json(['error' => 'Something went wrong, please try again!']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            $da = DaBill::find($id);
            $da->status = 0;
            $da->save();
            return response()->json([
                'success' => 'Da Difference updated to pending'
            ]);
        }catch(\Exception $e){
            \Log::info($e);
            return response()->json([
                'error' => 'Something went wrong, please try again!'
            ]);
        }
    }
}
