<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\SuplimentaryEmployeeLeaveBill;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\FreezeAttendance;
use Carbon\Carbon;
use App\Models\EmployeeSalary;

class SupplimentaryLeaveCalculateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $supplimentoryBills = SuplimentaryEmployeeLeaveBill::with('employee.ward', 'suplimentryLeaveBill')
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereHas('employee', function ($q) {
                        $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                    });
                })
                ->latest();

            return DataTables::of($supplimentoryBills)
                ->addIndexColumn()
                ->addColumn('ward', function ($suplimentry) {
                    return $suplimentry?->employee?->ward?->name;
                })
                ->addColumn('employee_name', function ($suplimentry) {
                    return $suplimentry?->employee?->fname . ' ' . $suplimentry?->employee?->mname . ' ' . $suplimentry?->employee?->lname;
                })
                ->addColumn('bill_no', function ($suplimentry) {
                    return $suplimentry?->suplimentryLeaveBill?->bill_no;
                })
                ->addColumn('month', function ($suplimentry) {
                    return \Carbon\Carbon::parse($suplimentry?->month)->format('F');
                })
                ->addColumn('year', function ($suplimentry) {
                    return \Carbon\Carbon::parse($suplimentry?->month)->format('Y');
                })
                ->filterColumn('employee_name', function ($query, $keyword) {
                    $query->whereHas('employee', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("CONCAT(fname, ' ', mname,' ', lname) LIKE ?", ["%$keyword%"]);
                    });
                })
                ->filterColumn('month', function ($query, $keyword) {
                    $query->whereRaw("MONTHNAME(STR_TO_DATE(month, '%M')) LIKE ?", ["%$keyword%"]);
                })
                ->filterColumn('year', function ($query, $keyword) {
                    $query->whereRaw("YEAR(STR_TO_DATE(month, '%Y')) = ?", [$keyword]);
                })
                ->make(true);
        }

        return view('admin.supplimentary-leave-bill-calculate.index');
    }


    public function edit($id)
    {
        $suplimentaryEmployeeLeaveBill = SuplimentaryEmployeeLeaveBill::with(['suplimentryLeaveBill', 'employee'])->find($id);

        $allowances = Allowance::select('id', 'allowance')->get();

        $deductions = Deduction::select('id', 'deduction')->get();

        // $freezeAttendance = FreezeAttendance::where('Emp_Code', $suplimentaryEmployeeLeaveBill->emp_code)->where('month', Carbon::parse($suplimentaryEmployeeLeaveBill->month)->format('m'))->where('financial_year_id', $suplimentaryEmployeeLeaveBill->financial_year_id)->first();

        // $employeeSalary = EmployeeSalary::with(['employee_allowances', 'employee_deductions'])->where('Emp_Code', $suplimentaryEmployeeLeaveBill->emp_code)->first();

        return view('admin.supplimentary-leave-bill-calculate.edit')->with([
            'allowances' => $allowances,
            'deductions' => $deductions,
            'suplimentaryEmployeeLeaveBill' => $suplimentaryEmployeeLeaveBill
        ]);
    }

    public function update(Request $request, $id)
    {
        $suplimentaryEmployeeLeaveBill = SuplimentaryEmployeeLeaveBill::find($id);

        $suplimentaryEmployeeLeaveBill->employee_salary_net_salary = $request->employee_net_salary;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_net_salary = $request->freeze_net_salary;
        $suplimentaryEmployeeLeaveBill->net_salary = $request->suplimentry_net_salary;

        $suplimentaryEmployeeLeaveBill->basic_salary = $request->suplimentryBasicSalary;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_basic_salary = $request->freezeAttendanceBasicSalary;
        $suplimentaryEmployeeLeaveBill->employee_salary_basic_salary = $request->employeeSalaryBasicSalary;


        $suplimentaryEmployeeLeaveBill->employee_total_earning = $request->employee_total_earning;
        $suplimentaryEmployeeLeaveBill->freeze_total_earning = $request->freeze_total_earning;
        $suplimentaryEmployeeLeaveBill->suplimentary_total_earning = $request->suplimentary_total_earning;
        $suplimentaryEmployeeLeaveBill->employee_total_deduction = $request->employee_total_deduction;
        $suplimentaryEmployeeLeaveBill->freeze_total_deduction = $request->freeze_total_deduction;
        $suplimentaryEmployeeLeaveBill->suplimentary_total_deduction = $request->suplimentary_total_deduction;


        $suplimentryAllowanceId = "";
        $suplimentryAllowanceAmount = "";
        $suplimentryAllowanceType = "";

        $freezeAttendanceAllowanceId = "";
        $freezeAttendanceAllowanceAmount = "";
        $freezeAttendanceAllowanceType = "";

        $employeeSalaryAllowanceId = "";
        $employeeSalaryAllowanceAmount = "";
        $employeeSalaryAllowanceType = "";
        if (isset($request->freezeAttendanceEarningDrawn)) {
            for ($i = 0; $i < count($request->freezeAttendanceEarningDrawn); $i++) {
                $suplimentryAllowanceId .= $request->suplimentryAllowanceId[$i] . ',';
                $suplimentryAllowanceAmount .= $request->suplimentryEarningDifference[$i] . ',';
                $suplimentryAllowanceType .= $request->suplimentryAllowancetype[$i] . ',';

                $freezeAttendanceAllowanceId .= $request->freezeAttendanceAllowanceId[$i] . ',';
                $freezeAttendanceAllowanceAmount .= $request->freezeAttendanceEarningDrawn[$i] . ',';
                $freezeAttendanceAllowanceType .= $request->freezeAttendanceAllowancetype[$i] . ',';


                $employeeSalaryAllowanceId .= $request->employeeSalaryAllowanceId[$i] . ',';
                $employeeSalaryAllowanceAmount .= $request->employeeSalaryEarningDue[$i] . ',';
                $employeeSalaryAllowanceType .= $request->employeeSalaryAllowancetype[$i] . ',';
            }
        }

        $suplimentaryEmployeeLeaveBill->allowance_id = $suplimentryAllowanceId;
        $suplimentaryEmployeeLeaveBill->allowance_amount = $suplimentryAllowanceAmount;
        $suplimentaryEmployeeLeaveBill->allowance_type = $suplimentryAllowanceType;

        $suplimentaryEmployeeLeaveBill->freeze_attendance_allowance_id = $freezeAttendanceAllowanceId;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_allowance_amount = $freezeAttendanceAllowanceAmount;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_allowance_type = $freezeAttendanceAllowanceType;

        $suplimentaryEmployeeLeaveBill->employee_salary_allowance_id = $employeeSalaryAllowanceId;
        $suplimentaryEmployeeLeaveBill->employee_salary_allowance_amount = $employeeSalaryAllowanceAmount;
        $suplimentaryEmployeeLeaveBill->employee_salary_allowance_type = $employeeSalaryAllowanceType;


        // for deduction
        $suplimentryDeductionId = "";
        $suplimentryDeductionAmount = "";
        $suplimentryDeductionType = "";

        $freezeAttendanceDeductionId = "";
        $freezeAttendanceDeductionAmount = "";
        $freezeAttendanceDeductionType = "";

        $employeeSalaryDeductionId = "";
        $employeeSalaryDeductionAmount = "";
        $employeeSalaryDeductionType = "";
        if (isset($request->employeeSalaryDeductiontype)) {
            for ($i = 0; $i < count($request->employeeSalaryDeductiontype); $i++) {
                $suplimentryDeductionId .= $request->suplimentryDeductionId[$i] . ',';
                $suplimentryDeductionAmount .= $request->suplimentryDeductionDifference[$i] . ',';
                $suplimentryDeductionType .= $request->suplimentryDeductiontype[$i] . ',';

                $freezeAttendanceDeductionId .= $request->freezeAttendanceDeductionId[$i] . ',';
                $freezeAttendanceDeductionAmount .= $request->freezeAttendanceDeductionDrawn[$i] . ',';
                $freezeAttendanceDeductionType .= $request->freezeAttendanceDeductiontype[$i] . ',';


                $employeeSalaryDeductionId .= $request->employeeSalaryDeductionId[$i] . ',';
                $employeeSalaryDeductionAmount .= $request->employeeSalaryDeductionDue[$i] . ',';
                $employeeSalaryDeductionType .= $request->employeeSalaryDeductiontype[$i] . ',';
            }
        }

        $suplimentaryEmployeeLeaveBill->deduction_id = $suplimentryDeductionId;
        $suplimentaryEmployeeLeaveBill->deduction_amount = $suplimentryDeductionAmount;
        $suplimentaryEmployeeLeaveBill->deduction_type = $suplimentryDeductionType;

        $suplimentaryEmployeeLeaveBill->freeze_attendance_deduction_id = $freezeAttendanceDeductionId;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_deduction_amount = $freezeAttendanceDeductionAmount;
        $suplimentaryEmployeeLeaveBill->freeze_attendance_deduction_type = $freezeAttendanceDeductionType;

        $suplimentaryEmployeeLeaveBill->employee_salary_deduction_id = $employeeSalaryDeductionId;
        $suplimentaryEmployeeLeaveBill->employee_salary_deduction_amount = $employeeSalaryDeductionAmount;
        $suplimentaryEmployeeLeaveBill->employee_salary_deduction_type = $employeeSalaryDeductionType;
        $suplimentaryEmployeeLeaveBill->save();

        return redirect()->route('supplimentary-calculate.index')->with('success', "Supplimentary Calculation updated successfully");
    }
}
