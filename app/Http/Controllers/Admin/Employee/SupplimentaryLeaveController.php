<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SuplimentaryLeaveBill;
use App\Models\SuplimentryEmployeeBill;
use App\Models\SupplimentaryBill;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use Carbon\Carbon;
use App\Models\FinancialYear;
use App\Models\Attendance;
use App\Models\AddEmployeeLeave;
use App\Models\SuplimentaryEmployeeLeaveBill;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\EmployeeSalary;
use App\Models\EmployeeAllowance;
use App\Models\EmployeeDeduction;
use App\Models\Allowance;
use App\Models\FreezeAttendance;

class SupplimentaryLeaveController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $supplimentoryBills = SuplimentaryLeaveBill::with('supplimentaryBill.ward')
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereHas('ward.department', function ($q) {
                        $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                    });
                })
                ->latest();

            return DataTables::of($supplimentoryBills)
                ->addIndexColumn()
                ->addColumn('ward', function ($suplimentry) {
                    return $suplimentry?->supplimentaryBill?->ward?->name;
                })
                ->make(true);
        }

        $supplimentryBills = SupplimentaryBill::select('id', 'bill_no')->where('is_bill_created', 0)->latest()->get();

        $supplimentryLeaveBills = SuplimentaryLeaveBill::select('id', 'bill_no')->latest()->get();

        return view('admin.supplimentary-leave-bill.index')->with([
            'supplimentryBills' => $supplimentryBills,
            'supplimentryLeaveBills' => $supplimentryLeaveBills
        ]);
    }

    public function create(Request $request)
    {
        if (isset($request->billno) && $request->billno) {
            $suplimentry = SupplimentaryBill::where('bill_no', $request->billno)->first();

            $supplimentaryBills = SuplimentryEmployeeBill::with(['employee'])
                ->where('supplimentary_bill_id', $suplimentry->id)
                ->get();

            return view('admin.supplimentary-leave-bill.create')->with([
                'supplimentaryBills' => $supplimentaryBills,
                'suplimentry' => $suplimentry
            ]);
        }
    }

    public function fetchDetails(Request $request)
    {
        if ($request->ajax()) {
            $date = Carbon::parse($request->month . "-01");

            $financialYear = FinancialYear::whereDate('from_date', '<=', $date)
                ->whereDate('to_date', '>=', $date)
                ->value('id');

            $attendance = Attendance::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->select('total_present_days', 'total_absent_days', 'total_leave')
                ->first();

            $earnLeave = AddEmployeeLeave::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->where('leave_type_id', 1)
                ->sum('no_of_days');

            $sickLeave = AddEmployeeLeave::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->where('leave_type_id', 3)
                ->sum('no_of_days');

            $suplimentryEmployeeLeaveBills = SuplimentaryEmployeeLeaveBill::where('month', date('Y-m', strtotime($request->month)))
                ->where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->select('supplimentry_pr', 'supplimentry_ul', 'supplimentry_ml', 'supplimentry_el')
                ->get();

            $totalBillLeave = 0;
            foreach ($suplimentryEmployeeLeaveBills as $sup) {
                $pr = $sup->supplimentry_pr ?? 0;
                $ul = $sup->supplimentry_ul ?? 0;
                $ml = $sup->supplimentry_ml ?? 0;
                $el = $sup->supplimentry_el ?? 0;
                $totalBillLeave = $totalBillLeave + $pr + $ul + $ml + $el;
            }

            return response()->json([
                'attendance' => $attendance,
                'earnLeave' => $earnLeave,
                'sickLeave' => $sickLeave,
                'row' => $request->row,
                'suplimentryEmployeeLeaveBill' => $totalBillLeave
            ]);
        }
    }

    public function fetchUpdateDetails(Request $request)
    {
        if ($request->ajax()) {
            $date = Carbon::parse($request->month . "-01");

            $financialYear = FinancialYear::whereDate('from_date', '<=', $date)
                ->whereDate('to_date', '>=', $date)
                ->value('id');

            $attendance = Attendance::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->select('total_present_days', 'total_absent_days', 'total_leave')
                ->first();

            $earnLeave = AddEmployeeLeave::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->where('leave_type_id', 1)
                ->sum('no_of_days');

            $sickLeave = AddEmployeeLeave::where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('month', date('m', strtotime($request->month)))
                ->where('leave_type_id', 3)
                ->sum('no_of_days');

            $suplimentryEmployeeLeaveBills = SuplimentaryEmployeeLeaveBill::where('month', date('Y-m', strtotime($request->month)))
                ->where('financial_year_id', $financialYear)
                ->where('employee_id', $request->employeeId)
                ->where('suplimentry_leave_id', '!=', $request->suplimentry_leave_id)
                ->select('supplimentry_pr', 'supplimentry_ul', 'supplimentry_ml', 'supplimentry_el')
                ->get();

            $totalBillLeave = 0;
            foreach ($suplimentryEmployeeLeaveBills as $sup) {
                $pr = $sup->supplimentry_pr ?? 0;
                $ul = $sup->supplimentry_ul ?? 0;
                $ml = $sup->supplimentry_ml ?? 0;
                $el = $sup->supplimentry_el ?? 0;
                $totalBillLeave = $totalBillLeave + $pr + $ul + $ml + $el;
            }

            return response()->json([
                'attendance' => $attendance,
                'earnLeave' => $earnLeave,
                'sickLeave' => $sickLeave,
                'row' => $request->row,
                'suplimentryEmployeeLeaveBill' => $totalBillLeave
            ]);
        }
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $suplimentaryLeaveBill = SuplimentaryLeaveBill::create([
                    'supplimentary_bill_id' => $request->supplimentary_bill_id,
                    'bill_no' => $request->bill_no,
                    'bill_date' => date('Y-m-d', strtotime($request->bill_date)),
                    'bill_description' => $request->bill_description,
                    'payment_status' => $request->payment_status,
                    'cheque_no' => $request->cheque_no,
                    'cheque_date' => date('Y-m-d', strtotime($request->cheque_date))
                ]);

                if ($request->has('month') && !empty($request->month)) {

                    $suplimentryLeaveId = $suplimentaryLeaveBill->id;

                    foreach ($request->month as $i => $month) {
                        $empCode = Employee::where('id', $request->employee_id[$i])->value('employee_id');

                        $date = Carbon::parse($month . "-01");

                        $financialYear = FinancialYear::whereDate('from_date', '<=', $date)
                            ->whereDate('to_date', '>=', $date)
                            ->value('id');

                        SuplimentaryEmployeeLeaveBill::create([
                            'financial_year_id' => $financialYear,
                            'suplimentry_leave_id' => $suplimentryLeaveId,
                            'employee_id' => $request->employee_id[$i],
                            'emp_code' => $empCode,
                            'month' => $month,
                            'attandance_pr' => $request->pr[$i],
                            'attandance_ul' => $request->ul[$i],
                            'attandance_ml' => $request->ml[$i],
                            'attandance_el' => $request->el[$i],
                            'supplimentry_pr' => $request->pr1[$i],
                            'supplimentry_ul' => $request->ul1[$i],
                            'supplimentry_ml' => $request->ml1[$i],
                            'supplimentry_el' => $request->el1[$i]
                        ]);
                    }
                }

                SupplimentaryBill::where('id', $request->supplimentary_bill_id)->update([
                    'is_bill_created' => 1
                ]);

                DB::commit();
                return response()->json(['success' => 'Supplimentary Leave Bill Created Successffully!']);
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
            }
        }
    }

    public function edit(Request $request, $id)
    {
        $suplimentry = SuplimentaryLeaveBill::where('id', $id)->first();

        $supplimentaryLeaveBills = SuplimentaryEmployeeLeaveBill::with(['employee'])
            ->where('suplimentry_leave_id', $suplimentry->id)
            ->get();

        $supplimentaryBills = SuplimentryEmployeeBill::with(['employee'])
            ->where('supplimentary_bill_id', $suplimentry->supplimentary_bill_id)
            ->get();
        // return $supplimentaryLeaveBills;

        return view('admin.supplimentary-leave-bill.edit')->with([
            'supplimentaryBills' => $supplimentaryBills,
            'suplimentry' => $suplimentry,
            'supplimentaryLeaveBills' => $supplimentaryLeaveBills
        ]);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $suplimentaryLeaveBill = SuplimentaryLeaveBill::find($request->supplimentary_leave_id);
                $suplimentaryLeaveBill->update([
                    'supplimentary_bill_id' => $request->supplimentary_bill_id,
                    'bill_no' => $request->bill_no,
                    'bill_date' => date('Y-m-d', strtotime($request->bill_date)),
                    'bill_description' => $request->bill_description,
                    'payment_status' => $request->payment_status,
                    'cheque_no' => $request->cheque_no,
                    'cheque_date' => date('Y-m-d', strtotime($request->cheque_date))
                ]);

                $suplimentryLeaveId = $request->supplimentary_leave_id;
                if ($request->has('month') && !empty($request->month)) {
                    SuplimentaryEmployeeLeaveBill::where('suplimentry_leave_id', $suplimentryLeaveId)->delete();
                    foreach ($request->month as $i => $month) {

                        $empCode = Employee::where('id', $request->employee_id[$i])->value('employee_id');

                        $date = Carbon::parse($month . "-01");

                        $financialYear = FinancialYear::whereDate('from_date', '<=', $date)
                            ->whereDate('to_date', '>=', $date)
                            ->value('id');

                        SuplimentaryEmployeeLeaveBill::create([
                            'financial_year_id' => $financialYear,
                            'suplimentry_leave_id' => $suplimentryLeaveId,
                            'employee_id' => $request->employee_id[$i],
                            'emp_code' => $empCode,
                            'month' => $month,
                            'attandance_pr' => $request->pr[$i],
                            'attandance_ul' => $request->ul[$i],
                            'attandance_ml' => $request->ml[$i],
                            'attandance_el' => $request->el[$i],
                            'supplimentry_pr' => $request->pr1[$i],
                            'supplimentry_ul' => $request->ul1[$i],
                            'supplimentry_ml' => $request->ml1[$i],
                            'supplimentry_el' => $request->el1[$i]
                        ]);
                    }
                }

                DB::commit();
                return response()->json(['success' => 'Supplimentary Leave Bill Updated Successffully!']);
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
            }
        }
    }

    public function show(Request $request, $id)
    {
        $suplimentry = SuplimentaryLeaveBill::where('id', $id)->first();

        $supplimentaryLeaveBills = SuplimentaryEmployeeLeaveBill::with(['employee'])
            ->where('suplimentry_leave_id', $suplimentry->id)
            ->get();


        return view('admin.supplimentary-leave-bill.show')->with([
            'suplimentry' => $suplimentry,
            'supplimentaryLeaveBills' => $supplimentaryLeaveBills
        ]);
    }

    public function storeGenerate(Request $request)
    {
        try {
            DB::beginTransaction();
            $suplimentryEmpLeaves = SuplimentaryEmployeeLeaveBill::where('suplimentry_leave_id', $request->suplimentry_leave_id)
                ->get();

            foreach ($suplimentryEmpLeaves as $suplimentryEmpLeave) {
                // calculate no of days
                $noOfDays = $suplimentryEmpLeave->supplimentry_pr + $suplimentryEmpLeave->supplimentry_ul + $suplimentryEmpLeave->supplimentry_ml + $suplimentryEmpLeave->supplimentry_el;
                $suplimentaryTotalEarning = 0;
                $suplimentaryTotalDeduction = 0;

                // to get finalcial year id
                $date = $suplimentryEmpLeave->month . '-01';
                $financial_year = FinancialYear::whereDate('from_date', "<=", $date)
                    ->whereDate('to_date', ">=", $date)
                    ->value('id');

                $input['financial_year_id'] = $financial_year;

                // to get employee current basic salary
                $employeeSalary = EmployeeSalary::where('Emp_Code', $suplimentryEmpLeave->emp_code)
                    ->select('basic_salary', 'grade_pay')
                    ->first();

                $netSalary = 0;
                $date = Carbon::createFromDate(date('Y', strtotime($suplimentryEmpLeave->month)), date('m', strtotime($suplimentryEmpLeave->month)), 1);
                $daysInMonth = $date->daysInMonth;

                $basicSalary = ($employeeSalary->basic_salary / $daysInMonth) * $noOfDays; //to get basic salary of no of days
                $netSalary = $netSalary + $basicSalary;

                $suplimentaryTotalEarning = $suplimentaryTotalEarning + $basicSalary;
                $input['basic_salary'] = $basicSalary;
                $input['net_salary'] = $netSalary;

                // to get employee allowance
                $employeeAllowances = EmployeeAllowance::where('Emp_Code', $suplimentryEmpLeave->emp_code)->get();

                $allowanceId = "";
                $allowanceAmount = "";
                $allowanceType = "";
                foreach ($employeeAllowances as $employeeAllowance) {
                    if ($employeeAllowance->allowance_id == "6") {
                        continue;
                    }
                    $allowanceActualAmount = 0;
                    if ($employeeAllowance->allowance_type == 2) {
                        $allowanceId .= $employeeAllowance->allowance_id . ',';
                        $allowanceActualAmount = round(((($employeeSalary->basic_salary * $employeeAllowance->allowance_amt) / 100) / $daysInMonth) *  $noOfDays);
                        $allowanceAmount .=  $allowanceActualAmount . ',';
                        $allowanceType .= $employeeAllowance->allowance_type . ',';
                    } else {
                        $allowanceId .= $employeeAllowance->allowance_id . ',';
                        $allowanceActualAmount = round(($employeeAllowance->allowance_amt / $daysInMonth) *  $noOfDays);
                        $allowanceAmount .=  $allowanceActualAmount . ',';
                        $allowanceType .= $employeeAllowance->allowance_type . ',';
                    }
                    $netSalary = $netSalary + $allowanceActualAmount;
                    $suplimentaryTotalEarning = $suplimentaryTotalEarning + $allowanceActualAmount;
                }


                $employee = Employee::where('id', $suplimentryEmpLeave->employee_id)
                    ->where('employee_id', $suplimentryEmpLeave->emp_code)
                    ->select('doj', 'department_id')
                    ->first();

                $daAllowance = Allowance::where('id', 1)->select('type', 'amount')->first();
                if ($daAllowance->type == "2") {
                    $da = ($basicSalary * $daAllowance->amount) / 100;
                } else {
                    $da = ($daAllowance->amount / $daysInMonth) * $request->no_of_days;
                }
                $input['da'] = $da;
                $deductionId = "";
                $deductionAmount = "";
                $deductionType = "";
                if ($employee->doj > '2005-11-01' && $employee->department_id != 3) {
                    $dcpAmount = $basicSalary + $da;
                    $allowanceDcpCorporation = round((($dcpAmount * 14) / 100), 2);
                    $deductionDcpCorporation = round((($dcpAmount * 14) / 100), 2);
                    $deductionDcpEmployee = round((($dcpAmount * 10) / 100), 2);


                    $netSalary = $netSalary + $allowanceDcpCorporation - $deductionDcpCorporation - $deductionDcpEmployee;


                    $allowanceId .=  "6,";
                    $allowanceAmount .= $allowanceDcpCorporation . ",";
                    $allowanceType .= "1,";
                    $suplimentaryTotalEarning = $suplimentaryTotalEarning + $allowanceDcpCorporation;

                    $deductionId .= "7,";
                    $deductionAmount .= $deductionDcpEmployee . ",";
                    $deductionType .= "1,";
                    $suplimentaryTotalDeduction = $suplimentaryTotalDeduction + $deductionDcpEmployee;

                    $deductionId .= "8,";
                    $deductionAmount .= $deductionDcpCorporation . ",";
                    $deductionType .= "1,";
                    $suplimentaryTotalDeduction = $suplimentaryTotalDeduction + $deductionDcpCorporation;
                }


                $input['allowance_id'] = rtrim($allowanceId, ',');
                $input['allowance_amount'] = rtrim($allowanceAmount, ',');
                $input['allowance_type'] = rtrim($allowanceType, ',');


                $employeeDeductions = EmployeeDeduction::where('Emp_Code', $suplimentryEmpLeave->emp_code)->get();
                // end of calculation of salary, allowance and deduction

                foreach ($employeeDeductions as $employeeDeduction) {
                    if ($employeeDeduction->deduction_id == "8" || $employeeDeduction->deduction_id == "7") {
                        continue;
                    }
                    $deductionActualAmount = 0;
                    if ($employeeDeduction->deduction_type == "2") {
                        $deductionId .= $employeeDeduction->deduction_id . ',';
                        $deductionActualAmount = round(((($employeeSalary->basic_salary * $employeeDeduction->deduction_amt) / 100) / $daysInMonth) *  $noOfDays);
                        $deductionAmount .=  0 . ',';
                        $deductionType .= $employeeDeduction->deduction_type . ',';
                    } else {
                        $deductionId .= $employeeDeduction->deduction_id . ',';
                        $deductionActualAmount = round(($employeeDeduction->deduction_amt / $daysInMonth) *  $noOfDays);
                        $deductionAmount .=  0 . ',';
                        $deductionType .= $employeeDeduction->deduction_type . ',';
                    }
                    $netSalary = $netSalary + 0;
                    $suplimentaryTotalDeduction = $suplimentaryTotalDeduction + 0;
                }

                $input['suplimentary_total_earning'] = $suplimentaryTotalEarning;
                $input['suplimentary_total_deduction'] = $suplimentaryTotalDeduction;


                $input['deduction_id'] = rtrim($deductionId, ',');
                $input['deduction_amount'] = rtrim($deductionAmount, ',');
                $input['deduction_type'] = rtrim($deductionType, ',');
                $input['net_salary'] = $netSalary;


                // for freeze Attendance
                $freezeAttendance = FreezeAttendance::where('Emp_Code', $suplimentryEmpLeave->emp_code)
                    ->where('month', date('m', strtotime($suplimentryEmpLeave->month)))->first();

                $freezeTotalEarning = 0;
                $freezeTotalDeduction = 0;

                $input['freeze_attendance_id'] = $freezeAttendance->id;
                $input['freeze_attendance_basic_salary'] = $freezeAttendance->basic_salary;
                $input['freeze_attendance_present_day'] = $freezeAttendance->present_day;
                $input['freeze_attendance_net_salary'] = $freezeAttendance->net_salary;
                $allowanceId = $freezeAttendance->allowance_Id;
                $allowanceAmount = $freezeAttendance->allowance_Amt;
                $allowanceType = $freezeAttendance->allowance_Type;
                $freezeTotalEarning = $freezeTotalEarning + $freezeAttendance->basic_salary;

                $explodeArr = explode(',', $freezeAttendance->allowance_Amt);
                for ($i = 0; $i < count($explodeArr); $i++) {
                    if (isset($explodeArr[$i]) && $explodeArr[$i] > 0) {
                        $freezeTotalEarning = $freezeTotalEarning + $explodeArr[$i];
                    }
                }

                if ($freezeAttendance->corporation_share_da && $freezeAttendance->corporation_share_da > 0) {
                    $allowanceId .=  ",6";
                    $allowanceAmount .= ',' . $freezeAttendance->corporation_share_da;
                    $allowanceType .= ",1";
                    $freezeTotalEarning = $freezeTotalEarning + $freezeAttendance->corporation_share_da;
                }


                $input['freeze_attendance_allowance_id'] = $allowanceId;
                $input['freeze_attendance_allowance_amount'] = $allowanceAmount;
                $input['freeze_attendance_allowance_type'] = $allowanceType;


                $deductionId = $freezeAttendance->deduction_Id;
                $deductionAmount = $freezeAttendance->deduction_Amt;
                $deductionType = $freezeAttendance->deduction_Type;
                $deductionArr = explode(',', $freezeAttendance->deduction_Amt);
                for ($i = 0; $i < count($deductionArr); $i++) {
                    if (isset($deductionArr[$i]) && $deductionArr[$i] > 0) {
                        $freezeTotalDeduction = $freezeTotalDeduction + $deductionArr[$i];
                    }
                }

                if ($freezeAttendance->festival_deduction_amt && $freezeAttendance->festival_deduction_amt > 0) {
                    $deductionId .= "," . $freezeAttendance->festival_deduction_id;
                    $deductionAmount .= "," . $freezeAttendance->festival_deduction_amt;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->festival_deduction_amt;
                }

                if ($freezeAttendance->corporation_share_da && $freezeAttendance->corporation_share_da > 0) {
                    $deductionId .= ",8";
                    $deductionAmount .= ',' . $freezeAttendance->corporation_share_da;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->corporation_share_da;
                }

                if ($freezeAttendance->stamp_duty && $freezeAttendance->stamp_duty > 0) {
                    $deductionId .= ",22";
                    $deductionAmount .= ',' . $freezeAttendance->stamp_duty;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->stamp_duty;
                }

                if ($freezeAttendance->corporation_share_da && $freezeAttendance->employee_share_da > 0) {
                    $deductionId .= ",7";
                    $deductionAmount .= ',' . $freezeAttendance->employee_share_da;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->employee_share_da;
                }

                if ($freezeAttendance->lic_deduction_amt && $freezeAttendance->lic_deduction_amt > 0) {
                    $deductionId .= "," . $freezeAttendance->lic_deduction_id;
                    $deductionAmount .= ',' . $freezeAttendance->lic_deduction_amt;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->lic_deduction_amt;
                }

                if ($freezeAttendance->loan_deduction_amt && $freezeAttendance->loan_deduction_amt > 0) {
                    $deductionId .= "," . $freezeAttendance->loan_deduction_id;
                    $deductionAmount .= ',' . $freezeAttendance->loan_deduction_amt;
                    $deductionType .= ",1";
                    $freezeTotalDeduction = $freezeTotalDeduction + $freezeAttendance->loan_deduction_amt;
                }

                $input['freeze_total_earning'] = $freezeTotalEarning;
                $input['freeze_total_deduction'] = $freezeTotalDeduction;

                $input['freeze_attendance_deduction_id'] = $deductionId;
                $input['freeze_attendance_deduction_amount'] = $deductionAmount;
                $input['freeze_attendance_deduction_type'] = $deductionType;


                // for employee salary structure
                $employeeNetSalary = 0;
                $employeeSalary = EmployeeSalary::with('employee_allowances', 'employee_deductions', 'employee')->where('Emp_Code', $suplimentryEmpLeave->emp_code)->first();

                $employeeTotalEarning = 0;
                $employeeTotalDeduction = 0;
                $input['employee_salary_id'] = $employeeSalary->id;
                $input['employee_salary_basic_salary'] = $employeeSalary->basic_salary;
                $input['employee_salary_present_day'] = $freezeAttendance->present_day;
                $employeeNetSalary = $employeeNetSalary + $employeeSalary->basic_salary;
                $employeeTotalEarning = $employeeTotalEarning + $employeeSalary->basic_salary;


                $allowanceId = "";
                $allowanceAmount = "";
                $allowanceType = "";
                $employeeDaAllowance = 0;
                foreach ($employeeSalary->employee_allowances as $allowance) {
                    if ($allowance->allowance_id == "6") {
                        continue;
                    }
                    if ($allowance->allowance_type == "2") {
                        $calculatedAllowanceAmount = (($employeeSalary->basic_salary * $allowance->allowance_amt) / 100);
                        $allowanceAmount .=  $calculatedAllowanceAmount . ",";
                    } else {
                        $calculatedAllowanceAmount = $allowance->allowance_amt;
                        $allowanceAmount .= $allowance->allowance_amt . ",";
                    }
                    $allowanceId .= $allowance->allowance_id . ",";
                    $allowanceType .= $allowance->allowance_type . ",";

                    if ($allowance->allowance_id == "1") {
                        $employeeDaAllowance = $calculatedAllowanceAmount;
                    }
                    $employeeNetSalary = $employeeNetSalary + $calculatedAllowanceAmount;

                    $employeeTotalEarning = $employeeTotalEarning + $calculatedAllowanceAmount;
                }
                $input['employee_salary_da'] = $employeeDaAllowance;

                $deductionId = "";
                $deductionAmount = "";
                $deductionType = "";

                if ($employeeSalary?->employee?->doj > '2005-11-01' && $employeeSalary?->employee?->department_id != 3) {
                    $dcpAmount = $employeeSalary->basic_salary + $employeeDaAllowance;
                    $allowanceDcpCorporation = round((($dcpAmount * 14) / 100), 2);
                    $deductionDcpCorporation = round((($dcpAmount * 14) / 100), 2);
                    $deductionDcpEmployee = round((($dcpAmount * 10) / 100), 2);

                    $employeeNetSalary = $employeeNetSalary + $allowanceDcpCorporation - $deductionDcpCorporation - $deductionDcpEmployee;

                    $allowanceId .=  "6,";
                    $allowanceAmount .= $allowanceDcpCorporation . ",";
                    $allowanceType .= "1,";

                    $employeeTotalEarning = $employeeTotalEarning + $allowanceDcpCorporation;

                    $deductionId .= "7,";
                    $deductionAmount .= $deductionDcpEmployee . ",";
                    $deductionType .= "1,";
                    $employeeTotalDeduction = $employeeTotalDeduction + $deductionDcpEmployee;

                    $deductionId .= "8,";
                    $deductionAmount .= $deductionDcpCorporation . ",";
                    $deductionType .= "1,";
                    $employeeTotalDeduction = $employeeTotalDeduction + $deductionDcpCorporation;
                }

                $deductionId .= "22,";
                $deductionAmount .= "1,";
                $deductionType .= "1,";
                $employeeTotalDeduction = $employeeTotalDeduction + 1;

                $input['employee_salary_allowance_id'] = rtrim($allowanceId, ',');
                $input['employee_salary_allowance_amount'] = rtrim($allowanceAmount, ',');
                $input['employee_salary_allowance_type'] = rtrim($allowanceType, ',');




                foreach ($employeeSalary->employee_deductions as $deduction) {
                    if ($deduction->deduction_id == "8" || $deduction->deduction_id == "7") {
                        continue;
                    }
                    if ($deduction->deduction_type == "2") {
                        $deductionAmountCalculate = (($employeeSalary->basic_salary * $deduction->deduction_amt) / 100);
                        $deductionAmount .=  $deductionAmountCalculate . ",";
                    } else {
                        $deductionAmountCalculate = $deduction->deduction_amt;
                        $deductionAmount .=  $deductionAmountCalculate . ",";
                    }
                    $deductionId .= $deduction->deduction_id . ",";
                    $deductionType .= $deduction->deduction_type . ",";
                    $employeeNetSalary = $employeeNetSalary - $deductionAmountCalculate;

                    $employeeTotalDeduction = $employeeTotalDeduction + $deductionAmountCalculate;
                }

                $input['employee_total_earning'] = $employeeTotalEarning;
                $input['employee_total_deduction'] = $employeeTotalDeduction;

                $input['employee_salary_deduction_id'] = rtrim($deductionId, ',');
                $input['employee_salary_deduction_amount'] = rtrim($deductionAmount, ',');
                $input['employee_salary_deduction_type'] = rtrim($deductionType, ',');
                $input['employee_salary_net_salary'] = $employeeNetSalary;

                $suplimentryEmpLeave->update($input);
            }

            SuplimentaryLeaveBill::where('id', $request->suplimentry_leave_id)->update(['is_generate' => 1]);

            DB::commit();

            return redirect()->back()->with('success', 'Supplimentary Leave Bill generated successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', 'This employee salary is not generated.');
        }
    }
}
