<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreSalaryStructureRequest;
use App\Http\Requests\Admin\Employee\UpdateSalaryStructureRequest;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\EmployeeSalary;
use App\Models\PayScale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Repositories\EmployeeSalaryStructure;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;

class EmployeeSalaryController extends Controller
{

    protected $employeeSalaryStructureRepository;
    public function __construct()
    {
        $this->employeeSalaryStructureRepository = new EmployeeSalaryStructure;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_salarys = EmployeeSalary::with('employee', 'pay_scale')
            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('ward_id', $authUser->ward_id);
                });
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->latest()->get();

        return view('admin.employee.employee-salary')->with(['employee_salarys' => $employee_salarys]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $employees = Employee::latest()->get();

        $pay_scales = PayScale::latest()->get();

        $allowances = Allowance::latest()->get();

        $deductions = Deduction::latest()->get();

        return view('admin.employee.add-employee-salary')->with(['employees' => $employees, 'pay_scales' => $pay_scales, 'allowances' => $allowances, 'deductions' => $deductions]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSalaryStructureRequest $request)
    {
        try {
            $this->employeeSalaryStructureRepository->store($request->validated());
            return response()->json(['success' => 'Salary created successfully!']);
        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'adding', 'Employee');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeSalary $employeeSalary)
    {
        if ($employeeSalary) {
            $data = $this->employeeSalaryStructureRepository->editEmployeeSalary($employeeSalary);

            return view('admin.employee.edit-employee-salary', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSalaryStructureRequest $request, EmployeeSalary $employeeSalary)
    {
        try {
            $this->employeeSalaryStructureRepository->updateEmployeeSalary($request->validated(), $employeeSalary);
            return response()->json(['success' => 'Salary updated successfully!']);
        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Salary');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EmployeeSalary $employeeSalary)
    {
        try {
            DB::beginTransaction();
            $employeeSalary->delete();
            DB::commit();
            return response()->json(['success' => 'Salary deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Salary');
        }
    }

    public function fetchpayScaleDetails($pay_scale_id)
    {

        $pay_scale_details = PayScale::find($pay_scale_id);

        if ($pay_scale_details) {
            $response = [
                'result' => 1,
                'pay_scale_details' => $pay_scale_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }
}
