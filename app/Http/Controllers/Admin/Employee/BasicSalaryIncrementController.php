<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreIncrementBasicSalaryRequest;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeSalary;
use App\Models\IncrementBasicSalary;
use App\Models\OldEmployeeSalary;
use App\Models\PayScale;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;

class BasicSalaryIncrementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $departments = Department::latest()
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })->get();

        return view('admin.employee.basic-salary-increment')->with(['departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreIncrementBasicSalaryRequest $request)
    {
        $authUser = Auth::user();

        try {
            DB::beginTransaction();
            $input = $request->validated();

            if ($input['department_id'] == 'all') {
                $input['department_id'] = 0;
            } else {
                $input['department_id'] = $input['department_id'];
            }

            IncrementBasicSalary::create(Arr::only($input, IncrementBasicSalary::getFillables()));

            $emp_ids = $input['emp_id'];
            $salary_unique_ids = $input['salary_unique_id'];
            foreach ($emp_ids as $key => $emp_id) {

                $employee_salary = EmployeeSalary::where('id', $salary_unique_ids[$key]);

                $old_employee_salary = OldEmployeeSalary::with('employee')->where('employee_id', $emp_id)->latest()->first();

                $employee_salary->update([
                    'basic_salary' => $input['incrementted_basic'][$key],
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                ]);

                $old_employee_salary->update([
                    'end_date' => date('Y-m-d'),
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                ]);

                $old_employee_salary_create = [
                    'employee_id' => $emp_id,
                    'Emp_Code' => $old_employee_salary->employee->employee_id,
                    'pay_scale_id' => $old_employee_salary->pay_scale_id,
                    'basic_salary' => $input['incrementted_basic'][$key],
                    'grade_pay' => $old_employee_salary->grade_pay,
                    'applicable_date' => date('Y-m-d')
                ];

                OldEmployeeSalary::create($old_employee_salary_create);
            }

            DB::commit();

            return response()->json(['success' => 'Basic Salary Increment successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Basic Salary');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    // Get Incremented Salary
    public function getIncrementedBasicSalary(Request $request)
    {
        try {
            $departmentId = $request->input('department_id');
            $month = $request->input('month');

            $get_employees = Employee::with('department')
                ->when($departmentId != "all", function ($q) use ($departmentId) {
                    $q->where('department_id', $departmentId);
                })
                ->where('increment_month', $month)
                ->latest()
                ->get();

            $incrementArr = [];

            foreach ($get_employees as $key => $employee) {
                $employeeSalary = EmployeeSalary::with('pay_scale')->where('employee_id', $employee->id)->first();

                if ($employeeSalary) {
                    $get_next_slab = PayScale::where('grade_amp', $employeeSalary->pay_scale?->grade_amp)
                        ->where('grade_pay_name', $employeeSalary->pay_scale?->grade_pay_name)
                        ->where('pay_band', $employeeSalary->pay_scale?->pay_band)
                        ->where('pay_band_scale', $employeeSalary->pay_scale?->pay_band_scale)
                        ->where('amount', '>', $employeeSalary?->basic_salary)
                        ->first();

                    if ($get_next_slab) {
                        $incrementArr[] = [
                            'sr_no'                 =>  $key + 1,
                            'employee_uniques_id'   =>  $employee->id,
                            'salary_unique_id'      =>  $employeeSalary->id,
                            'emp_id'                =>  $employee->employee_id,
                            'emp_name'              =>  $employee->fname . " " . $employee->mname . " " . $employee->lname,
                            'department'            =>  $employee->department?->name,
                            'current_basic_salary'  =>  $employeeSalary->basic_salary,
                            'increment_basic_salary' => $get_next_slab->amount,
                        ];
                    }
                }
            }
            $response = [
                'result' => 1,
                'incrementArr' => $incrementArr,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            // Handle exceptions or errors here
            return response()->json(['result' => 0, 'error' => $e->getMessage()]);
        }
    }
}
