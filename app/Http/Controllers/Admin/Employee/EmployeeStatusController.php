<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Employee\StoreEmployeeStatusRequest;
use App\Http\Requests\Admin\Employee\UpdateEmployeeStatusRequest;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;

class EmployeeStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $emp_statuses = EmployeeStatus::with('status')
            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('ward_id', $authUser->ward_id);
                });
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('department_id', $authUser->department_id);
                });
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->latest()
            ->get();

        return view('admin.employee.employee-status')->with(['emp_statuses' => $emp_statuses]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $statuses = Status::latest()->get();

        return view('admin.employee.add-employee-status')->with(['statuses' => $statuses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeStatusRequest $request)
    {
        try {
            DB::beginTransaction();

            $input = $request->validated();

            // Check if the status already exists for the given employee ID
            $existingStatus = EmployeeStatus::where('employee_id', $input['employee_id'])
                // ->where('status_id', $input['status_id'])
                ->first();

            if ($existingStatus) {
                return response()->json(['error2' => 'Employee Status already exists for this employee.']);
            }

            // If the status doesn't exist, create a new record
            EmployeeStatus::create(Arr::only($input, EmployeeStatus::getFillables()));

            Employee::where('id', $input['employee_id'])
                ->update(['status_id' => $input['status_id'], 'retirement_date' => $input['applicable_date']]);

            DB::commit();

            return response()->json(['success' => 'Employee Status added successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Employee Status');
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeStatus $employeeStatus)
    {
        $statuses = Status::latest()->get();
        $employeeStatus->load('employee.ward', 'employee.designation', 'employee.class', 'employee.department');

        return view('admin.employee.edit-employee-status')->with(['statuses' => $statuses, 'employeeStatus' => $employeeStatus]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmployeeStatusRequest $request, EmployeeStatus $employeeStatus)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $employeeStatus->update(Arr::only($input, EmployeeStatus::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Employee status updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Employee status');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EmployeeStatus $employeeStatus)
    {
        try {
            DB::beginTransaction();
            $employeeStatus->employee()->update(['status_id' => null]);
            $employeeStatus->delete();
            DB::commit();
            return response()->json(['success' => 'Employee status deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Employee status');
        }
    }

    public function fetchEmployeeDetails($emp_id)
    {
        $authUser = Auth::user();
        $employee_details = Employee::with('ward', 'department', 'designation', 'class', 'salary')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->select('*')
            ->whereNotExists(function ($query) use ($emp_id) {
                $query->select(DB::raw(1))
                    ->from('employee_statuses')
                    ->whereColumn('employee_statuses.employee_id', 'employees.id')
                    ->where('employee_statuses.Emp_Code', $emp_id)
                    ->whereNull('employee_statuses.deleted_at');
            })
            ->first();

        if ($employee_details) {
            $response = [
                'result' => 1,
                'employee_details' => $employee_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function fetchPensionEmployeeDetails($emp_id)
    {
        $authUser = Auth::user();
        $employee_details = Employee::with('ward', 'department', 'designation', 'class', 'salary')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->first();

        if ($employee_details) {
            $response = [
                'result' => 1,
                'employee_details' => $employee_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }
}
