<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LeaveType;
use App\Models\AddEmployeeLeave;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DataTables;


class SupplimentaryLeaveCountReportController extends Controller
{
    public function index(Request $request)
    {
        $leaveType = LeaveType::latest()->get();

        return view('admin.supplimentary-leave-count.supplimentary_leave_count')->with([
            'leaveTypes' => $leaveType
        ]);
    }

    public function list(Request $request)
    {
        if ($request->ajax()) {
            $authUser = Auth::user();

            $employeeLeave = AddEmployeeLeave::with('employee.department', 'leaveType')
                ->whereHas('employee', function ($query) use ($authUser) {
                    $query->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                        return $query->where('ward_id', $authUser->ward_id);
                    })
                        ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                            return $query->where('department_id', $authUser->department_id);
                        });
                })
                ->where('status', 1)
                ->when(function ($query) {
                        return $query->whereIn('leave_type_id', ['2', '3', '4', '5'])->exists();
                    },
                    function ($query) {
                        $query->whereRaw('DAY(amc_datetime) > ?', [21]);
                    })
                ->when(function ($query) {
                        return $query->whereIn('leave_type_id', ['1', '6'])->exists();
                    },
                    function ($query) {
                        $query->whereRaw('DAY(dmc_datetime) > ?', [21]);
                    })
                ->latest();
                
            return DataTables::of($employeeLeave)
                ->addIndexColumn()
                ->addColumn('full_name', function ($data) {
                    return $data?->employee->fname . ' ' . $data?->employee->mname . ' ' . $data?->employee->lname;
                })
                ->filterColumn('full_name', function ($query, $keyword) {
                    $query->whereHas('employee', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("CONCAT(fname, ' ', mname,' ', lname) LIKE ?", ["%$keyword%"]);
                    });
                })
                ->addColumn('department_name', function ($data) {
                    return $data?->employee?->department?->name;
                })
                ->filterColumn('department_name', function ($query, $keyword) {
                    $query->whereHas('employee.department', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->addColumn('leave_type_name', function ($data) {
                    return $data?->leaveType?->name;
                })
                ->filterColumn('leave_type_name', function ($query, $keyword) {
                    $query->whereHas('leaveType', function ($subQuery) use ($keyword) {
                        $subQuery->whereRaw("name LIKE ?", ["%$keyword%"]);
                    });
                })
                ->editColumn('apply_date', function ($data) {
                    return ($data?->created_at) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->created_at)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('apply_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('amc_datetime', function ($data) {
                    return ($data?->amc_datetime) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->amc_datetime)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('amc_datetime', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('dmc_datetime', function ($data) {
                    return ($data?->dmc_datetime) ? Carbon::createFromFormat('Y-m-d H:i:s', $data?->dmc_datetime)->format('d-m-Y h:i A') : '-';
                })
                ->filterColumn('dmc_datetime', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y %h:%i %A') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('from_date', function ($data) {
                    return Carbon::createFromFormat('Y-m-d', $data?->from_date)->format('d-m-Y');
                })
                ->filterColumn('from_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(from_date,'%d-%m-%Y') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('to_date', function ($data) {
                    return Carbon::createFromFormat('Y-m-d', $data?->to_date)->format('d-m-Y');
                })
                ->filterColumn('to_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(to_date,'%d-%m-%Y') LIKE ?", ["%$keyword%"]);
                })
                ->editColumn('leave_file', function ($data) {
                    return asset('storage/' . $data?->leave_file);
                })
                ->editColumn('amc_status', function ($data) {
                    return $data?->amc_status == 1 ? 'Approved' : 'Pending';
                })
                ->filterColumn('amc_status', function ($query, $keyword) {
                    $query->whereRaw("
                        CASE
                            WHEN amc_status = '1' THEN 'Approved'
                            ELSE 'Pending'
                        END LIKE ?", ["%{$keyword}%"]);
                })
                ->editColumn('dmc_status', function ($data) {
                    if ($data?->leaveType->is_approve_by == 2) {
                        return $data?->dmc_status == 1 ? 'Approved' : 'Pending';
                    }
                    return '-';
                })
                ->filterColumn('dmc_status', function ($query, $keyword) {
                    $query->whereRaw("
                        CASE
                            WHEN dmc_status = '1' THEN 'Approved'
                            ELSE 'Pending'
                        END LIKE ?", ["%{$keyword}%"]);
                })
                ->editColumn('id', function ($data) {
                    if (Auth::user()->hasRole('Super Admin')) {
                        return $data->id;
                    } else {
                        if ($data->status != "") {
                            return $data->id;
                        }
                    }
                    return "";
                })
                ->make(true);
        }
    }
}
