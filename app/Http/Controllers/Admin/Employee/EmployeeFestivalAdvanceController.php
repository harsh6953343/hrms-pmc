<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Employee\StoreEmployeeFestivalAdvanceRequest;
use App\Models\EmployeeFestivalAdvance;
use App\Models\FinancialYear;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;

class EmployeeFestivalAdvanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employee_festival_advances = EmployeeFestivalAdvance::with('employee')
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereHas('employee', function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                });
            })
            ->latest()
            ->get();

        return view('admin.employee.festivalAdvance.employee-festival-advance')->with(['employee_festival_advances' => $employee_festival_advances]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeFestivalAdvanceRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $input['pending_instalment'] = $input['total_instalment'];
            $input['financial_year_id'] = session('financial_year');

            $check_advance_already_added = EmployeeFestivalAdvance::where('employee_id', $input['employee_id'])->where('financial_year_id', $input['financial_year_id'])->first();

            if (empty($check_advance_already_added)) {
                EmployeeFestivalAdvance::create(Arr::only($input, EmployeeFestivalAdvance::getFillables()));
                DB::commit();
                return response()->json(['success' => 'Employee Festival Advance created successfully!']);
            } else {
                return response()->json(['error2' => 'Employee Festival Advance Already Added!']);
            }
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Employee Festival Advance');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchDateRange(Request $request, $month)
    {
        $financial_year = FinancialYear::where('id', session('financial_year'))->first();
        $current_month = date('m');

        if ($month >= $current_month) {
            if($month == 12){
                $month = 1;
            }else{
                $month += 1;
            }

            if ($financial_year) {
                if ($month <= 3) {
                    $year = date('Y', strtotime($financial_year->to_date));
                } else {
                    $year = date('Y', strtotime($financial_year->from_date));
                }

                $fromDate = Carbon::parse($year . '-' . $month . '-' . 16)->subMonth();
                $toDate = $fromDate->copy()->addMonths(10)->subDay();

                return response()->json([
                    'success'   => true,
                    'fromDate'  => $fromDate->toDateString(),
                    'toDate'    => $toDate->toDateString(),
                ]);
            } else {
                return response()->json(['error' => 'Financial Year Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'Select at least current month'], 200);
        }
    }
}
