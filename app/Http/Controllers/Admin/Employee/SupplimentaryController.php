<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\SupplimentaryBill;
use App\Models\SuplimentryEmployeeBill;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;
use App\Models\Attendance;
use DataTables;
use App\Models\Setting;
use App\Models\Ward;

class SupplimentaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $supplimentoryBills = SupplimentaryBill::with('ward')
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereHas('ward.department', function ($q) {
                        $q->whereIn('id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                    });
                })
                ->latest();

            return DataTables::of($supplimentoryBills)
                ->addIndexColumn()
                ->addColumn('ward', function ($suplimentry) {
                    return $suplimentry->ward->name;
                })
                ->editColumn('cheque_date', function ($data) {
                    return Carbon::parse($data->cheque_date)->format('d-m-Y');
                })
                ->filterColumn('cheque_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(cheque_date,'%d-%m-%Y') LIKE ?", ["%$keyword%"]);
                })
                ->make(true);
        }

        return view('admin.supplimentary-bill.index')->with(['type' => 1]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $billNo = Setting::where('name', 'supplimentary_bill_no')->value('value');

        $wards = Ward::select('id', 'name')->get();

        return view('admin.supplimentary-bill.add')->with([
            'billNo' => $billNo,
            'wards' => $wards
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $request['financial_year_id'] = session('financial_year');
            $request['cheque_date'] = \Carbon\Carbon::parse($request->cheque_date)->format('Y-m-d');
            $suplimentryBill = SupplimentaryBill::create($request->all());

            if (isset($request->emp_code)) {
                $count = count($request->emp_code);
                for ($i = 0; $i < $count; $i++) {
                    $suplimentryEmployeeBill = new SuplimentryEmployeeBill;
                    $suplimentryEmployeeBill->supplimentary_bill_id = $suplimentryBill->id;
                    $suplimentryEmployeeBill->employee_id = $request->employee_id[$i];
                    $suplimentryEmployeeBill->department_id = $request->department_id[$i];
                    $suplimentryEmployeeBill->emp_code = $request->emp_code[$i];
                    $suplimentryEmployeeBill->save();
                }
            }
            Setting::where('name', 'supplimentary_bill_no')->increment('value', 1);
            DB::commit();

            return response()->json(['success' => 'Supplementary Bill created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
        }
    }

    public function edit($id)
    {
        $wards = Ward::select('id', 'name')->get();

        $suplimentryBill = SupplimentaryBill::with('supplimentryEmployeeBills')->find($id);

        $authUser = Auth::user();

        $employees = Employee::with('ward', 'department')
            ->where('ward_id', $suplimentryBill->ward_id)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'employee_id')
            ->get();

        return view('admin.supplimentary-bill.edit')->with([
            'wards' => $wards,
            'suplimentryBill' => $suplimentryBill,
            'employees' => $employees
        ]);
    }

    public function show($id)
    {
        $suplimentryBill = SupplimentaryBill::with(['supplimentryEmployeeBills.employee', 'supplimentryEmployeeBills.department', 'ward'])->find($id);

        return view('admin.supplimentary-bill.show')->with([
            'suplimentryBill' => $suplimentryBill
        ]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            DB::beginTransaction();
            $request['financial_year_id'] = session('financial_year');
            $request['cheque_date'] = \Carbon\Carbon::parse($request->cheque_date)->format('Y-m-d');
            $suplimentryBill = SupplimentaryBill::find($id);
            $request['cheque_date'] = date('Y-m-d', strtotime($request->cheque_date));
            $suplimentryBill->update($request->all());

            if (isset($request->emp_code)) {
                SuplimentryEmployeeBill::where('supplimentary_bill_id', $id)->delete();
                $count = count($request->emp_code);
                for ($i = 0; $i < $count; $i++) {
                    $suplimentryEmployeeBill = new SuplimentryEmployeeBill;
                    $suplimentryEmployeeBill->supplimentary_bill_id = $suplimentryBill->id;
                    $suplimentryEmployeeBill->employee_id = $request->employee_id[$i];
                    $suplimentryEmployeeBill->department_id = $request->department_id[$i];
                    $suplimentryEmployeeBill->emp_code = $request->emp_code[$i];
                    $suplimentryEmployeeBill->save();
                }
            }
            DB::commit();

            return response()->json(['success' => 'Supplementary Bill created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SupplimentaryBill $supplimentaryBill)
    {
        try {
            DB::beginTransaction();
            $supplimentaryBill->delete();
            DB::commit();
            return response()->json(['success' => 'Supplimentary Bill deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Supplimentary Bill');
        }
    }

    public function fetchEmployeeDetails($ward)
    {
        $authUser = Auth::user();
        $employee_details = Employee::with('ward', 'department')
            ->where('ward_id', $ward)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'employee_id')
            ->get();


        if ($employee_details) {
            $response = [
                'result' => 1,
                'employee_details' => $employee_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function fetchSupplimentaryBillNoofDays(Request $request)
    {
        if ($request->ajax()) {
            $suplimentryCount = SupplimentaryBill::where('month', $request->month)
                ->where('Emp_Code', $request->emp_code)
                ->where('financial_year_id', session('financial_year'))
                ->sum('no_of_days');

            $freezeSalary = Attendance::where('Emp_Code', $request->emp_code)
                ->where('month', $request->month)
                ->latest()
                ->value('total_absent_days');

            $totalDay =  $freezeSalary - $suplimentryCount;

            return response()->json(['days' => $totalDay]);
        }
    }
}
