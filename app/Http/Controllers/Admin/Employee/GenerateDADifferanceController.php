<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DaBill;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class GenerateDADifferanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $daBills = DaBill::latest()->get();

        return view('admin.employee.da-differance-generate')->with(['daBills' => $daBills]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $setting = Setting::where('name', 'da_difference')->value('value');
            $request['bill_no'] = $setting;
            $request['bill_date'] = now();
            DaBill::create($request->all());

            Setting::where('name', 'da_difference')->increment('value');
            DB::commit();

            return response()->json(['success' => 'Da Difference sended successfully']);
        }catch(\Exception $e){
            \Log::info($e);
            DB::rollback();
            return response()->json(['error' => 'Something went wrong, please try again!']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            $da = DaBill::find($id);
            $da->status = 0;
            $da->save();
            return response()->json([
                'success' => 'Da Difference updated to pending'
            ]);
        }catch(\Exception $e){
            \Log::info($e);
            return response()->json([
                'error' => 'Something went wrong, please try again!'
            ]);
        }
    }
}
