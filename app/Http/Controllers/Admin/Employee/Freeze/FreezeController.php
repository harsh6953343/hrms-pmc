<?php

namespace App\Http\Controllers\Admin\Employee\Freeze;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Freeze\DeleteFreezeRequest;
use App\Http\Requests\Admin\Freeze\StoreFreezeRequest;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\EmployeeMonthlyLoan;
use App\Models\EmployeeMonthlyLic;
use App\Models\EmployeeProvidentFund;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\SupplimentaryBill;
use App\Models\FreezeCron;
use App\Models\PayScale;
use App\Models\RemainingFreezeSalary;
use App\Models\EmployeeMonthlyFestivalAdvance;
use App\Models\EmployeeFestivalAdvance;
use App\Models\Attendance;
use App\Models\EmployeeDaDifferance;
use App\Models\Ward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;

class FreezeController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $userDepartment = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');

        $freeze = FreezeAttendance::groupBy('from_date', 'to_date')
            ->select('from_date', 'to_date')

            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) use ($userDepartment) {
                $q->whereIn('department_id', $userDepartment);
            })
            ->latest()
            ->get();

        $wards = Ward::when(Auth::user()->hasRole('AMC', 'DMC'), function ($q) use ($userDepartment) {
            $q->whereHas('departments', function ($q) use ($userDepartment) {
                return $q->whereIn('id', $userDepartment);
            });
        })->latest()->get();

        return view('admin.freeze.freeze')->with(['freezes' => $freeze, 'wards' => $wards]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFreezeRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            $check_already_added = FreezeCron::where('from_date', $input['from_date'])
                ->where('to_date', $input['to_date'])
                ->where('month', $input['month'])
                ->where('ward_id', $input['ward_id'])
                ->first();

            if ($check_already_added) {
                return response()->json(['error' => 'Already Generated!']);
            } else {
                $input['financial_year_id'] = session('financial_year');
                FreezeCron::create(Arr::only($input, FreezeCron::getFillables()));
                DB::commit();
                return response()->json(['success' => 'Freeze Attendance successfully ,wait for 5 minutes to reflect!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($from_date)
    {
        $response = [
            'result'            => 1,
            'from_date'         => $from_date
        ];
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DeleteFreezeRequest $request, $from_date)
    {
        $wardId = $request->input('ward_id');

        // Retrieve the records based on the provided criteria
        $recordsToDelete = FreezeAttendance::where('from_date', $from_date)
            ->where('ward_id', $wardId)
            ->get();

        FreezeCron::where('ward_id', $wardId)->where('from_date', $from_date)->delete();

        foreach ($recordsToDelete as $val) {
            RemainingFreezeSalary::where('freeze_attendance_id', $val->id)->delete();

            EmployeeProvidentFund::where('employee_id', $val->employee_id)->where('salary_month', $from_date)->delete();
        }

        // Delete the retrieved records
        $deleteCount = $recordsToDelete->count();
        $recordsToDelete->each->delete();

        // Return response indicating successful deletion
        return response()->json([
            'success' => "$deleteCount records deleted successfully.",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($from_date)
    {
        try {
            $freeze_cron = FreezeCron::where('from_date', $from_date)->first();;
            if (!$freeze_cron) {
                return response()->json(['error' => 'Data not found'], 404);
            }
            DB::beginTransaction();
            $freeze_cron->salary_generated = 1;
            $freeze_cron->save();
            DB::commit();
            return response()->json(['success' => 'Update successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while salary genereated'], 500);
        }
    }

    public function unfreezeEmployee(Request $request)
    {
        try {
            $freeze_attendance = FreezeAttendance::find($request->id);
            if (!$freeze_attendance) {
                return response()->json(['error' => 'Employee not found'], 404);
            }
            DB::beginTransaction();
            $freeze_attendance->freeze_status = 0;
            $freeze_attendance->save();
            DB::commit();
            return response()->json(['success' => 'Unfreeze successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while unfreeze the employee'], 500);
        }
    }

    // public function freezeEmployee(Request $request)
    // {

    //     try {
    //         $freeze_attendance = FreezeAttendance::find($request->id);
    //         if (!$freeze_attendance) {
    //             return response()->json(['error' => 'Employee not found'], 404);
    //         }



    //         $total_allowance = 0;
    //         $remaining_total_allowance = 0;

    //         $total_deduction = 0;
    //         $remaining_total_deduction = 0;

    //         $total_loan_deduction = 0;
    //         $total_loan_remaining_deduction = 0;

    //         $allowance_ids_array = [];
    //         $allowance_amt_array = [];
    //         $allowance_type_array = [];

    //         $remaining_allowance_ids_array = [];
    //         $remaining_allowance_amt_array = [];
    //         $remaining_allowance_type_array = [];

    //         $deduction_ids_array = [];
    //         $deduction_amt_array = [];
    //         $deduction_type_array = [];

    //         $remaining_deduction_ids_array = [];
    //         $remaining_deduction_amt_array = [];
    //         $remaining_deduction_type_array = [];

    //         $loan_ids_array = [];
    //         $loan_amt_array = [];
    //         $loan_deduction_bank_id = [];

    //         $STAMP_DUTY = 1;

    //         $Net_Salary = 0;
    //         $Remaining_Net_Salary = 0;

    //         $bncmc_share_da = 0;
    //         $remaining_bncmc_share_da = 0;
    //         $basicandDA = 0;
    //         $remaining_basicandDA = 0;
    //         $share = 0;
    //         $remaining_share = 0;

    //         $pf_loan = 0;
    //         $pf_contribution = 0;

    //         // LIC Deduction
    //         $lic_ids_array = [];
    //         $lic_amt_array = [];

    //         $total_lic_deduction = 0;
    //         $total_lic_remaining_deduction = 0;

    //         // festival advance Deduction
    //         $festadv_ids_array = [];
    //         $festadv_amt_array = [];
    //         $total_festAdv_deduction = 0;
    //         $total_festAdv_remaining_deduction = 0;

    //         // festival advance Allowance
    //         $totalFestivalAdvance = 0;
    //         $remainingtotalFestivalAdvance = 0;

    //         $startDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
    //         $endDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->to_date);
    //         $numberOfDaysInMonth = $startDate->diffInDays($endDate);
    //         $numberOfDaysInMonth += 1;

    //         $employee = Employee::with('salary', 'employee_status')->where('id', $freeze_attendance->employee_id)->first();

    //         if ($employee->salary) {

    //             // Fetch loans for the employee within the given date range
    //             $loanArr = EmployeeMonthlyLoan::where('from_date', $freeze_attendance->from_date)
    //                 ->where('to_date', $freeze_attendance->to_date)
    //                 ->where('employee_id', $employee->id)
    //                 ->get();

    //             // LIC Dedcution Data Get
    //             $licArr = EmployeeMonthlyLic::
    //             where('from_date', $freeze_attendance->from_date)
    //             ->where('to_date', $freeze_attendance->to_date)
    //             ->where('employee_id', $employee->id)
    //             ->get();

    //             // Festival advance Dedcution Data Get
    //             $festAdvArr = EmployeeMonthlyFestivalAdvance::
    //             where('from_date', $freeze_attendance->from_date)
    //             ->where('to_date', $freeze_attendance->to_date)
    //             ->where('employee_id', $employee->id)
    //             ->get();

    //             // Fetch pay scales for the employee within the given salary
    //             $payScales = PayScale::where('id', $employee->salary->pay_scale_id)
    //                 ->first();

    //             $check_status = 0;
    //             if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {
    //             } else {
    //                 $check_status = 1;
    //             }

    //             $present_days_new = $numberOfDaysInMonth;

    //             // Loan Logic
    //             foreach ($loanArr as $loan) {

    //                 $loan_ids_array[] = $loan->id;
    //                 $loan_amt_array[] = $loan->installment_amount;
    //                 $loan_deduction_bank_id[] = $loan->loan_id;

    //                 $total_loan_deduction += $loan->installment_amount;
    //                 $total_deduction += $loan->installment_amount;

    //                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //                 } else {
    //                     if ($employee?->employee_status?->is_salary_applicable == 0) {
    //                         $total_loan_remaining_deduction += $loan->installment_amount;
    //                         $remaining_total_deduction += $loan->installment_amount;
    //                     }
    //                 }

    //                 // To store pf loan
    //                 if ($loan->id == 1) {
    //                     $pf_loan += $loan->installment_amount;
    //                 }
    //             }

    //             $implode_loan_ids_array = implode(',', $loan_ids_array);
    //             $implode_loan_amt_array = implode(',', $loan_amt_array);
    //             $implode_loan_bank_array = implode(',', $loan_deduction_bank_id);

    //             // LIC Logic
    //             foreach ($licArr as $lic) {
    //                 $lic_ids_array[] = $lic->id;
    //                 $lic_amt_array[] = $lic->installment_amt;

    //                 $total_lic_deduction += $lic->installment_amt;
    //                 $total_deduction += $lic->installment_amt;

    //                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //                 } else {
    //                     if ($employee?->employee_status?->is_salary_applicable == 0) {
    //                         $total_lic_remaining_deduction += $lic->installment_amt;
    //                         $remaining_total_deduction += $lic->installment_amt;
    //                     }
    //                 }
    //             }

    //             $implode_lic_ids_array = implode(',', $lic_ids_array);
    //             $implode_lic_amt_array = implode(',', $lic_amt_array);

    //              // Festival advance deduction logic start

    //             foreach ($festAdvArr as $festAdv) {
    //                 $festadv_ids_array[] = $festAdv->id;
    //                 $festadv_amt_array[] = $festAdv->installment_amount;

    //                 $total_festAdv_deduction    += $festAdv->installment_amount;
    //                 $total_deduction            += $festAdv->installment_amount;

    //                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //                 } else {
    //                     if ($employee?->employee_status?->is_salary_applicable == 0) {
    //                         $total_festAdv_remaining_deduction  += $festAdv->installment_amount;
    //                         $remaining_total_deduction          += $festAdv->installment_amount;
    //                     }
    //                 }
    //             }

    //             $implode_festAdv_ids_array = implode(',', $festadv_ids_array);
    //             $implode_festAdv_amt_array = implode(',', $festadv_amt_array);

    //             // Festival advance deduction logic end

    //             if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //             } else {
    //                 $implode_loan_remaining_ids_array = implode(',', $loan_ids_array);
    //                 $implode_loan_remaining_amt_array = implode(',', $loan_amt_array);
    //                 $implode_loan_remaining_bank_array = implode(',', $loan_deduction_bank_id);

    //                 $implode_lic_remaining_ids_array = implode(',', $lic_ids_array);
    //                 $implode_lic_remaining_amt_array = implode(',', $lic_amt_array);

    //                 $implode_festAdv_remaining_ids_array = implode(',', $festadv_ids_array);
    //                 $implode_festAdv_remaining_amt_array = implode(',', $festadv_amt_array);
    //             }

    //             // Festival Advance Allowance Logic Start
    //             $getFestivalAllowance = EmployeeFestivalAdvance::
    //             where('employee_id',$employee->id)
    //             ->where('applicable_month',$freeze_attendance->month)
    //             ->where('apply_status',2)->first();

    //             if(!empty($getFestivalAllowance)){
    //             $totalFestivalAdvance           = $getFestivalAllowance->total_amount;
    //             $remainingtotalFestivalAdvance  = $getFestivalAllowance->total_amount;

    //             $total_allowance                = $getFestivalAllowance->total_amount;
    //             $remaining_total_allowance      = $getFestivalAllowance->total_amount;

    //             }
    //             // Festival Advance Allowance Logic End

    //             if ($employee?->employee_status === null || ($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 0)) {

    //                 $fromDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
    //                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {

    //                     if($employee?->employee_status?->applicable_date > $freeze_attendance->to_date){
    //                         $present_days = $numberOfDaysInMonth;
    //                     }else{
    //                         $present_days = $fromDate->diffInDays($employee?->employee_status?->applicable_date) + 1;
    //                     }
    //                 } elseif ($employee?->employee_status && $employee?->employee_status?->applicable_date <= $freeze_attendance->from_date) {
    //                     $present_days = 0;
    //                 } else {
    //                     // Assuming present days for now
    //                     $present_days = $numberOfDaysInMonth;
    //                 }

    //                 // Assuming present days for now
    //                 // $present_days = $numberOfDaysInMonth;

    //                 // Calculate salary per day and salary based on present days
    //                 $basicSalary = optional($employee->salary)->basic_salary;
    //                 $gradePay = optional($employee->salary)->grade_pay;
    //                 $basicPlusGpay = $basicSalary + $gradePay;
    //                 $salary_per_day = $basicSalary / $numberOfDaysInMonth; //change if salary calculates on basic plus gpay
    //                 $salary_based_on_present_day = round($salary_per_day * $present_days);

    //                 if ($present_days != $numberOfDaysInMonth) {
    //                     $remaining_salary_based_on_present_day = round($salary_per_day * ($numberOfDaysInMonth - $present_days));
    //                 } else {
    //                     $remaining_salary_based_on_present_day = round($salary_per_day * $present_days);
    //                 }

    //                 // Allowance logic
    //                 foreach ($employee->employee_allowances as $allowance) {
    //                     $allowanceMaster = Allowance::find($allowance->allowance_id);

    //                     if ($allowance->is_active == 1 && $allowanceMaster) {
    //                         if ($allowance->allowance_type == 1) {
    //                             if ($allowanceMaster->calculation == 1) {
    //                                 $total_allowance += $allowance->allowance_amt;

    //                                 // Store allowance details in arrays
    //                                 $allowance_ids_array[] = $allowance->allowance_id;
    //                                 $allowance_amt_array[] = $allowance->allowance_amt;
    //                                 $allowance_type_array[] = $allowance->allowance_type;

    //                                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //                                 } else {
    //                                     $remaining_total_allowance += $allowance->allowance_amt;
    //                                     $remaining_allowance_ids_array[] = $allowance->allowance_id;
    //                                     $remaining_allowance_amt_array[] = $allowance->allowance_amt;
    //                                     $remaining_allowance_type_array[] = $allowance->allowance_type;
    //                                 }
    //                             } else {
    //                                 $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * $present_days;
    //                                 $total_allowance += round($dynamicAllowanceAmt);

    //                                 // if status apply and applicable
    //                                 $remaining_dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($numberOfDaysInMonth - $present_days);
    //                                 $remaining_total_allowance += round($remaining_dynamicAllowanceAmt);


    //                                 // Store allowance details in arrays
    //                                 $allowance_ids_array[] = $allowance->allowance_id;
    //                                 $allowance_amt_array[] = round($dynamicAllowanceAmt);
    //                                 $allowance_type_array[] = $allowance->allowance_type;

    //                                 $remaining_allowance_ids_array[] = $allowance->allowance_id;
    //                                 $remaining_allowance_amt_array[] = round($remaining_dynamicAllowanceAmt);
    //                                 $remaining_allowance_type_array[] = $allowance->allowance_type;
    //                             }
    //                         } elseif ($allowance->allowance_type == 2) {

    //                             if ($allowance->allowance_id == 1) {
    //                                 $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                 $cal_amount2 = ($remaining_salary_based_on_present_day * $allowance->allowance_amt) / 100;

    //                                 $bncmc_share_da = $cal_amount;
    //                                 $basicandDA =  $cal_amount + $salary_based_on_present_day;

    //                                 $remaining_bncmc_share_da = $cal_amount2;
    //                                 $remaining_basicandDA =  $cal_amount2 + $remaining_salary_based_on_present_day;

    //                                 $total_allowance +=  round($cal_amount);
    //                                 $remaining_total_allowance += round($cal_amount2);
    //                             } else {
    //                                 $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                 $cal_amount2 = ($remaining_salary_based_on_present_day * $allowance->allowance_amt) / 100;

    //                                 $total_allowance += round($cal_amount);
    //                                 $remaining_total_allowance += round($cal_amount2);
    //                             }
    //                             // Store allowance details in arrays
    //                             $allowance_ids_array[] = $allowance->allowance_id;
    //                             $allowance_amt_array[] = round($cal_amount);
    //                             $allowance_type_array[] = $allowance->allowance_type;

    //                             $remaining_allowance_ids_array[] = $allowance->allowance_id;
    //                             $remaining_allowance_amt_array[] = round($cal_amount2);
    //                             $remaining_allowance_type_array[] = $allowance->allowance_type;
    //                         }
    //                     }
    //                 }

    //                 // Implode arrays for allowance
    //                 $implode_allowance_ids_array = implode(',', $allowance_ids_array);
    //                 $implode_allowance_amt_array = implode(',', $allowance_amt_array);
    //                 $implode_allowance_type_array = implode(',', $allowance_type_array);

    //                 $implode_remaining_allowance_ids_array = implode(',', $remaining_allowance_ids_array);
    //                 $implode_remaining_allowance_amt_array = implode(',', $remaining_allowance_amt_array);
    //                 $implode_remaining_allowance_type_array = implode(',', $remaining_allowance_type_array);


    //                 // Deduction logic
    //                 foreach ($employee->employee_deductions as $deduction) {

    //                     $deductionMaster = Deduction::find($deduction->deduction_id);

    //                     if ($deduction->is_active == 1 && $deductionMaster) {

    //                         if ($deduction->deduction_type == 1) {
    //                             if ($deductionMaster->calculation == 1) {
    //                                 $total_deduction += $deduction->deduction_amt;

    //                                 if ($deduction->deduction_id == 3) {
    //                                     $pf_contribution += $deduction->deduction_amt;
    //                                 }

    //                                 // Store deduction details in arrays
    //                                 $deduction_ids_array[] = $deduction->deduction_id;
    //                                 $deduction_amt_array[] = $deduction->deduction_amt;
    //                                 $deduction_type_array[] = $deduction->deduction_type;

    //                                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) {
    //                                 } else {
    //                                     $remaining_total_deduction += $deduction->deduction_amt;
    //                                     $remaining_deduction_ids_array[] = $deduction->deduction_id;
    //                                     $remaining_deduction_amt_array[] = $deduction->deduction_amt;
    //                                     $remaining_deduction_type_array[] = $deduction->deduction_type;
    //                                 }
    //                             } else {
    //                                 $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * $present_days;
    //                                 $total_deduction += round($dynamicDeductionAmt);

    //                                 // only applicable if status and applicable date in between
    //                                 $remaining_dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($numberOfDaysInMonth - $present_days);
    //                                 $remaining_total_deduction += round($remaining_dynamicDeductionAmt);

    //                                 if ($deduction->deduction_id == 3) {
    //                                     $pf_contribution += round($dynamicDeductionAmt);
    //                                 }
    //                                 // Store deduction details in arrays
    //                                 $deduction_ids_array[] = $deduction->deduction_id;
    //                                 $deduction_amt_array[] = round($dynamicDeductionAmt);
    //                                 $deduction_type_array[] = $deduction->deduction_type;

    //                                 $remaining_deduction_ids_array[] = $deduction->deduction_id;
    //                                 $remaining_deduction_amt_array[] = round($remaining_dynamicDeductionAmt);
    //                                 $remaining_deduction_type_array[] = $deduction->deduction_type;
    //                             }
    //                         } elseif ($deduction->deduction_type == 2) {

    //                             if ($deduction->deduction_id == 4) {

    //                                 $cal_amount = ($basicandDA * $deduction->deduction_amt) / 100;
    //                                 $total_deduction +=  round($cal_amount);

    //                                 $cal_amount3 = ($remaining_basicandDA * $deduction->deduction_amt) / 100;

    //                                 $remaining_total_deduction +=  round($cal_amount3);

    //                             } else {

    //                                 $cal_amount = ($salary_based_on_present_day * $deduction->deduction_amt) / 100;
    //                                 $total_deduction += round($cal_amount);

    //                                 $cal_amount3 = ($remaining_salary_based_on_present_day * $deduction->deduction_amt) / 100;
    //                                 $remaining_total_deduction += round($cal_amount3);

    //                                 if ($deduction->deduction_id == 3) {
    //                                     $pf_contribution += round($cal_amount);
    //                                 }
    //                             }

    //                             // Store deduction details in arrays
    //                             $deduction_ids_array[] = $deduction->deduction_id;
    //                             $deduction_amt_array[] = round($cal_amount);
    //                             $deduction_type_array[] = $deduction->deduction_type;

    //                             $remaining_deduction_ids_array[] = $deduction->deduction_id;
    //                             $remaining_deduction_amt_array[] = round($cal_amount3);
    //                             $remaining_deduction_type_array[] = $deduction->deduction_type;

    //                         }

    //                     }
    //                 }

    //                 // Implode arrays for deduction
    //                 $implode_deduction_ids_array = implode(',', $deduction_ids_array);
    //                 $implode_deduction_amt_array = implode(',', $deduction_amt_array);
    //                 $implode_deduction_type_array = implode(',', $deduction_type_array);

    //                 $implode_remaining_deduction_ids_array = implode(',', $remaining_deduction_ids_array);
    //                 $implode_remaining_deduction_amt_array = implode(',', $remaining_deduction_amt_array);
    //                 $implode_remaining_deduction_type_array = implode(',', $remaining_deduction_type_array);

    //                 // Calculate total deduction including stamp duty
    //                 $total_deduction += $STAMP_DUTY;
    //                 $remaining_total_deduction += $STAMP_DUTY;

    //                 // Calculate net salary
    //                 $Net_Salary = $salary_based_on_present_day + $total_allowance - ($total_deduction);
    //                 $Remaining_Net_Salary = $remaining_salary_based_on_present_day + $remaining_total_allowance - ($remaining_total_deduction);


    //                 if ($employee->doj > '2005-11-01') {
    //                     $bncmc_share = ($salary_based_on_present_day + $bncmc_share_da) * 14 / 100;
    //                     $share =  round($bncmc_share);

    //                     $remaining_bncmc_share = ($remaining_salary_based_on_present_day + $remaining_bncmc_share_da) * 14 / 100;

    //                     $remaining_share =  round($remaining_bncmc_share);
    //                 }

    //                   // Get supplimentary Data
    //                 $supplimentary_data = SupplimentaryBill::where('id',$freeze_attendance->supplimentary_ids)
    //                   ->where('status',1)->first();

    //                 if(!empty($supplimentary_data)){
    //                   SupplimentaryBill::where('id', $supplimentary_data->id)
    //                   ->update(['status' => 1]);

    //                   $supplimentary_ids = $supplimentary_data->id;

    //                   $supplimentary_status = 1;
    //                 }else{

    //                       $supplimentary_data = SupplimentaryBill::where('employee_id',$employee->id)
    //                       ->where('status',0)
    //                       ->where('to_date','<=',$freeze_attendance->to_date)->first();

    //                       if(!empty($supplimentary_data)){
    //                           SupplimentaryBill::where('id', $supplimentary_data->id)
    //                           ->update(['status' => 1]);

    //                           $supplimentary_ids = $supplimentary_data->id;
    //                           $supplimentary_status = 1;
    //                       }else{
    //                           $supplimentary_status = 0;
    //                           $supplimentary_ids = null;
    //                       }
    //                 }


    //                 // Create FreezeAttendance record

    //                 $data = FreezeAttendance::updateOrCreate(
    //                     [
    //                         'id' => $freeze_attendance->id,
    //                         'from_date' => $freeze_attendance->from_date,
    //                         'to_date' => $freeze_attendance->to_date,
    //                     ],
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'Emp_Code' => $employee->employee_id,
    //                         'freeze_status' => 1,
    //                         'attendance_UId' => NULL, //pending
    //                         'ward_id' => $employee->ward_id,
    //                         'department_id' => $employee->department_id,
    //                         'designation_id' => $employee->designation_id,
    //                         'clas_id' => $employee->clas_id,
    //                         'from_date' => $freeze_attendance->from_date,
    //                         'to_date' => $freeze_attendance->to_date,
    //                         'month' => $freeze_attendance->month,
    //                         'financial_year_id' => $freeze_attendance->financial_year_id,
    //                         'present_day' =>  $present_days,
    //                         'basic_salary' => ($present_days != 0) ? $salary_based_on_present_day : 0,
    //                         'actual_basic' => $basicSalary,
    //                         'grade_pay' => $gradePay,
    //                         'allowance_Id' => ($present_days != 0) ? $implode_allowance_ids_array : '',
    //                         'allowance_Amt' => ($present_days != 0) ? $implode_allowance_amt_array : '',
    //                         'allowance_Type' => ($present_days != 0) ? $implode_allowance_type_array : '',
    //                         'festival_allowance'        => ($present_days != 0) ? $totalFestivalAdvance : 0,
    //                         'festival_allowance_id'     => ($present_days != 0 && !empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,

    //                         'total_allowance' => ($present_days != 0) ? $total_allowance : 0,
    //                         'deduction_Id' => ($present_days != 0) ? $implode_deduction_ids_array : '',
    //                         'deduction_Amt' => ($present_days != 0) ? $implode_deduction_amt_array : '',
    //                         'deduction_Type' => ($present_days != 0) ? $implode_deduction_type_array : '',
    //                         'total_deduction' => ($present_days != 0) ? $total_deduction : 0,
    //                         'stamp_duty' => ($present_days != 0) ? $STAMP_DUTY : 0,
    //                         'loan_deduction_id' => ($present_days != 0) ? $implode_loan_ids_array : '',
    //                         'loan_deduction_amt' => ($present_days != 0) ? $implode_loan_amt_array : '',
    //                         'loan_deduction_bank_id' => ($present_days != 0) ? $implode_loan_bank_array : '',
    //                         'total_loan_deduction' => ($present_days != 0) ? $total_loan_deduction : 0,
    //                         'lic_deduction_id' => ($present_days != 0) ? $implode_lic_ids_array : '',
    //                         'lic_deduction_amt' => ($present_days != 0) ? $implode_lic_amt_array : '',
    //                         'total_lic_deduction' => ($present_days != 0) ? $total_lic_deduction : 0,

    //                         'festival_deduction_id'     => ($present_days != 0) ? $implode_festAdv_ids_array : '',
    //                         'festival_deduction_amt'    => ($present_days != 0) ? $implode_festAdv_amt_array : '',
    //                         'total_festival_deduction'  => ($present_days != 0) ? $total_festAdv_deduction : 0,

    //                         'net_salary' => ($present_days != 0) ? $Net_Salary : 0,
    //                         'emp_name' =>  $employee?->fname . " " . $employee?->mname . " " . $employee?->lname,
    //                         'pf_account_no' => ($employee?->pf_account_no) ? $employee?->pf_account_no : 0,
    //                         'pay_band_scale' => $payScales->pay_band_scale,
    //                         'grade_pay_scale' => $payScales->grade_pay_name,
    //                         'date_of_birth' => $employee?->dob,
    //                         'date_of_appointment' => $employee?->doj,
    //                         'date_of_retirement' => $employee?->retirement_date,
    //                         'bank_account_number' => $employee?->account_no,
    //                         'phone_no' => $employee?->mobile_number,
    //                         'corporation_share_da' => ($present_days != 0) ? $share : 0,
    //                         'supplimentary_status'  => $supplimentary_status,
    //                         'supplimentary_ids'     => $supplimentary_ids,
    //                     ]
    //                 );

    //                 // Remaining Salary
    //                 if ((!empty($employee?->employee_status) && $employee?->employee_status?->applicable_date < $freeze_attendance->to_date) || $present_days < $numberOfDaysInMonth) {
    //                     RemainingFreezeSalary::updateOrCreate(
    //                         [
    //                             'employee_id' => $employee->id,
    //                             'freeze_attendance_id' => $data->id,
    //                         ],
    //                         [
    //                             'employee_id' => $employee->id,
    //                             'freeze_attendance_id' => $data->id,
    //                             'Emp_Code' => $employee->employee_id,
    //                             'from_date' => $freeze_attendance->from_date,
    //                             'to_date' => $freeze_attendance->to_date,
    //                             'month' => $freeze_attendance->month,
    //                             'present_day' => ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) ? $numberOfDaysInMonth - $present_days : $present_days,
    //                             'basic_salary' => $remaining_salary_based_on_present_day,
    //                             'actual_basic' => $basicSalary,
    //                             'grade_pay' => $gradePay,
    //                             'allowance_Id' =>   $implode_remaining_allowance_ids_array,
    //                             'allowance_Amt' =>  $implode_remaining_allowance_amt_array,
    //                             'allowance_Type' => $implode_remaining_allowance_type_array,
    //                             'festival_allowance'        => $remainingtotalFestivalAdvance,
    //                             'festival_allowance_id'     => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
    //                             'total_allowance' => $remaining_total_allowance,
    //                             'deduction_Id' => $implode_remaining_deduction_ids_array,
    //                             'deduction_Amt' => $implode_remaining_deduction_amt_array,
    //                             'deduction_Type' => $implode_remaining_deduction_type_array,
    //                             'total_deduction' => $remaining_total_deduction,
    //                             'stamp_duty' => $STAMP_DUTY,
    //                             'loan_deduction_id' => (!empty($implode_loan_remaining_ids_array)) ? $implode_loan_remaining_ids_array : '',
    //                             'loan_deduction_amt' => (!empty($implode_loan_remaining_amt_array)) ? $implode_loan_remaining_amt_array : '',
    //                             'loan_deduction_bank_id' => (!empty($implode_loan_remaining_bank_array)) ? $implode_loan_remaining_bank_array : '',
    //                             'total_loan_deduction' => $total_loan_remaining_deduction,

    //                             'lic_deduction_id' => (!empty($implode_lic_remaining_ids_array)) ? $implode_lic_remaining_ids_array : '',
    //                             'lic_deduction_amt' => (!empty($implode_lic_remaining_amt_array)) ? $implode_lic_remaining_amt_array : '',
    //                             'total_lic_deduction' => $total_lic_remaining_deduction,

    //                             'festival_deduction_id'     => (!empty($implode_festAdv_remaining_ids_array)) ? $implode_festAdv_remaining_ids_array : '',
    //                             'festival_deduction_amt'    => (!empty($implode_festAdv_remaining_amt_array)) ? $implode_festAdv_remaining_amt_array : '',
    //                             'total_festival_deduction'  => $total_festAdv_remaining_deduction,


    //                             'net_salary' => $Remaining_Net_Salary,
    //                             'corporation_share_da' => $remaining_share,
    //                         ]
    //                     );
    //                 }

    //                 // Store PF Data
    //                 EmployeeProvidentFund::updateOrCreate(
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'current_month' => $freeze_attendance->to_date,
    //                         'salary_month' => $freeze_attendance->from_date,
    //                     ],
    //                     [
    //                         'employee_id'       => $employee->id,
    //                         'Emp_Code'          => $employee->employee_id,
    //                         'pf_account_no'     => $employee->pf_account_no,
    //                         'current_month'     => $freeze_attendance->to_date,
    //                         'salary_month'      => $freeze_attendance->from_date,
    //                         'financial_year_id' => $freeze_attendance->financial_year_id,
    //                         'pf_contribution'   => ($employee?->employee_status) ? 0 : $pf_contribution,
    //                         'pf_loan'           => $pf_loan,
    //                     ]
    //                 );

    //                 if(!empty($getFestivalAllowance)){
    //                     EmployeeFestivalAdvance::where('id', $getFestivalAllowance->id)
    //                     ->update(['apply_status' => 2]);
    //                 }

    //             } elseif ($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 1) {

    //                 $applicable_present_days = 0;
    //                 $fromDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
    //                 if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {
    //                     if($employee?->employee_status?->applicable_date > $freeze_attendance->to_date){
    //                         $applicable_present_days = 0;
    //                     }else{
    //                     $applicable_present_days = $fromDate->diffInDays($employee?->employee_status?->applicable_date) + 1;
    //                     }
    //                 }

    //                 // Assuming present days for now
    //                 $present_days = $numberOfDaysInMonth;

    //                 $salary_percent = 0;

    //                 if ($employee?->employee_status?->salary_percent) {
    //                     $salary_percent = $employee?->employee_status?->salary_percent;
    //                 }
    //                 // else if ($employee?->employee_status?->salary_percent == 2) {
    //                 //     $salary_percent = 50;
    //                 // } else if ($employee?->employee_status?->salary_percent == 3) {
    //                 //     $salary_percent = 75;
    //                 // } else if ($employee?->employee_status?->salary_percent == 4) {
    //                 //     $salary_percent = 100;
    //                 // }

    //                 $remaining_percent = (100 - $salary_percent);

    //                 // Calculate salary per day and salary based on present days
    //                 $basicSalary = optional($employee->salary)->basic_salary;
    //                 $gradePay = optional($employee->salary)->grade_pay;
    //                 $basicPlusGpay = $basicSalary + $gradePay;
    //                 $salary_per_day = $basicSalary / $numberOfDaysInMonth; //change if salary calculates on basic plus gpay

    //                 // if applicable date in between from date and to date // 17/04/2024
    //                 if ($applicable_present_days != 0) {
    //                     $salary_based_on_present_day = round(round($salary_per_day * ($present_days - $applicable_present_days)) * $salary_percent / 100);
    //                     $salary_based_on_applicable_day = round($salary_per_day * $applicable_present_days);

    //                     $remaining_based_on_present_day = round(round($salary_per_day * ($present_days - $applicable_present_days)) * $remaining_percent / 100);
    //                 } else {
    //                     $salary_based_on_present_day = round(round($salary_per_day * $present_days) * $salary_percent / 100);
    //                     $remaining_based_on_present_day = round(round($salary_per_day * $present_days) * $remaining_percent / 100);
    //                 }

    //                 // Allowance logic
    //                 foreach ($employee->employee_allowances as $allowance) {
    //                     $allowanceMaster = Allowance::find($allowance->allowance_id);

    //                     if ($allowance->is_active == 1 && $allowanceMaster) {
    //                         if ($allowance->allowance_type == 1) {
    //                             if ($allowanceMaster->calculation == 1) {
    //                                 $total_allowance += $allowance->allowance_amt;

    //                                 // Store allowance details in arrays
    //                                 $allowance_ids_array[] = $allowance->allowance_id;
    //                                 $allowance_amt_array[] = $allowance->allowance_amt;
    //                                 $allowance_type_array[] = $allowance->allowance_type;
    //                             } else {
    //                                 if ($applicable_present_days != 0) {
    //                                     $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * $applicable_present_days;
    //                                     $pendingdynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
    //                                     $total_allowance += round($dynamicAllowanceAmt);
    //                                     $total_allowance += round(round($pendingdynamicAllowanceAmt)  * $salary_percent / 100);

    //                                     $remaining_total_allowance += round(round($pendingdynamicAllowanceAmt)  * $remaining_percent / 100);
    //                                 } else {
    //                                     $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
    //                                     $total_allowance += round(round($dynamicAllowanceAmt)  * $salary_percent / 100);
    //                                     $remaining_total_allowance += round(round($dynamicAllowanceAmt)  * $remaining_percent / 100);
    //                                 }

    //                                 // Store allowance details in arrays
    //                                 $allowance_ids_array[] = $allowance->allowance_id;
    //                                 $new_allowance_amt = 0;
    //                                 $new_remaining_allowance_amt = 0;
    //                                 if ($applicable_present_days != 0) {
    //                                     $new_allowance_amt += round($dynamicAllowanceAmt);
    //                                     $new_allowance_amt += round(round($pendingdynamicAllowanceAmt)  * $salary_percent / 100);
    //                                     $new_remaining_allowance_amt += round(round($pendingdynamicAllowanceAmt)  * $remaining_percent / 100);
    //                                 } else {
    //                                     $new_allowance_amt += round(round($dynamicAllowanceAmt)  * $salary_percent / 100);
    //                                     $new_remaining_allowance_amt += round(round($dynamicAllowanceAmt)  * $remaining_percent / 100);
    //                                 }

    //                                 $allowance_amt_array[] = $new_allowance_amt;
    //                                 $allowance_type_array[] = $allowance->allowance_type;

    //                                 $remaining_allowance_ids_array[] = $allowance->allowance_id;
    //                                 $remaining_allowance_amt_array[] = $new_remaining_allowance_amt;
    //                                 $remaining_allowance_type_array[] = $allowance->allowance_type;
    //                             }
    //                         } elseif ($allowance->allowance_type == 2) {

    //                             if ($allowance->allowance_id == 1) {

    //                                 if ($applicable_present_days != 0) {
    //                                     $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $allowance->allowance_amt) / 100;
    //                                     $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                 } else {
    //                                     $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                     $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                 }

    //                                 $bncmc_share_da = $cal_amount;
    //                                 $remaining_bncmc_share_da = $cal_amount1;

    //                                 if ($applicable_present_days != 0) {
    //                                     $basicandDA =  $cal_amount + ($salary_based_on_present_day + $salary_based_on_applicable_day);
    //                                     $remainingbasicandDA =  $cal_amount1 + ($remaining_based_on_present_day);
    //                                 } else {
    //                                     $basicandDA =  $cal_amount + $salary_based_on_present_day;
    //                                     $remainingbasicandDA =  $cal_amount1 + $remaining_based_on_present_day;
    //                                 }

    //                                 $total_allowance +=  round($cal_amount);
    //                                 $remaining_total_allowance += round($cal_amount1);
    //                             } else {
    //                                 if ($applicable_present_days != 0) {
    //                                     $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $allowance->allowance_amt) / 100;
    //                                 } else {
    //                                     $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
    //                                 }
    //                                 $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;

    //                                 $total_allowance += round($cal_amount);
    //                                 $remaining_total_allowance += round($cal_amount1);
    //                             }
    //                             // Store allowance details in arrays
    //                             $allowance_ids_array[] = $allowance->allowance_id;
    //                             $allowance_amt_array[] = round($cal_amount);
    //                             $allowance_type_array[] = $allowance->allowance_type;

    //                             $remaining_allowance_ids_array[] = $allowance->allowance_id;
    //                             $remaining_allowance_amt_array[] = round($cal_amount1);
    //                             $remaining_allowance_type_array[] = $allowance->allowance_type;
    //                         }
    //                     }
    //                 }

    //                 // Implode arrays for allowance
    //                 $implode_allowance_ids_array = implode(',', $allowance_ids_array);
    //                 $implode_allowance_amt_array = implode(',', $allowance_amt_array);
    //                 $implode_allowance_type_array = implode(',', $allowance_type_array);

    //                 $implode_remaining_allowance_ids_array = implode(',', $remaining_allowance_ids_array);
    //                 $implode_remaining_allowance_amt_array = implode(',', $remaining_allowance_amt_array);
    //                 $implode_remaining_allowance_type_array = implode(',', $remaining_allowance_type_array);

    //                 // Deduction logic
    //                 foreach ($employee->employee_deductions as $deduction) {
    //                     $deductionMaster = Deduction::find($deduction->deduction_id);

    //                     if ($deduction->is_active == 1 && $deductionMaster) {
    //                         if ($deduction->deduction_type == 1) {
    //                             if ($deductionMaster->calculation == 1) {
    //                                 $total_deduction += $deduction->deduction_amt;

    //                                 if ($deduction->deduction_id == 3) {
    //                                     $pf_contribution += $deduction->deduction_amt;
    //                                 }

    //                                 // Store deduction details in arrays
    //                                 $deduction_ids_array[] = $deduction->deduction_id;
    //                                 $deduction_amt_array[] = $deduction->deduction_amt;
    //                                 $deduction_type_array[] = $deduction->deduction_type;
    //                             } else {

    //                                 if ($applicable_present_days != 0) {
    //                                     $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth)  * $applicable_present_days;
    //                                     $pendingdynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
    //                                     $total_deduction += round($dynamicDeductionAmt);
    //                                     $total_deduction += round(round($pendingdynamicDeductionAmt)  * $salary_percent / 100);

    //                                     $remaining_total_deduction += round(round($pendingdynamicDeductionAmt)  * $remaining_percent / 100);
    //                                 } else {
    //                                     $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
    //                                     $total_deduction += round(round($dynamicDeductionAmt) * $salary_percent / 100);
    //                                     $remaining_total_deduction += round(round($dynamicDeductionAmt) * $remaining_percent / 100);
    //                                 }

    //                                 if ($deduction->deduction_id == 3) {
    //                                     $pf_contribution += round(round($dynamicDeductionAmt) * $salary_percent / 100);
    //                                 }

    //                                 // Store deduction details in arrays
    //                                 $deduction_ids_array[] = $deduction->deduction_id;
    //                                 $new_deduction_amt = 0;
    //                                 $new_remaining_deduction_amt = 0;
    //                                 if ($applicable_present_days != 0) {
    //                                     $new_deduction_amt += round($dynamicDeductionAmt);
    //                                     $new_deduction_amt += round(round($pendingdynamicDeductionAmt)  * $salary_percent / 100);
    //                                     $new_remaining_deduction_amt += round(round($pendingdynamicDeductionAmt)  * $remaining_percent / 100);
    //                                 } else {
    //                                     $new_deduction_amt += round(round($dynamicDeductionAmt) * $salary_percent / 100);
    //                                     $new_remaining_deduction_amt += round(round($dynamicDeductionAmt) * $remaining_percent / 100);
    //                                 }


    //                                 $deduction_amt_array[] = $new_deduction_amt;
    //                                 $deduction_type_array[] = $deduction->deduction_type;

    //                                 $remaining_deduction_ids_array[] = $deduction->deduction_id;
    //                                 $remaining_deduction_amt_array[] = $new_remaining_deduction_amt;
    //                                 $remaining_deduction_type_array[] = $deduction->deduction_type;
    //                             }
    //                         } elseif ($deduction->deduction_type == 2) {

    //                             if ($deduction->deduction_id == 4) {
    //                                 $cal_amount = ($basicandDA * $deduction->deduction_amt) / 100;
    //                                 $cal_amount1 = ($remainingbasicandDA * $deduction->deduction_amt) / 100;
    //                                 $total_deduction +=  round($cal_amount);
    //                                 $remaining_total_deduction +=  round($cal_amount1);
    //                             } else {
    //                                 if ($applicable_present_days != 0) {
    //                                     $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $deduction->deduction_amt) / 100;
    //                                     $cal_amount1 = (($remaining_based_on_present_day + $salary_based_on_applicable_day) * $deduction->deduction_amt) / 100;
    //                                 } else {
    //                                     $cal_amount = ($salary_based_on_present_day * $deduction->deduction_amt) / 100;
    //                                     $cal_amount1 = ($remaining_based_on_present_day * $deduction->deduction_amt) / 100;
    //                                 }
    //                                 $total_deduction += round($cal_amount);
    //                                 $remaining_total_deduction +=  round($cal_amount1);
    //                             }

    //                             // Store deduction details in arrays
    //                             $deduction_ids_array[] = $deduction->deduction_id;
    //                             $deduction_amt_array[] = round($cal_amount);
    //                             $deduction_type_array[] = $deduction->deduction_type;


    //                             $remaining_deduction_ids_array[] = $deduction->deduction_id;
    //                             $remaining_deduction_amt_array[] = round($cal_amount1);
    //                             $remaining_deduction_type_array[] = $deduction->deduction_type;
    //                         }
    //                     }
    //                 }

    //                 // Implode arrays for deduction
    //                 $implode_deduction_ids_array = implode(',', $deduction_ids_array);
    //                 $implode_deduction_amt_array = implode(',', $deduction_amt_array);
    //                 $implode_deduction_type_array = implode(',', $deduction_type_array);

    //                 $implode_remaining_deduction_ids_array = implode(',', $remaining_deduction_ids_array);
    //                 $implode_remaining_deduction_amt_array = implode(',', $remaining_deduction_amt_array);
    //                 $implode_remaining_deduction_type_array = implode(',', $remaining_deduction_type_array);


    //                 // Calculate total deduction including stamp duty
    //                 $total_deduction += $STAMP_DUTY;
    //                 $remaining_total_deduction += $STAMP_DUTY;

    //                 // Calculate net salary
    //                 if ($applicable_present_days != 0) {
    //                     $Net_Salary = ($salary_based_on_present_day + $salary_based_on_applicable_day) + $total_allowance - ($total_deduction);
    //                     $Remaining_Net_Salary = $remaining_based_on_present_day  + $remaining_total_allowance - ($remaining_total_deduction);
    //                 } else {
    //                     $Net_Salary = $salary_based_on_present_day + $total_allowance - ($total_deduction);
    //                     $Remaining_Net_Salary = $remaining_based_on_present_day + $remaining_total_allowance - ($remaining_total_deduction);
    //                 }

    //                 if ($employee->doj > '2005-11-01') {
    //                     $bncmc_share = ($salary_based_on_present_day + $bncmc_share_da) * 14 / 100;
    //                     $share =  round($bncmc_share);

    //                     $remainign_bncmc_share = ($remaining_based_on_present_day + $remaining_bncmc_share_da) * 14 / 100;
    //                     $remaining_share =  round($remainign_bncmc_share);
    //                 }

    //                 // Get supplimentary Data
    //                 $supplimentary_data = SupplimentaryBill::where('id',$freeze_attendance->supplimentary_ids)
    //                 ->where('status',1)->first();

    //                 if(!empty($supplimentary_data)){
    //                 SupplimentaryBill::where('id', $supplimentary_data->id)
    //                 ->update(['status' => 1]);

    //                 $supplimentary_ids = $supplimentary_data->id;

    //                 $supplimentary_status = 1;
    //                 }else{

    //                     $supplimentary_data = SupplimentaryBill::where('employee_id',$employee->id)
    //                     ->where('status',0)
    //                     ->where('to_date','<=',$freeze_attendance->to_date)->first();

    //                     if(!empty($supplimentary_data)){
    //                         SupplimentaryBill::where('id', $supplimentary_data->id)
    //                         ->update(['status' => 1]);

    //                         $supplimentary_ids = $supplimentary_data->id;

    //                         $supplimentary_status = 1;
    //                     }else{
    //                         $supplimentary_status = 0;
    //                         $supplimentary_ids = null;
    //                     }
    //                 }

    //                 // Create FreezeAttendance record
    //                 $data = FreezeAttendance::updateOrCreate(
    //                     [
    //                         'id' => $freeze_attendance->id,
    //                         'from_date' => $freeze_attendance->from_date,
    //                         'to_date' => $freeze_attendance->to_date,
    //                     ],
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'Emp_Code' => $employee->employee_id,
    //                         'freeze_status' => 1,
    //                         'attendance_UId' => NULL, //pending
    //                         'ward_id' => $employee->ward_id,
    //                         'department_id' => $employee->department_id,
    //                         'designation_id' => $employee->designation_id,
    //                         'clas_id' => $employee->clas_id,
    //                         'from_date' => $freeze_attendance->from_date,
    //                         'to_date' => $freeze_attendance->to_date,
    //                         'month' => $freeze_attendance->month,
    //                         'financial_year_id' => $freeze_attendance->financial_year_id,
    //                         'present_day' =>  $present_days,
    //                         'basic_salary' => ($applicable_present_days != 0) ? ($salary_based_on_applicable_day + $salary_based_on_present_day) : $salary_based_on_present_day,
    //                         'actual_basic' =>  $basicSalary,
    //                         'grade_pay' =>  $gradePay,
    //                         'allowance_Id' =>  $implode_allowance_ids_array,
    //                         'allowance_Amt' =>  $implode_allowance_amt_array,
    //                         'allowance_Type' =>  $implode_allowance_type_array,
    //                         'festival_allowance'            => $totalFestivalAdvance,
    //                         'festival_allowance_id'         => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
    //                         'total_allowance' =>  $total_allowance,
    //                         'deduction_Id' =>  $implode_deduction_ids_array,
    //                         'deduction_Amt' =>  $implode_deduction_amt_array,
    //                         'deduction_Type' =>  $implode_deduction_type_array,
    //                         'total_deduction' =>  $total_deduction,
    //                         'stamp_duty' =>  $STAMP_DUTY,
    //                         'loan_deduction_id' =>  $implode_loan_ids_array,
    //                         'loan_deduction_amt' =>  $implode_loan_amt_array,
    //                         'loan_deduction_bank_id' =>  $implode_loan_bank_array,
    //                         'total_loan_deduction' =>  $total_loan_deduction,
    //                         'lic_deduction_id' =>  $implode_lic_ids_array,
    //                         'lic_deduction_amt' =>  $implode_lic_amt_array,
    //                         'total_lic_deduction' =>  $total_lic_deduction,
    //                         'festival_deduction_id'         => $implode_festAdv_ids_array,
    //                         'festival_deduction_amt'        => $implode_festAdv_amt_array,
    //                         'total_festival_deduction'      => $total_festAdv_deduction,
    //                         'net_salary' =>  $Net_Salary,
    //                         'emp_name' =>  $employee?->fname . " " . $employee?->mname . " " . $employee?->lname,
    //                         'pf_account_no' => ($employee?->pf_account_no) ? $employee?->pf_account_no : 0,
    //                         'pay_band_scale' => $payScales->pay_band_scale,
    //                         'grade_pay_scale' => $payScales->grade_pay_name,
    //                         'date_of_birth' => $employee?->dob,
    //                         'date_of_appointment' => $employee?->doj,
    //                         'date_of_retirement' => $employee?->retirement_date,
    //                         'bank_account_number' => $employee?->account_no,
    //                         'phone_no' => $employee?->mobile_number,
    //                         'corporation_share_da' => $share,
    //                         'salary_percentage' => $salary_percent,
    //                         'supplimentary_status'  => $supplimentary_status,
    //                         'supplimentary_ids'=> $supplimentary_ids,
    //                     ]
    //                 );

    //                 // Remaining Salary
    //                 RemainingFreezeSalary::updateOrCreate(
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'freeze_attendance_id' => $data->id,
    //                     ],
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'freeze_attendance_id' => $data->id,
    //                         'Emp_Code' => $employee->employee_id,
    //                         'from_date' => $freeze_attendance->from_date,
    //                         'to_date' => $freeze_attendance->to_date,
    //                         'month' => $freeze_attendance->month,
    //                         'present_day' => $numberOfDaysInMonth,
    //                         'basic_salary' => $remaining_based_on_present_day,
    //                         'actual_basic' => $basicSalary,
    //                         'grade_pay' => $gradePay,
    //                         'allowance_Id' => $implode_remaining_allowance_ids_array,
    //                         'allowance_Amt' => $implode_remaining_allowance_amt_array,
    //                         'allowance_Type' => $implode_remaining_allowance_type_array,
    //                         'festival_allowance'            => $remainingtotalFestivalAdvance,
    //                         'festival_allowance_id'         => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
    //                         'total_allowance' => $remaining_total_allowance,
    //                         'deduction_Id' => $implode_remaining_deduction_ids_array,
    //                         'deduction_Amt' => $implode_remaining_deduction_amt_array,
    //                         'deduction_Type' => $implode_remaining_deduction_type_array,
    //                         'total_deduction' => $remaining_total_deduction,
    //                         'stamp_duty' => $STAMP_DUTY,
    //                         'loan_deduction_id' => (!empty($implode_loan_remaining_ids_array) && $check_status != 1) ? $implode_loan_remaining_ids_array : '',
    //                         'loan_deduction_amt' => (!empty($implode_loan_remaining_amt_array) && $check_status != 1) ? $implode_loan_remaining_amt_array : '',
    //                         'loan_deduction_bank_id' => (!empty($implode_loan_remaining_bank_array) && $check_status != 1) ? $implode_loan_remaining_bank_array : '',
    //                         'total_loan_deduction' => ($check_status != 1) ? $total_loan_remaining_deduction : 0,
    //                         'lic_deduction_id' => (!empty($implode_lic_remaining_ids_array) && $check_status != 1) ? $implode_lic_remaining_ids_array : '',
    //                         'lic_deduction_amt' => (!empty($implode_lic_remaining_amt_array) && $check_status != 1) ? $implode_lic_remaining_amt_array : '',
    //                         'total_lic_deduction' => ($check_status != 1) ? $total_lic_remaining_deduction : 0,
    //                         'festival_deduction_id'         => (!empty($implode_festAdv_remaining_ids_array) && $check_status != 1) ? $implode_festAdv_remaining_ids_array : '',
    //                         'festival_deduction_amt'        => (!empty($implode_festAdv_remaining_amt_array) && $check_status != 1) ? $implode_festAdv_remaining_amt_array : '',
    //                         'total_festival_deduction'      => ($check_status != 1) ? $total_festAdv_remaining_deduction : 0,

    //                         'net_salary' => $Remaining_Net_Salary,
    //                         'corporation_share_da' => $remaining_share,
    //                         'salary_percentage'     => $remaining_percent,
    //                     ]
    //                 );

    //                 // Store PF Data
    //                 EmployeeProvidentFund::updateOrCreate(
    //                     [
    //                         'employee_id' => $employee->id,
    //                         'current_month' => $freeze_attendance->to_date,
    //                         'salary_month' => $freeze_attendance->from_date,
    //                     ],
    //                     [
    //                         'employee_id'       => $employee->id,
    //                         'Emp_Code'          => $employee->employee_id,
    //                         'pf_account_no'     => $employee->pf_account_no,
    //                         'current_month'     => $freeze_attendance->to_date,
    //                         'salary_month'      => $freeze_attendance->from_date,
    //                         'financial_year_id' => $freeze_attendance->financial_year_id,
    //                         'pf_contribution'   => ($pf_contribution) ? $pf_contribution : 0,
    //                         'pf_loan'           => $pf_loan,
    //                     ]
    //                 );

    //                 if(!empty($getFestivalAllowance)){
    //                     EmployeeFestivalAdvance::where('id', $getFestivalAllowance->id)
    //                     ->update(['apply_status' => 2]);
    //                 }
    //             }
    //         }
    //         return response()->json(['success' => 'Freeze successfully!']);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return response()->json(['error' => 'An error occurred while Freeze the employee'], 500);
    //     }
    // }
    public function freezeEmployee(Request $request)
    {

        try {
            $freeze_attendance = FreezeAttendance::find($request->id);
            if (!$freeze_attendance) {
                return response()->json(['error' => 'Employee not found'], 404);
            }



            $total_allowance = 0;
            $remaining_total_allowance = 0;

            $total_deduction = 0;
            $remaining_total_deduction = 0;

            $total_loan_deduction = 0;
            $total_loan_remaining_deduction = 0;

            $allowance_ids_array = [];
            $allowance_amt_array = [];
            $allowance_type_array = [];

            $remaining_allowance_ids_array = [];
            $remaining_allowance_amt_array = [];
            $remaining_allowance_type_array = [];

            $deduction_ids_array = [];
            $deduction_amt_array = [];
            $deduction_type_array = [];

            $remaining_deduction_ids_array = [];
            $remaining_deduction_amt_array = [];
            $remaining_deduction_type_array = [];

            $loan_ids_array = [];
            $loan_amt_array = [];
            $loan_deduction_bank_id = [];

            $STAMP_DUTY = 1;

            $Net_Salary = 0;
            $Remaining_Net_Salary = 0;

            $bncmc_share_da = 0;
            $remaining_bncmc_share_da = 0;
            $basicandDA = 0;
            $remaining_basicandDA = 0;
            $share = 0;
            $remaining_share = 0;
            $employee_share = 0;
            $employee_remaining_share = 0;

            $pf_loan = 0;
            $pf_contribution = 0;

            // LIC Deduction
            $lic_ids_array = [];
            $lic_amt_array = [];

            $total_lic_deduction = 0;
            $total_lic_remaining_deduction = 0;

            // festival advance Deduction
            $festadv_ids_array = [];
            $festadv_amt_array = [];
            $total_festAdv_deduction = 0;
            $total_festAdv_remaining_deduction = 0;

            // festival advance Allowance
            $totalFestivalAdvance = 0;
            $remainingtotalFestivalAdvance = 0;

            $startDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
            $endDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->to_date);
            $numberOfDaysInMonth = $startDate->diffInDays($endDate);
            $numberOfDaysInMonth += 1;

            $employee = Employee::with('salary', 'employee_status')->where('id', $freeze_attendance->employee_id)->first();

            $is_employee_retire_within_3_months = 0;
            if ($employee->salary) {

                $is_employee_retire_within_3_months = Carbon::parse($employee->retirement_date)->lessThanOrEqualTo(Carbon::now()->addMonths(3));

                // Fetch loans for the employee within the given date range
                $loanArr = EmployeeMonthlyLoan::where('from_date', $freeze_attendance->from_date)
                    ->where('to_date', $freeze_attendance->to_date)
                    ->where('employee_id', $employee->id)
                    ->get();

                // LIC Dedcution Data Get
                $licArr = EmployeeMonthlyLic::where('from_date', $freeze_attendance->from_date)
                    ->where('to_date', $freeze_attendance->to_date)
                    ->where('employee_id', $employee->id)
                    ->get();

                // Festival advance Dedcution Data Get
                $festAdvArr = EmployeeMonthlyFestivalAdvance::where('from_date', $freeze_attendance->from_date)
                    ->where('to_date', $freeze_attendance->to_date)
                    ->where('employee_id', $employee->id)
                    ->get();

                // Fetch pay scales for the employee within the given salary
                $payScales = PayScale::where('id', $employee->salary->pay_scale_id)
                    ->first();

                $check_status = 0;
                if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {
                } else {
                    $check_status = 1;
                }

                $get_present_days = Attendance::where('employee_id', $employee->id)
                    ->where('from_date', $freeze_attendance->from_date)
                    ->where('to_date', $freeze_attendance->to_date)->first();

                $present_days_new = $get_present_days->total_present_days + $get_present_days->total_leave;

                // Loan Logic
                foreach ($loanArr as $loan) {

                    $loan_ids_array[] = $loan->id;
                    $loan_amt_array[] = $loan->installment_amount;
                    $loan_deduction_bank_id[] = $loan->loan_id;

                    $total_loan_deduction += $loan->installment_amount;
                    $total_deduction += $loan->installment_amount;

                    if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                    } else {
                        if ($employee?->employee_status?->is_salary_applicable == 0) {
                            $total_loan_remaining_deduction += $loan->installment_amount;
                            $remaining_total_deduction += $loan->installment_amount;
                        }
                    }

                    // To store pf loan
                    if ($loan->id == 1) {
                        $pf_loan += $loan->installment_amount;
                    }
                }

                $implode_loan_ids_array = implode(',', $loan_ids_array);
                $implode_loan_amt_array = implode(',', $loan_amt_array);
                $implode_loan_bank_array = implode(',', $loan_deduction_bank_id);

                // LIC Logic
                foreach ($licArr as $lic) {
                    $lic_ids_array[] = $lic->id;
                    $lic_amt_array[] = $lic->installment_amt;

                    $total_lic_deduction += $lic->installment_amt;
                    $total_deduction += $lic->installment_amt;

                    if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                    } else {
                        if ($employee?->employee_status?->is_salary_applicable == 0) {
                            $total_lic_remaining_deduction += $lic->installment_amt;
                            $remaining_total_deduction += $lic->installment_amt;
                        }
                    }
                }

                $implode_lic_ids_array = implode(',', $lic_ids_array);
                $implode_lic_amt_array = implode(',', $lic_amt_array);

                // Festival advance deduction logic start

                foreach ($festAdvArr as $festAdv) {
                    $festadv_ids_array[] = $festAdv->id;
                    $festadv_amt_array[] = $festAdv->installment_amount;

                    $total_festAdv_deduction    += $festAdv->installment_amount;
                    $total_deduction            += $festAdv->installment_amount;

                    if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                    } else {
                        if ($employee?->employee_status?->is_salary_applicable == 0) {
                            $total_festAdv_remaining_deduction  += $festAdv->installment_amount;
                            $remaining_total_deduction          += $festAdv->installment_amount;
                        }
                    }
                }

                $implode_festAdv_ids_array = implode(',', $festadv_ids_array);
                $implode_festAdv_amt_array = implode(',', $festadv_amt_array);

                // Festival advance deduction logic end

                if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days_new < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                } else {
                    $implode_loan_remaining_ids_array = implode(',', $loan_ids_array);
                    $implode_loan_remaining_amt_array = implode(',', $loan_amt_array);
                    $implode_loan_remaining_bank_array = implode(',', $loan_deduction_bank_id);

                    $implode_lic_remaining_ids_array = implode(',', $lic_ids_array);
                    $implode_lic_remaining_amt_array = implode(',', $lic_amt_array);

                    $implode_festAdv_remaining_ids_array = implode(',', $festadv_ids_array);
                    $implode_festAdv_remaining_amt_array = implode(',', $festadv_amt_array);
                }

                // Festival Advance Allowance Logic Start
                $getFestivalAllowance = EmployeeFestivalAdvance::where('employee_id', $employee->id)
                    ->where('applicable_month', $freeze_attendance->month)
                    ->where('apply_status', 2)->first();

                if (!empty($getFestivalAllowance)) {
                    $totalFestivalAdvance           = $getFestivalAllowance->total_amount;
                    $remainingtotalFestivalAdvance  = $getFestivalAllowance->total_amount;

                    $total_allowance                = $getFestivalAllowance->total_amount;
                    $remaining_total_allowance      = $getFestivalAllowance->total_amount;
                }
                // Festival Advance Allowance Logic End

                if ($employee?->employee_status === null || ($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 0)) {

                    $fromDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
                    if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {

                        if ($employee?->employee_status?->applicable_date > $freeze_attendance->to_date) {
                            // $present_days = $numberOfDaysInMonth;
                            $present_days = $get_present_days->total_present_days + $get_present_days->total_leave;
                            $actual_present_days = $get_present_days->total_present_days;
                            $leave_days = $get_present_days->total_leave;
                        } else {
                            $present_days = $fromDate->diffInDays($employee?->employee_status?->applicable_date) + 1;
                            $actual_present_days = $present_days;
                            $leave_days = 0;
                        }
                    } elseif ($employee?->employee_status && $employee?->employee_status?->applicable_date <= $freeze_attendance->from_date) {
                        $present_days = 0;
                        $actual_present_days = 0;
                        $leave_days = 0;
                    } else {
                        // Assuming present days for now
                        // $present_days = $numberOfDaysInMonth;
                        $present_days = $get_present_days->total_present_days + $get_present_days->total_leave;
                        $actual_present_days = $get_present_days->total_present_days;
                        $leave_days = $get_present_days->total_leave;
                    }

                    // Assuming present days for now
                    // $present_days = $numberOfDaysInMonth;

                    // Calculate salary per day and salary based on present days
                    $basicSalary = optional($employee->salary)->basic_salary;
                    $gradePay = optional($employee->salary)->grade_pay;
                    $basicPlusGpay = $basicSalary + $gradePay;
                    $salary_per_day = $basicSalary / $numberOfDaysInMonth; //change if salary calculates on basic plus gpay
                    $earned_basic_salary = round($salary_per_day * $actual_present_days);
                    $leave_pay = round($salary_per_day * $leave_days);

                    // $salary_based_on_present_day = round($salary_per_day * $present_days);
                    $salary_based_on_present_day = $earned_basic_salary + $leave_pay;

                    if ($present_days != $numberOfDaysInMonth) {
                        $remaining_earned_basic_salary = round($salary_per_day * ($numberOfDaysInMonth - $actual_present_days));
                        $remaining_leave_pay = round($salary_per_day * ($numberOfDaysInMonth - $leave_days));

                        // $remaining_salary_based_on_present_day = round($salary_per_day * ($numberOfDaysInMonth - $present_days));
                        $remaining_salary_based_on_present_day = $remaining_earned_basic_salary + $remaining_leave_pay;
                    } else {
                        $remaining_earned_basic_salary = round($salary_per_day * $actual_present_days);
                        $remaining_leave_pay = round($salary_per_day * $leave_days);

                        // $remaining_salary_based_on_present_day = round($salary_per_day * $present_days);
                        $remaining_salary_based_on_present_day = $remaining_earned_basic_salary + $remaining_leave_pay;
                    }

                    // Allowance logic
                    foreach ($employee->employee_allowances as $allowance) {
                        $allowanceMaster = Allowance::find($allowance->allowance_id);

                        if ($allowance->is_active == 1 && $allowanceMaster) {
                            if ($allowance->allowance_type == 1) {
                                if ($allowanceMaster->calculation == 1) {
                                    $total_allowance += $allowance->allowance_amt;

                                    // Store allowance details in arrays
                                    $allowance_ids_array[] = $allowance->allowance_id;
                                    $allowance_amt_array[] = $allowance->allowance_amt;
                                    $allowance_type_array[] = $allowance->allowance_type;

                                    // if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                                    // }
                                    if ((!empty($employee?->employee_status) && $employee?->employee_status?->applicable_date > $freeze_attendance->from_date)) {
                                    } else if (($present_days <= 0 && empty($employee?->employee_status)) && $allowance->allowance_id != 12) {
                                        $remaining_total_allowance += $allowance->allowance_amt;
                                        $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                        $remaining_allowance_amt_array[] = $allowance->allowance_amt;
                                        $remaining_allowance_type_array[] = $allowance->allowance_type;
                                    } else if ($present_days <= 0) {
                                        $remaining_total_allowance += $allowance->allowance_amt;
                                        $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                        $remaining_allowance_amt_array[] = $allowance->allowance_amt;
                                        $remaining_allowance_type_array[] = $allowance->allowance_type;
                                    }
                                } else {
                                    $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * $present_days;
                                    $total_allowance += round($dynamicAllowanceAmt);

                                    // if status apply and applicable
                                    $remaining_dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($numberOfDaysInMonth - $present_days);
                                    $remaining_total_allowance += round($remaining_dynamicAllowanceAmt);


                                    // Store allowance details in arrays
                                    $allowance_ids_array[] = $allowance->allowance_id;
                                    $allowance_amt_array[] = round($dynamicAllowanceAmt);
                                    $allowance_type_array[] = $allowance->allowance_type;

                                    $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                    $remaining_allowance_amt_array[] = round($remaining_dynamicAllowanceAmt);
                                    $remaining_allowance_type_array[] = $allowance->allowance_type;
                                }
                            } elseif ($allowance->allowance_type == 2) {

                                if ($allowance->allowance_id == 1) {
                                    $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
                                    $cal_amount2 = ($remaining_salary_based_on_present_day * $allowance->allowance_amt) / 100;

                                    $bncmc_share_da = $cal_amount;
                                    $basicandDA =  $cal_amount + $salary_based_on_present_day;

                                    $remaining_bncmc_share_da = $cal_amount2;
                                    $remaining_basicandDA =  $cal_amount2 + $remaining_salary_based_on_present_day;

                                    $total_allowance +=  round($cal_amount);
                                    $remaining_total_allowance += round($cal_amount2);
                                } else {
                                    $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
                                    $cal_amount2 = ($remaining_salary_based_on_present_day * $allowance->allowance_amt) / 100;

                                    $total_allowance += round($cal_amount);
                                    $remaining_total_allowance += round($cal_amount2);
                                }
                                // Store allowance details in arrays
                                $allowance_ids_array[] = $allowance->allowance_id;
                                $allowance_amt_array[] = round($cal_amount);
                                $allowance_type_array[] = $allowance->allowance_type;

                                $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                $remaining_allowance_amt_array[] = round($cal_amount2);
                                $remaining_allowance_type_array[] = $allowance->allowance_type;
                            }
                        }
                    }

                    // Implode arrays for allowance
                    $implode_allowance_ids_array = implode(',', $allowance_ids_array);
                    $implode_allowance_amt_array = implode(',', $allowance_amt_array);
                    $implode_allowance_type_array = implode(',', $allowance_type_array);

                    $implode_remaining_allowance_ids_array = implode(',', $remaining_allowance_ids_array);
                    $implode_remaining_allowance_amt_array = implode(',', $remaining_allowance_amt_array);
                    $implode_remaining_allowance_type_array = implode(',', $remaining_allowance_type_array);


                    // Deduction logic
                    foreach ($employee->employee_deductions as $deduction) {

                        $deductionMaster = Deduction::find($deduction->deduction_id);

                        if ($deduction->is_active == 1 && $deductionMaster) {

                            if ($deduction->deduction_type == 1) {
                                if ($deductionMaster->calculation == 1) {

                                    if ($is_employee_retire_within_3_months && $deduction->deduction_id == 3) {
                                    } else {

                                        $total_deduction += $deduction->deduction_amt;

                                        if ($deduction->deduction_id == 3) {
                                            $pf_contribution += $deduction->deduction_amt;
                                        }

                                        // Store deduction details in arrays
                                        $deduction_ids_array[] = $deduction->deduction_id;
                                        $deduction_amt_array[] = $deduction->deduction_amt;
                                        $deduction_type_array[] = $deduction->deduction_type;
                                    }

                                    // if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) {
                                    // }
                                    if ((!empty($employee?->employee_status) && $employee?->employee_status?->applicable_date > $freeze_attendance->from_date)) {
                                    } else if (($present_days <= 0 && empty($employee?->employee_status)) &&
                                        (!$is_employee_retire_within_3_months || $deduction->deduction_id != 3)
                                    ) {
                                        $remaining_total_deduction += $deduction->deduction_amt;
                                        $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                        $remaining_deduction_amt_array[] = $deduction->deduction_amt;
                                        $remaining_deduction_type_array[] = $deduction->deduction_type;
                                    } else if ($present_days <= 0 && (!$is_employee_retire_within_3_months || $deduction->deduction_id != 3)) {
                                        $remaining_total_deduction += $deduction->deduction_amt;
                                        $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                        $remaining_deduction_amt_array[] = $deduction->deduction_amt;
                                        $remaining_deduction_type_array[] = $deduction->deduction_type;
                                    }
                                } else {

                                    if ($is_employee_retire_within_3_months && $deduction->deduction_id == 3) {
                                    } else {
                                        $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * $present_days;
                                        $total_deduction += round($dynamicDeductionAmt);

                                        // only applicable if status and applicable date in between
                                        $remaining_dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($numberOfDaysInMonth - $present_days);
                                        $remaining_total_deduction += round($remaining_dynamicDeductionAmt);

                                        if ($deduction->deduction_id == 3) {
                                            $pf_contribution += round($dynamicDeductionAmt);
                                        }
                                        // Store deduction details in arrays
                                        $deduction_ids_array[] = $deduction->deduction_id;
                                        $deduction_amt_array[] = round($dynamicDeductionAmt);
                                        $deduction_type_array[] = $deduction->deduction_type;

                                        $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                        $remaining_deduction_amt_array[] = round($remaining_dynamicDeductionAmt);
                                        $remaining_deduction_type_array[] = $deduction->deduction_type;
                                    }
                                }
                            } elseif ($deduction->deduction_type == 2) {

                                if ($deduction->deduction_id == 4) {

                                    $cal_amount = ($basicandDA * $deduction->deduction_amt) / 100;
                                    $total_deduction +=  round($cal_amount);

                                    $cal_amount3 = ($remaining_basicandDA * $deduction->deduction_amt) / 100;

                                    $remaining_total_deduction +=  round($cal_amount3);
                                } else {

                                    if ($is_employee_retire_within_3_months && $deduction->deduction_id == 3) {
                                    } else {
                                        $cal_amount = ($salary_based_on_present_day * $deduction->deduction_amt) / 100;
                                        $total_deduction += round($cal_amount);

                                        $cal_amount3 = ($remaining_salary_based_on_present_day * $deduction->deduction_amt) / 100;
                                        $remaining_total_deduction += round($cal_amount3);

                                        if ($deduction->deduction_id == 3) {
                                            $pf_contribution += round($cal_amount);
                                        }
                                    }
                                }

                                if ($is_employee_retire_within_3_months && $deduction->deduction_id == 3) {
                                } else {
                                    // Store deduction details in arrays
                                    $deduction_ids_array[] = $deduction->deduction_id;
                                    $deduction_amt_array[] = round($cal_amount);
                                    $deduction_type_array[] = $deduction->deduction_type;

                                    $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                    $remaining_deduction_amt_array[] = round($cal_amount3);
                                    $remaining_deduction_type_array[] = $deduction->deduction_type;
                                }
                            }
                        }
                    }

                    // Implode arrays for deduction
                    $implode_deduction_ids_array = implode(',', $deduction_ids_array);
                    $implode_deduction_amt_array = implode(',', $deduction_amt_array);
                    $implode_deduction_type_array = implode(',', $deduction_type_array);

                    $implode_remaining_deduction_ids_array = implode(',', $remaining_deduction_ids_array);
                    $implode_remaining_deduction_amt_array = implode(',', $remaining_deduction_amt_array);
                    $implode_remaining_deduction_type_array = implode(',', $remaining_deduction_type_array);

                    // Calculate total deduction including stamp duty
                    $total_deduction += $STAMP_DUTY;
                    $remaining_total_deduction += $STAMP_DUTY;


                    if ($employee->doj > '2005-11-01' && $employee->department_id != 3 && !$is_employee_retire_within_3_months && $employee->is_dcps_applicable) {
                        $bncmc_share = ($salary_based_on_present_day + $bncmc_share_da) * 14 / 100;
                        $share =  round($bncmc_share);

                        $employee_share_calculate = ($salary_based_on_present_day + $bncmc_share_da) * 10 / 100;
                        $employee_share = round($employee_share_calculate);

                        $remaining_bncmc_share = ($remaining_salary_based_on_present_day + $remaining_bncmc_share_da) * 14 / 100;
                        $remaining_share =  round($remaining_bncmc_share);

                        $remaining_employee_share = ($remaining_salary_based_on_present_day + $remaining_bncmc_share_da) * 10 / 100;
                        $employee_remaining_share  =  round($remaining_employee_share);

                        $total_deduction            += $employee_share;
                        $remaining_total_deduction  += $employee_remaining_share;
                    }

                    // Calculate net salary
                    $Net_Salary = $salary_based_on_present_day + $total_allowance - ($total_deduction);
                    $Remaining_Net_Salary = $remaining_salary_based_on_present_day + $remaining_total_allowance - ($remaining_total_deduction);


                    // Get supplimentary Data
                    // $supplimentary_data = SupplimentaryBill::where('id', $freeze_attendance->supplimentary_ids)
                    //     ->where('status', 1)->first();

                    // if (!empty($supplimentary_data)) {
                    //     SupplimentaryBill::where('id', $supplimentary_data->id)
                    //         ->update(['status' => 1]);

                    //     $supplimentary_ids = $supplimentary_data->id;

                    //     $supplimentary_status = 1;
                    // } else {

                    //     $supplimentary_data = SupplimentaryBill::where('employee_id', $employee->id)
                    //         ->where('status', 0)
                    //         ->where('to_date', '<=', $freeze_attendance->to_date)->first();

                    //     if (!empty($supplimentary_data)) {
                    //         SupplimentaryBill::where('id', $supplimentary_data->id)
                    //             ->update(['status' => 1]);

                    //         $supplimentary_ids = $supplimentary_data->id;
                    //         $supplimentary_status = 1;
                    //     } else {
                    //         $supplimentary_status = 0;
                    //         $supplimentary_ids = null;
                    //     }
                    // }

                    // DA Differance
                    $fetch_employee_da_differance = EmployeeDaDifferance::where('employee_id', $employee->id)
                        ->where('Emp_Code', $employee->employee_id)
                        ->where('status', 0)
                        ->where('given_month', $freeze_attendance->month)
                        ->first();

                    if ($fetch_employee_da_differance) {

                        if ($present_days != 0) {

                            $da_differance = $fetch_employee_da_differance->differance;
                            $da_differance_id = $fetch_employee_da_differance->id;

                            $fetch_employee_da_differance->where('id', $fetch_employee_da_differance->id)
                                ->update([
                                    'status'    => 1,
                                ]);
                        } else {

                            $da_differance = 0;
                            $da_differance_id = null;

                            $given_month = ($fetch_employee_da_differance->given_month % 12) + 1;
                            $differance_in_percent = ($fetch_employee_da_differance->DA_newRate - $fetch_employee_da_differance->DA_currentRate);

                            $fetch_employee_da_differance->where('id', $fetch_employee_da_differance->id)
                                ->update([
                                    'given_month'   => $given_month,
                                    'no_of_month'   => $fetch_employee_da_differance->no_of_month + 1,
                                    'differance'    => $fetch_employee_da_differance->differance + ($employee->salary->basic_salary * $differance_in_percent) / 100,
                                    'status'        => 0,
                                ]);
                        }
                    } else {

                        if ($freeze_attendance->employee_da_differance_id != NULL) {
                            $fetch_employee_da_differance_new = EmployeeDaDifferance::where('id', $freeze_attendance->employee_da_differance_id)->first();

                            if ($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 0) {

                                $given_month = ($fetch_employee_da_differance_new->given_month % 12) + 1;
                                $differance_in_percent = ($fetch_employee_da_differance_new->DA_newRate - $fetch_employee_da_differance_new->DA_currentRate);

                                $fetch_employee_da_differance_new->where('id', $fetch_employee_da_differance_new->id)
                                    ->update([
                                        'given_month'   => $given_month,
                                        'no_of_month'   => $fetch_employee_da_differance_new->no_of_month + 1,
                                        'differance'    => $fetch_employee_da_differance_new->differance + ($employee->salary->basic_salary * $differance_in_percent) / 100,
                                        'status'        => 0,
                                    ]);
                                $da_differance = 0;
                                $da_differance_id = null;
                            } else {
                                $da_differance = $fetch_employee_da_differance_new->differance;
                                $da_differance_id = $fetch_employee_da_differance_new->id;
                            }
                        } else {
                            $da_differance = 0;
                            $da_differance_id = null;
                        }
                    }

                    // if ($get_present_days->total_leave > 0 && $present_days > 0) {
                    //     $leavepay = ($salary_based_on_present_day / $present_days) * $get_present_days->total_leave;
                    //     $earned_basic = $salary_based_on_present_day - $leavepay;
                    // } else {
                    //     $leavepay = 0;
                    //     $earned_basic = $salary_based_on_present_day;
                    // }
                    // Create FreezeAttendance record

                    $data = FreezeAttendance::updateOrCreate(
                        [
                            'id' => $freeze_attendance->id,
                            'from_date' => $freeze_attendance->from_date,
                            'to_date' => $freeze_attendance->to_date,
                        ],
                        [
                            'employee_id' => $employee->id,
                            'Emp_Code' => $employee->employee_id,
                            'freeze_status' => 1,
                            'attendance_UId' => NULL, //pending
                            'ward_id' => $employee->ward_id,
                            'department_id' => $employee->department_id,
                            'designation_id' => $employee->designation_id,
                            'clas_id' => $employee->clas_id,
                            'from_date' => $freeze_attendance->from_date,
                            'to_date' => $freeze_attendance->to_date,
                            'month' => $freeze_attendance->month,
                            'financial_year_id' => $freeze_attendance->financial_year_id,
                            'present_day' =>  $present_days,
                            'actual_present_day'        =>  $get_present_days->total_present_days,
                            'total_leave'               =>  $get_present_days->total_leave,
                            'basic_salary' => ($present_days != 0) ? $salary_based_on_present_day : 0,
                            'earned_basic'              => ($present_days != 0) ? $earned_basic_salary : 0,
                            'leave_pay'                 => $leave_pay,

                            'actual_basic' => $basicSalary,
                            'grade_pay' => $gradePay,
                            'allowance_Id' => ($present_days != 0 && $basicSalary != 0) ? $implode_allowance_ids_array : '',
                            'allowance_Amt' => ($present_days != 0 && $basicSalary != 0) ? $implode_allowance_amt_array : '',
                            'allowance_Type' => ($present_days != 0 && $basicSalary != 0) ? $implode_allowance_type_array : '',
                            'festival_allowance'        => ($present_days != 0 && $basicSalary != 0) ? $totalFestivalAdvance : 0,
                            'festival_allowance_id'     => ($present_days != 0 && $basicSalary != 0 && !empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,

                            'total_allowance' => ($present_days != 0 && $basicSalary != 0) ? $total_allowance : 0,
                            'deduction_Id' => ($present_days != 0 && $basicSalary != 0) ? $implode_deduction_ids_array : '',
                            'deduction_Amt' => ($present_days != 0 && $basicSalary != 0) ? $implode_deduction_amt_array : '',
                            'deduction_Type' => ($present_days != 0 && $basicSalary != 0) ? $implode_deduction_type_array : '',
                            'total_deduction' => ($present_days != 0 && $basicSalary != 0) ? $total_deduction : 0,
                            'stamp_duty' => ($present_days != 0 && $basicSalary != 0) ? $STAMP_DUTY : 0,
                            'loan_deduction_id' => ($present_days != 0 && $basicSalary != 0) ? $implode_loan_ids_array : '',
                            'loan_deduction_amt' => ($present_days != 0 && $basicSalary != 0) ? $implode_loan_amt_array : '',
                            'loan_deduction_bank_id' => ($present_days != 0 && $basicSalary != 0) ? $implode_loan_bank_array : '',
                            'total_loan_deduction' => ($present_days != 0 && $basicSalary != 0) ? $total_loan_deduction : 0,
                            'lic_deduction_id' => ($present_days != 0 && $basicSalary != 0) ? $implode_lic_ids_array : '',
                            'lic_deduction_amt' => ($present_days != 0 && $basicSalary != 0) ? $implode_lic_amt_array : '',
                            'total_lic_deduction' => ($present_days != 0 && $basicSalary != 0) ? $total_lic_deduction : 0,

                            'festival_deduction_id'     => ($present_days != 0 && $basicSalary != 0) ? $implode_festAdv_ids_array : '',
                            'festival_deduction_amt'    => ($present_days != 0 && $basicSalary != 0) ? $implode_festAdv_amt_array : '',
                            'total_festival_deduction'  => ($present_days != 0 && $basicSalary != 0) ? $total_festAdv_deduction : 0,

                            'net_salary' => ($present_days != 0 && $basicSalary != 0) ? $Net_Salary : 0,
                            'emp_name' =>  $employee?->fname . " " . $employee?->mname . " " . $employee?->lname,
                            'pf_account_no' => ($employee?->pf_account_no) ? $employee?->pf_account_no : 0,
                            'pay_band_scale' => $payScales->pay_band_scale,
                            'grade_pay_scale' => $payScales->grade_pay_name,
                            'date_of_birth' => $employee?->dob,
                            'date_of_appointment' => $employee?->doj,
                            'date_of_retirement' => $employee?->retirement_date,
                            'bank_account_number' => $employee?->account_no,
                            'phone_no' => $employee?->mobile_number,
                            'corporation_share_da'  => ($present_days != 0 && $basicSalary != 0) ? $share : 0,
                            'employee_share_da'     => ($present_days != 0 && $basicSalary != 0) ? $employee_share  : 0,
                            // 'supplimentary_status'  => $supplimentary_status,
                            // 'supplimentary_ids'     => $supplimentary_ids,
                            'da_differance'             => $da_differance,
                            'employee_da_differance_id' => $da_differance_id
                        ]
                    );

                    // $remaing_p_days = ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status)))
                    // ? $numberOfDaysInMonth - $present_days
                    // : $present_days;

                    // $remaing_leavepay = ($remaing_p_days > 0)
                    // ? ($remaining_salary_based_on_present_day / $remaing_p_days) * $get_present_days->total_leave
                    // : 0;

                    // $remaing_earned_basic = $remaining_salary_based_on_present_day - $remaing_leavepay;

                    // Remaining Salary
                    if ((!empty($employee?->employee_status) && $employee?->employee_status?->applicable_date < $freeze_attendance->to_date) || $present_days < $numberOfDaysInMonth) {
                        RemainingFreezeSalary::updateOrCreate(
                            [
                                'employee_id' => $employee->id,
                                'freeze_attendance_id' => $data->id,
                            ],
                            [
                                'employee_id' => $employee->id,
                                'freeze_attendance_id' => $data->id,
                                'Emp_Code' => $employee->employee_id,
                                'from_date' => $freeze_attendance->from_date,
                                'to_date' => $freeze_attendance->to_date,
                                'month' => $freeze_attendance->month,
                                'present_day' => ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date || ($present_days < $numberOfDaysInMonth && empty($employee?->employee_status))) ? $numberOfDaysInMonth - $present_days : $present_days,
                                'basic_salary' => $remaining_salary_based_on_present_day,
                                'earned_basic'              => $remaining_earned_basic_salary,
                                'leave_pay'                 => $remaining_leave_pay,

                                'actual_basic' => $basicSalary,
                                'grade_pay' => $gradePay,
                                'allowance_Id' =>   $implode_remaining_allowance_ids_array,
                                'allowance_Amt' =>  $implode_remaining_allowance_amt_array,
                                'allowance_Type' => $implode_remaining_allowance_type_array,
                                'festival_allowance'        => $remainingtotalFestivalAdvance,
                                'festival_allowance_id'     => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
                                'total_allowance' => $remaining_total_allowance,
                                'deduction_Id' => $implode_remaining_deduction_ids_array,
                                'deduction_Amt' => $implode_remaining_deduction_amt_array,
                                'deduction_Type' => $implode_remaining_deduction_type_array,
                                'total_deduction' => $remaining_total_deduction,
                                'stamp_duty' => $STAMP_DUTY,
                                'loan_deduction_id' => (!empty($implode_loan_remaining_ids_array)) ? $implode_loan_remaining_ids_array : '',
                                'loan_deduction_amt' => (!empty($implode_loan_remaining_amt_array)) ? $implode_loan_remaining_amt_array : '',
                                'loan_deduction_bank_id' => (!empty($implode_loan_remaining_bank_array)) ? $implode_loan_remaining_bank_array : '',
                                'total_loan_deduction' => $total_loan_remaining_deduction,

                                'lic_deduction_id' => (!empty($implode_lic_remaining_ids_array)) ? $implode_lic_remaining_ids_array : '',
                                'lic_deduction_amt' => (!empty($implode_lic_remaining_amt_array)) ? $implode_lic_remaining_amt_array : '',
                                'total_lic_deduction' => $total_lic_remaining_deduction,

                                'festival_deduction_id'     => (!empty($implode_festAdv_remaining_ids_array)) ? $implode_festAdv_remaining_ids_array : '',
                                'festival_deduction_amt'    => (!empty($implode_festAdv_remaining_amt_array)) ? $implode_festAdv_remaining_amt_array : '',
                                'total_festival_deduction'  => $total_festAdv_remaining_deduction,


                                'net_salary' => $Remaining_Net_Salary,
                                'corporation_share_da' => $remaining_share,
                                'employee_share_da'    => $employee_remaining_share,

                            ]
                        );
                    }

                    // Store PF Data
                    EmployeeProvidentFund::updateOrCreate(
                        [
                            'employee_id' => $employee->id,
                            'current_month' => $freeze_attendance->to_date,
                            'salary_month' => $freeze_attendance->from_date,
                        ],
                        [
                            'employee_id'       => $employee->id,
                            'Emp_Code'          => $employee->employee_id,
                            'pf_account_no'     => $employee->pf_account_no,
                            'current_month'     => $freeze_attendance->to_date,
                            'salary_month'      => $freeze_attendance->from_date,
                            'financial_year_id' => $freeze_attendance->financial_year_id,
                            'pf_contribution'   => ($employee?->employee_status) ? 0 : $pf_contribution,
                            'pf_loan'           => $pf_loan,
                        ]
                    );

                    if (!empty($getFestivalAllowance)) {
                        EmployeeFestivalAdvance::where('id', $getFestivalAllowance->id)
                            ->update(['apply_status' => 2]);
                    }
                } elseif ($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 1) {

                    $applicable_present_days = 0;
                    $fromDate = Carbon::createFromFormat('Y-m-d', $freeze_attendance->from_date);
                    if ($employee?->employee_status?->applicable_date > $freeze_attendance->from_date) {
                        if ($employee?->employee_status?->applicable_date > $freeze_attendance->to_date) {
                            $applicable_present_days = 0;
                        } else {
                            $applicable_present_days = $fromDate->diffInDays($employee?->employee_status?->applicable_date) + 1;
                        }
                    }

                    // Assuming present days for now
                    // $present_days = $numberOfDaysInMonth;
                    $present_days = $get_present_days->total_present_days + $get_present_days->total_leave;
                    $actual_present_days = $get_present_days->total_present_days;
                    $leave_days = $get_present_days->total_leave;

                    $salary_percent = 0;

                    if ($employee?->employee_status?->salary_percent) {
                        $salary_percent = $employee?->employee_status?->salary_percent;
                    }
                    // else if ($employee?->employee_status?->salary_percent == 2) {
                    //     $salary_percent = 50;
                    // } else if ($employee?->employee_status?->salary_percent == 3) {
                    //     $salary_percent = 75;
                    // } else if ($employee?->employee_status?->salary_percent == 4) {
                    //     $salary_percent = 100;
                    // }

                    $remaining_percent = (100 - $salary_percent);

                    // Calculate salary per day and salary based on present days
                    $basicSalary = optional($employee->salary)->basic_salary;
                    $gradePay = optional($employee->salary)->grade_pay;
                    $basicPlusGpay = $basicSalary + $gradePay;
                    $salary_per_day = $basicSalary / $numberOfDaysInMonth; //change if salary calculates on basic plus gpay

                    // if applicable date in between from date and to date // 17/04/2024
                    if ($applicable_present_days != 0) {
                        $salary_based_on_applicable_day = round($salary_per_day * $applicable_present_days);

                        $earned_basic   = round(round($salary_per_day * ($actual_present_days - $applicable_present_days)) * $salary_percent / 100);
                        $leave_pay      = round(round($salary_per_day * ($leave_days - $applicable_present_days)) * $salary_percent / 100);

                        // $salary_based_on_present_day = round(round($salary_per_day * ($present_days - $applicable_present_days)) * $salary_percent / 100);
                        $salary_based_on_present_day = $earned_basic + $leave_pay;

                        $remaining_earned_basic_salary  = round(round($salary_per_day * ($actual_present_days - $applicable_present_days)) * $remaining_percent / 100);
                        $remaining_leave_pay  = round(round($salary_per_day * ($leave_days - $applicable_present_days)) * $remaining_percent / 100);

                        // $remaining_based_on_present_day = round(round($salary_per_day * ($present_days - $applicable_present_days)) * $remaining_percent / 100);
                        $remaining_based_on_present_day = $remaining_earned_basic_salary + $remaining_leave_pay;
                    } else {
                        $earned_basic = round(round($salary_per_day * $actual_present_days) * $salary_percent / 100);
                        $leave_pay = round(round($salary_per_day * $leave_days) * $salary_percent / 100);

                        // $salary_based_on_present_day = round(round($salary_per_day * $present_days) * $salary_percent / 100);
                        $salary_based_on_present_day = $earned_basic + $leave_pay;


                        $remaining_earned_basic_salary = round(round($salary_per_day * $actual_present_days) * $remaining_percent / 100);
                        $remaining_leave_pay = round(round($salary_per_day * $leave_days) * $remaining_percent / 100);

                        // $remaining_based_on_present_day = round(round($salary_per_day * $present_days) * $remaining_percent / 100);
                        $remaining_based_on_present_day = $remaining_earned_basic_salary + $remaining_leave_pay;
                    }

                    // Allowance logic
                    foreach ($employee->employee_allowances as $allowance) {
                        $allowanceMaster = Allowance::find($allowance->allowance_id);

                        if ($allowance->is_active == 1 && $allowanceMaster) {
                            if ($allowance->allowance_type == 1) {
                                if ($allowanceMaster->calculation == 1) {
                                    $total_allowance += $allowance->allowance_amt;

                                    // Store allowance details in arrays
                                    $allowance_ids_array[] = $allowance->allowance_id;
                                    $allowance_amt_array[] = $allowance->allowance_amt;
                                    $allowance_type_array[] = $allowance->allowance_type;
                                } else {
                                    if ($applicable_present_days != 0) {
                                        $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * $applicable_present_days;
                                        $pendingdynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
                                        $total_allowance += round($dynamicAllowanceAmt);
                                        $total_allowance += round(round($pendingdynamicAllowanceAmt)  * $salary_percent / 100);

                                        $remaining_total_allowance += round(round($pendingdynamicAllowanceAmt)  * $remaining_percent / 100);
                                    } else {
                                        $dynamicAllowanceAmt = ($allowance->allowance_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
                                        $total_allowance += round(round($dynamicAllowanceAmt)  * $salary_percent / 100);
                                        $remaining_total_allowance += round(round($dynamicAllowanceAmt)  * $remaining_percent / 100);
                                    }

                                    // Store allowance details in arrays
                                    $allowance_ids_array[] = $allowance->allowance_id;
                                    $new_allowance_amt = 0;
                                    $new_remaining_allowance_amt = 0;
                                    if ($applicable_present_days != 0) {
                                        $new_allowance_amt += round($dynamicAllowanceAmt);
                                        $new_allowance_amt += round(round($pendingdynamicAllowanceAmt)  * $salary_percent / 100);
                                        $new_remaining_allowance_amt += round(round($pendingdynamicAllowanceAmt)  * $remaining_percent / 100);
                                    } else {
                                        $new_allowance_amt += round(round($dynamicAllowanceAmt)  * $salary_percent / 100);
                                        $new_remaining_allowance_amt += round(round($dynamicAllowanceAmt)  * $remaining_percent / 100);
                                    }

                                    $allowance_amt_array[] = $new_allowance_amt;
                                    $allowance_type_array[] = $allowance->allowance_type;

                                    $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                    $remaining_allowance_amt_array[] = $new_remaining_allowance_amt;
                                    $remaining_allowance_type_array[] = $allowance->allowance_type;
                                }
                            } elseif ($allowance->allowance_type == 2) {

                                if ($allowance->allowance_id == 1) {

                                    if ($applicable_present_days != 0) {
                                        $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $allowance->allowance_amt) / 100;
                                        $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;
                                    } else {
                                        $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
                                        $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;
                                    }

                                    $bncmc_share_da = $cal_amount;
                                    $remaining_bncmc_share_da = $cal_amount1;

                                    if ($applicable_present_days != 0) {
                                        $basicandDA =  $cal_amount + ($salary_based_on_present_day + $salary_based_on_applicable_day);
                                        $remainingbasicandDA =  $cal_amount1 + ($remaining_based_on_present_day);
                                    } else {
                                        $basicandDA =  $cal_amount + $salary_based_on_present_day;
                                        $remainingbasicandDA =  $cal_amount1 + $remaining_based_on_present_day;
                                    }

                                    $total_allowance +=  round($cal_amount);
                                    $remaining_total_allowance += round($cal_amount1);
                                } else {
                                    if ($applicable_present_days != 0) {
                                        $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $allowance->allowance_amt) / 100;
                                    } else {
                                        $cal_amount = ($salary_based_on_present_day * $allowance->allowance_amt) / 100;
                                    }
                                    $cal_amount1 = ($remaining_based_on_present_day * $allowance->allowance_amt) / 100;

                                    $total_allowance += round($cal_amount);
                                    $remaining_total_allowance += round($cal_amount1);
                                }
                                // Store allowance details in arrays
                                $allowance_ids_array[] = $allowance->allowance_id;
                                $allowance_amt_array[] = round($cal_amount);
                                $allowance_type_array[] = $allowance->allowance_type;

                                $remaining_allowance_ids_array[] = $allowance->allowance_id;
                                $remaining_allowance_amt_array[] = round($cal_amount1);
                                $remaining_allowance_type_array[] = $allowance->allowance_type;
                            }
                        }
                    }

                    // Implode arrays for allowance
                    $implode_allowance_ids_array = implode(',', $allowance_ids_array);
                    $implode_allowance_amt_array = implode(',', $allowance_amt_array);
                    $implode_allowance_type_array = implode(',', $allowance_type_array);

                    $implode_remaining_allowance_ids_array = implode(',', $remaining_allowance_ids_array);
                    $implode_remaining_allowance_amt_array = implode(',', $remaining_allowance_amt_array);
                    $implode_remaining_allowance_type_array = implode(',', $remaining_allowance_type_array);

                    // Deduction logic
                    foreach ($employee->employee_deductions as $deduction) {
                        $deductionMaster = Deduction::find($deduction->deduction_id);

                        if ($deduction->is_active == 1 && $deductionMaster) {
                            if ($deduction->deduction_type == 1) {
                                if ($deductionMaster->calculation == 1) {

                                    if ($is_employee_retire_within_3_months && $deduction->deduction_id == 3) {
                                    } else {
                                        $total_deduction += $deduction->deduction_amt;

                                        if ($deduction->deduction_id == 3) {
                                            $pf_contribution += $deduction->deduction_amt;
                                        }

                                        // Store deduction details in arrays
                                        $deduction_ids_array[] = $deduction->deduction_id;
                                        $deduction_amt_array[] = $deduction->deduction_amt;
                                        $deduction_type_array[] = $deduction->deduction_type;
                                    }
                                } else {

                                    if ($applicable_present_days != 0) {
                                        $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth)  * $applicable_present_days;
                                        $pendingdynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
                                        $total_deduction += round($dynamicDeductionAmt);
                                        $total_deduction += round(round($pendingdynamicDeductionAmt)  * $salary_percent / 100);

                                        $remaining_total_deduction += round(round($pendingdynamicDeductionAmt)  * $remaining_percent / 100);
                                    } else {
                                        $dynamicDeductionAmt = ($deduction->deduction_amt / $numberOfDaysInMonth) * ($present_days - $applicable_present_days);
                                        $total_deduction += round(round($dynamicDeductionAmt) * $salary_percent / 100);
                                        $remaining_total_deduction += round(round($dynamicDeductionAmt) * $remaining_percent / 100);
                                    }

                                    if ($deduction->deduction_id == 3) {
                                        $pf_contribution += round(round($dynamicDeductionAmt) * $salary_percent / 100);
                                    }

                                    // Store deduction details in arrays
                                    $deduction_ids_array[] = $deduction->deduction_id;
                                    $new_deduction_amt = 0;
                                    $new_remaining_deduction_amt = 0;
                                    if ($applicable_present_days != 0) {
                                        $new_deduction_amt += round($dynamicDeductionAmt);
                                        $new_deduction_amt += round(round($pendingdynamicDeductionAmt)  * $salary_percent / 100);
                                        $new_remaining_deduction_amt += round(round($pendingdynamicDeductionAmt)  * $remaining_percent / 100);
                                    } else {
                                        $new_deduction_amt += round(round($dynamicDeductionAmt) * $salary_percent / 100);
                                        $new_remaining_deduction_amt += round(round($dynamicDeductionAmt) * $remaining_percent / 100);
                                    }


                                    $deduction_amt_array[] = $new_deduction_amt;
                                    $deduction_type_array[] = $deduction->deduction_type;

                                    $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                    $remaining_deduction_amt_array[] = $new_remaining_deduction_amt;
                                    $remaining_deduction_type_array[] = $deduction->deduction_type;
                                }
                            } elseif ($deduction->deduction_type == 2) {

                                if ($deduction->deduction_id == 4) {
                                    $cal_amount = ($basicandDA * $deduction->deduction_amt) / 100;
                                    $cal_amount1 = ($remainingbasicandDA * $deduction->deduction_amt) / 100;
                                    $total_deduction +=  round($cal_amount);
                                    $remaining_total_deduction +=  round($cal_amount1);
                                } else {
                                    if ($applicable_present_days != 0) {
                                        $cal_amount = (($salary_based_on_present_day + $salary_based_on_applicable_day) * $deduction->deduction_amt) / 100;
                                        $cal_amount1 = (($remaining_based_on_present_day + $salary_based_on_applicable_day) * $deduction->deduction_amt) / 100;
                                    } else {
                                        $cal_amount = ($salary_based_on_present_day * $deduction->deduction_amt) / 100;
                                        $cal_amount1 = ($remaining_based_on_present_day * $deduction->deduction_amt) / 100;
                                    }
                                    $total_deduction += round($cal_amount);
                                    $remaining_total_deduction +=  round($cal_amount1);
                                }

                                // Store deduction details in arrays
                                $deduction_ids_array[] = $deduction->deduction_id;
                                $deduction_amt_array[] = round($cal_amount);
                                $deduction_type_array[] = $deduction->deduction_type;


                                $remaining_deduction_ids_array[] = $deduction->deduction_id;
                                $remaining_deduction_amt_array[] = round($cal_amount1);
                                $remaining_deduction_type_array[] = $deduction->deduction_type;
                            }
                        }
                    }

                    // Implode arrays for deduction
                    $implode_deduction_ids_array = implode(',', $deduction_ids_array);
                    $implode_deduction_amt_array = implode(',', $deduction_amt_array);
                    $implode_deduction_type_array = implode(',', $deduction_type_array);

                    $implode_remaining_deduction_ids_array = implode(',', $remaining_deduction_ids_array);
                    $implode_remaining_deduction_amt_array = implode(',', $remaining_deduction_amt_array);
                    $implode_remaining_deduction_type_array = implode(',', $remaining_deduction_type_array);


                    // Calculate total deduction including stamp duty
                    $total_deduction += $STAMP_DUTY;
                    $remaining_total_deduction += $STAMP_DUTY;

                    if ($employee->doj > '2005-11-01' && $employee->department_id != 3 && !$is_employee_retire_within_3_months && $employee->is_dcps_applicable) {
                        $bncmc_share = ($salary_based_on_present_day + $bncmc_share_da) * 14 / 100;
                        $share =  round($bncmc_share);

                        $employee_share_calculate = ($salary_based_on_present_day + $bncmc_share_da) * 10 / 100;
                        $employee_share = round($employee_share_calculate);

                        $remainign_bncmc_share = ($remaining_based_on_present_day + $remaining_bncmc_share_da) * 14 / 100;
                        $remaining_share =  round($remainign_bncmc_share);

                        $remaining_employee_share = ($remaining_based_on_present_day + $remaining_bncmc_share_da) * 10 / 100;
                        $employee_remaining_share  =  round($remaining_employee_share);

                        $total_deduction            += $employee_share;
                        $remaining_total_deduction  += $employee_remaining_share;
                    }

                    // Calculate net salary
                    if ($applicable_present_days != 0) {
                        $Net_Salary = ($salary_based_on_present_day + $salary_based_on_applicable_day) + $total_allowance - ($total_deduction);
                        $Remaining_Net_Salary = $remaining_based_on_present_day  + $remaining_total_allowance - ($remaining_total_deduction);
                    } else {
                        $Net_Salary = $salary_based_on_present_day + $total_allowance - ($total_deduction);
                        $Remaining_Net_Salary = $remaining_based_on_present_day + $remaining_total_allowance - ($remaining_total_deduction);
                    }

                    // Get supplimentary Data
                    // $supplimentary_data = SupplimentaryBill::where('id', $freeze_attendance->supplimentary_ids)
                    //     ->where('status', 1)->first();

                    // if (!empty($supplimentary_data)) {
                    //     SupplimentaryBill::where('id', $supplimentary_data->id)
                    //         ->update(['status' => 1]);

                    //     $supplimentary_ids = $supplimentary_data->id;

                    //     $supplimentary_status = 1;
                    // } else {

                    //     $supplimentary_data = SupplimentaryBill::where('employee_id', $employee->id)
                    //         ->where('status', 0)
                    //         ->where('to_date', '<=', $freeze_attendance->to_date)->first();

                    //     if (!empty($supplimentary_data)) {
                    //         SupplimentaryBill::where('id', $supplimentary_data->id)
                    //             ->update(['status' => 1]);

                    //         $supplimentary_ids = $supplimentary_data->id;

                    //         $supplimentary_status = 1;
                    //     } else {
                    //         $supplimentary_status = 0;
                    //         $supplimentary_ids = null;
                    //     }
                    // }


                    // DA Differance
                    $fetch_employee_da_differance = EmployeeDaDifferance::where('employee_id', $employee->id)
                        ->where('Emp_Code', $employee->employee_id)
                        ->where('status', 0)
                        ->where('given_month', $freeze_attendance->month)
                        ->first();

                    if ($fetch_employee_da_differance) {

                        if ($present_days != 0) {

                            $da_differance = $fetch_employee_da_differance->differance;
                            $da_differance_id = $fetch_employee_da_differance->id;

                            $fetch_employee_da_differance->where('id', $fetch_employee_da_differance->id)
                                ->update([
                                    'status'    => 1,
                                ]);
                        } else {

                            $da_differance = 0;
                            $da_differance_id = null;

                            $given_month = ($fetch_employee_da_differance->given_month % 12) + 1;
                            $differance_in_percent = ($fetch_employee_da_differance->DA_newRate - $fetch_employee_da_differance->DA_currentRate);

                            $fetch_employee_da_differance->where('id', $fetch_employee_da_differance->id)
                                ->update([
                                    'given_month'   => $given_month,
                                    'no_of_month'   => $fetch_employee_da_differance->no_of_month + 1,
                                    'differance'    => $fetch_employee_da_differance->differance + ($employee->salary->basic_salary * $differance_in_percent) / 100,
                                    'status'        => 0,
                                ]);
                        }
                    } else {

                        if ($freeze_attendance->employee_da_differance_id != NULL) {
                            $fetch_employee_da_differance_new = EmployeeDaDifferance::where('id', $freeze_attendance->employee_da_differance_id)->first();

                            // if($employee?->employee_status && $employee?->employee_status?->is_salary_applicable == 1){

                            //         $given_month = ($fetch_employee_da_differance_new->given_month % 12) + 1;
                            //         $differance_in_percent = ($fetch_employee_da_differance_new->DA_newRate - $fetch_employee_da_differance_new->DA_currentRate);

                            //         $fetch_employee_da_differance_new->where('id', $fetch_employee_da_differance_new->id)
                            //         ->update([
                            //         'given_month'   => $given_month,
                            //         'no_of_month'   => $fetch_employee_da_differance_new->no_of_month + 1,
                            //         'differance'    => $fetch_employee_da_differance_new->differance + ($employee->salary->basic_salary * $differance_in_percent) / 100,
                            //         'status'        => 0,
                            //         ]);
                            //     $da_differance = 0;
                            //     $da_differance_id = null;
                            // }else{
                            $da_differance = $fetch_employee_da_differance_new->differance;
                            $da_differance_id = $fetch_employee_da_differance_new->id;
                            // }
                        } else {
                            $da_differance = 0;
                            $da_differance_id = null;
                        }
                    }

                    // if ($get_present_days->total_leave > 0) {
                    //     if(($applicable_present_days != 0)){
                    //         $e_leavepay = ($salary_based_on_applicable_day + $salary_based_on_present_day / $present_days) * $get_present_days->total_leave;
                    //         $e_earned_basic = ($salary_based_on_applicable_day + $salary_based_on_present_day) - $e_leavepay;
                    //     }else{
                    //         $e_leavepay = ($salary_based_on_applicable_day / $present_days) * $get_present_days->total_leave;
                    //         $e_earned_basic = ($salary_based_on_applicable_day) - $e_leavepay;
                    //     }
                    // } else {
                    //     $e_leavepay = 0;
                    //     if(($applicable_present_days != 0)){
                    //         $e_earned_basic = $salary_based_on_applicable_day + $salary_based_on_present_day;
                    //     }else{
                    //         $e_earned_basic = $$salary_based_on_applicable_day;
                    //     }
                    // }

                    // Create FreezeAttendance record
                    $data = FreezeAttendance::updateOrCreate(
                        [
                            'id' => $freeze_attendance->id,
                            'from_date' => $freeze_attendance->from_date,
                            'to_date' => $freeze_attendance->to_date,
                        ],
                        [
                            'employee_id' => $employee->id,
                            'Emp_Code' => $employee->employee_id,
                            'freeze_status' => 1,
                            'attendance_UId' => NULL, //pending
                            'ward_id' => $employee->ward_id,
                            'department_id' => $employee->department_id,
                            'designation_id' => $employee->designation_id,
                            'clas_id' => $employee->clas_id,
                            'from_date' => $freeze_attendance->from_date,
                            'to_date' => $freeze_attendance->to_date,
                            'month' => $freeze_attendance->month,
                            'financial_year_id' => $freeze_attendance->financial_year_id,
                            'present_day' =>  $present_days,
                            'actual_present_day'        =>  $get_present_days->total_present_days,
                            'total_leave'               =>  $get_present_days->total_leave,

                            'basic_salary' => ($applicable_present_days != 0) ? ($salary_based_on_applicable_day + $salary_based_on_present_day) : $salary_based_on_present_day,
                            'earned_basic'                  => $earned_basic,
                            'leave_pay'                     => $leave_pay,

                            'actual_basic' =>  $basicSalary,
                            'grade_pay' =>  $gradePay,
                            'allowance_Id' =>  $implode_allowance_ids_array,
                            'allowance_Amt' =>  $implode_allowance_amt_array,
                            'allowance_Type' =>  $implode_allowance_type_array,
                            'festival_allowance'            => $totalFestivalAdvance,
                            'festival_allowance_id'         => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
                            'total_allowance' =>  $total_allowance,
                            'deduction_Id' =>  $implode_deduction_ids_array,
                            'deduction_Amt' =>  $implode_deduction_amt_array,
                            'deduction_Type' =>  $implode_deduction_type_array,
                            'total_deduction' =>  $total_deduction,
                            'stamp_duty' =>  $STAMP_DUTY,
                            'loan_deduction_id' =>  $implode_loan_ids_array,
                            'loan_deduction_amt' =>  $implode_loan_amt_array,
                            'loan_deduction_bank_id' =>  $implode_loan_bank_array,
                            'total_loan_deduction' =>  $total_loan_deduction,
                            'lic_deduction_id' =>  $implode_lic_ids_array,
                            'lic_deduction_amt' =>  $implode_lic_amt_array,
                            'total_lic_deduction' =>  $total_lic_deduction,
                            'festival_deduction_id'         => $implode_festAdv_ids_array,
                            'festival_deduction_amt'        => $implode_festAdv_amt_array,
                            'total_festival_deduction'      => $total_festAdv_deduction,
                            'net_salary' =>  $Net_Salary,
                            'emp_name' =>  $employee?->fname . " " . $employee?->mname . " " . $employee?->lname,
                            'pf_account_no' => ($employee?->pf_account_no) ? $employee?->pf_account_no : 0,
                            'pay_band_scale' => $payScales->pay_band_scale,
                            'grade_pay_scale' => $payScales->grade_pay_name,
                            'date_of_birth' => $employee?->dob,
                            'date_of_appointment' => $employee?->doj,
                            'date_of_retirement' => $employee?->retirement_date,
                            'bank_account_number' => $employee?->account_no,
                            'phone_no' => $employee?->mobile_number,
                            'corporation_share_da' => $share,
                            'employee_share_da'    => $employee_share,
                            'salary_percentage' => $salary_percent,
                            // 'supplimentary_status'  => $supplimentary_status,
                            // 'supplimentary_ids' => $supplimentary_ids,
                            'da_differance'             => $da_differance,
                            'employee_da_differance_id' => $da_differance_id,
                        ]
                    );

                    // $e_remaing_p_days = $numberOfDaysInMonth;

                    // $e_remaing_leavepay = ($e_remaing_p_days > 0)
                    // ? ($remaining_based_on_present_day / $e_remaing_p_days) * $get_present_days->total_leave
                    // : 0;

                    // $e_remaing_earned_basic = $remaining_based_on_present_day - $e_remaing_leavepay;

                    // Remaining Salary
                    RemainingFreezeSalary::updateOrCreate(
                        [
                            'employee_id' => $employee->id,
                            'freeze_attendance_id' => $data->id,
                        ],
                        [
                            'employee_id' => $employee->id,
                            'freeze_attendance_id' => $data->id,
                            'Emp_Code' => $employee->employee_id,
                            'from_date' => $freeze_attendance->from_date,
                            'to_date' => $freeze_attendance->to_date,
                            'month' => $freeze_attendance->month,
                            'present_day' => $numberOfDaysInMonth,
                            'basic_salary' => $remaining_based_on_present_day,
                            'earned_basic'                  => $remaining_earned_basic_salary,
                            'leave_pay'                     => $remaining_leave_pay,

                            'actual_basic' => $basicSalary,
                            'grade_pay' => $gradePay,
                            'allowance_Id' => $implode_remaining_allowance_ids_array,
                            'allowance_Amt' => $implode_remaining_allowance_amt_array,
                            'allowance_Type' => $implode_remaining_allowance_type_array,
                            'festival_allowance'            => $remainingtotalFestivalAdvance,
                            'festival_allowance_id'         => (!empty($getFestivalAllowance)) ? $getFestivalAllowance->id : 0,
                            'total_allowance' => $remaining_total_allowance,
                            'deduction_Id' => $implode_remaining_deduction_ids_array,
                            'deduction_Amt' => $implode_remaining_deduction_amt_array,
                            'deduction_Type' => $implode_remaining_deduction_type_array,
                            'total_deduction' => $remaining_total_deduction,
                            'stamp_duty' => $STAMP_DUTY,
                            'loan_deduction_id' => (!empty($implode_loan_remaining_ids_array) && $check_status != 1) ? $implode_loan_remaining_ids_array : '',
                            'loan_deduction_amt' => (!empty($implode_loan_remaining_amt_array) && $check_status != 1) ? $implode_loan_remaining_amt_array : '',
                            'loan_deduction_bank_id' => (!empty($implode_loan_remaining_bank_array) && $check_status != 1) ? $implode_loan_remaining_bank_array : '',
                            'total_loan_deduction' => ($check_status != 1) ? $total_loan_remaining_deduction : 0,
                            'lic_deduction_id' => (!empty($implode_lic_remaining_ids_array) && $check_status != 1) ? $implode_lic_remaining_ids_array : '',
                            'lic_deduction_amt' => (!empty($implode_lic_remaining_amt_array) && $check_status != 1) ? $implode_lic_remaining_amt_array : '',
                            'total_lic_deduction' => ($check_status != 1) ? $total_lic_remaining_deduction : 0,
                            'festival_deduction_id'         => (!empty($implode_festAdv_remaining_ids_array) && $check_status != 1) ? $implode_festAdv_remaining_ids_array : '',
                            'festival_deduction_amt'        => (!empty($implode_festAdv_remaining_amt_array) && $check_status != 1) ? $implode_festAdv_remaining_amt_array : '',
                            'total_festival_deduction'      => ($check_status != 1) ? $total_festAdv_remaining_deduction : 0,

                            'net_salary' => $Remaining_Net_Salary,
                            'corporation_share_da' => $remaining_share,
                            'employee_share_da'             => $employee_remaining_share,
                            'salary_percentage'     => $remaining_percent,
                        ]
                    );

                    // Store PF Data
                    EmployeeProvidentFund::updateOrCreate(
                        [
                            'employee_id' => $employee->id,
                            'current_month' => $freeze_attendance->to_date,
                            'salary_month' => $freeze_attendance->from_date,
                        ],
                        [
                            'employee_id'       => $employee->id,
                            'Emp_Code'          => $employee->employee_id,
                            'pf_account_no'     => $employee->pf_account_no,
                            'current_month'     => $freeze_attendance->to_date,
                            'salary_month'      => $freeze_attendance->from_date,
                            'financial_year_id' => $freeze_attendance->financial_year_id,
                            'pf_contribution'   => ($pf_contribution) ? $pf_contribution : 0,
                            'pf_loan'           => $pf_loan,
                        ]
                    );

                    if (!empty($getFestivalAllowance)) {
                        EmployeeFestivalAdvance::where('id', $getFestivalAllowance->id)
                            ->update(['apply_status' => 2]);
                    }
                }
            }
            return response()->json(['success' => 'Freeze successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while Freeze the employee'], 500);
        }
    }

    public function fetchDateRange(Request $request, $month)
    {
        $financial_year = FinancialYear::where('id', session('financial_year'))->first();

        if ($financial_year) {
            if ($month <= 3) {
                $year = date('Y', strtotime($financial_year->to_date));
            } else {
                $year = date('Y', strtotime($financial_year->from_date));
            }
            $month = $month ?? 1;

            $fromDate = Carbon::parse($year . '-' . ($month) . '-' . 01);
            $toDate = clone ($fromDate);

            // $fromDate = (string) $fromDate->startOfMonth()->toDateString();
            // $toDate = (string) $toDate->endOfMonth()->toDateString();

            $fromDate = Carbon::parse("$year-$month-01")->startOfMonth()->toDateString();
            $toDate = Carbon::parse("$year-$month-01")->endOfMonth()->toDateString();

            return response()->json([
                'success' => true,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
            ]);
        } else {
            return response()->json(['error' => 'Financial Year Not Found'], 404);
        }
    }

    public function showEmployeeSalary($from_date, $to_date)
    {
        $authUser = Auth::user();
        $employee_monthly_salary = FreezeAttendance::where('from_date', $from_date)->where('to_date', $to_date)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->latest()->get();
        return view('admin.freeze.employee-salary')->with(['employee_monthly_salaries' => $employee_monthly_salary]);
    }
}
