<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreEmployeeTransferDetails;
use App\Models\Clas;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\EmployeeTransfer;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EmployeeTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $wards = Ward::latest()->get();
        $departments = Department::latest()->get();
        $designations = Designation::latest()->get();
        $class = Clas::latest()->get();

        return view('admin.employee.employee-transfer')->with(['wards' => $wards, 'departments'=> $departments, 'designations', 'class'=> $class]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeTransferDetails $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            if ($request->hasFile('transfer_order')) {
                $documentFile = $request->file('transfer_order');

                $transferOrderPath = $documentFile->store('transfer_orders', 'public');
                $input['transfer_order'] = $transferOrderPath;
            } else {
                $input['transfer_order'] = null;
            }

            $input['financial_year_id'] = session('financial_year');

            EmployeeTransfer::create(Arr::only($input, EmployeeTransfer::getFillables()));

            $emp_id = $input['employee_id'];

            Employee::where('id',$emp_id)->update([
                'ward_id'           => $input['ward_id'],
                'department_id'     => $input['department_id'],
                'clas_id'           => $input['clas_id'],
                'designation_id'    => $input['designation_id']
            ]);


            DB::commit();

            return response()->json(['success' => 'Employee Transfer Details created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack(); // Rollback the transaction if there's an error
            return $this->respondWithAjax($e, 'creating', 'Employee Transfer Details');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
