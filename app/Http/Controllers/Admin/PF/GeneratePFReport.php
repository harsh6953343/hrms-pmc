<?php

namespace App\Http\Controllers\Admin\PF;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PF\StorePFDepartmentLoanRequest;
use App\Models\EmployeeProvidentFund;
use App\Models\PfIntrestRate;
use App\Models\PfOpeningBalance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeneratePFReport extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pf.generate-pf-report');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePFDepartmentLoanRequest $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->validated();

            $pf_account_no = $input['pf_account_no'];
            $employee_id = $input['employee_id'];
            $Emp_Code = $input['Emp_Code'];
            $financial_year_id = session('financial_year');

            $employee_provident_funds = EmployeeProvidentFund::where('pf_account_no', $pf_account_no)
                ->where('employee_id', $employee_id)
                ->where('Emp_Code', $Emp_Code)
                ->where('financial_year_id', $financial_year_id)
                ->get();

            if ($employee_provident_funds->isEmpty()) {
                return response()->json(['error2' => 'PF Data does not exist for this employee.']);
            } else {

                $pf_opening_balance = PfOpeningBalance::where('financial_year_id', $financial_year_id)
                    ->where('pf_no', $pf_account_no)
                    ->where('employee_id', $employee_id)
                    ->where('Emp_Code', $Emp_Code)
                    ->first();

                if (!$pf_opening_balance) {
                    return response()->json(['error2' => 'Opening balance data not found.']);
                }

                $get_intrest_rate = PfIntrestRate::where('active_status', 1)->first();

                if (!$get_intrest_rate) {
                    return response()->json(['error2' => 'Interest rate data not found.']);
                }

                $total = 0;
                $total_interest_amt = 0;
                $grand_total = $pf_opening_balance->opening_balance;

                foreach ($employee_provident_funds as $key => $val) {
                    // $total += $val->pf_contribution + $val->pf_loan + ($val->other_amount ?? 0);
                    $total += $val->pf_contribution + $val->loan_amt + ($val->other_amount ?? 0);

                    $grand_total += $val->pf_contribution + $val->pf_loan + ($val->other_amount ?? 0);
                    $grand_total -= $val->loan_amt ?? 0;

                    $interest_amt = ($grand_total / 12) * ($get_intrest_rate->interest_rate / 100);

                    $calculate_interest = ($interest_amt / 12) * count($employee_provident_funds);
                    $total_interest_amt += round($calculate_interest);
 
                    $val->update([
                        'total' => $total,
                        'grand_total' => $grand_total,
                        'intrest_rate' => $get_intrest_rate->interest_rate,
                        'intrest_amt' => round($calculate_interest,2),
                        'previous_year_opening' => $pf_opening_balance->opening_balance
                    ]);
                }

                $pf_opening_balance->update([
                    'status' => 1,
                ]);


                DB::commit();
                return response()->json(['success' => 'PF Report generated successfully!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'An error occurred while generating the PF report.']);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
