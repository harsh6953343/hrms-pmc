<?php

namespace App\Http\Controllers\Admin\PF;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PF\StoreOpeningBalanceRequest;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\PfOpeningBalance;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserDepartment;

class PFOpeningController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $financial_years         = FinancialYear::latest()->get();
        $pf_opening_balances    = PfOpeningBalance::when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
            $q->whereHas('employee', function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            });
        })->latest()->get();

        $employees = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
            })
            ->latest()
            ->get();

        return view('admin.pf.pf-opening-balance')->with(['pf_opening_balances' => $pf_opening_balances, 'financial_years' => $financial_years, 'employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOpeningBalanceRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            // Check if an opening balance already exists for this financial year or if PF is already closed
            $pf_opening_balance = PfOpeningBalance::where('employee_id', $input['employee_id'])
                ->where('Emp_Code', $input['Emp_Code'])
                ->where(function ($query) use ($input) {
                    $query->where('financial_year_id', $input['financial_year_id'])
                        ->orWhere('closing_status', 1);
                })
                ->first();

            if ($pf_opening_balance) {
                if ($pf_opening_balance->financial_year_id == $input['financial_year_id']) {
                    return response()->json(['error2' => 'Opening Balance already exists for this financial year.']);
                } elseif ($pf_opening_balance->closing_status == 1) {
                    return response()->json(['error2' => 'PF Close for this employee.']);
                }
            }

            // Create new PF opening balance record
            PfOpeningBalance::create(Arr::only($input, PfOpeningBalance::getFillables()));
            DB::commit();

            return response()->json(['success' => 'PF Opening created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'PF Opening');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PfOpeningBalance $pfOpeningBalance)
    {
        try {
            DB::beginTransaction();
            $pfOpeningBalance->delete();
            DB::commit();
            return response()->json(['success' => 'Opening Balance deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Opening Balance');
        }
    }
}
