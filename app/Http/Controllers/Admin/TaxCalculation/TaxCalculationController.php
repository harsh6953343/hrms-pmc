<?php

namespace App\Http\Controllers\Admin\TaxCalculation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FinancialYear;
use App\Models\Employee;
use App\Models\EmployeeMonthlyLoan;
use App\Models\SuplimentaryEmployeeLeaveBill;
use App\Models\FreezeAttendance;
use App\Models\Allowance;
use App\Models\IncomeTax;
use Carbon\Carbon;
use App\Http\Requests\StoreIncomeTaxRequest;
use Illuminate\Support\Facades\DB;
use App\Models\OtherIncome;
use App\Models\tds_detail;

class TaxCalculationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function saveIncomeTaxDetails(StoreIncomeTaxRequest $request)
    {
        try {
            $validatedData = $request->validated();
            // IncomeTax::create($validatedData);
            $itdetails = IncomeTax::updateOrCreate(
                ['Emp_Code' => $validatedData['Emp_Code']],
                $validatedData
            );
            $request['income_tax_id'] = $itdetails->id;

            OtherIncome::updateOrCreate([
                'income_tax_id' => $itdetails->id
            ], [
                'income_tax_id' => $itdetails->id,
                'un_furnished' => $request->un_furnished,
                'personal_attendant' => $request->personal_attendant,
                'perk_furnished_value' => $request->perk_furnished_value,
                'gas_electricity_water' => $request->gas_electricity_water,
                'interest_free_or_concessional_loans' => $request->interest_free_or_concessional_loans,
                'cost_of_furniture' => $request->cost_of_furniture,
                'furniture_rentals' => $request->furniture_rentals,
                'holiday_expenses' => $request->holiday_expenses,
                'perquisite_value_of_furniture' => $request->perquisite_value_of_furniture,
                'free_meals' => $request->free_meals,
                'free_education' => $request->free_education,
                'perk_furnished_total' => $request->perk_furnished_total,
                'gifts_vouchers_etc' => $request->gifts_vouchers_etc,
                'rent_paid_by_employee' => $request->rent_paid_by_employee,
                'credit_card_expenses' => $request->credit_card_expenses,
                'value_of_perquisites' => $request->value_of_perquisites,
                'club_expenses' => $request->club_expenses,
                'conveyance' => $request->conveyance,
                'use_of_movable_assets_by_employees' => $request->use_of_movable_assets_by_employees,
                'remuneration_paid_on_behalf_of_employee' => $request->remuneration_paid_on_behalf_of_employee,
                'transfer_of_assets_to_employees' => $request->transfer_of_assets_to_employees,
                'taxable_ltc' => $request->taxable_ltc,
                'stock_option_non_qualified_options' => $request->stock_option_non_qualified_options,
                'other_benefits' => $request->other_benefits,
                'stock_options_referred_80_iac' => $request->stock_options_referred_80_iac,
                'pf_in_excess_of_12' => $request->pf_in_excess_of_12,
                'contribution_by_employer' => $request->contribution_by_employer,
                'excess_iterest_credited' => $request->excess_iterest_credited,
                'annual_accretion_taxable' => $request->annual_accretion_taxable
            ]);

            if (!$validatedData['submitted']) {
                $response = [
                    'data_already_saved'    => 1,
                    'itdetails'             => $itdetails,
                    'employee_details'      => $itdetails->employee,
                    'current_gross_salary'  => $itdetails->current_gross,
                    'estimated_salary'      => $itdetails->estimated_salary,
                    'supplementary_bill'    => $itdetails->supplementary_bill,
                    'total_lic_ded'         => $itdetails->lic,
                    'dcps_employee'         => $itdetails->dcps_emp,
                    'dcps_employer'         => $itdetails->empr_contribution,
                    'gpf'                   => $itdetails->gpf,
                    'income_tax'            => $itdetails->tax_deducted,
                    'gross_income'          => $itdetails->gross_income,
                    'success'               => 'Income Tax saved as draft!'
                ];
                return response()->json($response);
            }

            return response()->json(['success' => 'Income Tax saved successfully!']);
        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'saving', 'IncomeTax');
        }
    }

    public function showCalculationForm()
    {
        $finYears = FinancialYear::where('is_active', 1)->latest()->pluck('title', 'id')->toArray();

        $employees = Employee::get();


        return view('admin.incomeTax.income-tax-form')->with(['finYears' => $finYears, 'employees' => $employees]);
    }

    public function fetchEmployeeDetails($emp_code, $financial_year_id)
    {
        //check if income tax data already present
        $itdetails = IncomeTax::with(['employee', 'otherIncomeValue'])->where('Emp_Code', $emp_code)->where('financial_year_id', $financial_year_id)->first();

        if ($itdetails) {
            $response = [
                'data_already_saved'    => 1,
                'data_already_submitted' => $itdetails->submitted,
                'itdetails'             => $itdetails,
                'employee_details'      => $itdetails->employee,
                'current_gross_salary'  => $itdetails->current_gross,
                'estimated_salary'      => $itdetails->estimated_salary,
                'supplementary_bill'    => $itdetails->supplementary_bill,
                'total_lic_ded'         => $itdetails->lic,
                'dcps_employee'         => $itdetails->dcps_emp,
                'dcps_employer'         => $itdetails->empr_contribution,
                'gpf'                   => $itdetails->gpf,
                'income_tax'            => $itdetails->tax_deducted,
                'gross_income'          => $itdetails->gross_income
            ];

            return response()->json($response);
        }

        $employee_details = Employee::where('employee_id', $emp_code)
            ->select('employee_id', 'fname', 'mname', 'lname', 'dob', 'gender', 'pan', 'ward_id', 'department_id','retirement_date')
            ->first();

        //--Get current gross salary and supplementary bill-------------------//
        $data['allowances'] = Allowance::get();

        $fy = FinancialYear::where('id', $financial_year_id)->get()->pluck('title');
        $financial_year = $fy[0];
        // Extract the start year from the current fiscal year string
        $startYear = intval(substr($financial_year, 2, 4));
        // Calculate the previous fiscal year range 
        $previousEndYear = $startYear;
        $endYear = $startYear + 1;

        $currentYearRec = FreezeAttendance::with('employee')
            ->where('from_date', '>=', "$previousEndYear-06-01")
            ->where('to_date', '<', "$endYear-02-30")
            ->where('freeze_status', 1)
            ->where('Emp_code', $emp_code)
            ->orderBy('from_date')
            ->get();

        $estimated_salary = 0;
        $lastRec = $currentYearRec->last();//dd($lastRec);

        if ($lastRec) {
            if($employee_details->retirement_date > $endYear.'-01-31'){
                $last_salary_month = $lastRec->month;
                if($last_salary_month == 1){
                    $months_remaining = 1; 
                }elseif($last_salary_month == 2){
                    $months_remaining = 0;
                }else{
                    $months_remaining = 14 - $last_salary_month;
                }

                $last_gross = $lastRec->earned_basic + $lastRec->leave_pay + $lastRec->total_allowance + $lastRec->corporation_share_da;
                $estimated_salary = $last_gross * $months_remaining;
            }
            //     if ($lastRec->month < 3) {
            //         $assumptionRecordCount = 2 - $lastRec->month;
            //     } else {
            //         $assumptionRecordCount = 14 - $lastRec->month;
            //     }
            //     $lastMonth = $lastRec->month;
            //     for ($i = 0; $i < $assumptionRecordCount; $i++) {
            //         $newRec = $lastRec->replicate();
            //         $lastMonth = $newRec->month = $lastMonth + 1;
            //         if ($lastMonth == 14) {
            //             if ($endYear % 4 == 0) {
            //                 $newRec->present_day = 29;
            //             } else {
            //                 $newRec->present_day = 28;
            //             }
            //         } else {
            //             $newRec->present_day = $dayCountOfMonths[$lastMonth];
            //         }
            //         $currentYearRec->push($newRec);
            //     }
        } else {
            $response = [
                'error'                 => 1,
                'message'               => 'Employee data not present.'
            ];
            return response()->json($response);
        }

        $data['freezeAttendances'] = $currentYearRec;
        $income = $this->getGrossSalary($data);

        //Supplementary bill
        $supplementary_bill = 0;
        // $supp_bill = SuplimentaryEmployeeLeaveBill::where('financial_year_id', $financial_year_id)
        //     ->where('Emp_code', $emp_code)
        //     ->selectRaw('SUM(suplimentary_total_earning) as suppbill')
        //     ->groupBy('Emp_code')
        //     ->get();

        // if ($supp_bill->count()) {
        //     $supplementary_bill = $supp_bill[0]->suppbill;
        // }

        $supp_bill_from_april = SuplimentaryEmployeeLeaveBill::query()
                                ->where('month', '>=', $previousEndYear . "-04")
                                ->where('month', '<=', $endYear . "-03")
                                ->where('emp_code', $emp_code)
                                ->get();

        // $data['supp_bill_from_april'] = $supp_bill_from_april;
        // dd($supplementary_bill);

        foreach ($supp_bill_from_april as $supp) {
            $supplementary_bill += $supp->suplimentary_total_earning;
        }

        $supp_bill_csv = DB::table('csv_imported_supplementary')
            ->where('employeeid', $emp_code)
            ->selectRaw('SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
            ->groupBy('employeeid')
            ->get();
        //------------------------------------------------------------------------------------//

        $dcps_emp = $dcps_empr = $gpf = $income_tax = $total_lic_deduction = 0;
        $professional_tax = 0;
        foreach ($currentYearRec as $ded) {
            $ded_ids = explode(',', $ded->deduction_Id);
            $ded_amts = explode(',', $ded->deduction_Amt);

            foreach ($ded_ids as $key => $id) {
                if ($id == 11) {     //LIC
                    $total_lic_deduction += $ded_amts[$key];
                } elseif ($id == 7) {   //dcps employee
                    $dcps_emp += $ded_amts[$key];
                } elseif ($id == 8) {   //dcps employer
                    $dcps_empr += $ded_amts[$key];
                } elseif ($id == 5) {     //gpf
                    $gpf += $ded_amts[$key];
                } elseif ($id == 2) {     //income tax
                    $income_tax += $ded_amts[$key];
                } elseif ($id == 1) {     //professional tax
                    $professional_tax += $ded_amts[$key];
                }
            }
            $dcps_emp += $ded->employee_share_da;
            $dcps_empr += $ded->corporation_share_da;
        }
        
        if ($supp_bill_csv->count()) {
            $supplementary_bill += $supp_bill_csv[0]->suppbill;
            $total_lic_deduction += $supp_bill_csv[0]->lic;
            $dcps_emp += $supp_bill_csv[0]->dcpsemployee;
            $dcps_empr += $supp_bill_csv[0]->dcpsemployer;
            $gpf += $supp_bill_csv[0]->gpf;
        }

        //Process excel imported records
        $deductionsFromExcel = DB::table('csv_imported_salary')
            ->select('Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX', 'PROFESSIONAL TAX as PROFESSIONAL_TAX')
            ->where('Emp_code', $emp_code)
            ->get();

        $gross_salary_mar_april = 0;
        foreach ($deductionsFromExcel as $ded) {

            $gross_salary_mar_april += $ded->Total_Earning;
            $total_lic_deduction += $ded->LIC;
            $dcps_emp += $ded->DCPS_I_EMPLOYEE_CONTRIBUTION;
            $dcps_empr += $ded->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION;
            $gpf += $ded->GENERAL_PROVIDENT_FUND;
            $income_tax += $ded->INCOME_TAX;
            $professional_tax += $ded->PROFESSIONAL_TAX;
        }

        if($employee_details->retirement_date > $endYear.'-01-31'){
            $professional_tax = 2500;
        }

        //Dearness allowance
        $da_csv = DB::table('csv_imported_da')
            ->where('EmployeeId', $emp_code)
            ->selectRaw('SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
            ->groupBy('EmployeeId')
            ->get();
        if ($da_csv->count()) {
            $supplementary_bill += $da_csv[0]->da;
            $dcps_emp += $da_csv[0]->dcpsemployee;
            $dcps_empr += $da_csv[0]->dcpsemployer;
        }

        //7th pay difference
        $seventh_pay_diff_csv = DB::table('csv_imported_7th_difference')
            ->where('Employee ID', $emp_code)
            ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
            ->groupBy('Employee ID')
            ->get();
        $seventh_pay_diff_gross = 0;
        if ($seventh_pay_diff_csv->count()) {
            $seventh_pay_diff_gross = $seventh_pay_diff_csv[0]->fifth_installment;
        }
        //====================================================================

        // $totalgross = $income['gross_salary'] + $gross_salary_mar_april + $da_gross + $seventh_pay_diff_gross;
        $totalgross = $income['gross_salary'] + $gross_salary_mar_april + $seventh_pay_diff_gross;
        $gross_income = $totalgross + $estimated_salary + $supplementary_bill;
        // $net_salary = $gross_income - 52500;
        // if ($gpf) {
        //     $total_taxable_salary = $net_salary - $total_lic_deduction - $gpf;
        // } else {
        //     $total_taxable_salary = $net_salary - $total_lic_deduction - $dcps_emp - $dcps_empr;
        // }

        $response = [
            'result'                => 1,
            'data_already_saved'    => 0,
            'employee_details'      => $employee_details,
            'current_gross_salary'  => $totalgross,
            'estimated_salary'      => $estimated_salary,
            'supplementary_bill'    => $supplementary_bill,
            'total_lic_ded'         => $total_lic_deduction,
            'dcps_employee'         => $dcps_emp,
            'dcps_employer'         => $dcps_empr,
            'gpf'                   => $gpf,
            'income_tax'            => $income_tax,
            'professional_tax'      => $professional_tax,
            'gross_income'          => $gross_income
        ];

        return response()->json($response);
    }

    //--Get the current gross salary
    public function getGrossSalary($data)
    {
        $grand_total_basic_salary = 0;
        $grand_total_da_differance = 0;
        $grand_total_earn = 0;
        $grand_total_festival_allowance = 0;

        foreach ($data['freezeAttendances'] as $index_key => $freezeAttendance) {

            $grand_total_basic_salary = $freezeAttendance->earned_basic + $freezeAttendance->leave_pay;
            // $grand_total_da_differance += $freezeAttendance->da_differance;

            $grand_total_earn += ($grand_total_basic_salary + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da);
            // $grand_total_festival_allowance += $freezeAttendance->festival_allowance;
        }
        // $grand_total_earn += $grand_total_da_differance;

        return array('gross_salary' => $grand_total_earn);
    }

    public function saveTDSDetails(Request $request){

        tds_detail::updateOrCreate(
            ['Emp_Code' => $request->Emp_Code],
            $request->all()
        );
    }
}
