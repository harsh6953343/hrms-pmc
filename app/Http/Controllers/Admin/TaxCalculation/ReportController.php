<?php

namespace App\Http\Controllers\Admin\TaxCalculation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IncomeTax;
use App\Models\FinancialYear;
use App\Models\Ward;
use App\Exports\IncomeTaxExports;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        $financialYears = FinancialYear::get();

        $wards = Ward::get();

        return view('admin.incomeTax.report.index')->with([
            'financialYears' => $financialYears,
            'wards' => $wards
        ]);
    }


    public function excel(Request $request)
    {
        $incomeTax = IncomeTax::where('financial_year_id', $request->financial_year)
            ->when(isset($request->ward) && $request->ward != "", fn($q) => $q->where('ward_id', $request->ward))
            ->with(['otherIncomeValue', 'financialYear', 'employee'])
            ->get();

        return Excel::download(new IncomeTaxExports($incomeTax), 'income-tax.xlsx');
    }
}
