<?php

namespace App\Http\Controllers\Admin\TaxCalculation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\FinancialYear;
use App\Models\IncomeTax;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\IncomeTaxExportFile;

class Form16Controller extends Controller
{
    public function index(Request $request)
    {
        try {
            $authUser = Auth::user();
            $fy_id = session('financial_year');
            $financial_year_id = $request->input('financial_year_id', $fy_id);
            $finYears = FinancialYear::where('is_active', 1)->latest()->pluck('title', 'id')->toArray();
            $selectedFinYear = $finYears[$financial_year_id];

            $form16_list = IncomeTax::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                    return $query->where('ward_id', $authUser->ward_id);
                })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
                    return $query->where('Emp_Code', $authUser->employee_id);
                })
                ->when(Auth::user()->hasRole(['AMC', 'DMC']), function ($q) {
                    $q->whereIn('department_id', UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id'));
                })
                ->when(isset($request->ward_id) && $request->ward_id != "", function ($q) use ($request) {
                    $q->where('ward_id', $request->ward_id);
                })
                ->where('financial_year_id', $financial_year_id)
                // ->where('submitted', 1)
                ->get();

            return view('admin.incomeTax.form16-list')->with(['form16_list' => $form16_list, 'finYears' => $finYears, 'selectedFinYear' => $selectedFinYear]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }

    public function showForm16Pdf($incometaxid)
    {
        $data['itdetails'] = IncomeTax::with('employee','otherIncomeValue')->where('id',$incometaxid)->first();
        $data['gender'][1] = 'Male';
        $data['gender'][2] = 'Female';
        $data['financial_year'] =  FinancialYear::where('id', $data['itdetails']->financial_year_id)->pluck('title')->toArray();
        $data['financial_year'] = str_replace('FY','',$data['financial_year'][0]);

        $data['slabwise_table'] = $this->showSlabwiseCalculation($data['itdetails']->taxable_sal);

        $filename = "Form16-$incometaxid.pdf";
        $pdf = SnappyPdf::loadView('admin.incomeTax.pdf.form16-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }

    public function showSlabwiseCalculation($taxableIncome){

        $tax = 0;
        // Calculate tax for each slab
        $slab1Income = min($taxableIncome, 250000);
        $slab1Tax = 0;

        $slab2Income = max(0, min($taxableIncome - 250000, 250000));
        $slab2Tax = $slab2Income * 0.05;

        $slab3Income = max(0, min($taxableIncome - 500000, 500000));
        $slab3Tax = $slab3Income * 0.2;

        $slab4Income = max(0, $taxableIncome - 1000000);
        $slab4Tax = $slab4Income * 0.3;

        $totalTaxableIncome = $slab1Income + $slab2Income + $slab3Income + $slab4Income;
        $tax += $slab1Tax + $slab2Tax + $slab3Tax + $slab4Tax;

        $table = '<table class="taxTable">
        <thead>
            <tr>
                <td colspan=4 class="td_extend" width="70%">Income Tax Calculation</td>
            </tr>
            <tr>
                <th>Income Range</th>
                <th>Tax Rate (%)</th>
                <th>Taxable Income at Slab</th>
                <th>Tax Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0 - 2,50,000</td>
                <td>0</td>
                <td id="slab1Income">'.$slab1Income.'</td>
                <td id="slab1Tax">'.$slab1Tax.'</td>
            </tr>
            <tr>
                <td>2,50,001 - 5,00,000</td>
                <td>5</td>
                <td id="slab2Income">'.$slab2Income.'</td>
                <td id="slab2Tax">'.$slab2Tax.'</td>
            </tr>
            <tr>
                <td>5,00,001 - 10,00,000</td>
                <td>20</td>
                <td id="slab3Income">'.$slab3Income.'</td>
                <td id="slab3Tax">'.$slab3Tax.'</td>
            </tr>
            <tr>
                <td>10,00,001 and above</td>
                <td>30</td>
                <td id="slab4Income">'.$slab4Income.'</td>
                <td id="slab4Tax">'.$slab4Tax.'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Total Tax</b></td>
                <td id="totalTaxableIncome">'.$totalTaxableIncome.'</td>
                <td id="totalTax">'.$tax.'</td>
            </tr>
        </tbody>
        </table>';

        return $table;
    }

    public function exportExcel($financial_year)
    {
        return Excel::download(new IncomeTaxExportFile($financial_year), 'tax_report.xlsx');
    }
}
