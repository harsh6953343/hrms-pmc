<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenssionerPayroll extends Model
{
    use HasFactory;

    protected $fillable = ['pension_new_id', 'pension_item_master_id', 'basis', 'based_on', 'type_of_pay', 'amount', 'effective_date', 'reason'];
}
