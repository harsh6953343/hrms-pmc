<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaBillCalculate extends Model
{
    use HasFactory;

    protected $fillable = ['financial_year_id', 'da_bill_id', 'ward_id', 'department_id', 'designation_id', 'freeze_attendance_id', 'emp_code', 'month', 'from_date', 'to_date', 'basic', 'grade_pay', 'da_payable', 'da_given', 'da_difference', 'hra_payable', 'hra_given', 'hra_difference', 'dcps_corporation_allowance', 'dcps_employee_deduction', 'dcps_corporation_deduction'];

    public function employee(){
        return $this->belongsTo(Employee::class, 'emp_code', 'employee_id');
    }
}
