<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceExcelBackup extends Model
{
    use HasFactory;

    protected $fillable = ['attendance_id', 'employee_id', 'financial_year_id', 'emp_code', 'emp_name', 'from_date', 'to_date', 'main_present_days', 'total_present_days', 'total_absent_days', 'total_leave', 'total_half_days', 'month'];
}
