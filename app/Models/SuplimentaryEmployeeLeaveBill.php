<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SuplimentaryLeaveBill;

class SuplimentaryEmployeeLeaveBill extends Model
{
    use HasFactory;

    protected $fillable = ['suplimentry_leave_id', 'employee_id', 'emp_code', 'month', 'attandance_pr', 'attandance_ul', 'attandance_ml', 'attandance_el', 'supplimentry_pr', 'supplimentry_ul', 'supplimentry_ml', 'supplimentry_el', 'financial_year_id', 'basic_salary', 'net_salary', 'da', 'allowance_id', 'allowance_amount', 'allowance_type', 'deduction_id', 'deduction_amount', 'deduction_type', 'freeze_attendance_id', 'freeze_attendance_present_day', 'freeze_attendance_basic_salary', 'freeze_attendance_net_salary', 'freeze_attendance_da', 'freeze_attendance_allowance_id', 'freeze_attendance_allowance_amount', 'freeze_attendance_allowance_type', 'freeze_attendance_deduction_id', 'freeze_attendance_deduction_amount', 'freeze_attendance_deduction_type', 'employee_salary_id', 'employee_salary_present_day', 'employee_salary_basic_salary', 'employee_salary_net_salary', 'employee_salary_da', 'employee_salary_allowance_id', 'employee_salary_allowance_amount', 'employee_salary_allowance_type', 'employee_salary_deduction_id', 'employee_salary_deduction_amount', 'employee_salary_deduction_type', 'employee_total_earning', 'freeze_total_earning', 'suplimentary_total_earning', 'employee_total_deduction', 'freeze_total_deduction', 'suplimentary_total_deduction', 'created_by', 'updated_by', 'deleted_by'];

    public function suplimentryLeaveBill()
    {
        return $this->belongsTo(SuplimentaryLeaveBill::class, 'suplimentry_leave_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }
}
