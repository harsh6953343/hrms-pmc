<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportExcelFile extends Model
{
    use HasFactory;

    protected $fillable = ['month', 'file', 'financial_year'];
}
