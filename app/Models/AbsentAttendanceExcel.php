<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AbsentAttendanceExcel extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['ward', 'month', 'period', 'excel_file'];
}
