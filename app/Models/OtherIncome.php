<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherIncome extends Model
{
    use HasFactory;

    protected $fillable = ['income_tax_id', 'un_furnished', 'personal_attendant', 'perk_furnished_value', 'gas_electricity_water', 'interest_free_or_concessional_loans', 'cost_of_furniture', 'furniture_rentals', 'holiday_expenses', 'perquisite_value_of_furniture', 'free_meals', 'free_education', 'perk_furnished_total', 'gifts_vouchers_etc', 'rent_paid_by_employee', 'credit_card_expenses', 'value_of_perquisites', 'club_expenses', 'conveyance', 'use_of_movable_assets_by_employees', 'remuneration_paid_on_behalf_of_employee', 'transfer_of_assets_to_employees', 'taxable_ltc', 'stock_option_non_qualified_options', 'other_benefits', 'stock_options_referred_80_iac', 'pf_in_excess_of_12', 'contribution_by_employer', 'excess_iterest_credited', 'annual_accretion_taxable'];
}
