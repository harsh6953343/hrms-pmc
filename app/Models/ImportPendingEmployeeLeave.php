<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportPendingEmployeeLeave extends Model
{
    use HasFactory;

    protected $fillable = ['excel_file', 'month', 'period'];
}
