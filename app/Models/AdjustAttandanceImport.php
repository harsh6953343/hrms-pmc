<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdjustAttandanceImport extends Model
{
    use HasFactory;

    protected $fillable = ['month', 'from_date', 'to_date', 'no_of_days', 'file', 'financial_year'];
}
