<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class SupplimentaryBill extends BaseModel
{
    use HasFactory;

    use HasFactory, SoftDeletes;

    protected $fillable = ['ward_id', 'financial_year_id', 'bill_no', 'bill_description', 'cheque_date', 'payment_status', 'is_bill_created', 'created_by', 'updated_by', 'deleted_by'];

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id', 'id');
    }

    public function supplimentryEmployeeBills()
    {
        return $this->hasMany(SuplimentryEmployeeBill::class, 'supplimentary_bill_id', 'id');
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
