<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class RemainingFreezeSalary extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['freeze_attendance_id', 'employee_id', 'Emp_Code', 'from_date', 'to_date', 'month', 'present_day', 'basic_salary', 'earned_basic', 'leave_pay', 'actual_basic', 'grade_pay', 'allowance_Id', 'allowance_Amt', 'allowance_Type', 'festival_allowance', 'festival_allowance_id', 'total_allowance', 'deduction_Id', 'deduction_Amt', 'deduction_Type', 'total_deduction', 'stamp_duty', 'loan_deduction_id', 'loan_deduction_amt', 'loan_deduction_bank_id', 'total_loan_deduction', 'lic_deduction_id', 'lic_deduction_amt', 'total_lic_deduction', 'festival_deduction_id', 'festival_deduction_amt', 'total_festival_deduction', 'net_salary', 'corporation_share_da', 'employee_share_da', 'salary_percentage'];

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
