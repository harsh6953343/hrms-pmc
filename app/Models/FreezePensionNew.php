<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class FreezePensionNew extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['pension_id', 'pensioner_type', 'ward_id', 'employee_id', 'from_date', 'to_date', 'month', 'financial_year_id', 'employee_name', 'pensioner_name', 'account_no', 'ifsc_code', 'basic', 'dearness_relief', 'pension_arrears', 'total_earning', 'miscellaneous_deduction', 'commutation', 'recovery', 'total_deduction', 'net_salary', 'pension_bank_id'];

    public function bank()
    {
        return $this->hasOne(PensionBank::class, 'id', 'pension_bank_id');
    }

    public function pensionNew()
    {
        return $this->belongsTo(PensionNew::class, 'pension_id', 'pension_id');
    }

    public function ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
