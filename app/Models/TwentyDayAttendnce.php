<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TwentyDayAttendnce extends BaseModel
{
    use HasFactory;

    use HasFactory, SoftDeletes;

    protected $fillable = ['employee_id','from_date','to_date','month','financial_year_id','Emp_Code','emp_name','P','A','H','HP','WO','WOP','CL','PL','SL','other_leave','total_present','total_pay_days','total_ot_in_hrs','total_late_by','total_early_by'];

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
