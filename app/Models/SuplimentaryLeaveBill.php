<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SuplimentaryEmployeeLeaveBill;

class SuplimentaryLeaveBill extends Model
{
    use HasFactory;

    protected $fillable = ['supplimentary_bill_id', 'bill_no', 'bill_date', 'bill_description', 'payment_status', 'cheque_date', 'cheque_no', 'is_generate', 'created_by', 'updated_by', 'deleted_by'];

    public function supplimentaryBill()
    {
        return $this->belongsTo(SupplimentaryBill::class, 'supplimentary_bill_id', 'id');
    }

    public function supplimentaryEmployeeLeaveBill()
    {
        return $this->hasMany(SuplimentaryEmployeeLeaveBill::class, 'suplimentry_leave_id', 'id');
    }
}
