<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PensionItemMaster extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'basis', 'based_on', 'amount', 'type_of_pay', 'status'];

    public function pensionItemMasters()
    {
        return $this->hasMany(PensionItemMaster::class, 'id', 'based_on');
    }

    public function pensionItemMaster()
    {
        return $this->belongsTo(PensionItemMaster::class, 'based_on', 'id');
    }
}
