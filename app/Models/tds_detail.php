<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class tds_detail extends Model
{
    use HasFactory;

    protected $fillable = ['Emp_Code', 'prev_emp_tax', 'prev_emp_surcharge', 'wodt_surcharge', 'wdt_surcharge', 'prev_emp_cess', 'wodt_cess', 'wdt_cess'];

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
