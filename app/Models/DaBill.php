<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaBill extends Model
{
    use HasFactory;

    protected $fillable = ['bill_no', 'bill_date', 'from_date', 'to_date', 'status', 'description'];
}
