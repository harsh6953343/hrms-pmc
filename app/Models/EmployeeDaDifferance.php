<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class EmployeeDaDifferance extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['da_differance_id', 'employee_id', 'Emp_Code', 'DA_currentRate', 'DA_newRate', 'given_month', 'no_of_month', 'differance', 'status', 'financial_year_id'];

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }


    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
