<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class PensionNew extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['pension_id', 'employee_id', 'emp_title', 'emp_fname', 'emp_fname_marathi', 'emp_mname', 'emp_mname_marathi', 'emp_lname', 'emp_lname_marathi', 'ward_id', 'department_id', 'designation_id', 'pay_scale_id', 'last_basic', 'last_grade_pay', 'doj', 'dor', 'dod', 'da', 'age', 'percent_share', 'pensioner_title', 'pensioner_type', 'pensioner_fname', 'pensioner_fname_marathi', 'pensioner_mname', 'pensioner_mname_marathi', 'pensioner_lname', 'pensioner_lname_marathi', 'mobile', 'aadhar', 'address', 'address_marathi', 'city', 'state', 'pension_bank_id', 'branch', 'account_no', 'ifsc_code', 'pensionser_status', 'status_change_date', 'remarks', 'pension_start_date', 'da_applicabe', 'pay_commission', 'pension_end_date', 'dob', 'basic', 'basic_date', 'after_basic', 'after_basic_date', 'dearness_relief', 'dearness_relief_date', 'relief_fund', 'relief_fund_date', 'pensioner_arrears', 'pensioner_arrears_date', 'special_allowance', 'special_allowance_date', 'miscellaneous_deduction', 'miscellaneous_deduction_date', 'miscellaneous_arrears', 'miscellaneous_arrears_date', 'recovery', 'recovery_date', 'commutation', 'commutation_date'];

    public function group()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function designation()
    {
        return $this->hasOne(Designation::class, 'id', 'designation_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function pension_bank()
    {
        return $this->hasOne(PensionBank::class, 'id', 'pension_bank_id');
    }

    public function pay_scale()
    {
        return $this->hasOne(PayScale::class, 'id', 'pay_scale_id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function pensionPayrolls(){
        return $this->hasMany(PenssionerPayroll::class, 'id', 'pension_new_id');
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
