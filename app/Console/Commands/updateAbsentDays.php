<?php

namespace App\Console\Commands;

use App\Models\Attendance;
use Illuminate\Console\Command;

class updateAbsentDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-absent-days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $get_attendance = Attendance::get();

        foreach($get_attendance as $attendace)
        {
            $absent_days = 0;
            if($attendace->total_present_days < $attendace->main_present_days && $attendace->total_absent_days == 0)
            {
                $absent_days = $attendace->main_present_days - $attendace->total_present_days;
                $attendace->update(['total_absent_days' => $absent_days]);
            }
        }
    }
}
