<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EmployeeDeduction;
use App\Models\Employee;
use App\Models\EmployeeSalary;

class EmployeeIncomeTaxUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:employee-income-tax-update-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $datas = [
            ["emp_code" => "1001", "total" => "21828"],
            ["emp_code" => "1003", "total" => "8695.2"],
            ["emp_code" => "1004", "total" => "17306.4"],
            ["emp_code" => "1005", "total" => "3734.4"],
            ["emp_code" => "1019", "total" => "19002"],
            ["emp_code" => "1023", "total" => "8626.8"],
            ["emp_code" => "1024", "total" => "15981.6"],
            ["emp_code" => "1030", "total" => "9192"],
            ["emp_code" => "1033", "total" => "9608.4"],
            ["emp_code" => "1038", "total" => "5576.4"],
            ["emp_code" => "1040", "total" => "4305.6"],
            ["emp_code" => "1041", "total" => "3878.4"],
            ["emp_code" => "1046", "total" => "3822"],
            ["emp_code" => "1047", "total" => "8091.6"],
            ["emp_code" => "1049", "total" => "4267.2"],
            ["emp_code" => "1050", "total" => "17262"],
            ["emp_code" => "1057", "total" => "11826"],
            ["emp_code" => "1059", "total" => "20094"],
            ["emp_code" => "1061", "total" => "3962.4"],
            ["emp_code" => "1062", "total" => "5875.2"],
            ["emp_code" => "1068", "total" => "6261.6"],
            ["emp_code" => "1076", "total" => "24268.8"],
            ["emp_code" => "1077", "total" => "11114.4"],
            ["emp_code" => "1084", "total" => "9192"],
            ["emp_code" => "1087", "total" => "10286.4"],
            ["emp_code" => "1090", "total" => "6904.8"],
            ["emp_code" => "1095", "total" => "7586.4"],
            ["emp_code" => "1098", "total" => "6820.8"],
            ["emp_code" => "1103", "total" => "3750"],
            ["emp_code" => "1112", "total" => "9706.8"],
            ["emp_code" => "1114", "total" => "73125.6"],
            ["emp_code" => "1116", "total" => "65584.8"],
            ["emp_code" => "1124", "total" => "16512"],
            ["emp_code" => "1125", "total" => "43348.8"],
            ["emp_code" => "1129", "total" => "7876.8"],
            ["emp_code" => "1130", "total" => "2787.6"],
            ["emp_code" => "1143", "total" => "6864"],
            ["emp_code" => "1144", "total" => "6354"],
            ["emp_code" => "1147", "total" => "5368.8"],
            ["emp_code" => "1149", "total" => "4960.8"],
            ["emp_code" => "1158", "total" => "12502.8"],
            ["emp_code" => "1159", "total" => "8427.6"],
            ["emp_code" => "1162", "total" => "4137.6"],
            ["emp_code" => "1166", "total" => "4254"],
            ["emp_code" => "1167", "total" => "10513.2"],
            ["emp_code" => "1172", "total" => "3745.2"],
            ["emp_code" => "1177", "total" => "8252.4"],
            ["emp_code" => "1178", "total" => "9181.2"],
            ["emp_code" => "1182", "total" => "5253.6"],
            ["emp_code" => "1184", "total" => "5138.4"],
            ["emp_code" => "1186", "total" => "6970.8"],
            ["emp_code" => "1192", "total" => "3158.4"],
            ["emp_code" => "1194", "total" => "2170.8"],
            ["emp_code" => "1198", "total" => "6177.6"],
            ["emp_code" => "1201", "total" => "13178.4"],
            ["emp_code" => "1204", "total" => "3447.6"],
            ["emp_code" => "1225", "total" => "5122.8"],
            ["emp_code" => "1227", "total" => "6589.2"],
            ["emp_code" => "1229", "total" => "9483.6"],
            ["emp_code" => "1238", "total" => "1689.6"],
            ["emp_code" => "1241", "total" => "4027.2"],
            ["emp_code" => "1243", "total" => "6094.8"],
            ["emp_code" => "1250", "total" => "12036"],
            ["emp_code" => "1254", "total" => "4503.6"],
            ["emp_code" => "1256", "total" => "6250.8"],
            ["emp_code" => "1258", "total" => "306"],
            ["emp_code" => "1263", "total" => "11394"],
            ["emp_code" => "1264", "total" => "2628"],
            ["emp_code" => "1266", "total" => "5331.6"],
            ["emp_code" => "1269", "total" => "5224.8"],
            ["emp_code" => "1279", "total" => "2014.8"],
            ["emp_code" => "1288", "total" => "4369.2"],
            ["emp_code" => "1290", "total" => "10580.4"],
            ["emp_code" => "1291", "total" => "8463.6"],
            ["emp_code" => "1294", "total" => "10651.2"],
            ["emp_code" => "1296", "total" => "10070.4"],
            ["emp_code" => "1297", "total" => "3908.4"],
            ["emp_code" => "1299", "total" => "2124"],
            ["emp_code" => "1304", "total" => "6094.8"],
            ["emp_code" => "1307", "total" => "9853.2"],
            ["emp_code" => "1308", "total" => "2818.8"],
            ["emp_code" => "1310", "total" => "5962.8"],
            ["emp_code" => "1311", "total" => "3600"],
            ["emp_code" => "1319", "total" => "3261.6"],
            ["emp_code" => "1331", "total" => "5779.2"],
            ["emp_code" => "1335", "total" => "12051.6"],
            ["emp_code" => "1337", "total" => "8856"],
            ["emp_code" => "1343", "total" => "8017.2"],
            ["emp_code" => "1346", "total" => "8736"],
            ["emp_code" => "1350", "total" => "3849.6"],
            ["emp_code" => "1354", "total" => "4231.2"],
            ["emp_code" => "1355", "total" => "6949.2"],
            ["emp_code" => "1357", "total" => "3843.6"],
            ["emp_code" => "1360", "total" => "3537.6"],
            ["emp_code" => "1362", "total" => "3367.2"],
            ["emp_code" => "1365", "total" => "5354.4"],
            ["emp_code" => "1370", "total" => "8102.4"],
            ["emp_code" => "1374", "total" => "2492.4"],
            ["emp_code" => "1381", "total" => "6916.8"],
            ["emp_code" => "1398", "total" => "5827.2"],
            ["emp_code" => "1412", "total" => "6285.6"],
            ["emp_code" => "1430", "total" => "2389.2"],
            ["emp_code" => "1431", "total" => "5127.6"],
            ["emp_code" => "1558", "total" => "2509.2"],
            ["emp_code" => "1605", "total" => "10771.2"],
            ["emp_code" => "3001", "total" => "13226.4"],
            ["emp_code" => "3003", "total" => "4243.2"],
            ["emp_code" => "3004", "total" => "4578"],
            ["emp_code" => "3019", "total" => "2667.6"],
            ["emp_code" => "3020", "total" => "4352.4"],
            ["emp_code" => "3024", "total" => "6439.2"],
            ["emp_code" => "3026", "total" => "11196"],
            ["emp_code" => "3027", "total" => "3763.2"],
            ["emp_code" => "3054", "total" => "10118.4"],
            ["emp_code" => "3055", "total" => "7197.6"],
            ["emp_code" => "3056", "total" => "3771.6"],
            ["emp_code" => "3072", "total" => "2865.6"],
            ["emp_code" => "3096", "total" => "8260.8"],
            ["emp_code" => "3097", "total" => "2863.2"],
            ["emp_code" => "3100", "total" => "3253.2"],
            ["emp_code" => "3101", "total" => "16687.2"],
            ["emp_code" => "3102", "total" => "6580.8"],
            ["emp_code" => "3103", "total" => "5758.8"],
            ["emp_code" => "3104", "total" => "6117.6"],
            ["emp_code" => "3105", "total" => "4264.8"],
            ["emp_code" => "3106", "total" => "6514.8"],
            ["emp_code" => "3113", "total" => "3327.6"],
            ["emp_code" => "3114", "total" => "3686.4"],
            ["emp_code" => "3116", "total" => "3757.2"],
            ["emp_code" => "3117", "total" => "3871.2"],
            ["emp_code" => "3162", "total" => "12994.8"],
            ["emp_code" => "3163", "total" => "9692.4"],
            ["emp_code" => "3164", "total" => "2398.8"],
            ["emp_code" => "3201", "total" => "29385.6"],
            ["emp_code" => "3219", "total" => "8041.2"],
            ["emp_code" => "3221", "total" => "5684.4"],
            ["emp_code" => "3296", "total" => "6920.4"],
            ["emp_code" => "3297", "total" => "4436.4"],
            ["emp_code" => "3306", "total" => "11682"],
            ["emp_code" => "3307", "total" => "4186.8"],
            ["emp_code" => "3308", "total" => "1923.6"],
            ["emp_code" => "3319", "total" => "9740.4"],
            ["emp_code" => "3328", "total" => "10098"],
            ["emp_code" => "3329", "total" => "3201.6"],
            ["emp_code" => "5005", "total" => "4077.6"],
            ["emp_code" => "5007", "total" => "3175.2"],
            ["emp_code" => "5010", "total" => "10522.8"],
            ["emp_code" => "5014", "total" => "8551.2"],
            ["emp_code" => "5019", "total" => "2320"],
            ["emp_code" => "5022", "total" => "10288.8"],
            ["emp_code" => "5024", "total" => "23295.6"],
            ["emp_code" => "5025", "total" => "3656.4"],
            ["emp_code" => "5027", "total" => "10140"],
            ["emp_code" => "5028", "total" => "2455.2"],
            ["emp_code" => "5040", "total" => "10114.8"],
            ["emp_code" => "5045", "total" => "8031.6"],
            ["emp_code" => "5046", "total" => "5415.6"],
            ["emp_code" => "5049", "total" => "10147.2"],
            ["emp_code" => "5060", "total" => "3438"],
            ["emp_code" => "5062", "total" => "3030"],
            ["emp_code" => "5063", "total" => "9769.2"],
            ["emp_code" => "5064", "total" => "9769.2"],
            ["emp_code" => "5068", "total" => "9693.6"],
            ["emp_code" => "5071", "total" => "7922.4"],
            ["emp_code" => "5074", "total" => "9606"],
            ["emp_code" => "5098", "total" => "9301.2"],
            ["emp_code" => "1101021", "total" => "10000"],
            ["emp_code" => "101022", "total" => "10000"]
        ];

        foreach ($datas as $data) {
            $emp = Employee::where('employee_id', $data['emp_code'])->first();

            $employeeSalaryId = EmployeeSalary::where('Emp_Code', $data['emp_code'])->value('id');
            EmployeeDeduction::where(
                [
                    'employee_id' => $emp->id,
                    'Emp_Code' => $emp->employee_id,
                    'deduction_id' => 2
                ]
            )->delete();

            EmployeeDeduction::create(
                [
                    'employee_id' => $emp->id,
                    'Emp_Code' => $emp->employee_id,
                    'employee_salary_id' => $employeeSalaryId,
                    'deduction_id' => 2,
                    'is_active' => 1,
                    'deduction_amt' => round($data['total']),
                    'deduction_type' => 1,
                    'created_by' => 1
                ]
            );
        }
    }
}
