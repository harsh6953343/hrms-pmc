<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AddEmployeeLeave;

class SetEmployeeLeaveApproveOrRejectDateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:set-employee-leave-approve-or-reject-date-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $leaves = AddEmployeeLeave::with('leaveType')->where('status', 1)->get();

        foreach ($leaves as $leave) {
            if ($leave->amc_status) {
                if ($leave->leaveType?->is_approve_by == "2") {
                    $leave->dmc_datetime = $leave->updated_at;
                } else {
                    $leave->amc_datetime = $leave->updated_at;
                }
                $leave->save();

                $this->info($leave->updated_at);
            }
        }
    }
}
