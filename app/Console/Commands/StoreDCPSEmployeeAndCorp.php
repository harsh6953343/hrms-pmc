<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\SuplimentaryEmployeeLeaveBill;
use Illuminate\Support\Facades\DB;
use App\Models\PfOpeningBalance;
use App\Models\PfIntrestRate;
use App\Models\EmployeeProvidentFund;

class StoreDCPSEmployeeAndCorp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:store-dcps-emp-corp {financial_year_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert DCPS empokyee and corporation deduction data in employee_provident_funds table.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $financial_year_id = $this->argument('financial_year_id');

        $employees = Employee::where('doj', '>=', '2005-11-01')->get();
        $dataToInsert = [];

        $get_intrest_rate = PfIntrestRate::where('active_status', 1)->first();
        if (!$get_intrest_rate) {
            $this->info('Please add PF interest rate');
        }

        foreach ($employees as $emp) {

            $emp_code = $emp->employee_id;
            $this->info($emp_code);

            $fy = FinancialYear::where('id', $financial_year_id)->get()->pluck('title');
            $financial_year = $fy[0];
            $startYear = intval(substr($financial_year, 2, 4));
            $previousEndYear = $startYear;
            $endYear = $startYear + 1;

            $currentYearRec = FreezeAttendance::with('employee')
                ->where('from_date', '>=', "$previousEndYear-04-01")
                ->where('to_date', '<', "$endYear-03-31")
                ->where('freeze_status', 1)
                ->where('Emp_code', $emp_code)
                ->orderBy('from_date')
                ->get();

            $pf_opening_balance = PfOpeningBalance::where('financial_year_id', $financial_year_id)
                ->where('pf_no', $emp->pf_account_no)
                ->where('employee_id', $emp->id)
                ->where('Emp_Code', $emp_code)
                ->first();

            if (!$pf_opening_balance) {
                continue;
            }

            $total = 0;
            $total_interest_amt = 0;
            $grand_total = $pf_opening_balance->opening_balance;

            $dcps_emp = $dcps_empr = 0;
            foreach ($currentYearRec as $ded) {

                $ded_ids = explode(',', $ded->deduction_Id);
                $ded_amts = explode(',', $ded->deduction_Amt);

                foreach ($ded_ids as $key => $id) {
                    if ($id == 7) {
                        $dcps_emp = $ded_amts[$key];
                    } elseif ($id == 8) {
                        $dcps_empr = $ded_amts[$key];
                    }
                }
                if (!isset($ded_amts[7]) || $ded_amts[7] == 0 || $ded_amts[7] == '') {
                    $dcps_emp = $ded->employee_share_da;
                    $dcps_empr = $ded->corporation_share_da;
                }

                $total += $dcps_emp + $dcps_empr;
                $grand_total += $dcps_emp + $dcps_empr;
                $calculate_interest = (($grand_total * $get_intrest_rate->interest_rate) / 100) / 12;

                $dataToInsert = [
                    'financial_year_id'    =>  $financial_year_id,
                    'employee_id'      => $emp->id,
                    'Emp_Code'      => $emp_code,
                    'pf_account_no'      => $emp->pf_account_no,
                    'current_month'      => $ded->to_date,
                    'salary_month'      => $ded->from_date,
                    'pf_contribution'      => 0,
                    'pf_loan'      => 0,
                    'dcps_employee'      => $dcps_emp,
                    'dcps_corp'  => $dcps_empr,
                    'total'      => $total,
                    'grand_total'      => $grand_total,
                    'intrest_rate' => $get_intrest_rate->interest_rate,
                    'intrest_amt' => round($calculate_interest, 2),
                    'previous_year_opening' => $pf_opening_balance->opening_balance
                ];

                $this->upsertData($dataToInsert, $emp, $ded->from_date, $ded->to_date, $financial_year_id);
            }

            $pf_opening_balance->update([
                'status' => 1,
            ]);
        }
    }

    public function upsertData($dataToInsert, $emp, $fromdate, $todate, $fn_yr)
    {
        try {
            EmployeeProvidentFund::updateOrCreate([
                'employee_id'        => $emp->id,
                'Emp_Code'           => $emp->employee_id,
                'pf_account_no'      => $emp->pf_account_no,
                'current_month'            => $todate,
                'salary_month'              => $fromdate,
                'financial_year_id'              => $fn_yr,
            ], $dataToInsert);
            $this->info('Records inserted successfully');
        } catch (\Exception $e) {
            $this->info('Error inserting records: ' . $e->getMessage());
        }
    }
}
