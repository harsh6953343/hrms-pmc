<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Employee;
use App\Models\Allowance;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\SuplimentaryEmployeeLeaveBill;
use Illuminate\Support\Facades\DB;
use App\Models\IncomeTax;

class InsertDataForTax extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:insert-data-for-tax {financial_year_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data like gross salary and deductions in income_tax table.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $financial_year_id = $this->argument('financial_year_id');

        $skip_emp = [5007, 1036, 1301, 1191, 3210, 1341, 3212, 1255, 1373, 1136, 1047, 1277, 1371, 1189, 3101, 1130, 3012, 3002, 1175, 1309, 3148, 3010, 1088, 1335, 1431, 1031, 1105, 3105, 1315, 1614, 3096, 1147, 3200, 3018, 3098, 3113, 1049, 1270, 1398, 3319, 3103, 1220, 3109, 3261, 3019, 3097, 3116, 3117, 1205, 1306, 5018, 5062, 3237, 1096, 1045, 1264, 3320, 1321, 1585, 3307, 3306, 3037, 3099, 1605, 3220, 54, 3024, 3056, 1063, 3163, 3054, 29, 3142, 3123, 1061, 3201, 3075, 1517, 3328, 1574, 1512, 1068, 3164, 1159, 3213, 3202, 1343, 3257, 1201, 5019, 1325, 1502, 1229, 3074, 1004, 1235, 34, 3060, 1514, 1548, 5093, 3100, 1377, 1101021, 1257, 1535, 1617, 2014, 1003, 1125, 3155, 1041, 1350, 3036, 3063, 3138, 3175, 3258, 5016, 5013, 5014, 5021, 5054, 5071, 5069, 5163, 5169, 5158, 5238, 5273];

        // $employees = Employee::whereNotIn('employee_id', $skip_emp)->get();
        $employees = Employee::get();
        $dataToInsert = [];

        foreach ($employees as $emp) {

            $emp_code = $emp->employee_id;
            $this->info($emp_code);

            //--Get current gross salary and supplementary bill-------------------//
            $data['allowances'] = Allowance::get();

            $fy = FinancialYear::where('id', $financial_year_id)->get()->pluck('title');
            $financial_year = $fy[0];
            $startYear = intval(substr($financial_year, 2, 4));
            $previousEndYear = $startYear;
            $endYear = $startYear + 1;

            $currentYearRec = FreezeAttendance::with('employee')
                ->where('from_date', '>=', "$previousEndYear-06-01")
                ->where('to_date', '<', "$endYear-02-30")
                ->where('freeze_status', 1)
                ->where('Emp_code', $emp_code)
                ->orderBy('from_date')
                ->get();

            $estimated_salary = 0;
            $lastRec = $currentYearRec->last();

            if ($lastRec) {
                if($emp->retirement_date > $endYear.'-01-31'){
                    $last_salary_month = $lastRec->month;
                    if($last_salary_month == 1){
                        $months_remaining = 1; 
                    }elseif($last_salary_month == 2){
                        $months_remaining = 0;
                    }else{
                        $months_remaining = 14 - $last_salary_month;
                    }

                    $last_gross = $lastRec->earned_basic + $lastRec->leave_pay + $lastRec->total_allowance + $lastRec->corporation_share_da;
                    $estimated_salary = $last_gross * $months_remaining;
                }
            } else {
                $dataToInsert[] = [
                    'financial_year_id'    =>  $financial_year_id,
                    'Emp_Code'      => $emp_code,
                    'ward_id'      => $emp->ward_id,
                    'department_id'      => $emp->department_id,
                    'tax_regime'      => 1,
                    'current_gross'  => 0,
                    'estimated_salary'      => 0,
                    'supplementary_bill'    => 0,
                    'lic'         => 0,
                    'dcps_emp'         => 0,
                    'empr_contribution'         => 0,
                    'gpf'                   => 0,
                    'tax_deducted'            => 0,
                    'gross_income'          => 0,
                    'ptax' => 0,
                    'standard_deduction' => 0,
                    'total_exemptions' => 0,
                    'taxable_sal' => 0,
                    'rebated_amount' => 0,
                    'net_tax' => 0,
                    'tax_after_cess' => 0,
                    'edu_cess' => 0,
                    'tax_payable' => 0,
                    'tax_pending' => 0
                ];
                continue;
            }

            $data['freezeAttendances'] = $currentYearRec;
            $income = $this->getGrossSalary($data);

            //Supplementary bill
            $supplementary_bill = 0;
            $supp_bill_from_april = SuplimentaryEmployeeLeaveBill::query()
                ->where('month', '>=', $previousEndYear . "-04")
                ->where('month', '<=', $endYear . "-03")
                ->where('emp_code', $emp_code)
                ->get();

            foreach ($supp_bill_from_april as $supp) {
                $supplementary_bill += $supp->suplimentary_total_earning;
            }

            $supp_bill_csv = DB::table('csv_imported_supplementary')
                ->where('employeeid', $emp_code)
                ->selectRaw('SUM(DCPSCDDIFF) as dcpsemployer,SUM(DCPSEDDiff) as dcpsemployee,SUM(LICDIFF) as lic,SUM(GPFDIFF) as gpf,SUM(NPDDIFF) as suppbill')
                ->groupBy('employeeid')
                ->get();
            //------------------------------------------------------------------------------------//

            $dcps_emp = $dcps_empr = $gpf = $income_tax = $total_lic_deduction = 0;
            $professional_tax = 0;
            foreach ($currentYearRec as $ded) {
                $ded_ids = explode(',', $ded->deduction_Id);
                $ded_amts = explode(',', $ded->deduction_Amt);

                foreach ($ded_ids as $key => $id) {
                    if ($id == 11) {     //LIC
                        $total_lic_deduction += $ded_amts[$key];
                    } elseif ($id == 7) {   //dcps employee
                        $dcps_emp += $ded_amts[$key];
                    } elseif ($id == 8) {   //dcps employer
                        $dcps_empr += $ded_amts[$key];
                    } elseif ($id == 5) {     //gpf
                        $gpf += $ded_amts[$key];
                    } elseif ($id == 2) {     //income tax
                        $income_tax += $ded_amts[$key];
                    } elseif ($id == 1) {     //professional tax
                        $professional_tax += $ded_amts[$key];
                    }
                }
                $dcps_emp += $ded->employee_share_da;
                $dcps_empr += $ded->corporation_share_da;
            }

            if ($supp_bill_csv->count()) {
                $supplementary_bill += $supp_bill_csv[0]->suppbill;
                $total_lic_deduction += $supp_bill_csv[0]->lic;
                $dcps_emp += $supp_bill_csv[0]->dcpsemployee;
                $dcps_empr += $supp_bill_csv[0]->dcpsemployer;
                $gpf += $supp_bill_csv[0]->gpf;
            }

            //Process excel imported records
            $deductionsFromExcel = DB::table('csv_imported_salary')
                ->select('Emp_Code', 'Total_Earning', 'LIC', 'GENERAL_PROVIDENT_FUND', 'DCPS_I_EMPLOYEE_CONTRIBUTION', 'DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION', 'INCOME_TAX', 'PROFESSIONAL TAX as PROFESSIONAL_TAX')
                ->where('Emp_code', $emp_code)
                ->get();

            $gross_salary_mar_april = 0;
            foreach ($deductionsFromExcel as $ded) {

                $gross_salary_mar_april += $ded->Total_Earning;
                $total_lic_deduction += $ded->LIC;
                $dcps_emp += $ded->DCPS_I_EMPLOYEE_CONTRIBUTION;
                $dcps_empr += $ded->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION;
                $gpf += $ded->GENERAL_PROVIDENT_FUND;
                $income_tax += $ded->INCOME_TAX;
                $professional_tax += $ded->PROFESSIONAL_TAX;
            }

            if($emp->retirement_date > $endYear.'-01-31'){
                $professional_tax = 2500;
            }

            //Dearness allowance
            $da_csv = DB::table('csv_imported_da')
                ->where('EmployeeId', $emp_code)
                ->selectRaw('SUM(DADiff) as da,SUM(DCPSCAllDiff) as dcpsemployee,SUM(DCPSEContriDiff) as dcpsemployer')
                ->groupBy('EmployeeId')
                ->get();
            if ($da_csv->count()) {
                $supplementary_bill += $da_csv[0]->da;
                $dcps_emp += $da_csv[0]->dcpsemployee;
                $dcps_empr += $da_csv[0]->dcpsemployer;
            }

            //7th pay difference
            $seventh_pay_diff_csv = DB::table('csv_imported_7th_difference')
                ->where('Employee ID', $emp_code)
                ->selectRaw('SUM(5th_Inst_Amount) as fifth_installment')
                ->groupBy('Employee ID')
                ->get();
            $seventh_pay_diff_gross = 0;
            if ($seventh_pay_diff_csv->count()) {
                $seventh_pay_diff_gross = $seventh_pay_diff_csv[0]->fifth_installment;
            }
            //====================================================================

            $totalgross = $income['gross_salary'] + $gross_salary_mar_april + $seventh_pay_diff_gross;
            $gross_income = $totalgross + $estimated_salary + $supplementary_bill;

            $exemption = $professional_tax + 50000;
            
            $net_salary = $gross_income - $exemption;
            
            $total_taxable_salary = $net_salary - $total_lic_deduction - $dcps_emp - $dcps_empr - $gpf;

            $tax_details = $this->calculateTax($total_taxable_salary, $income_tax);

            $dataToInsert[] = [
                'financial_year_id'    =>  $financial_year_id,
                'Emp_Code'      => $emp_code,
                'ward_id'      => $emp->ward_id,
                'department_id'      => $emp->department_id,
                'tax_regime'      => 1,
                'current_gross'  => $totalgross,
                'estimated_salary'      => $estimated_salary,
                'supplementary_bill'    => $supplementary_bill,
                'lic'         => $total_lic_deduction,
                'dcps_emp'         => $dcps_emp,
                'empr_contribution'         => $dcps_empr,
                'gpf'                   => $gpf,
                'tax_deducted'            => $income_tax,
                'gross_income'          => $gross_income,
                'ptax'                  => $professional_tax,
                'standard_deduction' => 50000,
                'total_exemptions' => $professional_tax + 50000,
                'taxable_sal' => $total_taxable_salary,
                'rebated_amount' => $tax_details[3],
                'net_tax' => $tax_details[0],
                'tax_after_cess' => $tax_details[2],
                'edu_cess' => $tax_details[1],
                'tax_payable' => $tax_details[0],
                'tax_pending' => $tax_details[5]
            ];
        }

        $this->upsertData($dataToInsert);
    }

    public function calculateTax($taxable_income, $tax_deducted)
    {

        $tax = 0;

        if ($taxable_income <= 250000) {
            $tax = 0;
        } else if ($taxable_income <= 500000) {
            $tax = ($taxable_income - 250000) * 0.05;
        } else if ($taxable_income <= 1000000) {
            $tax = 12500 + ($taxable_income - 500000) * 0.2;
        } else {
            $tax = 112500 + ($taxable_income - 1000000) * 0.3;
        }

        $net_tax = round($tax, 2);
        $edu_cess = round($net_tax * 0.04);

        //Apply rebate Section 87a
        $rebated_amount = 0;
        if ($taxable_income <= 500000) {
            if ($net_tax < 12500) {
                $rebated_amount = $net_tax;
            } else {
                $rebated_amount = 12500;
            }
            $net_tax = $net_tax - $rebated_amount;
        }
        $rebate = $rebated_amount;

        if ($net_tax == 0) {
            $edu_cess = 0;
        }
        $total_tax = round($net_tax + $edu_cess);

        $tax_pending = $total_tax - $tax_deducted;

        return array($net_tax, $edu_cess, $total_tax, $rebate,  $tax_deducted, $tax_pending);
    }

    public function upsertData($dataToInsert)
    {
        try {
            // IncomeTax::upsert(
            //     $dataToInsert,
            //     ['financial_year_id', 'Emp_Code'],
            //     ['ward_id', 'department_id', 'tax_regime', 'current_gross', 'estimated_salary', 'supplementary_bill', 'lic', 'dcps_emp', 'empr_contribution', 'gpf', 'tax_deducted', 'gross_income', 'ptax', 'standard_deduction', 'total_exemptions', 'taxable_sal', 'rebated_amount', 'net_tax', 'tax_after_cess', 'edu_cess', 'tax_payable', 'tax_pending']
            // );
            IncomeTax::upsert(
                $dataToInsert,
                ['financial_year_id', 'Emp_Code'],
                ['estimated_salary']
            );
            $this->info('Records inserted successfully');
        } catch (\Exception $e) {
            $this->info('Error inserting records: ' . $e->getMessage());
        }
    }

    public function getGrossSalary($data)
    {
        $grand_total_basic_salary = 0;
        $grand_total_earn = 0;

        foreach ($data['freezeAttendances'] as $index_key => $freezeAttendance) {

            $grand_total_basic_salary = $freezeAttendance->earned_basic + $freezeAttendance->leave_pay;

            $grand_total_earn += ($grand_total_basic_salary + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da);
        }
        return array('gross_salary' => $grand_total_earn);
    }
}
