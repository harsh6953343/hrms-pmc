<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GpfOpeningBalance;
use App\Imports\NpsOpeningBalance;

class ImportOpeningBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:opening-bal {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $type = $this->argument('type');

        if($type == 'gpf')
        {
            $filePath = public_path('gpf_opening.xlsx');
            if (!file_exists($filePath))
            {
                $this->error("File not found: $filePath");
                return;
            }

            try
            {
                Excel::import(new GpfOpeningBalance, $filePath);
                $this->info('Excel data imported successfully!');
            } catch (\Exception $e) {
                $this->error('Error: ' . $e->getMessage());
            }
        }
        elseif($type == 'nps')
        {
            $filePath = public_path('nps_opening.xlsx');
            if (!file_exists($filePath))
            {
                $this->error("File not found: $filePath");
                return;
            }

            try
            {
                Excel::import(new NpsOpeningBalance, $filePath);
                $this->info('Excel data imported successfully!');
            } catch (\Exception $e) {
                $this->error('Error: ' . $e->getMessage());
            }
        }
        else
        {
            $this->info('Which type of import you want to proceed with GPF/NPS ?');
        }
    }
}
