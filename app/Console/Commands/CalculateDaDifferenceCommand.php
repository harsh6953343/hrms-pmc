<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DaBill;
use App\Models\DaBillCalculate;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\FreezeAttendance;
use App\Models\SuplimentaryEmployeeLeaveBill;
use App\Models\Allowance;

class CalculateDaDifferenceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:calculate-da-difference-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $setting = Setting::where('name', 'is_da_execute')->value('value');

        if($setting){
            try{
                DB::beginTransaction();
                Setting::where('name', 'is_da_execute')->update(['value' => 0]);
                $daBills = DaBill::where('status', 0)->get();
                $allowances = Allowance::whereIn('id', [1, 2])->pluck('amount', 'id');
                $newDaValue = $allowances[1] ?? 0;
                $newHRAValue = $allowances[2] ?? 0;

                foreach($daBills as $daBill){
                    $fromDate = Carbon::parse($daBill->from_date);
                    $toDate = Carbon::parse($daBill->to_date);
                    $period = CarbonPeriod::create($fromDate, '1 month', $toDate);

                    foreach ($period as $date) {
                        $freezeAttendances = FreezeAttendance::with(['employee'])->whereDate('from_date', $date->copy()->startOfMonth())
                            ->whereDate('to_date', $date->copy()->endOfMonth())
                            ->where('freeze_status', 1)
                            ->get();

                            foreach($freezeAttendances as $freezeAttendance){
                                $suplimentryMonth = Carbon::parse($freezeAttendance->from_date)->format('Y-m');
                                $suplimentries = SuplimentaryEmployeeLeaveBill::with(['employee'])->where('emp_code', $freezeAttendance->Emp_Code)->where('month', $suplimentryMonth)->get();


                            // for suplimentry
                            $suplimentryBasic = 0;
                            $suplimentryGradePay = 0;
                            $suplimentryDaPayable = 0;
                            $suplimentryDaGiven = 0;
                            $suplimentryHraPayablee = 0;
                            $suplimentryHraGiven = 0;
                            $suplimentryDcpsCorporationAllowance = 0;
                            $suplimentryDcpsCorporationDeduction = 0;
                            $suplimentryDcpsEmployeeDeduction = 0;
                            foreach($suplimentries as $suplimentry){
                                $suplimentryBasic += round($suplimentry->basic_salary);


                                $suplimentryAllowanceId = explode(',', $suplimentry->allowance_id);
                                $suplimentryAllowanceAmount = explode(',', $suplimentry->allowance_amount);
                                $suplimentryAllowanceCombine = array_combine( $suplimentryAllowanceId, $suplimentryAllowanceAmount);

                                $suplimentryGradePay += ($suplimentryAllowanceCombine[12] ?? 0);
                                // for da payable for suplimentry
                                $daPayableSupli = (round($suplimentry->basic_salary)*$newDaValue) / 100;
                                $suplimentryDaPayable += round($daPayableSupli);

                                // for HRA
                                $suplimentryHraPayablee += (round($suplimentry->basic_salary)*$newHRAValue) / 100;
                                $suplimentryHraGiven += round($suplimentryAllowanceCombine[2] ?? 0);

                                // for da given of supplimentry
                                $daGivenSupli = round($suplimentryAllowanceCombine[1] ?? 0);
                                $suplimentryDaGiven += $daGivenSupli;

                                if ($suplimentry?->employee?->doj > '2005-11-01' && $suplimentry?->employee?->department_id != 3 && $suplimentry->employee?->is_dcps_applicable) {
                                    $corporationCal = ((($suplimentry->basic_salary + $daPayableSupli)*0.14) - (($suplimentry->basic_salary + $daGivenSupli)*0.14));
                                    $suplimentryDcpsCorporationAllowance += round($corporationCal);

                                    $suplimentryDcpsCorporationDeduction += round($corporationCal);

                                    $suplimentryDcpsEmployeeDeduction += round(((($suplimentry->basic_salary + $daPayableSupli)*0.1) - (($suplimentry->basic_salary + $daGivenSupli)*0.1)));
                                }

                            }

                            $freezeAllowanceId = explode(',', $freezeAttendance->allowance_Id);
                            $freezeAllowanceAmount = explode(',', $freezeAttendance->allowance_Amt);
                            $freezeAllowanceCombine = array_combine($freezeAllowanceId, $freezeAllowanceAmount);

                            $freezeAttendanceBasic = round(($suplimentryBasic + $freezeAttendance->basic_salary));
                            $freezeAttendanceGradePay = round(($suplimentryGradePay + ($freezeAllowanceCombine[12] ?? 0)));

                            $freezeAttendanceDaPayable = round($suplimentryDaPayable + (($freezeAttendance->basic_salary*$newDaValue) / 100));

                            $freezeAttendanceDaGiven = round($suplimentryDaGiven + ($freezeAllowanceCombine[1] ?? 0));

                            $freezeAttendanceHraPayablee = round($suplimentryHraPayablee + (($freezeAttendance->basic_salary*$newHRAValue) / 100));
                            $freezeAttendanceHraGiven = round($suplimentryHraGiven + ($freezeAllowanceCombine[2] ?? 0));

                            $freezeAttendanceDcpsCorporationAllowance = round($suplimentryDcpsCorporationAllowance);
                            $freezeAttendanceDcpsCorporationDeduction = round($suplimentryDcpsCorporationDeduction);
                            $freezeAttendanceDcpsEmployeeDeduction = round($suplimentryDcpsEmployeeDeduction);
                            if ($freezeAttendance?->employee?->doj > '2005-11-01' && $freezeAttendance?->employee?->department_id != 3 && $freezeAttendance->employee?->is_dcps_applicable) {
                                $freezeCorporationCal = round(((($freezeAttendance->basic_salary + $freezeAttendanceDaPayable)*0.14) - (($freezeAttendance->basic_salary + $freezeAttendanceDaGiven)*0.14)));
                                $freezeAttendanceDcpsCorporationAllowance += round($freezeCorporationCal);

                                $freezeAttendanceDcpsCorporationDeduction += round($freezeCorporationCal);

                                $freezeAttendanceDcpsEmployeeDeduction += round(((($freezeAttendance->basic_salary + $freezeAttendanceDaPayable)*0.1) - (($freezeAttendance->basic_salary + $freezeAttendanceDaGiven)*0.1)));
                            }

                            // end of for suplimentry

                            DaBillCalculate::updateOrCreate([
                                'emp_code' =>  $freezeAttendance->Emp_Code,
                                'month' =>  $freezeAttendance->month,
                                'financial_year_id' =>  $freezeAttendance->financial_year_id,
                                'from_date' =>  $freezeAttendance->from_date,
                                'to_date' =>  $freezeAttendance->to_date,
                                'da_bill_id' => $daBill->id,
                            ],[
                                'financial_year_id' => $freezeAttendance->financial_year_id,
                                'da_bill_id' => $daBill->id,
                                'ward_id' => $freezeAttendance->ward_id,
                                'department_id' => $freezeAttendance->department_id,
                                'designation_id' => $freezeAttendance->designation_id,
                                'freeze_attendance_id' => $freezeAttendance->id,
                                'emp_code' => $freezeAttendance->Emp_Code,
                                'month' => $freezeAttendance->month,
                                'from_date' => $freezeAttendance->from_date,
                                'to_date' => $freezeAttendance->to_date,
                                'basic' => round($freezeAttendanceBasic ?? 0),
                                'grade_pay' => round($freezeAttendanceGradePay ?? 0),
                                'da_payable' => round($freezeAttendanceDaPayable ?? 0),
                                'da_given' => round($freezeAttendanceDaGiven ?? 0),
                                'da_difference' => round($freezeAttendanceDaPayable - $freezeAttendanceDaGiven),
                                'hra_payable' => round($freezeAttendanceHraPayablee ?? 0),
                                'hra_given' => round($freezeAttendanceHraGiven ?? 0),
                                'hra_difference' => round($freezeAttendanceHraPayablee - $freezeAttendanceHraGiven),
                                'dcps_corporation_allowance' => round($freezeAttendanceDcpsCorporationAllowance),
                                'dcps_employee_deduction' => round($freezeAttendanceDcpsEmployeeDeduction),
                                'dcps_corporation_deduction' => round($freezeAttendanceDcpsCorporationDeduction),
                            ]);

                            $this->info('execute');
                        }
                    }

                    $daBill->update(['status' => 1]);
                }


                Setting::where('name', 'is_da_execute')->update(['value' => 1]);
                DB::commit();
            }catch(\Exception $e){
                \Log::info($e);
                DB::rollback();
            }
        }
    }
}
