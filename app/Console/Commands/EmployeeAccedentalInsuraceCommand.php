<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Employee;
use App\Models\EmployeeDeduction;
use App\Models\OldEmployeeDeduction;

class EmployeeAccedentalInsuraceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:employee-accedental-insurace-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $employees = Employee::with('salary')->whereIn('clas_id', [1,2,3,4])->get();

        foreach($employees as $employee){
            EmployeeDeduction::updateOrCreate([
                'employee_id' => $employee->id,
                'Emp_Code' => $employee->employee_id,
                'deduction_id' =>  17
            ],[
                'employee_id' => $employee->id,
                'Emp_Code' => $employee->employee_id,
                'employee_salary_id' => $employee->salary?->id,
                'deduction_id' => 17,
                'deduction_amt' => 850,
                'deduction_type' => 1,
                'is_active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]);

            OldEmployeeDeduction::updateOrCreate([
                'employee_id' => $employee->id,
                'Emp_Code' => $employee->employee_id,
                'deduction_id' =>  17
            ],[
                'employee_id' => $employee->id,
                'Emp_Code' => $employee->employee_id,
                'old_employee_salary_id' => $employee->salary?->id,
                'deduction_id' => 17,
                'deduction_amt' => 850,
                'deduction_type' => 1,
                'applicable_date' => "2025-01-31",
                'is_active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
            ]);
        }
    }
}
