<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
})->name('/');




// Guest Users
Route::middleware(['guest', 'PreventBackHistory'])->group(function () {
    Route::get('login', [App\Http\Controllers\Admin\AuthController::class, 'showLogin'])->name('login');
    Route::post('login', [App\Http\Controllers\Admin\AuthController::class, 'login'])->name('signin');
    Route::get('register', [App\Http\Controllers\Admin\AuthController::class, 'showRegister'])->name('register');
    Route::post('register', [App\Http\Controllers\Admin\AuthController::class, 'register'])->name('signup');
    Route::get('mob-login', [App\Http\Controllers\Admin\AuthController::class, 'showMobLogin'])->name('mob-login');
    Route::post('mob-login', [App\Http\Controllers\Admin\AuthController::class, 'mobLogin'])->name('mob-signin');
});

// Authenticated users
Route::middleware(['auth', 'PreventBackHistory'])->group(function () {

    // Auth Routes
    Route::get('home', fn() => redirect()->route('dashboard'))->name('home');
    Route::get('dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
    Route::post('logout', [App\Http\Controllers\Admin\AuthController::class, 'Logout'])->name('logout');
    Route::get('change-theme-mode', [App\Http\Controllers\Admin\DashboardController::class, 'changeThemeMode'])->name('change-theme-mode');
    Route::get('show-change-password', [App\Http\Controllers\Admin\AuthController::class, 'showChangePassword'])->name('show-change-password');
    Route::post('change-password', [App\Http\Controllers\Admin\AuthController::class, 'changePassword'])->name('change-password');



    // Masters
    Route::resource('wards', App\Http\Controllers\Admin\Masters\WardController::class);
    Route::resource('department', App\Http\Controllers\Admin\Masters\DepartmentController::class);
    Route::resource('class', App\Http\Controllers\Admin\Masters\ClassController::class);
    Route::resource('bank', App\Http\Controllers\Admin\Masters\BankController::class);
    Route::resource('financial_year', App\Http\Controllers\Admin\Masters\FinancialYearController::class);
    Route::resource('allowance', App\Http\Controllers\Admin\Masters\AllowanceController::class);
    Route::resource('deduction', App\Http\Controllers\Admin\Masters\DeductionController::class);
    Route::resource('loan', App\Http\Controllers\Admin\Masters\LoanController::class);
    // To Change Activity Status of Loan
    Route::post('loan-activity-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Masters\LoanController::class, 'changeStatus'])->name('loan-activity-status');

    Route::resource('designation', App\Http\Controllers\Admin\Masters\DesignationController::class);
    // Fetch departments on ward
    Route::get('fetch-departments/{ward_id}', [App\Http\Controllers\Admin\Masters\DesignationController::class, 'fetchDepartment'])->name('fetch-departments');
    Route::get('fetch-department/{ward_id}', [App\Http\Controllers\Admin\Masters\DesignationController::class, 'fetchDepartments'])->name('fetch-department');

    Route::resource('leave-type', App\Http\Controllers\Admin\Masters\LeaveTypeController::class);
    Route::resource('pay-scale', App\Http\Controllers\Admin\Masters\PayScaleController::class);
    Route::resource('status', App\Http\Controllers\Admin\Masters\StatusController::class);
    Route::resource('pension-bank', App\Http\Controllers\Admin\Masters\PensionBankController::class);
    Route::resource('intrest-rate', App\Http\Controllers\Admin\Masters\IntrestRateController::class);
    // To Change Activity Status of Loan
    Route::post('intrest-rate-activity-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Masters\IntrestRateController::class, 'changeStatus'])->name('intrest-rate-activity-status');

    Route::resource('document', App\Http\Controllers\Admin\Masters\DocumentController::class);



    //Employee List
    Route::resource('employee', App\Http\Controllers\Admin\Employee\EmployeeController::class);
    Route::resource('employee-transfer', App\Http\Controllers\Admin\Employee\EmployeeTransferController::class);

    // Fetch Class
    Route::get('fetch-class/{employee_category}', [App\Http\Controllers\Admin\Masters\ClassController::class, 'fetchClass'])->name('fetch-class');

    // Fetch designation on ward/class/department
    Route::get('fetch-designations/{ward_id}/{department_id}/{class_id}', [App\Http\Controllers\Admin\Employee\EmployeeController::class, 'fetchDesignation'])->name('fetch-designations');
    Route::get('fetch-working-years/{class_id}/{dob}', [App\Http\Controllers\Admin\Employee\EmployeeController::class, 'fetchWorkingYear'])->name('fetch-working-years');

    Route::resource('employee-status', App\Http\Controllers\Admin\Employee\EmployeeStatusController::class);
    Route::get('fetch-employee-details/{Emp_Code}', [App\Http\Controllers\Admin\Employee\EmployeeStatusController::class, 'fetchEmployeeDetails'])
        ->name('fetch-employee-details');

    Route::get('fetch-employee-details-ps/{Emp_Code}', [App\Http\Controllers\Admin\Employee\EmployeeStatusController::class, 'fetchPensionEmployeeDetails'])->name('fetch-employee-details-ps');


    // Salary Structure
    Route::resource('employee-salary', App\Http\Controllers\Admin\Employee\EmployeeSalaryController::class);
    Route::get('fetch-payscale-details/{pay_scale_id}', [App\Http\Controllers\Admin\Employee\EmployeeSalaryController::class, 'fetchpayScaleDetails'])
        ->name('fetch-payscale-details');

    Route::resource('basic-salary-increment', App\Http\Controllers\Admin\Employee\BasicSalaryIncrementController::class);
    Route::get('get-incremented-salary', [App\Http\Controllers\Admin\Employee\BasicSalaryIncrementController::class, 'getIncrementedBasicSalary'])
        ->name('get-incremented-salary');

    Route::resource('allowance-increment', App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class);
    Route::get('fetch-allowance-details/{allowance_id}', [App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class, 'fetchAllowanceDetails'])
        ->name('fetch-allowance-details');
    Route::get('get-incremented-allowance', [App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class, 'getIncrementedAllowance'])
        ->name('get-incremented-allowance');

    Route::resource('deduction-increment', App\Http\Controllers\Admin\Employee\DeductionIncrementController::class);
    Route::get('fetch-deduction-details/{deduction_id}', [App\Http\Controllers\Admin\Employee\DeductionIncrementController::class, 'fetchDeductionDetails'])
        ->name('fetch-deduction-details');
    Route::get('get-incremented-deduction', [App\Http\Controllers\Admin\Employee\DeductionIncrementController::class, 'getIncrementedDeduction'])
        ->name('get-incremented-deduction');

    // DA Differance
    Route::resource('generate-da-differance', App\Http\Controllers\Admin\Employee\GenerateDADifferanceController::class);
    Route::resource('da-differance', App\Http\Controllers\Admin\Employee\DADifferanceController::class);

    // End Salary Structure

    // Employee Loans / LIC /Festival Advance Start
    Route::resource('employee-loans', App\Http\Controllers\Admin\Employee\EmployeeLoanController::class);
    Route::post('employee-loan-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Employee\EmployeeLoanController::class, 'employeeLoanStatus'])->name('employee-loan-status');

    Route::resource('employee-monthly-loans', App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class);
    Route::get('employee-monthly-loans/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class, 'showEmployeeLoans'])->name('employee-monthly-loans.showEmployeeLoans');
    Route::post('employee-monthly-loan-status/{model_id}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class, 'employeeMonthlyLoanStatus'])->name('employee-monthly-loan-status');

    // LIC
    Route::resource('lic-dedcution', App\Http\Controllers\Admin\Employee\LICDeductionController::class);
    Route::post('employee-lic-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Employee\LICDeductionController::class, 'employeeLicStatus'])->name('employee-lic-status');

    Route::resource('employee-monthly-lic', App\Http\Controllers\Admin\Employee\EmployeeMonthlyLicController::class);
    Route::get('employee-monthly-lic/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLicController::class, 'showEmployeeLic'])->name('employee-monthly-lic.showEmployeeLic');
    Route::post('employee-monthly-lic-status/{model_id}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLicController::class, 'employeeMonthlyLicStatus'])->name('employee-monthly-lic-status');

    // Festival Advance
    Route::resource('employee-festival-advance', App\Http\Controllers\Admin\Employee\EmployeeFestivalAdvanceController::class);
    Route::get('fetch-date-range-festival-advance/{month}', [App\Http\Controllers\Admin\Employee\EmployeeFestivalAdvanceController::class, 'fetchDateRange'])
        ->name('fetch-date-range-festival-advance');

    Route::resource('monthly-festivalAdvance', App\Http\Controllers\Admin\Employee\EmployeeMonthlyFestivalController::class);
    Route::get('monthly-festival-advance/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyFestivalController::class, 'showEmployeeFestivalAdvance'])->name('monthly-festival-advance');
    Route::post('monthly-festivalAdvance-status/{model_id}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyFestivalController::class, 'monthlyFestivalAdvanceStatus'])->name('monthly-festivalAdvance-status');

    Route::resource('bonus', App\Http\Controllers\Admin\Employee\BonusController::class);

    // Employee Loans / LIC / Festival Advance End

    // Attendance Managament
    Route::resource('add-absent', App\Http\Controllers\Admin\Attendance\EmployeeAbsent::class);
    Route::get('fetch-attendance-details/{Emp_Code}/{month}', [App\Http\Controllers\Admin\Attendance\EmployeeAbsent::class, 'fetchAttendanceDetails'])
        ->name('fetch-attendance-details');

    Route::get('leave/delete', [App\Http\Controllers\Admin\Attendance\AddEmployeeLeaveController::class, 'removePendingLeave'])->name('remove-pending-leave');
    Route::post('leave/delete', [App\Http\Controllers\Admin\Attendance\AddEmployeeLeaveController::class, 'storeRemovePendingLeave'])->name('store-remove-pending-leave');

    Route::post('add-leave/list', [App\Http\Controllers\Admin\Attendance\AddEmployeeLeaveController::class, 'list'])->name('add-leave.list');
    Route::resource('add-leave', App\Http\Controllers\Admin\Attendance\AddEmployeeLeaveController::class);
    Route::get('fetch-employee-details-leave/{Emp_Code}/{month}', [App\Http\Controllers\Admin\Attendance\AddEmployeeLeaveController::class, 'fetchEmployeeDetails'])
        ->name('fetch-employee-details-leave');

    Route::get('leave/approve/static', [App\Http\Controllers\Admin\Attendance\LeaveApprovalController::class, 'leaveApproveStatic'])->name('leave-approve-static');
    Route::post('leave/approve/static', [App\Http\Controllers\Admin\Attendance\LeaveApprovalController::class, 'storeLeaveApproveStatic'])->name('store-leave-approve-static');

    Route::resource('leave-approval', App\Http\Controllers\Admin\Attendance\LeaveApprovalController::class);
    Route::get('approve-leave/{id}', [App\Http\Controllers\Admin\Attendance\LeaveApprovalController::class, 'ApproveLeave'])
        ->name('approve-leave');

    Route::post('add-attendance-excel/add-absent/store', [App\Http\Controllers\Admin\Attendance\AttendanceExcelController::class, 'storeImportAbsent'])->name('add-attendance-excel.absent');
    Route::get('add-attendance-excel/leave/delete', [App\Http\Controllers\Admin\Attendance\AttendanceExcelController::class, 'deleteAllImportAbsent'])->name('add-attendance-excel.absent.delete');
    Route::post('add-attendance-excel/leave/delete', [App\Http\Controllers\Admin\Attendance\AttendanceExcelController::class, 'postDeleteAllImportAbsent'])->name('add-attendance-excel.absent.post-delete');
    Route::resource('add-attendance-excel', App\Http\Controllers\Admin\Attendance\AttendanceExcelController::class);


    // Route::get('reject-leave/{id}', [App\Http\Controllers\Admin\Attendance\LeaveApprovalController::class, 'RejectLeave'])
    // ->name('reject-leave');
    // End Attendance Management

    // Freeze Attendance
    Route::resource('freeze-attendance', App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class);
    Route::get('employee-monthly-salary/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'showEmployeeSalary'])->name('employee-monthly-salary.showEmployeeSalary');
    Route::get('unfreeze-employee', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'unfreezeEmployee'])->name('unfreeze-employee');
    Route::get('freeze-employee', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'freezeEmployee'])->name('freeze-employee');

    Route::get('fetch-date-range/{month}', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'fetchDateRange'])
        ->name('fetch-date-range');
    // Freeze Attendance End

    // Supplimentary Bill Leave
    Route::get('supplimentary-leave-bill/fetch/details', [App\Http\Controllers\Admin\Employee\SupplimentaryLeaveController::class, 'fetchDetails'])->name('supplimentary-leave-bill.fetch-details');

    Route::get('supplimentary-leave-bill/fetch/update-details', [App\Http\Controllers\Admin\Employee\SupplimentaryLeaveController::class, 'fetchUpdateDetails'])->name('supplimentary-leave-bill.fetch-update-details');

    Route::post('supplimentary-leave-bill/generate/store', [App\Http\Controllers\Admin\Employee\SupplimentaryLeaveController::class, 'storeGenerate'])->name('supplimentary-leave-bill.store-generate');
    Route::resource('supplimentary-leave-bill', App\Http\Controllers\Admin\Employee\SupplimentaryLeaveController::class);
    // End of Supplimentary Bill Leave

    // Calculated regeneration Suplimentry leave bill
    Route::resource('supplimentary-calculate', App\Http\Controllers\Admin\Employee\SupplimentaryLeaveCalculateController::class);
    // End of Calculated regeneration Suplimentry leave bill

    // Supplimentary Bill
    Route::post('select-supplimentary-bill-no-of-days', [App\Http\Controllers\Admin\Employee\SupplimentaryController::class, 'fetchSupplimentaryBillNoofDays'])->name('select-supplimentary-bill-no-of-days');
    Route::resource('supplimentary-bill', App\Http\Controllers\Admin\Employee\SupplimentaryController::class);
    Route::get('fetch-employee-details-supplimentary/{ward}', [App\Http\Controllers\Admin\Employee\SupplimentaryController::class, 'fetchEmployeeDetails'])
        ->name('fetch-employee-details-supplimentary');
    Route::resource('suspend-supplimentary-bill', App\Http\Controllers\Admin\Employee\SuspendSupplimentaryController::class);

    Route::get('supplementary-leave-count', [App\Http\Controllers\Admin\Employee\SupplimentaryLeaveCountReportController::class,'index'])->name('view-supp-leave-count');
    Route::post('supplementary-leave-count-list', [App\Http\Controllers\Admin\Employee\SupplimentaryLeaveCountReportController::class,'list'])->name('supp-leave-count.list');

    // Reports Start
    Route::get('salary-slips/view-salary/group/{lang?}', [App\Http\Controllers\Admin\Report\SalarySlipController::class, 'viewSalaryInGroup'])->name('view-ward-salary-slips');

    Route::resource('salary-slips', App\Http\Controllers\Admin\Report\SalarySlipController::class);
    Route::get('salary-slips-marathi/{Emp_Code}', [App\Http\Controllers\Admin\Report\SalarySlipController::class, 'salarySlipMarathi'])
        ->name('salary-slips-marathi');


    Route::get('bank-statement/pdf', [App\Http\Controllers\Admin\Report\BankStatementController::class, 'bankStatementPdf'])->name('reports.bank-statement-pdf');
    Route::get('bank-statement/excel', [App\Http\Controllers\Admin\Report\BankStatementController::class, 'bankStatementExcel'])->name('reports.bank-statement-excel');
    Route::resource('bank-statement', App\Http\Controllers\Admin\Report\BankStatementController::class);
    Route::resource('pay-sheet', App\Http\Controllers\Admin\Report\PaySheetController::class);
    Route::get('pay-sheet-pdf', [App\Http\Controllers\Admin\Report\PaySheetController::class, 'showPaySheetPDF'])->name('pay-sheet-pdf');

    Route::resource('yearly-pay-sheet', App\Http\Controllers\Admin\Report\YearlyPaySheetController::class);
    Route::get('yearly-pay-sheet-pdf/{emp_id?}/{finyear?}', [App\Http\Controllers\Admin\Report\YearlyPaySheetController::class, 'showPaySheetPDF'])->name('yearly-pay-sheet-pdf');
    // Route::get('yearly-pay-sheet-marathi-pdf/{emp_id?}/{finyear?}', [App\Http\Controllers\Admin\Report\YearlyPaySheetController::class, 'showPaySheetMarathiPDF'])->name('yearly-pay-sheet-marathi-pdf');
    Route::get('yearly-pay-sheet-marathi-pdf/{emp_id?}/{finyear?}', [App\Http\Controllers\Admin\Report\YearlyPaySheetController::class, 'generateYearlyPaysheetPdf'])->name('yearly-pay-sheet-marathi-pdf');


    Route::resource('bank-deduction-employee-report', App\Http\Controllers\Admin\Report\BankDeductionEmployeeController::class);
    Route::resource('bank-deduction-report', App\Http\Controllers\Admin\Report\BankDeductionController::class);
    Route::resource('summary-department-report', App\Http\Controllers\Admin\Report\GrandSummaryDepartmentController::class);
    Route::get('grand-summary-department-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryDepartmentController::class, 'showGrandSummaryDepartmentPDF'])->name('grand-summary-department-pdf');
    Route::resource('summary-ward-report', App\Http\Controllers\Admin\Report\GrandSummaryWardController::class);
    Route::get('grand-summary-ward-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryWardController::class, 'showGrandSummaryWardPDF'])->name('grand-summary-ward-pdf');
    Route::resource('summary-report', App\Http\Controllers\Admin\Report\GrandSummaryController::class);
    Route::get('grand-summary-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryController::class, 'showGrandSummaryPDF'])->name('grand-summary-pdf');
    Route::resource('pay-sheet-excel', App\Http\Controllers\Admin\Report\PaySheetExcelController::class);

    Route::resource('allowance-report', App\Http\Controllers\Admin\Report\AllowanceReportController::class);
    Route::get('deduction-report/pdf/{lang}', [App\Http\Controllers\Admin\Report\DeductionReportController::class, 'deductionReportPdf'])->name('deduction-report.pdf');
    Route::resource('deduction-report', App\Http\Controllers\Admin\Report\DeductionReportController::class);

    Route::resource('retirement-report', App\Http\Controllers\Admin\Report\RetirementController::class);
    // Reports End


    // Pension Start
    Route::resource('pension-item-master', App\Http\Controllers\Admin\Pension\PensionItemMasterController::class);
    Route::resource('manage-pensioner-payroll', App\Http\Controllers\Admin\Pension\ManagePensionerPayrollControler::class);

    Route::resource('pension', App\Http\Controllers\Admin\Pension\PensionController::class);

    Route::post('pension-new/datatable/list', [App\Http\Controllers\Admin\Pension\PensionNewController::class, 'list'])->name('pension-new.list');
    Route::resource('pension-new', App\Http\Controllers\Admin\Pension\PensionNewController::class);
    Route::resource('freeze-pension', App\Http\Controllers\Admin\Pension\FreezePensionController::class);
    Route::resource('freeze-pension-new', App\Http\Controllers\Admin\Pension\FreezePensionNewController::class);

    Route::get('pensioner-bill-pdf/{month}/{from_date}/{to_date}', [App\Http\Controllers\Admin\Pension\FreezePensionController::class, 'showPensionerBillPDF'])->name('pensioner-bill-pdf');
    Route::resource('pensioner-bill', App\Http\Controllers\Admin\Pension\PensionerBillController::class);
    Route::get('pensioner-bill-new/pdf', [App\Http\Controllers\Admin\Pension\PensionBillNewController::class, 'pdf'])->name('pensioner-bill-new.pdf');
    Route::resource('pensioner-bill-new', App\Http\Controllers\Admin\Pension\PensionBillNewController::class);
    Route::get('pensioner-bill-bank-pdf', [App\Http\Controllers\Admin\Pension\PensionerBillController::class, 'showPensionerBillPDF'])->name('pensioner-bill-bank-pdf');
    Route::resource('pension-bank-statement', App\Http\Controllers\Admin\Pension\BankStatementController::class);
    Route::resource('grand-total-pensioner', App\Http\Controllers\Admin\Pension\GrandTotalPensionerController::class);
    Route::get('grand-total-pdf', [App\Http\Controllers\Admin\Pension\GrandTotalPensionerController::class, 'showGrandTotalPensionerPDF'])->name('grand-total-pdf');

    // Pension End


    // PF Start
    Route::resource('pf-opening-balance', App\Http\Controllers\Admin\PF\PFOpeningController::class);
    Route::resource('pf-department-loan', App\Http\Controllers\Admin\PF\PFDepartmentLoan::class);
    Route::get('fetch-employee-details-pf/{pf_no}', [App\Http\Controllers\Admin\PF\PFDepartmentLoan::class, 'fetchEmployeeDetails'])
        ->name('fetch-employee-details-pf');

    Route::resource('generate-pf-report', App\Http\Controllers\Admin\PF\GeneratePFReport::class);
    Route::resource('pf-report', App\Http\Controllers\Admin\PF\PFReport::class);
    Route::resource('pf-closing', App\Http\Controllers\Admin\PF\PFClosing::class);
    Route::post('pf-report-gpf', [App\Http\Controllers\Admin\PF\PFReport::class, 'pfReportGpf'])->name('pf-report-gpf');

    Route::get('show-dcps-ledger-page', [App\Http\Controllers\Admin\PF\PFReport::class, 'showDCPSLedgerPage'])->name('show-dcps-ledger-page');
    Route::post('show-dcps-ledger-report', [App\Http\Controllers\Admin\PF\PFReport::class, 'showDCPSLedgerReport'])->name('show-dcps-ledger-report');

    // PF End

    // Service book start
    Route::get('service-book-pdf', [App\Http\Controllers\Admin\ServiceBook\ServiceBookController::class, 'showServiceBookPDF'])->name('service-book-pdf');
    // Service book end

    Route::get('tax-calculation', [App\Http\Controllers\Admin\TaxCalculation\TaxCalculationController::class, 'showCalculationForm'])->name('tax-calculation');
    Route::get('fetch-emp-details/{Emp_Code}/{fi_year}', [App\Http\Controllers\Admin\TaxCalculation\TaxCalculationController::class, 'fetchEmployeeDetails'])->name('fetch-emp-details');
    Route::post('save-income-tax', [App\Http\Controllers\Admin\TaxCalculation\TaxCalculationController::class, 'saveIncomeTaxDetails'])->name('saveIncomeTaxDetails');
    Route::get('form16-list', [App\Http\Controllers\Admin\TaxCalculation\Form16Controller::class, 'index'])->name('form16-list');
    Route::post('form16-list', [App\Http\Controllers\Admin\TaxCalculation\Form16Controller::class, 'index'])->name('form16-list-fy');
    Route::get('show-form16/{incometaxid}', [App\Http\Controllers\Admin\TaxCalculation\Form16Controller::class, 'showForm16Pdf'])->name('show-form16-pdf');

    Route::post('save-tds-details', [App\Http\Controllers\Admin\TaxCalculation\TaxCalculationController::class, 'saveTDSDetails'])->name('saveTDSDetails');

    Route::get('income-tax/report', [App\Http\Controllers\Admin\TaxCalculation\ReportController::class, 'index'])->name('income-tax.report');
    Route::get('income-tax/report/excel', [App\Http\Controllers\Admin\TaxCalculation\ReportController::class, 'excel'])->name('income-tax.report-excel');


    // Users Roles n Permissions
    Route::resource('users', App\Http\Controllers\Admin\UserController::class);
    Route::get('users/{user}/toggle', [App\Http\Controllers\Admin\UserController::class, 'toggle'])->name('users.toggle');
    Route::get('users/{user}/retire', [App\Http\Controllers\Admin\UserController::class, 'retire'])->name('users.retire');
    Route::put('users/{user}/change-password', [App\Http\Controllers\Admin\UserController::class, 'changePassword'])->name('users.change-password');
    Route::get('users/{user}/get-role', [App\Http\Controllers\Admin\UserController::class, 'getRole'])->name('users.get-role');
    Route::put('users/{user}/assign-role', [App\Http\Controllers\Admin\UserController::class, 'assignRole'])->name('users.assign-role');
    Route::resource('roles', App\Http\Controllers\Admin\RoleController::class);


    Route::prefix('import')->name('import.')->group(function () {
        Route::get('salary/employee_credited_society', [App\Http\Controllers\Admin\ImportSalaryStuctureController::class, 'employeeCreditedSociety'])->name('salary.employee-credited-society');
        Route::post('salary/store_employee_credited_society', [App\Http\Controllers\Admin\ImportSalaryStuctureController::class, 'storeEmployeeCreditedSociety'])->name('salary.store-employee-credited-society');


        Route::get('employee/update-details', [App\Http\Controllers\ImportExcelController::class, 'updateEmployeeDetails'])->name('updateEmployeeDetails');
        Route::post('employee/update-details', [App\Http\Controllers\ImportExcelController::class, 'storeEmployeeUpdateDetails'])->name('store-employee-update-details');

        Route::get('employee/update-opening-balance', [App\Http\Controllers\ImportOpeningBalanceController::class, 'updateOpeningBalance'])->name('updateOpeningBalance');
        Route::post('employee/update-opening-balance', [App\Http\Controllers\ImportOpeningBalanceController::class, 'storeEmployeeUpdateDetails'])->name('store-employee-opening-balance');

        Route::get('attandance/adjustment', [App\Http\Controllers\AttandanceAdjustmentController::class, 'index'])->name('attandance-adjustment');
        Route::post('attandance/adjustment/save', [App\Http\Controllers\AttandanceAdjustmentController::class, 'save'])->name('attandance-adjustment.save');
    });

    Route::get('report/suplimentry-slip/excel', [App\Http\Controllers\Admin\Report\SuplimentryReportController::class, 'excel'])->name('reports.suplimentry-slip.excel');
    Route::get('report/suplimentry-slip', [App\Http\Controllers\Admin\Report\SuplimentryReportController::class, 'index'])->name('reports.suplimentry-slip');
    Route::get('report/suplimentry/bank-statement/pdf', [App\Http\Controllers\Admin\Report\SuplimentryReportController::class, 'bankStatementPdf'])->name('reports.suplimentry-bank-statement-pdf');
    Route::get('report/suplimentry/bank-statement/excel', [App\Http\Controllers\Admin\Report\SuplimentryReportController::class, 'bankStatementExcel'])->name('reports.suplimentry-bank-statement-excel');
    Route::get('report/suplimentry/bank-statement', [App\Http\Controllers\Admin\Report\SuplimentryReportController::class, 'bankStatement'])->name('reports.suplimentry-bank-statement');

    Route::post('show-salary-slips', [App\Http\Controllers\Admin\Report\SalarySlipController::class, 'showSalarySlips'])->name('show-salary-slips');

    // Route::get('import/attandance/excel', [App\Http\Controllers\ImportAttendanceController::class, 'index'])->name('import.attandance.excel');
    // Route::get('import/attandance/excel/store', [App\Http\Controllers\ImportAttendanceController::class, 'store'])->name('import.attandance.excel.store');
});

Route::middleware('auth')->group(function () {
    Route::get('export-yearly-tax/{fin_year}', [App\Http\Controllers\Admin\TaxCalculation\Form16Controller::class, 'exportExcel'])->name('export-yearly-tax');
});

//-----Mobile routes---------------------------------------//
Route::middleware(['auth', 'PreventBackHistory'])->group(function () {
    Route::prefix('mob')->name('mob.')->group(function () {
        Route::get('home-page', [App\Http\Controllers\Admin\Employee\PayrollController::class, 'index'])->name('home-page');
        Route::get('salary-slip', [App\Http\Controllers\Admin\Employee\PayrollController::class, 'showSalarySlipList'])->name('salary-slip');
    });
});
Route::get('mob/sspdf/show-salary-slip/{id}', [App\Http\Controllers\Admin\Employee\PayrollController::class, 'showSalarySlip'])->name('mob.show-salary-slip');
Route::get('mob/sspdf/salary-slip-marathi/{id}', [App\Http\Controllers\Admin\Employee\PayrollController::class, 'salarySlipMarathi'])->name('mob.marathi-salary-slip');
//------------------------------------------------------------//


Route::get('/php', function (Request $request) {
    if (!auth()->check())
        return 'Unauthorized request';

    Artisan::call($request->artisan);
    return dd(Artisan::output());
});

Route::get('export/employee/attendance', [App\Http\Controllers\ExportController::class, 'exportAttendanceWithLeaveApplied']);
