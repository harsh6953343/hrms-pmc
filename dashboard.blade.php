<x-admin.layout>
    <x-slot name="title">Dashboard</x-slot>
    <x-slot name="heading">Dashboard</x-slot>

    <style>
        .section-title {
            background-color: #d9cbe7;
            color: black;
            font-weight: bold;
            padding: 5px 10px;
            margin-bottom: 10px;
            text-align: center;
            border-radius: 5px;
        }

        .section-content {
            background-color: #f3f1f8;
            padding: 15px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .form-control {
            /* margin: 0; */
            padding: 5px;
            height: 35px;
        }

        .section-footer {
            background-color: #292929;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .section-footer input {
            background-color: #e0e0e0;
            color: #000;
            border: 0;
            font-weight: bold;
            text-align: center;
            width: 100%;
        }

        .claim-button {
            background-color: #ccc;
            color: black;
            border: 1px solid #999;
            padding: 3px 10px;
            border-radius: 3px;
        }

        .claim-button:hover {
            background-color: #bbb;
        }

       /* ---- Deduction  -----*/
       .hidden-section {
            display: none;
            margin-top: 2px;
            background-color: #ffffff;
            padding: 20px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .toggle-btn {
            background-color: #d9cbe7;
            cursor: pointer;
            padding: 10px;
            border-radius: 5px;
            text-align: center;
            font-weight: bold;
        }

        .deduction-table th,
        .deduction-table td {
            padding: 8px;
        }


        /* Claculate Tax */
        .calculate-btn {
            width: 50%;
            background-color: #8C68CD;
            color: white;
            padding: 10px;
            font-size: 25px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin: 0px auto;
        }


        .black-section {
            background-color: #333;
            color: white;
            padding: 20px;
            border-radius: 8px;
            margin-top: 20px;
        }

        .btnSubmit{
            width: 10%;
            background-color: #8C68CD;
            color: white;
            font-size: 20px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin: 20px auto;
            padding: 10px;
            text-align: center;
        }



    </style>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-header">
                    <!--------------------------- Tables  Data -------------------------------->
                        <div class="container">
                            <h2 class="text-center my-3" style="text-transform:uppercase">Income Tax Calculator</h2>

                            <!-- Section: Financial Year and Employee ID -->
                            <div class="">
                                <div class="section-title"> </div><br>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <label for="financial_year" class="form-label">Financial Year:</label>
                                        <select class="form-control" id="financial_year">
                                            <option>--Select--</option>
                                            <option value="2023-2024">2023-2024</option>
                                            <option value="2022-2023">2022-2023</option>
                                            <option value="2021-2022">2021-2022</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="employee_id" class="form-label">Employee ID:</label>
                                        <select class="form-control" id="employee_id">
                                            <option>--Select--</option>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-4  d-flex align-items-end">
                                        <button class="btn btn-flat" style=" margin-left:200px; background-color:#8C68CD;color:white">Print</button>
                                    </div>
                                </div>
                            </div>

                            <!-- Section: Personal Information -->
                            <div class="section-content mb-5" style="">
                                <div class="section-title">Personal Information</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td> Employee Name:</td>
                                            <td><input class="form-control" type="text" id="employee_name" placeholder=""></td>
                                            <td>Gender:</td>
                                            <td>
                                                <select class="form-control" >
                                                    <option>--Select--</option>
                                                    <option value="">Male</option>
                                                    <option value="">Female</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td><input class="form-control" type="date" id="date_of_birth" ></td>
                                            <td>Pan No:</td>
                                            <td><input class="form-control" type="text" id="pan_no" placeholder=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Salary Income -->
                            <div class="section-content">
                                <div class="section-title">Salary Income</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>Previous Employer Gross:</td>
                                            <td><input class="form-control" type="number" id="previous_gross" placeholder="$0.00"></td>
                                            <td>Current Gross Salary:</td>
                                            <td><input class="form-control" type="number" id="current_gross" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Salary:</td>
                                            <td><input class="form-control" type="number" id="estimated_salary" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Supplementary Bill:</td>
                                            <td><input class="form-control" type="number" id="supplementary_bill" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Supplementary Bill:</td>
                                            <td><input class="form-control" type="number" id="estimated_supplementary_bill" placeholder="$0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Gross Income:</label>
                                    <input type="number" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Exemptions -->
                            <div class="section-content mt-4">
                                <div class="section-title">Exemptions</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>House Rent Exemption:</td>
                                            <td><input class="form-control" type="number" id="house_rent_exemption" placeholder="$0.00"></td>
                                            <td><button class="claim-button">Claim</button></td>
                                            <td>Standard Deduction:</td>
                                            <td><input class="form-control" type="number" id="standard_deduction" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Previous Employer Ptax:</td>
                                            <td><input class="form-control" type="number" id="previous_ptax" placeholder="$0.00"></td>
                                            <td></td>
                                            <td>Professional Tax:</td>
                                            <td><input class="form-control" type="number" id="professional_tax" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Election Allowance:</td>
                                            <td><input class="form-control" type="number" id="election_allowance" placeholder="$0.00"></td>
                                            <td></td>
                                            <td>Ex Gratia:</td>
                                            <td><input class="form-control" type="number" id="ex_gratia" placeholder="$0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Transport Allowance:</td>
                                            <td><input class="form-control" type="number" id="transport_allowance" placeholder="$0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Total Exemptions:</label>
                                    <input type="number" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Net Salary -->
                            <div class="section-content mt-4">
                                <div class="section-title">Net Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="net_salary" placeholder="$0.00">
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Section: Other Income -->
                            <div class="section-content mt-4">
                                <div class="section-title">Other Income</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>Investment:</td>
                                            <td><input class="form-control" type="number" id="investment" placeholder="$0.00"></td>
                                            <td></td>
                                            <td>Other Investment:</td>
                                            <td><input class="form-control" type="number" id="other_investment" placeholder="$0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Total Investment:</label>
                                    <input type="number" class="form-control bg-light" >
                                </div>
                            </div>


                            <!-- Section: Deduction -->
                            <div class="section-content mt-4 deduction-section">
                                <div class="section-title">Deduction</div>

                                <!-- 8OD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODExplanation" onclick="toggleSection('bodExplanationDetails')">
                                    80D(Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="bodExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th colspan="2">Assessee, Spouse, and dependent Children</th>
                                                <th colspan="2">Assessee's parents</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Health Insurance (Age less Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health Insurance (Age More Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health-Checkup <span class="ri-information-fill"></span>:</td>
                                                <td colspan="1"><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OG Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOGExplanation"  onclick="toggleSection('bogExplanationDetails')">
                                    80G (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="bogExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Other Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OC Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCExplanation"  onclick="toggleSection('bocExplanationDetails')">
                                    80C (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="bocExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NSC:</td>
                                                <td><input class="form-control" type="number" id="nsc" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>NSC Intrest:</td>
                                                <td><input class="form-control" type="number" id="nsc_intrest" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>LIC:</td>
                                                <td><input class="form-control" type="number" id="lic" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Other LIC:</td>
                                                <td><input class="form-control" type="number" id="other_lic" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS Employee Contribution:</td>
                                                <td><input class="form-control" type="number" id="dcp_contribution" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Under 80ccc Mutual Fund:</td>
                                                <td><input class="form-control" type="number" id="mutual_fund" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GIS:</td>
                                                <td><input class="form-control" type="number" id="gis" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Other GIS:</td>
                                                <td><input class="form-control" type="number" id="other_gis" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>PLI:</td>
                                                <td><input class="form-control" type="number" id="pli" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Other PLI:</td>
                                                <td><input class="form-control" type="number" id="other_pli" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Housing Loan Repayment:</td>
                                                <td><input class="form-control" type="number" id="housing_loan_repayment" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Other Housing Loan Repayment:</td>
                                                <td><input class="form-control" type="number" id="other_housing_loan_repayment" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS 1 Employer Contribution:</td>
                                                <td><input class="form-control" type="number" id="dcps_one_contribution" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>Infrastructure Bond:</td>
                                                <td><input class="form-control" type="number" id="other_housing_loan_repayment" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GPF:</td>
                                                <td><input class="form-control" type="number" id="housing_loan_repayment" placeholder="$0.00"></td>
                                                <td></td>
                                                <td>PPF:</td>
                                                <td><input class="form-control" type="number" id="ppf" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Education Fees:</td>
                                                <td><input class="form-control" type="number" id="education_fees" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80CCD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleB0CCDExplanation" onclick="toggleSection('BOCCDExplanationDetails')">
                                    80CCD (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BOCCDExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NPS :</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td>DCPS + Under 80CCD :</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80E Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOEExplanation" onclick="toggleSection('BOEExplanationDetails')">
                                    80E (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BOEExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Education Loan :</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- Section24 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleSection24Explanation" onclick="toggleSection('section24ExplanationDetails')">
                                    Section24 ▼

                                    <span style="margin-left: 360px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="section24ExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Home Loan Interest Before : </td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td>Home Loan Interest After:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Oth Home Loan Interest Before:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                                <td>Oth Home Loan Interest After:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80DDB Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODDBExplanation" onclick="toggleSection('BODDBExplanationDetails')">
                                    80DDB (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BODDBExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Medical Treatment  :</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80TTA Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOTTAExplanation" onclick="toggleSection('BOTTAExplanationDetails')">
                                    80TTA (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BOTTAExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Deduction On SB Interest :</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OU Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOUExplanation"  onclick="toggleSection('BOUExplanationDetails')">
                                    80U (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BOUExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Disability:</td>
                                                <td>40% <input type="radio" placeholder="$0.00"></td>
                                                <td>60% <input type="radio" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Handicapped Allowance <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OCCD2 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCCD2Explanation"  onclick="toggleSection('BOCCD2ExplanationDetails')">
                                    8OCCD2 (Explanation) ▼

                                    <span style="margin-left: 270px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block" style="width: 150px;" placeholder="$0.00">
                                </div>

                                <div class="hidden-section" id="BOCCD2ExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Employer Contribution:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" class="form-control bg-light" >
                                    </div>
                                </div>

                            </div>


                            <!-- Section: Total Taxable Salary -->
                            <div class="section-content mt-4">
                                <div class="section-title">Total Taxable Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="net_salary" placeholder="$0.00">
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Tax Toggle Button -->
                            <div class="section-content mt-4" >
                                <button class="calculate-btn" id="calculateTaxBtn" onclick="toggleSection('TaxExplanationDetails')">Calculate Tax</button>

                                <div class="hidden-section" id="TaxExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        Slabwise Calculation
                                    </table>
                                </div>
                            </div>


                            <!-- Black Data Section -->
                            <div class="black-section">
                                <div class="" id="">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Tax Payable:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Rebate Under 87A: </td>
                                                <td><input type="checkbox" ></td>
                                                <td>Rebated Amount: </td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable Before Cess:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Edu Cess(4%): </td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Tax Paid in Cash: </td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable After Cess:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Tax Deducted Upto Now:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Tax Pending: </td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax for Remaining Months:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>Tax Refunded:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                                <td>User Requested Deduction: </td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Reason For Requestd Deduction:</td>
                                                <td><input type="text" class="form-control" placeholder="$0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <button class="btnSubmit">Submit</button>


                        </div>
                    <!--------------------------- Tables  Data -------------------------------->
                </div>

            </div>
        </div>
    </div>



    <script>
        function toggleSection(sectionId) {
            var selectedSection = document.getElementById(sectionId);

            var isVisible = selectedSection.style.display === 'block';

            var allSections = document.querySelectorAll('.hidden-section');
            allSections.forEach(function (section) {
                section.style.display = 'none';
            });

            if (!isVisible) {
                selectedSection.style.display = 'block';
            }
        }
    </script>



</x-admin.layout>
