@php
    use App\Models\AddEmployeeLeave;
    use Carbon\Carbon;
@endphp
<table>
    <thead>
        <tr>
            <th>Emp Code</th>
            <th>Employee Name</th>
            <th>Ward</th>
            <th>Department</th>
            <th>Total Preset Day</th>
            <th>Total Approve Leave</th>
        </tr>
    </thead>
    <tbody>
        @foreach($attendances as $attendance)
        <tr>
            <th>{{ $attendance->employee?->employee_id }}</th>
            <th>{{ $attendance->employee?->fname." ".$attendance->employee?->mname." ".$attendance->employee?->lname }}</th>
            <th>{{ $attendance->employee?->ward?->name }}</th>
            <th>{{ $attendance->employee?->department?->name }}</th>
            <th>{{ $attendance->total_present_days }}</th>
            @php
                $fromDate = Carbon::parse("2025-02-01")->format('Y-m-d');
                $toDate = Carbon::parse("2025-02-28")->format('Y-m-d');
                $leaves = AddEmployeeLeave::where('from_date', '>=', $fromDate)
                ->where('to_date', '<=', $toDate)
                ->where('status', 1)    
                ->where('Emp_Code', $attendance->Emp_Code)
                ->get();
                $leaveDaysCount  = 0;
                foreach ($leaves as $leave) {
                    $leaveStart = Carbon::parse($leave->from_date);
                    $leaveEnd = Carbon::parse($leave->to_date);
                    
                    while ($leaveStart->lte($leaveEnd) && $leaveStart->lte(Carbon::parse($toDate))) {
                        if ($leaveStart->day < 21) { // Count only if the date is 20  less
                            $leaveDaysCount++;
                        }
                        $leaveStart->addDay();
                    }
                }
            @endphp
            <th>{{ $leaveDaysCount }}</th>
        </tr>
        @endforeach
    </tbody> 
</table>
