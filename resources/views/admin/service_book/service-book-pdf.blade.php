@php
  use App\Models\EmployeeMonthlyLoan;
  use App\Models\SupplimentaryBill;
  use App\Models\RemainingFreezeSalary;
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Service Book PDF</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
        border: 1px solid rgb(177, 173, 173);
        border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:20px;
            padding: 15px;
            text-align: left;
            /* background-color: #feebd1; */

        }

        table,td
        {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 15px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
                page-break-after: always;
            }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }

        @page {
            @top-center {
            content: element(pageHeader);
            }
        }
        #header{

            position: running(pageHeader);
        }
    </style>
</head>
<body>

    <table style="width: 100%; border:none; margin-bottom: 20px;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:center;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" style="height: auto; max-width: 400px;">
                    @endif
                </td>
            </tr>
        </thead>
    </table>

    <div style="text-align: center; font-family: Arial, sans-serif;">
        <h1 style="font-size: 34px; margin: 5px 0; line-height: 1.9;">{{ $corporation->name }}</h1>
        <h2 style="font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Service Book</h2>

        <h3 style="font-size: 25px; margin: 15px 0; line-height: 1.9;">{{ $employee->fname }} {{ $employee->mname }} {{ $employee->lname }}</h3>
        <h4 style="font-size: 25px; margin: 5px 0; line-height: 1.9;">Post: {{ $employee->designation->name }}</h4>

        <div style="font-size: 25px; margin: 15px 0; line-height: 1.9;">
            <p>Date of Birth: <strong>{{ $employee->dob }}</strong></p>
            <p>Date of Enrollment: <strong> {{ $employee->doj }} </strong></p>
            <p>Date of Retirement: <strong>{{ $employee->retirement_date }}</strong></p>
        </div>
    </div>

    <div class="page-break"></div>
    {{-- Service roll start --}}

    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">SERVICE ROLL</h2>
    <h2 style="text-align: center;">
        (To be maintained for Goverment Servants of Class IV and Class III for Whom
            Service Books are not mainatined.)
    </h2>

    <table style="width: 100%; margin-top:8%;">
        <thead>
            <tr>
                <th>Employee Id:-</th>
                <th>{{ $employee->employee_id }}</th>
                <th>Employee Type:-</th>
                <th>PERMENANT</th>
            </tr>
            <tr>
                <th>Name:-</th>
                <th>{{ $employee->fname." ".$employee->mname." ".$employee->lname }}</th>
                <th>Gender:-</th>
                <th>{{ ($employee->gender == 1) ? 'Male': 'Female'  }}</th>
            </tr>
            <tr>
                <th>Date Of Birth:-</th>
                <th>{{ $employee->dob }}</th>
                <th>Date Of Enrollment:-</th>
                <th>{{ $employee->doj }}</th>
            </tr>
            <tr>
                <th>Date Of Permanent Employment:-</th>
                <th></th>
                <th>Caste:-</th>
                <th>{{ $employee->caste  }}</th>
            </tr>
            <tr>
                <th>Tribe:-</th>
                <th></th>
                <th>Native Place:-</th>
                <th></th>
            </tr>
            <tr>
                <th>Home Town:-</th>
                <th></th>
                <th>Height:-</th>
                <th></th>
            </tr>
            <tr>
                <th>Marks Of Identification:-</th>
                <th></th>
                <th>Degree Of Education:-</th>
                <th></th>
            </tr>
            <tr>
                <th>Retirement Date:-</th>
                <th></th>
                <th>Thumb Impression:-</th>
                <th></th>
            </tr>

        </thead>
    </table>

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
    <!---------------------------Appointment Letter Start---------------------->

    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Appointment Letter</h2>



    <!---------------------------End Appointment Letter Start---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
    <!---------------------------Leave Details Start---------------------->

    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Leaves Balance Details</h2>

    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Type Of Leave</th>
                <th>Days of Leave</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employee_leaves as $emp_leave)
            <tr>
                <td>{{ $emp_leave->leaveType->name }}</td>
                <td>{{ $emp_leave->no_of_leaves }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Leaves Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Type Of Leave</th>
                <th>From Date </th>
                <th>To Date </th>
                <th>No. of Days </th>
                <th>Leaves Details File </th>
                <th>Remark</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($leave_taken_details as $leave_taken)
            <tr>
                <td>{{ $leave_taken->leaveType->name }}</td>
                <td>{{ $leave_taken->from_date }}</td>
                <td>{{ $leave_taken->to_date }}</td>
                <td>{{ $leave_taken->no_of_days }}</td>
                <td>-</td>
                <td>{{ $leave_taken->remark }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!---------------------------Leave Details End---------------------->


    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Transfer of Employee Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Transfer of Employee</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Transfer Order Date </th>
                <th>Transfer Order File </th>
                <th>Remark</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transfer_details as $transfer_detail)
            <tr>
                <td>{{ $transfer_detail->transfer_date }}</td>
                <td>{{ $transfer_detail->transfer_order }}</td>
                <td>{{ $transfer_detail->remark }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!---------------------------End Transfer of Employee---------------------->


    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Suspension Details Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Suspension Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Type Of Suspension </th>
                <th>From Date </th>
                <th>To Date</th>
                <th>Reason</th>
                <th>Action Taken</th>
                <th>Suspension Order No.</th>
                <th>Suspension Order Date</th>
                <th>Suspension File </th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!---------------------------End Suspension Details---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Demotion Details Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Demotion Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Date </th>
                <th>Order No.</th>
                <th>Description</th>
                <th>Uploaded File</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!---------------------------End Demotion Details---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Rejoining Details Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Rejoining Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Officer Name </th>
                <th>Rejoining Date</th>
                <th>Order Number</th>
                <th>Order Date</th>
                <th>Rejoining File</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!---------------------------End Rejoining Details---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Promotion Details Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Promotion Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Previous Class </th>
                <th>Previous Post</th>
                <th>Current Class</th>
                <th>Current Post</th>
                <th>From Date</th>
                <th>To Date</th>
                <th>Previous Pay</th>
                <th>Previous Pay Band</th>
                <th>Previous Grade Pay</th>
                <th>Previous Level</th>
                <th>Order No.</th>
                <th>Order Date</th>
                <th>Promotion File</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!---------------------------End Promotion Details---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>

    <!---------------------------Increment Details Start---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Increment Details</h2>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>Previous Pay </th>
                <th>Previous Pay Band </th>
                <th>Previous Grade Pay </th>
                <th>Previous Level</th>
                <th>Current Pay</th>
                <th>Current Pay Band</th>
                <th>Current Grade Pay</th>
                <th>Current Level</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!---------------------------End Increment Details---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
     <!---------------------------Education & Qualification Start---------------------->
     <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Education & Qualification</h2>
     <table style="width: 100%;">
         <thead>
             <tr>
                 <th>Qualification </th>
                 <th>Passing Month / Year </th>
                 <th>Board </th>
                 <th>University</th>
                 <th>Subject</th>
                 <th>Percentage</th>
                 <th>Grade</th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>
     <!---------------------------End Education & Qualification---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
     <!---------------------------Details Of Service Start---------------------->
     <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Details Of Service</h2>
     <table style="width: 100%;">
         <thead>
             <tr>
                 <th>From Date </th>
                 <th>To Date  </th>
                 <th>Pay </th>
                 <th>Pay Band </th>
                 <th>Grade Pay</th>
                 <th>Percentage</th>
                 <th>Level</th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>
     <!---------------------------End Details Of Service---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
     <!---------------------------Confidential Report Start---------------------->
     <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Confidential Report </h2>
     <table style="width: 100%;">
         <thead>
             <tr>
                 <th>Post </th>
                 <th>Financial Year  </th>
                 <th>Reporting Officer </th>
                 <th>From Date </th>
                 <th>To Date</th>
                 <th>Reviewing Officer</th>
                 <th>From Date</th>
                 <th>To Date</th>
                 <th>Overall Gradation by Reporting Officer</th>
                 <th>Overall Gradation by Reviewing Officer</th>
                 <th>Date</th>
                 <th>Place  </th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>
     <!---------------------------End Confidential Report---------------------->

    <div class="page-break"></div>
    <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
        <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
    </div>
     <!---------------------------Other Document---------------------->
     <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Other Document </h2>
     <table style="width: 100%;">
         <thead>
             <tr>
                <th>Office orders </th>
                <th>Promotion order </th>
                <th>Wage Fixation Order </th>
                <th>Nomination letter </th>
                <th>Order of Dismissal / Reconciliation / Memorandum / Inquiry / Restored </th>
                <th>Academic record</th>
                <th>Caste Certificate</th>
                <th>Record of caste verification certificate </th>
                <th>Marathi Typing Doc</th>
                <th>English Typing Doc</th>
                <th>Special Leave (Cancer / Child Care / Maternity / T.B. / etc.) </th>
                <th>Earned and half pay / retroactive leave </th>
                <th>Retirement Order </th>
                <th>Encashment of leave </th>
                <th>Daily wage order </th>
                <th>Annual Pay Increment / Additional Pay Increment </th>
                <th>Language Name </th>
                <th>Language Read</th>
                <th>Language Write </th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>
     <!---------------------------End Other Document---------------------->

     <div class="page-break"></div>
     <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
         <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
     </div>
      <!---------------------------Retirement Details---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Retirement Details </h2>
        <table style="width: 100%;">
          <thead>
              <tr>
                 <th>Start Date </th>
                 <th>End Date </th>
                 <th>Order No. </th>
                 <th>Upload Order File </th>
              </tr>
          </thead>
          <tbody>

          </tbody>
    </table>
    <!---------------------------End Retirement Details---------------------->

    <div class="page-break"></div>
     <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
         <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
     </div>
      <!---------------------------Pension Details---------------------->
    <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">Retirement Details </h2>
        <table style="width: 100%;">
          <thead>
              <tr>
                <th> </th>
                <th> </th>
                <th> </th>
                <th> </th>
              </tr>
          </thead>
          <tbody>

          </tbody>
    </table>
    <!---------------------------End Pension Details---------------------->


    <div class="page-break"></div>
     <div class="header" style="border-bottom: 1px solid gainsboro;font-size: 10px;margin-top: 10px;">{{ $corporation->name }}
         <img src="data:image/png;base64,{{ $base64Logo }}" alt="" style="height:50px;float: right;">
     </div>
     <h2 style="text-align:center; font-size: 32px; margin: 10px 0; text-transform: uppercase; line-height: 1.9;">नामनिर्देशनाचा नमुना </h2>

     <p style="font-size: 30px; padding:15px;">
        (जेव्हा अर्जदाराला कुटुंब असेल आणि त्यातील एका सदस्याला नामनिर्देशित करण्याची त्याची इच्छा असेल.)
        सर्वसाधारण भविष्यनिर्वाह निधीतील माझ्या नावे असलेली रक्कम देय होण्यापूर्वी किया देय होऊनही ती देण्यापूर्वी माझा मृत्यू झाला.
        तर ती रक्कम घेण्यास मी मुंबई सर्वसाधारण भविष्यनिर्वाह निधी नियमाच्या नियम २ मध्ये केलेल्या व्याख्येनुसार माझ्या कुटुंबाचा सदस्य असलेल्या खाली निर्देशित केलेल्या व्यक्तीला / व्यक्तींना निर्देशित करीत आहे.
        (जेव्हा अर्जदाराला कुटुंब नसेल व एका व्यक्तीस नामनिर्देशन करण्याची इच्छा असेल.)
        सर्वसाधारण भविष्यनिर्वाह निधीतील माझ्या नावे असलेली रक्कम देय होण्यापूर्वी किंवा देय होऊनही ती देण्यापूर्वी माझा मृत्यू झाला तर ती रक्कम घेण्यास मी,
        याद्वारे मुंबई सर्वसाधारण भविष्यनिर्वाह निधी नियमाच्या नियम २ मध्ये केलेल्या व्याख्येनुसार मला कुटुंब नसल्यामुळे खाली निर्देशित केलेल्या व्यक्तीस नामनिर्देशित करीत आहे.
     </p>


</body>
</html>
