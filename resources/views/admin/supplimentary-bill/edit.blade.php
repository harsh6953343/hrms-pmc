<x-admin.layout>
    <x-slot name="title">Update Generate Supplimentary Bill</x-slot>
    <x-slot name="heading">Update Generate Supplimentary Bill</x-slot>
    <style>
        #myInput::-webkit-input-placeholder {
            color: black;
            font-weight: bold;
        }
    </style>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <form class="theme-form" name="editForm" id="editForm" enctype="multipart/form-data">
                @method('put')
                @csrf
            <div class="card">
                <header class="card-header">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Update Supplimentary Bill</h4>
                        <button type="submit" class="btn btn-primary" id="addSubmit">Update</button>
                    </div>
                </header>

                <div class="card-body">
                    <div class="mb-3 row">
                        <input type="hidden" name="id" id="updateSupplimentryId" value="{{ $suplimentryBill->id }}">
                        <div class="col-md-4">
                            <label class="col-form-label" for="bill_no">Bill No<span class="text-danger">*</span></label>
                            <input class="form-control" id="bill_no" name="bill_no" type="text" value="{{ $suplimentryBill->bill_no }}" required readonly>
                            <span class="text-danger invalid bill_no_err"></span>
                        </div>
                        
                        <div class="col-md-4">
                            <label class="col-form-label" for="bill_description">Bill Description<span class="text-danger">*</span></label>
                            <input class="form-control" id="bill_description" name="bill_description" type="text" placeholder="Enter Bill Description" value="{{ $suplimentryBill->bill_description }}" required>
                            <span class="text-danger invalid bill_description_err"></span>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label" for="cheque_date">Select Cheque Date<span class="text-danger">*</span></label>
                            <input class="form-control datepicker" id="cheque_date" name="cheque_date" type="text" placeholder="Enter Employee Id" value="{{ \Carbon\Carbon::parse($suplimentryBill->cheque_date)->format('d-m-Y') }}" required readonly>
                            <span class="text-danger invalid cheque_date_err"></span>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label" for="ward_id">Select Group<span class="text-danger">*</span></label>
                            <select name="ward_id" class="form-select" id="selectWardId" required>
                                <option value="">--Select--</option>
                                @foreach($wards as $ward)
                                <option @if($suplimentryBill->ward_id == $ward->id)selected @endif value="{{ $ward->id }}">{{ $ward->name }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger invalid ward_id_err"></span>
                        </div>
                        

                        <div class="col-md-4">
                            <label class="col-form-label" for="ward_id">Select Payment Status<span class="text-danger">*</span></label>
                            <select name="payment_status" class="form-select" required>
                                <option value="">--Select--</option>
                                <option @if($suplimentryBill->payment_status == "1")selected @endif value="1">Not Processed</option>
                                <option @if($suplimentryBill->payment_status == "2")selected @endif value="2">Processed & Not Paid</option>
                                <option @if($suplimentryBill->payment_status == "3")selected @endif value="3">Processed & Paid</option>
                            </select>
                            <span class="text-danger invalid ward_id_err"></span>
                        </div>                      
                    </div>

                </div>
                <div class="card-footer">
                    <input type="text" id="myInput" class="form-control mb-3" onkeyup="myFunction()" placeholder="Search here">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="selectAll" class="form-check-input">
                                    </th>
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Department</th>
                                    <th>Group Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="d-none departmentId" name="department_id[]" value="{{ $employee->department_id }}" 
                                            @if(in_array($employee->id, $suplimentryBill->supplimentryEmployeeBills->pluck('employee_id')->toArray()))checked @endif />

                                            <input type="checkbox" class="d-none employeeId" name="employee_id[]" value="{{ $employee->id }}" @if(in_array($employee->id, $suplimentryBill->supplimentryEmployeeBills->pluck('employee_id')->toArray()))checked @endif />

                                            <input type="checkbox" name="emp_code[]" value="{{ $employee->employee_id }}" class="form-check-input emp_code_input" @if(in_array($employee->id, $suplimentryBill->supplimentryEmployeeBills->pluck('employee_id')->toArray()))checked @endif>
                                        </td>
                                        <td>{{ $employee->employee_id}}</td>
                                        <td>{{ $employee->fname}} {{ $employee->mname }} {{ $employee->lname }}</td>
                                        <td>{{ $employee->department?->name }}</td>
                                        <td>{{ $employee->ward?->name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, j, txtValue, found;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("dataTable");
        tr = table.getElementsByTagName("tr");
        
        // Loop through all rows in the table (except the header row)
        for (i = 1; i < tr.length; i++) {
            tr[i].style.display = "none"; // Hide the row by default
            td = tr[i].getElementsByTagName("td");
            found = false; // Reset the found flag
        
            // Loop through all columns in the row
            for (j = 0; j < td.length; j++) {
            if (td[j]) {
                txtValue = td[j].textContent || td[j].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                found = true; // Match found
                break; // No need to check other columns
                }
            }
            }
        
            // If a match was found in any column, display the row
            if (found) {
            tr[i].style.display = "";
            }
        }
    }
</script>
    
<script>
    $('#editForm').submit(function(e) {
        e.preventDefault();
        if ($('input[name="emp_code[]"]:checked').length === 0) {
            alert('Please select at least one employee.');
            return false;
        }
        $("#addSubmit").prop('disabled', true);

        let id = $('#updateSupplimentryId').val();
        var formdata = new FormData(this);
        var url = "{{ route('supplimentary-bill.update', ':model_id') }}";
        $.ajax({
            url: url.replace(':model_id', id),
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('supplimentary-bill.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });


    $(document).ready(function() {

        $("#selectWardId").change(function() {
            var group = $(this).val();
            
            if (group != '') {
                var url = "{{ route('fetch-employee-details-supplimentary', ':group') }}";

                $.ajax({
                    url: url.replace(':group', group),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error) {
                            $('#dataTable tbody').empty();
                            var html = "";
                            $.each(data.employee_details, function(key, value){
                                html += `
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="d-none departmentId" name="department_id[]" value="${value.department_id}" />
                                            <input type="checkbox" class="d-none employeeId" name="employee_id[]" value="${value.id}" />
                                            <input type="checkbox" name="emp_code[]" value="${value.employee_id}" class="form-check-input emp_code_input">
                                        </td>
                                        <td>${value.employee_id}</td>
                                        <td>${value.fname} ${value.mname} ${value.lname}</td>
                                        <td>${value?.department?.name}</td>
                                        <td>${value?.ward?.name}</td>
                                </tr>`;
                            });

                         
                            $('#dataTable tbody').html(html);
                            // Check if DataTable already exists and destroy it
                            

                            $('.card-footer').removeClass('d-none');
                            $('#addSubmit').prop('disabled', false);
                        }else{
                            alert('Employee not found')
                            $('.card-footer').addClass('d-none');
                            $('#addSubmit').prop('disabled', true);
                        }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            } else {
                $('#dataTable tbody').empty();
                $('.card-footer').addClass('d-none');
                $('#addSubmit').prop('disabled', true);
            }

        });
    });

    $(document).ready(function(){
        $('#selectAll').change(function(){
            
            if(this.checked){
                $('body').find('.emp_code_input').prop('checked', true);
            }else{
                $('body').find('.emp_code_input').prop('checked', false);
            }
        });

        $('body').on('change', '.emp_code_input', function(){
            if(this.checked){
                $(this).closest("td").find('.departmentId').prop('checked', true);
                $(this).closest("td").find('.employeeId').prop('checked', true);
            }else{
                $(this).closest("td").find('.departmentId').prop('checked', false);
                $(this).closest("td").find('.employeeId').prop('checked', false);
            }
        });
    });
</script>