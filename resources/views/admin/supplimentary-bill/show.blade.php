<x-admin.layout>
    <x-slot name="title">View Generate Supplimentary Bill</x-slot>
    <x-slot name="heading">View Generate Supplimentary Bill</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">View Supplimentary Bill</h4>
                </header>

                <div class="card-body">
                    <div class="mb-3 row">
                        
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Bill No</th>
                                        <td>{{ $suplimentryBill->bill_no }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Bill Description</th>
                                        <td>{{ $suplimentryBill->bill_description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cheque Date</th>
                                        <td>{{ \Carbon\Carbon::parse($suplimentryBill->cheque_date)->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Group</th>
                                        <td>{{ $suplimentryBill?->ward?->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Status</th>
                                        <td>
                                            @if($suplimentryBill->payment_status == "1")
                                            Not Processed
                                            @elseif($suplimentryBill->payment_status == "2")
                                            Processed & Not Paid
                                            @elseif($suplimentryBill->payment_status == "3")
                                            Processed & Paid
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <h4 class="mt-3">Select Employee List</h4>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Group Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($suplimentryBill->supplimentryEmployeeBills as $suplimentry)
                                        <tr>
                                            <td>{{ $suplimentry?->employee?->employee_id}}</td>
                                            <td>{{ $suplimentry?->employee?->fname}} {{ $suplimentry?->employee->mname }} {{ $suplimentry?->employee->lname }}</td>
                                            <td>{{ $suplimentry->department?->name }}</td>
                                            <td>{{ $suplimentryBill->ward?->name}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</x-admin.layout>
