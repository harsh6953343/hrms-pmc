<x-admin.layout>
    <x-slot name="title">Supplementary Leave Count</x-slot>
    <x-slot name="heading">Supplementary Leave Count</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Supplementary Leave Count</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">

                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="number" placeholder="Enter Employee Id">
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1"  >January</option>
                                    <option value="2" >February</option>
                                    <option value="3" >March</option>
                                    <option value="4" >April</option>
                                    <option value="5" >May</option>
                                    <option value="6" >June</option>
                                    <option value="7" >July</option>
                                    <option value="8" >August</option>
                                    <option value="9" >September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <span class="text-danger invalid month_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="leave_type_id">Select Leave Type<span class="text-danger">*</span></label>
                                <select class="form-select" id="leave_type_id" name="leave_type_id">
                                    <option value="">Select Leave Type</option>
                                    @foreach ($leaveTypes as $leaveType)
                                        <option value="{{ $leaveType->id }}">{{ $leaveType->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid type_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="from_date" name="from_date" type="date" placeholder="Enter From Date">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="to_date" name="to_date" type="date" placeholder="Enter To Date">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="rejoin_date">Rejoin Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="rejoin_date" name="rejoin_date" type="date" placeholder="Enter Rejoin Date" readonly>
                                <span class="text-danger invalid rejoin_date_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="no_of_days">No of Days<span class="text-danger">*</span></label>
                                <input class="form-control" id="no_of_days" name="no_of_days" type="text" placeholder="No. Of Days" readonly>
                                <span class="text-danger invalid no_of_days_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="leave_files">Select File<span class="text-danger">*</span></label>
                                <input class="form-control" id="leave_files" name="leave_files" type="file">
                                <span class="text-danger invalid leave_files_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="remark">Remark<span class="text-danger"></span></label>
                                <textarea name="remark" id="remark" class="form-control"></textarea>
                                <span class="text-danger invalid remark_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Allowance</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Allowance (In English)<span class="text-danger">*</span></label>
                                <input class="form-control title" id="allowance" name="allowance" type="text" placeholder="Enter Allowance In English">
                                <span class="text-danger invalid allowance_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="allowance_in_marathi">Allowance (In marathi)<span class="text-danger">*</span></label>
                                <input class="form-control title" id="allowance_in_marathi" name="allowance_in_marathi" type="text" placeholder="Enter Allowance (In Marathi)">
                                <span class="text-danger invalid allowance_in_marathi_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="type">Select Type<span class="text-danger">*</span></label>
                                <select class="form-select type_class" id="edit_type" name="type">
                                    <option value="">Select Type</option>
                                </select>
                                <span class="text-danger invalid type_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="amount">Amount<span class="text-danger">*</span></label>
                                <input class="form-control title" id="edit_amount" name="amount" type="text" placeholder="Enter Amount">
                                <span class="text-danger invalid amount_err"></span>
                            </div>

                            <div class="col-md-4 mt-5">
                                <label class="col-form-check-label" for="formCheck8">
                                    Is Applicable On Suspension
                                </label>
                                <input class="form-check-input size-checkbox" type="checkbox" id="is_applicable" name="is_applicable" checked="" value="1">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="calculation">Allowance Calculation <b>(If dynamic then calculation will change as per employee attendance)</b><span class="text-danger">*</span></label>
                                <select class="form-select" id="edit_calculation" name="calculation">
                                    <option value="">Select Calculation</option>
                                </select>
                                <span class="text-danger invalid calculation_err"></span>
                            </div>


                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                {{-- @can('allowance.create') --}}
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- @endcan --}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Department</th>
                                    <th>Leave</th>
                                    <th>Apply Date</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>No. of Days</th>
                                    <th>File</th>
                                    <th>Remark</th>
                                    <th>Status</th>
                                    @if(Auth::user()->hasRole(['AMC', 'DMC', 'Ward HOD', 'Department HOD']) || Auth::user()->id == 1)
                                    <th>AMC</th>
                                    <th>AMC Approve / Reject Datetime</th>
                                    @endif
                                    @if(Auth::user()->hasRole(['DMC', 'Ward HOD', 'Department HOD']) || Auth::user()->id == 1)
                                    <th>DMC</th>
                                    <th>DMC Approve / Reject Datetim</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('supp-leave-count.list') }}",
                        type: "post",
                        data: function(d){
                            d._token = "{{ csrf_token() }}";
                        }
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'Emp_Code',
                            name: 'Emp_Code',
                            orderable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'full_name',
                            name: 'full_name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'department_name',
                            name: 'department_name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'leave_type_name',
                            name: 'leave_type_name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'apply_date',
                            name: 'apply_date',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'from_date',
                            name: 'from_date',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'to_date',
                            name: 'to_date',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'no_of_days',
                            name: 'no_of_days',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'leave_file',
                            name: 'leave_file',
                            orderable: false,
                            render: function(data, type, row){
                                return `<a href="${data}" target='_blank' class='btn btn-primary btn-sm'>View File</a>`;
                            },
                        },
                        {
                            data: 'remark',
                            name: 'remark',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'status',
                            name: 'status',
                            orderable: true,
                            render: function(data, type, row){
                                if (data == 1)
                                    return 'Approved';
                                else if (data == 2)
                                    return 'Rejected';
                                else
                                    return 'Pending';

                            },
                        },
                        @if(Auth::user()->hasRole(['AMC', 'DMC', 'Ward HOD', 'Department HOD']) || Auth::user()->id == 1)
                        {
                            data: 'amc_status',
                            name: 'amc_status',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'amc_datetime',
                            name: 'amc_datetime',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        @endif
                        @if(Auth::user()->hasRole(['DMC', 'Ward HOD', 'Department HOD']) || Auth::user()->id == 1)
                        {
                            data: 'dmc_status',
                            name: 'dmc_status',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'dmc_datetime',
                            name: 'dmc_datetime',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        @endif
                    ]
                });
            })
        </script>
    @endpush


</x-admin.layout>


{{-- Add --}}
<script>
    $(document).ready(function() {
        $("#searchBtn").click(function(){
                var Emp_Code = $('#Emp_Code').val();
                var month = $('#month').val();
                if(Emp_Code != '' && month != '')
                {
                    var url = "{{ route('fetch-employee-details-leave', [':Emp_Code', ':month']) }}";

                    $.ajax({
                        url: url.replace(':Emp_Code', Emp_Code).replace(':month', month),
                        type: 'GET',
                        data: {
                            '_method': "GET",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data) {
                            if (!data.error && !data.error2) {
                                if (data.result === 1) {

                                    var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                    $('#emp_name').val(full_name);
                                    $('#ward').val(data.employee_details.ward.name);
                                    $('#department').val(data.employee_details.department.name);
                                    $('#class').val(data.employee_details.class.name);
                                    $('#designation').val(data.employee_details.designation.name);
                                    $('#employee_id').val(data.employee_details.id);


                                    $('#from_date').attr('min', data.from_date);
                                    $('#from_date').attr('max', data.to_date);
                                    $('#to_date').attr('min', data.from_date);
                                    $('#to_date').attr('max', data.to_date);

                                    $('#status_div').removeAttr('style');

                                } else if (data.result === 0) {
                                    $('#status_div').hide();

                                    alert("Employee details not found or Employee Status already added!");
                                } else {
                                    alert("Unexpected result from the server");
                                }
                            }else {
                                if (data.error2) {
                                    alert(data.error2);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
                else{
                    alert('Please Enter Employee Id && select Month');
                }

            });
    });

    function calculateDates() {
        const fromDate = $('#from_date').val();
        const toDate = $('#to_date').val();

        if (fromDate && toDate) {
            const from = new Date(fromDate);
            const to = new Date(toDate);

            if (to < from) {
                alert('To Date must be the same as or greater than From Date');
                $('#rejoin_date').val('');
                $('#no_of_days').val('');
                return;
            }

            // Calculate the number of days
            const timeDifference = to.getTime() - from.getTime();
            const dayDifference = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1; // Including the start date

            // Calculate the rejoin date (next day of toDate)
            const rejoin = new Date(to);
            rejoin.setDate(rejoin.getDate() + 1);
            const rejoinDate = rejoin.toISOString().split('T')[0];

            // Update the values in the inputs
            $('#rejoin_date').val(rejoinDate);
            $('#no_of_days').val(dayDifference);
        }
    }

    $('#from_date, #to_date').on('change', calculateDates);



    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('add-leave.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('add-leave.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
{{-- <script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('allowance.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {
                    $("#editForm input[name='edit_model_id']").val(data.allowance.id);
                    $("#editForm input[name='allowance']").val(data.allowance.allowance);
                    $("#editForm input[name='amount']").val(data.allowance.amount);
                    $("#editForm input[name='allowance_in_marathi']").val(data.allowance.allowance_in_marathi);
                    $("#edit_type").html(data.typeHtml);
                    $("#edit_calculation").html(data.calculationHtml);

                    if(data.allowance.is_applicable == '1'){
                        $('#is_applicable').prop('checked', true);
                        $('.size-checkbox').val('1');
                    }else{
                        $('#is_applicable').prop('checked', false);
                        $('.size-checkbox').val('0');
                    }

                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script> --}}


<!-- Update -->
{{-- <script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('allowance.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('allowance.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script> --}}


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Leave?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('add-leave.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>

