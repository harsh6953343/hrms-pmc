<x-admin.layout>
    <x-slot name="title">Import Employee Credited Society</x-slot>
    <x-slot name="heading">Import Employee Credited Society</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Import Employee Credited Society</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" action="{{ route('import.salary.store-employee-credited-society') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label" for="month">Select Month <span class="text-danger">*</span></label>
                                <select name="month" id="month" class="form-select" required>
                                    <option value="">Select month</option>
                                    @for($i=1; $i <= 12; $i++)
                                        <option value="{{ $i }}">{{ \Carbon\Carbon::create()->month($i)->format('F') }}</option>
                                    @endfor
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label">Select File <span class="text-danger">*</span></label>
                                <input type="file" name="import" class="form-control" accept=".xlsx, .xls" required>
                                @error('import')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Import</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <button id="addToTable" class="btn btn-primary">Import <i class="fa fa-plus"></i></button>
                                <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="table-responsiv">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Month</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($files as $file)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ \Carbon\Carbon::create()->month($file->month)->format('F') }}</td>
                                    <td>
                                        <a href="{{ asset('storage/'.$file->file) }}" class="btn btn-primary btn-sm">View File</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>
