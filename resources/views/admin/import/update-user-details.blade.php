<x-admin.layout>
    <x-slot name="title">Import Employee Upate Details</x-slot>
    <x-slot name="heading">Import Employee Upate Details</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header d-flex justify-content-between">
                    <h4 class="card-title">Import Employee Upate Details</h4>
                    <a class="btn btn-primary btn-sm" href="{{ asset('admin/user_details.xlsx') }}">Download Sample Excel File</a>
                </header>
                <form class="theme-form" name="addForm" id="addForm" action="{{ route('import.store-employee-update-details') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label">Select File <span class="text-danger">*</span></label>
                                <input type="file" name="import" class="form-control" accept=".xlsx, .xls" required>
                                @error('import')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Import</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>



</x-admin.layout>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>
