<x-admin.layout>
    <x-slot name="title">Supplimentary Leave Bill</x-slot>
    <x-slot name="heading">Supplimentary Leave Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('supplimentary-bill.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-12">
                                <div class="row d-flex justify-content-between">
                                    <div class="col-6">
                                        <form action="{{ route('supplimentary-leave-bill.store-generate') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="mb-3">
                                                        <select name="suplimentry_leave_id" class="form-select" id="geneareBillNo">
                                                            <option value="">--Select--</option>
                                                            @foreach($supplimentryLeaveBills as $supplimentryLeaveBill)
                                                            <option value="{{ $supplimentryLeaveBill->id }}">{{ $supplimentryLeaveBill->bill_no }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <button id="generateLeaveBill" class="btn btn-primary">Generate</a>
                                                </div>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <select name="bill_no" class="form-select" id="createNewBillNo">
                                                        <option value="">--Select--</option>
                                                        @foreach($supplimentryBills as $supplimentryBill)
                                                        <option value="{{ $supplimentryBill->bill_no }}">{{ $supplimentryBill->bill_no }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <button id="createNewLeaveBill" class="btn btn-primary">Create New</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Bill No</th>
                                    <th>Bill Description</th>
                                    <th>Group</th>
                                    <th>Payment Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('supplimentary-leave-bill.index') }}",
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'bill_no',
                            name: 'bill_no',
                            orderable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'bill_description',
                            name: 'bill_description',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'ward',
                            name: 'supplimentaryBill.ward.name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'payment_status',
                            name: 'payment_status',
                            orderable: true,
                            render: function(data, type, row){
                                if(data == "1"){
                                    return "Not Processed";
                                }else if(data == "2"){
                                    return "Processed & Not Paid";
                                }else if(data == "3"){
                                    return "Processed & Paid";
                                }else{
                                    return "-";
                                }
                            },
                        },
                        {
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            render: function(data, type, row){
                                let editUrl = "{{ route('supplimentary-leave-bill.edit', ':id') }}";
                                let viewUrl = "{{ route('supplimentary-leave-bill.show', ':id') }}";
                                let html = `
                                    <a href="${viewUrl.replace(':id', data)}" class="btn btn-primary px-2 py-1" title="View">
                                        View
                                    </a>
                                    <a href="${editUrl.replace(':id', data)}" class="btn btn-warning px-2 py-1" title="Edit">
                                        Edit
                                    </a>`;
                                return html;
                            },
                        },
                    ]
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                $('#createNewLeaveBill').click(function(){
                    let createNewBillNo = $('#createNewBillNo').val();

                    if(createNewBillNo != ""){
                        window.location.href = "{{ route('supplimentary-leave-bill.create') }}?billno="+ createNewBillNo
                    }else{
                        alert('Please select create new bill no');
                    }
                });

                $('#generateLeaveBill').click(function(){
                    let generateBillNo = $('#geneareBillNo').val();

                    if(generateBillNo != ""){
                        window.location.href = "{{ route('supplimentary-leave-bill.create') }}?billno="+ generateBillNo
                    }else{
                        alert('Please select generate bill no');
                    }

                });
            });
        </script>
    @endpush


</x-admin.layout>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>


