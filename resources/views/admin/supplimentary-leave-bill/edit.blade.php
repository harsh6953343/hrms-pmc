<x-admin.layout>
    <x-slot name="title">Supplimentary Leave Update Bill</x-slot>
    <x-slot name="heading">Supplimentary Leave Update Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <form method="post" id="editForm" method="post">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <input type="hidden" name="supplimentary_bill_id" id="supplimentary_bill_id" value="{{ $suplimentry->supplimentary_bill_id }}">
                        <input type="hidden" name="supplimentary_leave_id" id="supplimentary_leave_id" value="{{ $suplimentry->id }}">
                        <div class="row">
                            <div class="col-3 mb-3">
                                <label for="">Bill No</label>
                                <input type="text" class="form-control" name="bill_no" value="{{ $suplimentry->bill_no }}" readonly>
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Bill Date</label>
                                <input type="text" class="form-control" name="bill_date" value="{{ \Carbon\Carbon::parse($suplimentry->bill_date)->format('Y-m-d') }}" readonly>
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Bill Status</label>
                                <input type="hidden" value="{{ $suplimentry->payment_status }}" name="payment_status" readonly>
                                <input type="text" class="form-control"  value="@if($suplimentry->payment_status == "1")Not Processed @elseif($suplimentry->payment_status == "2")Processed & Not Paid @elseif($suplimentry->payment_status == "3") Processed & Paid @else - @endif" readonly>
                            </div>
                            
                            <div class="col-6 mb-3">
                                <label for="">Bill Description</label>
                                <input type="text" class="form-control" value="{{ $suplimentry->bill_description }}" name="bill_description" readonly>
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Cheque No</label>
                                <input type="text" class="form-control" value="{{ $suplimentry->cheque_no }}" name="cheque_no">
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Cheque Date</label>
                                <input type="text" class="form-control datepicker" value="{{ \Carbon\Carbon::parse($suplimentry->cheque_date)->format('d-m-Y') }}" name="cheque_date" readonly>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" id="editSubmit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-end my-3">
                                <button type="button" class="btn btn-primary btn-sm addMore">Add More</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Month</th>
                                        <th>PR</th>
                                        <th>UL</th>
                                        <th>ML</th>
                                        <th>EL</th>
                                        <th>PR</th>
                                        <th>UL</th>
                                        <th>ML</th>
                                        <th>EL</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="addMoreRow">
                                    @foreach($supplimentaryLeaveBills as $key => $supplimentaryLeaveBill)
                                    <tr id="row{{ $key }}">
                                        <th>
                                            <select name="employee_id[]" class="employeeId">
                                                <option value="">--select--</option>
                                                @foreach($supplimentaryBills as $supplimentaryBill)
                                                <option value="{{ $supplimentaryBill->employee_id }}" {{ ($supplimentaryLeaveBill->employee_id == $supplimentaryBill->employee_id) ? 'selected' : '' }}>{{ $supplimentaryBill?->employee?->fname.' '. $supplimentaryBill?->employee?->mname. ' '. $supplimentaryBill?->employee?->lname }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <input type="month" name="month[]" value="{{ $supplimentaryLeaveBill->month }}" max="{{ date('Y-m') }}" class="selectMonth">
                                        </th>
                                        <th><input type="number" step="0.01" min="0" placeholder="0.00" name="pr[]" value="{{ $supplimentaryLeaveBill->attandance_pr }}" style="width:70px" /></th>
                                        <th><input type="number" step="0.01" min="0" class="ulInput" placeholder="0.00" name="ul[]" value="{{ $supplimentaryLeaveBill->attandance_ul }}" style="width:70px" /></th>
                                        <th>
                                            <input type="number" step="0.01" min="0" placeholder="0.00" name="ml[]" value="{{ $supplimentaryLeaveBill->attandance_ml }}" style="width:70px" />
                                        </th>
                                        <th><input type="number" step="0.01" min="0" placeholder="0.00" name="el[]" value="{{ $supplimentaryLeaveBill->attandance_el }}" style="width:70px" /></th>
                                        <th><input type="number" step="0.01" min="0" placeholder="0.00" name="pr1[]" class="pr1Input" value="{{ $supplimentaryLeaveBill->supplimentry_pr }}" style="width:70px" /></th>
                                        <th><input type="number" step="0.01" min="0" placeholder="0.00" name="ul1[]" class="ul1Input" value="{{ $supplimentaryLeaveBill->supplimentry_ul }}" style="width:70px" /></th>
                                        <th><input type="number" value="{{ $supplimentaryLeaveBill->supplimentry_ml }}" step="0.01" min="0" placeholder="0.00" class="ml1Input" name="ml1[]" style="width:70px" /></th>
                                        <th><input type="number" step="0.01" min="0" placeholder="0.00" name="el1[]" class="el1Input" value="{{ $supplimentaryLeaveBill->supplimentry_el }}" style="width:70px" /></th>
                                        <th><button class="btn btn-danger btn-sm removeRow" data-id="{{ $key }}">Remove</button></th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</x-admin.layout>

<script>
    $(document).ready(function(){
        var count = 200;
        $('.addMore').click(function(){
            let html = `<tr id="row${count}">
                <th>
                    <select name="employee_id[]" class="employeeId">
                        <option value="">--select--</option>
                        @foreach($supplimentaryBills as $supplimentaryBill)
                        <option value="{{ $supplimentaryBill->employee_id }}">{{ $supplimentaryBill?->employee?->fname.' '. $supplimentaryBill?->employee?->mname. ' '. $supplimentaryBill?->employee?->lname }}</option>
                        @endforeach
                    </select>
                </th>
                <th>
                    <input type="month" name="month[]" max="{{ date('Y-m') }}" class="selectMonth">
                </th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" name="pr[]" style="width:70px" /></th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" class="ulInput" name="ul[]" style="width:70px" /></th>
                <th>
                    <input type="number" step="0.01" min="0" placeholder="0.00" name="ml[]" style="width:70px" />
                </th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" name="el[]" style="width:70px" /></th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" class="pr1Input" name="pr1[]" style="width:70px" /></th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" class="ul1Input" name="ul1[]" style="width:70px" /></th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" class="ml1Input" name="ml1[]" style="width:70px" /></th>
                <th><input type="number" step="0.01" min="0" placeholder="0.00" class="el1Input" name="el1[]" style="width:70px" /></th>
                <th>
                    <button class="btn btn-danger btn-sm removeRow" data-id="${count}">Remove</button>
                </th>
            </tr>`;
            count = count + 1;

            $('#addMoreRow').append(html);
        });

        $('body').on('click', '.removeRow', function(){
            let id = $(this).attr('data-id');
            $('#row'+id).remove();
        });

        $('body').on('change', '.selectMonth, .employeeId', function(){
            let month = $(this).closest('tr').find('.selectMonth').val();
            let employeeId = $(this).closest('tr').find('.employeeId :selected').val();
            let row = $(this).closest('tr').attr('id');

            if(month != "" && employeeId != ""){
                $.ajax({
                    url: "{{ route('supplimentary-leave-bill.fetch-update-details') }}",
                    type: 'GET',
                    data: {
                        'month': month,
                        'employeeId': employeeId,
                        'row': row,
                        'suplimentry_leave_id': "{{ $suplimentry->id }}"
                    },
                    success: function(data) {
                        if(data.attendance){
                            let absentDays = parseFloat(data.attendance?.total_absent_days || 0) - parseFloat(data.suplimentryEmployeeLeaveBill || 0);
                            $('body').find('#'+data.row).find("input[name='pr[]']").val(data.attendance?.total_present_days);
                            $('body').find('#'+data.row).find("input[name='ul[]']").val(absentDays);
                            $('body').find('#'+data.row).find("input[name='ml[]']").val(data.earnLeave);
                            $('body').find('#'+data.row).find("input[name='el[]']").val(data.sickLeave);
                        }
                    },
                    statusCode: {
                        422: function(responseObject, textStatus, jqXHR) {
                            $("#editSubmit").prop('disabled', false);
                            resetErrors();
                            printErrMsg(responseObject.responseJSON.errors);
                        },
                        500: function(responseObject, textStatus, errorThrown) {
                            $("#editSubmit").prop('disabled', false);
                            swal("Error occured!", "Something went wrong please try again", "error");
                        }
                    }
                });
            }
        });
    });
</script>


<script>
    $("#editForm").submit(function(e) {
        e.preventDefault();
        $("#editSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        formdata.append('_method', 'PUT');
        var model_id = $('#supplimentary_leave_id').val();
        var url = "{{ route('supplimentary-leave-bill.update', ':model_id') }}";
        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#editSubmit").prop('disabled', false);
                if (!data.error)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('supplimentary-leave-bill.index') }}';
                    });
                else
                    swal("Error!", data.error, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#editSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#editSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>

<script>
    $(document).ready(function(){
        $('body').on('keyup', '.pr1Input, .ul1Input, .el1Input, .ml1Input', function(){
            let row = $(this).closest('tr');
            let ul = parseFloat(row.find('.ulInput').val()) || 0;
            let pr1 = parseFloat(row.find('.pr1Input').val()) || 0;
            let ul1 = parseFloat(row.find('.ul1Input').val()) || 0;
            let el1 = parseFloat(row.find('.el1Input').val()) || 0;
            let ml1 = parseFloat(row.find('.ml1Input').val()) || 0;

            let total = pr1 + ul1 + el1 + ml1;

            if(ul < total){
                $(this).val(0)
            }
        });
    });
</script>