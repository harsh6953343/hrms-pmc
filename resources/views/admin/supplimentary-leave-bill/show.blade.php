<x-admin.layout>
    <x-slot name="title">Supplimentary Leave Update Bill</x-slot>
    <x-slot name="heading">Supplimentary Leave Update Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Bill No</th>
                                        <td>{{ $suplimentry->bill_no }}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Bill Date</th>
                                        <td>{{ \Carbon\Carbon::parse($suplimentry->bill_date)->format('Y-m-d') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bill Status</th>
                                        <td>
                                            @if($suplimentry->payment_status == "1")
                                                Not Processed 
                                            @elseif($suplimentry->payment_status == "2")
                                                Processed & Not Paid 
                                            @elseif($suplimentry->payment_status == "3") 
                                                Processed & Paid 
                                            @else 
                                                - 
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Bill Description</th>
                                        <td>{{ $suplimentry->bill_description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cheque No</th>
                                        <td>{{ $suplimentry->cheque_no }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cheque Date</th>
                                        <td>{{ \Carbon\Carbon::parse($suplimentry->cheque_date)->format('d-m-Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h2 class="card-title">Supplimentary Leave Employee</h2>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Month</th>
                                    <th>PR</th>
                                    <th>UL</th>
                                    <th>ML</th>
                                    <th>EL</th>
                                    <th>PR</th>
                                    <th>UL</th>
                                    <th>ML</th>
                                    <th>EL</th>
                                </tr>
                            </thead>
                            <tbody id="addMoreRow">
                                @foreach($supplimentaryLeaveBills as $supplimentaryLeaveBill)
                                <tr>
                                    <td>{{ $supplimentaryLeaveBill?->employee?->fname.' '.$supplimentaryLeaveBill?->employee?->mname.' '.$supplimentaryLeaveBill?->employee?->lname }}</td>
                                    <td>{{ $supplimentaryLeaveBill->month }}</td>
                                    <td>{{ $supplimentaryLeaveBill->attandance_pr ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->attandance_ul ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->attandance_ml ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->attandance_el ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->supplimentry_pr ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->supplimentry_ul ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->supplimentry_ml ?? 0 }}</td>
                                    <td>{{ $supplimentaryLeaveBill->supplimentry_el ?? 0 }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>
