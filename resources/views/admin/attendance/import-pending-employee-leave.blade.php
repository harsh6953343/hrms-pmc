<x-admin.layout>
    <x-slot name="title">Import Pending Employee Leave</x-slot>
    <x-slot name="heading">Import Pending Employee Leave</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Import Pending Employee Leave</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" action="{{ route('leave-approval.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" name="month" id="month" required>
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-none">
                                <label class="col-form-label" for="period">Period<span class="text-danger">*</span></label>
                                <select class="form-select" id="period" name="period">
                                    <option value="1"  >01-20</option>
                                    <option value="2" >21-31</option>
                                </select>
                                <span class="text-danger invalid period_err"></span>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                <label class="col-form-label" for="leave_files">Select File<span class="text-danger">*</span></label>
                                <input class="form-control" id="leave_files" name="leave_files" type="file" accept=".xls,.xlsx" required>
                                @error('leave_files')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                <div>&nbsp;</div>
                                <button class="btn btn-success btn-md mt-3">Import</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <button id="addToTable" class="btn btn-primary">Import <i class="fa fa-plus"></i></button>
                                <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="table-responsiv">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Month</th>
                                    {{-- <th>Period</th> --}}
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($importLeaves as $importLeave)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ \Carbon\Carbon::create()->month($importLeave->month)->format('F') }}</td>
                                    {{-- <td>
                                        @if($importLeave->period == "1")
                                        01-20
                                        @else
                                        21-31
                                        @endif
                                    </td> --}}
                                    <td>
                                        <a href="{{ asset('storage/'.$importLeave->excel_file) }}" class="btn btn-primary btn-sm">View File</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>
