<x-admin.layout>
    <x-slot name="title">Employees</x-slot>
    <x-slot name="heading">Employees</x-slot>

    @push('styles')
        <style>
            .size-checkbox{
                width: 1.7rem;
                height: 1.7rem;
            }
        </style>
    @endpush

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Add Employee</h4>
                </div>
                <!-- end card header -->
                <div class="card-body">
                    <form class="form-steps" autocomplete="off" id="addForm">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <th>Employee Name</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                </thead>
                            </table>
                        </div>
                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>

</x-admin.layout>

