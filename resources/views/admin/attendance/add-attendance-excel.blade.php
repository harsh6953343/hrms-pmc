<x-admin.layout>
    <x-slot name="title">Add Attendance Excel</x-slot>
    <x-slot name="heading">Add Attendance Excel</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Attendance Excel</h4>
                </header>
                <form class="form-horizontal form-bordered" method="post" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="mb-3 row">

                            <div class="col-md-4 mt-2">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1"  >January</option>
                                    <option value="2" >February</option>
                                    <option value="3" >March</option>
                                    <option value="4" >April</option>
                                    <option value="5" >May</option>
                                    <option value="6" >June</option>
                                    <option value="7" >July</option>
                                    <option value="8" >August</option>
                                    <option value="9" >September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>

                            </div>

                            <div class="col-md-4 mt-2">
                                <label class="col-form-label" for="period">Period<span class="text-danger">*</span></label>
                                <select class="form-select" id="period" name="period">
                                    <option value="">Select Period</option>
                                    <option value="1"  >01-20</option>
                                    <option value="2" >21-31</option>
                                </select>
                                <span class="text-danger invalid period_err"></span>
                            </div>

                            <div class="col-md-4 mt-2">
                                <label class="col-form-label" for="file">Choose File<span class="text-danger">*</span></label>
                                <input type="file" name="file" id="file" class="form-control">
                                <span class="text-danger invalid file_err"></span>

                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-admin.layout>


<script>
      $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('add-attendance-excel.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('add-attendance-excel.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>
