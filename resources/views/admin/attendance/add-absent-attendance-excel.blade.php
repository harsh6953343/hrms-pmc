<x-admin.layout>
    <x-slot name="title">Add Absent Attendance Excel</x-slot>
    <x-slot name="heading">Add Absent Attendance Excel</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Absent Attendance Excel</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" action="{{ route('add-attendance-excel.absent') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-4 mt-2">
                                <label class="col-form-label" for="ward">Group<span class="text-danger">*</span></label>
                                <select class="form-select" id="ward" name="ward" required>
                                    <option value="">Select Group</option>
                                    <option value="All">MUNICIPAL CORPORATION</option>
                                    @foreach($wards as $ward)
                                    <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                @error('ward')
                                <span class="text-danger invalid">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-2">
                                <label class="col-form-label" for="month">Select Month <span class="text-danger">*</span></label>
                                <select name="month" id="month" class="form-select" required>
                                    <option value="">Select month</option>
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-md-4 d-none">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly>
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4 d-none">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly>
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                            <div class="col-md-4 mt-2">
                                <label class="col-form-label" for="period">Period<span class="text-danger">*</span></label>
                                <select class="form-select" id="period" name="period" required>
                                    <option value="">Select Period</option>
                                    <option value="1">01-20</option>
                                    <option value="2">21-31</option>
                                </select>
                                @error('period')
                                <span class="text-danger invalid">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label">Select File 1<span class="text-danger">*</span></label>
                                <input type="file" name="file" class="form-control" accept=".xlsx, .xls" required>
                                @error('file')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label">Select File 2</label>
                                <input type="file" name="file2" class="form-control" accept=".xlsx, .xls" id="selectFile2">
                                @error('file2')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to import the absent excel file.')" id="addSubmit">Import</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>

                </form>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <button id="addToTable" class="btn btn-primary">Import <i class="fa fa-plus"></i></button>
                                <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsiv">
                        <table class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Month</th>
                                    <th>Period</th>
                                    <th>File</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($absentAttendances as $absentAttendance)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ \Carbon\Carbon::create()->month($absentAttendance->month)->format('F') }}</td>
                                    <td>
                                        @if($absentAttendance->period == "1")
                                        01-20
                                        @else
                                        21-31
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ asset('storage/'.$absentAttendance->excel_file) }}" class="btn btn-primary btn-sm">View File</a>
                                    </td>
                                    {{-- <td>
                                        <form action="{{ route('add-attendance-excel.destroy', $absentAttendance->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button onclick="return confirm('Are you sure you want to delete this excel file')" class="btn btn-danger rem-element px-2 py-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                            </button>
                                        </form>
                                    </td> --}}
                                </tr>
                                @empty
                                    <tr align="center">
                                        <th colspan="5">No Data Found</th>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>
<script>
    $('#ward').change(function(){
        let ward = $(this).val();
        if(ward == "All"){
            $('#selectFile2').prop('required', true);
        }else{
            $('#selectFile2').prop('required', false);
        }
    });

    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
</script>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>
