@php
    use App\Models\Allowance;
    use App\Models\Deduction;
    use App\Models\EmployeeMonthlyLoan;
    use Carbon\Carbon;

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supplimentary Bill - {{ $employee_details->fname." ".$employee_details->mname." ".$employee_details->lname }}</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        /* body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        } */
        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 45px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 10px 0 5px;
            font-size: 24px;
        }
        .subsection {
            width: 50%;
            float: left;
        }

        .subsection-details {
            width: 48%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border-collapse: collapse; /* Collapse borders */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            border: 1px solid black; /* Single border */
            text-align: left;
            /* background-color: lightgray; */
        }
        .earnings-header, .deductions-header {
            background-color: #355495; /* Blue background color */
            color: #ffffff; /* White text color */
        }
        .dashed-hr {
            border: 1px dashed black;
            clear: both;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="110">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading" style="margin-left:10%;">{{ $corporation->name }}</h2>
                    <h5 class="section-heading" style="font-size: 18px; margin-left:10%;">Supplimentary Bill
                        for the month of
                        @php
                            $monthNames = collect(explode(',', $supplimentory_bill->month))
                                                            ->map(function ($monthNumber) {
                                                                return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                                            })
                                                            ->sort()
                                                            ->implode(', ');
                            echo $monthNames.",".$supplimentory_bill->financialYear->title;
                        @endphp
                    </h5>
                </td>
            </tr>
        </thead>
    </table>

    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">Employee Name</span><strong>: {{ $employee_details->fname." ".$employee_details->mname." ".$employee_details->lname }}</strong></p>
            <p><span class="label">Ward</span><strong>: {{ $employee_details->ward->name }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details->designation->name }}</strong></p>
            <p><span class="label">Date of Retirement</span><strong>: {{ $employee_details->retirement_date }}</strong></p>



        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Employee Code</span><strong>: {{ $supplimentory_bill->Emp_Code }}</strong></p>
            <p><span class="label">Department</span><strong>: {{ $employee_details->department->name }}</strong></p>
            <p><span class="label">Date of Appointment</span><strong>: {{ $employee_details->doj }}</strong></p>
            <p><span class="label">Bank A/C No.</span><strong>: {{ $employee_details->account_no }}</strong></p>

        </div>
    </div>

    <hr class="dashed-hr" style="margin-bottom: 3%;">

    @php
        $total_basic_salary = 0;
        $grand_total_earn = 0;

        $allowanceTotals = [];
        $deductionTotals = [];
        $loanTotals = [];

        $grand_total_bank_loan = 0;
        $total_stamp_duty = 0;
        $total_deductions = 0;
        $total_net_salary = 0;
        $present_days = 0;

    @endphp

    @foreach ($freezeAttendance as $freeze)

        @php
            $explode_allowance_ids = explode(',', $freeze->allowance_Id);
            $explode_allowance_type = explode(',', $freeze->allowance_Type);
            $explode_allowance_amt = explode(',', $freeze->allowance_Amt);

            $explode_deduction_ids = explode(',', $freeze->deduction_Id);
            $explode_deduction_amt = explode(',', $freeze->deduction_Amt);
            $explode_deduction_type = explode(',', $freeze->deduction_Type);

            $explode_loan_ids = explode(',', $freeze->loan_deduction_id);

            $total_basic_salary += $freeze->basic_salary;
            $grand_total_earn   += ($freeze->basic_salary + $freeze->total_allowance);

            // $grand_total_bank_loan+= $bank_loan;
            $total_stamp_duty+= $freeze->stamp_duty;

            $total_deductions+=$freeze->total_deduction;

            $total_net_salary+= $freeze->net_salary;

            if($freeze->present_day == 0){
                $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                $numberOfDaysInMonth += 1;
                $present_days+= $numberOfDaysInMonth;
            }
            $present_days += $freeze->present_day;

        @endphp

        {{-- Allowance --}}
        @foreach ($allowances->chunk(5, true) as $chunk)
            @foreach ($chunk as $allowance)
                @php
                    $index = array_search($allowance->id, $explode_allowance_ids);
                @endphp
                @if($index !== false)
                    @php
                    if (array_key_exists($allowance->id, $allowanceTotals)) {
                        // If it exists, add the deduction amount to the existing total
                        $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                    } else {
                        // If it doesn't exist, initialize the total with the deduction amount
                        $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                    }
                    @endphp
                @endif
            @endforeach
        @endforeach

        {{-- Deductions --}}
        @foreach ($deductions->chunk(5, true) as $chunk)
            @foreach ($chunk as $deduction)
                @php
                    $index = array_search($deduction->id, $explode_deduction_ids);
                @endphp
                @if($index !== false)
                    @php
                    if (array_key_exists($deduction->id, $deductionTotals)) {
                        $deductionTotals[$deduction->id] += $explode_deduction_amt[$index];
                    } else {
                        $deductionTotals[$deduction->id] = $explode_deduction_amt[$index];
                    }
                    @endphp
                @endif
            @endforeach
        @endforeach

        {{-- Loan --}}

        @if ($freeze->loan_deduction_id)
            @foreach ($explode_loan_ids as $loan_id)
                @php
                    $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();
                    if ($emp_loan) {
                        $loan_name = $emp_loan->loan->loan;
                        $loan_amount = $emp_loan->installment_amount;

                        // Store loan name and amount
                        if (array_key_exists($loan_name, $loanTotals)) {
                            $loanTotals[$loan_name] += $loan_amount;
                        } else {
                            $loanTotals[$loan_name] = $loan_amount;
                        }
                    }
                @endphp
            @endforeach
        @endif


{{-- End Main for each loop --}}
    @endforeach


    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="earnings-header">EARNINGS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BASIC SALARY</td>
                            <td><b>{{ $total_basic_salary }}</b></td>
                        </tr>
                        {{-- Allowance --}}
                        @foreach($allowances as $key => $allowance)
                            @if(isset($allowanceTotals[$allowance->id]))
                            <tr>
                                <td>{{ $allowance?->allowance }}</td>
                                <td><b>{{ $allowanceTotals[$allowance->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach
                        {{-- Count --}}
                        @php

                            $deduction_rows =  count($deductionTotals) + 1; //including stamp duty
                            if ($loanTotals)
                                $deduction_rows += count($loanTotals);
                        @endphp
                        @if($deduction_rows > count($allowanceTotals) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($allowanceTotals) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

        <div class="subsection" style="width: 45%;">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Deductions --}}
                        @foreach($deductions as $key => $deduction)
                            @if(isset($deductionTotals[$deduction->id]))
                            <tr>
                                <td>{{ $deduction?->deduction }}</td>
                                <td><b>{{ $deductionTotals[$deduction->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach

                        {{-- Employee Loan --}}
                        @foreach($loanTotals as $loan_name => $loan_total)
                            <tr>
                                <td>{{ $loan_name }}</td>
                                <td><b>{{ $loan_total }}</b></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>STAMP DUTY</td>
                            <td><b>{{ $total_stamp_duty }}</b></td>
                        </tr>

                        @if(count($allowanceTotals) + 1 > $deduction_rows)
                        @php
                            $no_of_rows_deduction = count($allowanceTotals) + 1 - $deduction_rows;
                        @endphp
                        @for($i = 0; $i < $no_of_rows_deduction; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <p class="check">


    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <table>
                <tr>
                    <td><b>TOTAL EARNINGS</b></td>
                    <td><b>{{ $grand_total_earn }}</b></td>
                </tr>
            </table>
        </div>
        <div class="subsection" style="width: 45%;">
            <table>
                <tr>
                    <td><b>TOTAL DEDUCTIONS</b></td>
                    <td><b>{{ $total_deductions }}</b></td>
                </tr>
                <tr>
                    <td><b>NET SALARY</b></td>
                    <td ><b>{{ $total_net_salary }}</b></td>
                </tr>
            </table>
        </div>
    </div>


    <hr class="dashed-hr" style="margin-top: 12%;">
    <div style="margin-left:25px;">
    <p><b>Present Days: {{ $present_days }}</b></p>
    <p><b>Remark:</b> </p>
    <p><b>This is a computer-generated slip. It does not require any authorized sign.</b></p>
    <p><b>Date: {{ date('d-m-Y') }} <span style="margin-left:60%;">Establishment Clerk</span></b></p>
    <p><b>Generated & Download by: {{ Auth::user()->name }}</b></p>
    </div>

</body>
</html>

