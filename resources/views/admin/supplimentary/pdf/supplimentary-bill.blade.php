@php
    use App\Models\Allowance;
    use App\Models\Deduction;
    use App\Models\EmployeeMonthlyLoan;
    use Carbon\Carbon;

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supplimentary Bill - {{ $employee_details->fname." ".$employee_details->mname." ".$employee_details->lname }}</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        /* body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        } */
        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 45px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 10px 0 5px;
            font-size: 24px;
        }
        .subsection {
            width: 50%;
            float: left;
        }

        .subsection-details {
            width: 48%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border-collapse: collapse; /* Collapse borders */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            border: 1px solid black; /* Single border */
            text-align: left;
            /* background-color: lightgray; */
        }
        .earnings-header, .deductions-header {
            background-color: #355495; /* Blue background color */
            color: #ffffff; /* White text color */
        }
        .dashed-hr {
            border: 1px dashed black;
            clear: both;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="110">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading" style="margin-left:10%;">{{ $corporation->name }}</h2>
                    <h5 class="section-heading" style="font-size: 18px; margin-left:10%;">Supplimentary Bill
                        for the month of
                        @php
                            $monthNames = collect(explode(',', $supplimentory_bill->month))
                                                            ->map(function ($monthNumber) {
                                                                return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                                            })
                                                            ->sort()
                                                            ->implode(', ');
                            echo $monthNames.",".$supplimentory_bill->financialYear->title;
                        @endphp
                    </h5>
                </td>
            </tr>
        </thead>
    </table>

    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">Employee Name</span><strong>: {{ $employee_details->fname." ".$employee_details->mname." ".$employee_details->lname }}</strong></p>
            <p><span class="label">Ward</span><strong>: {{ $employee_details->ward->name }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details->designation->name }}</strong></p>
            <p><span class="label">Date of Retirement</span><strong>: {{ $employee_details->retirement_date }}</strong></p>



        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Employee Code</span><strong>: {{ $supplimentory_bill->Emp_Code }}</strong></p>
            <p><span class="label">Department</span><strong>: {{ $employee_details->department->name }}</strong></p>
            <p><span class="label">Date of Appointment</span><strong>: {{ $employee_details->doj }}</strong></p>
            <p><span class="label">Bank A/C No.</span><strong>: {{ $employee_details->account_no }}</strong></p>

        </div>
    </div>

    <hr class="dashed-hr" style="margin-bottom: 3%;">

    @php
        $grand_total_earn = 0;
        $total_deductions = 0;
        $explode_allowance_ids = explode(',', $supplimentory_bill->allowance_id);
        $explode_allowance_amt = explode(',', $supplimentory_bill->allowance_amount);
        $allopwanceKeyValue = array_combine( $explode_allowance_ids, $explode_allowance_amt );

        $explode_deduction_ids = explode(',', $supplimentory_bill->deduction_id);
        $explode_deduction_amt = explode(',', $supplimentory_bill->deduction_amount);
        $deductionKeyValue = array_combine( $explode_deduction_ids, $explode_deduction_amt );

    @endphp



    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="earnings-header">EARNINGS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BASIC SALARY</td>
                            <td><b>{{ round($supplimentory_bill->basic_salary) }}</b></td>
                            @php $grand_total_earn = $grand_total_earn + $supplimentory_bill->basic_salary; @endphp
                        </tr>
                        {{-- Allowance --}}
                        @foreach($allowances as $key => $allowance)
                            @if(isset($allopwanceKeyValue[$allowance->id]))
                            <tr>
                                <td>{{ $allowance?->allowance }}</td>
                                <td><b>{{ $allopwanceKeyValue[$allowance->id] }}</b></td>
                            </tr>
                            @php 
                            $grand_total_earn = $grand_total_earn + $explode_allowance_amt[$key]; 
                            @endphp
                            @endif
                        @endforeach

                        @if($supplimentory_bill->allowance_dcp_corporation > 0)
                        <tr>
                            <td>DCPS | CORPORATION CONTRIBUTION ALLOWANCE</td>
                            <td><b>{{ round($supplimentory_bill->allowance_dcp_corporation) }}</b></td>
                        </tr>
                        @php 
                            $grand_total_earn = $grand_total_earn + $supplimentory_bill->allowance_dcp_corporation; 
                            @endphp
                        @endif
                        {{-- Count --}}
                        @php
                            $deduction_rows =  count($explode_deduction_ids);
                        @endphp
                        @if($deduction_rows > count($explode_allowance_ids) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($explode_allowance_ids) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

        <div class="subsection" style="width: 45%;">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Deductions --}}
                        @foreach($deductions as $key => $deduction)
                            @if(isset($deductionKeyValue[$deduction->id]))
                            <tr>
                                <td>{{ $deduction?->deduction }}</td>
                                {{-- <td><b>{{ $deductionKeyValue[$deduction->id] }}</b></td> --}}
                                <td><b>0</b></td>
                            </tr>
                            {{-- @php $total_deductions = $total_deductions + $explode_deduction_amt[$key]; @endphp --}}
                            @endif
                        @endforeach

                        @if($supplimentory_bill->deduction_dcp_employee > 0)
                        <tr>
                            <td>DCPS | EMPLOYEE CONTRIBUTION</td>
                            <td><b>{{ round($supplimentory_bill->deduction_dcp_employee) }}</b></td>
                        </tr>
                        @php 
                        $total_deductions = $total_deductions + $supplimentory_bill->deduction_dcp_employee;
                        @endphp
                        @endif

                        @if($supplimentory_bill->deduction_dcp_corporation > 0)
                        <tr>
                            <td>DCPS | CORPORATION CONTRIBUTION DEDUCTION</td>
                            <td><b>{{ round($supplimentory_bill->deduction_dcp_corporation) }}</b></td>
                        </tr>
                        @php 
                        $total_deductions = $total_deductions + $supplimentory_bill->deduction_dcp_corporation;
                        @endphp
                        @endif



                        @php
                            $deduction_rows =  count($explode_allowance_ids);
                        @endphp
                        @if($deduction_rows > count($explode_allowance_ids) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($explode_allowance_ids) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <p class="check">


    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <table>
                <tr>
                    <td><b>TOTAL EARNINGS</b></td>
                    <td><b>{{ round($grand_total_earn) }}</b></td>
                </tr>
            </table>
        </div>
        <div class="subsection" style="width: 45%;">
            <table>
                <tr>
                    <td><b>TOTAL DEDUCTIONS</b></td>
                    <td><b>{{ round($total_deductions) }}</b></td>
                </tr>
                <tr>
                    <td><b>NET SALARY</b></td>
                    <td ><b>{{ round($grand_total_earn + $total_deductions) }}</b></td>
                </tr>
            </table>
        </div>
    </div>


    <hr class="dashed-hr" style="margin-top: 12%;">
    <div style="margin-left:25px;">
    <p><b>Present Days: {{ $supplimentory_bill->no_of_days }}</b></p>
    <p><b>Remark:</b> </p>
    <p><b>This is a computer-generated slip. It does not require any authorized sign.</b></p>
    <p><b>Date: {{ date('d-m-Y') }} <span style="margin-left:60%;">Establishment Clerk</span></b></p>
    <p><b>Generated & Download by: {{ Auth::user()->name }}</b></p>
    </div>

</body>
</html>

