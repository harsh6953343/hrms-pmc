<x-admin.layout>
    <x-slot name="title">Supplimentary Bill</x-slot>
    <x-slot name="heading">Supplimentary Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('supplimentary-bill.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    @if($type == 1)
                                        <a href="{{ route('supplimentary-bill.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                                    @else
                                        <a href="{{ route('suspend-supplimentary-bill.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Bill No</th>
                                    <th>Bill Description</th>
                                    <th>Group</th>
                                    <th>Cheque Date</th>
                                    <th>Payment Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('supplimentary-bill.index') }}",
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'bill_no',
                            name: 'supplimentary_bills.bill_no',
                            orderable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'bill_description',
                            name: 'bill_description',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'ward',
                            name: 'ward.name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'cheque_date',
                            name: 'cheque_date',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'payment_status',
                            name: 'payment_status',
                            orderable: true,
                            render: function(data, type, row){
                                if(data == "1"){
                                    return "Not Processed";
                                }else if(data == "2"){
                                    return "Processed & Not Paid";
                                }else if(data == "3"){
                                    return "Processed & Paid";
                                }else{
                                    return "-";
                                }
                            },
                        },
                        {
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            render: function(data, type, row){
                                let html = `
                                    <button class="edit-element btn btn-primary px-2 py-1" title="Edit category" data-id="${data}">
                                        Edit
                                    </button>
                                    
                                    <button class="btn btn-danger rem-element px-2 py-1" title="Delete category" data-id="${data}">
                                        Delete
                                    </button>`;
                                return html;
                            },
                        },
                    ]
                });
            })
        </script>
        @endpush


</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Supplimentary Bill?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('supplimentary-bill.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>

