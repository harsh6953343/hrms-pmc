<x-admin.layout>
    <x-slot name="title">Pay Scale</x-slot>
    <x-slot name="heading">Pay Scale</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Pay Scale</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_mst_id">Select Pay<span class="text-danger">*</span></label>
                                <select class="form-select" name="pay_mst_id">
                                    <option value="">Select Pay</option>
                                    @foreach ($payNames as $payName)
                                        <option value="{{ $payName->id }}">{{ $payName->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid pay_mst_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="level">Level <span class="text-danger">*</span></label>
                                <input class="form-control" id="level" name="level" type="number" placeholder="Enter Level">
                                <span class="text-danger invalid level_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="grade_amp">Grade <span class="text-danger">*</span></label>
                                <input class="form-control" id="grade_amp" name="grade_amp" type="number" placeholder="Enter Grade">
                                <span class="text-danger invalid grade_amp_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="grade_pay_name">Grade Pay Name <span class="text-danger">*</span></label>
                                <input class="form-control" id="grade_pay_name" name="grade_pay_name" type="text" placeholder="Enter Grade Pay Name">
                                <span class="text-danger invalid grade_pay_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_band">Pay Band <span class="text-danger">*</span></label>
                                <input class="form-control" id="pay_band" name="pay_band" type="text" placeholder="Enter Pay Band Name">
                                <span class="text-danger invalid pay_band_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_band_scale">Pay Band Scale<span class="text-danger">*</span></label>
                                <input class="form-control" id="pay_band_scale" name="pay_band_scale" type="text" placeholder="Enter Pay Band Name">
                                <span class="text-danger invalid pay_band_scale_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="amount">Amount <span class="text-danger">*</span></label>
                                <input class="form-control" id="amount" name="amount" type="number" placeholder="Enter Amount">
                                <span class="text-danger invalid amount_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('pay_scale.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Pay</th>
                                    <th>Level</th>
                                    <th>Grade</th>
                                    <th>Grade Pay Name</th>
                                    <th>Pay Band</th>
                                    <th>Pay Band Scale</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($payScales as $payScale)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $payScale?->pay?->name }}</td>
                                        <td>{{ $payScale?->level }}</td>
                                        <td>{{ $payScale?->grade_amp }}</td>
                                        <td>{{ $payScale?->grade_pay_name }}</td>
                                        <td>{{ $payScale?->pay_band }}</td>
                                        <td>{{ $payScale?->pay_band_scale }}</td>
                                        <td>{{ $payScale?->amount }}</td>
                                        <td>
                                            @can('pay_scale.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Pay Scale" data-id="{{ $payScale->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('pay-scale.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('pay-scale.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Pay Scale?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('pay-scale.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

</script>
