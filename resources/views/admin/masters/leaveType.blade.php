<x-admin.layout>
    <x-slot name="title">Leave Type</x-slot>
    <x-slot name="heading">Leave Type</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Leave Type</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="leave_type">Leave Type Name <span class="text-danger">*</span></label>
                                <input class="form-control" id="leave_type" name="name" type="text" placeholder="Enter Leave Type">
                                <span class="text-danger invalid name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="type">Select Type<span class="text-danger">*</span></label>
                                <select class="form-select type_div" id="type" name="type">
                                    <option value="">Select Type</option>
                                    <option value="0">Monthly</option>
                                    <option value="1">Yearly</option>
                                </select>
                                <span class="text-danger invalid type_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="no_of_leaves">No. of Leave <span class="text-danger"></span></label>
                                <input class="form-control" id="no_of_leaves" name="no_of_leaves" type="number" placeholder="Enter No. of Leave">
                                <span class="text-danger invalid no_of_leaves_err"></span>
                            </div>

                            <div class="col-md-4">
                                    <label   label class="col-form-label" for="carry_forward">Is Leave Carry Forward ? <span class="text-danger"></span></label>
                                    <div class="form-check mb-2">
                                        <input class="form-check-input" type="radio" name="carry_forward" id="carry_forward1" value="1">
                                        <label class="form-check-label" for="carry_forward1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="carry_forward" id="carry_forward2" value="2" checked="">
                                        <label class="form-check-label" for="carry_forward2">
                                            No
                                        </label>
                                    </div>
                            </div>

                            <div class="col-md-4">
                                <label   label class="col-form-label" for="encashable">Is Encashable ? <span class="text-danger"></span></label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="encashable" id="encashable1" value="1">
                                    <label class="form-check-label" for="encashable1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="encashable" id="encashable2" checked="" value="2">
                                    <label class="form-check-label" for="encashable2">
                                        No
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <label for="is_approve_by" class="col-form-label">Is Approved By</label>
                                <select name="is_approve_by" id="is_approve_by" class="form-select">
                                    <option value="1">AMC</option>
                                    <option value="2">DMC</option>
                                </select>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Leave Type</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="leave_type">Leave Type Name <span class="text-danger">*</span></label>
                                <input class="form-control" id="leave_type" name="name" type="text" placeholder="Enter Leave Type">
                                <span class="text-danger invalid name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="type">Select Type<span class="text-danger">*</span></label>
                                <select class="form-select type_div" id="edit_type" name="type">
                                    <option value="">Select Type</option>
                                    <option value="0">Monthly</option>
                                    <option value="1">Yearly</option>
                                </select>
                                <span class="text-danger invalid type_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="no_of_leaves">No. of Leave <span class="text-danger"></span></label>
                                <input class="form-control" id="no_of_leaves" name="no_of_leaves" type="number" placeholder="Enter No. of Leave">
                                <span class="text-danger invalid no_of_leaves_err"></span>
                            </div>

                            <div class="col-md-4">
                                    <label   label class="col-form-label" for="carry_forward">Is Leave Carry Forward ? <span class="text-danger"></span></label>
                                    <div class="form-check mb-2">
                                        <input class="form-check-input" type="radio" name="carry_forward" id="carry_forward1" value="1">
                                        <label class="form-check-label" for="carry_forward1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="carry_forward" id="carry_forward2" value="2" checked="">
                                        <label class="form-check-label" for="carry_forward2">
                                            No
                                        </label>
                                    </div>
                            </div>

                            <div class="col-md-4">
                                <label   label class="col-form-label" for="encashable">Is Encashable ? <span class="text-danger"></span></label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="encashable" id="encashable1" value="1">
                                    <label class="form-check-label" for="encashable1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="encashable" id="encashable2" checked="" value="2">
                                    <label class="form-check-label" for="encashable2">
                                        No
                                    </label>
                                </div>
                            </div>

                            
                            <div class="col-md-4">
                                <label for="is_approve_by" class="col-form-label">Is Approved By</label>
                                <select name="is_approve_by" id="is_approve_by" class="form-select">
                                    <option value="1">AMC</option>
                                    <option value="2">DMC</option>
                                </select>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('leaveType.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>No. of Leaves</th>
                                    <th>Is Carry Forward</th>
                                    <th>Is Encashabale</th>
                                    <th>Is DMC Approve</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leaveTypes as $leaveType)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $leaveType?->name }}</td>
                                        <td>{{ ($leaveType->type == 1)?'Yearly':'Monthly' }}</td>
                                        <td>{{ $leaveType?->no_of_leaves }}</td>
                                        <td>{{ ($leaveType->carry_forward == 1)?'Yes':'No' }}</td>
                                        <td>{{ ($leaveType->encashable == 1)?'Yes':'No' }}</td>
                                        <td>
                                            @if($leaveType?->is_approve_by == "1")
                                            Approved By AMC
                                            @else
                                            Approved By DMC
                                            @endif
                                        </td>
                                        <td>
                                            @can('leaveType.edit')
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Edit Leave Type" data-id="{{ $leaveType->id }}"><i data-feather="edit"></i></button>
                                            @endcan
                                            @can('leaveType.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Leave Type" data-id="{{ $leaveType->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('leave-type.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('leave-type.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('leave-type.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {
                    $("#editForm input[name='edit_model_id']").val(data.leaveType.id);
                    $("#editForm input[name='name']").val(data.leaveType.name);
                    $("#editForm input[name='no_of_leaves']").val(data.leaveType.no_of_leaves);
                    $("#editForm select[name='is_approve_by']").val(data.leaveType.is_approve_by);
                    $("#editForm input[name='carry_forward']").filter(`[value='${data.leaveType.carry_forward}']`).prop('checked', true);
                    $("#editForm input[name='encashable']").filter(`[value='${data.leaveType.encashable}']`).prop('checked', true);
                    $("#edit_type").html(data.leavetypeHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('leave-type.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('leave-type.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Leave Type?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('leave-type.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
