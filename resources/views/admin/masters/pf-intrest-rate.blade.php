<x-admin.layout>
    <x-slot name="title">PF Intrest Rate</x-slot>
    <x-slot name="heading">PF Intrest Rate</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add PF Intrest Rate</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="interest_rate">Intrest Rate <span class="text-danger">*</span></label>
                                <input class="form-control" id="interest_rate" name="interest_rate" type="text" placeholder="Enter Intrest Rate">
                                <span class="text-danger invalid interest_rate_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date <span class="text-danger"></span></label>
                                <input class="form-control" id="from_date" name="from_date" type="date" placeholder="Enter From Date">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="to_date" name="to_date" type="date" placeholder="Enter To Date">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Bank</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="interest_rate">Intrest Rate <span class="text-danger">*</span></label>
                                <input class="form-control" id="interest_rate" name="interest_rate" type="text" placeholder="Enter Intrest Rate">
                                <span class="text-danger invalid interest_rate_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date <span class="text-danger"></span></label>
                                <input class="form-control" id="from_date" name="from_date" type="date" placeholder="Enter From Date">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="to_date" name="to_date" type="date" placeholder="Enter To Date">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('intrest-rate.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Intrest Rate</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Activity Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pf_intrest_rates as $pf_intrest_rate)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $pf_intrest_rate->interest_rate }}</td>
                                        <td>{{ $pf_intrest_rate->to_date }}</td>
                                        <td>{{ $pf_intrest_rate->to_date }}</td>
                                        <td>
                                            <div class="form-check form-switch form-switch-lg ml-4" dir="ltr">
                                                &emsp;<input type="checkbox" class="form-check-input status-btn" id="customSwitchsizelg" data-id="{{ $pf_intrest_rate->id }}" value="{{ $pf_intrest_rate?->active_status }}"  {{ ($pf_intrest_rate?->active_status)?'Checked':'' }} >
                                            </div>
                                        </td>
                                        <td>
                                            @can('intrest-rate.edit')
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Edit PF Intrest Rate" data-id="{{ $pf_intrest_rate->id }}"><i data-feather="edit"></i></button>
                                            @endcan
                                            @can('intrest-rate.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete PF Intrest Rate" data-id="{{ $pf_intrest_rate->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('intrest-rate.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('intrest-rate.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('intrest-rate.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {

                    $("#editForm input[name='edit_model_id']").val(data.pfIntrestRate.id);
                    $("#editForm input[name='interest_rate']").val(data.pfIntrestRate.interest_rate);
                    $("#editForm input[name='from_date']").val(data.pfIntrestRate.from_date);
                    $("#editForm input[name='to_date']").val(data.pfIntrestRate.to_date);
                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('intrest-rate.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('intrest-rate.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Intrest Rate?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('intrest-rate.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>


<script>
        $(".status-btn").change(function(e){
        e.preventDefault();
        if ($(this).prop('checked')) {
            $('.status-btn').val('1')
        }else{
            $('.status-btn').val('0')
        }

        var btn_status = $('.status-btn').val();

        swal({
                title: "Are you sure to Change the Status?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
    });


    $("#buttons-datatables").on("click", ".status-btn", function(e) {
        e.preventDefault();

        if ($(this).prop('checked')) {
            $('.status-btn').val('1')
        }else{
            $('.status-btn').val('0')
        }

        var btn_status = $('.status-btn').val();

        swal({
                title: "Are you sure to Change the Status?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('intrest-rate-activity-status', [':model_id', ':btn_status']) }}";

                    url = url.replace(':model_id', model_id);
                    url = url.replace(':btn_status', btn_status);

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            '_method': "POST",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

</script>
