<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">
@php
    use Carbon\Carbon;
@endphp
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>GPF Ledger</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 14px;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        h1,
        h3 {
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            border: 1px solid black;
            padding: 5px;
            text-align: left;
        }

        #toptable th,
        td {
            border: 1px dashed black;
            padding: 8px;
            text-align: left;
        }

        .amount {
            text-align: right;
        }

        .notes {
            margin-top: 20px;
        }

        .list {
            list-style-type: none;
            padding: 0;
        }

        .list li {
            margin-bottom: 5px;
        }

        .footer {
            margin-top: 20px;
        }

        .bottom_row {
            display: inline-block;
            vertical-align: top;
            width: 20%;
        }
    </style>
</head>

<body>

    <h1>पनवेल महानगरपालिका</h1>
    <h3>नमुना क्र. १८-आर्थिक वर्ष {{ convertToMarathiNumber(substr($financial_year->from_date, 0, 4)) }}-{{ convertToMarathiNumber( substr($financial_year->to_date, 0, 4) ) }} चा भविष्य निर्वाह निधी खातेवही</h3>

    <table id="toptable">
        <tr>
            <td>कमचयाचे नाव:</td>
            <td>{{ $employee?->fullname }}</td>
            <td>वेतन देयक :</td>
            <td>महानगरपालिका</td>
        </tr>
        <tr>
            <td>पदनाम:</td>
            <td>{{ $employee?->designation?->marathi_name }}</td>
            <td>भ नि खाते क्रं:</td>
            <td>{{ $employee?->pf_account_no }}</td>
        </tr>
        <tr>
            <td>प्रारंभिक शिल्लक:</td>
            <td>{{ $employee->pfOpeningBalance?->opening_balance }}</td>
            <td>कर्मचारी ओळख क्र.:</td>
            <td>{{ $employee?->employee_id }}</td>
        </tr>
    </table>

    <table>
        <thead>
            <tr>
                <th>माहे</th>
                <th>भ.नी.नी. वर्गणी</th>
                <th>अग्रीम वसुली</th>
                <th>एकूण वर्गणी व वसुली</th>
                <th>इतर जमा</th>
                <th>परतावा अग्रीम</th>
                <th>ना-परतावा अग्रीम</th>
                <th>संकलित जमा</th>
                <th>व्याज दर</th>
                <th>व्याज रक्कम</th>
            </tr>
        </thead>
        <tbody>

            {{-- @php
                $total_pf_contribution = 0;
                $total_pf_loan = 0;
                $total_other_amt = 0;
                $total_amt = 0;
                $total_loan_amt = 0;
                $total_grand_total = 0;
                $total_intrest_amt = 0;
                $last_total = 0;
            @endphp --}}

            {{-- @foreach ($employee->freezeAttendances  as $attendance)
                @if ($pf_opening_balance?->closing_status == 1 && $pf_opening_balance?->closing_date < $employee_provident_fund->salary_month)
                    <tr>
                        <td>
                            <b>{{ $pf_opening_balance?->type == 1 ? 'Retire' : 'Expired' }}</b>
                        </td>
                        <td colspan="10">
                            <b>{{ $pf_opening_balance?->remark }}</b>
                        </td>
                    </tr>
                    @php
                        break;
                    @endphp
                @else
                    @php
                        $total_pf_contribution += $employee_provident_fund->pf_contribution;
                        $total_pf_loan += $employee_provident_fund->pf_loan;
                        $total_other_amt += $employee_provident_fund->other_amount;
                        $total_amt = $employee_provident_fund->total;
                        $total_loan_amt += $employee_provident_fund->loan_amt;
                        $total_grand_total += $employee_provident_fund->grand_total;
                        $total_intrest_amt += $employee_provident_fund->intrest_amt;
                        $last_total = $employee_provident_fund->total;
                    @endphp

                    <tr @if (empty($employee_provident_fund->total)) style="color: red;" @endif>
                        <td>{{ $employee_provident_fund->current_month }}</td>
                        <td>{{ $employee_provident_fund->pf_contribution }}</td>
                        <td>{{ $employee_provident_fund->loan_amt }}</td>
                        <td>{{ $employee_provident_fund->other_amount }}</td>
                        <td>{{ $employee_provident_fund->total }}</td>
                        <td>{{ $employee_provident_fund->loan_date }}</td>
                        <td>{{ $employee_provident_fund->loan_amt }}</td>
                        <td>{{ $employee_provident_fund->grand_total }}</td>
                        <td>{{ $employee_provident_fund->intrest_rate }}</td>
                        <td>
                            @if (empty($employee_provident_fund->total))
                                {{ 'PF Report Not generated for this month' }}
                            @else
                                {{ $employee_provident_fund->remark }}
                            @endif
                        </td>
                    </tr>
                @endif
            @endforeach --}}
            @php
                $nextSum = $employee->pfOpeningBalance->opening_balance;
            @endphp
            @foreach ($employee->freezeAttendances as $salary)
                @php
                    $deductionIds = explode(',', $salary->deduction_Id);
                    $deductionAmounts = explode(',', $salary->deduction_Amt);
                    $deductionCollection = collect(array_combine($deductionIds, $deductionAmounts));
                    $deductionCollection = $deductionCollection->only([5]);
                    // dd($deductionCollection);
                    $nextSum += $deductionCollection[5];
                @endphp
                <tr>
                    <td>{{ __('messages.' . Carbon::createFromFormat('m', $salary->month)->format('F')) . '-' . convertToMarathiNumber(substr($salary->from_date, 0, 4)) }}</td>
                    <td>{{ convertToMarathiNumber(number_format($deductionCollection[5])) }}</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>{{ convertToMarathiNumber($nextSum) }}</td>
                    <td>{{ convertToMarathiNumber($pfIntrestRate->interest_rate) }}</td>
                    <td>{{ convertToMarathiNumber($deductionCollection[5]*( $pfIntrestRate->interest_rate/12) ) }}</td>
                </tr>
            @endforeach
            <tr>
                <td>एकूण</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>

    <br>

    <div style="display: flex; align-items: flex-start;">
        <div style="display: inline-block; vertical-align: top; width: 70%;">
            <table style="width: 80%;">
                <tr>
                    <td>ए.#ल, २०२३#ारंभक श$लक</td>
                    <td style="width: 30%;">₹१८०६३९</td>
                </tr>
                <tr>
                    <td>वगणीव अमवसलु ी</td>
                    <td>₹६४२००</td>
                </tr>
                <tr>
                    <td>सातवावेतन फरक अनुसार (पहला+Kसरा+.तसरा+चौथाहNता)</td>
                    <td>₹ १४४९३४.००</td>
                </tr>
                <tr>
                    <td>सातवावेतन फरक अनुसार भ.न. न. फरक</td>
                    <td>१०,००</td>
                </tr>
                <tr>
                    <td>माच, २०२४ अखेर अजंतवाषंक Sज </td>
                    <td>₹२२१००,००</td>
                </tr>
                <tr>
                    <td>या वेबसाइट वर असलेले</td>
                    <td>१०</td>
                </tr>
                <tr>
                    <td>एकूण र=कम</td>
                    <td>₹२९६२६०</td>
                </tr>
                <tr>
                    <td>सातवावेतन फरक अनुसार (पहला+Kसरा+.तसरा+चौथाहNता) </td>
                    <td>₹</td>
                </tr>
                <tr>
                    <td>सातवावेतन फरक अनुसारपाचवाहNता </td>
                    <td>१०,००</td>
                </tr>
                <tr>
                    <td>परतावाव ना-परतावा अHीम</td>
                    <td>₹८६०००</td>
                </tr>
                <tr>
                    <td>अं.तमपरतावा </td>
                    <td>१०</td>
                </tr>
                <tr>
                    <td>माच, २०२४ अखेरची श$लक </td>
                    <td>₹६५३२६</td>
                </tr>
            </table>
        </div>
        <div style="display: inline-block; vertical-align: top; margin-left: 10px;">
            <p>शेरा:</p>
            <p>दिनांक : २/१०/२०२५ ४:५३:५१ PM</p>
        </div>
    </div>

    <br><br><br>

    <div style="display: flex; align-items: flex-start;">
        <div class="bottom_row">
            <p>लपक (आ8थापना) </p>
            <p>पनवेल महानगरपालका </p>
        </div>

        <div class="bottom_row">
            <p>अ.धZक (आ8थापना)</p>
            <p>पनवेल महानगरपालका </p>
        </div>

        <div class="bottom_row">
            <p>सहायकआयत(आ8थापना)</p>
            <p>पनवेल महानगरपालक</p>
        </div>

        <div class="bottom_row">
            <p>उपआयत- I(मयालय)</p>
            <p>पनवेल महानगरपालका</p>
        </div>
    </div>
</body>

</html>
