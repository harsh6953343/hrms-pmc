<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">
@php
    use Carbon\Carbon;
@endphp
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>GPF Ledger</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 14px;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        h1,
        h3 {
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            border: 1px solid black;
            padding: 5px;
            text-align: left;
        }

        #toptable th,
        td {
            border: 1px dashed black;
            padding: 8px;
            text-align: left;
        }

        .amount {
            text-align: right;
        }

        .notes {
            margin-top: 20px;
        }

        .list {
            list-style-type: none;
            padding: 0;
        }

        .list li {
            margin-bottom: 5px;
        }

        .footer {
            margin-top: 20px;
        }

        .bottom_row {
            display: inline-block;
            vertical-align: top;
            width: 20%;
        }
    </style>
</head>

<body>

    <h1>पनवेल महानगरपालिका</h1>
    <h3>नमुना क्र. १८-आर्थिक वर्ष {{ convertToMarathiNumber(substr($financial_year->from_date, 0, 4)) }}-{{ convertToMarathiNumber( substr($financial_year->to_date, 0, 4) ) }} चा भविष्य निर्वाह निधी खातेवही</h3>

    <table id="toptable">
        <tr>
            <td>कमचयाचे नाव:</td>
            <td>{{ $employee?->fullname }}</td>
            <td>वेतन देयक :</td>
            <td>महानगरपालिका</td>
        </tr>
        <tr>
            <td>पदनाम:</td>
            <td>{{ $employee?->designation?->marathi_name }}</td>
            <td>भ नि खाते क्रं:</td>
            <td>{{ convertToMarathiNumber($employee?->pf_account_no) }}</td>
        </tr>
        <tr>
            <td>प्रारंभिक शिल्लक:</td>
            <td>{{ $employee->pfOpeningBalance?->opening_balance }}</td>
            <td>कर्मचारी ओळख क्र.:</td>
            <td>{{ convertToMarathiNumber($employee?->employee_id) }}</td>
        </tr>
    </table>

    <table>
        <thead>
            <tr>
                <th>माहे</th>
                <th>भ.नी.नी. वर्गणी</th>
                <th>अग्रीम वसुली</th>
                <th>एकूण वर्गणी व वसुली</th>
                <th>इतर जमा</th>
                <th>परतावा अग्रीम</th>
                <th>ना-परतावा अग्रीम</th>
                <th>संकलित जमा</th>
                <th>व्याज दर</th>
                <th>व्याज रक्कम</th>
            </tr>
        </thead>
        <tbody>
            @php
                $totalContro = $totalAgreem = $totalIttar = $totalPartava = $totalNapartava = $totalInterest = 0;
                if(substr($financial_year->from_date, 0, 4) == 2024)
                {
                    $firstFreeze = $employee->freezeAttendances[0];
                    $deductionIds = explode(',', $firstFreeze->deduction_Id);
                    $deductionAmounts = explode(',', $firstFreeze->deduction_Amt);
                    $deductionCollection = collect(array_combine($deductionIds, $deductionAmounts));
                    $firstContribution = $deductionCollection->only([5]);

                    $tillMayData = collect([
                        ['id' => 1, 'month' => 3, 'from_date' => '2024', 'contribution' => $firstContribution[5], 'interest_rate' => $pfIntrestRate->interest_rate],
                        ['id' => 2, 'month' => 4, 'from_date' => '2024', 'contribution' => $firstContribution[5], 'interest_rate' => $pfIntrestRate->interest_rate],
                        ['id' => 3, 'month' => 5, 'from_date' => '2024', 'contribution' => $firstContribution[5], 'interest_rate' => $pfIntrestRate->interest_rate],
                    ]);
                }
                $nextSum = $employee->pfOpeningBalance?->opening_balance;
            @endphp

            @if(substr($financial_year->from_date, 0, 4) == 2024)
                @foreach ($tillMayData as $salary)
                    @php
                        $nextSum += $salary['contribution'];
                        $totalContro += $salary['contribution'];
                        $interestAmt = $nextSum*( $salary['interest_rate']/12);
                        $totalInterest += $interestAmt;
                    @endphp
                    <tr>
                        <td>{{ __('messages.' . Carbon::createFromFormat('m', $salary['month'])->format('F')) . '-' . convertToMarathiNumber($salary['from_date']) }}</td>
                        <td>{{ convertToMarathiNumber(number_format($salary['contribution'])) }}</td>
                        <td>-</td>
                        <td>{{ convertToMarathiNumber(number_format($salary['contribution'])) }}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>{{ convertToMarathiNumber($nextSum) }}</td>
                        <td>{{ convertToMarathiNumber($salary['interest_rate']) }}</td>
                        <td>{{ convertToMarathiNumber( number_format( $interestAmt , 2) ) }}</td>
                    </tr>
                @endforeach
            @endif


            @foreach ($employee->freezeAttendances as $salary)
                @php
                    $deductionIds = explode(',', $salary->deduction_Id);
                    $deductionAmounts = explode(',', $salary->deduction_Amt);
                    $deductionCollection = collect(array_combine($deductionIds, $deductionAmounts));
                    $deductionCollection = $deductionCollection->only([5]);

                    $interestAmt = $nextSum*( $pfIntrestRate->interest_rate/12);
                    $nextSum += $deductionCollection[5];
                    $totalContro += $deductionCollection[5];
                    $totalInterest += $interestAmt;
                @endphp
                <tr>
                    <td>{{ __('messages.' . Carbon::createFromFormat('m', $salary->month)->format('F')) . '-' . convertToMarathiNumber(substr($salary->from_date, 0, 4)) }}</td>
                    <td>{{ convertToMarathiNumber(number_format($deductionCollection[5])) }}</td>
                    <td>-</td>
                    <td>{{ convertToMarathiNumber(number_format($deductionCollection[5])) }}</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>{{ convertToMarathiNumber(number_format($nextSum)) }}</td>
                    <td>{{ convertToMarathiNumber($pfIntrestRate->interest_rate) }}</td>
                    <td>{{ convertToMarathiNumber( number_format($nextSum*( $pfIntrestRate->interest_rate/12), 2) ) }}</td>
                </tr>
            @endforeach
            <tr>
                <td>एकूण</td>
                <td>{{ convertToMarathiNumber( number_format($totalContro,2) ) }}</td>
                <td>0</td>
                <td>{{ convertToMarathiNumber( number_format($totalContro,2) ) }}</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>{{ convertToMarathiNumber( number_format(($totalInterest+$nextSum)) ) }}</td>
                <td>-</td>
                <td>{{ convertToMarathiNumber( 0 ) }}</td>
            </tr>
        </tbody>
    </table>

    <br>

    <div style="display: flex; align-items: flex-start;">
        <div style="display: inline-block; vertical-align: top; width: 70%;">
            <table style="width: 80%;">
                <tr>
                    <td>एप्रिल, {{ convertToMarathiNumber(((int)substr($financial_year->from_date, 0, 4))-1)  }} प्रारंभिक शिल्लक</td>
                    <td style="width: 30%;">₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>वर्गणी व अग्रीम वसुली</td>
                    <td>₹ {{ convertToMarathiNumber(number_format($totalContro,2)) }} </td>
                </tr>
                <tr>
                    <td>सातवा वेतन फरक अनुसार (पहिला + दुसरा + तिसरा + चौथा हफ्ता)</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>सातवा वेतन फरक अनुसार पाचवा हफ्ता</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>सातवा वेतन फरक अनुसार भ. नि. फरक</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>मार्च, {{ convertToMarathiNumber((int)substr($financial_year->from_date, 0, 4)) }} अखेर अर्जित वार्षिक व्याज</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>एकूण रक्कम</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>सातवा वेतन फरक अनुसार (पहिला + दुसरा + तिसरा + चौथा हफ्ता)</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>परतावा व ना-परतावा अग्रीम</td>
                    <td>₹</td>
                </tr>
                <tr>
                    <td>अंतिम परतावा</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
                <tr>
                    <td>मार्च, {{ convertToMarathiNumber((int)substr($financial_year->from_date, 0, 4)) }} अखेरची शिल्लक</td>
                    <td>₹ {{ convertToMarathiNumber(0) }}</td>
                </tr>
            </table>
        </div>
        <div style="display: inline-block; vertical-align: top; margin-left: 10px;">
            <p>शेरा:</p>
            <p>दिनांक : {{ convertToMarathiNumber(Carbon::now()->format('d/m/Y h:i:s a')) }}</p>
        </div>
    </div>

    <br><br><br>
    <br><br><br>

    <div style="display: block; align-items: flex-start; width: 90%; margin-left:150px;">
        <div class="bottom_row">
            <p>लिपिक (आस्थापना)</p>
            <p>पनवेल महानगरपालिका </p>
        </div>

        <div class="bottom_row">
            <p>अधिक्षक (आस्थापना)</p>
            <p>पनवेल महानगरपालिका </p>
        </div>

        <div class="bottom_row">
            <p>सहाय्यक आयुक्त (आस्थापना)</p>
            <p>पनवेल महानगरपालिका</p>
        </div>

        <div class="bottom_row">
            <p>उपआयुक्त- I(मुख्यालय)</p>
            <p>पनवेल महानगरपालिका</p>
        </div>
    </div>
</body>

</html>
