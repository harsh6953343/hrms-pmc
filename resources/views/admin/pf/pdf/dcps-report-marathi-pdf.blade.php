@php
    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>DCPS Ledger</title>
<style>
body {
    font-family: sans-serif;
    font-size: 14px;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
}

h1, h3 {
  text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}
th, td {
    border: 1px solid black;
    padding: 5px;
    text-align: left;
}
#toptable th, td {
    border: 1px dashed black;
    padding: 8px;
    text-align: left;
  }
.amount {
    text-align: right;
}
.notes {
    margin-top: 20px;
}
.list {
    list-style-type: none;
    padding: 0;
}
.list li {
    margin-bottom: 5px;
}
.footer {
    margin-top: 20px;
}

.bottom_row{
    display: inline-block; 
    vertical-align: top; 
    width: 20%;
}
.text-mod {
  font-size: 35px;
  font-weight: bold;
}
.bold-text {
  font-weight: bold;
}
</style>
</head>
<body>

    <h1 class="text-mod">पनवेल महानगरपालिका</h1>
    <h3 class="bold-text">आर्थिक वर्ष '{{convertToMarathiNumber(substr($financial_year->title,2))}}' ची परिभाषित आंशदायी निवरततिवेतन योजना -१ खातेवही</h3>

    <table id="toptable">
    <tr>
        <td class="bold-text">कर्मचान्याचे नाव:</td>
        <td>{{ $employee_details?->fullname }}</td>
        <td class="bold-text">वेतन देयक:</td>
        <td>महानगरपालिका</td>
    </tr>
    <tr>
        <td class="bold-text">पदनाम:</td>
        <td>{{ $employee_details?->designation?->marathi_name }}</td>
        <td class="bold-text">अ. न. वे. यो.-१ खाते क्र: </td>
        <td>{{ $employee_details?->pf_account_no }}</td>
    </tr>
    <tr>
        <td class="bold-text">प्रारंभिक शिल्लक:</td>
        <td>₹{{ $pf_opening_balance?->opening_balance }}</td>
        <td class="bold-text">कर्मचारी ओळख क्र.:</td>
        <td>{{ $employee_details?->employee_id }}</td>
    </tr>
    </table>

    <table>
        <thead>
          <tr>
            <th rowspan=2>माहे</th>
            <th>वर्गणी</th>
            <th>वर्गणी</th>
            <th>शि.व.</th>
            <th>शि.व.</th>
            <th>प.म.पा</th>
            <th>इतर</th>
            <th>एकूण</th>
            <th>संकलित</th>
            <th>व्याज</th>
            <th>व्याज</th>
          </tr>
          <tr>
            <th>(वेतन)</th>
            <th>(पुरवणी)</th>
            <th>(वेतन)</th>
            <th>(पुरवणी)</th>
            <th>हिस्सा</th>
            <th>जमा</th>
            <th>जमा</th>
            <th>जमा</th>
            <th>दर</th>
            <th>रक्कम</th>
          </tr>
        </thead>
        <tbody>
            
            @php
            $total_employee_contribution = 0;
            $total_corporate_contribution = 0;
            $total_pf_loan = 0;
            $total_other_amt = 0;
            $total_amt = 0;
            $total_loan_amt = 0;
            $total_grand_total = 0;
            $total_intrest_amt = 0;
            $last_total = 0;

        @endphp

        @foreach ($employee_provident_funds as $employee_provident_fund)

            @if($pf_opening_balance?->closing_status == 1 && $pf_opening_balance?->closing_date < $employee_provident_fund->salary_month)

               <tr>
                    <td>
                        <b>{{ ($pf_opening_balance?->type == 1)?'Retire':'Expired' }}</b>
                    </td>
                    <td colspan="10">
                        <b>{{ $pf_opening_balance?->remark }}</b>
                    </td>
                </tr>
                @php
                    break;
                @endphp
            @else

                @php
                $total_employee_contribution+= $employee_provident_fund->dcps_employee;
                $total_corporate_contribution += $employee_provident_fund->dcps_corp;
                $total_other_amt += $employee_provident_fund->other_amount;
                $total_amt = $employee_provident_fund->total;
                $total_loan_amt += $employee_provident_fund->loan_amt;
                $total_grand_total += $employee_provident_fund->grand_total;
                $total_intrest_amt += $employee_provident_fund->intrest_amt;
                // $last_total = $employee_provident_fund->total;
                @endphp

                <tr  @if(empty($employee_provident_fund->total))
                    style="color: red;"
                @endif>
                    @php
                        list($month,$year) = explode(' ',Carbon::parse($employee_provident_fund->current_month)->format('F Y'));
                    @endphp
                    <td>{{ __('messages.'.$month).', '.convertToMarathiNumber($year) }}</td>
                    <td>{{ $employee_provident_fund->dcps_employee ?? 0}}</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>{{ $employee_provident_fund->dcps_corp ?? 0}}</td>
                    <td>{{ $employee_provident_fund->other_amount }}</td>
                    <td>{{ $employee_provident_fund->total }}</td>
                    <td>{{ $employee_provident_fund->grand_total }}</td>
                    <td>{{ $employee_provident_fund->intrest_rate }}</td>
                    <td>{{ $employee_provident_fund->intrest_amt }}</td>
                </tr>

            @endif

        @endforeach
          <tr class="bold-text">
            <td>एकूण</td>
            <td>{{ $total_employee_contribution }}</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>{{ $total_corporate_contribution }}</td>
            <td>{{ $total_other_amt }}</td>
            <td>{{ $total_amt }}</td>
            <td>{{ $total_grand_total }}</td>
            <td></td>
            <td>{{ $total_intrest_amt }}</td>
          </tr>
        </tbody>
      </table>

      <br>

      <div style="display: flex; align-items: flex-start;">
        <div style="display: inline-block; vertical-align: top; width: 70%;">
            <table style="width: 80%;">
                <tr>
                <td class="bold-text">एप्रिल,२०२४ प्रारंभीक शिल्लक </td>
                <td style="width: 30%;">₹{{ $pf_opening_balance?->opening_balance }}</td>
                </tr>
                <tr>
                <td class="bold-text">कर्मचारी हिस्सा</td>
                <td>₹{{$total_employee_contribution}}</td>
                </tr>
                <tr>
                <td class="bold-text">शिल्लक वसुली </td>
                <td>₹0</td>
                </tr>
                <tr>
                <td class="bold-text">प.म.पा. हिस्सा</td>
                <td>₹{{$total_corporate_contribution}}</td>
                </tr>
                <tr>
                <td class="bold-text">इतर जमा </td>
                <td>₹{{ $total_other_amt }}</td>
                </tr>
                <tr>
                <td class="bold-text">मार्च, २०२५ अखेर अर्जित वार्षिक व्याज</td>
                <td>₹{{$total_intrest_amt}}</td>
                </tr>
                <tr>
                <td class="bold-text">मार्च, २०२५ अखेरची शिल्लक</td>
                <td>₹{{$pf_opening_balance?->opening_balance + $total_employee_contribution + $total_corporate_contribution + $total_intrest_amt}}</td>
                </tr>
            </table>
        </div>
        <div style="display: inline-block; vertical-align: top; margin-left: 10px;">
            
            <p><strong>दिनांक</strong> : {{ convertToMarathiNumber(\Carbon\Carbon::now()->format('d/m/Y h:i:s A')) }}</p>
        </div>
      </div>
    
    <br><br><br>

    <div style="display: flex; align-items: flex-start;">
      <div class="bottom_row">
        <p>लिपिक (आस्थापना) </p>
        <p>पनवेल महानगरपालिका  </p>
      </div>

      <div class="bottom_row">
        <p>अधिक्षक (आस्थापना)</p>
        <p>पनवेल महानगरपालिका </p>
      </div>

      <div class="bottom_row">
        <p>सहाय्यक आयुक्त (आस्थापना)</p>
        <p>पनवेल महानगरपालिका</p>
      </div>

      <div class="bottom_row">
        <p>उपआयुक्त-I(मुख्यालय)</p>
        <p>पनवेल महानगरपालिका</p>
      </div>
    </div>
</body>
</html>