<x-admin.layout>
    <x-slot name="title">PF Opening Balance</x-slot>
    <x-slot name="heading">PF Opening Balance</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Opening Balance</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="employee_id" id="employee_id">
                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="Emp_Code">Select Employee<span class="text-danger">*</span></label>
                                <select class="js-example-basic-multiple" id="Emp_Code" name="Emp_Code">
                                    <option value="">Select Employee</option>
                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->employee_id }}">{{ $employee->fname." ".$employee->mname." ".$employee->lname." (". $employee->employee_id .")" }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pf_no">PF No.<span class="text-danger">*</span></label>
                                <input class="form-control" id="pf_no" name="pf_no" type="text" placeholder="PF No." readonly>
                                <span class="text-danger invalid pf_no_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="financial_year_id">Select Financial Year<span class="text-danger">*</span></label>
                                <select class="js-example-basic-multiple" id="financial_year_id" name="financial_year_id">
                                    <option value="">Select Financial Year</option>
                                    @foreach ($financial_years as $financial_year)
                                        <option value="{{ $financial_year->id }}">{{ $financial_year->title}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid financial_year_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="opening_balance">PF Opening Balance<span class="text-danger">*</span></label>
                                <input class="form-control title" id="opening_balance" name="opening_balance" type="text" placeholder="Enter PF Opening Balance">
                                <span class="text-danger invalid opening_balance_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('pf-opening-balance.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Joining Date</th>
                                    <th>PF No</th>
                                    <th>Financial Year</th>
                                    <th>Opening Balance</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pf_opening_balances as $pf_opening_balance)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $pf_opening_balance->Emp_Code }}</td>
                                        <td>{{ $pf_opening_balance?->employee?->doj }}</td>
                                        <td>{{ $pf_opening_balance?->pf_no }}</td>
                                        <td>{{ $pf_opening_balance->financial_year->title }}</td>
                                        <td>{{ $pf_opening_balance->opening_balance }}</td>
                                        <td>
                                            @if($pf_opening_balance->status == 0)
                                                @can('pf-opening-balance.delete')
                                                    <button class="btn btn-danger rem-element px-2 py-1" title="Delete Allowance" data-id="{{ $pf_opening_balance->id }}"><i data-feather="trash-2"></i> </button>
                                                @endcan
                                            @else
                                            {{ 'Opening Balance Record cannot' }} <br> {{ 'delete because pf report is generated' }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-admin.layout>

{{-- On change Employee --}}
<script>
    $("#Emp_Code").change(function(){
        var Emp_Code = $('#Emp_Code').val();
        if(Emp_Code != '')
        {
            var url = "{{ route('fetch-employee-details', ':Emp_Code') }}";

            $.ajax({
                url: url.replace(':Emp_Code', Emp_Code),
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data) {
                    if (!data.error && !data.error2) {
                        if (data.result === 1) {

                            $('#ward').val(data.employee_details.ward.name);
                            $('#department').val(data.employee_details.department.name);
                            $('#class').val(data.employee_details.class.name);
                            $('#designation').val(data.employee_details.designation.name);
                            $('#employee_id').val(data.employee_details.id);
                            $('#pf_no').val(data.employee_details.pf_account_no);


                        } else if (data.result === 0) {
                            $('#ward').val('');
                            $('#department').val('');
                            $('#designation').val('');
                            $('#class').val('');
                            $('#employee_id').val('');
                            $('#pf_no').val('');


                            alert("Employee details not found!");
                        } else {
                            alert("Unexpected result from the server");
                        }
                }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }
        else{
            alert('Please Enter Employee Id');
        }

    });

</script>



{{-- Add --}}
<script>

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('pf-opening-balance.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('pf-opening-balance.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Opening Balance?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('pf-opening-balance.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
