<x-admin.layout>
    @php
        // freeze attendance allowance
        $freezeAallowanceId = explode(',', $suplimentaryEmployeeLeaveBill->freeze_attendance_allowance_id);
        $freezeAllowanceAmt = explode(',', $suplimentaryEmployeeLeaveBill->freeze_attendance_allowance_amount);
        $freezeAllowanceArray = array_combine($freezeAallowanceId, $freezeAllowanceAmt);

        // freeze attendance deduction
        $freezeDeductionId = explode(',', $suplimentaryEmployeeLeaveBill->freeze_attendance_deduction_id);
        $freezeDeductionAmt = explode(',', $suplimentaryEmployeeLeaveBill->freeze_attendance_deduction_amount);
        $freezeDeductionArray = array_combine($freezeDeductionId, $freezeDeductionAmt);

        // suplimentry allowance
        $suplimentryAallowanceId = explode(',', $suplimentaryEmployeeLeaveBill->allowance_id);
        $suplimentryAllowanceAmt = explode(',', $suplimentaryEmployeeLeaveBill->allowance_amount);
        $suplimentryAllowanceArray = array_combine($suplimentryAallowanceId, $suplimentryAllowanceAmt);

        // suplimentry deduction
        $suplimentryDeductionId = explode(',', $suplimentaryEmployeeLeaveBill->deduction_id);
        $suplimentryDeductionAmt = explode(',', $suplimentaryEmployeeLeaveBill->deduction_amount);
        $suplimentryDeductionArray = array_combine($suplimentryDeductionId, $suplimentryDeductionAmt);


        // employee salary structure allowance
        $employeeSalaryStructureAallowanceId = explode(',', $suplimentaryEmployeeLeaveBill->employee_salary_allowance_id);
        $employeeSalaryStructureAllowanceAmt = explode(',', $suplimentaryEmployeeLeaveBill->employee_salary_allowance_amount);
        $employeeSalaryStructureAllowanceArray = array_combine($employeeSalaryStructureAallowanceId, $employeeSalaryStructureAllowanceAmt);

        // employee salary structure deduction
        $employeeSalaryStructureDeductionId = explode(',', $suplimentaryEmployeeLeaveBill->employee_salary_deduction_id);
        $employeeSalaryStructureDeductionAmt = explode(',', $suplimentaryEmployeeLeaveBill->employee_salary_deduction_amount);
        $employeeSalaryStructureDeductionArray = array_combine($employeeSalaryStructureDeductionId, $employeeSalaryStructureDeductionAmt);



        $totalDueAllowance = 0;
        $totalDrawnAllowance = 0;
        $totalDifferenceAllowance = 0;

        $totalDueDeduction = 0;
        $totalDrawnDeduction = 0;
        $totalDifferenceDeduction = 0;

    @endphp
    <x-slot name="title">Supplimentary Leave Bill Calculate</x-slot>
    <x-slot name="heading">Supplimentary Leave Bill Calculate</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <form method="post" id="addForm" action="{{ route('supplimentary-calculate.update', $suplimentaryEmployeeLeaveBill->id) }}">
                @method('put')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-3 mb-3">
                                <label for="">Employee Code</label>
                                <input type="text" class="form-control" name="bill_no" value="{{ $suplimentaryEmployeeLeaveBill->emp_code }}" readonly>
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Year</label>
                                <input type="text" class="form-control" name="bill_date" value="{{ date('Y', strtotime($suplimentaryEmployeeLeaveBill->month)) }}" readonly>
                            </div>
                            <div class="col-3 mb-3">
                                <label for="">Month</label>
                                <input type="text" value="{{ date('F', strtotime($suplimentaryEmployeeLeaveBill->month)) }}" class="form-control" readonly>
                            </div>

                            <div class="col-3 mb-3">
                                <label for="">Bill No</label>
                                <input type="text" class="form-control" value="{{ $suplimentaryEmployeeLeaveBill?->suplimentryLeaveBill?->bill_no }}" name="bill_description" readonly>
                            </div>

                            <div class="col-3 mb-3">
                                <label for="">No Of Days</label>
                                <input type="text" class="form-control" value="{{ $suplimentaryEmployeeLeaveBill->supplimentry_pr + $suplimentaryEmployeeLeaveBill->supplimentry_ul + $suplimentaryEmployeeLeaveBill->supplimentry_ml + $suplimentaryEmployeeLeaveBill->supplimentry_el }}" readonly>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <h1>Earning</h1>
                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Type of Pay</th>
                                                <th>Due</th>
                                                <th>Drawn</th>
                                                <th>Difference</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Earned Basic</td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employeeSalaryBasicSalary" class="employeeSalaryBasicSalary" value="{{ $suplimentaryEmployeeLeaveBill->employee_salary_basic_salary ?? 0 }}" style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freezeAttendanceBasicSalary" class="freezeAttendanceBasicSalary" value="{{ $suplimentaryEmployeeLeaveBill->freeze_attendance_basic_salary ?? 0 }}" style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentryBasicSalary" class="suplimentryBasicSalary"  value="{{ round($suplimentaryEmployeeLeaveBill->basic_salary) ?? 0 }}" style="width: 100px" />
                                                </td>
                                            </tr>
                                            @php
                                                $totalDueAllowance += $suplimentaryEmployeeLeaveBill->employee_salary_basic_salary;
                                                $totalDrawnAllowance += $suplimentaryEmployeeLeaveBill->freeze_attendance_basic_salary;
                                                $totalDifferenceAllowance += $suplimentaryEmployeeLeaveBill->basic_salary;
                                            @endphp

                                            @foreach($allowances as $allowance)
                                            @if ($suplimentaryEmployeeLeaveBill?->employee?->doj < '2005-11-01' && $suplimentaryEmployeeLeaveBill?->employee?->department_id != 3 && $allowance->id == 6)
                                            @continue
                                            @endif
                                            <tr>
                                                <td>{{ $allowance->allowance }}</td>
                                                <td>
                                                    <input type="hidden" name="employeeSalaryAllowanceId[]" value="{{ $allowance->id }}" />
                                                    <input type="hidden" name="employeeSalaryAllowancetype[]" value="{{ $allowance->type }}" />

                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employeeSalaryEarningDue[]" class="employeeSalaryAllowanceDue" value="{{ $employeeSalaryStructureAllowanceArray[$allowance->id] ?? 0 }}" style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="hidden" name="freezeAttendanceAllowanceId[]" value="{{ $allowance->id }}" />
                                                    <input type="hidden" name="freezeAttendanceAllowancetype[]" value="{{ $allowance->type }}" />

                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freezeAttendanceEarningDrawn[]" class="freezeAttendanceAllowanceDrawn" value="{{ $freezeAllowanceArray[$allowance->id] ?? 0 }}" style="width: 100px" />
                                                </td>
                                                <td>
                                                    
                                                    <input type="hidden" name="suplimentryAllowanceId[]" value="{{ $allowance->id }}" />
                                                    <input type="hidden" name="suplimentryAllowancetype[]" value="{{ $allowance->type }}" />


                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentryEarningDifference[]" class="suplimentryAllowanceDrawn suplimentryDeductionDifferencedd"  value="{{ $suplimentryAllowanceArray[$allowance->id] ?? 0 }}" style="width: 100px" />
                                                </td>
                                            </tr>

                                            @php
                                                $employeeSalaryAllowanceAmount = $employeeSalaryStructureAllowanceArray[$allowance->id] ?? 0;
                                                $freezeAllowanceAmount = $freezeAllowanceArray[$allowance->id] ?? 0;
                                                $suplimentryAllowanceAmount = $suplimentryAllowanceArray[$allowance->id] ?? 0;

                                                $totalDueAllowance += $employeeSalaryAllowanceAmount;
                                                $totalDrawnAllowance += $freezeAllowanceAmount;
                                                $totalDifferenceAllowance += $suplimentryAllowanceAmount;
                                            @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <h1>Deduction</h1>
                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Type of Pay</th>
                                                <th>Due</th>
                                                <th>Drawn</th>
                                                <th>Difference</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($deductions as $deduction)
                                            @if ($suplimentaryEmployeeLeaveBill?->employee?->doj < '2005-11-01' && $suplimentaryEmployeeLeaveBill?->employee?->department_id != 3 && ($deduction->id == 8 || $deduction->id == 7))
                                            @continue
                                            @endif
                                            <tr>
                                                <td>{{ $deduction->deduction }}</td>
                                                <td>
                                                    <input type="hidden" name="employeeSalaryDeductionId[]" value="{{ $deduction->id }}" />
                                                    <input type="hidden" name="employeeSalaryDeductiontype[]" value="{{ $deduction->type }}" />

                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employeeSalaryDeductionDue[]" value="{{ $employeeSalaryStructureDeductionArray[$deduction->id] ?? 0 }}" class="employeeSalaryDeductionDrawn" style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="hidden" name="freezeAttendanceDeductionId[]" value="{{ $deduction->id }}" />
                                                    <input type="hidden" name="freezeAttendanceDeductiontype[]" value="{{ $deduction->type }}"  />

                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freezeAttendanceDeductionDrawn[]" value="{{ $freezeDeductionArray[$deduction->id] ?? 0 }}" class="freezeAttendanceDeductionDrawn" style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="hidden" name="suplimentryDeductionId[]" value="{{ $deduction->id }}" />
                                                    <input type="hidden" name="suplimentryDeductiontype[]" value="{{ $deduction->type }}" />

                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentryDeductionDifference[]" class="suplimentryDeductionDrawn suplimentryDeductionDifferencedd" value="{{ $suplimentryDeductionArray[$deduction->id] ?? 0 }}" style="width: 100px" />
                                                </td>
                                            </tr>

                                            @php
                                                $employeeSalaryDeductionAmount = $employeeSalaryStructureDeductionArray[$deduction->id] ?? 0;
                                                $freezeDeductionAmount = $freezeDeductionArray[$deduction->id] ?? 0;
                                                $suplimentryDeductionAmount = $suplimentryDeductionArray[$deduction->id] ?? 0;


                                                $totalDueDeduction += $employeeSalaryDeductionAmount;
                                                $totalDrawnDeduction += $freezeDeductionAmount;
                                                $totalDifferenceDeduction += $suplimentryDeductionAmount;
                                            @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-lg-6 col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Total Earnings</th>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employee_total_earning" class="totalEarningDue" value="{{ round($totalDueAllowance) }}" readonly style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freeze_total_earning" class="totalEarningDrawn" value="{{ round($totalDrawnAllowance) }}" readonly style="width: 100px" />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentary_total_earning" class="totalEarningDifference" value="{{ round($totalDifferenceAllowance) }}" readonly style="width: 100px" />
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Total Deductions</th>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employee_total_deduction" class="totalDeductionDue" value="{{ round($totalDueDeduction) }}" style="width: 100px" readonly />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freeze_total_deduction" class="totalDeductionDrawn" value="{{ round($totalDrawnDeduction) }}" style="width: 100px" readonly />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentary_total_deduction" class="totalDeductionDifference" value="{{ round($totalDifferenceDeduction) }}" style="width: 100px" readonly />
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="row mt-4">
                            <div class="col-lg-6 col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Total Net Payables</th>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="employee_net_salary" class="employee_net_salary" value="{{ round($suplimentaryEmployeeLeaveBill->employee_salary_net_salary) }}" style="width: 100px" readonly />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="freeze_net_salary" class="freeze_net_salary" value="{{ round($suplimentaryEmployeeLeaveBill->freeze_attendance_net_salary) }}" style="width: 100px" readonly />
                                                </td>
                                                <td>
                                                    <input type="number" step="0.01" min="0" placeholder="0.00" name="suplimentry_net_salary" class="suplimentry_net_salary" value="{{ round($suplimentaryEmployeeLeaveBill->net_salary) }}" style="width: 100px" readonly />
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @push('scripts')
        <script>
            function calculateTotalAllowance(){
                var employeeSalaryAllowanceDue = 0;
                $(".employeeSalaryAllowanceDue").each(function(counter) {
                    let allowance = parseFloat($(this).val() || 0); 
                    employeeSalaryAllowanceDue = employeeSalaryAllowanceDue + allowance;
                });
                let employeeSalaryBasicSalary = parseFloat($('.employeeSalaryBasicSalary').val() || 0);
                employeeSalaryAllowanceDue = employeeSalaryAllowanceDue + employeeSalaryBasicSalary;
                $('.totalEarningDue').val(employeeSalaryAllowanceDue);


                
                var freezeSalaryAllowanceDrawn = 0;
                $(".freezeAttendanceAllowanceDrawn").each(function(counter) {
                    let freeze = parseFloat($(this).val() || 0); 
                    freezeSalaryAllowanceDrawn = freezeSalaryAllowanceDrawn + freeze;
                });
                let freezeSalaryBasicSalary = parseFloat($('.freezeAttendanceBasicSalary').val() || 0);
                freezeSalaryAllowanceDrawn = freezeSalaryAllowanceDrawn + freezeSalaryBasicSalary;
                $('.totalEarningDrawn').val(freezeSalaryAllowanceDrawn);


                var suplimentrySalaryAllowanceDrawn = 0;
                $(".suplimentryAllowanceDrawn").each(function(counter) {
                    let suplimentry = parseFloat($(this).val() || 0); 
                    suplimentrySalaryAllowanceDrawn = suplimentrySalaryAllowanceDrawn + suplimentry;
                });
                let suplimentrySalaryBasicSalary = parseFloat($('.suplimentryBasicSalary').val() || 0);
                suplimentrySalaryAllowanceDrawn = suplimentrySalaryAllowanceDrawn + suplimentrySalaryBasicSalary;
                $('.totalEarningDifference').val(suplimentrySalaryAllowanceDrawn);


                var employeeSalaryDeductionDrawn = 0;
                $(".employeeSalaryDeductionDrawn").each(function(counter) {
                    let deduction = parseFloat($(this).val() || 0); 
                    employeeSalaryDeductionDrawn = employeeSalaryDeductionDrawn + deduction;
                });
                $('.totalDeductionDue').val(employeeSalaryDeductionDrawn);



                var freezeAttendanceDeductionDrawn = 0;
                $(".freezeAttendanceDeductionDrawn").each(function(counter) {
                    let freeze = parseFloat($(this).val() || 0); 
                    freezeAttendanceDeductionDrawn = freezeAttendanceDeductionDrawn + freeze;
                });
                $('.totalDeductionDrawn').val(freezeAttendanceDeductionDrawn);


                var suplimentryDeductionDrawn = 0;
                $(".suplimentryDeductionDrawn").each(function(counter) {
                    let suplimentry = parseFloat($(this).val() || 0); 
                    suplimentryDeductionDrawn = suplimentryDeductionDrawn + suplimentry;
                });
                $('.totalDeductionDifference').val(suplimentryDeductionDrawn);


                let employee_net_salary = employeeSalaryAllowanceDue - employeeSalaryDeductionDrawn;
                $('.employee_net_salary').val(employee_net_salary);

                
                let freeze_net_salary = freezeSalaryAllowanceDrawn - freezeAttendanceDeductionDrawn;
                $('.freeze_net_salary').val(freeze_net_salary);

                
                let suplimentry_net_salary = suplimentrySalaryAllowanceDrawn - suplimentryDeductionDrawn;
                $('.suplimentry_net_salary').val(suplimentry_net_salary);

            }

        </script>
    @endpush

</x-admin.layout>

<script>
    $(document).ready(function(){

        $('body').on('keyup', '.suplimentryDeductionDifferencedd', function(){
            calculateTotalAllowance();
        });
        
        $('body').on('keyup', '.freezeAttendanceBasicSalary, .employeeSalaryBasicSalary', function(){
            let employeeSalary = parseFloat($(this).closest('tr').find('.employeeSalaryBasicSalary').val() || 0)
            let freezeSalary = parseFloat($(this).closest('tr').find('.freezeAttendanceBasicSalary').val() || 0)
            let suplimentry = employeeSalary - freezeSalary;

            $(this).closest('tr').find('.suplimentryBasicSalary').val(suplimentry);
            calculateTotalAllowance();
        })
    });
    
</script>

<script>
    $(document).ready(function(){
        $('body').on('keyup', '.freezeAttendanceDeductionDrawn, .employeeSalaryDeductionDrawn', function(){
            let deductionDrawn = parseFloat($(this).closest('tr').find('input[name="freezeAttendanceDeductionDrawn[]"]').val() || 0);
            let deductionDue = parseFloat($(this).closest('tr').find('input[name="employeeSalaryDeductionDue[]"]').val() || 0);

            let difference = deductionDue - deductionDrawn;
            $(this).closest('tr').find('input[name="suplimentryDeductionDifference[]"]').val(difference)

            calculateTotalAllowance();
        });

        $('body').on('keyup', '.freezeAttendanceAllowanceDrawn, .employeeSalaryAllowanceDue', function(){
            let allowanceDrawn = parseFloat($(this).closest('tr').find('input[name="freezeAttendanceEarningDrawn[]"]').val() || 0);
            let allowanceDue = parseFloat($(this).closest('tr').find('input[name="employeeSalaryEarningDue[]"]').val() || 0);

            let difference = allowanceDue - allowanceDrawn;
            $(this).closest('tr').find('input[name="suplimentryEarningDifference[]"]').val(difference);

            calculateTotalAllowance();
        });
    })
</script>
