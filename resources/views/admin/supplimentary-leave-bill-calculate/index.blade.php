<x-admin.layout>
    <x-slot name="title">Supplimentary Leave Bill Calculate</x-slot>
    <x-slot name="heading">Supplimentary Leave Bill Calculate</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Bill No</th>
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Group</th>
                                    <th>Month</th>
                                    <th>Year</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('supplimentary-calculate.index') }}",
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'bill_no',
                            name: 'suplimentryLeaveBill.bill_no',
                            orderable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'emp_code',
                            name: 'emp_code',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'employee_name',
                            name: 'employee_name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'ward',
                            name: 'employee.ward.name',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'month',
                            name: 'month',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'year',
                            name: 'year',
                            orderable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            render: function(data, type, row){
                                let editUrl = "{{ route('supplimentary-calculate.edit', ':id') }}";
                                let viewUrl = "{{ route('supplimentary-calculate.show', ':id') }}";
                                let html = `
                                    {{-- <a href="${viewUrl.replace(':id', data)}" class="btn btn-primary px-2 py-1" title="View">
                                        View
                                    </a> --}}
                                    <a href="${editUrl.replace(':id', data)}" class="btn btn-warning px-2 py-1" title="Edit">
                                        Edit
                                    </a>`;
                                return html;
                            },
                        },
                    ]
                });
            });
        </script>
        <script>
            @if(Session::has('success'))
                swal({
                    title: "Successful!",
                    text: "{{ Session::get('success') }}",
                    icon: "success",
                    button: "OK",
                });
            @endif
        
            @if(Session::has('error'))
                swal({
                    title: "Error!",
                    text: "{{ Session::get('error') }}",
                    icon: "error",
                    button: "OK",
                });
            @endif
        </script>

    @endpush


</x-admin.layout>




