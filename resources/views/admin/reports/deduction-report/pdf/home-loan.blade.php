<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Loan</title>
    <style>
        table, td, th {
          border: 1px solid;
        }

        table {
          width: 100%;
          border-collapse: collapse;
        }
        thead {
            display: table-header-group;
            page-break-inside: avoid;
        }
        tbody {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
            page-break-after: auto;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            @include('admin.reports.deduction-report.pdf.common', ['rowspan' => 4])
            @php $lang = request()->lang; @endphp
            <tr style="padding: 10px; font-size:18px; background:#b8dbfd">
                <th>@if($lang == "en")Sr.No. @else अनु. क्र. @endif</th>
                <th>@if($lang == "en")Emp Code @else कर्मचारी ओळख @endif</th>
                <th>@if($lang == "en")Employee Name @else कर्मचारी नाव @endif</th>
                <th>@if($lang == "en")Home Loan @else गृहकर्ज @endif</th>
            </tr>
        </thead>
        <tbody style="text-align: center; font-size:17px">
            @php $count = 1;$total = 0; @endphp
            @foreach($deduction_reports as $deduction_report)
            @php
                $deductionIds = explode(',', $deduction_report->deduction_Id);
                $deductionAmount = explode(',', $deduction_report->deduction_Amt);
                $combineArray = array_combine($deductionIds, $deductionAmount);
            @endphp
            @if(!in_array($deduction_id, $deductionIds))
                @continue
            @endif
            <tr>
                <td>{{ ($lang == "en") ? $count++ : convertToMarathiNumber($count++) }}</td>
                <td>{{ ($lang == "en") ? $deduction_report->Emp_Code : convertToMarathiNumber($deduction_report->Emp_Code) }}</td>
                <td>{{ ($lang == "en") ? $deduction_report?->employee?->fname.' '. $deduction_report?->employee?->mname. ' '.$deduction_report?->employee?->lname : $deduction_report?->employee?->m_fname.' '. $deduction_report?->employee?->m_mname. ' '.$deduction_report?->employee?->m_lname }}</td>
                <td>{{ ($lang == "en") ? $combineArray[$deduction_id] ?? 0 : convertToMarathiNumber($combineArray[$deduction_id] ?? 0) }}</td>
                @php $total = $total + $combineArray[$deduction_id] ?? 0; @endphp
            </tr>
            @endforeach
            <tr>
                <th style="font-size: 18x;">@if($lang == "en")Total @else एकूण @endif</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $count-1 : convertToMarathiNumber($count - 1) }}</th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $total : convertToMarathiNumber($total) }}</th>
            </tr>
        </tbody>
    </table>
</body>
</html>
