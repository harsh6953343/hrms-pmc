<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Maharashtra Labour Welfare Board Employee Deduction</title>
        <style>
            table, td, th {
                border: 1px solid;
            }

            table {
                width: 100%;
                border-collapse: collapse;
            }
            thead {
                display: table-header-group;
                page-break-inside: avoid;
            }
            tbody {
                display: table-row-group;
            }
            tr {
                page-break-inside: avoid;
                page-break-after: auto;
            }
        </style>
    </head>
    <body>
        <table>
            <thead>
                @include('admin.reports.deduction-report.pdf.common', ['rowspan' => 5])
                @php $lang = request()->lang; @endphp
                <tr style="padding: 10px; font-size:18px; background:#b8dbfd">
                    <th>@if($lang == "en")Sr.No. @else अनु. क्र. @endif</th>
                    <th>@if($lang == "en")Emp Code @else कर्मचारी ओळख @endif</th>
                    <th>@if($lang == "en")Employee Name @else कर्मचारी नाव @endif</th>
                    <th>@if($lang == "en")Maharashtra Labor Welfare Board Staff Deduction @else महाराष्ट्र कामगार कल्याण मंडळ कर्मचारी कपात @endif</th>
                    <th>@if($lang == "en")Maharashtra Labor Welfare Board MP Cut @else महाराष्ट्र कामगार कल्याण मंडळ मपा कपात @endif</th>
                </tr>
            </thead>
            <tbody style="text-align: center; font-size:17px">
                @php $count = 1;$total = 0; $totalMaharastraWelfaceEmployee = 0; $totalMaharastraWelfaceCorporation = 0; @endphp
                @foreach($deduction_reports as $deduction_report)
                @php
                    $deductionIds = explode(',', $deduction_report->deduction_Id);
                    $deductionAmount = explode(',', $deduction_report->deduction_Amt);
                    $combineArray = array_combine($deductionIds, $deductionAmount);
                    $maharastraWelfaceCorporation = $combineArray[19] ?? 0;
                    $maharastraWelfaceEmployee = $combineArray[18] ?? 0;
                @endphp
                @if(!($maharastraWelfaceCorporation > 0 || $maharastraWelfaceEmployee > 0))
                    @continue
                @endif
                <tr>
                    <td>{{ ($lang == "en") ? $count++ : convertToMarathiNumber($count++) }}</td>
                    <td>{{ ($lang == "en") ? $deduction_report->Emp_Code : convertToMarathiNumber($deduction_report->Emp_Code) }}</td>
                    <td>{{ ($lang == "en") ? $deduction_report?->employee?->fname.' '. $deduction_report?->employee?->mname. ' '.$deduction_report?->employee?->lname : $deduction_report?->employee?->m_fname.' '. $deduction_report?->employee?->m_mname. ' '.$deduction_report?->employee?->m_lname }}</td>
                    <td>{{ ($lang == "en") ? $maharastraWelfaceEmployee : convertToMarathiNumber($maharastraWelfaceEmployee) }}</td>
                    <td>{{ ($lang == "en") ? $maharastraWelfaceCorporation : convertToMarathiNumber($maharastraWelfaceCorporation) }}</td>
                    @php
                        $totalMaharastraWelfaceEmployee += $maharastraWelfaceEmployee;
                        $totalMaharastraWelfaceCorporation += $maharastraWelfaceCorporation;
                    @endphp
                </tr>
                @endforeach
                <tr>
                    <th style="font-size: 18x;">@if($lang == "en")Total @else एकूण @endif</th>
                    <th style="font-size: 18x;">{{ ($lang == "en") ? $count - 1 : convertToMarathiNumber($count - 1) }}</th>
                    <th style="font-size: 18x;"></th>
                    <th style="font-size: 18x;">{{ ($lang == "en") ? $totalMaharastraWelfaceEmployee : convertToMarathiNumber($totalMaharastraWelfaceEmployee) }}</th>
                    <th style="font-size: 18x;">{{ ($lang == "en") ? $totalMaharastraWelfaceCorporation : convertToMarathiNumber($totalMaharastraWelfaceCorporation) }}</th>
                </tr>
            </tbody>
        </table>
    </body>
</html>
