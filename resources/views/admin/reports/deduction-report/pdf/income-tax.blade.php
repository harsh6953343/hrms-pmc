<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Income Tax</title>
    <style>
        table, td, th {
          border: 1px solid;
        }

        table {
          width: 100%;
          border-collapse: collapse;
        }
        thead {
            display: table-header-group;
            page-break-inside: avoid;
        }
        tbody {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
            page-break-after: auto;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            @include('admin.reports.deduction-report.pdf.common', ['rowspan' => 8])
            @php $lang = request()->lang; @endphp
            <tr style="padding: 10px; font-size:18px; background:#b8dbfd">
                <th>@if($lang == "en")Sr.No. @else अनु. क्र. @endif</th>
                <th>@if($lang == "en")Emp Code @else कर्मचारी ओळख @endif</th>
                <th>@if($lang == "en")Employee Name @else कर्मचारी नाव @endif</th>
                <th>@if($lang == "en")Pan No @else पॅन क्र @endif</th>
                <th>@if($lang == "en")Total Salary @else एकूण पगार @endif</th>
                <th>@if($lang == "en")Income tax @else आयकर @endif</th>
                <th>@if($lang == "en")Education Cess @else शिक्षण उपकर @endif</th>
                <th>@if($lang == "en")Total TDS @else एकूण TDS @endif</th>
            </tr>
        </thead>
        <tbody style="text-align: center; font-size:17px">
            @php $count = 1;$totalGrossAmount = 0; $totalEducationCessAmount = 0; $totalTdsAmount = 0; $totalIncomeAmount = 0; @endphp
            @foreach($deduction_reports as $deduction_report)
            @php
                $deductionIds = explode(',', $deduction_report->deduction_Id);
                $deductionAmount = explode(',', $deduction_report->deduction_Amt);
                $combineArray = array_combine($deductionIds, $deductionAmount);

                $incomeTaxAmount = ($deduction_report?->employee?->ward_id == "5") ? $combineArray[23] ?? 0 : $combineArray[2] ?? 0;

                $educationCess = round(($incomeTaxAmount * 4 ) / 100);

                $totalGross = $deduction_report->earned_basic + $deduction_report->leave_pay + $deduction_report->total_allowance + $deduction_report->corporation_share_da;
            @endphp
            @if(!in_array($deduction_id, $deductionIds))
                @continue
            @endif

            <tr>
                <td>{{ ($lang == "en") ? $count++ : convertToMarathiNumber($count++) }}</td>
                <td>{{ ($lang == "en") ? $deduction_report->Emp_Code : convertToMarathiNumber($deduction_report->Emp_Code) }}</td>
                <td>{{ ($lang == "en") ? $deduction_report?->employee?->fname.' '. $deduction_report?->employee?->mname. ' '.$deduction_report?->employee?->lname : $deduction_report?->employee?->m_fname.' '. $deduction_report?->employee?->m_mname. ' '.$deduction_report?->employee?->m_lname }}</td>
                <td>{{ $deduction_report?->employee?->pan }}</td>
                <td>{{ ($lang == "en") ? $totalGross : convertToMarathiNumber($totalGross) }}</td>
                <td>{{ ($lang == "en") ? $incomeTaxAmount : convertToMarathiNumber($incomeTaxAmount) }}</td>
                <td>{{ ($lang == "en") ? $educationCess : convertToMarathiNumber($educationCess) }}</td>
                <td>{{ ($lang == "en") ? $incomeTaxAmount + $educationCess : convertToMarathiNumber($incomeTaxAmount + $educationCess) }}</td>
                @php
                    $totalGrossAmount += $totalGross;
                    $totalIncomeAmount += $incomeTaxAmount;
                    $totalEducationCessAmount += $educationCess;
                    $totalTdsAmount = $totalTdsAmount + $incomeTaxAmount + $educationCess;
                @endphp
            </tr>
            @endforeach
            <tr>
                <th style="font-size: 18x;">@if($lang == "en")Total @else एकूण @endif</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $count - 1 : convertToMarathiNumber($count - 1) }}</th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $totalGrossAmount : convertToMarathiNumber($totalGrossAmount) }}</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $totalIncomeAmount : convertToMarathiNumber($totalIncomeAmount) }}</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $totalEducationCessAmount : convertToMarathiNumber($totalEducationCessAmount) }}</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $totalTdsAmount : convertToMarathiNumber($totalTdsAmount) }}</th>
            </tr>
        </tbody>
    </table>
</body>
</html>
