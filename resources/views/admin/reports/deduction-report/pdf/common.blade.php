<tr>
    @php
        $lang = request()->lang;
        if($lang == 'en'){
            $monthName = ['1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'];
            $year = date('Y', strtotime($to_date));
        }else{
            $monthName = ['1' => 'जानेवारी', '2' => 'फेब्रुवारी', '3' => 'मार्च', '4' => 'एप्रिल', '5' => 'मे', '6' => 'जून', '7' => 'जुलै', '8' => 'ऑगस्ट', '9' => 'सप्टेंबर', '10' => 'ऑक्टोबर', '11' => 'नोहेंबर', '12' => 'डिसेंबर'];
            $year = convertToMarathiNumber(date('Y', strtotime($to_date)));
        }
    @endphp
    <th colspan="{{ $rowspan }}" style="padding: 8px">
        <span style="font-size:28px;">@if($lang == 'en')Panvel Municipal Corporation @else पनवेल महानगरपालिका @endif</span> <br> <span style="font-size:15px;">@if($lang == 'en')Government of Maharashtra Month @else महाराष्ट्र शासन माहे @endif - {{ $monthName[$month] ?? '' }}-{{ $year }}</span>
    </th>
</tr>