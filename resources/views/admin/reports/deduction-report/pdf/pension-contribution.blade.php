<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pension Contribution</title>
    <style>
        table, td, th {
          border: 1px solid;
        }

        table {
          width: 100%;
          border-collapse: collapse;
        }
        thead {
            display: table-header-group;
            page-break-inside: avoid;
        }
        tbody {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
            page-break-after: auto;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            @include('admin.reports.deduction-report.pdf.common', ['rowspan' => 6])
            @php $lang = request()->lang; @endphp
            <tr style="padding: 10px; font-size:18px; background:#b8dbfd">
                <th>@if($lang == "en")Sr.No. @else अनु. क्र. @endif</th>
                <th>@if($lang == "en") IFSC Code @else IFSC कोड @endif</th>
                <th>@if($lang == "en")Account No @else खाते क्रमांक @endif</th>
                <th>@if($lang == "en")Pensioner Id @else पेन्शनर आयडी @endif</th>
                <th>@if($lang == "en")Employee Name @else कर्मचारी नाव @endif</th>
                <th>@if($lang == "en")Net payable @else निव्वळ देय @endif</th>
            </tr>
        </thead>
        <tbody style="text-align: center; font-size:17px">
            @php $count = 1; $totalNetSalary = 0; @endphp
            @foreach($deduction_reports as $deduction_report)
            @php
                $deductionIds = explode(',', $deduction_report->deduction_Id);
                $deductionAmount = explode(',', $deduction_report->deduction_Amt);
                $combineArray = array_combine($deductionIds, $deductionAmount);
            @endphp
            @if(!in_array($deduction_id, $deductionIds))
                @continue
            @endif

            <tr>
                <td>{{ ($lang == "en") ? : convertToMarathiNumber($count++) }}</td>
                <td>{{ ($deduction_report?->ifsc_code) ? $deduction_report?->ifsc_code : $deduction_report?->employee?->ifsc }}</td>
                <td>{{ ($deduction_report?->account_no) ? $deduction_report?->account_no : $deduction_report?->employee?->account_no }}</td>
                <td>{{ ($lang == "en") ? $deduction_report->pension_id : convertToMarathiNumber($deduction_report->pension_id) }}</td>
                <td>{{ ($lang == "en") ? $deduction_report?->employee_name : $deduction_report?->employee_name }}</td>
                <td>{{ ($lang == "en") ? $deduction_report->net_salary : convertToMarathiNumber($deduction_report->net_salary) }}</td>
                @php
                    $netSalary = ($deduction_report->net_salary) ? $deduction_report->net_salary : 0;
                    $totalNetSalary += $deduction_report->net_salary;
                @endphp
            </tr>
            @endforeach
            <tr>
                <th style="font-size: 18x;">एकूण</th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $count - 1 : convertToMarathiNumber($count - 1) }}</th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;"></th>
                <th style="font-size: 18x;">{{ ($lang == "en") ? $totalNetSalary : convertToMarathiNumber($totalNetSalary) }}</th>
            </tr>
        </tbody>
    </table>
</body>
</html>
