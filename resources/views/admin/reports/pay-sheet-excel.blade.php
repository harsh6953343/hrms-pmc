<x-admin.layout>
    <x-slot name="title">Pay Sheet Report</x-slot>
    <x-slot name="heading">Pay Sheet Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('pay-sheet-excel.store') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data" method="POST">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-3 ward_div">
                                <label class="col-form-label" for="ward_id">Select Ward <span class="text-danger">*</span></label>
                                <select class="form-select ward_id" name="ward_id" required>
                                    <option value="">--Select Ward--</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-3 department_div d-none">
                                <label class="col-form-label" for="department_id">Select Department <span class="text-danger">*</span></label>
                                <select class="form-select department_id" name="department_id">
                                    <option value="">--Select Department--</option>
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>
                            {{-- <div class="col-md-3">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <select class="form-select" id="department" name="department">
                                    <option value="">Select Department</option>
                                    <option value="all">All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                @error('department')
                                <span class="text-danger invalid department_err">{{ $message }}</span>
                                @enderror
                            </div> --}}

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month" required>
                                    <option value="">Select Month</option>
                                    <option value="1"  >January</option>
                                    <option value="2" >February</option>
                                    <option value="3" >March</option>
                                    <option value="4" >April</option>
                                    <option value="5" >May</option>
                                    <option value="6" >June</option>
                                    <option value="7" >July</option>
                                    <option value="8" >August</option>
                                    <option value="9" >September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <span class="text-danger invalid month_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ old('from_date') }}">
                                @error('from_date')
                                <span class="text-danger invalid from_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ old('to_date') }}">
                                @error('to_date')
                                <span class="text-danger invalid to_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit1">Download Excel</button>
                        <a href="{{ route('pay-sheet-excel.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

            </div>
        </div>
    </div>

    @unless($authUser->hasRole(['Pension HOD']) || $authUser->hasRole(['PF HOD']))
    <div id="salaryModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="salaryModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="salaryModalLabel">Net Salary in Negative</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <table>
                        <table id="" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Name</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Month</th>
                                    <th>Salary</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_salaries as $employee_salary)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_salary->emp_name }}</td>
                                        <td>{{ $employee_salary?->employee?->ward?->name }}</td>
                                        <td>{{ $employee_salary?->employee?->department?->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($employee_salary->month)->format('F') }}</td>
                                        <td>{{ $employee_salary->net_salary }}</td>
                                    </tr>
                                @endforeach
                        </table>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endunless

</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });

    $(".ward_id").on("change", function(e) {
        var ward_id = $(this).val();

        if(ward_id != ''){
            var url = "{{ route('fetch-department', ':ward_id') }}";

            $.ajax({
                url: url.replace(":ward_id", ward_id),
                type: "GET",
                data: {
                    _method: "GET",
                    _token: "{{ csrf_token() }}",
                },
                success: function(data, textStatus, jqXHR) {
                    if (!data.error && !data.error2) {
                        $(".department_id").html(data.deptHtml);
                        $('.department_div').removeClass('d-none');
                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }else{
            $('.department_div').addClass('d-none');
        }
    });

    $(document).ready(function() {
        //Show modal having employees whose net salary is negative
        var employee_salarys = <?php echo json_encode($employee_salaries); ?>;
        if (employee_salarys.length > 0) {
            $('#salaryModal').modal('show');
        }
    });
    </script>
