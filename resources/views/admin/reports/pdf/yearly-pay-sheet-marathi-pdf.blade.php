@php
    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yearly Pay Sheet Report</title>
    <style>
        body {
            font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
        }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
            text-align: left;
            margin: 10px 0 5px 350px;
            font-size: 24px;
        }

        .section-description {
            text-align: left;
            margin: 10px 0 5px 310px;
            font-size: 16px;
        }

        table,
        th,
        td {
            font-size: 12px;
            border: 1px solid rgb(177, 173, 173);
            border-collapse: collapse;

        }

        table,
        th {
            font-weight: 5px;
            font-size: 15px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,
        td {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer {
            background-color: lightgray !important;
        }

        .signature {
            background-color: white;
            border: none;
            padding: 5px;
            /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
            page-break-after: always;
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-row-group
        }

        tr {
            page-break-inside: avoid
        }
    </style>
</head>

<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:left;">
                    @if (isset($base64Logo))
                        <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="100">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    {{-- @if (!empty($freezeAttendances) && isset($freezeAttendances[0])) --}}
                    <h5 class="section-description">
                        आर्थिक वर्ष - {{ $startYear }} - {{ $endYear }}
                        कर्मचारी - {{ $employee->employee_id }} - {{ $employee->full_name }}
                    </h5>
                    {{-- @endif --}}
                </td>
            </tr>
        </thead>
    </table>


    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">अनु. क्र.</th>
                <th class="th">पेमेंट प्रकार</th>
                <th class="th">माहे</th>
                <th class="th">मूळ वेतन</th>
                <th class="th">अर्जित वेतन</th>
                <th class="th">महागाई भत्ता</th>
                <th class="th">घर भाडेभत्ता</th>
                <th class="th">शहर भरपाई देणारा भत्ता</th>
                <th class="th">प्रतिनियुक्ती भत्ता</th>
                <th class="th">अंनिवयो - मपा</th>
                <th class="th">एकूण वेतन व भत्ते</th>
                <th class="th">भ. नि. नि. वर्गणी</th>
                <th class="th">अंनिवयो क.</th>
                <th class="th">अंनिवयो मपा</th>
                <th class="th">व्यावसायिक कर</th>
                <th class="th">आयकर</th>
                <th class="th">गट. विमा. यो.</th>
                <th class="th">मुद्रांक शुल्क</th>
                <th class="th">एकूण कपात</th>
                <th class="th">निव्वल देय्य</th>
            </tr>
        </thead>

        <tbody>
            @php
                $grandAllowance = array_fill_keys([1, 2, 4, 13, 6], 0);
                $grandDeduction = array_fill_keys([5, 7, 8, 1, 2, 21, 22], 0);
                $grandBasic = 0;
                $iteration = 1;
                $tillMay7thDiff = collect($tillMay7thDiff);
                $tillMayDa = collect($tillMayDa);
                $tillMaySalary = collect($tillMaySalary);
            @endphp


            @foreach ($tillMaySalary as $maySalary)
                @php
                    $basic = round($maySalary->Actual_Basic);
                    $earnedBasic = round($maySalary->Earned_Basic);
                    $leavePay = round($maySalary->LEAVEPAY);
                    $grandAllowance[1] += round($maySalary->DEARNESS_ALLOWANCE);
                    $grandAllowance[2] += round($maySalary->HOUSERENT_ALLOWANCE);
                    $grandAllowance[4] += round($maySalary->CITY_COMPENSATORY_ALLOWANCE);
                    $grandAllowance[13] += round($maySalary->DEPUTATION_ALLOWANCE);
                    $grandAllowance[6] += round($maySalary->DCPSCorpAllw);

                    $grandDeduction[5] += round($maySalary->GENERAL_PROVIDENT_FUND);
                    $grandDeduction[7] += round($maySalary->DCPS_I_EMPLOYEE_CONTRIBUTION);
                    $grandDeduction[8] += round($maySalary->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION);
                    $grandDeduction[2] += round($maySalary->INCOME_TAX);
                    $grandDeduction[21] += round($maySalary->GROUP_INSURANCE_SCHEME);

                    $tillMayearnings = round($earnedBasic + $leavePay) + round($maySalary->DEARNESS_ALLOWANCE) + round($maySalary->HOUSERENT_ALLOWANCE) + round($maySalary->CITY_COMPENSATORY_ALLOWANCE) + round($maySalary->DEPUTATION_ALLOWANCE) + round($maySalary->DCPSCorpAllw);
                    $tillMayDeductions = round($maySalary->GENERAL_PROVIDENT_FUND) + round($maySalary->DCPS_I_EMPLOYEE_CONTRIBUTION) + round($maySalary->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION) + round($maySalary->INCOME_TAX) + round($maySalary->GROUP_INSURANCE_SCHEME);
                @endphp
                <tr>
                    <td> {{ convertToMarathiNumber($iteration++) }} </td>
                    <td> PAYROLL ACTUAL</td>
                    <td> {{ __('messages.' . Carbon::createFromFormat('m', $maySalary->month)->format('F')) . '-' . 2024 }} </td>
                    <td> {{ round($basic) }} </td>
                    <td> {{ round($earnedBasic + $leavePay) }} </td>
                    <td> {{ $maySalary->DEARNESS_ALLOWANCE }} </td>
                    <td> {{ round($maySalary->HOUSERENT_ALLOWANCE) }} </td>
                    <td> {{ round($maySalary->CITY_COMPENSATORY_ALLOWANCE) }} </td>
                    <td> {{ round($maySalary->DEPUTATION_ALLOWANCE) }} </td>
                    <td> {{ round($maySalary->DCPSCorpAllw) }} </td>
                    <td> {{ $tillMayearnings }} </td>
                    <td> {{ round($maySalary->GENERAL_PROVIDENT_FUND) }} </td>
                    <td> {{ round($maySalary->DCPS_I_EMPLOYEE_CONTRIBUTION) }} </td>
                    <td> {{ round($maySalary->DCPS_ICORPORATIONCONTRIBUTIONDEDUCTION) }} </td>
                    <td> 0 </td>
                    <td> {{ round($maySalary->INCOME_TAX) }} </td>
                    <td> {{ round($maySalary->GROUP_INSURANCE_SCHEME) }} </td>
                    <td> 0 </td>
                    <td> {{ $tillMayDeductions }} </td>
                    <td> {{ round($tillMayearnings - $tillMayDeductions) }} </td>
                </tr>
            @endforeach
            @foreach ($salaries as $salary)
                @php
                    $allowanceIds = explode(',', $salary->allowance_Id);
                    $allowanceAmounts = explode(',', $salary->allowance_Amt);
                    $allowanceCollection = collect(array_combine($allowanceIds, $allowanceAmounts));
                    $allowanceCollection = $allowanceCollection->only([1, 2, 4, 13]);
                    $grandAllowance[1] += isset($allowanceCollection[1]) ? round($allowanceCollection[1]) : 0;
                    $grandAllowance[2] += isset($allowanceCollection[2]) ? round($allowanceCollection[2]) : 0;
                    $grandAllowance[4] += isset($allowanceCollection[4]) ? round($allowanceCollection[4]) : 0;
                    $grandAllowance[13] += isset($allowanceCollection[13]) ? round($allowanceCollection[13]) : 0;
                    $grandAllowance[6] += round($salary->corporation_share_da);

                    $deductionIds = explode(',', $salary->deduction_Id);
                    $deductionAmounts = explode(',', $salary->deduction_Amt);
                    $deductionCollection = collect(array_combine($deductionIds, $deductionAmounts));
                    $deductionCollection = $deductionCollection->only([5, 7, 8, 1, 2, 21]);
                    $grandDeduction[5] += isset($deductionCollection[5]) ? round($deductionCollection[5]) : 0;
                    $grandDeduction[7] += isset($deductionCollection[7]) ? round($deductionCollection[7]) : 0;
                    $grandDeduction[8] += isset($deductionCollection[8]) ? round($deductionCollection[8]) : 0;
                    $grandDeduction[1] += isset($deductionCollection[1]) ? round($deductionCollection[1]) : 0;
                    $grandDeduction[2] += isset($deductionCollection[2]) ? round($deductionCollection[2]) : 0;
                    $grandDeduction[21] += isset($deductionCollection[21]) ? round($deductionCollection[21]) : 0;
                    $grandDeduction[22] += $salary->stamp_duty;
                @endphp
                <tr>
                    <td>{{ convertToMarathiNumber($iteration++) }}</td>
                    <td>PAYROLL ACTUAL</td>
                    <td>{{ __('messages.' . Carbon::createFromFormat('m', $salary->month)->format('F')) . '-' . substr($salary->from_date, 0, 4) }}</td>
                    <td>{{ round($salary->basic_salary) }}</td>
                    <td>
                        {{ round($salary->earned_basic + $salary->leave_pay) }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[1]) ? round($allowanceCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[2]) ? round($allowanceCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[4]) ? round($allowanceCollection[4]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[13]) ? round($allowanceCollection[13]) : 0 }}
                    </td>
                    <td>
                        {{-- {{ isset($allowanceCollection[6]) ? round($allowanceCollection[6]) : 0 }} --}}
                        {{ round($salary->corporation_share_da) }}
                    </td>
                    <td>
                        {{ round($allowanceCollection->filter()->sum() + $salary->corporation_share_da + $salary->earned_basic + $salary->leave_pay) }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[5]) ? round($deductionCollection[5]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[7]) ? round($deductionCollection[7]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[8]) ? round($deductionCollection[8]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[1]) ? round($deductionCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[2]) ? round($deductionCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[21]) ? round($deductionCollection[21]) : 0 }}
                    </td>
                    <td>
                        {{ $salary->stamp_duty }}
                    </td>
                    <td>
                        {{-- {{ isset($deductionCollection[22]) ? round($deductionCollection[22]) : 0 }} --}}
                        @php
                            $allIncome = $allowanceCollection->filter()->sum() + $salary->corporation_share_da + $salary->earned_basic + $salary->leave_pay;
                            $allDeduction = $salary->stamp_duty + $deductionCollection->filter()->sum();
                        @endphp
                        {{ round($allDeduction) }}
                    </td>
                    <td>
                        {{ round($allIncome - $allDeduction) }}
                    </td>
                </tr>
            @endforeach


            @foreach ($tillMaySupplementary as $tillMaySupp)
                @php
                    $tillMaySupp = collect($tillMaySupp);
                    $month = explode('-', $tillMaySupp['Period'])[0];
                    $month = Carbon::createFromFormat('M', $month)->format('F');

                    $basic = $tillMayDa->where('Period', $tillMaySupp['Period']);
                    $basic = round(isset($basic[0]) ? $basic[0]->da : 0);
                    $dcpsemployee = $tillMayDa->where('Period', $tillMaySupp['Period']);
                    $dcpsemployee = round(isset($dcpsemployee[0]) ? $dcpsemployee[0]->dcpsemployee : 0);
                    $dcpsemployer = $tillMayDa->where('Period', $tillMaySupp['Period']);
                    $dcpsemployer = round(isset($dcpsemployer[0]) ? $dcpsemployer[0]->dcpsemployer : 0);

                    $grandBasic += $basic;
                    $grandAllowance[6] += round($tillMaySupp['suppbill']);
                    $grandDeduction[5] += $tillMaySupp['gpf'];
                    $grandDeduction[7] += $tillMaySupp['dcpsemployee'];
                    $grandDeduction[7] += $dcpsemployee;
                    $grandDeduction[8] += $tillMaySupp['dcpsemployer'];
                    $grandDeduction[8] += $dcpsemployer;

                    $showableAllowance = round($tillMaySupp['suppbill'] + $basic);
                    $showableDeductions = round($tillMaySupp['gpf'] + ($tillMaySupp['dcpsemployee'] + $dcpsemployee) + ($tillMaySupp['dcpsemployer'] + $dcpsemployer));
                @endphp
                <tr>
                    <td>{{ convertToMarathiNumber($iteration++) }}</td>
                    <td>पुरवणी बिल</td>
                    <td>{{ __('messages.' . $month) . '-' . 2024 }}</td>
                    <td>0</td>
                    <td>{{ $basic }}</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>{{ round($tillMaySupp['suppbill']) }}</td>
                    <td>{{ round($tillMaySupp['suppbill'] + $basic) }}</td>

                    <td>{{ $tillMaySupp['gpf'] ?? 0 }}</td>
                    <td>{{ $tillMaySupp['dcpsemployee'] ?? 0 }}</td>
                    <td>{{ $tillMaySupp['dcpsemployer'] ?? 0 }}</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>{{ $showableDeductions ?? 0 }}</td>
                    <td>{{ $showableAllowance - $showableDeductions }}</td>
                </tr>
            @endforeach
            @foreach ($supplementary as $monthlySupplymentary)
                @php
                    $supplAllowIds = explode(',', $monthlySupplymentary->allowance_id);
                    $supplAllowAmounts = explode(',', $monthlySupplymentary->allowance_amount);
                    $supplAllowanceCollection = collect(array_combine($supplAllowIds, $supplAllowAmounts));
                    $supplAllowanceCollection = $supplAllowanceCollection->only([1, 2, 4, 13, 6]);
                    $grandAllowance[1] += isset($allowanceCollection[1]) ? round($allowanceCollection[1]) : 0;
                    $grandAllowance[2] += isset($allowanceCollection[2]) ? round($allowanceCollection[2]) : 0;
                    $grandAllowance[4] += isset($allowanceCollection[4]) ? round($allowanceCollection[4]) : 0;
                    $grandAllowance[13] += isset($allowanceCollection[13]) ? round($allowanceCollection[13]) : 0;
                    $grandAllowance[6] += isset($allowanceCollection[6]) ? round($allowanceCollection[6]) : 0;

                    $supplDedIds = explode(',', $monthlySupplymentary->deduction_id);
                    $supplDedAmounts = explode(',', $monthlySupplymentary->deduction_amount);
                    $supplDeductionCollection = collect(array_combine($supplDedIds, $supplDedAmounts));
                    $supplDeductionCollection = $supplDeductionCollection->only([5, 7, 8, 1, 2, 21, 22]);
                    $grandDeduction[5] += isset($supplDeductionCollection[5]) ? round($supplDeductionCollection[5]) : 0;
                    $grandDeduction[7] += isset($supplDeductionCollection[7]) ? round($supplDeductionCollection[7]) : 0;
                    $grandDeduction[8] += isset($supplDeductionCollection[8]) ? round($supplDeductionCollection[8]) : 0;
                    $grandDeduction[1] += isset($supplDeductionCollection[1]) ? round($supplDeductionCollection[1]) : 0;
                    $grandDeduction[2] += isset($supplDeductionCollection[2]) ? round($supplDeductionCollection[2]) : 0;
                    $grandDeduction[21] += isset($supplDeductionCollection[21]) ? round($supplDeductionCollection[21]) : 0;
                    $grandDeduction[22] += isset($supplDeductionCollection[22]) ? round($supplDeductionCollection[22]) : 0;
                @endphp
                <tr>
                    <td>{{ convertToMarathiNumber($iteration++) }}</td>
                    <td>पुरवणी बिल</td>
                    <td>{{ __('messages.' . Carbon::createFromFormat('m', substr($monthlySupplymentary->month, 5))->format('F')) . '-' . substr($monthlySupplymentary->month, 0, 4) }}</td>
                    <td>
                        {{ round($monthlySupplymentary->basic_salary) }}
                    </td>
                    <td>
                        0
                    </td>
                    <td>
                        {{ isset($supplAllowanceCollection[1]) ? round($supplAllowanceCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplAllowanceCollection[2]) ? round($supplAllowanceCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplAllowanceCollection[4]) ? round($supplAllowanceCollection[4]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplAllowanceCollection[13]) ? round($supplAllowanceCollection[13]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplAllowanceCollection[6]) ? round($supplAllowanceCollection[6]) : 0 }}
                    </td>
                    <td>
                        {{ round($supplAllowanceCollection->filter()->sum() + $monthlySupplymentary->basic_salary) }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[5]) ? round($supplDeductionCollection[5]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[7]) ? round($supplDeductionCollection[7]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[8]) ? round($supplDeductionCollection[8]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[1]) ? round($supplDeductionCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[2]) ? round($supplDeductionCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[21]) ? round($supplDeductionCollection[21]) : 0 }}
                    </td>
                    <td>
                        {{ isset($supplDeductionCollection[22]) ? round($supplDeductionCollection[22]) : 0 }}
                    </td>
                    <td>
                        {{ round($supplDeductionCollection->filter()->sum()) }}
                    </td>
                    <td>
                        {{ round($supplAllowanceCollection->filter()->sum() + $monthlySupplymentary->basic_salary) - round($supplDeductionCollection->filter()->sum()) }}
                    </td>
                </tr>
            @endforeach

            @if ($tillMay7thDiff?->isNotEmpty())
                <tr>
                    <td>{{ convertToMarathiNumber($iteration++) }}</td>
                    <td>७व्या वेतनाचा ५वा हप्ता</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        {{ $tillMay7thDiff->sum('fifth_installment') }}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
        </tbody>

        <tfoot>
            <tr>
                @php
                    $totalIncome = $grandBasic + ($tillMay7thDiff?->isNotEmpty() ? round(collect($tillMay7thDiff)->sum('fifth_installment')) : 0) + round($salaries->sum('earned_basic') + $salaries->sum('leave_pay')) + round($tillMaySalary->sum('Earned_Basic') + $tillMaySalary->sum('LEAVEPAY')) + ($grandAllowance[1] + $grandAllowance[2] + $grandAllowance[4] + $grandAllowance[13] + $grandAllowance[6]);
                    $totalDeduction = $grandDeduction[5] + $grandDeduction[7] + $grandDeduction[8] + $grandDeduction[1] + $grandDeduction[2] + $grandDeduction[21] + $grandDeduction[22];
                @endphp
                <th class="table-footer" colspan="4" style="text-align: center; font-size:22px;">
                    <b> एकूण </b>
                </th>
                <th class="table-footer">
                    {{ round($salaries->sum('earned_basic') + $salaries->sum('leave_pay')) }}
                </th>
                <th class="table-footer">
                    {{ $grandAllowance[1] }}
                </th>
                <th class="table-footer">
                    {{ $grandAllowance[2] }}
                </th>
                <th class="table-footer">
                    {{ $grandAllowance[4] }}
                </th>
                <th class="table-footer">
                    {{ $grandAllowance[13] }}
                </th>
                <th class="table-footer">
                    {{ $grandAllowance[6] }}
                </th>
                <th class="table-footer">
                    {{ $totalIncome }}
                </th>

                <th class="table-footer">
                    {{ $grandDeduction[5] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[7] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[8] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[1] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[2] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[21] }}
                </th>
                <th class="table-footer">
                    {{ $grandDeduction[22] }}
                </th>
                <th class="table-footer">
                    {{ $totalDeduction }}
                </th>
                <th class="table-footer">
                    {{ round($totalIncome - $totalDeduction) }}
                </th>
            </tr>
        </tfoot>
    </table>
    <div class="page-break"></div>


    <table style="width: 100%; margin-top:8%;">
        <thead>
            <tr>
                <th>कमाई</th>
                <th>रक्कम</th>
                <th>वजावट</th>
                <th>रक्कम</th>
                {{-- <th>बँकेत वजावट</th>
                <th>रक्कम</th> --}}
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    अर्जित वेतन <br> <br>

                    महागाई भत्ता @53% <br> <br>

                    घर भाडेभत्ता @30% <br> <br>

                    शहर भरपाई देणारा भत्ता <br> <br>

                    प्रतिनियुक्ति भत्ता <br> <br>

                    सण आगाऊ <br> <br>

                    ७व्या वेतनाचा ५वा हप्ता <br> <br>

                </td>
                <td>
                    <b> {{ round($salaries->sum('earned_basic') + $salaries->sum('leave_pay')) }}</b> <br> <br>

                    <b> {{ round($salaries->sum('earned_basic') + $salaries->sum('leave_pay')) }}</b> <br> <br>

                    <b> {{ $grandAllowance[1] }} </b> <br> <br>

                    <b> {{ $grandAllowance[2] }} </b> <br> <br>

                    <b> {{ $grandAllowance[4] }} </b> <br> <br>

                    <b> {{ $grandAllowance[13] }} </b> <br> <br>

                    <b> {{ $grandAllowance[6] }} </b> <br> <br>

                    <b> {{ $tillMay7thDiff?->isNotEmpty() ? round(collect($tillMay7thDiff)->sum('fifth_installment')) : 0 }} </b> <br> <br>
                </td>

                <td>
                    व्यावसायिक कर  <br> <br>
                    आयकर  <br> <br>
                    भविष्य निवाह निधी वर्गणी <br> <br>
                    राष्ट्रीय नि.वे.यो. @१०% (कर्मचारी)  <br> <br>
                    राष्ट्रीय नि.वे.यो. @१४% (पमपा)  <br> <br>
                    गट विमा योजना <br> <br>
                </td>

                <td>
                    <b> {{ $grandDeduction[1] }} </b> <br> <br>
                    <b> {{ $grandDeduction[2] }} </b> <br> <br>
                    <b> {{ $grandDeduction[5] }} </b> <br> <br>
                    <b> {{ $grandDeduction[7] }} </b> <br> <br>
                    <b> {{ $grandDeduction[8] }} </b> <br> <br>
                    <b> {{ $grandDeduction[21] }} </b> <br> <br>
                </td>

                {{-- <td>
                    पीएफ कर्ज <br> <br>

                    बँक कर्ज <br> <br>

                    एलआयसी वजावट <br> <br>

                    उत्सव वजावट <br> <br>

                    कर्मचारी वाटा <br> <br>

                    मुद्रांक शुल्क <br> <br>
                </td>

                <td>
                    <b>
                        {{ $grand_total_pf }} <br> <br> {{ $grand_total_bank_loan }} <br> <br> {{ $grand_total_lic + $grand_supplimentary_total_lic }} <br> <br> {{ $grand_total_festival_deduction + $grand_supplimentary_total_festival_deduction }} <br> <br> {{ $grand_total_employee_share + $grand_supplimentary_total_employee_share }} <br> <br> {{ $grand_total_stamp_duty }} <br> <br>
                    </b>
                </td> --}}
            </tr>

        </tbody>

    </table>


</body>

</html>
