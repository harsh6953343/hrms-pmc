@php
  use App\Models\EmployeeMonthlyLoan;
  use App\Models\SupplimentaryBill;
  use App\Models\RemainingFreezeSalary;
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grand Summary Ward Wise Report</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
        border: 1px solid rgb(177, 173, 173);
        border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:15px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,td
        {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
                page-break-after: always;
            }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    <h5 class="section-heading">Grand Summary All of Ward: {{ $freezeAttendances[0]->employee->ward->name }} for the month of {{ Carbon::parse($to_date)->format('F Y') }}</h5>

                </td>
            </tr>
        </thead>
    </table>


    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">Grand Total</th>
                <th class="th">Basic_Pay_+_GP</th>
                @foreach ($allowances->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $allowance)
                            {{ substr($allowance->allowance, 0, 5) }}/ <br>
                        @endforeach
                    </th>
                @endforeach
                <th class="th">Fest Adv.</th>
                <th class="th">DCPS I CORPORATION CONTRIBUTION ALLOWANCE</th>
                <th class="th">Total_Earn</th>
                @foreach ($deductions->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $deduction)
                            {{ substr($deduction->deduction, 0, 5) }}/ <br>
                        @endforeach
                    </th>
                @endforeach
                <th class="th">PF Loan/ <br/> Bank_Loan/ <br>LIC/ <br> Festival/ <br>Revenue Stamp</th>
                <th class="th">DCPS I EMPLOYEE CONTRIBUTION/ <br/> DCPS I CORPORATIONCONTRIBUTIONDEDUCTION</th>
                <th class="th">Total_Deduct</th>
                <th class="th">Net_Salary</th>
                {{-- <th class="th">Corporation_Share</th> --}}
                <th class="th">Remark</th>
            </tr>
        </thead>

        <tbody>

            @php
                $grand_total_basic_salary = 0;
                $grand_total_earn = 0;

                $allowanceTotals = [];
                $deductionTotals = [];

                $grand_total_pf = 0;
                $grand_total_bank_loan = 0;
                $grand_total_stamp_duty = 0;
                $grand_total_deductions = 0;
                $grand_total_net_salary = 0;
                $grand_total_corporation_share = 0;
                $grand_total_lic = 0;
                $grand_total_festival_allowance = 0;
                $grand_total_festival_deduction = 0;

                $grand_total_employee_share = 0;

            @endphp

            @foreach ($freezeAttendances as $freezeAttendance)
                @php
                    $explode_allowance_ids = explode(',', $freezeAttendance->allowance_Id);
                    $explode_allowance_type = explode(',', $freezeAttendance->allowance_Type);
                    $explode_allowance_amt = explode(',', $freezeAttendance->allowance_Amt);

                    $explode_deduction_ids = explode(',', $freezeAttendance->deduction_Id);
                    $explode_deduction_amt = explode(',', $freezeAttendance->deduction_Amt);
                    $explode_deduction_type = explode(',', $freezeAttendance->deduction_Type);

                    $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
                    $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);


                    // Grand Total Calculations
                    $grand_total_basic_salary += $freezeAttendance->basic_salary;
                    $grand_total_earn+= ($freezeAttendance->basic_salary + $freezeAttendance->total_allowance);

                @endphp

                <tr>
                    {{-- Allowance --}}
                    @foreach ($allowances->chunk(5, true) as $chunk)
                        @foreach ($chunk as $allowance)
                            @php
                                // Find the index of the allowance ID in the $explode_allowance_ids array
                                $index = array_search($allowance->id, $explode_allowance_ids);

                            @endphp
                            @if($index !== false)
                                @php
                                if (array_key_exists($allowance->id, $allowanceTotals)) {
                                    // If it exists, add the deduction amount to the existing total
                                    $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                                } else {
                                    // If it doesn't exist, initialize the total with the deduction amount
                                    $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                                }
                                @endphp
                            @endif
                        @endforeach
                    @endforeach

                    {{-- Deductions  --}}

                    @foreach ($deductions->chunk(5, true) as $chunk)
                        @foreach ($chunk as $deduction)
                            @php
                                $index = array_search($deduction->id, $explode_deduction_ids);

                            @endphp
                            @if($index !== false)
                                @php
                                if (array_key_exists($deduction->id, $deductionTotals)) {
                                    // If it exists, add the deduction amount to the existing total
                                    $deductionTotals[$deduction->id] += $explode_deduction_amt[$index];
                                } else {
                                    // If it doesn't exist, initialize the total with the deduction amount
                                    $deductionTotals[$deduction->id] = $explode_deduction_amt[$index];
                                }
                                @endphp
                            @endif
                        @endforeach
                    @endforeach


                    {{-- Loan --}}
                    @php
                    $pf_amt = 0;
                    $bank_loan = 0;
                    @endphp

                    @if ($freezeAttendance->loan_deduction_id)
                        @foreach ($explode_loan_ids as $loan_id)
                            @php
                                $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();

                                if ($emp_loan && $emp_loan->loan_id == 1) {

                                        $pf_amt = $emp_loan?->installment_amount;
                                        $grand_total_pf += $pf_amt;
                                }else{
                                    $bank_loan = $emp_loan?->installment_amount;
                                }

                            @endphp
                        @endforeach
                    @endif

                    @php
                        $grand_total_bank_loan+= $bank_loan;
                        $grand_total_stamp_duty+= $freezeAttendance->stamp_duty;
                        $grand_total_lic+=$freezeAttendance->total_lic_deduction;

                        $grand_total_deductions+=$freezeAttendance->total_deduction;

                        $grand_total_net_salary+= $freezeAttendance->net_salary;
                        $grand_total_corporation_share+= $freezeAttendance->corporation_share_da;

                        $grand_total_festival_allowance+= $freezeAttendance->festival_allowance;
                        $grand_total_festival_deduction+=$freezeAttendance->total_festival_deduction;

                        $grand_total_employee_share+=$freezeAttendance->employee_share_da;
                    @endphp
                </tr>
            @endforeach
        </tbody>

        <tfoot>
            <tr>
                <th class="table-footer" colspan="1" style="text-align: center; font-size:22px;">
                    <b> Grand Total </b>
                </th>
                <th class="table-footer">
                    <b>{{ $grand_total_basic_salary }}</b>
                </th>

                {{-- Grand Total Allowances --}}
                @foreach ($allowances->chunk(5, true) as $chunk)
                <th class="table-footer">
                    @foreach ($chunk as $allowance)

                        @if(isset($allowanceTotals[$allowance->id]))
                            <b>{{ $allowanceTotals[$allowance->id] }} </b><br>
                        @else
                            -<br>
                        @endif
                    @endforeach
                </th>
                @endforeach

                {{-- Grand Total Festival allowance --}}
                <th class="table-footer">
                    {{ $grand_total_festival_allowance }}
                </th>
                {{-- Grand Total Corporation share --}}
                <th class="table-footer">
                    {{ $grand_total_corporation_share }}
                </th>
                {{-- Grand Total Earn --}}
                <th class="table-footer">
                    {{ $grand_total_earn + $grand_total_corporation_share }}
                </th>

                {{-- Grand Total Deductions --}}
                @foreach ($deductions->chunk(5, true) as $chunk)
                <th class="table-footer">
                    @foreach ($chunk as $key=>$deduction)
                        @if(isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] }} </b><br>
                        @else
                            -<br>
                        @endif
                    @endforeach
                </th>
                @endforeach

                {{-- Loan Calculation --}}
                <th class="table-footer">
                    {{ $grand_total_pf }} <br> {{ $grand_total_bank_loan }} <br> {{ $grand_total_lic }} <br> {{ $grand_total_festival_deduction }} <br> {{ $grand_total_stamp_duty }}
                </th>

                {{-- Employee share --}}
                <th class="table-footer">
                    {{ $grand_total_employee_share }} <br> {{ $grand_total_corporation_share }}
                </th>

                {{-- Total Deductions --}}
                <th class="table-footer">
                    {{ $grand_total_deductions + $grand_total_corporation_share}}
                </th>

                {{-- Total Net Salary  --}}
                <th class="table-footer" colspan="2">
                    {{ $grand_total_net_salary }}
                </th>

                {{-- Total Corporation Share --}}
                {{-- <th class="table-footer" colspan="2">
                    {{ $grand_total_corporation_share }}
                </th> --}}

            </tr>
        </tfoot>

    </table>


    {{-- Signature part --}}


    <table style="width: 100%; margin-top:8%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        <tr>
            <td class="signature">EST. Cleark</td>
            <td class="signature">Est Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            <td class="signature">Cheif Auditor</td>
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>

    </table>

    <div style="margin-top: 5%;">
        <hr>
            <h2>
                Total Employees : {{ count($freezeAttendances) }}
            </h2>
        <hr>
    </div>

    <div class="page-break"></div>

    {{-- All Totals Footer --}}

    <table style="width: 100%; margin-top:8%;">
        <thead>
            <tr>
                <th>Earnings</th>
                <th>Amount</th>
                <th>Deductions</th>
                <th>Amount</th>
                <th>Deductions In Bank</th>
                <th>Amount</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    Basic <br> <br>

                    @foreach ($allowances as $chunk)

                    {{ $chunk->allowance }}          <br> <br>

                    @endforeach
                    Festival Advance <br> <br>

                    DCPS I CORPORATION CONTRIBUTION ALLOWANCE
                </td>
                <td>
                <b>
                    {{ $grand_total_basic_salary }}  <br> <br>

                    @foreach ($allowances as $allowance)
                            @if(isset($allowanceTotals[$allowance->id]))
                                <b>{{ $allowanceTotals[$allowance->id] }} </b><br> <br>
                            @else
                                0<br> <br>
                            @endif
                    @endforeach
                </b>

                <b>{{ $grand_total_festival_allowance }}</b><br> <br>

                <b>{{ $grand_total_corporation_share }}</b>
                </td>

                <td>
                    @foreach ($deductions as $chunk)
                        {{ $chunk->deduction }}          <br> <br>
                    @endforeach
                </td>
                <td>
                    <b>
                        @foreach ($deductions as $deduction)
                            @if(isset($deductionTotals[$deduction->id]))
                                <b>{{ $deductionTotals[$deduction->id] }} </b><br> <br>
                            @else
                                0<br><br>
                            @endif
                        @endforeach
                    </b>
                </td>

                <td>
                    PF Loan <br> <br>

                    Bank Loan <br> <br>

                    LIC Deduction <br> <br>

                    Festival Deduction <br> <br>

                    DCPS I EMPLOYEE CONTRIBUTION <br> <br>

                    DCPS I CORPORATIONCONTRIBUTIONDEDUCTION <br> <br>

                    Revenue Stamp <br> <br>

                </td>

                <td>
                    <b>
                        {{ $grand_total_pf }} <br> <br> {{ $grand_total_bank_loan }} <br> <br> {{ $grand_total_lic }} <br> <br> {{ $grand_total_festival_deduction }} <br> <br> {{ $grand_total_employee_share }} <br> <br> {{ $grand_total_corporation_share }} <br> <br> {{ $grand_total_stamp_duty }} <br> <br>
                    </b>
                </td>
            </tr>

        </tbody>

    </table>

    {{-- Signature part --}}


    <table style="width: 100%; margin-top:8%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        <tr>
            <td class="signature">EST. Cleark</td>
            <td class="signature">Est Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            <td class="signature">Cheif Auditor</td>
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>

    </table>

</body>
</html>
