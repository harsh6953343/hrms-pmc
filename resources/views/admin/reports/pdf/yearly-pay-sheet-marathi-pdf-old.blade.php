@php
    use App\Models\EmployeeMonthlyLoan;
    use App\Models\SupplimentaryBill;
    use App\Models\RemainingFreezeSalary;
    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yearly Pay Sheet Report</title>
    <style>
        body {
            font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
        }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
            text-align: left;
            margin: 10px 0 5px 350px;
            font-size: 24px;
        }

        .section-description {
            text-align: left;
            margin: 10px 0 5px 310px;
            font-size: 16px;
        }


        table,
        th,
        td {
            font-size: 12px;
            border: 1px solid rgb(177, 173, 173);
            border-collapse: collapse;

        }

        table,
        th {
            font-weight: 5px;
            font-size: 15px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,
        td {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer {
            background-color: lightgray !important;
        }

        .signature {
            background-color: white;
            border: none;
            padding: 5px;
            /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
            page-break-after: always;
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-row-group
        }

        tr {
            page-break-inside: avoid
        }
    </style>
</head>

<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:left;">
                    @if (isset($base64Logo))
                        <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="100">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    @if (!empty($freezeAttendances) && isset($freezeAttendances[0]))
                        <h5 class="section-description">
                            आर्थिक वर्ष - {{ $startYear }} - {{ $endYear }}
                            कर्मचारी - {{ $freezeAttendances[0]->Emp_Code }} - {{ $marathi_name }}
                        </h5>
                    @endif
                </td>
            </tr>
        </thead>
    </table>


    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">अनु. क्र.</th>
                <th class="th">पेमेंट प्रकार</th>
                <th class="th">माहे</th>
                <th class="th">मूळ वेतन</th>
                <th class="th">अर्जित वेतन</th>
                <th class="th">महागाई भत्ता</th>
                <th class="th">घर भाडेभत्ता</th>
                <th class="th">शहर भरपाई देणारा भत्ता</th>
                <th class="th">प्रतिनियुक्ती भत्ता</th>
                <th class="th">अंनिवयो - मपा</th>
                <th class="th">एकूण वेतन व भत्ते</th>
                <th class="th">भ. नि. नि. वर्गणी</th>
                <th class="th">अंनिवयो क.</th>
                <th class="th">अंनिवयो मपा</th>
                <th class="th">व्यावसायिक कर</th>
                <th class="th">आयकर</th>
                <th class="th">गट. विमा. यो.</th>
                <th class="th">मुद्रांक शुल्क</th>
                <th class="th">एकूण कपात</th>
                <th class="th">निव्वल देय्य</th>
            </tr>
        </thead>

        <tbody>

            @php
                $grand_total_basic_salary = 0;
                $grand_total_da_differance = 0;
                $grand_total_earn = 0;
                $grand_total_festival_allowance = 0;
                $grand_total_corp_allowance = 0;

                $allowanceTotals = [];
                $deductionTotals = [];

                $grand_total_pf = 0;
                $grand_total_bank_loan = 0;
                $grand_total_stamp_duty = 0;
                $grand_total_deductions = 0;
                $grand_total_net_salary = 0;
                $grand_total_corporation_share = 0;
                $grand_total_lic = 0;
                $grand_total_festival_deduction = 0;
                $grand_total_employee_share = 0;

                // Supplimentary
                $grand_total_supplimentary_basic_salary = 0;
                $grand_supplimentary_allowanceTotals = [];
                $grand_supplimentary_total_earn = 0;
                $grand_supplimentary_deductionTotals = [];
                $grand_supplimentary_total_stamp_duty = 0;
                $grand_supplimentary_total_deductions = 0;
                $grand_supplimentary_total_net_salary = 0;
                $grand_supplimentary_total_corporation = 0;
                $grand_supplimentary_total_lic = 0;
                $grand_supplimentary_total_festival_advance = 0;
                $grand_supplimentary_total_festival_deduction = 0;
                $grand_supplimentary_total_employee_share = 0;

            @endphp

            @foreach ($freezeAttendances as $index_key => $freezeAttendance)
                @php
                    $explode_allowance_ids = explode(',', $freezeAttendance->allowance_Id);
                    $explode_allowance_type = explode(',', $freezeAttendance->allowance_Type);
                    $explode_allowance_amt = explode(',', $freezeAttendance->allowance_Amt);

                    $explode_deduction_ids = explode(',', $freezeAttendance->deduction_Id);
                    $explode_deduction_amt = explode(',', $freezeAttendance->deduction_Amt);
                    $explode_deduction_type = explode(',', $freezeAttendance->deduction_Type);

                    $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
                    $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);

                    $totalDeductionInCurrentRow = 0;

                    // Grand Total Calculations
                    $grand_total_basic_salary += $freezeAttendance->earned_basic + $freezeAttendance->leave_pay;
                    $grand_total_da_differance += $freezeAttendance->da_differance;

                    $grand_total_earn += $freezeAttendance->basic_salary + $freezeAttendance->total_allowance + +$freezeAttendance->corporation_share_da;
                    $grand_total_festival_allowance += $freezeAttendance->festival_allowance;

                    $supplimentaryData = '';
                @endphp



                {{-- Supplimentary Calculation --}}


                @if ($freezeAttendance->supplimentary_status == 1)
                    @php
                        $supplimentary_record = SupplimentaryBill::where('employee_id', $freezeAttendance->employee_id)->where('Emp_Code', $freezeAttendance->Emp_Code)->where('id', $freezeAttendance->supplimentary_ids)->first();

                        $remaining_freeze_ids = explode(',', $supplimentary_record->remaining_freeze_id);

                        $supplimentaryData = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();

                    @endphp
                @endif


                @php
                    $supplimentary_basic_salary = 0;
                    $supplimentary_total_earn = 0;

                    $supplimentaryallowanceTotals = [];
                    $supplimentarydeductionTotals = [];
                    $supplimentaryloanTotals = [];
                    $supplimentaryloanTotalsids = [];

                    $supplimentary_stamp_duty = 0;
                    $supplimentary_total_deductions = 0;
                    $suppliemnatry_net_salary = 0;
                    $suppliemnatry_corporation_share = 0;
                    $suppliemnatry_present_days = 0;
                    $supplimentary_lic = 0;
                    $supplimentary_festival_advance = 0;
                    $supplimentary_festival_deduction = 0;
                    $supplimentary_employee_share = 0;

                @endphp
                @if (!empty($supplimentaryData))
                    @foreach ($supplimentaryData as $freeze)
                        @php
                            $supplimentary_explode_allowance_ids = explode(',', $freeze->allowance_Id);
                            $supplimentary_explode_allowance_amt = explode(',', $freeze->allowance_Amt);

                            $supplimentary_explode_deduction_ids = explode(',', $freeze->deduction_Id);
                            $supplimentary_explode_deduction_amt = explode(',', $freeze->deduction_Amt);

                            $supplimentary_explode_loan_ids = explode(',', $freeze->loan_deduction_id);

                            $supplimentary_basic_salary += $freeze->basic_salary;

                            $supplimentary_total_earn += $freeze->basic_salary + $freeze->total_allowance;

                            // $grand_total_bank_loan+= $bank_loan;
                            $supplimentary_stamp_duty += $freeze->stamp_duty;
                            $supplimentary_lic += $freeze->total_lic_deduction;
                            $supplimentary_festival_advance += $freeze->festival_allowance;
                            $supplimentary_festival_deduction += $freeze->total_festival_deduction;
                            $supplimentary_employee_share += $freeze->employee_share_da;

                            $supplimentary_total_deductions += $freeze->total_deduction;

                            $suppliemnatry_net_salary += $freeze->net_salary;
                            $suppliemnatry_corporation_share += $freeze->corporation_share_da;

                            if ($freeze->present_day == 0) {
                                $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                                $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                                $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                                $numberOfDaysInMonth += 1;
                                $suppliemnatry_present_days += $numberOfDaysInMonth;
                            }
                            $suppliemnatry_present_days += $freeze->present_day;

                        @endphp


                        @php
                            $allowance_ids = [1, 2, 4, 6, 13];
                        @endphp
                        {{-- Allowance --}}
                        @foreach ($allowances->chunk(5, true) as $chunk)
                            @foreach ($chunk as $allowance)
                                @if (!in_array($allowance->id, $allowance_ids))
                                    @continue;
                                @endif
                                @php
                                    $index = array_search($allowance->id, $supplimentary_explode_allowance_ids);
                                @endphp
                                @if ($index !== false)
                                    @php
                                        if (array_key_exists($allowance->id, $supplimentaryallowanceTotals)) {
                                            // If it exists, add the deduction amount to the existing total
                                            $supplimentaryallowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                        } else {
                                            // If it doesn't exist, initialize the total with the deduction amount
                                            $supplimentaryallowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                        }

                                        if (array_key_exists($allowance->id, $grand_supplimentary_allowanceTotals)) {
                                            // If it exists, add the deduction amount to the existing total
                                            $grand_supplimentary_allowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                        } else {
                                            // If it doesn't exist, initialize the total with the deduction amount
                                            $grand_supplimentary_allowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                        }

                                    @endphp
                                @endif
                            @endforeach
                        @endforeach

                        @php
                            $deductions_ids = [5, 7, 8, 1, 2, 21];
                        @endphp
                        {{-- Deductions --}}
                        @foreach ($deductions->chunk(5, true) as $chunk)
                            @foreach ($chunk as $deduction)
                                @if (!in_array($deduction->id, $deductions_ids))
                                    @continue;
                                @endif
                                @php
                                    $index = array_search($deduction->id, $supplimentary_explode_deduction_ids);
                                @endphp
                                @if ($index !== false)
                                    @php
                                        if (array_key_exists($deduction->id, $supplimentarydeductionTotals)) {
                                            $supplimentarydeductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                                        } else {
                                            $supplimentarydeductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                                        }

                                        if (array_key_exists($deduction->id, $grand_supplimentary_deductionTotals)) {
                                            $grand_supplimentary_deductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                                        } else {
                                            $grand_supplimentary_deductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                                        }
                                    @endphp
                                @endif
                            @endforeach
                        @endforeach

                        {{-- Loan --}}

                        @if (!empty($freeze->loan_deduction_id) && $freeze->loan_deduction_id != '')
                            @php
                                $loan_ids = $supplimentary_explode_loan_ids; // Assuming $supplimentary_explode_loan_ids is an array of loan IDs
                                $emp_loans = EmployeeMonthlyLoan::with('loan')->whereIn('id', $loan_ids)->get();
                                foreach ($emp_loans as $emp_loan) {
                                    $loan_name = $emp_loan->loan->loan;
                                    $loan_amount = $emp_loan->installment_amount;
                                    $loan_auto_id = $emp_loan->id;

                                    // Store loan name and amount
                                    if (array_key_exists($emp_loan->loan_id, $supplimentaryloanTotals)) {
                                        $supplimentaryloanTotals[$emp_loan->loan_id] += $loan_amount;
                                    } else {
                                        $supplimentaryloanTotals[$emp_loan->loan_id] = $loan_amount;
                                    }

                                    if (array_key_exists($loan_auto_id, $supplimentaryloanTotalsids)) {
                                        $supplimentaryloanTotalsids[$loan_auto_id] += $loan_amount;
                                    } else {
                                        $supplimentaryloanTotalsids[$loan_auto_id] = $loan_amount;
                                    }
                                }
                            @endphp
                        @endif
                    @endforeach
                @endif
                {{-- Supplimentary Calculation End --}}

                @php

                    $grand_total_supplimentary_basic_salary += $supplimentary_basic_salary;
                    $grand_supplimentary_total_earn += $supplimentary_total_earn;
                    $grand_supplimentary_total_stamp_duty += $supplimentary_stamp_duty;
                    $grand_supplimentary_total_deductions += $supplimentary_total_deductions;
                    $grand_supplimentary_total_net_salary += $suppliemnatry_net_salary;
                    $grand_supplimentary_total_corporation += $suppliemnatry_corporation_share;
                    $grand_supplimentary_total_lic += $supplimentary_lic;
                    $grand_supplimentary_total_festival_advance += $supplimentary_festival_advance;
                    $grand_supplimentary_total_festival_deduction += $supplimentary_festival_deduction;
                    $grand_supplimentary_total_employee_share += $supplimentary_employee_share;

                @endphp

                <tr>
                    <td>{{ convertToMarathiNumber($loop->iteration) }}</td>
                    <td>PayRoll Actual</td>
                    <td>
                        @php
                            $dateObj = DateTime::createFromFormat('!m', $freezeAttendance->month);
                            $monthName = $dateObj->format('F'); // March

                            $year = $startYear;
                            if ($freezeAttendance->month < 3 || $freezeAttendance->month > 12) {
                                $year = $endYear;
                            }
                        @endphp
                        {{ __('messages.' . $monthName) . '-' . $year }}
                    </td>
                    <td>{{ $freezeAttendance->basic_salary + $supplimentary_basic_salary }}</td>
                    <td>{{ $freezeAttendance->earned_basic + $freezeAttendance->leave_pay }}</td>

                    @php
                        $allowance_ids = [1, 2, 4, 13];
                    @endphp
                    @foreach ($allowance_ids as $allowance_id)
                        <td>
                            @php
                                // Find the index of the allowance ID in the $explode_allowance_ids array
                                $index = array_search($allowance_id, $explode_allowance_ids);
                            @endphp

                            @if ($index !== false)
                                @php
                                    if (array_key_exists($allowance_id, $allowanceTotals)) {
                                        // If it exists, add the allowance amount to the existing total
                                        $allowanceTotals[$allowance_id] += $explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the allowance amount
                                        $allowanceTotals[$allowance_id] = $explode_allowance_amt[$index];
                                    }
                                @endphp

                                @php
                                    $total_amount = $explode_allowance_amt[$index];
                                    if (!empty($supplimentaryData)) {
                                        $supplementary_total = isset($supplimentaryallowanceTotals[$allowance_id]) ? $supplimentaryallowanceTotals[$allowance_id] : 0;
                                        $total_amount += $supplementary_total;
                                    }

                                @endphp

                                {{ $total_amount }} <br>
                            @elseif(!in_array($allowance_id, $explode_allowance_ids))
                                @php
                                    $supplementary_total = isset($supplimentaryallowanceTotals[$allowance_id]) ? $supplimentaryallowanceTotals[$allowance_id] : '-';
                                @endphp

                                {{ $supplementary_total }} <br>
                            @else
                                -<br>
                            @endif
                        </td>
                    @endforeach

                    {{-- Corporation share --}}
                    <td>
                        {{ $freezeAttendance->corporation_share_da }}
                    </td>
                    @php
                        $grand_total_corp_allowance += $freezeAttendance->corporation_share_da;
                    @endphp

                    {{-- Total Earnings --}}
                    <td>
                        @if (!empty($supplimentaryData))
                            {{ $freezeAttendance->basic_salary + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da + $supplimentary_total_earn }}
                        @else
                            {{ $freezeAttendance->basic_salary + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da }}
                        @endif

                    </td>

                    {{-- Deductions  --}}
                    @php
                        $deductions_ids = [5, 7, 8, 1, 2, 21];

                    @endphp
                    @foreach ($deductions_ids as $deduction_id)
                        <td>
                            @php
                                $index = array_search($deduction_id, $explode_deduction_ids);
                            @endphp

                            @if ($index !== false)
                                @php

                                    if (array_key_exists($deduction_id, $deductionTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $deductionTotals[$deduction_id] += $explode_deduction_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $deductionTotals[$deduction_id] = $explode_deduction_amt[$index];
                                    }
                                @endphp

                                @php
                                    $total_amount = $explode_deduction_amt[$index];
                                    if (!empty($supplimentaryData)) {
                                        $supplementary_total = isset($supplimentarydeductionTotals[$deduction_id]) ? $supplimentarydeductionTotals[$deduction_id] : 0;
                                    }
                                    $totalDeductionInCurrentRow += $total_amount;
                                @endphp

                                {{ $total_amount }} <br>
                            @elseif(in_array($deduction_id, [7, 8]))
                                @php

                                    $total_amount = 0;
                                    if ($deduction_id == 7) {
                                        $total_amount += $freezeAttendance->employee_share_da;
                                        $deductionTotals[7] += $freezeAttendance->employee_share_da;
                                    } elseif ($deduction_id == 8) {
                                        $total_amount += $freezeAttendance->corporation_share_da;
                                        $deductionTotals[8] += $freezeAttendance->corporation_share_da;
                                    }

                                    $totalDeductionInCurrentRow += $total_amount;

                                @endphp

                                {{ $total_amount }} <br>
                            @elseif(!in_array($deduction_id, $explode_deduction_ids))
                                @php
                                    $supplementary_total = isset($supplimentarydeductionTotals[$deduction_id]) ? $supplimentarydeductionTotals[$deduction_id] : '-';
                                @endphp

                                - <br>
                            @else
                                -<br>
                            @endif
                        </td>
                    @endforeach

                    {{-- Loan --}}
                    @php
                        $pf_amt = 0;
                        $bank_loan = 0;
                    @endphp


                    @if (!empty($supplimentaryData) && !empty($supplimentaryloanTotals))
                        @foreach ($supplimentaryloanTotals as $loan_id => $total_amt)
                            @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('loan_id', $loan_id)->first(); @endphp
                            @if (!in_array($emp_loan->loan_id, $explode_bank_ids))
                                @php
                                if ($emp_loan && $emp_loan->loan_id == 1) {
                                        $pf_amt = $total_amt;
                                        $grand_total_pf += $pf_amt;
                                    } else {
                                        $bank_loan = $total_amt;
                                    }
                                @endphp
                            @endif
                        @endforeach
                    @endif

                    @if ($freezeAttendance->loan_deduction_id)
                        @foreach ($explode_loan_ids as $loan_id)
                            @php
                                $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();

                                if ($emp_loan && $emp_loan->loan_id == 1) {
                                    if (!empty($supplimentaryData) && isset($supplimentaryloanTotals[$emp_loan->loan_id])) {
                                        $pf_amt = $supplimentaryloanTotals[$emp_loan->loan_id] + $emp_loan->installment_amount;
                                        $grand_total_pf += $pf_amt;
                                    } else {
                                        $pf_amt = $emp_loan->installment_amount;
                                        $grand_total_pf += $pf_amt;
                                    }
                                } else {
                                    $bank_loan = $emp_loan->installment_amount;
                                }

                                if (!empty($supplimentaryData)) {
                                    if (isset($supplimentaryloanTotals[$emp_loan->loan_id]) && $emp_loan->loan_id != 1) {
                                        $bank_loan = $emp_loan->installment_amount + $supplimentaryloanTotals[$emp_loan->loan_id];
                                    }
                                } elseif ($emp_loan->loan_id != 1) {
                                    $bank_loan = $emp_loan->installment_amount;
                                }
                            @endphp
                        @endforeach
                    @endif
                    {{-- Loan End  --}}

                    <td>
                        {{ $freezeAttendance->stamp_duty + $supplimentary_stamp_duty }}
                    </td>

                    @php
                        $grand_total_bank_loan += $bank_loan;
                        $grand_total_stamp_duty += $freezeAttendance->stamp_duty;
                        $grand_total_lic += $freezeAttendance->total_lic_deduction;

                        $grand_total_deductions += $totalDeductionInCurrentRow + $freezeAttendance->stamp_duty;

                        $grand_total_net_salary += $freezeAttendance->net_salary;
                        $grand_total_corporation_share += $freezeAttendance->corporation_share_da;
                        $grand_total_festival_deduction += $freezeAttendance->total_festival_deduction;
                        $grand_total_employee_share += $freezeAttendance->employee_share_da;

                    @endphp

                    {{-- Total Deduction --}}
                    <td>{{ $totalDeductionInCurrentRow + $freezeAttendance->stamp_duty }}</td>

                    {{-- Net Salary --}}
                    <td>{{ $freezeAttendance->net_salary + $suppliemnatry_net_salary }}</td>

                </tr>
                {{-- @if (($index_key + 1) % 5 == 0)
                    <tr class="page-break"></tr>
                @endif --}}
                <?php
                $currSrNo = $loop->iteration;
                ?>
            @endforeach

            <?php
            $columnStr = $sixColumnStr = $eightColumnStr = $fourColumnStr = '';
            for ($i = 0; $i < 9; $i++) {
                $columnStr .= '<td></td>';
            }
            for ($i = 0; $i < 8; $i++) {
                if ($i < 4) {
                    $fourColumnStr .= '<td></td>';
                }
                if ($i < 6) {
                    $sixColumnStr .= '<td></td>';
                }
                $eightColumnStr .= '<td></td>';
            }
            ?>

            @php
                if (isset($deductionTotals[5])) {
                    $deductionTotals[5] += $gpf_supplementary;
                } else {
                    $deductionTotals[5] = $gpf_supplementary;
                }
                if (isset($deductionTotals[7])) {
                    $deductionTotals[7] += $dcps_emp_supplementary;
                } else {
                    $deductionTotals[7] = $dcps_emp_supplementary;
                }
                if (isset($deductionTotals[8])) {
                    $deductionTotals[8] += $dcps_empr_supplementary;
                } else {
                    $deductionTotals[8] = $dcps_empr_supplementary;
                }
                $supplementary_net_salary = $supplementary_bill - $supplementary_total_deduction;

                $grand_total_basic_salary += $supplementary_bill;
                $grand_total_earn += $seventh_pay_fifth_installment;
                $grand_total_deductions += $supplementary_total_deduction;
                $grand_total_net_salary += $seventh_pay_fifth_installment + $supplementary_net_salary;

            @endphp

            <tr>
                <td>{{ convertToMarathiNumber($currSrNo + 1) }}</td>
                <td>७व्या वेतनाचा ५वा हप्ता</td>
                {!! $eightColumnStr !!}
                <td>
                    {{ $seventh_pay_fifth_installment }}
                </td>
                {!! $columnStr !!}
            </tr>


            {{-- Zafeer --}}
            @if ($till_may_supplementary_bill)
                <tr>
                    <td>{{ convertToMarathiNumber($currSrNo + 2) }}</td>
                    <td>पुरवणी बिल</td>
                    {{-- <td>{{ __('messages.' . Carbon::createFromFormat('m', substr($monthlySupplymentry->month, 5))->format('F')) . '-' . substr($monthlySupplymentry->month, 0, 4) }}</td> --}}
                    <td>--</td>
                    <td></td>
                    <td>
                        {{ $till_may_supplementary_bill }}
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>

                    <td>{{ $till_may_gpf_supplementary }}</td>
                    <td>{{ $till_may_dcps_emp_supplementary }}</td>
                    <td>{{ $till_may_dcps_empr_supplementary }}</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                        {{ $till_may_supplementary_total_deduction }}
                    </td>
                    <td>
                        {{ ($till_may_supplementary_bill - $till_may_supplementary_total_deduction) }}
                    </td>
                </tr>
                @php
                    $currSrNo++;
                @endphp
            @endif

            @php
                $currSrNo--;
            @endphp
            @foreach ($supp_bill_from_may as $monthlySupplymentry)
                @php
                    $ids = explode(',', $monthlySupplymentry->allowance_id);
                    $amounts = explode(',', $monthlySupplymentry->allowance_amount);
                    $allowanceCollection = collect(array_combine($ids, $amounts));

                    $dedIds = explode(',', $monthlySupplymentry->deduction_id);
                    $dedAmounts = explode(',', $monthlySupplymentry->deduction_amount);
                    $deductionCollection = collect(array_combine($dedIds, $dedAmounts));
                    $currSrNo++;
                @endphp
                <tr>
                    <td>{{ convertToMarathiNumber($currSrNo + 2) }}</td>
                    <td>पुरवणी बिल</td>
                    <td>{{ __('messages.' . Carbon::createFromFormat('m', substr($monthlySupplymentry->month, 5))->format('F')) . '-' . substr($monthlySupplymentry->month, 0, 4) }}</td>
                    <td></td>
                    <td>
                        {{ round($monthlySupplymentry->basic_salary) }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[1]) ? round($allowanceCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[2]) ? round($allowanceCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[4]) ? round($allowanceCollection[4]) : 0 }}
                    </td>
                    <td>
                        {{ isset($allowanceCollection[13]) ? round($allowanceCollection[13]) : 0 }}
                    </td>
                    <td>
                        @php
                            $grand_total_corp_allowance += isset($allowanceCollection[6]) ? $allowanceCollection[6] : 0;
                        @endphp
                        {{ isset($allowanceCollection[6]) ? round($allowanceCollection[6]) : 0 }}
                    </td>
                    <td>
                        @php
                            $grand_total_earn += collect($amounts)->filter()->sum();
                        @endphp
                        {{ round(collect($amounts)->filter()->sum()) }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[5]) ? round($deductionCollection[5]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[7]) ? round($deductionCollection[7]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[8]) ? round($deductionCollection[8]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[1]) ? round($deductionCollection[1]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[2]) ? round($deductionCollection[2]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[21]) ? round($deductionCollection[21]) : 0 }}
                    </td>
                    <td>
                        {{ isset($deductionCollection[22]) ? round($deductionCollection[22]) : 0 }}
                    </td>
                    <td>
                        {{ round(collect($dedAmounts)->filter()->sum()) }}
                    </td>
                    <td>
                        {{ round($monthlySupplymentry->basic_salary + collect($amounts)->filter()->sum() - collect($dedAmounts)->filter()->sum()) }}
                    </td>
                </tr>
            @endforeach


            {{-- <tr>
                <td>{{convertToMarathiNumber($currSrNo + 2)}}</td>
                <td>पुरवणी बिल</td>
                <td></td>
                <td></td>
                <td>
                    {{$supplementary_bill}}
                </td>
                <td>
                    {{$dearness_allowance_supplementary}}
                </td>
                <td>
                    {{$hra_supplementary}}
                </td>
                <td>
                    {{$city_allowance_supplementary}}
                </td>
                <td>
                    {{$deputation_allowance_supplementary}}
                </td>
                <td>
                    {{$dcps_corp_allowance_supplementary}}
                </td>
                <td>
                    {{$supplementary_total_allowances}}
                </td>



                <td>
                    {{$gpf_supplementary}}
                </td>
                <td>
                    {{$dcps_emp_supplementary}}
                </td>
                <td>
                    {{$dcps_empr_supplementary}}
                </td>

                <td>
                    {{$professional_tax_supplementary}}
                </td>
                <td>
                    {{$income_tax_supplementary}}
                </td>
                <td>
                    {{$gis_ded_supplementary}}
                </td>
                <td>
                    {{$stamp_duty_supplementary}}
                </td>
                <td>
                    {{$supplementary_total_deduction}}
                </td>
                <td>
                    {{$supplementary_net_salary}}
                </td>
            </tr> --}}

        </tbody>

        <tfoot>
            <tr>
                <th class="table-footer" colspan="4" style="text-align: center; font-size:22px;">
                    <b> एकूण </b>
                </th>
                <th class="table-footer">
                    {{-- <b>{{ round($grand_total_basic_salary+$till_may_supplementary_bill) }}</b> --}}
                    <b>{{ round($grand_total_basic_salary) }}</b>
                </th>

                {{-- Grand Total Allowances --}}
                @foreach ($allowances->chunk(5, true) as $chunk)
                    @foreach ($chunk as $allowance)
                        @if (!in_array($allowance->id, $allowance_ids))
                            @continue;
                        @endif
                        <th class="table-footer">
                            @if(isset($allowanceTotals[$allowance->id]))
                                <b>{{ round($allowanceTotals[$allowance->id] + $supplementary_allowances[$allowance->id]) }} </b><br>
                            @else
                                -<br>
                            @endif
                        </th>
                    @endforeach
                @endforeach

                <th class="table-footer">
                    {{ round($grand_total_corp_allowance) }}
                </th>

                {{-- Grand Total Earn --}}
                <th class="table-footer">
                    {{ round($grand_total_earn + $supplementary_total_allowances) }}
                </th>

                {{-- Grand Total Deductions --}}
                @php
                    $deduction_print_order = [5, 7, 8, 1, 2, 21];
                @endphp
                @foreach ($deduction_print_order as $ded_id)
                    <th class="table-footer">
                        @if (isset($deductionTotals[$ded_id]) && $ded_id == 5)
                            {{-- <b>{{ round($deductionTotals[$ded_id]) }} </b><br> --}}
                            <b>{{ round($deductionTotals[$ded_id] + $till_may_gpf_supplementary) }} </b><br>
                        @elseif(isset($deductionTotals[$ded_id]) && $ded_id == 7)
                        {{-- <b>{{ round($deductionTotals[$ded_id]) }} </b><br> --}}
                            <b>{{ round($deductionTotals[$ded_id] + $till_may_dcps_emp_supplementary) }} </b><br>
                        @elseif(isset($deductionTotals[$ded_id]) && $ded_id == 8)
                            {{-- <b>{{ round($deductionTotals[$ded_id]) }} </b><br> --}}
                            <b>{{ round($deductionTotals[$ded_id] + $till_may_dcps_empr_supplementary) }} </b><br>
                        @elseif(isset($deductionTotals[$ded_id]))
                            <b>{{ round($deductionTotals[$ded_id]) }} </b><br>
                        @else
                            -<br>
                        @endif
                    </th>
                @endforeach

                <th class="table-footer">
                    {{ $grand_total_stamp_duty + $stamp_duty_supplementary }}
                </th>

                {{-- Total Deductions --}}
                <th class="table-footer">
                    {{-- {{ $grand_total_deductions + $supplementary_total_deduction }} --}}
                    {{ round($grand_total_deductions + $supplementary_total_deduction + $till_may_supplementary_total_deduction) }}
                </th>

                {{-- Total Net Salary  --}}
                <th class="table-footer">
                    {{ $grand_total_net_salary + $supplementary_net_salary }}
                </th>

            </tr>
        </tfoot>

    </table>


    <div class="page-break"></div>

    {{-- All Totals Footer --}}

    <table style="width: 100%; margin-top:8%;">
        <thead>
            <tr>
                <th>कमाई</th>
                <th>रक्कम</th>
                <th>वजावट</th>
                <th>रक्कम</th>
                <th>बँकेत वजावट</th>
                <th>रक्कम</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    अर्जित वेतन <br> <br>

                    @foreach ($allowances as $chunk)
                        @if (!in_array($chunk->id, $allowance_ids))
                            @continue;
                        @endif
                        {{ $chunk->allowance_in_marathi }} <br> <br>
                    @endforeach

                    सण आगाऊ <br> <br>

                    ७व्या वेतनाचा ५वा हप्ता <br> <br>

                </td>
                <td>
                    <b>
                        {{ $grand_total_basic_salary }} <br> <br>

                        @foreach ($allowances as $allowance)
                            @if (!in_array($allowance->id, $allowance_ids))
                                @continue;
                            @endif
                            @if (isset($grand_supplimentary_allowanceTotals[$allowance->id]) && isset($allowanceTotals[$allowance->id]))
                                <b>{{ $allowanceTotals[$allowance->id] + $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br> <br>
                            @elseif(isset($allowanceTotals[$allowance->id]))
                                <b>{{ $allowanceTotals[$allowance->id] }} </b><br> <br>
                            @elseif(isset($grand_supplimentary_allowanceTotals[$allowance->id]))
                                <b>{{ $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br> <br>
                            @else
                                0<br> <br>
                            @endif
                        @endforeach

                        <b>{{ $grand_total_festival_allowance + $grand_supplimentary_total_festival_advance }}</b> <br> <br>

                        <b>{{ $seventh_pay_fifth_installment }}</b> <br> <br>

                    </b>
                </td>

                <td>
                    @foreach ($deductions as $chunk)
                        @if (!in_array($chunk->id, $deductions_ids))
                            @continue;
                        @endif
                        {{ $chunk->deduction_in_marathi }} <br> <br>
                    @endforeach
                </td>
                <td>
                    @foreach ($deductions as $deduction)
                        @if (!in_array($deduction->id, $deductions_ids))
                            @continue;
                        @endif
                        @if (isset($grand_supplimentary_deductionTotals[$deduction->id]) && isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] + $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br> <br>
                        @elseif(isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] }} </b><br> <br>
                        @elseif(isset($grand_supplimentary_deductionTotals[$deduction->id]))
                            <b>{{ $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br> <br>
                        @else
                            0<br><br>
                        @endif
                    @endforeach
                </td>

                <td>
                    पीएफ कर्ज <br> <br>

                    बँक कर्ज <br> <br>

                    एलआयसी वजावट <br> <br>

                    उत्सव वजावट <br> <br>

                    कर्मचारी वाटा <br> <br>

                    मुद्रांक शुल्क <br> <br>
                </td>

                <td>
                    <b>
                        {{ $grand_total_pf }} <br> <br> {{ $grand_total_bank_loan }} <br> <br> {{ $grand_total_lic + $grand_supplimentary_total_lic }} <br> <br> {{ $grand_total_festival_deduction + $grand_supplimentary_total_festival_deduction }} <br> <br> {{ $grand_total_employee_share + $grand_supplimentary_total_employee_share }} <br> <br> {{ $grand_total_stamp_duty }} <br> <br>
                    </b>
                </td>
            </tr>

        </tbody>

    </table>


</body>

</html>
