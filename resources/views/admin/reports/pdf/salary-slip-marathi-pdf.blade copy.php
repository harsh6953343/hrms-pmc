@php
    use App\Models\Allowance;
    use App\Models\Deduction;
    use App\Models\EmployeeMonthlyLoan;
    use Carbon\Carbon;


     $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
     $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);

     $explode_lic_ids = explode(',', $freezeAttendance->lic_deduction_id);

     $explode_festival_ids = explode(',', $freezeAttendance->festival_deduction_id);

    if (!function_exists('getRecoveryDays')) {
        function getRecoveryDays($freezeAttendance, $employee_details){

            $basicSalary = $freezeAttendance->basic_salary; //basic salary
            $nextMonthDays = $freezeAttendance->present_day; //present day
            $deductionIdArr = explode(',', $freezeAttendance->deduction_Id);
            $deductionAmountArr = explode(',', $freezeAttendance->deduction_Amt);
            $combineDeductionArr = array_combine($deductionIdArr,$deductionAmountArr);

            $total = $combineDeductionArr[12] ?? 0;  //amount

            $allowances = \App\Models\EmployeeAllowance::where('Emp_Code', $freezeAttendance->Emp_Code)
                ->where('allowance_id', '!=', 6)
                ->where('is_active', 1)
                ->get();

            $allowanceTotal = 0;
            $da = 0;

            foreach ($allowances as $allowance) {
                if ($allowance->allowance_type == "1") {
                    $allowanceTotal += $allowance->allowance_amt / $nextMonthDays;
                } else {
                    $allowanceTotal += (($basicSalary * $allowance->allowance_amt) / 100) / $nextMonthDays;
                }

                if ($allowance->allowance_id == "1") {
                    if ($allowance->allowance_type == "1") {
                        $da = $allowance->allowance_amt;
                    } else {
                        $da = ($basicSalary * $allowance->allowance_amt) / 100;
                    }
                }
            }

            if ($employee_details->doj > '2005-11-01' && $employee_details->department_id != 3) {
                $allowanceTotal += ((($da + $basicSalary) * 14) / 100) / $nextMonthDays;
            }

            $noOfDays = $total / (($basicSalary / $nextMonthDays) + $allowanceTotal);
            if(!$noOfDays){
                return '०';
            }else{
                $recovery_days = round($noOfDays, 1);
                return convertToMarathiNumber($recovery_days);
            }
        }
    }
@endphp
<!DOCTYPE html>
<html lang="{{ app()->setLocale('marathi') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Slip - {{ $freezeAttendance->emp_name }}</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        /* body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        } */
        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 19px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 2px 0 5px;
            font-size: 20px;
        }
        .subsection {
            width: 51%;
            float: left;
        }

        .subsection-details {
            width: 47.8%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border: 2px solid black; /* Single border */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            text-align: left;
            /* background-color: lightgray; */
        }
        #deductions {
            border-bottom: none;
            border-left: none;
        }
        #earnings{
            border-bottom: none;
        }
        #grand_total{
            border-left: none;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<body>

    <table style="width: 96%;margin-left:2%">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:left">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="90" width="150">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h5 class="section-heading" style="margin-left:20%;">{{ $corporation->corporation_in_marathi }}  </h5>
                    <h5 class="section-heading" style="margin-left:12%;">पनवेल, जिल्हा रायगड, पिन-४१०२०६</h5>
                </td>
                <td style="background-color:white; border:none;">
                    @php
                        list($month,$year) = explode(' ',Carbon::parse($freezeAttendance->to_date)->format('F Y'));
                    @endphp
                    <h5 class="section-heading" style="margin-left:25%;">माहे: {{ __('messages.'.$month).' '.convertToMarathiNumber($year) }} </h5>
                    <h5 class="section-heading" style="margin-left:32%;">ची वेतन पावती </h5>
                </td>
            </tr>
        </thead>
    </table>

    <div class="section">
        <div class="subsection-details" style="border-left: 2px solid black;">
            <p><span class="label">{{ __('messages.emp_code') }}</span><strong>: {{ convertToMarathiNumber($freezeAttendance->Emp_Code) }} </strong></p>
            <p><span class="label">{{ __('messages.designation') }}</span><strong>: {{ $employee_details->designation->marathi_name }}</strong></p>
            <p><span class="label">{{ __('messages.payscale') }}</span><strong>: {{-- convertToMarathiNumber($freezeAttendance->pay_band_scale)." ".convertToMarathiNumber($freezeAttendance->grade_pay_scale) --}}</strong></p>
            <p><span class="label">{{ __('messages.release_day') }}</span><strong>: {{ convertToMarathiNumber($freezeAttendance->actual_present_day) }} </strong></p>
            <p><span class="label">{{ __('messages.recovery_day') }}</span><strong>: {{ getRecoveryDays($freezeAttendance, $employee_details) }}</strong></p>
        </div>
        <div class="subsection-details" style="border-right: 2px solid black;margin-right: 12px;">
            <p><span class="label">{{ __('messages.emp_name') }}</span><strong>: {{ $employee_details->fullname }}</strong></p>
            <p><span class="label">{{ __('messages.ward') }}</span><strong>: {{ __('messages.'.$employee_details->ward->name) }}</strong></p>
            <p><span class="label">{{ __('messages.actual_basic') }}</span><strong>: {{ convertToMarathiNumber($freezeAttendance->actual_basic) }}</strong></p>
            <p><span class="label">{{ __('messages.leave_days') }}</span><strong>: {{ convertToMarathiNumber($freezeAttendance->total_leave) }} </strong></p>
            <p><span class="label"></span></p>
        </div>
    </div>
<!--
    {{-- Supplimentary Calculation --}}

    @php
    $total_basic_salary = 0;
    $grand_total_earn = 0;
    $grand_total_corporation = 0;

    $allowanceTotals = [];
    $deductionTotals = [];
    $loanTotals = [];

    $grand_total_bank_loan = 0;
    $total_stamp_duty = 0;
    $total_deductions = 0;
    $total_net_salary = 0;
    $present_days = 0;
    $total_lic = 0;

    @endphp
    @if(!empty($supplimentaryData))

        @foreach ($supplimentaryData as $freeze)

            @php
                $supplimentary_explode_allowance_ids = explode(',', $freeze->allowance_Id);
                $supplimentary_explode_allowance_amt = explode(',', $freeze->allowance_Amt);

                $supplimentary_explode_deduction_ids = explode(',', $freeze->deduction_Id);
                $supplimentary_explode_deduction_amt = explode(',', $freeze->deduction_Amt);

                $supplimentary_explode_loan_ids = explode(',', $freeze->loan_deduction_id);

                $total_basic_salary += $freeze->basic_salary;
                $grand_total_earn   += ($freeze->basic_salary + $freeze->total_allowance);
                $grand_total_corporation += $freeze->corporation_share_da;

                // $grand_total_bank_loan+= $bank_loan;
                $total_stamp_duty+= $freeze->stamp_duty;

                $total_deductions+=$freeze->total_deduction;

                $total_net_salary+= $freeze->net_salary;

                if($freeze->present_day == 0){
                    $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                    $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                    $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                    $numberOfDaysInMonth += 1;
                    $present_days+= $numberOfDaysInMonth;
                }
                $present_days += $freeze->present_day;

            @endphp

            {{-- Allowance --}}
            @foreach ($allowances->chunk(5, true) as $chunk)
                @foreach ($chunk as $allowance)
                    @php
                        $index = array_search($allowance->id, $supplimentary_explode_allowance_ids);
                    @endphp
                    @if($index !== false)
                        @php
                        if (array_key_exists($allowance->id, $allowanceTotals)) {
                            // If it exists, add the deduction amount to the existing total
                            $allowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                        } else {
                            // If it doesn't exist, initialize the total with the deduction amount
                            $allowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                        }
                        @endphp
                    @endif
                @endforeach
            @endforeach

            {{-- Deductions --}}
            @foreach ($deductions->chunk(5, true) as $chunk)
                @foreach ($chunk as $deduction)
                    @php
                        $index = array_search($deduction->id, $supplimentary_explode_deduction_ids);
                    @endphp
                    @if($index !== false)
                        @php
                        if (array_key_exists($deduction->id, $deductionTotals)) {
                            $deductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                        } else {
                            $deductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                        }
                        @endphp
                    @endif
                @endforeach
            @endforeach

            {{-- Loan --}}

            @if (!empty($freeze->loan_deduction_id) && $freeze->loan_deduction_id != '')
                @php
                    $loan_ids = $supplimentary_explode_loan_ids; // Assuming $supplimentary_explode_loan_ids is an array of loan IDs

                    $emp_loans = EmployeeMonthlyLoan::with('loan')->whereIn('id', $loan_ids)->get();

                    foreach ($emp_loans as $emp_loan) {
                        $loan_name = $emp_loan->loan->loan;
                        $loan_amount = $emp_loan->installment_amount;
                        $loan_auto_id = $emp_loan->id;

                        // Store loan name and amount
                        if (array_key_exists($emp_loan->loan_id, $loanTotals)) {
                            $loanTotals[$emp_loan->loan_id] += $loan_amount;
                        } else {
                            $loanTotals[$emp_loan->loan_id] = $loan_amount;
                        }
                    }
                @endphp
            @endif

            {{-- LIC Start --}}
            @if (!empty($freeze->lic_deduction_id) && $freeze->lic_deduction_id != '')
                @php
                    $total_lic += $freeze->total_lic_deduction;
                @endphp
            @endif
            {{-- LIC End --}}


        @endforeach
    @endif
    {{-- Supplimentary Calculation End --}} -->

    @if($freezeAttendance->basic_salary != 0)
        <div class="section" style="margin-left:19px;">
            <div class="subsection">
                <div class="table-container">
                    <table id="earnings">
                        <thead>
                            <tr>
                                <th class="earnings-header">वेतन व भत्ते</th>
                                <th class="earnings-header"></th>
                                <th class="earnings-header">देय्य</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>अर्जित वेतन</td>
                                <td>:</td>
                                <td><b>{{ convertToMarathiNumber($freezeAttendance->earned_basic + $freezeAttendance->leave_pay) }}
                                    <!-- @if(!empty($supplimentaryData)) / {{ $total_basic_salary }}  @endif  -->
                                </b></td>

                            </tr>

                            {{-- Allowance --}}
                            {{--<!-- @if(!empty($supplimentaryData))
                            @foreach($allowanceTotals as $allowance_id => $total_amt)
                                @if(!in_array($allowance_id, $explode_allowance_ids))
                                    @php $allowance = Allowance::find($allowance_id); @endphp
                                    <tr>
                                        <td>{{ $allowance->allowance }}</td>
                                        <td><b>0 / {{ $total_amt }}</b></td>
                                    </tr>
                                @endif
                            @endforeach
                            @endif -->--}}

                            @foreach($explode_allowance_ids as $key => $allowance_id)
                                @php $allowance = Allowance::find($allowance_id); @endphp
                                <tr>
                                    <td>{{ $allowance->allowance_in_marathi }}</td>
                                    <td>:</td>
                                    <td><b>{{ convertToMarathiNumber($explode_allowance_amt[$key]) }}
                                        {{-- <!-- @if(!empty($supplimentaryData))  / @if(isset($allowanceTotals[$allowance->id])) {{ $allowanceTotals[$allowance->id] }}
                                        @else 0  @endif @endif  --> --}}
                                    </b></td>
                                </tr>
                            @endforeach
                            @if($freezeAttendance->festival_allowance != 0)
                            <tr>
                                <td>महोत्सव आगाऊ</td>
                                <td>:</td>
                                <td><b>{{ convertToMarathiNumber($freezeAttendance->festival_allowance) }}</b></td>
                            </tr>
                        @endif
                        @if($employee_details->ward->id != 5)
                            <tr>
                                <td>राष्ट्रीय नि.वे.यो. @१४% (पमपा)</td>
                                <td>:</td>
                                <td><b>{{ convertToMarathiNumber($freezeAttendance->corporation_share_da) }}</b></td>

                            </tr>
                        @endif
                            @php
                                $deduction_rows =  (count($explode_deduction_ids)); //including stamp duty

                                if ($freezeAttendance->lic_deduction_id)
                                    $deduction_rows += 1;
                                if ($explode_loan_ids)
                                    $deduction_rows += count($explode_loan_ids);

                                if ($explode_festival_ids)
                                    $deduction_rows += 1;
                                $allowance_rows = count($explode_allowance_ids) + 3;
                                if ($freezeAttendance->festival_allowance != 0)
                                $allowance_rows += 1;
                                // echo $allowance_rows,$deduction_rows;die;
                            @endphp
                            @if($deduction_rows > $allowance_rows)
                            @php
                                $no_of_rows = $deduction_rows - $allowance_rows; // including basic
                            @endphp
                            @for($i = 0; $i < $no_of_rows; $i++)
                            <tr>
                                <td><br></td>
                            </tr>
                            @endfor
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="subsection" style="width: 45%;">
                <div class="table-container">
                    <table id="deductions">
                        <thead>
                            <tr>
                                <th class="deductions-header">कपात</th>
                                <th class="deductions-header"></th>
                                <th class="deductions-header">रक्कम</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- Deductions --}}

                            {{-- <!-- @if(!empty($supplimentaryData))
                            @foreach($deductionTotals  as $deduction_id  => $total_amt)
                            @if(!in_array($deduction_id, $explode_deduction_ids))

                                @php $deduction = Deduction::find($deduction_id); @endphp
                                <tr>
                                    <td>{{ $deduction->deduction }}</td>
                                    <td><b>{{ $total_amt }}</b></td>
                                </tr>
                            @endif
                            @endforeach
                            @endif --> --}}

                            @foreach($explode_deduction_ids as $key => $deduction_id)
                                @php $deduction = Deduction::find($deduction_id); @endphp
                                <tr>
                                    <td>{{ $deduction?->deduction_in_marathi }}</td>
                                    <td>:</td>
                                    <td><b>{{ convertToMarathiNumber($explode_deduction_amt[$key]) }}
                                        {{-- <!-- @if(!empty($supplimentaryData)) / @if(isset($deductionTotals[$deduction->id])) {{ $deductionTotals[$deduction->id] }} @else 0  @endif @endif  --> --}}
                                    </b></td>

                                </tr>
                            @endforeach


                            {{-- Employee Loan --}}
                            {{-- <!-- @if(!empty($supplimentaryData))
                                @foreach($loanTotals as $loan_id => $total_amt)
                                    @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('loan_id', $loan_id)->first(); @endphp
                                    @if(!in_array($emp_loan->loan_id, $explode_bank_ids))
                                        <tr>
                                            <td>{{ $emp_loan?->loan?->loan . ' (' . $emp_loan?->installment_no.')' }}</td>
                                            <td><b>{{ $total_amt }}</b></td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif --> --}}

                            @if ($freezeAttendance->loan_deduction_id)
                                @foreach ($explode_loan_ids as $key=>$loan_id)
                                    @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();
                                        $bank_id = $explode_bank_ids[$key];
                                    @endphp
                                    <tr>
                                        <td>{{ $emp_loan?->loan?->loan_in_marathi . ' (' . convertToMarathiNumber($emp_loan?->installment_no).')' }}</td>
                                        <td>:</td>
                                        <td><b>{{ convertToMarathiNumber($emp_loan?->installment_amount) }}
                                            {{-- <!-- @if(!empty($supplimentaryData))
                                            /
                                            @if(isset($loanTotals[$bank_id]))
                                            {{ $loanTotals[$bank_id] }} @else 0  @endif @endif --> --}}
                                        </b></td>

                                    </tr>
                                @endforeach
                            @endif

                            @if ($freezeAttendance->lic_deduction_id)
                            <tr>
                                <td>एलआयसी वजावट</td>
                                <td>:</td>
                                <td><b>{{ convertToMarathiNumber($freezeAttendance->total_lic_deduction) }}
                                    {{-- <!-- @if(!empty($supplimentaryData))  / {{ $total_lic }} @endif --> --}}
                                </b></td>
                            </tr>
                            @endif

                            {{-- Festival Deduction --}}
                            @if ($freezeAttendance->total_festival_deduction != 0)
                            <tr>
                                <td>उत्सव कपात</td>
                                <td>:</td>
                                <td><b>{{ convertToMarathiNumber($freezeAttendance->total_festival_deduction) }}</b></td>
                            </tr>
                            @endif
                            {{-- Festival Deduction End --}}

                            @if($employee_details->ward->id != 5)
                                {{-- Employee Contribution 10% --}}
                                @if ($freezeAttendance->employee_share_da != 0)
                                <tr>
                                    <td>राष्ट्रीय नि.वे.यो. @१०% (कर्मचारी)</td>
                                    <td>:</td>
                                    <td><b>{{ convertToMarathiNumber($freezeAttendance->employee_share_da) }}</b></td>

                                </tr>
                                @endif

                                {{-- Employer Contribution End --}}
                                <tr>
                                    <td>राष्ट्रीय नि.वे.यो. @१४% (पमपा)</td>
                                    <td>:</td>
                                    <td><b>{{ convertToMarathiNumber($freezeAttendance->corporation_share_da) }}</b></td>
                                </tr>
                            @endif
                            <tr>
                                <td>मुद्रांक शुल्क</td>
                                <td>:</td>
                                <td><b>
                                    {{ convertToMarathiNumber($freezeAttendance->stamp_duty) }}
                                    <!-- @if(!empty($supplimentaryData))  / {{ $total_stamp_duty }} @endif -->
                                </b></td>
                            </tr>

                            @if(count($explode_allowance_ids) + 1 > $deduction_rows)
                            @php
                                $no_of_rows_deduction = count($explode_allowance_ids) + 1 - $deduction_rows;
                            @endphp
                            @for($i = 0; $i < $no_of_rows_deduction; $i++)
                            <tr>
                                <td><br></td>
                            </tr>
                            @endfor
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <p class="check">

        <div class="section" style="margin-left:19px;">
            <div class="subsection">
                <table>
                    <tr>
                        <td><b>एकूण वेतन व भत्ते</b></td>
                        <td>:</td>
                        <td><b>{{ convertToMarathiNumber($freezeAttendance->earned_basic + $freezeAttendance->leave_pay + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da) }}
                            <!-- @if(!empty($supplimentaryData)) / {{ $grand_total_earn }} @endif -->
                        </b></td>
                    </tr>
                    <tr style="height: 42px;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="subsection" style="width: 45%;">
                <table id="grand_total">
                    <tr>
                        <td><b>एकूण कपात</b></td>
                        <td>:</td>
                        <td><b>{{ convertToMarathiNumber($freezeAttendance->total_deduction + $freezeAttendance->corporation_share_da) }}
                            <!-- @if(!empty($supplimentaryData)) / {{ $total_deductions }} @endif -->
                        </b></td>
                    </tr>
                    <tr>
                        <td><b>निव्वल देय्य वेतन</b></td>
                        <td>:</td>
                        <td ><b>{{ convertToMarathiNumber($freezeAttendance->net_salary) }}
                            <!-- @if(!empty($supplimentaryData)) / {{ $total_net_salary }} @endif -->
                        </b></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="section" style="margin-left:19px;">
            <div class="subsection">
                <table>
                    <tr colspan="2">
                        <td><b>बँक कपात वर्णन</b></td>
                        <td>:</td>
                    </tr>
                    <tr colspan="2">
                        <td><b>खाते क्र. </b></td>
                        <td>:</td>
                        <td><b>{{ convertToMarathiNumber($freezeAttendance->bank_account_number) }}</b></td>
                    </tr>
                </table>
            </div>
            <div class="subsection" style="width: 45%;">
                <table id="grand_total">
                    <tr style="height: 42px;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>दिनांक</b></td>
                        <td>:</td>
                        <td ><b>{{ convertToMarathiNumber(date('d-m-Y')) }}
                        </b></td>
                    </tr>
                </table>
            </div>
        </div>

    @else

    @if(empty($supplimentaryData))
        <h3 style="margin-left:25px;">या महिन्यात कोणताही पगार नाही</h3>
    {{-- <!-- @else --> --}}
    <!-- <h3 style="margin-left:25px;">No salary generate for this month, This is supplimentary Bill  for the month of
        @php
            $monthNames = collect(explode(',', $supplimentary_record->month))
                                            ->map(function ($monthNumber) {
                                                return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                            })
                                            ->sort()
                                            ->implode(', ');
            echo $monthNames;
        @endphp</h3>

    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="earnings-header">EARNINGS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BASIC SALARY</td>
                            <td><b>{{ $total_basic_salary }}</b></td>
                        </tr>
                        {{-- Allowance --}}
                        @foreach($allowances as $key => $allowance)
                            @if(isset($allowanceTotals[$allowance->id]))
                            <tr>
                                <td>{{ $allowance?->allowance }}</td>
                                <td><b>{{ $allowanceTotals[$allowance->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach
                        {{-- Count --}}
                        @php

                            $deduction_rows =  count($deductionTotals) + 1; //including stamp duty
                            if ($loanTotals)
                                $deduction_rows += count($loanTotals);
                        @endphp
                        @if($deduction_rows > count($allowanceTotals) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($allowanceTotals) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

        <div class="subsection" style="width: 45%;">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Deductions --}}
                        @foreach($deductions as $key => $deduction)
                            @if(isset($deductionTotals[$deduction->id]))
                            <tr>
                                <td>{{ $deduction?->deduction }}</td>
                                <td><b>{{ $deductionTotals[$deduction->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach

                        {{-- Employee Loan --}}
                        @foreach($loanTotals as $loan_name => $loan_total)
                            <tr>
                                <td>{{ $loan_name }}</td>
                                <td><b>{{ $loan_total }}</b></td>
                            </tr>
                        @endforeach
                        @if ($total_lic != 0)
                        <tr>
                            <td>एलआयसी</td>
                            <td><b>{{ $total_lic }}</b></td>
                        </tr>
                        @endif
                        <tr>
                            <td>STAMP DUTY</td>
                            <td><b>{{ $total_stamp_duty }}</b></td>
                        </tr>

                        @if(count($allowanceTotals) + 1 > $deduction_rows)
                        @php
                            $no_of_rows_deduction = count($allowanceTotals) + 1 - $deduction_rows;
                        @endphp
                        @for($i = 0; $i < $no_of_rows_deduction; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <p class="check">


    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <table>
                <tr>
                    <td><b>TOTAL EARNINGS</b></td>
                    <td><b>{{ $grand_total_earn }}</b></td>
                </tr>
                <tr>
                    <td><b>Corporation Share</b></td>
                    <td><b>{{ $grand_total_corporation }}</b></td>
                </tr>
            </table>
        </div>
        <div class="subsection" style="width: 45%;">
            <table>
                <tr>
                    <td><b>TOTAL DEDUCTIONS</b></td>
                    <td><b>{{ $total_deductions }}</b></td>
                </tr>
                <tr>
                    <td><b>NET SALARY</b></td>
                    <td ><b>{{ $total_net_salary }}</b></td>
                </tr>
            </table>
        </div>
    </div>

    @endif -->
    @endif

    <div class="section" style="width: 96%;">
        <table>
            <tr>
                <td>* सदरची वेतन पावती खालील अटींच्या अधीन राहून देण्यात येत आहे.</td>
            </tr>
            <tr>
                <td>१. कर्ज मंजूर करण्यासाठी वेतन पावती वापरल्यास कर्ज परत फेडीचा किंवा कर्ज वसूल करण्यास किंवा कोणत्याही नोटिशिला उत्तर देण्यास
                महानगरपालिका जबाबदार राहणार नाही.</td>
            </tr>
            <tr>
                <td>२. सदर वेतन पावती कोणत्याही प्रकारच्या कर्ज प्रकरणा करीता हमी पत्र नाही.</td>
            </tr>
            <tr>
                <td>३. वेतनपावती आधारे कर्ज देणे / जामीनदार करून घेणे बाबत वसुलीची संपूर्ण जबाबदारी बँकेची / वित्तसंस्थेची असेल.</td>
            </tr>
            <tr>
                <td>४. ही संगणकाद्वारे तयार केलीली वेतन पावती आहे, स्वाक्षरीची आवश्यकता नाही.</td>
            </tr>
        </table>
    </div>

</body>
</html>
