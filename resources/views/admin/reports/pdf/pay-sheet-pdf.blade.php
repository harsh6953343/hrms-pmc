@php
  use App\Models\EmployeeMonthlyLoan;
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pay Sheet Report</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
            border: 1px solid rgb(177, 173, 173);
            border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:14px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,td
        {
            font-size: 15px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
                page-break-after: always;
            }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ (request()->lang == "en") ? $corporation->name : $corporation->corporation_in_marathi }}</h2>
                    @if(!empty($freezeAttendances) && isset($freezeAttendances[0]))
                        @if(request()->lang == "en")
                        <h5 class="section-heading">Ward: {{ $freezeAttendances[0]->employee->ward->name }}
                        Salary Bill - All of {{ $freezeAttendances[0]->employee->department->name }}
                        for the month of {{ Carbon::parse($to_date)->format('F Y') }}</h5>
                        @else
                            @php
                            $monthMrName = ['01' => 'जानेवारी', '02' => 'फेब्रुवारी', '03' => 'मार्च', '04' => 'एप्रिल', '05' => 'मे', '06' => 'जून', '07' => 'जुलै', '08' => 'ऑगस्ट', '09' => 'सप्टेंबर', '10' => 'ऑक्टोबर', '11' => 'नोहेंबर', '12' => 'डिसेंबर'];
                            @endphp
                        <h5 class="section-heading">वार्ड: {{ $freezeAttendances[0]->employee->ward->name }}
                            पगार बिल - सर्व {{ $freezeAttendances[0]->employee->department->name }}
                            च्या महिन्यासाठी {{ $monthMrName[Carbon::parse($to_date)->format('m')].' '.convertToMarathiNumber(Carbon::parse($to_date)->format('Y')) }}</h5>
                        @endif
                    @endif
                </td>
            </tr>
        </thead>
    </table>


    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">@if(request()->lang == "en")Sr.No/<br/>Emp Code @else क्र./<br/>कर्मचारी ओळख @endif</th>
                <th class="th">@if(request()->lang == "en")Present Days @else सध्याचे दिवस @endif</th>
                <th class="th">@if(request()->lang == "en") Name/<br/>Date Of Appointment @else नाव/<br/>नियुक्तीची तारीख @endif</th>
                {{-- <th class="th">Name/<br/>Pay Scale/<br/>Date Of Appointment</th> --}}
                <th class="th">@if(request()->lang == "en")Desg @else पदनाम @endif</th>
                <th class="th">@if(request()->lang == "en")Actual Basic @else मूळ वेतन @endif</th>
                <th class="th">@if(request()->lang == "en")Earned Pay @else अर्जित वेतन @endif</th>
                <th class="th">@if(request()->lang == "en")LEAVEPAY @else रजा वेतन @endif</th>
                @foreach ($allowances->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $allowance)
                            @if(request()->lang == "en")
                            {{ substr($allowance->allowance, 0, 5) }}/ <br>
                            @else
                            {{ explode(" ",$allowance->allowance_in_marathi)[0] ?? '-' }}/ <br>
                            @endif
                        @endforeach
                    </th>
                @endforeach
                <th class="th">@if(request()->lang == "en")Fest Adv. @else उत्सव आगाऊ @endif</th>
                <th class="th">@if(request()->lang == "en")Corporation Share @else राष्ट्रीय नि.वे.यो.(मनपा) @endif</th>
                <th class="th">@if(request()->lang == "en")Total_Earn @else एकूण कमाई @endif</th>
                @foreach ($deductions->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $deduction)
                            @if(request()->lang == "en")
                            {{ substr($deduction->deduction, 0, 5) }}/ <br>
                            @else
                            {{ explode(" ",$deduction->deduction_in_marathi)[0] ?? '-' }}/ <br>
                            @endif
                        @endforeach
                    </th>
                @endforeach
                <th class="th">
                    @if(request()->lang == "en")
                    PF Loan/ <br/> Bank Loan/ <br>LIC/ <br> Festival/ <br> Revenue Stamp
                    @else
                    PF Loan/ <br/> Bank Loan/ <br>LIC/ <br> Festival/ <br> Revenue Stamp
                    @endif
                </th>
                <th class="th">
                    @if(request()->lang == "en")
                    Employee Share / <br/> Corporation Share
                    @else
                    राष्ट्रीय नि.वे.यो.(कर्मचारी) / <br/> राष्ट्रीय नि.वे.यो.(मनपा)
                    @endif
                </th>
                <th class="th">@if(request()->lang == "en")Total Deduct @else एकूण कपात @endif</th>
                <th class="th">@if(request()->lang == "en")Net Salary @else निव्वळ वेतन @endif</th>
                {{-- <th class="th"></th> --}}
                <th class="th">@if(request()->lang == "en")Remark @else शेरा @endif</th>
            </tr>
        </thead>

        <tbody>

            @php
                $grand_total_basic_salary = 0;
                $grand_total_earn = 0;
                $grand_total_leavepay = 0;
                $grand_total_festival_allowance = 0;

                $allowanceTotals = [];
                $deductionTotals = [];

                $grand_total_pf = 0;
                $grand_total_bank_loan = 0;
                $grand_total_stamp_duty = 0;
                $grand_total_deductions = 0;
                $grand_total_net_salary = 0;
                $grand_total_corporation_share = 0;
                $grand_total_lic = 0;
                $grand_total_festival_deduction = 0;
                $grand_total_employee_share = 0;


            @endphp
            @php $count = 1; @endphp
            @foreach ($freezeAttendances as $freezeAttendance)
                @if($freezeAttendance?->employee?->employee_status?->is_salary_applicable == "0")
                @continue
                @endif
                @php
                    $explode_allowance_ids = explode(',', $freezeAttendance->allowance_Id);
                    $explode_allowance_type = explode(',', $freezeAttendance->allowance_Type);
                    $explode_allowance_amt = explode(',', $freezeAttendance->allowance_Amt);

                    $explode_deduction_ids = explode(',', $freezeAttendance->deduction_Id);
                    $explode_deduction_amt = explode(',', $freezeAttendance->deduction_Amt);
                    $explode_deduction_type = explode(',', $freezeAttendance->deduction_Type);

                    $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
                    $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);


                    // Grand Total Calculations
                    $grand_total_basic_salary += $freezeAttendance->earned_basic;

                    $grand_total_earn += ($freezeAttendance->earned_basic + $freezeAttendance->leave_pay + $freezeAttendance->total_allowance);
                    $grand_total_festival_allowance += $freezeAttendance->festival_allowance;

                    $grand_total_leavepay += round(($freezeAttendance->actual_basic / $total_days) * $freezeAttendance->total_leave);

                @endphp

                <tr>
                    <td>@if(request()->lang == "en"){{ $count++ }} <br> {{ $freezeAttendance->Emp_Code }} @else {{ convertToMarathiNumber($count++) }} <br> {{ convertToMarathiNumber($freezeAttendance->Emp_Code) }} @endif </td>
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->present_day : convertToMarathiNumber($freezeAttendance->present_day) }}</td>
                    {{-- <td>{{ $freezeAttendance->emp_name }} <br> {{ $freezeAttendance->pay_band_scale." ".$freezeAttendance->grade_pay_scale }} <br> {{ $freezeAttendance->date_of_appointment }}</td> --}}
                    <td>@if(request()->lang == "en") {{ $freezeAttendance?->employee?->fname.' '.$freezeAttendance?->employee?->mname.' '.$freezeAttendance?->employee?->lname }} <br> {{ $freezeAttendance->date_of_appointment }} @else {{ $freezeAttendance?->employee?->m_fname.' '.$freezeAttendance?->employee?->m_mname.' '.$freezeAttendance?->employee?->m_lname }} <br> {{ convertToMarathiNumber($freezeAttendance->date_of_appointment) }} @endif</td>
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->designation->name : $freezeAttendance->designation->marathi_name }}</td>
                    <td>
                        {{ (request()->lang == "en") ? $freezeAttendance->actual_basic : convertToMarathiNumber($freezeAttendance->actual_basic) }}
                    </td>
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->earned_basic : convertToMarathiNumber($freezeAttendance->earned_basic) }}</td>
                    <td>
                        {{ (request()->lang == "en") ? $freezeAttendance->leave_pay : convertToMarathiNumber($freezeAttendance->leave_pay) }}
                    </td>
                    {{-- Allowance --}}
                    @foreach ($allowances->chunk(5, true) as $chunk)
                        <td>
                            @foreach ($chunk as $allowance)
                                @php
                                    // Find the index of the allowance ID in the $explode_allowance_ids array
                                    $index = array_search($allowance->id, $explode_allowance_ids);
                                @endphp

                                @if($index !== false)

                                    @php
                                    if (array_key_exists($allowance->id, $allowanceTotals)) {
                                        // If it exists, add the allowance amount to the existing total
                                        $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the allowance amount
                                        $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                                    }
                                    @endphp

                                    {{ (request()->lang == "en") ? $explode_allowance_amt[$index] : convertToMarathiNumber($explode_allowance_amt[$index]) }} <br>

                                @else
                                    -<br>
                                @endif
                            @endforeach
                        </td>
                    @endforeach

                    {{-- Festival Allowance --}}
                    <td>
                        {{ (request()->lang == "en") ? $freezeAttendance->festival_allowance : convertToMarathiNumber($freezeAttendance->festival_allowance) }}
                    </td>

                    {{-- Corporation share --}}
                    <td>
                        {{ (request()->lang == "en") ? $freezeAttendance->corporation_share_da : convertToMarathiNumber($freezeAttendance->corporation_share_da) }}
                    </td>

                    {{-- Total Allowance --}}
                    <td>
                        {{ (request()->lang == "en") ? $freezeAttendance->earned_basic + $freezeAttendance->leave_pay + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da : convertToMarathiNumber($freezeAttendance->earned_basic + $freezeAttendance->leave_pay + $freezeAttendance->total_allowance + $freezeAttendance->corporation_share_da) }}
                    </td>

                    {{-- Deductions  --}}

                    @foreach ($deductions->chunk(5, true) as $chunk)
                        <td>
                            @foreach ($chunk as $deduction)
                                @php
                                    $index = array_search($deduction->id, $explode_deduction_ids);
                                @endphp

                                @if($index !== false)

                                    @php
                                    if (array_key_exists($deduction->id, $deductionTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $deductionTotals[$deduction->id] += $explode_deduction_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $deductionTotals[$deduction->id] = $explode_deduction_amt[$index];
                                    }
                                    @endphp

                                    {{ (request()->lang == "en") ? $explode_deduction_amt[$index] : convertToMarathiNumber($explode_deduction_amt[$index]) }} <br>

                                @else
                                    -<br>
                                @endif
                            @endforeach
                        </td>
                    @endforeach


                    {{-- Loan --}}
                    @php
                    $pf_amt = 0;
                    $bank_loan = 0;
                    @endphp

                    @if ($freezeAttendance->loan_deduction_id)
                        @foreach ($explode_loan_ids as $loan_id)
                            @php
                                $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();

                                if ($emp_loan && $emp_loan->loan_id == 1) {

                                        $pf_amt = $emp_loan->installment_amount;
                                        $grand_total_pf += $pf_amt;
                                }else{

                                    $bank_loan = $emp_loan->installment_amount;
                                }

                            @endphp
                        @endforeach
                    @endif
                    <td>
                        {{ ($pf_amt != 0) ? (request()->lang == "en") ? $pf_amt : convertToMarathiNumber($pf_amt) : '-' }}<br>
                        {{ ($bank_loan != 0) ? (request()->lang == "en") ? $bank_loan : convertToMarathiNumber($bank_loan) : '-' }}<br>
                        {{ ($freezeAttendance->lic_deduction_id != '') ? (request()->lang == "en") ? $freezeAttendance->total_lic_deduction : convertToMarathiNumber($freezeAttendance->total_lic_deduction) : '-' }}
                            <br>
                        {{ ($freezeAttendance->festival_deduction_id != '') ? (request()->lang == "en") ? $freezeAttendance->total_festival_deduction : convertToMarathiNumber($freezeAttendance->total_festival_deduction) : '-' }}
                            <br>
                        {{ (request()->lang == "en") ? $freezeAttendance->stamp_duty : convertToMarathiNumber($freezeAttendance->stamp_duty) }}
                    </td>
                    {{-- Loan End  --}}

                    @php
                        $grand_total_bank_loan += $bank_loan;
                        $grand_total_stamp_duty += $freezeAttendance->stamp_duty;
                        $grand_total_lic += $freezeAttendance->total_lic_deduction;

                        $grand_total_deductions += $freezeAttendance->total_deduction;

                        $grand_total_net_salary += $freezeAttendance->net_salary;
                        $grand_total_corporation_share += $freezeAttendance->corporation_share_da;

                        $grand_total_festival_deduction += $freezeAttendance->total_festival_deduction;

                        $grand_total_employee_share += $freezeAttendance->employee_share_da;

                    @endphp

                    {{-- employee share --}}
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->employee_share_da : convertToMarathiNumber($freezeAttendance->employee_share_da) }} <br> {{ (request()->lang == "en") ? $freezeAttendance->corporation_share_da : convertToMarathiNumber($freezeAttendance->corporation_share_da) }} </td>

                    {{-- Total Deduction --}}
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->total_deduction + $freezeAttendance->corporation_share_da : convertToMarathiNumber($freezeAttendance->total_deduction + $freezeAttendance->corporation_share_da)}}</td>

                    {{-- Net Salary --}}
                    <td>{{ (request()->lang == "en") ? $freezeAttendance->net_salary : convertToMarathiNumber($freezeAttendance->net_salary) }}</td>

                    {{-- <td>{{ $freezeAttendance->corporation_share_da }}</td> --}}

                    <td>
                        {{ ($freezeAttendance?->employee?->employee_status?->remark) ? $freezeAttendance?->employee?->employee_status?->remark : $freezeAttendance?->remark }}

                    </td>
                </tr>

            @endforeach
        </tbody>



    </table>
    <div class="page-break"></div>

    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">@if(request()->lang == "en")Sr.No/<br/>Emp Code @else क्र./<br/>कर्मचारी ओळख @endif</th>
                <th class="th">@if(request()->lang == "en")Present Days @else सध्याचे दिवस @endif</th>
                <th class="th">@if(request()->lang == "en") Name/<br/>Pay Scale/<br/>Date Of Appointment @else नाव/<br>वेतनमान<br/>नियुक्तीची तारीख @endif</th>
                <th class="th">@if(request()->lang == "en")Desg @else पदनाम @endif</th>
                <th class="th">@if(request()->lang == "en")Actual Basic @else मूळ वेतन @endif</th>
                <th class="th">@if(request()->lang == "en")Earned Pay @else अर्जित वेतन @endif</th>
                <th class="th">@if(request()->lang == "en")LEAVEPAY @else रजा वेतन @endif</th>
                @foreach ($allowances->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $allowance)
                            @if(request()->lang == "en")
                            {{ substr($allowance->allowance, 0, 5) }}/ <br>
                            @else
                            {{ explode(" ",$allowance->allowance_in_marathi)[0] ?? '-' }}/ <br>
                            @endif
                        @endforeach
                    </th>
                @endforeach
                <th class="th">@if(request()->lang == "en")Fest Adv. @else उत्सव आगाऊ @endif</th>
                <th class="th">@if(request()->lang == "en")Corporation Share @else राष्ट्रीय नि.वे.यो.(मनपा) @endif</th>
                <th class="th">@if(request()->lang == "en")Total_Earn @else एकूण कमाई @endif</th>
                @foreach ($deductions->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $deduction)
                            @if(request()->lang == "en")
                            {{ substr($deduction->deduction, 0, 5) }}/ <br>
                            @else
                            {{ explode(" ",$deduction->deduction_in_marathi)[0] ?? '-' }}/ <br>
                            @endif
                        @endforeach
                    </th>
                @endforeach
                <th class="th">
                    @if(request()->lang == "en")
                    PF Loan/ <br/> Bank Loan/ <br>LIC/ <br> Festival/ <br> Revenue Stamp
                    @else
                    पीएफ कर्ज/ <br/> बँक कर्ज/ <br>एलआयसी/ <br> उत्सव/ <br> मुद्रांक शुल्क
                    @endif
                </th>
                <th class="th">
                    @if(request()->lang == "en")
                    Employee Share / <br/> Corporation Share
                    @else
                    राष्ट्रीय नि.वे.यो.(कर्मचारी) / <br/> राष्ट्रीय नि.वे.यो.(मनपा)
                    @endif
                </th>
                <th class="th">@if(request()->lang == "en")Total Deduct @else एकूण कपात @endif</th>
                <th class="th">@if(request()->lang == "en")Net Salary @else निव्वळ वेतन @endif</th>
                {{-- <th class="th"></th> --}}
                <th class="th">@if(request()->lang == "en")Remark @else शेरा @endif</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="table-footer" colspan="5" style="text-align: center; font-size:22px;">
                    <b> @if(request()->lang == "en")Grand Total @else ग्रँड टोटल @endif </b>
                </th>
                <th class="table-footer">
                    <b>{{ (request()->lang == "en") ? $grand_total_basic_salary : convertToMarathiNumber($grand_total_basic_salary) }}</b>
                </th>
                <th class="table-footer">
                    <b>{{ (request()->lang == "en") ? $grand_total_leavepay : convertToMarathiNumber($grand_total_leavepay) }}</b>
                </th>
                {{-- Grand Total Allowances --}}
                @foreach ($allowances->chunk(5, true) as $chunk)
                    <th class="table-footer">
                        @foreach ($chunk as $allowance)

                            @if(isset($allowanceTotals[$allowance->id]))
                                <b>{{ (request()->lang == "en") ? $allowanceTotals[$allowance->id] : convertToMarathiNumber($allowanceTotals[$allowance->id]) }} </b><br>
                            @else
                                -<br>
                            @endif

                        @endforeach
                    </th>
                @endforeach

                {{-- Grand Total Festival allowance --}}
                <th class="table-footer">
                    {{ (request()->lang == "en") ? $grand_total_festival_allowance : convertToMarathiNumber($grand_total_festival_allowance) }}
                </th>

                {{-- Grand Total Corporation share --}}
                <th class="table-footer">
                    {{ (request()->lang == "en") ? $grand_total_corporation_share : convertToMarathiNumber($grand_total_corporation_share) }}
                </th>
                {{-- Grand Total Earn --}}
                <th class="table-footer">
                    {{ (request()->lang == "en") ? $grand_total_earn + $grand_total_corporation_share : convertToMarathiNumber($grand_total_earn + $grand_total_corporation_share) }}
                </th>

                {{-- Grand Total Deductions --}}
                @foreach ($deductions->chunk(5, true) as $chunk)
                <th class="table-footer">
                    @foreach ($chunk as $key=>$deduction)

                        @if(isset($deductionTotals[$deduction->id]))
                            <b>{{ (request()->lang == "en") ? $deductionTotals[$deduction->id] : convertToMarathiNumber($deductionTotals[$deduction->id]) }} </b><br>
                        @else
                            -<br>
                        @endif

                    @endforeach
                </th>
                @endforeach

                {{-- Loan Calculation --}}
                <th class="table-footer">
                    @if(request()->lang == "en")
                    {{ $grand_total_pf }} <br> {{ $grand_total_bank_loan }} <br> {{ $grand_total_lic }} <br> {{ $grand_total_festival_deduction }} <br> {{ $grand_total_stamp_duty }}
                    @else
                    {{ convertToMarathiNumber($grand_total_pf) }} <br> {{ convertToMarathiNumber($grand_total_bank_loan) }} <br> {{ convertToMarathiNumber($grand_total_lic) }} <br> {{ convertToMarathiNumber($grand_total_festival_deduction) }} <br> {{ convertToMarathiNumber($grand_total_stamp_duty) }}
                    @endif
                </th>

                {{-- employee share --}}
                <th class="table-footer">
                    @if(request()->lang == "en")
                    {{ $grand_total_employee_share }} <br> {{ $grand_total_corporation_share }}
                    @else
                    {{ convertToMarathiNumber($grand_total_employee_share) }} <br> {{ convertToMarathiNumber($grand_total_corporation_share) }}
                    @endif
                </th>

                {{-- Total Deductions --}}
                <th class="table-footer">
                    @if(request()->lang == "en")
                    {{ $grand_total_deductions + $grand_total_corporation_share  }}
                    @else
                    {{ convertToMarathiNumber($grand_total_deductions + $grand_total_corporation_share) }}
                    @endif
                </th>

                {{-- Total Net Salary  --}}
                <th class="table-footer" colspan="2">
                    @if(request()->lang == "en")
                    {{ $grand_total_net_salary }}
                    @else
                    {{ convertToMarathiNumber($grand_total_net_salary) }}
                    @endif
                </th>
                {{-- Total Corporation Share --}}
                {{-- <th class="table-footer" colspan="2">
                    {{ $grand_total_corporation_share }}
                </th> --}}

            </tr>
        </tbody>
    </table>

    {{-- Signature part --}}


    <table style="width: 100%; margin-top:8%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            {{-- <td class="signature">__________________</td> --}}
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        @if(request()->lang == "en")
        <tr>
            <td class="signature">EST. Cleark</td>
            <td class="signature">Est Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            {{-- <td class="signature">Cheif Auditor</td> --}}
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>
        @else
        <tr>
            <td class="signature">EST. Cleark</td>
            <td class="signature">Est Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            {{-- <td class="signature">Cheif Auditor</td> --}}
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>
        @endif

    </table>

    <div class="page-break"></div>

    {{-- All Totals Footer --}}

    <table style="width: 100%; margin-top:8%;">
        <thead>
            @if(request()->lang == "en")
            <tr>
                <th>Earnings</th>
                <th>Amount</th>
                <th>Deductions</th>
                <th>Amount</th>
                <th>Deductions In Bank</th>
                <th>Amount</th>
            </tr>
            @else
            <tr>
                <th>कमाई</th>
                <th>रक्कम</th>
                <th>वजावट</th>
                <th>रक्कम</th>
                <th>बँकेत वजावट</th>
                <th>रक्कम</th>
            </tr>
            @endif
        </thead>

        <tbody>
            <tr>
                @if(request()->lang == "en")
                <td>
                    EARNED BASIC <br> <br>

                    LEAVE PAY <br> <br>

                    @foreach ($allowances as $chunk)

                    {{ $chunk->allowance }}          <br> <br>

                    @endforeach

                    Festival Advance <br> <br>

                    DCPS I CORPORATION CONTRIBUTION ALLOWANCE
                </td>
                @else
                <td>
                    बेसिक मिळवले <br> <br>

                    पे सोडा <br> <br>

                    @foreach ($allowances as $chunk)

                    {{ $chunk->allowance_in_marathi }}          <br> <br>

                    @endforeach

                    उत्सव आगाऊ <br> <br>

                    DCPS I कॉर्पोरेशन योगदान भत्ता
                </td>
                @endif
                <td>
                <b>
                    {{ (request()->lang == "en") ? $grand_total_basic_salary : convertToMarathiNumber($grand_total_basic_salary) }}  <br> <br>

                    {{ (request()->lang == "en") ? $grand_total_leavepay : convertToMarathiNumber($grand_total_leavepay) }}  <br> <br>

                    @foreach ($allowances as $allowance)
                            @if(isset($allowanceTotals[$allowance->id]))
                                <b>{{ (request()->lang == "en") ? $allowanceTotals[$allowance->id] : convertToMarathiNumber($allowanceTotals[$allowance->id]) }} </b><br> <br>
                            @else
                                0<br> <br>
                            @endif
                    @endforeach

                    <b>{{ (request()->lang == "en") ? $grand_total_festival_allowance : convertToMarathiNumber($grand_total_festival_allowance) }}</b> <br> <br>

                    <b>{{ (request()->lang == "en") ? $grand_total_corporation_share : convertToMarathiNumber($grand_total_corporation_share) }}</b>

                </b>
                </td>

                <td>
                    @foreach ($deductions as $chunk)

                        {{ (request()->lang == "en") ? $chunk->deduction : convertToMarathiNumber($chunk->deduction) }}          <br> <br>

                    @endforeach
                </td>
                <td>
                    @foreach ($deductions as $deduction)
                        @if(isset($deductionTotals[$deduction->id]))
                            <b>{{ (request()->lang == "en") ? $deductionTotals[$deduction->id] : convertToMarathiNumber($deductionTotals[$deduction->id]) }} </b><br> <br>
                        @else
                            0<br><br>
                        @endif
                    @endforeach
                </td>

                <td>
                    @if(request()->lang == "en")
                    PF Loan <br> <br>

                    Bank Loan <br> <br>

                    LIC Deduction <br> <br>

                    Festival Deduction <br> <br>

                    DCPS I EMPLOYEE CONTRIBUTION <br> <br>

                    DCPS I CORPORATIONCONTRIBUTIONDEDUCTION <br> <br>

                    Stamp Duty <br> <br>
                    @else
                    पीएफ कर्ज <br> <br>

                    बँक कर्ज <br> <br>

                    एलआयसी कपात <br> <br>

                    उत्सव वजावट <br> <br>

                    DCPS I कर्मचारी योगदान <br> <br>

                    DCPS I कॉर्पोरेशन योगदान वजा <br> <br>

                    मुद्रांक शुल्क <br> <br>
                    @endif
                </td>

                <td>
                    <b>
                        @if(request()->lang == "en")
                        {{ $grand_total_pf }} <br> <br> {{ $grand_total_bank_loan }} <br> <br> {{ $grand_total_lic }} <br> <br> {{ $grand_total_festival_deduction }} <br> <br> {{ $grand_total_employee_share }} <br> <br> {{ $grand_total_corporation_share }} <br> <br> {{ $grand_total_stamp_duty }} <br> <br>
                        @else
                        {{ convertToMarathiNumber($grand_total_pf) }} <br> <br> {{ convertToMarathiNumber($grand_total_bank_loan) }} <br> <br> {{ convertToMarathiNumber($grand_total_lic) }} <br> <br> {{ convertToMarathiNumber($grand_total_festival_deduction) }} <br> <br> {{ convertToMarathiNumber($grand_total_employee_share) }} <br> <br> {{ convertToMarathiNumber($grand_total_corporation_share) }} <br> <br> {{ convertToMarathiNumber($grand_total_stamp_duty) }} <br> <br>
                        @endif
                    </b>
                </td>
            </tr>

        </tbody>

    </table>


</body>
</html>
