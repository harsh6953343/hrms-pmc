<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Supplementary PDF</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .page-break {
                page-break-after: always;
            }
            body {
                font-family: 'freeserif', 'normal';
            }
            #tableborder, #tableborder th, #tableborder td {
                border: 1px solid rgb(105, 102, 102);
                border-collapse: collapse;
            }
            #pdftrheading th {
                font-size: 11px;
            }
            tbody td{
                padding-right: 3px;
            }
            #tableborder tbody td{
                font-size: 13px;
            }
        </style>
    </head>
    <body>
        <section style="border: 1px solid; height:750px; padding:20px;">
            <table style="width: 100%">
                <tr>
                    <td>
                        @php
                        $logoData = file_get_contents(public_path('logo.png'));
                        $base64Logo = base64_encode($logoData);
                        @endphp
                        <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="140" width="150">
                    </td>
                    <td>
                        <p style="font-size: 65px;font-weight:900">पनवेल महानगरपालिका</p>
                    </td>
                </tr>
            </table>
        </section>
        <div class="page-break"></div>

        <section>
            <div>
                @php
                    $count = 1;


                    $suplimentryEarnPay = 0;
                    $suplimentryLeavePay = 0;
                    $suplimentryGradePay = 0;
                    $suplimentryDearnessAllowance = 0;
                    $suplimentryHRA = 0;
                    $suplimentryCityAllowance = 0;
                    $suplimentryVehicalAllowance = 0;
                    $suplimentryDcpsICorporationDeduction = 0;
                    $suplimentryWashingAllowance = 0;
                    $suplimentryDeputAllowance = 0;
                    $suplimentryEnceragementAllowance = 0;
                    $suplimentryMaharastraLabourCorporationAllowance = 0;
                    $suplimentryOtherAllowance = 0;
                    $suplimentryTotalEarning = 0;
                    $suplimentryGeneralProvidentFund = 0;
                    $suplimentryDCPSCorporationDeduction = 0;
                    $suplimentryGeneralProvidentFundAdvance = 0;
                    $suplimentryDCPSEmployeeContribution = 0;
                    $suplimentryHomeLoan = 0;
                    $suplimentryFestivalAdvance = 0;
                    $suplimentryEmployeeCreditSocity = 0;
                    $suplimentryLic = 0;
                    $suplimentryProTax = 0;
                    $suplimentryIncomeTax = 0;
                    $suplimentryGroupInsurance = 0;
                    $suplimentrySalaryRecovery = 0;
                    $suplimentryRevenueStamp = 0;
                    $suplimentryBankRecovery = 0;
                    $suplimentryServiceCharge = 0;
                    $suplimentryAccidentalIssurance = 0;
                    $suplimentryMaharastraLabourCorporationDeduction = 0;
                    $suplimentryMaharastraLabourEmployeeDeduction = 0;
                    $suplimentryOtherDeduction = 0;
                    $suplimentryTotalDeduction = 0;
                    $suplimentryNetSalary = 0;
                @endphp
                @foreach($suplimentries as $suplimentry)

                @php
                    // employee salary allowance
                    $employeeSalaryAllowanceId = explode(',', $suplimentry->employee_salary_allowance_id);
                    $employeeSalaryAllowanceAmt = explode(',', $suplimentry->employee_salary_allowance_amount);
                    $employeeSalaryAllowanceArray = array_combine($employeeSalaryAllowanceId, $employeeSalaryAllowanceAmt);

                    // employee salary deduction
                    $employeeSalaryDeductionId = explode(',', $suplimentry->employee_salary_deduction_id);
                    $employeeSalaryDeductionAmt = explode(',', $suplimentry->employee_salary_deduction_amount);
                    $employeeSalaryDeductionArray = array_combine($employeeSalaryDeductionId, $employeeSalaryDeductionAmt);


                    // freeze attendance allowance
                    $freezeAallowanceId = explode(',', $suplimentry->freeze_attendance_allowance_id);
                    $freezeAllowanceAmt = explode(',', $suplimentry->freeze_attendance_allowance_amount);
                    $freezeAllowanceArray = array_combine($freezeAallowanceId, $freezeAllowanceAmt);

                    // freeze attendance deduction
                    $freezeDeductionId = explode(',', $suplimentry->freeze_attendance_deduction_id);
                    $freezeDeductionAmt = explode(',', $suplimentry->freeze_attendance_deduction_amount);
                    $freezeDeductionArray = array_combine($freezeDeductionId, $freezeDeductionAmt);

                    // for suplimentry
                    // suplimentry attendance allowance
                    $suplimentryAllowanceId = explode(',', $suplimentry->allowance_id);
                    $suplimentryAllowanceAmount = explode(',', $suplimentry->allowance_amount);
                    $suplimentryAllowanceArray = array_combine($suplimentryAllowanceId, $suplimentryAllowanceAmount);

                    // suplimentry attendance deduction
                    $suplimentryDeductionId = explode(',', $suplimentry->deduction_id);
                    $suplimentryDeductionAmount = explode(',', $suplimentry->deduction_amount);
                    $suplimentryDeductionArray = array_combine($suplimentryDeductionId, $suplimentryDeductionAmount);

                @endphp
                @if($count % 2 == 1)
                <table style="width: 100%" id="tableborder">
                    <thead>
                        <tr>
                            <td colspan="15" style="text-align:center">
                                <h1 style="font-weight: 900; font-size: 35px;">पनवेल महानगरपालिका</h1>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8"></td>
                            <td colspan="7" style="text-align:right">
                                <p  style="font-size: 18px;font-weight:700">पुरवणी वेतन(महानगरपालिका) - बिल क्र. {{ $suplimentry->suplimentryLeaveBill?->bill_no }} &nbsp;&nbsp; दिनांक :- {{ date('d-m-Y', strtotime($suplimentry->suplimentryLeaveBill?->bill_date)) }}&nbsp;&nbsp;&nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="text-align:center">वेतन व भते</td>
                            <td colspan="7" style="text-align:center">
                                कपात
                            </td>
                        </tr>
                        <tr style="text-align: center;" id="pdftrheading">
                            <th style="padding: 7px 0px;font-size: 13px;">अनु क्र.</th>
                            <th>&nbsp;&nbsp;</th>
                            <th style="padding: 7px 0px;font-size: 13px;">मूळ वेतन <br>ग्रेड पे<br> हजार दिवस</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">अर्जित वेतन <br> रजा वेतन <br> ग्रेड वेतन</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">महागाई भत्ता <br> घर भाडे भत्ता <br> शहर भत्ता</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">प्रवास-भत्ता <br>अंनिवयो-मपा</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">धुलाई-भत्ता <br> प्रति.-भत्ता <br>प्रोत्सा.-भत्ता</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">म-का-क-मं-कॉ-भ <br>इतर-भत्ते <br> एकूण-वेतन-व-भत्ते</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">भ-नि-नि-वर्गणी <br>भ-नि-नि-अग्रीम <br>अंनिवयो-क</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">अंनिवयो-मपा <br>गृहकर्ज <br>सण-अग्रीम</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">पतसंस्था <br>विमा-हप्ते <br>व्यव.-कर</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">आयकर <br>गट-विमा-यो <br>वेतन-वसुली</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">मुद्रांक-शुल्क <br>बँक-कपात <br>अनु.-शुल्क</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">अपघात-विमा <br>म-का-क-मं-क-क <br>म-का-क-मं-कॉ-क</th>
                            <th  style="padding: 7px 0px;font-size: 13px;">इतर-कपात <br>एकूण-कपात <br> निव्वळ -देय</th>
                        </tr>
                        <tr>
                            <td colspan="15" style="text-align: right">देयकाचे वर्णनः supplementary&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                    @endif
                        @if($count % 2 == 0)
                        <tr>
                            <td colspan="15">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="15">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="15">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="15">&nbsp;</td>
                        </tr>
                        @endif
                        <tr>
                            <td colspan="5">&nbsp;&nbsp;&nbsp;कर्मचारी नावः {{ $suplimentry?->employee?->fname." ".$suplimentry?->employee?->mname." ".$suplimentry?->employee?->lname }} ({{ $suplimentry?->employee?->employee_id }})</td>
                            <td colspan="5">&nbsp;&nbsp;&nbsp;वेतन देयक : {{ $suplimentry?->suplimentryLeaveBill?->supplimentaryBill?->ward?->name }} </td>
                            <td colspan="5">&nbsp;&nbsp;&nbsp;{{ $suplimentry?->employee?->bank?->name }}: {{ $suplimentry?->employee?->account_no }}</td>
                        </tr>
                        <tr>
                            <td colspan="15">&nbsp;&nbsp;&nbsp;वेतन प्रकार : supplementary</td>
                        </tr>

                        <tr style="text-align: right;">
                            @php
                            $monthMrName = ['01' => 'जानेवारी', '02' => 'फेब्रुवारी', '03' => 'मार्च', '04' => 'एप्रिल', '05' => 'मे', '06' => 'जून', '07' => 'जुलै', '08' => 'ऑगस्ट', '09' => 'सप्टेंबर', '10' => 'ऑक्टोबर', '11' => 'नोहेंबर', '12' => 'डिसेंबर'];
                            @endphp
                            <td colspan="2" style="text-align: center;font-size: 15px;">{{ $monthMrName[date('m', strtotime($suplimentry->month))] }}, {{ date('Y', strtotime($suplimentry->month)) }}</td>
                            <td>
                                <!-- basic salary -->
                                {{ round($suplimentry->employee_salary_basic_salary) }}
                            </td>
                            <td>
                                <!-- Earn Pay -->
                                {{ round($suplimentry->employee_salary_basic_salary) }}
                            </td>
                            <td>
                                <!-- Dearness Allowance -->
                                {{ round($employeeSalaryAllowanceArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Vehical Allowance -->
                                {{ round($employeeSalaryAllowanceArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- Washing Allowance -->
                                {{ round($employeeSalaryAllowanceArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION ALLOWANCE -->
                                {{ round($employeeSalaryAllowanceArray[10] ?? 0) }}

                            </td>
                            <td>
                                <!-- GENERAL PROVIDENT FUND -->
                                {{ round($employeeSalaryDeductionArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCP I Corporation Contribution Deduction -->
                                {{ round($employeeSalaryDeductionArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- Employee Credit Socity  -->
                                {{ round($employeeSalaryDeductionArray[10] ?? 0) }}
                            </td>
                            <td>
                                <!-- Income Tax -->
                                {{ ($suplimentry?->employee?->ward_id == "5") ? round($employeeSalaryDeductionArray[23] ?? 0) : round($employeeSalaryDeductionArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- Revenue Stamp -->
                                {{ round($employeeSalaryDeductionArray[22] ?? 0) }}
                            </td>
                            <td>
                                <!-- ACCIDENTAL INSURANCE PREMIUM -->
                                {{ round($employeeSalaryDeductionArray[17] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Deduction -->
                                0
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td colspan="2" rowspan="2" style="text-align: center;font-size: 15px;">देय</td>
                            <td>
                                <!-- Grade Pay -->
                                {{ round($employeeSalaryAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Leave Pay -->
                                {{ round($employeeSalaryAllowanceArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- House Rent -->
                                {{ round($employeeSalaryAllowanceArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Corporation Contribution -->
                                {{ round($employeeSalaryAllowanceArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Deput Allowance -->
                                {{ round($employeeSalaryAllowanceArray[13] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Allowance -->
                                {{ round($employeeSalaryAllowanceArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- General Provision Fund Advance -->
                                {{ round($employeeSalaryDeductionArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Home Loan -->
                                {{ round($employeeSalaryDeductionArray[20] ?? 0) }}
                            </td>
                            <td>
                                <!-- LIC -->
                                {{ round($employeeSalaryDeductionArray[11] ?? 0) }}
                            </td>
                            <td>
                                <!-- Group Issurance -->
                                {{ round($employeeSalaryDeductionArray[21] ?? 0) }}
                            </td>
                            <td>
                                <!-- Bank Recovery -->

                                @php
                                    $bankRecovery1 = $employeeSalaryDeductionArray[14] ?? 0;

                                    $bankRecovery2 = $employeeSalaryDeductionArray[15] ?? 0;
                                @endphp
                                {{ round($bankRecovery1 + $bankRecovery2) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION DEDUCTION -->
                                {{ round($employeeSalaryDeductionArray[19] ?? 0) }}
                            </td>
                            <td>
                                <!-- Total Deduction -->
                                {{ round($suplimentry->employee_total_deduction ?? 0) }}
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>
                                <!-- Day of Attendance -->
                                {{ cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($suplimentry->month)), date('Y', strtotime($suplimentry->month))) }}
                            </td>
                            <td>
                                <!-- Grade Pay -->
                                {{ round($employeeSalaryAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- City Allowance -->
                                {{ round($employeeSalaryAllowanceArray[4] ?? 0) }}
                            </td>
                            <td>0</td>
                            <td>
                                <!-- Enceragement Allowance -->
                                0
                            </td>
                            <td>
                                <!-- Total Earning -->
                                {{ round($suplimentry->employee_total_earning ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Employee Contribution -->
                                {{ round($employeeSalaryDeductionArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- Festival Advance -->
                                {{ round($employeeSalaryDeductionArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- ProFESIONAL Tax -->
                                {{ round($employeeSalaryDeductionArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Salary Recovery -->
                                {{ round($employeeSalaryDeductionArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Service Charge -->
                                {{ round($employeeSalaryDeductionArray[16] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD EMPLOYEE DEDUCTION -->
                                {{ round($employeeSalaryDeductionArray[18] ?? 0) }}
                            </td>
                            <td>
                                <!-- Net Salary -->
                                {{ round($suplimentry->employee_salary_net_salary ?? 0) }}
                            </td>
                        </tr>

                        <tr style="text-align: right;">
                            <td colspan="2" rowspan="3" style="text-align: center;font-size: 15px;">दिलेले</td>
                            <td>{{ round($suplimentry->freeze_attendance_basic_salary) }}</td>

                            <td>
                                <!-- Earn Pay -->
                                {{ round($suplimentry->freeze_attendance_basic_salary) }}
                            </td>
                            <td>
                                <!-- Dearness Allowance -->
                                {{ round($freezeAllowanceArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Vehical Allowance -->
                                {{ round($freezeAllowanceArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- Washing Allowance -->
                                {{ round($freezeAllowanceArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION ALLOWANCE -->
                                {{ round($freezeAllowanceArray[10] ?? 0) }}
                            </td>
                            <td>
                                <!-- GENERAL PROVIDENT FUND -->
                                {{ round($freezeDeductionArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCP I Corporation Contribution Deduction -->
                                {{ round($freezeDeductionArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- Employee Credit Socity  -->
                                {{ round($freezeDeductionArray[10] ?? 0) }}
                            </td>
                            <td>
                                <!-- Income Tax -->
                                {{ ($suplimentry?->employee?->ward_id == "5") ? round($freezeDeductionArray[23] ?? 0) : round($freezeDeductionArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- Revenue Stamp -->
                                {{ round($freezeDeductionArray[22] ?? 0) }}
                            </td>
                            <td>
                                <!-- ACCIDENTAL INSURANCE PREMIUM -->
                                {{ round($freezeDeductionArray[17] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Deduction -->
                                0
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>
                                <!-- Grade Pay -->
                                {{ round($freezeAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Leave Pay -->
                                {{ round($freezeAllowanceArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- HRA -->
                                {{ round($freezeAllowanceArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Corporation Contribution -->
                                {{ round($freezeAllowanceArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Deput Allowance -->
                                {{ round($freezeAllowanceArray[13] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Allowance -->
                                 {{ round($freezeAllowanceArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- General Provision Fund Advance -->
                                {{ round($freezeDeductionArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Home Loan -->
                                {{ round($freezeDeductionArray[20] ?? 0) }}
                            </td>
                            <td>
                                <!-- LIC -->
                                {{ round($freezeDeductionArray[11] ?? 0) }}
                            </td>
                            <td>
                                <!-- Group Issurance -->
                                {{ round($freezeDeductionArray[21] ?? 0) }}
                            </td>
                            <td>
                                <!-- Bank Recovery -->
                                @php
                                    $bankRecovery1 = $freezeDeductionArray[14] ?? 0;

                                    $bankRecovery2 = $freezeDeductionArray[15] ?? 0;
                                @endphp
                                {{ round($bankRecovery1 + $bankRecovery2) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION DEDUCTION -->
                                {{ round($freezeDeductionArray[19] ?? 0) }}
                            </td>
                            <td>
                                <!-- Total Deduction -->
                                {{ round($suplimentry->freeze_total_deduction ?? 0) }}
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>{{ $suplimentry->freeze_attendance_present_day }}</td>
                            <td>
                                <!-- Grade pay -->
                                {{ round($freezeAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- City Allowance -->
                                {{ round($freezeAllowanceArray[4] ?? 0) }}
                            </td>
                            <td>0</td>
                            <td>
                                <!-- Enceragement Allowance -->
                                0
                            </td>
                            <td>
                                <!-- Total Earning -->
                                {{ round($suplimentry->freeze_total_earning ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Employee Contribution -->
                                {{ round($freezeDeductionArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- Festival Advance -->
                                {{ round($freezeDeductionArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- ProFESIONAL Tax -->
                                {{ round($freezeDeductionArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Salary Recovery -->
                                {{ round($freezeDeductionArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Service Charge -->
                                {{ round($freezeDeductionArray[16] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD EMPLOYEE DEDUCTION -->
                                {{ round($freezeDeductionArray[18] ?? 0) }}
                            </td>
                            <td>
                                <!-- Net Salary -->
                                {{ round($suplimentry->freeze_attendance_net_salary ?? 0) }}
                            </td>
                        </tr>

                        <tr style="text-align: right;">
                            <td colspan="2" rowspan="3" style="text-align: center;font-size: 15px;">फरक</td>
                            <td>{{ round($suplimentry->basic_salary) }}</td>

                            <td>
                                <!-- Earn Pay -->
                                @php
                                // $earnPay = $suplimentry->basic_salary ?? 0;
                                // $suplimentryEarnPay = $suplimentryEarnPay + $earnPay;
                                $suplimentryEarnPay = $suplimentryEarnPay + $suplimentry->basic_salary;
                                @endphp
                                {{ round($suplimentry->basic_salary) }}
                            </td>
                            <td>
                                <!-- Dearness Allowance -->
                                @php
                                $da = $suplimentryAllowanceArray[1] ?? 0;
                                $suplimentryDearnessAllowance = $suplimentryDearnessAllowance + $da;
                                @endphp
                                {{ round($suplimentryAllowanceArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Vehical Allowance -->
                                @php
                                $vehical = $suplimentryAllowanceArray[5] ?? 0;
                                $suplimentryVehicalAllowance = $suplimentryVehicalAllowance + $vehical;
                                @endphp
                                {{ round($suplimentryAllowanceArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- Washing Allowance -->
                                @php
                                $washing = $suplimentryAllowanceArray[7] ?? 0;
                                $suplimentryWashingAllowance = $suplimentryWashingAllowance + $washing;
                                @endphp
                                {{ round($suplimentryAllowanceArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION ALLOWANCE -->
                                @php
                                $dcps = $suplimentryAllowanceArray[10] ?? 0;
                                $suplimentryMaharastraLabourCorporationAllowance = 0;
                                @endphp
                                0
                                {{-- {{ round($suplimentryAllowanceArray[10] ?? 0) }} --}}
                            </td>
                            <td>
                                <!-- GENERAL PROVIDENT FUND -->
                                @php
                                $generalProvidentFund = $suplimentryDeductionArray[5] ?? 0;
                                $suplimentryGeneralProvidentFund = $suplimentryGeneralProvidentFund + $generalProvidentFund;
                                @endphp
                                {{ round($suplimentryDeductionArray[5] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCP I Corporation Contribution Deduction -->
                                @php
                                $dcps = $suplimentryDeductionArray[8] ?? 0;
                                $suplimentryDCPSCorporationDeduction = $suplimentryDCPSCorporationDeduction + $dcps;
                                @endphp
                                {{ round($suplimentryDeductionArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- Employee Credit Socity  -->
                                @php
                                $employeeSoci = $suplimentryDeductionArray[10] ?? 0;
                                $suplimentryEmployeeCreditSocity = $suplimentryEmployeeCreditSocity + $employeeSoci;
                                @endphp
                                {{ round($suplimentryDeductionArray[10] ?? 0) }}
                            </td>
                            <td>
                                <!-- Income Tax -->
                                @php
                                $incomTax = ($suplimentry?->employee?->ward_id == "5") ? $suplimentryDeductionArray[23] ?? 0 : $suplimentryDeductionArray[2] ?? 0;
                                $suplimentryIncomeTax = $suplimentryIncomeTax + $incomTax;
                                @endphp
                                {{ ($suplimentry?->employee?->ward_id == "5") ? round($suplimentryDeductionArray[23] ?? 0) : round($suplimentryDeductionArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- Revenue Stamp -->
                                @php
                                $revenueStamp = $suplimentryDeductionArray[22] ?? 0;
                                $suplimentryRevenueStamp = $suplimentryRevenueStamp + $revenueStamp;
                                @endphp
                                {{ round($suplimentryDeductionArray[22] ?? 0) }}
                            </td>
                            <td>
                                <!-- ACCIDENTAL INSURANCE PREMIUM -->
                                @php
                                $accidental = $suplimentryDeductionArray[17] ?? 0;
                                $suplimentryAccidentalIssurance = $suplimentryAccidentalIssurance + $accidental;
                                @endphp
                                {{ round($suplimentryDeductionArray[17] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Deduction -->
                                0
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>
                                <!-- Grade Pay -->
                                @php
                                $gradePay = $suplimentryAllowanceArray[12] ?? 0;
                                $suplimentryGradePay = $suplimentryGradePay + $gradePay;
                                @endphp
                                {{ round($suplimentryAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Leave Pay -->
                                @php
                                $leavePay = $suplimentryAllowanceArray[8] ?? 0;
                                $suplimentryLeavePay = $suplimentryLeavePay + $leavePay;
                                @endphp
                                {{ round($suplimentryAllowanceArray[8] ?? 0) }}
                            </td>
                            <td>
                                <!-- House Rent -->
                                @php
                                $hra = $suplimentryAllowanceArray[2] ?? 0;
                                $suplimentryHRA = $suplimentryHRA + $hra;
                                @endphp
                                {{ round($suplimentryAllowanceArray[2] ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Corporation Contribution -->
                                @php
                                $hra = $suplimentryAllowanceArray[6] ?? 0;
                                $suplimentryDcpsICorporationDeduction = $suplimentryDcpsICorporationDeduction + $hra;
                                @endphp
                                {{ round($suplimentryAllowanceArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Deput Allowance -->
                                @php
                                $deputAllowance = $suplimentryAllowanceArray[13] ?? 0;
                                $suplimentryDeputAllowance = $suplimentryDeputAllowance + $deputAllowance;
                                @endphp
                                {{ round($suplimentryAllowanceArray[13] ?? 0) }}
                            </td>
                            <td>
                                <!-- Other Allowance -->
                                @php
                                $otherAllowance = $suplimentryAllowanceArray[9] ?? 0;
                                $suplimentryOtherAllowance = $suplimentryOtherAllowance + $otherAllowance;
                                @endphp
                                 {{ round($suplimentryAllowanceArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- General Provision Fund Advance -->
                                @php
                                $generalProvisionFundAdvance = $suplimentryDeductionArray[6] ?? 0;
                                $suplimentryGeneralProvidentFundAdvance = $suplimentryGeneralProvidentFundAdvance + $generalProvisionFundAdvance;
                                @endphp
                                {{ round($suplimentryDeductionArray[6] ?? 0) }}
                            </td>
                            <td>
                                <!-- Home Loan -->
                                @php
                                $homeLoan = $suplimentryDeductionArray[20] ?? 0;
                                $suplimentryHomeLoan = $suplimentryHomeLoan + $homeLoan;
                                @endphp
                                {{ round($suplimentryDeductionArray[20] ?? 0) }}
                            </td>
                            <td>
                                <!-- LIC -->
                                @php
                                $lic = $suplimentryDeductionArray[11] ?? 0;
                                $suplimentryLic = $suplimentryLic + $lic;
                                @endphp
                                {{ round($suplimentryDeductionArray[11] ?? 0) }}
                            </td>
                            <td>
                                <!-- Group Issurance -->
                                @php
                                $groupInsurance = $suplimentryDeductionArray[21] ?? 0;
                                $suplimentryGroupInsurance = $suplimentryGroupInsurance + $groupInsurance;
                                @endphp
                                {{ round($suplimentryDeductionArray[21] ?? 0) }}
                            </td>
                            <td>
                                <!-- Bank Recovery -->
                                @php
                                    $bankRecovery1 = $suplimentryDeductionArray[14] ?? 0;

                                    $bankRecovery2 = $suplimentryDeductionArray[15] ?? 0;

                                $suplimentryBankRecovery = $suplimentryBankRecovery + $bankRecovery1 + $bankRecovery2;
                                @endphp
                                {{ round($bankRecovery1 + $bankRecovery2) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION DEDUCTION -->
                                @php
                                $corporationDeduction = $suplimentryDeductionArray[19] ?? 0;
                                $suplimentryMaharastraLabourCorporationDeduction = $suplimentryMaharastraLabourCorporationDeduction + $corporationDeduction;
                                @endphp
                                {{ round($suplimentryDeductionArray[19] ?? 0) }}
                            </td>
                            <td>
                                <!-- Total Deduction -->
                                @php
                                $totalDeduction = $suplimentry->suplimentary_total_deduction ?? 0;
                                $suplimentryTotalDeduction = $suplimentryTotalDeduction + $totalDeduction;
                                @endphp
                                {{ round($suplimentry->suplimentary_total_deduction ?? 0) }}
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>{{ $suplimentry->supplimentry_pr + $suplimentry->supplimentry_ul + $suplimentry->supplimentry_ml + $suplimentry->supplimentry_el }}</td>
                            <td>
                                <!-- Grade pay -->
                                @php
                                $totalDeduction = $suplimentryAllowanceArray[12] ?? 0;
                                $suplimentryGradePay = $suplimentryGradePay + $totalDeduction;
                                @endphp
                                {{ round($suplimentryAllowanceArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- City Allowance -->
                                @php
                                $cityAllowance = $suplimentryAllowanceArray[4] ?? 0;
                                $suplimentryCityAllowance = $suplimentryCityAllowance + $cityAllowance;
                                @endphp
                                {{ round($suplimentryAllowanceArray[4] ?? 0) }}
                            </td>
                            <td>0</td>
                            <td>
                                <!-- Enceragement Allowance -->
                                0
                            </td>
                            <td>
                                <!-- Total Earning -->
                                @php
                                $totalEarning = $suplimentry->suplimentary_total_earning ?? 0;
                                $suplimentryTotalEarning = $suplimentryTotalEarning + round($totalEarning);
                                @endphp
                                {{ round($suplimentry->suplimentary_total_earning ?? 0) }}
                            </td>
                            <td>
                                <!-- DCPS I Employee Contribution -->
                                @php
                                $enployeeContribution = $suplimentryDeductionArray[7] ?? 0;
                                $suplimentryDCPSEmployeeContribution = $suplimentryDCPSEmployeeContribution + $enployeeContribution;
                                @endphp
                                {{ round($suplimentryDeductionArray[7] ?? 0) }}
                            </td>
                            <td>
                                <!-- Festival Advance -->
                                @php
                                $festivalAdvace = $suplimentryDeductionArray[9] ?? 0;
                                $suplimentryFestivalAdvance = $suplimentryFestivalAdvance + $festivalAdvace;
                                @endphp
                                {{ round($suplimentryDeductionArray[9] ?? 0) }}
                            </td>
                            <td>
                                <!-- ProFESIONAL Tax -->
                                @php
                                $profesionTax = $suplimentryDeductionArray[1] ?? 0;
                                $suplimentryProTax = $suplimentryProTax + $profesionTax;
                                @endphp
                                {{ round($suplimentryDeductionArray[1] ?? 0) }}
                            </td>
                            <td>
                                <!-- Salary Recovery -->
                                @php
                                $salaryRecovery = $suplimentryDeductionArray[12] ?? 0;
                                $suplimentrySalaryRecovery = $suplimentrySalaryRecovery + $salaryRecovery;
                                @endphp
                                {{ round($suplimentryDeductionArray[12] ?? 0) }}
                            </td>
                            <td>
                                <!-- Service Charge -->
                                @php
                                $serviceCharge = $suplimentryDeductionArray[16] ?? 0;
                                $suplimentryServiceCharge = $suplimentryServiceCharge + $serviceCharge;
                                @endphp
                                {{ round($suplimentryDeductionArray[16] ?? 0) }}
                            </td>
                            <td>
                                <!-- MAHARASHTRA LABOUR WELFARE BOARD EMPLOYEE DEDUCTION -->
                                @php
                                $laborDeduction = $suplimentryDeductionArray[18] ?? 0;
                                $suplimentryMaharastraLabourEmployeeDeduction = $suplimentryMaharastraLabourEmployeeDeduction + $laborDeduction;
                                @endphp
                                {{ round($suplimentryDeductionArray[18] ?? 0) }}
                            </td>
                            <td>
                                <!-- Net Salary -->
                                @php
                                $netSalary = $suplimentry->net_salary ?? 0;
                                $suplimentryNetSalary = $suplimentryNetSalary + round($netSalary);
                                @endphp
                                {{ round($suplimentry->net_salary ?? 0) }}
                            </td>
                        </tr>


                        @if($count % 2 == 0 || count($suplimentries) == $loop->iteration)
                    </tbody>
                </table>
                @else
                <div class="page-break"></div>
                @endif
                @php $count = $count + 1; @endphp
                @endforeach


            </div>

        </section>

        <div class="page-break"></div>
        <section>
            <table style="width: 100%" id="tableborder">
                <thead>
                    <tr style="text-align: center;" id="pdftrheading">
                        <th style="padding: 7px 0px;font-size: 13px;">अनु क्र.</th>
                        <th>&nbsp;&nbsp;</th>
                        <th style="padding: 7px 0px;font-size: 13px;">मूळ वेतन <br>ग्रेड पे<br> हजार दिवस</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">अर्जित वेतन <br> रजा वेतन <br> ग्रेड वेतन</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">महागाई भत्ता <br> घर भाडे भत्ता <br> शहर भत्ता</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">प्रवास-भत्ता <br>अंनिवयो-मपा</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">धुलाई-भत्ता <br> प्रति.-भत्ता <br>प्रोत्सा.-भत्ता</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">म-का-क-मं-कॉ-भ <br>इतर-भत्ते <br> एकूण-वेतन-व-भत्ते</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">भ-नि-नि-वर्गणी <br>भ-नि-नि-अग्रीम <br>अंनिवयो-क</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">अंनिवयो-मपा <br>गृहकर्ज <br>सण-अग्रीम</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">पतसंस्था <br>विमा-हप्ते <br>व्यव.-कर</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">आयकर <br>गट-विमा-यो <br>वेतन-वसुली</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">मुद्रांक-शुल्क <br>बँक-कपात <br>अनु.-शुल्क</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">अपघात-विमा <br>म-का-क-मं-क-क <br>म-का-क-मं-कॉ-क</th>
                        <th  style="padding: 7px 0px;font-size: 13px;">इतर-कपात <br>एकूण-कपात <br> निव्वळ -देय</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="text-align: right;">
                        <td colspan="3" style="text-align: center;font-size: 15px;">एकूण देय फरक</td>
                        <td>
                            <!-- Earn Pay -->
                            {{ round($suplimentryEarnPay) }}
                        </td>
                        <td>
                            <!-- Dearness Allowance -->
                            {{ round($suplimentryDearnessAllowance) }}
                        </td>
                        <td>
                            <!-- Vehical Allowance -->
                            {{ round($suplimentryVehicalAllowance) }}
                        </td>
                        <td>
                            <!-- Washing Allowance -->
                            {{ round($suplimentryWashingAllowance) }}
                        </td>
                        <td>
                            <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION ALLOWANCE -->
                            {{ round($suplimentryMaharastraLabourCorporationAllowance) }}

                        </td>
                        <td>
                            <!-- GENERAL PROVIDENT FUND -->
                            {{ round($suplimentryGeneralProvidentFund) }}
                        </td>
                        <td>
                            <!-- DCP I Corporation Contribution Deduction -->
                            {{ round($suplimentryDCPSCorporationDeduction) }}
                        </td>
                        <td>
                            <!-- Employee Credit Socity  -->
                            {{ round($suplimentryEmployeeCreditSocity) }}
                        </td>
                        <td>
                            <!-- Income Tax -->
                            {{ round($suplimentryIncomeTax) }}
                        </td>
                        <td>
                            <!-- Revenue Stamp -->
                            {{ round($suplimentryRevenueStamp) }}
                        </td>
                        <td>
                            <!-- ACCIDENTAL INSURANCE PREMIUM -->
                            {{ round($suplimentryAccidentalIssurance) }}
                        </td>
                        <td>
                            <!-- Other Deduction -->
                            0
                        </td>
                    </tr>
                    <tr style="text-align: right;">
                        <td colspan="3" style="text-align: center;font-size: 15px;"></td>
                        <td>
                            <!-- Leave Pay -->
                            {{ round($suplimentryLeavePay) }}
                        </td>
                        <td>
                            <!-- House Rent -->
                            {{ round($suplimentryHRA) }}
                        </td>
                        <td>
                            <!-- DCPS I Corporation Contribution -->
                            {{ round($suplimentryDcpsICorporationDeduction) }}
                        </td>
                        <td>
                            <!-- Deput Allowance -->
                            {{ round($suplimentryDeputAllowance) }}
                        </td>
                        <td>
                            <!-- Other Allowance -->
                            {{ round($suplimentryOtherAllowance) }}
                        </td>
                        <td>
                            <!-- General Provision Fund Advance -->
                            {{ round($suplimentryGeneralProvidentFundAdvance) }}
                        </td>
                        <td>
                            <!-- Home Loan -->
                            {{ round($suplimentryHomeLoan) }}
                        </td>
                        <td>
                            <!-- LIC -->
                            {{ round($suplimentryLic) }}
                        </td>
                        <td>
                            <!-- Group Issurance -->
                            {{ round($suplimentryGroupInsurance) }}
                        </td>
                        <td>
                            <!-- Bank Recovery -->
                            {{ round($suplimentryBankRecovery) }}
                        </td>
                        <td>
                            <!-- MAHARASHTRA LABOUR WELFARE BOARD CORPORATION DEDUCTION -->
                            {{ round($suplimentryMaharastraLabourCorporationDeduction) }}
                        </td>
                        <td>
                            <!-- Total Deduction -->
                            {{ round($suplimentryTotalDeduction) }}
                        </td>
                    </tr>
                    <tr style="text-align: right;">
                        <td colspan="3" style="text-align: center;font-size: 15px;"></td>
                        <td>
                            <!-- Grade Pay -->
                            {{ round($suplimentryGradePay) }}
                        </td>
                        <td>
                            <!-- City Allowance -->
                            {{ round($suplimentryCityAllowance) }}
                        </td>
                        <td>0</td>
                        <td>
                            <!-- Enceragement Allowance -->
                            0
                        </td>
                        <td>
                            <!-- Total Earning -->
                            {{ round($suplimentryTotalEarning) }}
                        </td>
                        <td>
                            <!-- DCPS I Employee Contribution -->
                            {{ round($suplimentryDCPSEmployeeContribution) }}
                        </td>
                        <td>
                            <!-- Festival Advance -->
                            {{ round($suplimentryFestivalAdvance) }}
                        </td>
                        <td>
                            <!-- ProFESIONAL Tax -->
                            {{ round($suplimentryProTax) }}
                        </td>
                        <td>
                            <!-- Salary Recovery -->
                            {{ round($suplimentrySalaryRecovery) }}
                        </td>
                        <td>
                            <!-- Service Charge -->
                            {{ round($suplimentryServiceCharge) }}
                        </td>
                        <td>
                            <!-- MAHARASHTRA LABOUR WELFARE BOARD EMPLOYEE DEDUCTION -->
                            {{ round($suplimentryMaharastraLabourEmployeeDeduction) }}
                        </td>
                        <td>
                            <!-- Net Salary -->
                            {{ round($suplimentryNetSalary) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div>
                <table style="width: 100%; align:center">
                    <thead>
                        <tr>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th style="font-size:20px; word-spacing: 2px">आस्थापना प्रमुख <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">सहा. आयुक्त (आस्थापना) <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">उपआयुक्त (मुख्यालय) <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">मुख्यलेखाअधिकारी <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">उपआयुक्त (मुख्यालय) <br> पनवेल महानगरपालिका</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </section>

    </body>
</html>


