<x-admin.layout>
    <x-slot name="title">Generate Supplimentary Bill PDF</x-slot>
    <x-slot name="heading">Generate Supplimentary Bill PDF</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('reports.suplimentry-slip') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data" method="GET">
                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="bill_no">Bill No<span class="text-danger">*</span></label>
                                <select class="form-select" id="bill_no" id="bill_no" name="bill_no" required>
                                    <option value="">Select Bill No</option>
                                    @foreach ($suplimentaryLeaveBills as $suplimentaryLeaveBill)
                                        <option value="{{ $suplimentaryLeaveBill->id }}">{{ $suplimentaryLeaveBill->bill_no }}</option>
                                    @endforeach
                                </select>
                                @error('department')
                                <span class="text-danger invalid department_err">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit1">PDF</button>
                        <button type="button" class="btn btn-success" id="excelSubmit1">Excel</button>
                        <a href="{{ route('reports.suplimentry-slip') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });

    $('#excelSubmit1').click(function(){
        let bill_no = $('#bill_no').val();
        if(bill_no == ""){
            alert('Please select bill no');
            return false;
        }
        let url = "{{ route('reports.suplimentry-slip.excel') }}?bill_no="+bill_no;
        window.location.href = url;
    });
    </script>
