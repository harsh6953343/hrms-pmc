@php
    use App\Models\SupplimentaryBill;
    use App\Models\RemainingFreezeSalary;
@endphp
<x-admin.layout>
    <x-slot name="title">Deduction Report</x-slot>
    <x-slot name="heading">Deduction Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('deduction-report.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="deduction">Deductions<span class="text-danger">*</span></label>
                                <select class="form-select" id="deduction" name="deduction">
                                    @foreach ($deductions as $deduction)
                                        <option value="{{ $deduction->id }}" {{ ($deduction_id == $deduction->id ) ? 'Selected':'' }} >{{ $deduction->deduction }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid deduction_err"></span>
                            </div>

                            <div class="col-md-3 ward_div">
                                <label class="col-form-label" for="ward_id">Select Ward <span class="text-danger">*</span></label>
                                <select class="form-select ward_id" name="ward_id" required>
                                    <option value="">--Select Ward--</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}" @if(isset(request()->ward_id) && request()->ward_id == $ward->id) selected @endif>{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-3 department_div  @if(isset(request()->ward_id) && request()->ward_id = $ward->id) '' @else d-none  @endif">
                                <label class="col-form-label" for="department_id">Select Department</label>
                                <select class="form-select department_id" name="department">
                                    <option value="">--Select Department--</option>
                                    @if(count($departments) > 0)
                                    @foreach($departments as $department)
                                    <option value="{{ $department->id }}" @if(isset(request()->department) && request()->department == $department->id) selected @endif>{{ $department->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>

                            {{-- <div class="col-md-3">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <select class="form-select" id="department" name="department">
                                    <option value="">Select Department</option>
                                    <option value="" {{ (empty($department_id))?'Selected':'' }}>All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}" {{ ($department_id == $department->id ) ? 'Selected':'' }} >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_err"></span>
                            </div> --}}

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month" required>
                                    <option value="">Select Month</option>
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Show</button>
                        <button type="button" class="btn btn-success" id="pdfEnglishButton">PDF in English</button>
                        <button type="button" class="btn btn-success" id="pdfMarathiButton">PDF in Marathi</button>
                        <a href="{{ route('deduction-report.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Code </th>
                                    <th>Employee Name</th>
                                    <th>Contact Number</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Month</th>
                                    <th>Deduction Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($deduction_reports))
                                    @php $count = 1; @endphp
                                    @foreach ($deduction_reports as $row => $deduction_report)

                                        @php
                                            $explode_deduction_ids = explode(',', $deduction_report->deduction_Id);
                                            $explode_deduction_amt = explode(',', $deduction_report->deduction_Amt);

                                            $deduction_amt = 0;
                                            $newArr = array_combine($explode_deduction_ids, $explode_deduction_amt);

                                            $deduction_amt = $newArr[$deduction_id] ?? 0;


                                        @endphp


                                        @if($deduction_amt != 0)

                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td>{{ $deduction_report->Emp_Code }}</td>
                                            <td>{{ $deduction_report->emp_name }}</td>
                                            <td>{{ $deduction_report->phone_no }}</td>
                                            <td>{{ $deduction_report->employee?->ward?->name }}</td>
                                            <td>{{ $deduction_report->employee?->department?->name }}</td>
                                            <td>{{ $deduction_report->employee?->designation?->name }}</td>
                                            <td>{{ $monthName }}</td>
                                            <td>{{ $deduction_amt }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });


    $(".ward_id").on("change", function(e) {
        var ward_id = $(this).val();

        if(ward_id != ''){
            var url = "{{ route('fetch-department', ':ward_id') }}";

            $.ajax({
                url: url.replace(":ward_id", ward_id),
                type: "GET",
                data: {
                    _method: "GET",
                    _token: "{{ csrf_token() }}",
                },
                success: function(data, textStatus, jqXHR) {
                    if (!data.error && !data.error2) {
                        $(".department_id").html(data.deptHtml);
                        $('.department_div').removeClass('d-none');
                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }else{
            $('.department_div').addClass('d-none');
        }
    });

    $(document).ready(function(){
        $('#pdfEnglishButton').click(function(){
            let url = $('#addForm').serialize();
            window.open(
                '{{ route("deduction-report.pdf", "en") }}?'+url,
                '_blank'
            );
        });

        $('#pdfMarathiButton').click(function(){
            let url = $('#addForm').serialize();
            window.open(
                '{{ route("deduction-report.pdf", "mr") }}?'+url,
                '_blank'
            );
        });
    });
</script>
