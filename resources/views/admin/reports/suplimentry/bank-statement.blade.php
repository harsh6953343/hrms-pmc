<x-admin.layout>
    <x-slot name="title">Supplimentory Bank Statement</x-slot>
    <x-slot name="heading">Supplimentory Bank Statement</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="bill_no">Supplimentary Bill No<span class="text-danger">*</span></label>
                                <select class="form-select" id="bill_no" name="bill_no" required>
                                    <option value="">Select Bill No</option>
                                    @foreach ($suplimentryLeaveBills as $suplimentryLeaveBill)
                                        <option value="{{ $suplimentryLeaveBill->id }}" {{ (request()->bill_no == $suplimentryLeaveBill->id)?'Selected':'' }} >{{ $suplimentryLeaveBill->bill_no }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid bill_no_err"></span>
                            </div>


                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="button" class="btn btn-success" id="excelBtn">Excel</button>
                        <button type="button" class="btn btn-secondary" id="pdfBtn">PDF</button>
                        <a href="{{ route('bank-statement.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>IFSC Code</th>
                                    {{-- <th>Month</th> --}}
                                    <th>Bank A/C No.</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count = 1; @endphp
                                @foreach ($bankStatements as $bankStatement)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $bankStatement->emp_code }}</td>
                                        <td>{{ $bankStatement->employee?->fname." ".$bankStatement->employee?->mname." ".$bankStatement->employee?->lname }}</td>
                                        <td>{{ $bankStatement?->employee?->ifsc }}</td>
                                        <td>{{ $bankStatement?->employee?->account_no }}</td>
                                        <td>{{ round($bankStatement->total_salary) }}</td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });

    $('#excelBtn').click(function(){
        let url = $('#addForm').serialize();
        window.open(
            '{{ route("reports.suplimentry-bank-statement-excel") }}?'+url,
            '_blank' // <- This is what makes it open in a new window.
        );
    })
    $('#pdfBtn').click(function(){
        let url = $('#addForm').serialize();
        window.open(
            '{{ route("reports.suplimentry-bank-statement-pdf") }}?'+url,
            '_blank' // <- This is what makes it open in a new window.
        );
    })
    </script>
