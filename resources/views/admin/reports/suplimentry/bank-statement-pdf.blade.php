<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supplimentry Bank Statement</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .page-break {
            page-break-after: always;
        }
        body {
            font-family: 'freeserif', 'normal';
        }
        table{
            width: 100%;
        }
        #tableborder, #tableborder th, #tableborder td {
            border: 1px solid rgb(105, 102, 102);
            border-collapse: collapse;
        }
        tr th{
            font-size: 18px;
        }
        tr td{
            font-size: 18px;
        }
        tbody td{
            padding-right: 3px;
        }
        #tableborder tbody td{
            font-size: 13px;
        }
        thead {
            display: table-header-group; /* Force header to repeat */
        }

        tbody {
            display: table-row-group;
        }

        tr {
            page-break-inside: avoid; /* Avoid breaking rows */
            page-break-after: auto;
        }
    </style>
</head>
<body>
    <section style="border: 1px solid; height:800px; padding:20px;">
        <table style="width: 100%">
            <tr>
                <td>
                    @php
                    $logoData = file_get_contents(public_path('logo.png'));
                    $base64Logo = base64_encode($logoData);
                    @endphp
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="140" width="150">
                </td>
                <td>
                    <p style="font-size: 65px;font-weight:900">पनवेल महानगरपालिका</p>
                </td>
            </tr>
        </table>
    </section>

    <div class="page-break"></div>
    <table id="tableborder">
        <thead>
            <tr>
                <th>Sr No.</th>
                <th>Employee Id</th>
                <th>Employee Name</th>
                <th>IFSC Code</th>
                <th>Bank A/C No.</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($bankStatements as $bankStatement)
            <tr>
                <td style="font-size: 17px;padding-left:5px;">{{ $loop->iteration }}</td>
                <td style="font-size: 17px;padding-left:5px;">{{ $bankStatement->emp_code }}</td>
                <td style="font-size: 17px;padding-left:5px;">{{ $bankStatement->employee?->fname." ".$bankStatement->employee?->mname." ".$bankStatement->employee?->lname }}</td>
                <td style="font-size: 17px;padding-left:5px;">{{ $bankStatement?->employee?->ifsc }}</td>
                <td style="font-size: 17px;padding-left:5px;">{{ $bankStatement?->employee?->account_no }}</td>
                <td style="font-size: 17px;padding-left:5px;">{{ round($bankStatement->total_salary) }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1" align="center" style="font-weight: 600">
                    <br><br>
                    <br><br>
                    अधिक्षक<br>आस्थापना विभाग <br>पनवेल महानगरपालिका
                </td>

                <td colspan="1" align="center" style="font-weight: 600">

                    <br><br>
                    <br><br>
                    सहा. आयुक्त<br> आस्थापना विभाग<br> पनवेल महानगरपालिका
                </td>
                <td colspan="4"></td>
            </tr>
        </tfoot>
    </table>
</body>
</html>
