<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supplimentry Bank Statement</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="11" style="font-size:25px;text-align:center;padding:15px 6px;">पनवेल महानगरपालिका</th>
            </tr>
            <tr>
                <th>Sr No.</th>
                <th>Employee Id</th>
                <th>Employee Name</th>
                <th>IFSC Code</th>
                <th>Bank A/C No.</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @php $count = 0; @endphp
            @foreach($bankStatements as $bankStatement)
            <tr>
                <td>{{ $count++ }}</td>
                <td>{{ $bankStatement->emp_code }}</td>
                <td>{{ $bankStatement->employee?->fname." ".$bankStatement->employee?->mname." ".$bankStatement->employee?->lname }}</td>
                <td>{{ $bankStatement?->employee?->ifsc }}</td>
                <td>{{ $bankStatement?->employee?->account_no }}</td>
                <td>{{ round($bankStatement->total_salary) }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1" rowspan="7" align="center">
                    <br><br>
                    <br><br>
                    अधिक्षक<br>आस्थापना विभाग <br>पनवेल महानगरपालिका
                </td>

                <td colspan="1" rowspan="7" align="center">
                    <br><br>
                    <br><br>
                    सहा. आयुक्त<br> आस्थापना विभाग<br> पनवेल महानगरपालिका
                </td>
                <td colspan="4"></td>
            </tr>
        </tfoot>
    </table>
</body>
</html>
