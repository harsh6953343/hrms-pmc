@php
  use App\Models\EmployeeMonthlyLoan;
  use App\Models\SupplimentaryBill;
  use App\Models\RemainingFreezeSalary;
@endphp
<x-admin.layout>
    <x-slot name="title">Bank Deduction Employee Wise Report</x-slot>
    <x-slot name="heading">Bank Deduction Employee Wise Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('bank-deduction-employee-report.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <select class="form-select" id="department" name="department">
                                    <option value="">Select Department</option>
                                    <option value="" {{ (empty($department_id))?'Selected':'' }}>All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}" {{ ($department_id == $department->id ) ? 'Selected':'' }} >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="loan">Loans<span class="text-danger">*</span></label>
                                <select class="form-select" id="loan" name="loan">
                                    <option value="">Select Loans</option>
                                    <option value="" {{ (empty($loan_id))?'Selected':'' }}>All</option>
                                    @foreach ($loans as $loan)
                                        <option value="{{ $loan->id }}" {{ ($loan_id == $loan->id ) ? 'Selected':'' }}>{{ $loan->loan }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid loan_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <a href="{{ route('bank-deduction-employee-report.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Loan</th>
                                    <th>Amount</th>
                                    <th>Month</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($bank_deductions))
                                    @php
                                        $count = 0;
                                        $total_loan_amount = 0;
                                    @endphp
                                    @foreach ($bank_deductions as $row=>$bank_deduction)

                                        @php
                                            $explode_loan_ids = explode(',', $bank_deduction->loan_deduction_id);
                                            $explode_bank_ids = explode(',', $bank_deduction->loan_deduction_bank_id);
                                            $explode_loan_amt = explode(',', $bank_deduction->loan_deduction_amt);

                                            $loan_amount = 0;
                                            $loan_name = '';

                                        @endphp

                                        @if($bank_deduction->loan_deduction_id)
                                            @php
                                                $emp_loans = EmployeeMonthlyLoan::whereIn('id', $explode_loan_ids)->with('loan')->get();
                                            @endphp

                                            @foreach ($emp_loans as $key => $emp_loan)
                                            @php $bank_loan_amount = 0; @endphp
                                                @if (empty($loan_id) || ($explode_bank_ids[$key] == $loan_id))
                                                    @php
                                                        $total_loan_amount+= $explode_loan_amt[$key];
                                                        $bank_loan_amount = $explode_loan_amt[$key];

                                                    @endphp
                                                    <tr>
                                                        <td>{{ ++$count }}</td>
                                                        <td>{{ $bank_deduction->Emp_Code }}</td>
                                                        <td>{{ $bank_deduction->emp_name }}</td>
                                                        <td>{{ $emp_loan->loan->loan }}</td>
                                                        <td>{{ $bank_loan_amount }}</td>
                                                        <td>{{ $monthName }}</td>
                                                        <td>{{ $bank_deduction->employee->ward->name }}</td>
                                                        <td>{{ $bank_deduction->employee->department->name }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif

                                    @endforeach

                                    <tr>
                                        <td>{{ ++$count }}</td>
                                        <td><b>Total</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>{{ $total_loan_amount }}</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>
