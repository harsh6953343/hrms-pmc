<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bank Statement</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="11" style="font-size:25px;text-align:center;padding:15px 6px;">पनवेल महानगरपालिका</th>
            </tr>
            <tr>
                <th>Sr No.</th>
                <th>Employee Id</th>
                {{-- <th>Month</th> --}}
                <th>Employee Name</th>
                <th>Bank A/C No.</th>
                {{-- <th>Bank Name</th> --}}
                {{-- <th>Branch</th> --}}
                <th>IFSC Code</th>
                <th>Net Salary</th>
                {{-- <th>Mobile</th>
                <th>Email</th> --}}
            </tr>
        </thead>
        <tbody>
            @php $count = 1; @endphp
            @foreach ($bankStatements as $bankStatement)
                @if($bankStatement?->employee?->employee_status?->is_salary_applicable == "0")
                @continue
                @endif

                @php
                    $net_salary = $bankStatement->net_salary;
                    $monthNames = '';
                @endphp

                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $bankStatement->Emp_Code }}</td>
                    {{-- <td>{{ $monthName }}</td> --}}
                    <td>{{ $bankStatement->emp_name }}</td>
                    <td>{{ $bankStatement?->employee->account_no }}</td>
                    <td>{{ $bankStatement?->employee?->ifsc }}</td>
                    {{-- <td>{{ $bankStatement?->employee->bank?->name }}</td> --}}
                    {{-- <td>{{ $bankStatement?->employee?->branch }}</td> --}}
                    <td>{{ $net_salary }}</td>
                    {{-- <td>{{ $bankStatement->phone_no }}</td>
                    <td>{{ $bankStatement?->employee?->email }}</td> --}}
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td rowspan="7" align="center">
                    <br><br>
                    <br><br>
                    अधिक्षक<br>आस्थापना विभाग <br>पनवेल महानगरपालिका
                </td>

                <td rowspan="7" align="center">

                    <br><br>
                    <br><br>
                    सहा. आयुक्त<br> आस्थापना विभाग<br> पनवेल महानगरपालिका
                </td>
                <td colspan="4"></td>
            </tr>
        </tfoot>
    </table>
</body>
</html>
