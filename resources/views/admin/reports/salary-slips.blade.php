<x-admin.layout>
    <x-slot name="title">Salary Slips</x-slot>
    <x-slot name="heading">Salary Slips</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form href="{{ route('salary-slips.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="ward_id">Select Group</label>
                                <select class="form-select" id="ward_id" name="ward_id">
                                    <option value="">Select group</option>
                                    @foreach($wards as $ward)
                                    <option value="{{ $ward->id }}" {{ (isset(request()->ward_id) && request()->ward_id == $ward->id) ?'Selected':'' }} >{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>


                            <div class="col-md-4">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <a href="{{ route('salary-slips.index') }}" class="btn btn-warning">Refresh</a>
                        <button type="button" class="btn btn-success" id="viewSalarySubmit">Download PDF</button>
                        <button type="button" class="btn btn-success" id="viewMarathiSalarySubmit">Download PDF (Marathi)</button>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>Salary Slip</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($salary_slips as $salary_slip)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $salary_slip->Emp_Code }}</td>
                                        <td>{{ $salary_slip->emp_name }}</td>
                                        <td>
                                            @can('salary-slips.view')
                                                <a href="{{ route('salary-slips.show',$salary_slip->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="View Salary" data-id="{{ $salary_slip->id }}">View Salary</a>
                                                <a href="{{ route('salary-slips-marathi',$salary_slip->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="View Salary" data-id="{{ $salary_slip->id }}">View Salary (Marathi)</a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>

    <script>
        $(document).ready(function(){
            $('#viewSalarySubmit').click(function(){
                // view-ward-salary-slips
                let form = $('#addForm').serialize();
                window.open(
                "{{ route('view-ward-salary-slips') }}?"+form,
                '_blank' // <- This is what makes it open in a new window.
                );
            })

            $('#viewMarathiSalarySubmit').click(function(){
                // view-ward-salary-slips in marathi
                let form = $('#addForm').serialize();
                window.open(
                "{{ route('view-ward-salary-slips') }}/mr?"+form,
                '_blank' 
                );
            })
        });
    </script>
