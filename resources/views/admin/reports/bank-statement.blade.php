@php
    use App\Models\SupplimentaryBill;
    use App\Models\RemainingFreezeSalary;
@endphp
<x-admin.layout>
    <x-slot name="title">Bank Statement</x-slot>
    <x-slot name="heading">Bank Statement</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form href="{{ route('bank-statement.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <select class="form-select" id="ward" name="ward">
                                    <option value="">Select Ward</option>
                                    <option value="" {{ (empty($ward))?'Selected':'' }}>All</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}" {{ ($ward_id == $ward->id)?'Selected':'' }} >{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="button" class="btn btn-success" id="excelBtn">Excel</button>
                        <button type="button" class="btn btn-secondary" id="pdfBtn">PDF</button>
                        <a href="{{ route('bank-statement.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Month</th>
                                    <th>Net Salary</th>
                                    <th>Bank A/C No.</th>
                                    <th>Bank Name</th>
                                    <th>Branch</th>
                                    <th>Employee Name</th>
                                    <th>IFSC Code</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count = 1; @endphp
                                @foreach ($bank_statements as $bank_statement)
                                    @if($bank_statement?->employee?->employee_status?->is_salary_applicable == "0")
                                    @continue
                                    @endif

                                    @php
                                        $net_salary = $bank_statement->net_salary;
                                        $monthNames = '';
                                    @endphp

                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $bank_statement->Emp_Code }}</td>
                                        <td>{{ $monthName }}
                                            <!-- . ($monthNames != '' ? ','.$monthNames : '') }} -->
                                        </td>

                                        <td>{{ $net_salary }}</td>
                                        <td>{{ $bank_statement?->employee->account_no }}</td>
                                        <td>{{ $bank_statement?->employee->bank?->name }}</td>
                                        <td>{{ $bank_statement?->employee?->branch }}</td>
                                        <td>{{ $bank_statement->emp_name }}</td>
                                        <td>{{ $bank_statement?->employee?->ifsc }}</td>
                                        <td>{{ $bank_statement->phone_no }}</td>
                                        <td>{{ $bank_statement?->employee?->email }}</td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    $('#excelBtn').click(function(){
        let url = $('#addForm').serialize();
        window.open(
            '{{ route("reports.bank-statement-excel") }}?'+url,
            '_blank' // <- This is what makes it open in a new window.
        );
    })
    $('#pdfBtn').click(function(){
        let url = $('#addForm').serialize();
        window.open(
            '{{ route("reports.bank-statement-pdf") }}?'+url,
            '_blank' // <- This is what makes it open in a new window.
        );
    })
    </script>
