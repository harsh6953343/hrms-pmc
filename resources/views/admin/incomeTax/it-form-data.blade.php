
    <div class="header">
        <h3>PANVEL MUNICIPAL CORPORATION</h3>
    </div>
    <div class="header">
        @php
            print_r($draftData);
        @endphp
        <h4>Income Tax Calculation for the Financial Year {{$draftData->financial_year ?? ''}}</h4>
    </div>

    <table>
        <tr>
            <td width="50%">
                Employee Name: {{ $itdetails->employee->fname.' '.$itdetails->employee->mname.' '.$itdetails->employee->lname ?? '' }}<br><br>
                Department: {{ $itdetails->department->name }}<br><br>
                Gender: {{ $gender[$itdetails->employee->gender] }}
            </td>
            <td width="50%">
                Employee ID: {{ $itdetails->employee->employee_id }}<br><br>
                PAN No: {{ $itdetails->employee->pan }}
            </td>
        </tr>
    </table>

    <table>
      <tr>
        <td colspan=3  class="td_extend" width="70%">INCOME FROM SALARY</td>
      </tr>
      <tr>
        <td width="70%">1. Gross Salary</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->gross_income }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">2. Add   I. Ex Gratia/Arrears/Others<br>              II. Election Allowance</td>
        <td class="amount">{{$itdetails->ex_gratia}}<br>{{ $itdetails->elec_allowance }}</td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 2</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->ex_gratia + $itdetails->elec_allowance}}</td>
      </tr>
      <tr>
        <td width="70%">3. Total Salary (1+2)</td>
        <td class="amount"></td>
        @php
          $total12 = $itdetails->gross_income + $itdetails->ex_gratia + $itdetails->elec_allowance;
        @endphp
        <td class="amount">{{  $total12 }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">4. Less   I.   Standard Deduction
                II.  Transport Allowance u/s 10(14)
                III. House rent Allowance u/s 10(13A)
                IV. Professional Tax u/s 16(11)
                 V. Interest Paid on Home Loans u/s 24(2)
                      If Purchased/Bought before 01-04-1999 (Max Rs. 30,000/-)
                      If Purchased/Bought after 01-04-1999 (Max Rs. 2,00,000/-)
        </td>
        <td class="amount">{{ $itdetails->standard_deduction }}<br>{{ $itdetails->trans_allowance }}<br>{{ $itdetails->hre }}<br>2500<br><br>{{ $itdetails->hlib24 }}<br>{{ $itdetails->hlia24 }}</td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 4</td>
        <td class="amount"></td>
        @php
          $total4 = $itdetails->standard_deduction + $itdetails->trans_allowance + $itdetails->hre + 2500 + $itdetails->hlib24 + $itdetails->hlia24;
        @endphp
        <td class="amount">{{ $total4 }}</td>
      </tr>
      <tr>
        <td width="70%">5. Total Salary Income (3-4)</td>
        <td class="amount"></td>
        @php
          $total34 = $total12 - $total4;
        @endphp
        <td class="amount">{{ $total34 }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">6. Add I. NSC Interest<br>            II. Others</td>
        <td class="amount">{{ $itdetails->investment }}<br>{{ $itdetails->other_investment }}</td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 6</td>
        <td class="amount"></td>
        @php
          $total6 = $itdetails->investment + $itdetails->other_investment;
        @endphp
        <td class="amount">{{ $total6 }}</td>
      </tr>
      <tr>
        <td width="70%">7. Gross Total Salary Income (5+6)</td>
        <td class="amount"></td>
        @php
          $total56 = $total34 + $total6;
        @endphp
        <td class="amount">{{ $total56 }}</td>
      </tr>
    </table>

    <table>
        <tr>
          <td colspan=3 class="td_extend" width="70%">8. Deduction under VI A</td>
        </tr>
        <tr>
            <td colspan=3 width="70%">(A) Section 80C, 80CCC and 80CCD</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">   (a) Section 80C</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Life Insurance Premium</td>
            <td class="amount">{{ $itdetails->lic }}</td>
            <td></td>
        </tr>
        @if($itdetails->gpf)
          <tr>
              <td class="spaced-text" width="70%">         Government Provident Fund</td>
              <td class="amount">{{ $itdetails->gpf }}</td>
              <td></td>
          </tr>
        @else
          <tr>
            <td class="spaced-text" width="70%">         DCPS Employee Contribution</td>
            <td class="amount">{{ $itdetails->dcps_emp }}</td>
            <td></td>
          </tr>
        @endif
        <tr>
            <td class="spaced-text" width="70%">         Group Insurance Scheme</td>
            <td class="amount">{{ $itdetails->gis }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Public Provident Fund</td>
            <td class="amount">{{ $itdetails->ppf }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         National Savings Certificate</td>
            <td class="amount">{{ $itdetails->nsc }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Post Linked Insurance</td>
            <td class="amount">{{ $itdetails->pli }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Housing Loan Repayment</td>
            <td class="amount">{{ $itdetails->hloan }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Education Fees (Two Children)</td>
            <td class="amount">{{ $itdetails->edu_fees }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other Tax Saving Investment</td>
            <td class="amount"></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Total</td>
            <td></td>
            <td class="amount">{{ $itdetails->sec80c }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (b) Section 80CCC-Annuity Pension Plan</td>
            <td class="amount"></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (c) Section 80CCD-Govt. Pension Scheme</td>
            <td class="amount">{{ $itdetails->sec80ccd }}</td>
            <td></td>
        </tr>
    </table>

    <table>
        <tr>
          <td colspan=3 class="td_extend" width="70%">(B) Other Sections under Chapter VIA</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (a) Section 80D - Mediclaim</td>
            <td class="amount">{{ $itdetails->sec80d }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (b) Section 80E - Education Loan</td>
            <td class="amount">{{ $itdetails->sec80e  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (c) Section 80G - Donation</td>
            <td class="amount">{{ $itdetails->sec80g  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (d) Section 80U - Handicapped Person</td>
            <td class="amount">{{ $itdetails->sec80u  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (e) Section 80CCD(2) - Employer DCPS Contribution</td>
            <td class="amount">{{ $itdetails->sec80ccd2  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (f) Section 80DDB - Medical Treatment Expenses</td>
            <td class="amount">{{ $itdetails->sec80ddb  }}</td>
            <td></td>
        </tr>
        <tr>
          <td class="spaced-text" width="70%">  (f) Section 80TTA</td>
          <td class="amount">{{ $itdetails->sec80tta  }}</td>
          <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (g) Section</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Total</td>
            @php
              $total_other_sections = $itdetails->sec80d + $itdetails->sec80e + $itdetails->sec80g + $itdetails->sec80u + $itdetails->sec80ccd2 + $itdetails->sec80ddb;
            @endphp
            <td></td>
            <td class="amount">{{ $total_other_sections }}</td>
        </tr>
    </table>

    <table>
        <tr>
            <td width="70%">9. Aggregate of deductible amounts under chapter VIA</td>
            <td></td>
            <td class="amount">{{ $itdetails->sec80c + $itdetails->sec80ccd + $total_other_sections  }}</td>
        </tr>
        <tr>
            <td width="70%">10. Total Taxable Salary (7-8)</td>
            <td></td>
            <td class="amount">{{ $itdetails->taxable_sal }}</td>
        </tr>
    </table>

    <table>
        <tr>
            <td width="70%">12. a) Net Tax Payable</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_payable  }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">      b) Rebate under Section 87a (Financial Year 2013-2014 Onwards)</td>
            <td></td>
            <td class="amount">{{ $itdetails->rebated_amount }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">      c) Tax Payable After Rebate under Section 87a</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_payable  }}</td>
        </tr>
        <tr>
            <td width="70%">13. Add @ 4% Education Cess on Net Tax Amount</td>
            <td></td>
            <td class="amount">{{ $itdetails->edu_cess  }}</td>
        </tr>
        <tr>
            <td width="70%">14. Tax Paid In Cash</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_paid_cash  }}</td>
        </tr>
        <tr>
            <td width="70%">15. Total Tax Payable (12+13)</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_after_cess  }}</td>
        </tr>
        <tr>
            <td width="70%">16. Tax already deducted / paid (if any)</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_deducted  }}</td>
        </tr>
        <tr>
            <td width="70%">17. Income Tax to be recovered in remaining months</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_pending }}</td>
        </tr>
    </table>

    <div class="border_for_div">
      <p>You are hereby called upon to submit the details of investments, if any, you want to make to reduce the Tax Liability. The details shall reach the Accounts Department on or before 26th December of the financial year. If the details are not submitted along with Xerox Copy, Income Tax TDS as mentioned above will be deducted from the salary and deposited in Govt. Treasury</p>

      <div class="footer">
          <b>Chief Accounts Officer<br>
          Panvel Municipal Corporation</b>
      </div>
    <div>

    <button class="no-print" onclick="window.print()">Print</button>  