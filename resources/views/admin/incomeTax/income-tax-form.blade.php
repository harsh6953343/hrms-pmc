<x-admin.layout>
    <x-slot name="title">INCOME TAX</x-slot>
    <x-slot name="heading">INCOME TAX</x-slot>

    <style>
        .section-title {
            background-color: #d9cbe7;
            color: black;
            font-weight: bold;
            padding: 5px 10px;
            margin-bottom: 10px;
            text-align: center;
            border-radius: 5px;
        }

        .section-content {
            background-color: #f3f1f8;
            padding: 15px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .form-control {
            /* margin: 0; */
            padding: 5px;
            height: 35px;
        }

        .section-footer {
            background-color: #292929;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .section-footer input {
            background-color: #e0e0e0;
            color: #000;
            border: 0;
            font-weight: bold;
            text-align: center;
            width: 100%;
        }

        .claim-button {
            background-color: #ccc;
            color: black;
            border: 1px solid #999;
            padding: 3px 10px;
            border-radius: 3px;
        }

        .claim-button:hover {
            background-color: #bbb;
        }

       /* ---- Deduction  -----*/
       .hidden-section {
            display: none;
            margin-top: 2px;
            background-color: #ffffff;
            padding: 20px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .toggle-btn {
            background-color: #d9cbe7;
            cursor: pointer;
            padding: 10px;
            border-radius: 5px;
            text-align: center;
            font-weight: bold;
        }

        .deduction-table th,
        .deduction-table td {
            padding: 8px;
        }

        /* Claculate Tax */
        .calculate-btn {
            width: 50%;
            background-color: #8C68CD;
            color: white;
            padding: 10px;
            font-size: 25px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin: 0px auto;
        }


        .black-section {
            background-color: #333;
            color: white;
            padding: 20px;
            border-radius: 8px;
            margin-top: 20px;
        }

        .btnDraft{
            background-color: #8C68CD;
            color: white;
            font-size: 20px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin-top: 20px;
            padding: 10px;
            text-align: center;
        }
        .btnSubmit{
            width: 10%;
            background-color: #8C68CD;
            color: white;
            font-size: 20px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin-top: 20px;
            margin-right: 41%;
            padding: 10px;
            text-align: center;
        }
        .btnPrint{
            background-color: #8C68CD;
            color: white;
            font-size: 20px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin-top: 20px;
            margin-right: 50px;
            padding: 10px;
            text-align: center;
            float:right;
        }

        /* Loader style */
        .loader {
            display: inline-block;
            border: 4px solid #f3f3f3;
            border-radius: 50%;
            border-top: 4px solid #3498db;
            width: 40px;
            height: 40px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .taxTable table {
            border-collapse: collapse;
            width: 50%;
            margin: 20px auto;
        }

        .taxTable th, .taxTable td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        .taxTable th {
            background-color: #f2f2f2;
        }

        @media print {
            body {
                margin: 0;
                font-size: 12pt;
            }
            .printable-area {
                width: 100%;
                margin: 0;
            }
            .no-print {
                display: none;
            }
        }

    </style>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-header">
                    <!--------------------------- Tables  Data -------------------------------->
                        <div class="container">
                            <h2 class="text-center my-3" style="text-transform:uppercase">Income Tax Calculator</h2>

                            <form id="saveIncomeTax" autocomplete="off">
                            @csrf
                            <!-- Section: Financial Year and Employee ID -->
                            <input type="hidden" id="ward_id" name="ward_id">
                            <input type="hidden" id="department_id" name="department_id">
                            <input type="hidden" id="submitted" name="submitted" value=0>
                            <div class="">
                                <div class="section-title"> </div><br>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <label for="financial_year" class="form-label">Financial Year:</label>
                                        <select class="form-select" id="financial_year" name="financial_year_id">
                                            @foreach($finYears as $id => $range)
                                                <option value="{{$id}}">{{$range}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="Emp_Code" class="form-label">Employee ID:</label>
                                        <select class="form-select js-example-basic-single" id="Emp_Code" name="Emp_Code">
                                            <option value="">--Select Employee--</option>
                                            @foreach ($employees as $employee)
                                                <option value="{{ $employee->employee_id }}">{{ $employee->fname." ".$employee->mname." ".$employee->lname." (".$employee->employee_id.")" }}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger invalid Emp_Code_err" data-error-for="Emp_Code"></span>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="tax_regime" class="form-label">Tax Regime:</label>
                                        <select class="form-select" id="tax_regime" name="tax_regime">
                                            <option value=1 selected>Old</option>
                                            <option value=2>New</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!-- Section: Personal Information -->
                            <div class="section-content mb-5" style="">
                                <div class="section-title">Personal Information</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td> Employee Name:</td>
                                            <td><input class="form-control" type="text" id="emp_name" name="emp_name" placeholder=""></td>
                                            <td>Gender:</td>
                                            <td>
                                                <select class="form-select" id="gender"  name="gender">
                                                    <option>--Select--</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td><input class="form-control" type="date" id="dob" name="dob" ></td>
                                            <td>Pan No:</td>
                                            <td><input class="form-control" type="text" id="pan" name="pan" placeholder=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Salary Income -->
                            <div class="section-content">
                                <div class="section-title">Salary Income</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>Previous Employer Gross:</td>
                                            <td><input class="form-control gross_income" type="number" id="previous_gross" name="prev_gross" placeholder="₹0.00"></td>
                                            <td>Current Gross Salary:</td>
                                            <td><input class="form-control gross_income" type="number" id="current_gross" name="current_gross" placeholder="₹0.00">
                                                <span class="text-danger invalid current_gross_err" data-error-for="current_gross"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Salary:</td>
                                            <td><input class="form-control gross_income" type="number" id="estimated_salary" name="estimated_salary" placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Supplementary Bill:</td>
                                            <td><input class="form-control gross_income" type="number" id="supplementary_bill" name="supplementary_bill" placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Supplementary Bill:</td>
                                            <td><input class="form-control gross_income" type="number" id="estimated_supplementary_bill" name="estimated_supp_bill" placeholder="₹0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Gross Income:</label>
                                    <input type="number" id="gross_income" name="gross_income" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Exemptions -->
                            <div class="section-content mt-4">
                                <div class="section-title">Exemptions</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>House Rent Exemption:</td>
                                            <td><input class="form-control exemptions" type="number" id="house_rent_exemption" name="hre" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Standard Deduction:</td>
                                            <td><input class="form-control exemptions" type="number" id="standard_deduction" name="standard_deduction" value=50000 placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Previous Employer Ptax:</td>
                                            <td><input class="form-control exemptions" type="number" id="previous_ptax" name="prev_ptax" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Professional Tax:</td>
                                            <td><input class="form-control exemptions" type="number" id="professional_tax" name="ptax" placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Election Allowance:</td>
                                            <td><input class="form-control exemptions" type="number" id="election_allowance" name="elec_allowance" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Ex Gratia:</td>
                                            <td><input class="form-control exemptions" type="number" id="ex_gratia" name="ex_gratia" value=0 placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Transport Allowance:</td>
                                            <td><input class="form-control exemptions" type="number" id="transport_allowance" name="trans_allowance" placeholder="₹0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Total Exemptions:</label>
                                    <input type="number" id="total_exemptions" name="total_exemptions" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Net Salary -->
                            <div class="section-content mt-4 old-regime">
                                <div class="section-title">Net Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="net_salary_exemption" placeholder="₹0.00">
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Section: Other Income -->
                            <div class="section-content mt-4 old-regime">
                                <div class="section-title toggle-btn mb-2" id="toggleOtherincomeExplanation" onclick="toggleSection('otherIncomeSection ')" style="cursor: pointer">Other Income ▼</div>

                                <div class="hidden-section" id="otherIncomeSection " style="font-size: 15px">
                                    @php
                                        $otherIncomes = [
                                            "un_furnished" => "Perk - Where accommodation is un furnished",
                                            "personal_attendant" => "Sweeper, gardener, watchman or personal attendant",
                                            "perk_furnished_value" => "Perk-Furnished-Value as if accommodation is unfurnished",
                                            "gas_electricity_water" => "Gas, electricity, water",
                                            "interest_free_or_concessional_loans" => "Interest free or concessional loans",
                                            "cost_of_furniture" => "Cost of furniture",
                                            "furniture_rentals" => "Furniture Rentals",
                                            "holiday_expenses" => "Holiday expenses",
                                            "perquisite_value_of_furniture" => "Perquisite value of furniture",
                                            "free_meals" => "Free meals",
                                            "free_education" => "Free education",
                                            "perk_furnished_total" => "Perk-Furnished-Total",
                                            "gifts_vouchers_etc" => "Gifts, vouchers, etc.",
                                            "rent_paid_by_employee" => "Rent paid by employee",
                                            "credit_card_expenses" => "Credit card expenses",
                                            "value_of_perquisites" => "Value of perquisites",
                                            "club_expenses" => "Club expenses",
                                            "conveyance" => "Conveyance",
                                            "use_of_movable_assets_by_employees" => "Use of movable assets by employees",
                                            "remuneration_paid_on_behalf_of_employee" => "Remuneration paid on behalf of employee",
                                            "transfer_of_assets_to_employees" => "Transfer of assets to employees",
                                            "taxable_ltc" => "Taxable LTC",
                                            "stock_option_non_qualified_options" => "Stock options (non-qualified options)",
                                            "other_benefits" => "Other benefits",
                                            "stock_options_referred_80_iac" => "Stock options referred u/s 80-IAC",
                                            "pf_in_excess_of_12" => "PF in excess of 12%",
                                            "contribution_by_employer" => "Contribution by employer u/s 17(2)(vii)",
                                            "excess_iterest_credited" => "Excess interest credited",
                                            "annual_accretion_taxable" => "Annual accretion taxable u/s 17(2)(viia)",
                                        ];
                                    @endphp
                                    <div class="row">
                                        @foreach($otherIncomes as $key => $otherIncome)
                                        <div class="col-lg-3 col-md-3 col-6 mb-2">
                                            {{ $otherIncome }}
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-6 mb-2">
                                            <input type="number" class="form-control otherIncomeValue other_incomes" name="{{ $key }}" id="{{ $key }}_id">
                                        </div>
                                        @endforeach
                                    </div>
                                        
                                </div>

                                <div class="section-footer">
                                    <label>Total Investment:</label>
                                    <input type="number" id="total_other_income" name="other_income" class="form-control bg-light" >
                                </div>
                            </div>


                            <!-- Section: Deduction -->
                            <div class="section-content mt-4 deduction-section old-regime">
                                <div class="section-title">Deduction</div>

                                <!-- 8OD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODExplanation" onclick="toggleSection('bodExplanationDetails')">
                                    80D(Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80d" id="sec80d" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bodExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th colspan="2">Assessee, Spouse, and dependent Children</th>
                                                <th colspan="2">Assessee's parents</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Health Insurance (Age less Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyd" type="number" id="hial60d" name="hial60d" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td><input class="form-control eightyd" type="number" id="hial60p" name="hial60p" placeholder="₹0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health Insurance (Age More Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyd" type="number" id="hiam60d" name="hiam60d" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td><input class="form-control eightyd" type="number" id="hiam60p" name="hiam60p" placeholder="₹0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health-Checkup <span class="ri-information-fill"></span>:</td>
                                                <td colspan="1"><input class="form-control eightyd" type="number" id="hcheckup" name="hcheckup" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="hitotal" name="hitotal" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OG Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOGExplanation"  onclick="toggleSection('bogExplanationDetails')">
                                    80G (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80g" id="sec80g" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bogExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyg" id="don80g" name="don80g" type="number" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Other Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyg" id="odon80g" name="odon80g" type="number" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totaldon" name="totaldon" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OC Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCExplanation"  onclick="toggleSection('bocExplanationDetails')">
                                    80C (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80c" id="sec80c" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bocExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NSC:</td>
                                                <td><input class="form-control eightyc" type="number" id="nsc" name="nsc" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>NSC Intrest:</td>
                                                <td><input class="form-control eightyc" type="number" id="nsc_interest" name="nsc_int" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>LIC:</td>
                                                <td><input class="form-control eightyc" type="number" id="lic" name="lic" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other LIC:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_lic" name="other_lic" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS Employee Contribution:</td>
                                                <td><input class="form-control eightyc" type="number" id="dcp_emp_contribution" name="dcps_emp" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Under 80ccc Mutual Fund:</td>
                                                <td><input class="form-control eightyc" type="number" id="mutual_fund" name="mutual_fund" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GIS:</td>
                                                <td><input class="form-control eightyc" type="number" id="gis" name="gis" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other GIS:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_gis" name="other_gis" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>PLI:</td>
                                                <td><input class="form-control eightyc" type="number" id="pli" name="pli" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other PLI:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_pli" name="other_pli" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Housing Loan Repayment:</td>
                                                <td><input class="form-control eightyc" type="number" id="housing_loan_repayment" name="hloan" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other Housing Loan Repayment:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_housing_loan_repayment" name="other_hloan" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS 1 Employer Contribution:</td>
                                                <td>
                                            <span id="dcps_employer_contribution"></span>        
                                                <!-- <input class="form-control" type="number" id="dcps_employer_contribution" name="dcps_empr_contrn" placeholder="₹0.00"> -->
                                            
                                            </td>
                                                <td></td>
                                                <td>Infrastructure Bond:</td>
                                                <td><input class="form-control eightyc" type="number" id="infrastructure_bond" name="infra_bond" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GPF:</td>
                                                <td><input class="form-control eightyc" type="number" id="gpf" name="gpf" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>PPF:</td>
                                                <td><input class="form-control eightyc" type="number" id="ppf" name="ppf" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Education Fees:</td>
                                                <td><input class="form-control eightyc" type="number" id="education_fees" name="edu_fees" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80c" name="total80c" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80CCD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleB0CCDExplanation" onclick="toggleSection('BOCCDExplanationDetails')">
                                    80CCD (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ccd" id="sec80ccd" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOCCDExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NPS :</td>
                                                <td><input class="form-control eightyccd" id="npsccd" name="nps" type="number" placeholder="₹0.00"></td>
                                                <td>DCPS + Under 80CCD :</td>
                                                <td><input class="form-control eightyccd" id="dcpsccd" name="dcps_u80ccd" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totalccd" name="total80ccd" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80E Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOEExplanation" onclick="toggleSection('BOEExplanationDetails')">
                                    80E (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80e" id="sec80e" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOEExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Education Loan :</td>
                                                <td><input class="form-control" id="eduloan" name="edu_loan" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80e" name="total80e" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- Section24 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleSection24Explanation" onclick="toggleSection('section24ExplanationDetails')">
                                    Section24 ▼

                                    <span style="margin-left: 360px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec24" id="sec24" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="section24ExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Home Loan Interest Before : </td>
                                                <td><input class="form-control section24" id="hlib" name="hlib24" type="number" placeholder="₹0.00"></td>
                                                <td>Home Loan Interest After:</td>
                                                <td><input class="form-control section24" id="hlia" name="hlia24" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Oth Home Loan Interest Before:</td>
                                                <td><input class="form-control section24" id="ohlib" name="ohlib24" type="number" placeholder="₹0.00"></td>
                                                <td>Oth Home Loan Interest After:</td>
                                                <td><input class="form-control section24" id="ohlia" name="ohlia24" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="sec24total" name="totalsec24" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80DDB Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODDBExplanation" onclick="toggleSection('BODDBExplanationDetails')">
                                    80DDB (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ddb" id="sec80ddb" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BODDBExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Medical Treatment  :</td>
                                                <td><input class="form-control eightyddb" type="number" id="medical_treatment" name="medical_treatment" placeholder="₹0.00"></td>
                                                <td><button type="button" id="add_medical_treatment" class="btn btn-primary btn-sm">submit</button></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totalmt" name="total80ddb" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80TTA Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOTTAExplanation" onclick="toggleSection('BOTTAExplanationDetails')">
                                    80TTA (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80tta" id="sec80tta" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOTTAExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Deduction On SB Interest :</td>
                                                <td><input class="form-control" type="number" id="sb_int_ded" name="sb_int_ded" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="ttatotal" name="total80tta" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OU Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOUExplanation"  onclick="toggleSection('BOUExplanationDetails')">
                                    80U (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80u" id="sec80u" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOUExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Disability:</td>
                                                <td>40% <input type="radio" name="disability" value="40" placeholder="₹0.00"></td>
                                                <td>60% <input type="radio" name="disability" value="60" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Handicapped Allowance <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" id="handicappedalwce" name="handicappedalwce" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80u" name="total80u" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OCCD2 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCCD2Explanation"  onclick="toggleSection('BOCCD2ExplanationDetails')">
                                    8OCCD2 (Explanation) ▼

                                    <span style="margin-left: 270px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ccd2" id="sec80ccd2" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOCCD2ExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Employer Contribution:</td>
                                                <td><input type="text" id="empr_contribution" name="empr_contribution" class="form-control" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80cc2" name="total80cc2" class="form-control bg-light" >
                                    </div>
                                </div>

                            </div>


                            <!-- Section: Total Taxable Salary -->
                            <div class="section-content mt-4">
                                <div class="section-title">Total Taxable Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="total_taxable_salary" name="taxable_sal" placeholder="₹0.00">
                                                <span class="text-danger invalid taxable_sal_err" data-error-for="taxable_sal"></span>
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Tax Toggle Button -->
                            <div class="section-content mt-4" >
                                <button class="calculate-btn" type="button" id="calculateTaxBtn" onclick="showTaxTable('TaxExplanationDetails')">Calculate Tax</button>

                                <div class="d-flex justify-content-center d-none" id="TaxExplanationDetails">
                                    <table class="taxTable">
                                        <thead>
                                            <tr>
                                                <th>Income Range</th>
                                                <th>Tax Rate (%)</th>
                                                <th>Taxable Income at Slab</th>
                                                <th>Tax Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0 - 2,50,000</td>
                                                <td>0</td>
                                                <td id="slab1Income"></td>
                                                <td id="slab1Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>2,50,001 - 5,00,000</td>
                                                <td>5</td>
                                                <td id="slab2Income"></td>
                                                <td id="slab2Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>5,00,001 - 10,00,000</td>
                                                <td>20</td>
                                                <td id="slab3Income"></td>
                                                <td id="slab3Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>10,00,001 and above</td>
                                                <td>30</td>
                                                <td id="slab4Income"></td>
                                                <td id="slab4Tax"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><b>Total Tax</b></td>
                                                <td id="totalTaxableIncome"></td>
                                                <td id="totalTax"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="d-flex justify-content-center d-none" id="TaxExplanationDetailsNewRegime">
                                    <table class="taxTable">
                                        <thead>
                                            <tr>
                                                <th>Income Range</th>
                                                <th>Tax Rate (%)</th>
                                                <th>Taxable Income at Slab</th>
                                                <th>Tax Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0 - 3,00,000</td>
                                                <td>0</td>
                                                <td id="slab1IncomeN"></td>
                                                <td id="slab1TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>3,00,001 - 7,00,000</td>
                                                <td>5</td>
                                                <td id="slab2IncomeN"></td>
                                                <td id="slab2TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>7,00,001 - 10,00,000</td>
                                                <td>10</td>
                                                <td id="slab3IncomeN"></td>
                                                <td id="slab3TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>10,00,001 - 12,00,000</td>
                                                <td>15</td>
                                                <td id="slab4IncomeN"></td>
                                                <td id="slab4TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>12,00,001 - 15,00,000</td>
                                                <td>20</td>
                                                <td id="slab5IncomeN"></td>
                                                <td id="slab5TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>15,00,001 - 50,00,000</td>
                                                <td>30</td>
                                                <td id="slab6IncomeN"></td>
                                                <td id="slab6TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><b>Total Tax</b></td>
                                                <td id="totalTaxableIncomeN"></td>
                                                <td id="totalTaxN"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <!-- Black Data Section -->
                            <div class="black-section">
                                <div class="" id="">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Tax Payable:</td>
                                                <td><input type="text" class="form-control" id="tax_payable" name="tax_payable" placeholder="₹0.00"></td>
                                                <td>Rebate Under 87A: </td>
                                                <td><input type="checkbox" checked disabled></td>
                                                <td>Rebated Amount: </td>
                                                <td><input type="text" class="form-control" id="rebated_amount" name="rebated_amount" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable Before Cess:</td>
                                                <td><input type="text" class="form-control" id="tax_before_cess" name="net_tax" placeholder="₹0.00"></td>
                                                <td>Edu Cess(4%): </td>
                                                <td><input type="text" class="form-control" id="edu_cess" name="edu_cess" placeholder="₹0.00"></td>
                                                <td>Tax Paid in Cash: </td>
                                                <td><input type="text" class="form-control" id="tax_paid_cash" name="tax_paid_cash" value=0 placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable After Cess:</td>
                                                <td><input type="text" class="form-control" id="tax_after_cess" name="tax_after_cess" placeholder="₹0.00"></td>
                                                <td>Tax Deducted Upto Now:</td>
                                                <td><input type="text" class="form-control" id="tax_deducted" name="tax_deducted" value=0 placeholder="₹0.00"></td>
                                                <td>Tax Pending:</td>
                                                <td><input type="text" class="form-control" id="tax_pending" name="tax_pending" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Total TDS:<button type="button" id="prevTdsPopUp" class="btn btn-primary btn-sm" data-id="21"  data-bs-toggle="modal" data-bs-target=".prevTdsModal">Prev. TDS</button> </td>
                                                <td><input type="text" class="form-control" id="total_tds" name="total_tds" placeholder="₹0.00"></td>

                                                <td>Tax for Remaining Months:</td>
                                                <td><input type="text" class="form-control" id="tax_rem_months" name="tax_rem_months" placeholder="₹0.00"></td>
                                                <td>Tax Refunded:</td>
                                                <td><input type="text" class="form-control" id="tax_refunded" name="tax_refunded" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>User Requested Deduction: </td>
                                                <td><input type="text" class="form-control" name="user_req_ded" placeholder="₹0.00"></td>
                                                <td>Reason For Requestd Deduction:</td>
                                                <td><input type="text" class="form-control" name="reason_requested_ded" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <div style="display: flex; justify-content: flex-end; gap: 10px;"> 
                                <button class="btnDraft" id="draftIncomeTax" type="button">Save As Draft</button>
                                <button class="btnSubmit" id="submitIncomeTax" type="submit">Submit</button>
                                <button class="btnPrint" onclick="printForm()" type="button">Print</button>
                            </div>

                        </div>
                    <!--------------------------- Tables  Data -------------------------------->
                    </form>
                </div>

            </div>
        </div>
    </div>




    <div class="modal fade prevTdsModal" tabindex="-1" role="dialog" aria-labelledby="prevTdsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header border-bottom">
                    <h5 class="modal-title" id="supplyModelRemarkStatusLabel">TDS Made Details</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>TDS Made</th>
                                <th>Income Tax</th>
                                <th>Surcharge</th>
                                <th>Cess</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Previous Employment</th>
                                <th><input type="number" class="form-control" id="previousEmployementIncomeTax" name="previousEmployementIncomeTax"></th>
                                <th><input type="number" class="form-control" id="previousEmployementSurcharge" name="previousEmployementSurcharge"></th>
                                <th><input type="number" class="form-control" id="previousEmployementCess" name="previousEmployementCess"></th>
                                <th><input type="number" class="form-control" id="previousEmployementTotal" readonly></th>
                            </tr>
                            <tr>
                                <th>Without Deduction details</th>
                                <th><input type="number" class="form-control" id="withoutDeductionIncomeTax" readonly></th>
                                <th><input type="number" class="form-control" id="withoutDeductionSurcharge" name="withoutDeductionSurcharge"></th>
                                <th><input type="number" class="form-control" id="withoutDeductionCess" name="withoutDeductionCess"></th>
                                <th><input type="number" class="form-control" id="withoutDeductionTotal" readonly></th>
                            </tr>
                            <tr>
                                <th>With Deduction details</th>
                                <th><input type="number" class="form-control" id="withDeductionIncomeTax" readonly></th>
                                <th><input type="number" class="form-control" id="withDeductionSurcharge" name="withDeductionSurcharge"></th>
                                <th><input type="number" class="form-control" id="withDeductionCess" name="withDeductionCess"></th>
                                <th><input type="number" class="form-control" id="withDeductionTotal" readonly></th>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <th><input type="number" class="form-control" id="totalIncomeTax" readonly></th>
                                <th><input type="number" class="form-control" id="totalSurcharge" readonly></th>
                                <th><input type="number" class="form-control" id="totalCess" readonly></th>
                                <th><input type="number" class="form-control" id="totalTotal" readonly></th>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5" align="left">
                                    <button type="button" class="btn btn-primary" id="saveTaxPensing">Save</button>
                                    <button type="button" class="btn btn-primary" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <script>
        function toggleSection(sectionId) {
            var selectedSection = document.getElementById(sectionId);

            var isVisible = selectedSection.style.display === 'block';

            var allSections = document.querySelectorAll('.hidden-section');
            allSections.forEach(function (section) {
                section.style.display = 'none';
            });

            if (!isVisible) {
                selectedSection.style.display = 'block';
            }
        }

        function showTaxTable(sectionId) {
            if($('#tax_regime').val() == 2){
                $("#TaxExplanationDetailsNewRegime").removeClass("d-none");
                $("#TaxExplanationDetails").addClass("d-none");
            }else{
                $("#TaxExplanationDetails").removeClass("d-none");
                $("#TaxExplanationDetailsNewRegime").addClass("d-none");
            }
        }

        </script>


</x-admin.layout>

<script>
    $('body').on('keyup', '#previousEmployementIncomeTax, #withoutDeductionIncomeTax, #withDeductionIncomeTax', function(){
        let prevIncomeTax = parseFloat($('#previousEmployementIncomeTax').val()) || 0;
        let withoutIncomeTax = parseFloat($('#withoutDeductionIncomeTax').val()) || 0;
        let withIncomeTax = parseFloat($('#withDeductionIncomeTax').val()) || 0;
        let total = prevIncomeTax + withoutIncomeTax + withIncomeTax;
        $('#totalIncomeTax').val(total);

        update_total_tds()
    });
    
    $('body').on('keyup', '#previousEmployementSurcharge, #withoutDeductionSurcharge, #withDeductionSurcharge', function(){
        let prevSurcharge = parseFloat($('#previousEmployementSurcharge').val()) || 0;
        let withoutSurcharge = parseFloat($('#withoutDeductionSurcharge').val()) || 0;
        let withSurcharge = parseFloat($('#withDeductionSurcharge').val()) || 0;
        let total = prevSurcharge + withoutSurcharge + withSurcharge;
        $('#totalSurcharge').val(total);

        update_total_tds()
    });

    $('body').on('keyup', '#previousEmployementCess, #withoutDeductionCess, #withDeductionCess', function(){
        let prevCess = parseFloat($('#previousEmployementCess').val()) || 0;
        let withoutCess = parseFloat($('#withoutDeductionCess').val()) || 0;
        let withCess = parseFloat($('#withDeductionCess').val()) || 0;
        let total = prevCess + withoutCess + withCess;
        $('#totalCess').val(total);

        update_total_tds()
    });
    $('body').on('keyup', '#previousEmployementTotal, #withoutDeductionTotal, #withDeductionTotal', function(){
        let prevTotal = parseFloat($('#previousEmployementTotal').val()) || 0;
        let withoutTotal = parseFloat($('#withoutDeductionTotal').val()) || 0;
        let withTotal = parseFloat($('#withDeductionTotal').val()) || 0;
        let total = prevTotal + withoutTotal + withTotal;
        $('#totalTotal').val(total);

        update_total_tds()
    });

    $('#saveTaxPensing').click(function(){
        let total = $('#totalTotal').val();
        $('#total_tds').val(total);

        let prev_emp = $('#previousEmployementIncomeTax').val();
        let tax_pending = $('#tax_pending').val();
        let difference = tax_pending - prev_emp

        $('#tax_pending').val(difference);

        saveTDSDetails()
    })

    function saveTDSDetails(){

        var emp_code = $('#Emp_Code').val()

        $.ajax({
            url: '{{ route('saveTDSDetails') }}',
            type: 'POST',
            data: {
                Emp_Code: emp_code,
                prev_emp_tax: $('#previousEmployementIncomeTax').val(),
                prev_emp_surcharge: $('#previousEmployementSurcharge').val(),
                wodt_surcharge: $('#withoutDeductionSurcharge').val(),
                wdt_surcharge: $('#withDeductionSurcharge').val(),
                prev_emp_cess: $('#previousEmployementCess').val(),
                wodt_cess: $('#withoutDeductionCess').val(),
                wdt_cess: $('#withDeductionCess').val(),
                '_token': "{{ csrf_token() }}"

            },
            success: function(response) {
                console.log('success')
                // alert("Data submitted successfully!");
            },
            error: function(xhr, status, error) {
                console.error('Error:', xhr, status, error);
                alert("An error occurred during submission.");
            }
        });
    }

    function update_total_tds(){

        let total_income_tax = $('#totalIncomeTax').val()
        let total_surcharge = $('#totalSurcharge').val()
        let total_cess = $('#totalCess').val()
        let total_tds = parseFloat(total_income_tax || 0) + parseFloat(total_surcharge || 0) + parseFloat(total_cess || 0)
        
        $('#totalTotal').val(total_tds)
    }

</script>

<script>
    $(document).ready(function(){
        $('.otherIncomeValue').keyup(function(){
            var otherIncomeValue = 0;
            $(".otherIncomeValue").each(function(counter) {
                let deduction = parseFloat($(this).val() || 0); 
                otherIncomeValue = otherIncomeValue + deduction;
            });
            $('#total_other_income').val(otherIncomeValue);
        });
    })
</script>


<script>

$(document).ready(function() {

    $("#openModalBtn").click(function(){
        $("#previewModal").show();
    });

    $( ".gross_income" ).on( "keyup", function() {

        if($('#Emp_Code').val() == ''){
            swal("Error!", 'Please select Employee ID', "error")
            $('#Emp_Code').focus();
            return false
        }

        let pg = parseFloat($('#previous_gross').val()) || 0
        let cg = parseFloat($('#current_gross').val()) || 0
        let es = parseFloat($('#estimated_salary').val()) || 0
        let sb = parseFloat($('#supplementary_bill').val()) || 0
        let esb = parseFloat($('#estimated_supplementary_bill').val()) || 0

        let total_gross = pg + cg + es + sb + esb

        $('#gross_income').val(total_gross)

        let net_salary = total_gross - $('#total_exemptions').val()
        $('#net_salary_exemption').val(net_salary)
        calculateTaxableSalary()
    });

    $( ".exemptions" ).on( "keyup", function() {

        if($('#Emp_Code').val() == ''){
            swal("Error!", 'Please select Employee ID', "error")
            $('#Emp_Code').focus();
            return false
        }

        let hre = parseFloat($('#house_rent_exemption').val()) || 0
        let ppt = parseFloat($('#previous_ptax').val()) || 0
        let ea  = parseFloat($('#election_allowance').val()) || 0
        let ta  = parseFloat($('#transport_allowance').val()) || 0
        let sd  = parseFloat($('#standard_deduction').val()) || 0
        let pt  = parseFloat($('#professional_tax').val()) || 0
        let eg  = parseFloat($('#ex_gratia').val()) || 0

        let total_exemptions = hre + ppt + ea + ta + sd + pt + eg

        $('#total_exemptions').val(total_exemptions)

        let net_salary = $('#gross_income').val() - total_exemptions
        $('#net_salary_exemption').val(net_salary)
        calculateTaxableSalary()
    });

    $( ".other_incomes" ).on( "keyup", function() {
        let un_furnished_id = parseFloat($('#un_furnished_id').val()) || 0
        let perk_furnished_value_id = parseFloat($('#perk_furnished_value_id').val()) || 0
        let interest_free_or_concessional_loans_id = parseFloat($('#interest_free_or_concessional_loans_id').val()) || 0
        let furniture_rentals_id = parseFloat($('#furniture_rentals_id').val()) || 0
        let perquisite_value_of_furniture_id = parseFloat($('#perquisite_value_of_furniture_id').val()) || 0
        let free_education_id = parseFloat($('#free_education_id').val()) || 0
        let gifts_vouchers_etc_id = parseFloat($('#gifts_vouchers_etc_id').val()) || 0
        let credit_card_expenses_id = parseFloat($('#credit_card_expenses_id').val()) || 0
        let club_expenses_id = parseFloat($('#club_expenses_id').val()) || 0
        let use_of_movable_assets_by_employees_id = parseFloat($('#use_of_movable_assets_by_employees_id').val()) || 0
        let transfer_of_assets_to_employees_id = parseFloat($('#transfer_of_assets_to_employees_id').val()) || 0
        let stock_option_non_qualified_options_id = parseFloat($('#stock_option_non_qualified_options_id').val()) || 0
        let stock_options_referred_80_iac_id = parseFloat($('#stock_options_referred_80_iac_id').val()) || 0
        let contribution_by_employer_id = parseFloat($('#contribution_by_employer_id').val()) || 0
        let annual_accretion_taxable_id = parseFloat($('#annual_accretion_taxable_id').val()) || 0
        let personal_attendant_id = parseFloat($('#personal_attendant_id').val()) || 0
        let gas_electricity_water_id = parseFloat($('#gas_electricity_water_id').val()) || 0
        let cost_of_furniture_id = parseFloat($('#cost_of_furniture_id').val()) || 0
        let holiday_expenses_id = parseFloat($('#holiday_expenses_id').val()) || 0
        let free_meals_id = parseFloat($('#free_meals_id').val()) || 0
        let perk_furnished_total_id = parseFloat($('#perk_furnished_total_id').val()) || 0
        let rent_paid_by_employee_id = parseFloat($('#rent_paid_by_employee_id').val()) || 0
        let value_of_perquisites_id = parseFloat($('#value_of_perquisites_id').val()) || 0
        let conveyance_id = parseFloat($('#conveyance_id').val()) || 0
        let remuneration_paid_on_behalf_of_employee_id = parseFloat($('#remuneration_paid_on_behalf_of_employee_id').val()) || 0
        let taxable_ltc_id = parseFloat($('#taxable_ltc_id').val()) || 0
        let other_benefits_id = parseFloat($('#other_benefits_id').val()) || 0
        let pf_in_excess_of_12_id = parseFloat($('#pf_in_excess_of_12_id').val()) || 0
        let excess_iterest_credited_id = parseFloat($('#excess_iterest_credited_id').val()) || 0

        let total_income = un_furnished_id + perk_furnished_value_id + interest_free_or_concessional_loans_id + furniture_rentals_id + perquisite_value_of_furniture_id + free_education_id + gifts_vouchers_etc_id + credit_card_expenses_id + club_expenses_id + use_of_movable_assets_by_employees_id + transfer_of_assets_to_employees_id + stock_option_non_qualified_options_id + stock_options_referred_80_iac_id + contribution_by_employer_id + annual_accretion_taxable_id + personal_attendant_id + gas_electricity_water_id + cost_of_furniture_id + holiday_expenses_id + free_meals_id + perk_furnished_total_id + rent_paid_by_employee_id + value_of_perquisites_id + conveyance_id + remuneration_paid_on_behalf_of_employee_id + taxable_ltc_id + other_benefits_id + pf_in_excess_of_12_id + excess_iterest_credited_id

        $('#total_other_income').val(total_income)
        calculateTaxableSalary()
    });

    $( ".eightyd" ).on( "keyup", function() {
        let hi1 = parseFloat($('#hial60d').val()) || 0
        let hi2 = parseFloat($('#hial60p').val()) || 0
        let hi3  = parseFloat($('#hiam60d').val()) || 0
        let hi4  = parseFloat($('#hiam60p').val()) || 0
        let hc  = parseFloat($('#hcheckup').val()) || 0

        let total = hi1 + hi2 + hi3 + hi4 + hc

        $('#hitotal').val(total)
        $('#sec80d').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyg" ).on( "keyup", function() {
        let d1 = parseFloat($('#don80g').val()) || 0
        let d2 = parseFloat($('#odon80g').val()) || 0

        let total = d1 + d2

        $('#totaldon').val(total)
        $('#sec80g').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyc" ).on( "keyup", function() {
        let c1 = parseFloat($('#nsc').val()) || 0
        let c2 = parseFloat($('#lic').val()) || 0
        let c3 = parseFloat($('#dcp_emp_contribution').val()) || 0
        let c4 = parseFloat($('#gis').val()) || 0
        let c5 = parseFloat($('#pli').val()) || 0
        let c6 = parseFloat($('#housing_loan_repayment').val()) || 0
        // let c7 = parseFloat($('#dcps_employer_contribution').val()) || 0
        let c8 = parseFloat($('#gpf').val()) || 0
        let c9 = parseFloat($('#nsc_interest').val()) || 0
        let c10 = parseFloat($('#other_lic').val()) || 0
        let c11 = parseFloat($('#mutual_fund').val()) || 0
        let c12 = parseFloat($('#other_gis').val()) || 0
        let c13 = parseFloat($('#other_pli').val()) || 0
        let c14 = parseFloat($('#other_housing_loan_repayment').val()) || 0
        let c15 = parseFloat($('#infrastructure_bond').val()) || 0
        let c16 = parseFloat($('#ppf').val()) || 0
        let c17 = parseFloat($('#education_fees').val()) || 0

        let total = c1 + c2 +c3 + c4 +c5 + c6 + c8 +c9 + c10 +c11 + c12 +c13 + c14 +c15 + c16 +c17

        $('#total80c').val(total)
        $('#sec80c').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyccd" ).on( "keyup", function() {
        let d1 = parseFloat($('#npsccd').val()) || 0
        let d2 = parseFloat($('#dcpsccd').val()) || 0

        let total = d1 + d2

        $('#totalccd').val(total)
        $('#sec80ccd').val(total)

        calculateTaxableSalary()
    });

    $( "#eduloan" ).on( "keyup", function() {
        let l = parseFloat($('#eduloan').val()) || 0
        $('#total80e').val(l)
        $('#sec80e').val(l)
        calculateTaxableSalary()
    });

    $( ".section24" ).on( "keyup", function() {
        let l1 = parseFloat($('#hlib').val()) || 0
        let l2 = parseFloat($('#hlia').val()) || 0
        let l3 = parseFloat($('#ohlib').val()) || 0
        let l4 = parseFloat($('#ohlia').val()) || 0
        let total = l1 + l2 + l3 + l4

        $('#sec24').val(total)
        $('#sec24total').val(total)
        calculateTaxableSalary()
    });

    // $( ".eightyddb" ).on( "keyup", function() {
        // let m = parseFloat($('#medical_treatment').val()) || 0
        // $('#totalmt').val(m)
        // $('#sec80ddb').val(m)
        // calculateTaxableSalary()
    // });

    $('#add_medical_treatment').click(function(){
        let m = parseFloat($('#medical_treatment').val()) || 0
        if(m > 100000){
            let med = m - 100000
            let ts = $('#total_taxable_salary').val() || 0
            let taxable_salary = parseFloat(med) + parseFloat(ts)
            $('#total_taxable_salary').val(taxable_salary)
        }
    })

    $( "#sb_int_ded" ).on( "keyup", function() {
        let sbint = parseFloat($('#sb_int_ded').val()) || 0
        $('#ttatotal').val(sbint)
        $('#sec80tta').val(sbint)
        calculateTaxableSalary()
    });

    $( "#handicappedalwce" ).on( "keyup", function() {
        let hallw = parseFloat($('#handicappedalwce').val()) || 0
        $('#total80u').val(hallw)
        $('#sec80u').val(hallw)
        calculateTaxableSalary()
    });

    $( "#empr_contribution" ).on( "keyup", function() {
        let ec = parseFloat($('#empr_contribution').val()) || 0
        $('#total80cc2').val(ec)
        $('#sec80ccd2').val(ec)
        calculateTaxableSalary()
    });

    function calculateTaxableSalary(){
        let net_salary = parseFloat($('#net_salary_exemption').val()) || 0
        let other_income = parseFloat($('#total_other_income').val()) || 0

        let deductions = [];
        $(".deductions").each(function() {
            deduction = parseFloat($(this).val()) || 0;
            deductions.push(deduction);
        });
		let totalDeductions = deductions.reduce((sum, deduction) => sum + deduction, 0);

        $('#total_taxable_salary').val(net_salary + other_income - totalDeductions)

        $('#add_medical_treatment').triggerHandler('click');
    }

    // Function to process inputs and calculate taxable income
    $("#calculateTaxBtn").click(function() {
        let grossSalary = parseFloat($("#gross_income").val());
        // let grossSalary = 663430;
        // let grossSalary = 1050000;
        
        //------For new regime-------//
        if($('#tax_regime').val() == 2){
            let exgratia = parseFloat($('#ex_gratia').val())
            let taxableincome = $('#total_taxable_salary').val()
            // $('#total_taxable_salary').val(taxableincome)
            let tax_payable = calculateTaxNewRegime(taxableincome)
            // $('#tax_payable').val(tax_payable)

            showSlabwiseCalculationNewRegime(taxableincome)

            return true
        }
        //---------------------------//

        // let deductions = [];
        
        // let deduction = 0;
        // $(".exemptions").each(function() {
        //     deduction = parseFloat($(this).val()) || 0;
        //     deductions.push(deduction);
        // });

        // let totalExemption = deductions.reduce((sum, deduction) => sum + deduction, 0);
        // $('#net_salary_exemption').val(grossSalary - totalExemption)

        // let other_incomes = [];
        // let otherincome = 0
        // $(".other_incomes").each(function() {
        //     otherincome = parseFloat($(this).val()) || 0;
        //     other_incomes.push(otherincome);
        // });

        // $(".deductions").each(function() {
        //     deduction = parseFloat($(this).val()) || 0;
        //     deductions.push(deduction);
        // });

        // let taxableIncome = calculateTaxableIncome(grossSalary, deductions, other_incomes);
        // $('#total_taxable_salary').val(taxableIncome)
        let taxableIncome = $('#total_taxable_salary').val()
        
        let tax_payable = calculateTax(taxableIncome)
        // $('#tax_payable').val(tax_payable)

        showSlabwiseCalculation(taxableIncome)
    });

    function calculateTaxableIncome(grossSalary, deductions, other_incomes) {
        let totalDeductions = deductions.reduce((sum, deduction) => sum + deduction, 0);
        let totalOtherIncomes = other_incomes.reduce((sum, oi) => sum + oi, 0);

        let taxable_income = parseFloat(grossSalary + totalOtherIncomes - totalDeductions) || 0
        return taxable_income;
    }

    // Old Tax Regime
    function calculateTax(taxable_income) {
        let tax = 0;
    
        if (taxable_income <= 250000) {
            tax = 0;
        } else if (taxable_income <= 500000) {
            tax = (taxable_income - 250000) * 0.05;
        } else if (taxable_income <= 1000000) {
            tax = 12500 + (taxable_income - 500000) * 0.2;
        } else {
            tax = 112500 + (taxable_income - 1000000) * 0.3;
        }

        let net_tax = tax.toFixed(2)
        let edu_cess = Math.round(net_tax * 0.04)

        //Apply rebate Section 87a
        var rebated_amount = 0
        if(taxable_income <= 500000){
            if(net_tax < 12500){
                rebated_amount = net_tax
            }else{
                rebated_amount = 12500
            }
            net_tax = net_tax - rebated_amount
        }
        $('#rebated_amount').val(rebated_amount);
        if(net_tax == 0){
            edu_cess = 0
        }   
        let total_tax = Math.round(parseFloat(net_tax) + parseFloat(edu_cess))

        $('#tax_payable').val(net_tax)
        $('#tax_before_cess').val(Math.round(net_tax));
        $('#tax_after_cess').val(total_tax);
        $('#edu_cess').val(edu_cess);

        let tax_pending = total_tax - parseFloat($('#tax_deducted').val()) || 0;
        $('#tax_pending').val(tax_pending)
        
        return total_tax;
    }

    // New Tax Regime
    function calculateTaxNewRegime(taxable_income) {
        var tax = 0; 
        let initialtaxableincome = taxable_income
        
        if (taxable_income > 1500000) { 
            tax += (taxable_income - 1500000) * 0.30; 
            taxable_income = 1500000; 
        } 
        if (taxable_income > 1200000) { 
            tax += (taxable_income - 1200000) * 0.20; 
            taxable_income = 1200000; 
        } 
        if (taxable_income > 1000000) { 
            tax += (taxable_income - 1000000) * 0.15;
            taxable_income = 1000000; 
        } 
        if (taxable_income > 700000) { 
            tax += (taxable_income - 700000) * 0.10; 
            taxable_income = 700000; 
        }
        if (taxable_income > 300000) { 
            tax += (taxable_income - 300000) * 0.05;
        }
        let net_tax = tax.toFixed(2)
        let edu_cess = Math.round(net_tax * 0.04)

        //Apply rebate Section 87a
        var rebated_amount = 0
        if(initialtaxableincome <= 700000){
            if(net_tax < 25000){
                rebated_amount = net_tax
            }else{
                rebated_amount = 25000
            }
            net_tax = net_tax - rebated_amount
        }
        $('#rebated_amount').val(rebated_amount);
        if(net_tax == 0){
            edu_cess = 0
        }   
        let total_tax = Math.round(parseFloat(net_tax) + parseFloat(edu_cess))

        $('#tax_payable').val(net_tax)
        $('#tax_before_cess').val(Math.round(net_tax));
        $('#tax_after_cess').val(total_tax);
        $('#edu_cess').val(edu_cess);
        
        return total_tax;
    }

    function showSlabwiseCalculation(taxableIncome){
        let tax = 0;

        // Calculate tax for each slab
        let slab1Income = Math.min(taxableIncome, 250000);
        let slab1Tax = 0;

        let slab2Income = Math.max(0, Math.min(taxableIncome - 250000, 250000));
        let slab2Tax = slab2Income * 0.05;

        let slab3Income = Math.max(0, Math.min(taxableIncome - 500000, 500000));
        let slab3Tax = slab3Income * 0.2;

        let slab4Income = Math.max(0, taxableIncome - 1000000);
        let slab4Tax = slab4Income * 0.3;

        let totalTaxableIncome = slab1Income + slab2Income + slab3Income + slab4Income;
        tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax;

        // Display results in the table
        document.getElementById("slab1Income").textContent = slab1Income;
        document.getElementById("slab1Tax").textContent = slab1Tax.toFixed(2);
        document.getElementById("slab2Income").textContent = slab2Income;
        document.getElementById("slab2Tax").textContent = slab2Tax.toFixed(2);
        document.getElementById("slab3Income").textContent = slab3Income;
        document.getElementById("slab3Tax").textContent = slab3Tax.toFixed(2);
        document.getElementById("slab4Income").textContent = slab4Income;
        document.getElementById("slab4Tax").textContent = slab4Tax.toFixed(2);
        document.getElementById("totalTaxableIncome").textContent = totalTaxableIncome.toFixed(2);
        document.getElementById("totalTax").textContent = tax.toFixed(2);
    }

    function showSlabwiseCalculationNewRegime(taxableIncome){
        let tax = 0;

        // Calculate tax for each slab
        let slab1Income = Math.min(taxableIncome, 300000);
        let slab1Tax = 0;

        let slab2Income = Math.max(0, Math.min(taxableIncome - 300000, 400000));
        let slab2Tax = slab2Income * 0.05;
        
        let slab3Income = Math.max(0, Math.min(taxableIncome - 700000, 300000));
        if(slab3Income != 0){
            var slab3Tax = slab3Income * 0.1;
        }else{
            var slab3Tax = 0
        }

        let slab4Income = Math.max(0, Math.min(taxableIncome - 1000000, 200000));
        if(slab4Income != 0){
            var slab4Tax = slab4Income * 0.15;
        }else{
            var slab4Tax = 0
        }

        let slab5Income = Math.max(0, Math.min(taxableIncome - 1200000, 300000));
        if(slab5Income != 0){
            var slab5Tax = slab5Income * 0.2;
        }else{
            var slab5Tax = 0
        }

        let slab6Income = Math.max(0, Math.min(taxableIncome - 1500000, 3500000));
        if(slab6Income != 0){
            var slab6Tax = slab6Income * 0.3;
        }else{
            var slab6Tax = 0
        }

        // let slab7Income = Math.max(0, taxableIncome - 1500000);
        // let slab7Tax = slab7Income * 0.3;

        // tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax + slab5Tax + slab6Tax + slab7Tax;
        let totalTaxableIncome = slab1Income + slab2Income + slab3Income + slab4Income + slab5Income + slab6Income
        tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax + slab5Tax + slab6Tax;

        // Display results in the table
        document.getElementById("slab1IncomeN").textContent = slab1Income;
        document.getElementById("slab1TaxN").textContent = slab1Tax.toFixed(2);
        document.getElementById("slab2IncomeN").textContent = slab2Income;
        document.getElementById("slab2TaxN").textContent = slab2Tax.toFixed(2);
        document.getElementById("slab3IncomeN").textContent = slab3Income;
        document.getElementById("slab3TaxN").innerHTML = slab3Tax.toFixed(2)
        document.getElementById("slab4IncomeN").textContent = slab4Income;
        document.getElementById("slab4TaxN").innerHTML = slab4Tax.toFixed(2)
        document.getElementById("slab5IncomeN").textContent = slab5Income;
        document.getElementById("slab5TaxN").innerHTML = slab5Tax.toFixed(2)
        document.getElementById("slab6IncomeN").textContent = slab6Income;
        document.getElementById("slab6TaxN").innerHTML = slab6Tax.toFixed(2)
        // document.getElementById("slab7IncomeN").textContent = slab7Income;
        // document.getElementById("slab7TaxN").textContent = slab7Tax.toFixed(2);
        document.getElementById("totalTaxableIncomeN").textContent = totalTaxableIncome.toFixed(2);
        document.getElementById("totalTaxN").textContent = tax.toFixed(2);
    }

    $("#draftIncomeTax").click(function() {
        saveFormData(0)
    });

    $("#saveIncomeTax").submit(function(e) {
        // $("form").removeClass("was-validated");
        // $("input").removeClass("is-valid");
        e.preventDefault();
        $('#submitted').val(1)

        // $("#submitIncomeTax").prop('disabled', true);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((isConfirm) => {
            if (isConfirm) {
                saveFormData(1)
            }
        });
    });

    function saveFormData(submit){

        var loaderHtml = '<div class="loader btnSubmit"></div>'; 
        $("#submitIncomeTax").replaceWith(loaderHtml);// Replace the submit button with the loader 
        var submitBtnHtml = '<button class="btnSubmit" id="submitIncomeTax" type="submit">Submit</button>';

        const form = document.getElementById('saveIncomeTax');
        var formdata = new FormData(form);

        $.ajax({
            url: '{{ route('saveIncomeTaxDetails') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#submitIncomeTax").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        if(submit){
                            window.location.href = '{{ route('tax-calculation') }}';
                        }else{
                            $(".loader").replaceWith(submitBtnHtml);
                        }
                    });
                else
                    swal("Error!", data.error2, "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                    swal("Error occured!", "Please fill the required fields", "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
                },
                500: function(responseObject, textStatus, errorThrown) {
                    swal("Error occured!", "Something went wrong please try again", "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
                }
            }
        });
    }

    $("#tax_regime").change(function(){

        if(localStorage.getItem('profess_tax') !== null){
            var pt = localStorage.getItem('profess_tax');
        }else{
            var pt = $('#professional_tax').val();
            localStorage.setItem('profess_tax', pt);
        }

        if($("#tax_regime").val() == 1){
            $("#house_rent_exemption").prop('disabled', false);
            $("#previous_ptax").prop('disabled', false);
            $("#election_allowance").prop('disabled', false);
            $("#transport_allowance").prop('disabled', false);
            $('#professional_tax').val(pt);
            $("#professional_tax").prop('disabled', false);
            $(".old-regime").show(); 
            $('#standard_deduction').val(50000);

            var te = +pt + 50000

            $('#total_exemptions').val(te);
        }else{
            $("#house_rent_exemption").prop('disabled', true);
            $("#previous_ptax").prop('disabled', true);
            $("#election_allowance").prop('disabled', true);
            $("#transport_allowance").prop('disabled', true);
            $('#professional_tax').val(0);
            $("#professional_tax").prop('disabled', true);
            $(".old-regime").hide(); 
            $('#standard_deduction').val(75000);
            $('#total_exemptions').val(75000);
        }
    });

    $( "#tax_deducted" ).on( "keyup", function() {

        let tax_pending = parseFloat($('#tax_after_cess').val()) - parseFloat($('#tax_deducted').val());
        $('#tax_pending').val(tax_pending)
    });

    $('.prevTdsModal').on('show.bs.modal', function () {

        let prev_emp = $('#previousEmployementIncomeTax').val()
        let tax_deducted = $('#tax_deducted').val()
        let tax_pending = $('#tax_pending').val()
        let total_tds = parseInt(prev_emp) + parseInt(tax_deducted) + parseInt(tax_pending)

        $('#withoutDeductionIncomeTax').val(tax_pending)
        $('#withDeductionIncomeTax').val(tax_deducted)
        $('#totalIncomeTax').val(total_tds)
    });
});

$("#Emp_Code").change(function(){
            var Emp_Code = $('#Emp_Code').val();

            var url = "{{ route('fetch-emp-details', [':Emp_Code', ':fi_year']) }}";
            url = url.replace(':Emp_Code', Emp_Code);

            var fyear = $('#financial_year').val()
            if(fyear != ''){
                url = url.replace(':fi_year', $('#financial_year').val());
            }else{
                swal("Error!", "Please select financial year", "error");
                return false
            }
            
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data) {
                    if (!data.error && !data.error2) {

                        if(data.data_already_saved == 1){

                            fillIncomeTaxForm(data, 1)
                            if(data.data_already_submitted == 1){
                                // $("#draftIncomeTax").hide();
                                $("#submitIncomeTax").hide();
                            }
                        }
                        else if (data.result === 1) {

                            fillIncomeTaxForm(data, 0)

                            $("#draftIncomeTax").show();
                            // $("#submitIncomeTax").show();

                        } else {
                            alert("Unexpected result from the server");
                        }
                    }else if(data.error){
                        swal("", data.message, "info");
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        });

    function fillIncomeTaxForm(data, data_already_saved){

        clearAllFields()

        var full_name = '';
        if (data.employee_details.fname) {
            full_name += data.employee_details.fname;
        }
        if (data.employee_details.mname) {
            full_name += (full_name.length ? " " : "") + data.employee_details.mname;
        }
        if (data.employee_details.lname) {
            full_name += (full_name.length ? " " : "") + data.employee_details.lname;
        }
        $('#emp_name').val(full_name);
        $('#gender').val(data.employee_details.gender);
        $('#dob').val(data.employee_details.dob);
        $('#pan').val(data.employee_details.pan);
        $('#ward_id').val(data.employee_details.ward_id);
        $('#department_id').val(data.employee_details.department_id);

        $('#current_gross').val(data.current_gross_salary);
        $('#estimated_salary').val(data.estimated_salary);
        $('#supplementary_bill').val(data.supplementary_bill);
        $('#gross_income').val(data.gross_income);

        if(data_already_saved){
            $('#previous_gross').val(data.itdetails.prev_gross)
            $('#estimated_supplementary_bill').val(data.itdetails.estimated_supp_bill)
            $('#house_rent_exemption').val(data.itdetails.hre)
            $('#previous_ptax').val(data.itdetails.prev_ptax)
            $('#election_allowance').val(data.itdetails.elec_allowance)
            $('#transport_allowance').val(data.itdetails.trans_allowance)
            $('#standard_deduction').val(data.itdetails.standard_deduction)
            $('#professional_tax').val(data.itdetails.ptax)
            $('#ex_gratia').val(data.itdetails.ex_gratia)
            $('#total_exemptions').val(data.itdetails.total_exemptions)

            let net_salary = data.itdetails.gross_income - $('#total_exemptions').val()
            $('#net_salary_exemption').val(net_salary)

            $('#investment').val(data.itdetails.investment);
            $('#other_investment').val(data.itdetails.other_investment);
            $('#total_other_income').val(data.itdetails?.other_income);

            // other income value
            $('#un_furnished_id').val(data?.itdetails?.other_income_value?.un_furnished);
            $('#personal_attendant_id').val(data?.itdetails?.other_income_value?.personal_attendant);
            $('#perk_furnished_value_id').val(data?.itdetails?.other_income_value?.perk_furnished_value);
            $('#gas_electricity_water_id').val(data?.itdetails?.other_income_value?.gas_electricity_water);
            $('#interest_free_or_concessional_loans_id').val(data?.itdetails?.other_income_value?.interest_free_or_concessional_loans);
            $('#cost_of_furniture_id').val(data?.itdetails?.other_income_value?.cost_of_furniture);
            $('#furniture_rentals_id').val(data?.itdetails?.other_income_value?.furniture_rentals);
            $('#holiday_expenses_id').val(data?.itdetails?.other_income_value?.holiday_expenses);
            $('#perquisite_value_of_furniture_id').val(data?.itdetails?.other_income_value?.perquisite_value_of_furniture);
            $('#free_meals_id').val(data?.itdetails?.other_income_value?.free_meals);
            $('#free_education_id').val(data?.itdetails?.other_income_value?.free_education);
            $('#perk_furnished_total_id').val(data?.itdetails?.other_income_value?.perk_furnished_total);
            $('#gifts_vouchers_etc_id').val(data?.itdetails?.other_income_value?.gifts_vouchers_etc);
            $('#rent_paid_by_employee_id').val(data?.itdetails?.other_income_value?.rent_paid_by_employee);
            $('#credit_card_expenses_id').val(data?.itdetails?.other_income_value?.credit_card_expenses);
            $('#value_of_perquisites_id').val(data?.itdetails?.other_income_value?.value_of_perquisites);
            $('#club_expenses_id').val(data?.itdetails?.other_income_value?.club_expenses);
            $('#conveyance_id').val(data?.itdetails?.other_income_value?.conveyance);
            $('#use_of_movable_assets_by_employees_id').val(data?.itdetails?.other_income_value?.use_of_movable_assets_by_employees);
            $('#remuneration_paid_on_behalf_of_employee_id').val(data?.itdetails?.other_income_value?.remuneration_paid_on_behalf_of_employee);
            $('#transfer_of_assets_to_employees_id').val(data?.itdetails?.other_income_value?.transfer_of_assets_to_employees);
            $('#taxable_ltc_id').val(data?.itdetails?.other_income_value?.taxable_ltc);
            $('#stock_option_non_qualified_options_id').val(data?.itdetails?.other_income_value?.stock_option_non_qualified_options);
            $('#other_benefits_id').val(data?.itdetails?.other_income_value?.other_benefits);
            $('#stock_options_referred_80_iac_id').val(data?.itdetails?.other_income_value?.stock_options_referred_80_iac);
            $('#pf_in_excess_of_12_id').val(data?.itdetails?.other_income_value?.pf_in_excess_of_12);
            $('#contribution_by_employer_id').val(data?.itdetails?.other_income_value?.contribution_by_employer);
            $('#excess_iterest_credited_id').val(data?.itdetails?.other_income_value?.excess_iterest_credited);
            $('#annual_accretion_taxable_id').val(data?.itdetails?.other_income_value?.annual_accretion_taxable);
            //end of other income value

            $('#hitotal').val(data.itdetails.hitotal)
            $('#hial60d').val(data.itdetails.hial60d);
            $('#hiam60d').val(data.itdetails.hiam60d);
            $('#hcheckup').val(data.itdetails.hcheckup);
            $('#hial60p').val(data.itdetails.hial60p);
            $('#hiam60p').val(data.itdetails.hiam60p);
            $('#sec80d').val(data.itdetails.sec80d)

            $('#totaldon').val(data.itdetails.totaldon)
            $('#don80g').val(data.itdetails.don80g);
            $('#odon80g').val(data.itdetails.odon80g);
            $('#sec80g').val(data.itdetails.sec80g)

            $('#total80c').val(data.itdetails.total80c)

            $('#nsc').val(data.itdetails.nsc);
            $('#gis').val(data.itdetails.gis);
            $('#pli').val(data.itdetails.pli);
            $('#housing_loan_repayment').val(data.itdetails.hloan);
            
            $('#nsc_interest').val(data.itdetails.nsc_int);
            $('#other_lic').val(data.itdetails.other_lic);
            $('#mutual_fund').val(data.itdetails.mutual_fund);
            $('#other_gis').val(data.itdetails.other_gis);
            $('#other_pli').val(data.itdetails.other_pli);
            $('#other_housing_loan_repayment').val(data.itdetails.other_hloan);
            $('#infrastructure_bond').val(data.itdetails.infra_bond);
            $('#ppf').val(data.itdetails.ppf);
            $('#education_fees').val(data.itdetails.edu_fees);
            $('#sec80c').val(data.itdetails.sec80c)

            $('#totalccd').val(data.itdetails.total80ccd)
            $('#sec80ccd').val(data.itdetails.sec80ccd)

            $('#total80e').val(data.itdetails.total80e)
            $('#eduloan').val(data.itdetails.edu_loan);
            $('#sec80e').val(data.itdetails.sec80e)

            $('#sec24total').val(data.itdetails.totalsec24)
            $('#hlib').val(data.itdetails.hlib24);
            $('#ohlib').val(data.itdetails.ohlib24);
            $('#hlia').val(data.itdetails.hlia24);
            $('#ohlia').val(data.itdetails.ohlia24);
            $('#sec24').val(data.itdetails.sec24)

            $('#medical_treatment').val(data.itdetails.medical_treatment)
            $('#totalmt').val(data.itdetails.total80ddb)
            $('#sec80ddb').val(data.itdetails.sec80ddb)

            $('#ttatotal').val(data.itdetails.total80tta)
            $('#sec80tta').val(data.itdetails.sec80tta)

            $('#total80u').val(data.itdetails.total80u)
            $('#handicappedalwce').val(data.itdetails.handicappedalwce)
            $('#sec80u').val(data.itdetails.sec80u)
            let dis_percentage = data.itdetails.disability
            $('input[name="disability"][value="'+dis_percentage+'"]').prop('checked', true);

            $('#total80cc2').val(data.itdetails.total80cc2)
            $('#sec80ccd2').val(data.itdetails.sec80ccd2)

            $('#total_taxable_salary').val(data.itdetails.taxable_sal)

            $('#tax_payable').val(data.itdetails.tax_payable)
            $('#rebated_amount').val(data.itdetails.rebated_amount)
            $('#edu_cess').val(data.itdetails.edu_cess)
            $('#tax_before_cess').val(data.itdetails.net_tax)
            $('#tax_after_cess').val(data.itdetails.tax_after_cess)
            $('#tax_paid_cash').val(data.itdetails.tax_paid_cash)
            $('#tax_pending').val(data.itdetails.tax_pending)
            $('#tax_rem_months').val(data.itdetails.tax_rem_months)
            $('#tax_refunded').val(data.itdetails.tax_refunded)
            $('#total_tds').val(data.itdetails.total_tds)
        }

        $('#lic').val(data.total_lic_ded);
        $('#dcp_emp_contribution').val(data.dcps_employee);
        $('#dcps_employer_contribution').text(data.dcps_employer);
        $('#empr_contribution').val(data.dcps_employer);
        $('#gpf').val(data.gpf);
        $('#tax_deducted').val(data.income_tax);

        if(!data_already_saved){
            $('#professional_tax').val(data.professional_tax)

            let pt = data.professional_tax
            let te = +pt + 50000
            $('#total_exemptions').val(te)

            let net_salary = data.gross_income - $('#total_exemptions').val()
            $('#net_salary_exemption').val(net_salary)
            $('#total_taxable_salary').val(net_salary)

            $('.eightyc').triggerHandler('keyup');
            $('#empr_contribution').triggerHandler('keyup');
        }
    }

    function printForm() {

        const e = document.getElementById("financial_year");
        let finyear = e.options[e.selectedIndex].text;
        const f = document.getElementById("Emp_Code");
        let emp_code = f.value
        let emp_name = f.options[f.selectedIndex].text;
        const g = document.getElementById("gender");
        let gender = g.options[g.selectedIndex].text;

        const formContent = `
            <h2>Form Data</h2>

            <p><strong>Financial Year:</strong>${finyear}</p>
            <p><strong>Employee ID:</strong> ${emp_code}</p>
            <h3>Personal Information</h3>
            <p><strong>Employee Name:</strong> ${emp_name}</p>
            <p><strong>Gender:</strong> ${gender}</p>
            <p><strong>Date of birth:</strong> ${$('#dob').val()}</p>
            <p><strong>Pan No:</strong> ${$('#pan').val()}</p>

            <h3>Salary Income</h3>
            <p>Previous Employer Gross: ${$('#previous_gross').val()}</p>
            <p>Current Gross Salary ${$('#current_gross').val()}</p>
            <p>Estimated Salary: ${$('#estimated_salary').val()}</p>
            <p>Supplementary Bill: ${$('#supplementary_bill').val()}</p>
            <p>Estimated Supplementary Bill: ${$('#estimated_supplementary_bill').val()}</p>
            <p><strong>Gross Income:</strong> ${$('#gross_income').val()}</p>

            <h3>Exemptions</h3>
            <p>House Rent Exemption: ${$('#house_rent_exemption').val()}</p>
            <p>Previous Employer Ptax: ${$('#previous_ptax').val()}</p>
            <p>Election Allowance:	 ${$('#election_allowance').val()}</p>
            <p>Transport Allowance:	 ${$('#transport_allowance').val()}</p>
            <p>Standard Deduction:	 ${$('#standard_deduction').val()}</p>
            <p>Professional Tax:	 ${$('#professional_tax').val()}</p>
            <p>Ex Gratia:	 ${$('#ex_gratia').val()}</p>
            <p><b>Total Exemptions: </b>${$('#total_exemptions').val()}</p>
            <p><b>Net Salary: </b>${$('#net_salary_exemption').val()}</p>

            <h3>Other Income</h3>
            <p>Perk - Where accommodation is un furnished :	 ${$('#un_furnished_id').val()}</p>
            <p>Perk-Furnished-Value as if accommodation is unfurnished:	 ${$('#perk_furnished_value_id').val()}</p>
            <p>Interest free or concessional loans:	 ${$('#interest_free_or_concessional_loans_id').val()}</p>
            <p>Furniture Rentals:	 ${$('#furniture_rentals_id').val()}</p>
            <p>Perquisite value of furniture:	 ${$('#perquisite_value_of_furniture_id').val()}</p>
            <p>Free education:	 ${$('#free_education_id').val()}</p>
            <p>Gifts, vouchers, etc.:	 ${$('#gifts_vouchers_etc_id').val()}</p>
            <p>Credit card expenses:	 ${$('#credit_card_expenses_id').val()}</p>
            <p>Club expenses:	 ${$('#club_expenses_id').val()}</p>
            <p>Use of movable assets by employees:	 ${$('#use_of_movable_assets_by_employees_id').val()}</p>
            <p>Transfer of assets to employees:	 ${$('#transfer_of_assets_to_employees_id').val()}</p>
            <p>Stock options (non-qualified options):	 ${$('#stock_option_non_qualified_options_id').val()}</p>
            <p>Stock options referred u/s 80-IAC:	 ${$('#stock_options_referred_80_iac_id').val()}</p>
            <p>Contribution by employer u/s 17(2)(vii):	 ${$('#contribution_by_employer_id').val()}</p>
            <p>Annual accretion taxable u/s 17(2)(viia):	 ${$('#annual_accretion_taxable_id').val()}</p>
            <p>Sweeper, gardener, watchman or personal attendant:	 ${$('#personal_attendant_id').val()}</p>
            <p>Gas, electricity, water:	 ${$('#gas_electricity_water_id').val()}</p>
            <p>Cost of furniture:	 ${$('#cost_of_furniture_id').val()}</p>
            <p>Holiday expenses:	 ${$('#holiday_expenses_id').val()}</p>
            <p>Free meals:	 ${$('#free_meals_id').val()}</p>
            <p>Perk-Furnished-Total:	 ${$('#perk_furnished_total_id').val()}</p>
            <p>Rent paid by employee:	 ${$('#rent_paid_by_employee_id').val()}</p>
            <p>Value of perquisites:	 ${$('#value_of_perquisites_id').val()}</p>
            <p>Conveyance:	 ${$('#conveyance_id').val()}</p>
            <p>Remuneration paid on behalf of employee:	 ${$('#remuneration_paid_on_behalf_of_employee_id').val()}</p>
            <p>Taxable LTC:	 ${$('#taxable_ltc_id').val()}</p>
            <p>Other benefits:	 ${$('#other_benefits_id').val()}</p>
            <p>PF in excess of 12%:	 ${$('#pf_in_excess_of_12_id').val()}</p>
            <p>Excess interest credited:	 ${$('#excess_iterest_credited_id').val()}</p>
            <p>Total Investment:	 ${$('#total_other_income').val()}</p>

            <h3>Deduction</h3>
            <p>Total 80D :	 ${$('#hitotal').val()}</p>
            <p>Deducted :	 ${$('#sec80d').val()}</p>
            <br>
            <p>Total 80G :	 ${$('#totaldon').val()}</p>
            <p>Deducted :	 ${$('#sec80g').val()}</p>
            <br>
            <p> LIC:	 ${$('#lic').val()}</p>
            <p> DCPS Employee Contribution:	 ${$('#dcp_emp_contribution').val()}</p>
            <p> GPF:	 ${$('#gpf').val()}</p>
            <p>Total 80C :	 ${$('#total80c').val()}</p>
            <p>Deducted :	 ${$('#sec80c').val()}</p>
            <br>
            <p>Total 80CCD :	 ${$('#totalccd').val()}</p>
            <p>Deducted :	 ${$('#sec80ccd').val()}</p>
            <br>
            <p>Total 80E :	 ${$('#total80e').val()}</p>
            <p>Deducted :	 ${$('#sec80e').val()}</p>
            <br>
            <p>Section 24 :	 ${$('#sec24total').val()}</p>
            <p>Deducted :	 ${$('#sec24').val()}</p>
            <br>
            <p>Medical Treatment :	 ${$('#medical_treatment').val()}</p>
            <p>Total 80DDB :	 ${$('#totalmt').val()}</p>
            <p>Deducted :	 ${$('#sec80ddb').val()}</p>
            <br>
            <p>Total 80TTA :	 ${$('#ttatotal').val()}</p>
            <p>Deducted :	 ${$('#sec80tta').val()}</p>
            <br>
            <p>Total 80U :	 ${$('#total80u').val()}</p>
            <p>Deducted :	 ${$('#sec80u').val()}</p>
            <br>
            <p>Employer Contribution :	 ${$('#empr_contribution').val()}</p>
            <p>Total 80CCD2 :	 ${$('#total80cc2').val()}</p>
            <p>Deducted :	 ${$('#sec80ccd2').val()}</p>
            <br><br>
            <p><b>Total Taxable Salary</b> : ${$('#total_taxable_salary').val()}</p>
            <br><br>
            <p><b>Tax Payable</b> :	 ${$('#tax_payable').val()}</p>
            <p><b>Rebate</b> :	 ${$('#rebated_amount').val()}</p>
            <p><b>Education cess</b> :	 ${$('#edu_cess').val()}</p>
            <p><b>Tax Payable After Cess</b> :	 ${$('#tax_after_cess').val()}</p>
            <p><b>Tax Paid in Cash</b> :	 ${$('#tax_paid_cash').val()}</p>
            <p><b>Tax Deducted</b> :	 ${$('#tax_deducted').val()}</p>
            <p><b>Tax Pending</b> :	 ${$('#tax_pending').val()}</p>
            <p><b>Total TDS</b> :	 ${$('#total_tds').val()}</p>
        `;

        // Open a new window and print the form data
        const printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title>Form Data</title></head><body>');
        printWindow.document.write(formContent);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    }

    function clearAllFields(){
        $('#saveIncomeTax input:not([type="hidden"], [type="radio"]), #saveIncomeTax textarea').val(0);
        $('input[name="disability"][type="radio"]').prop('checked', false);

        if($("#tax_regime").val() == 1){
            $('#standard_deduction').val(50000);
        }else{
            $('#standard_deduction').val(75000);
            $('#total_exemptions').val(75000);
        }
    }



    </script>
