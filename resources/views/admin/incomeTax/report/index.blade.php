<x-admin.layout>
    <x-slot name="title">Form 16 List</x-slot>
    <x-slot name="heading">Form 16 List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('income-tax.report-excel') }}" class="theme-form" id="addForm
addForm" name="addForm">
                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label for="financial_year" class="form-label">Financial Year:</label>
                                <select class="form-select" id="financial_year" name="financial_year">
                                    @foreach($financialYears as $financialYear)
                                        <option value="{{ $financialYear->id }}">{{ $financialYear->title }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-4">
                                <label for="ward" class="form-label">Wards:</label>
                                <select class="form-select" id="ward" name="ward">
                                    <option value="">All</option>
                                    @foreach($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success" id="addSubmit">Download Excel</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-admin.layout>

{{-- <script>
    $(document).ready(function(){
        $('#addSubmit').click(function(){
            let param = $('#addForm').serialize();
            alert(param);exit;
            window.open(
                '{{ route("income-tax.report-excel") }}?'+param,
                '_blank' // <- This is what makes it open in a new window.
            );
        });
    })
</script> --}}
