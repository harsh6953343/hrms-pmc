
<table>
    <thead>
        <tr>
            <th>Sr. No.</th>
            <th>Financial Year</th>
            <th>EmployeeId</th>
            <th>Employee Name</th>
            <th>Gender</th>
            <th>Pan No</th>
            <th>Previous Employer Gross</th>
            <th>Gross Salary</th>
            <th>Estimated Salary</th>
            <th>Suplimentry Bill</th>
            <th>Estimeted Suplimentry Bill Salary</th>
            <th>Gross Income</th>
            <th>H.R.A</th>
            <th>Election Allowance</th>
            <th>Transport Allowance</th>
            <th>Previous Employer Professional Tax</th>
            <th>Professional Tax</th>
            <th>Ex_Gratia</th>
            <th>Standard Deduction</th>
            <th>Total Exemptions</th>
            <th>Net Salary</th>
            <th>Investments</th>
            <th>Other Investments</th>
            <th>total Investment</th>
            <th>Health Insurance(Age less than 60)</th>
            <th>Health Insurance(Age more than 60)</th>
            <th>Assesee's(Parent less than 60)</th>
            <th>Assesee's(Parent more than 60)</th>
            <th>Mediclaim</th>
            <th>Other Health Insurance</th>
            <th>80D</th>
            <th>Disability</th>
            <th>Handicapped</th>
            <th>80U</th>
            <th>Other Donation</th>
            <th>80G</th>
            <th>NSC Interest</th>
            <th>Donation</th>
            <th>Life Insurance Premiums</th>
            <th>Other Lic</th>
            <th>GPF</th>
            <th>DCPS Employee Contribution</th>
            <th>DCPS1  Employer Contribution</th>
            <th>Group Insurance Scheme</th>
            <th>Other Gis</th>
            <th>Public Provident Fund</th>
            <th>Post Linked Insurance</th>
            <th>NSC</th>
            <th>Housing Loan Repayment</th>
            <th>Other Pli</th>
            <th>Other Housing Loan Repayment</th>
            <th>Under 80ccc</th><th>Infrastructure Bond</th>
            <th>Education Fees</th>
            <th>80C</th>
            <th>Total 80C_80CCC</th>
            <th>Education Loan</th>
            <th>80E</th>
            <th>Home Loan Interest Before</th>
            <th>Home Loan Interest After</th>
            <th>Other Home Loan Interest Before</th>
            <th>Other Home Loan Interest After</th>
            <th>Section 24 Total</th>
            <th>Section 24</th>
            <th>Medical Treatment</th>
            <th>Total_80DDB</th>
            <th>80DDB</th>
            <th>SB Interest</th>
            <th>80TTA</th>
            <th>80CCD_1B</th>
            <th>80CCD2</th>
            <th>Tax paid in cash</th>
            <th>Total Taxable Salary</th>
            <th>Tax Rebate</th>
            <th>Tax Payable Before Cess</th>
            <th>Tax Payable After Cess</th>
            <th>Education Cess</th>
            <th>Tax payable</th>
            <th>Tax Deducted</th>
            <th>Tax Pending</th>
            <th>Tax For Remaining Months</th>
            <th>June Salary Tax</th>
        </tr>
    </thead>
    <tbody>
        @php $count = 1; @endphp
        @foreach($incomeTaxs as $incomeTax)
        <tr>
            <th>{{ $count++ }}</th>
            <th>{{ $incomeTax->financialYear?->title }}</th>
            <th>{{ $incomeTax->employee?->employee_id }}</th>
            <th>{{ $incomeTax->employee?->fname.' '.$incomeTax->employee?->mname.' '.$incomeTax->employee?->lname }}</th>
            <th>{{ ($incomeTax->employee?->gender) ? 'Male' : 'Female' }}</th>
            <th>{{ $incomeTax->employee?->pan }}</th>
            <th>{{ $incomeTax->prev_gross }}</th>
            <th>{{ $incomeTax->current_gross }}</th>
            <th>{{ $incomeTax->estimated_salary }}</th>
            <th>{{ $incomeTax->supplementary_bill }}</th>
            <th>{{ $incomeTax->estimated_supp_bill }}</th>
            <th>{{ $incomeTax->gross_income }}</th>
            <th>{{ $incomeTax->hre }}</th>
            <th>{{ $incomeTax->elec_allowance }}</th>
            <th>{{ $incomeTax->trans_allowance }}</th>
            <th>{{ $incomeTax->prev_ptax }}</th>
            <th>{{ $incomeTax->ptax }}</th>
            <th>{{ $incomeTax->ex_gratia }}</th>
            <th>{{ $incomeTax->standard_deduction }}</th>
            <th>{{ $incomeTax->total_exemptions }}</th>
            <th>{{ $incomeTax->gross_income - $incomeTax->total_exemptions }}</th>
            <th>{{ $incomeTax->investment }}</th>
            <th>{{ $incomeTax->other_investment }}</th>
            <th>{{ $incomeTax->investment + $incomeTax->other_investment }}</th>
            <th>{{ $incomeTax->hial60d }}</th>
            <th>{{ $incomeTax->hial60p }}</th>
            <th>{{ $incomeTax->hiam60d }}</th>
            <th>{{ $incomeTax->hiam60p }}</th>
            <th>{{ $incomeTax->medical_treatment }}</th>
            {{-- <th>Other Health Insurance</th> --}}
            <th>0</th>
            <th>{{ $incomeTax->medical_treatment }}</th>
            <th>{{ $incomeTax->sec80d }}</th>
            <th>{{ $incomeTax->disability }}</th>
            <th>{{ $incomeTax->handicappedalwce }}</th>
            <th>{{ $incomeTax->sec80u }}</th>
            <th>{{ $incomeTax->odon80g }}</th>
            <th>{{ $incomeTax->sec80g }}</th>
            <th>{{ $incomeTax->nsc_int }}</th>
            <th>{{ $incomeTax->don80g }}</th>
            <th>{{ $incomeTax->lic }}</th>
            <th>{{ $incomeTax->other_lic }}</th>
            <th>{{ $incomeTax->gpf }}</th>
            <th>{{ $incomeTax->dcps_empr_contrn }}</th>
            <th>{{ $incomeTax->dcps_empr_contrn }}</th>
            <th>{{ $incomeTax->gis }}</th>
            <th>{{ $incomeTax->other_gis }}</th>
            <th>{{ $incomeTax->ppf }}</th>
            <th>{{ $incomeTax->pli }}</th>
            <th>{{ $incomeTax->nsc }}</th>
            <th>{{ $incomeTax->hloan }}</th>
            <th>{{ $incomeTax->other_pli }}</th>
            <th>{{ $incomeTax->other_hloan }}</th>
            <th>{{ $incomeTax->mutual_fund }}</th>
            <th>{{ $incomeTax->infra_bond }}</th>
            <th>{{ $incomeTax->edu_fees }}</th>
            <th>{{ $incomeTax->sec80c }}</th>
            <th>{{ $incomeTax->total80c }}</th>
            <th>{{ $incomeTax->edu_loan }}</th>
            <th>{{ $incomeTax->sec80e }}</th>
            <th>{{ $incomeTax->hlib24 }}</th>
            <th>{{ $incomeTax->hlia24 }}</th>
            <th>{{ $incomeTax->ohlib24 }}</th>
            <th>{{ $incomeTax->ohlia24 }}</th>
            <th>{{ $incomeTax->totalsec24 }}</th>
            <th>{{ $incomeTax->sec24 }}</th>
            <th>{{ $incomeTax->medical_treatment }}</th>
            <th>{{ $incomeTax->total80ddb }}</th>
            <th>{{ $incomeTax->sec80ddb }}</th>
            <th>{{ $incomeTax->sb_int_ded }}</th>
            <th>{{ $incomeTax->total80tta }}</th>
            <th>{{ $incomeTax->sec80ccd }}</th>
            <th>{{ $incomeTax->sec80ccd2 }}</th>
            <th>{{ $incomeTax->tax_paid_cash }}</th>
            <th>{{ $incomeTax->taxable_sal }}</th>
            <th>{{ $incomeTax->rebated_amount }}</th>
            <th>{{ $incomeTax->net_tax }}</th>
            <th>{{ $incomeTax->tax_after_cess }}</th>
            <th>{{ $incomeTax->edu_cess }}</th>
            <th>{{ $incomeTax->tax_payable }}</th>
            <th>{{ $incomeTax->tax_deducted }}</th>
            <th>{{ $incomeTax->tax_pending }}</th>
            <th>{{ $incomeTax->tax_rem_months }}</th>
            <th>0</th>
            {{-- <th>June Salary Tax</th> --}}
        </tr>
        @endforeach
    </tbody>
</table>

