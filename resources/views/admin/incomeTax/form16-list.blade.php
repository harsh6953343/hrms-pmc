<x-admin.layout>
    <x-slot name="title">Form 16 List</x-slot>
    <x-slot name="heading">Form 16 List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form href="{{ route('form16-list-fy') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label for="financial_year" class="form-label">Financial Year:</label>
                                <select class="form-select" id="financial_year" name="financial_year_id">
                                    @foreach($finYears as $id => $range)
                                        <option value="{{$id}}">{{$range}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <a href="{{ route('form16-list') }}" class="btn btn-warning">Refresh</a>
                        <a href="export-yearly-tax/{{$selectedFinYear}}" class="btn btn-success">Download Excel</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>Ward</th>
                                    <th>Tax Payable</th>
                                    <th>Tax Deducted</th>
                                    <th>Tax Pending</th>
                                    <th>Salary Slip</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($form16_list as $f)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $f->Emp_Code }}</td>
                                        <td>{{ $f->employee->fname.' '.$f->employee->mname.' '.$f->employee->lname }}</td>
                                        <td>{{ $f->ward->name }}</td>
                                        <td>{{ $f->tax_after_cess }}</td>
                                        <td>{{ $f->tax_deducted }}</td>
                                        <td>{{ $f->tax_pending }}</td>
                                        <td>
                                            @can('form16-list.view')
                                                <a href="{{ route('show-form16-pdf', $f->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="View Form 16" data-id="{{ $f->id }}">View Form 16</a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>

