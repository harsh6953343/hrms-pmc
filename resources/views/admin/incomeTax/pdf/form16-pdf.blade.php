<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Income Tax Calculation</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 12pt;
            margin: 20mm; /* Adjust margins for A4 */
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
            text-align: left;
            vertical-align: top;
        }
        th {
            background-color: #f2f2f2;
        }
        .header {
            text-align: center;
            /* margin-bottom: 10px; */
            border: 1px solid #000;
        }
        .footer {
            margin-top: 20px;
            text-align: right;
        }
        .page-break {
            /* page-break-before: always; */
        }
        .amount {
            text-align: right;
        }
        .underline {
            text-decoration: underline;
        }
        .td_extend {
          height: 50px;
          font-size: 20px;
          vertical-align: bottom; 
        }
        .spaced-text {
            white-space: pre;
        }
        .border_for_div{
          border: 1px solid #000;
        }
        /* Styles for Snappy PDF (if needed) */
        @page {
            size: A4; /* Explicitly set page size for Snappy */
            margin: 20mm;
        }
    </style>
</head>
<body>

    <div class="header">
        <h3>PANVEL MUNICIPAL CORPORATION</h3>
    </div>
    <div class="header">
        <h4>Income Tax Calculation for the Financial Year {{$financial_year}}</h4>
    </div>

    <table>
        <tr>
            <td width="50%">
                Employee Name: {{ $itdetails->employee->fname.' '.$itdetails->employee->mname.' '.$itdetails->employee->lname }}<br><br>
                Department: {{ $itdetails->department->name }}<br><br>
                Gender: {{ $gender[$itdetails->employee->gender] }}
            </td>
            <td width="50%">
                Employee ID: {{ $itdetails->employee->employee_id }}<br><br>
                PAN No: {{ $itdetails->employee->pan }}
            </td>
        </tr>
    </table>

    {{-- <h2>INCOME FROM SALARY</h2> --}}

    <table>
      <tr>
        <td colspan=3  class="td_extend" width="70%">INCOME FROM SALARY</td>
      </tr>
      <tr>
        <td width="70%">1. Gross Salary</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->gross_income }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">2. Add   I. Ex Gratia/Arrears/Others<br>              II. Election Allowance</td>
        <td class="amount">{{$itdetails->ex_gratia}}<br>{{ $itdetails->elec_allowance }}</td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 2</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->ex_gratia + $itdetails->elec_allowance}}</td>
      </tr>
      <tr>
        <td width="70%">3. Total Salary (1+2)</td>
        <td class="amount"></td>
        @php
          $total12 = $itdetails->gross_income + $itdetails->ex_gratia + $itdetails->elec_allowance;
        @endphp
        <td class="amount">{{  $total12 }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">4. Less   I.   Standard Deduction
                II.  Transport Allowance u/s 10(14)
                III. House rent Allowance u/s 10(13A)
                IV. Professional Tax u/s 16(11)
                 V. Interest Paid on Home Loans u/s 24(2)
                      If Purchased/Bought before 01-04-1999 (Max Rs. 30,000/-)
                      If Purchased/Bought after 01-04-1999 (Max Rs. 2,00,000/-)
        </td>
        <td class="amount">{{ $itdetails->standard_deduction }}<br>{{ $itdetails->trans_allowance }}<br>{{ $itdetails->hre }}<br>2500<br><br>{{ $itdetails->hlib24 }}<br>{{ $itdetails->hlia24 }}</td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 4</td>
        <td class="amount"></td>
        @php
          $total4 = $itdetails->standard_deduction + $itdetails->trans_allowance + $itdetails->hre + 2500 + $itdetails->hlib24 + $itdetails->hlia24;
        @endphp
        <td class="amount">{{ $total4 }}</td>
      </tr>
      <tr>
        <td width="70%">5. Total Salary Income (3-4)</td>
        <td class="amount"></td>
        @php
          $total34 = $total12 - $total4;
        @endphp
        <td class="amount">{{ $total34 }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">6. Add I. NSC Interest<br>            II. Others</td>
        <td class="amount"><br></td>
        <td class="amount"></td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Perk - Where accommodation is un furnished</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->un_furnished }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Perk-Furnished-Value as if accommodation is unfurnished</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->perk_furnished_value }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Interest free or concessional loans</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->interest_free_or_concessional_loans }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Furniture Rentals</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->furniture_rentals }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Perquisite value of furniture</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->perquisite_value_of_furniture }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Free education</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->free_education }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Gifts, vouchers, etc.</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->gifts_vouchers_etc }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Credit card expenses</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->credit_card_expenses }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Club expenses</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->club_expenses }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Use of movable assets by employees</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->use_of_movable_assets_by_employees }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Transfer of assets to employees</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->transfer_of_assets_to_employees }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Stock options (non-qualified options)</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->stock_option_non_qualified_options }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Stock options referred u/s 80-IAC</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->stock_options_referred_80_iac }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Contribution by employer u/s 17(2)(vii)</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->contribution_by_employer }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Annual accretion taxable u/s 17(2)(viia)</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->annual_accretion_taxable }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Sweeper, gardener, watchman or personal attendant</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->personal_attendant }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Gas, electricity, water</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->gas_electricity_water }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Cost of furniture</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->cost_of_furniture }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Holiday expenses</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->holiday_expenses }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Free meals</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->free_meals }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Perk-Furnished-Total</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->perk_furnished_total }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Rent paid by employee</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->rent_paid_by_employee }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Value of perquisites</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->value_of_perquisites }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Conveyance</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->conveyance }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Remuneration paid on behalf of employee</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->remuneration_paid_on_behalf_of_employee }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Taxable LTC</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->taxable_ltc }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Other benefits</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->other_benefits }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     PF in excess of 12%</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->pf_in_excess_of_12 }}</td>
      </tr>
      <tr>
        <td class="spaced-text" width="70%">     Excess interest credited</td>
        <td class="amount"></td>
        <td class="amount">{{ $itdetails->otherIncomeValue->excess_iterest_credited }}</td>
      </tr>
      <tr>
        <td class="amount" width="70%">Total - 6</td>
        <td class="amount"></td>
        @php
          $total6 = $itdetails->other_income;
        @endphp
        <td class="amount">{{ $total6 }}</td>
      </tr>
      <tr>
        <td width="70%">7. Gross Total Salary Income (5+6)</td>
        <td class="amount"></td>
        @php
          $total56 = $total34 + $total6;
        @endphp
        <td class="amount">{{ $total56 }}</td>
      </tr>
    </table>

    <table>
        <tr>
          <td colspan=3 class="td_extend" width="70%">8. Deduction under VI A</td>
        </tr>
        <tr>
            <td colspan=3 width="70%">(A) Section 80C, 80CCC and 80CCD</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">   (a) Section 80C</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Life Insurance Premium</td>
            <td class="amount">{{ $itdetails->lic }}</td>
            <td></td>
        </tr>
        @if($itdetails->gpf)
          <tr>
              <td class="spaced-text" width="70%">         Government Provident Fund</td>
              <td class="amount">{{ $itdetails->gpf }}</td>
              <td></td>
          </tr>
        @else
          <tr>
            <td class="spaced-text" width="70%">         DCPS Employee Contribution</td>
            <td class="amount">{{ $itdetails->dcps_emp }}</td>
            <td></td>
          </tr>
        @endif
        <tr>
            <td class="spaced-text" width="70%">         Group Insurance Scheme</td>
            <td class="amount">{{ $itdetails->gis }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Public Provident Fund</td>
            <td class="amount">{{ $itdetails->ppf }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         National Savings Certificate</td>
            <td class="amount">{{ $itdetails->nsc }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Post Linked Insurance</td>
            <td class="amount">{{ $itdetails->pli }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Housing Loan Repayment</td>
            <td class="amount">{{ $itdetails->hloan }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Education Fees (Two Children)</td>
            <td class="amount">{{ $itdetails->edu_fees }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other Tax Saving Investment</td>
            <td class="amount"></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         NSC Interest</td>
            <td class="amount">{{ $itdetails->nsc_int }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other LIC</td>
            <td class="amount">{{ $itdetails->other_lic }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Under 80ccc Mutual Fund:</td>
            <td class="amount">{{ $itdetails->mutual_fund }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other GIS:</td>
            <td class="amount">{{ $itdetails->other_gis }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other PLI:</td>
            <td class="amount">{{ $itdetails->other_pli }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Other Housing Loan Repayment:</td>
            <td class="amount">{{ $itdetails->other_hloan }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Infrastructure Bond</td>
            <td class="amount">{{ $itdetails->infra_bond }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         PPF</td>
            <td class="amount">{{ $itdetails->ppf }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">         Education Fees</td>
            <td class="amount">{{ $itdetails->edu_fees }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Total</td>
            <td></td>
            <td class="amount">{{ $itdetails->sec80c }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (b) Section 80CCC-Annuity Pension Plan</td>
            <td class="amount"></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (c) Section 80CCD-Govt. Pension Scheme</td>
            <td class="amount">{{ $itdetails->sec80ccd }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">    NPS</td>
            <td class="amount">{{ $itdetails->nps }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  DCPS + Under 80CCD</td>
            <td class="amount">{{ $itdetails->dcps_u80ccd }}</td>
            <td></td>
        </tr>
    </table>

    <table>
        <tr>
          <td colspan=3 class="td_extend" width="70%">(B) Other Sections under Chapter VIA</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (a) Section 80D - Mediclaim</td>
            <td class="amount">{{ $itdetails->sec80d }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Health Insurance (Age less Than 60)</td>
            <td class="amount">{{ $itdetails->hial60d }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Health Insurance (Age More Than 60) :	</td>
            <td class="amount">{{ $itdetails->hiam60d }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Health-Checkup :	</td>
            <td class="amount">{{ $itdetails->hcheckup }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Health Insurance (Age less Than 60) (Parents)</td>
            <td class="amount">{{ $itdetails->hial60p }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Health Insurance (Age More Than 60)(Parents)</td>
            <td class="amount">{{ $itdetails->hiam60p }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (b) Section 80E - Education Loan</td>
            <td class="amount">{{ $itdetails->sec80e  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Education Loan</td>
            <td class="amount">{{ $itdetails->edu_loan  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (c) Section 80G - Donation</td>
            <td class="amount">{{ $itdetails->sec80g  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">     Donation</td>
            <td class="amount">{{ $itdetails->don80g  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">      Other Donation</td>
            <td class="amount">{{ $itdetails->odon80g  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (d) Section 80U - Handicapped Person ({{ $itdetails->disability  }}%) </td>
            <td class="amount">{{ $itdetails->sec80u  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (e) Section 80CCD(2) - Employer DCPS Contribution</td>
            <td class="amount">{{ $itdetails->sec80ccd2  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (f) Section 80DDB - Medical Treatment Expenses</td>
            <td class="amount">{{ $itdetails->sec80ddb  }}</td>
            <td></td>
        </tr>
        <tr>
          <td class="spaced-text" width="70%">  (f) Section 80TTA</td>
          <td class="amount">{{ $itdetails->sec80tta  }}</td>
          <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  (g) Section 24</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Home Loan Interest Before </td>
            <td class="amount">{{ $itdetails->hlib24  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Oth Home Loan Interest Before: </td>
            <td class="amount">{{ $itdetails->ohlib24  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Home Loan Interest After </td>
            <td class="amount">{{ $itdetails->hlia24  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Oth Home Loan Interest After </td>
            <td class="amount">{{ $itdetails->ohlia24  }}</td>
            <td></td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">  Total</td>
            @php
              $total_other_sections = $itdetails->sec80d + $itdetails->sec80e + $itdetails->sec80g + $itdetails->sec80u + $itdetails->sec80ccd2 + $itdetails->sec80ddb;
            @endphp
            <td></td>
            <td class="amount">{{ $total_other_sections }}</td>
        </tr>
    </table>

    <table>
        <tr>
            <td width="70%">9. Aggregate of deductible amounts under chapter VIA</td>
            <td></td>
            <td class="amount">{{ $itdetails->sec80c + $itdetails->sec80ccd + $total_other_sections  }}</td>
        </tr>
        <tr>
            <td width="70%">10. Total Taxable Salary (7-8)</td>
            <td></td>
            <td class="amount">{{ $itdetails->taxable_sal }}</td>
        </tr>
    </table>

    {!!$slabwise_table!!}

    <table>
        <tr>
            <td width="70%">12. a) Net Tax Payable</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_payable  }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">      b) Rebate under Section 87a (Financial Year 2013-2014 Onwards)</td>
            <td></td>
            <td class="amount">{{ $itdetails->rebated_amount }}</td>
        </tr>
        <tr>
            <td class="spaced-text" width="70%">      c) Tax Payable After Rebate under Section 87a</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_payable  }}</td>
        </tr>
        <tr>
            <td width="70%">13. Add @ 4% Education Cess on Net Tax Amount</td>
            <td></td>
            <td class="amount">{{ $itdetails->edu_cess  }}</td>
        </tr>
        <tr>
            <td width="70%">14. Tax Paid In Cash</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_paid_cash  }}</td>
        </tr>
        <tr>
            <td width="70%">15. Total Tax Payable (12+13)</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_after_cess  }}</td>
        </tr>
        <tr>
            <td width="70%">16. Tax already deducted / paid (if any)</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_deducted  }}</td>
        </tr>
        <tr>
            <td width="70%">Tax Pending</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_pending }}</td>
        </tr>
        <tr>
            <td width="70%">Tax for remaining months</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_rem_months }}</td>
        </tr>
        {{--<tr>
            <td width="70%">Total TDS</td>
            <td></td>
            <td class="amount">{{ $itdetails->total_tds }}</td>
        </tr>--}}
        <tr>
            <td width="70%">Tax Refunded</td>
            <td></td>
            <td class="amount">{{ $itdetails->tax_refunded }}</td>
        </tr>
        <tr>
            <td width="70%">User Requested Deduction</td>
            <td></td>
            <td class="amount">{{ $itdetails->user_req_ded }}</td>
        </tr>
        <tr>
            <td width="70%">Reason For Requestd Deduction	</td>
            <td></td>
            <td class="amount">{{ $itdetails->reason_requested_ded }}</td>
        </tr>
    </table>

    <div class="border_for_div">
      <p>You are hereby called upon to submit the details of investments, if any, you want to make to reduce the Tax Liability. The details shall reach the Accounts Department on or before 26th December of the financial year. If the details are not submitted along with Xerox Copy, Income Tax TDS as mentioned above will be deducted from the salary and deposited in Govt. Treasury</p>

      <div class="footer">
          <b>Chief Accounts Officer<br>
          Panvel Municipal Corporation</b>
      </div>
    <div>

</body>
</html>