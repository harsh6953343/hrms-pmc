@php
    use Illuminate\Support\Facades\Auth;
    $authUser = Auth::user();
@endphp
<x-admin.layout>
    <x-slot name="title">Dashboard</x-slot>
    <x-slot name="heading">Dashboard</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}

     <div class="row">
        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-secondary-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3"> Total Employees</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{ $employees }}">{{ $employees }}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!--end col-->
        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-success-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3">Departments</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{ $departments }}">{{ $departments }}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div>
        </div><!--end col-->
        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-warning-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3">Groups</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{ $wards }}">{{ $wards }}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div>
        </div><!--end col-->

        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-dark-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3">PF Employees</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{$pf_employees_count}}">{{$pf_employees_count}}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div>
        </div><!--end col-->
        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-info-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3">NPS Employees</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{$nps_employees_count}}">{{$nps_employees_count}}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div>
        </div><!--end col-->
        <div class="col-4">
            <div class="card card-animate overflow-hidden">
                <div class="card-body bg-primary-subtle" style="z-index:1 ;">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                            <p class="text-uppercase fw-semibold text-muted text-truncate mb-3">Pensioners</p>
                            <h4 class="fs-22 fw-semibold ff-secondary mb-0"><span class="counter-value" data-target="{{$pensioners_count}}">{{$pensioners_count}}</span></h4>
                        </div>
                        <div class="flex-shrink-0">
                            <div id="total_jobs" data-colors='["--vz-success"]' class="apex-charts" dir="ltr"></div>
                        </div>
                    </div>
                </div><!-- end card body -->
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Monthly Attendance</h4>
                </div>
                <div class="card-body">
                    <div id="attendance-chart-bar" class="e-charts"></div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Salary Pay (In lakhs)</h4>
                </div>
                <div class="card-body">
                    <div id="salary-pay" data-colors='["--vz-success"]' class="e-charts"></div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Retired Employee Count (current and upcoming months)</h4>
                </div>
                <div class="card-body">
                    <div class="live-preview">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered align-middle table-nowrap mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Month</th>
                                        <th scope="col">Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($retire_emps_count as $re)
                                    <tr>
                                        <td>{{$re->ret_month_name}}</td>
                                        <td>{{$re->ret_count}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Group Wise Employees</h4>
                </div>
                <div class="card-body">
                    <div class="live-preview">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered align-middle table-nowrap mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Group</th>
                                        <th scope="col">Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($group_wise_employees as $group_wise_employee)
                                    <tr>
                                        <td>{{ $group_wise_employee->ward->name }}</td>
                                        <td>{{ $group_wise_employee->total_employees }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div><!--end row-->

    <script src="{{ asset('admin/libs/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('admin/js/pages/echarts.init.js') }}"></script>

</x-admin.layout>


<script>
    $(document).ready(function() {

        let bar_chart_months = '<?php echo $bar_chart_months; ?>'
        let presents = '<?php echo $presents; ?>'
        let leaves = '<?php echo $leaves; ?>'
        let absents = '<?php echo $absents; ?>'
        bar_chart_months = bar_chart_months.split(',')
        presents = presents.split(',')
        leaves = leaves.split(',')
        absents = absents.split(',')

        var chart = echarts.init(document.getElementById('attendance-chart-bar'));

        var options = {
            title: {
                text: '',
                left: 'center'
            },
            grid: {
                left: "1%",
                right: "0%",
                bottom: "0%",
                containLabel: !0
            },
            tooltip: {
                trigger: 'axis',
            },
            legend: {
                data: ['Present', 'Leave', 'Absent'],
                textStyle: {
                    color: "#858d98"
                }
            },
            xAxis: {
                type: 'category',
                data: bar_chart_months
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: 'Present',
                    type: 'bar',
                    data: presents,
                    label: {
                        show: true,
                        position: 'top',
                        color: '#000',
                        fontSize: 14
                    },
                    itemStyle: {
                        color: '#41BB82'
                    }
                },
                {
                    name: 'Leave',
                    type: 'bar',
                    data: leaves,
                    label: {
                        show: true,
                        position: 'top',
                        color: '#000',
                        fontSize: 14
                    },
                    itemStyle: {
                        color: '#FFCA5C'
                    }
                },
                {
                    name: 'Absent',
                    type: 'bar',
                    data: absents,
                    label: {
                        show: true,
                        position: 'top',
                        color: '#000',
                        fontSize: 14
                    },
                    itemStyle: {
                        color: '#EE6352'
                    }
                }
            ]
        };

        chart.setOption(options);

        let salary_paid = '<?php echo $salary_paid; ?>'
        salary_paid = salary_paid.split(',')

        var chart = echarts.init(document.getElementById('salary-pay'));
        var options = {
            title: {
                text: '',
                left: 'center'
            },
            grid: {
                left: "1%",
                right: "0%",
                bottom: "0%",
                containLabel: !0
            },
            tooltip: {
                trigger: 'axis',
            },
            legend: {
                data: ['Amount'],
                textStyle: {
                    color: "#3498db"
                }
            },
            xAxis: {
                type: 'category',
                data: bar_chart_months
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: 'Amount',
                    type: 'bar',
                    data: salary_paid,
                    label: {
                        show: true,
                        position: 'top',
                        color: '#000',
                        fontSize: 14
                    },
                    itemStyle: {
                        color: '#3498db'
                    }
                },
            ]
        };
        chart.setOption(options);
        
    });
</script>
