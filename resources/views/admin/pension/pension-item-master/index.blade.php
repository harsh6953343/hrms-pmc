<x-admin.layout>
    <x-slot name="title">Pension Item Master</x-slot>
    <x-slot name="heading">Pension Item Master</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Pension Item Master</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Name <span class="text-danger">*</span></label>
                                <input class="form-control" id="name" name="name" type="text" placeholder="Enter Ward Name" required>
                                <span class="text-danger invalid name_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="basis">Basis <span class="text-danger">*</span></label>
                                <select name="basis" id="basis" class="form-select" required>
                                    <option value="2">Amount</option>
                                    <option value="1">Percentage</option>
                                    <option value="3">Other</option>
                                </select>
                                <span class="text-danger invalid basis_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="based_on">Based On</label>
                                <select name="based_on" id="based_on" class="form-select">
                                    <option value="">Select option</option>
                                    @foreach($basedOns as $basedOn)
                                    <option value="{{ $basedOn->id }}">{{ $basedOn->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid based_on_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="amount">Amount <span class="text-danger">*</span></label>
                                <input class="form-control" id="amount" name="amount" type="text" placeholder="Enter Ward Amount" required>
                                <span class="text-danger invalid amount_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="type_of_pay">Type of pay <span class="text-danger">*</span></label>
                                <select name="type_of_pay" id="type_of_pay" class="form-select" required>
                                    <option value="">Select Option</option>
                                    <option value="1">Earning</option>
                                    <option value="0">Deduction</option>
                                </select>
                                <span class="text-danger invalid type_of_pay_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="statuss">Status <span class="text-danger">*</span></label>
                                <select name="status" id="statuss" class="form-select" required>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <span class="text-danger invalid status_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Pension Item Master</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Name <span class="text-danger">*</span></label>
                                <input class="form-control" id="name" name="name" type="text" placeholder="Enter Ward Name" required>
                                <span class="text-danger invalid name_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="basis">Basis <span class="text-danger">*</span></label>
                                <select name="basis" id="basis" class="form-select" required>
                                    <option value="2">Amount</option>
                                    <option value="1">Percentage</option>
                                    <option value="3">Other</option>
                                </select>
                                <span class="text-danger invalid basis_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="based_on">Based On</label>
                                <select name="based_on" id="based_on" class="form-select">
                                    <option value="">Select option</option>
                                </select>
                                <span class="text-danger invalid based_on_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="amount">Amount <span class="text-danger">*</span></label>
                                <input class="form-control" id="amount" name="amount" type="text" placeholder="Enter Ward Amount" required>
                                <span class="text-danger invalid amount_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="type_of_pay">Type of pay <span class="text-danger">*</span></label>
                                <select name="type_of_pay" id="type_of_pay" class="form-select" required>
                                    <option value="">Select Option</option>
                                    <option value="1">Earning</option>
                                    <option value="0">Deduction</option>
                                </select>
                                <span class="text-danger invalid type_of_pay_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="statuss">Status <span class="text-danger">*</span></label>
                                <select name="status" id="statuss" class="form-select" required>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <span class="text-danger invalid status_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('departments.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Name</th>
                                    <th>Basis</th>
                                    <th>Based on</th>
                                    <th>Amount</th>
                                    <th>Type of pay</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pensionItemMasters as $pensionItemMaster)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pensionItemMaster->name }}</td>
                                    <td>{{ ["1" => "Percentage", "2" => "Amount", "3" => "Other"][$pensionItemMaster->basis] ?? '-' }}</td>
                                    <td>{{ $pensionItemMaster->pensionItemMaster?->name }}</td>
                                    <td>{{ $pensionItemMaster->amount }}</td>
                                    <td>{{ ["1" => "Earning", "0" => "Deduction"][$pensionItemMaster->type_of_pay] ?? '-' }}</td>
                                    <td>
                                        @if($pensionItemMaster->status)
                                        <span class="badge bg-success">Active</span>
                                        @else
                                        <span class="badge bg-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <button class="edit-element btn btn-secondary px-2 py-1" title="Edit ward" data-id="{{ $pensionItemMaster->id }}"><i data-feather="edit"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('pension-item-master.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('pension-item-master.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('pension-item-master.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {
                    $("#editForm input[name='edit_model_id']").val(data.pensionItemMaster.id);
                    $("#editForm input[name='name']").val(data.pensionItemMaster.name);
                    $("#editForm input[name='amount']").val(data.pensionItemMaster.amount);
                    $("#editForm select[name='basis']").val(data.pensionItemMaster.basis);
                    $("#editForm select[name='type_of_pay']").val(data.pensionItemMaster.type_of_pay);
                    $("#editForm select[name='based_on']").html(data.pensionItemMasterHtml);
                    // $("#edit_ward").html(data.wardHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('pension-item-master.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('pension-item-master.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>

