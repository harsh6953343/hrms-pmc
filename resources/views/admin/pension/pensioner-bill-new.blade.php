
<x-admin.layout>
    <x-slot name="title">Pensioner Bill</x-slot>
    <x-slot name="heading">Pensioner Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('pensioner-bill-new.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data" method="GET">
                    <div class="card-body">
                        <div class="mb-3 row">
                            
                            <div class="col-md-3">
                                <label class="col-form-label" for="pensioner_type">Pensioner Type<span class="text-danger">*</span></label>
                                <select class="form-select" id="pensioner_type" name="pensioner_type" required>
                                    <option value="">Select Type</option>
                                    <option value="all" {{ (request()->pensioner_type == "all")?'Selected':'' }}>Regular</option>
                                    <option value="family" {{ (request()->pensioner_type == "family") ?'selected' : '' }}>Family</option>
                                    {{-- @foreach ($getPensionerType as $getPensionerTyp)
                                        @if($getPensionerTyp->pensioner_type == 'vol_ret')
                                            <option value="{{ $getPensionerTyp->pensioner_type }}" {{ ($pensioner_type == $getPensionerTyp->pensioner_type) ? 'Selected':'' }}  >Voluntary Retirement</option>
                                        @elseif($getPensionerTyp->pensioner_type == 'ret_med')
                                            <option value="{{ $getPensionerTyp->pensioner_type }}" {{ ($pensioner_type == $getPensionerTyp->pensioner_type) ? 'Selected':'' }}  >Retirement Due to Medical Reasons</option>
                                        @else
                                            <option value="{{ $getPensionerTyp->pensioner_type }}" {{ ($pensioner_type == $getPensionerTyp->pensioner_type) ? 'Selected':'' }}  >{{ $getPensionerTyp->pensioner_type }}</option>
                                        @endif
                                    @endforeach --}}
                                </select>
                                @error('pensioner_type')
                                <span class="text-danger invalid pensioner_type_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="ward_id">Group<span class="text-danger">*</span></label>
                                <select class="form-select" id="ward_id" name="ward_id" required>
                                    <option value="">Select Group</option>
                                    <option value="all" {{ (request()->ward_id == "all")?'Selected':'' }}>All</option>
                                    @foreach ($getWards as $getWard)
                                        <option value="{{ $getWard->ward_id }}" {{ ($ward_id == $getWard->ward_id) ? 'Selected':'' }}  >{{ $getWard->ward?->name }}</option>
                                    @endforeach
                                </select>
                                @error('ward_id')
                                <span class="text-danger invalid ward_id_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="bank">Banks<span class="text-danger">*</span></label>
                                <select class="form-select" id="bank" name="bank" required>
                                    <option value="">Select Bank</option>
                                    <option value="all" {{ (request()->bank == "all")?'Selected':'' }}>All</option>
                                    @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}" {{ ($bank_id == $bank->id) ? 'Selected':'' }}  >{{ $bank->name }}</option>
                                    @endforeach
                                </select>
                                @error('bank')
                                <span class="text-danger invalid bank_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month" required>
                                    <option value="">Select Month</option>
                                    <option value="1" {{ ($month == 1) ? 'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2) ? 'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3) ? 'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4) ? 'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5) ? 'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6) ? 'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7) ? 'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8) ? 'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9) ? 'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10) ? 'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11) ? 'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12) ? 'Selected':'' }}>December</option>
                                </select>
                                @error('month')
                                <span class="text-danger invalid month_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                @error('from_date')
                                <span class="text-danger invalid from_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                @error('to_date')
                                <span class="text-danger invalid to_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Show</button>
                        <button type="button" class="btn btn-success" id="pdfAddSubmit">PDF</button>
                        <a href="{{ route('pensioner-bill-new.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="table-responsive">
                    <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Pensioner ID</th>
                                <th>Employee Name</th>
                                <th>Pensioner Name</th>
                                <th>Pensioner Type</th>
                                <th>Bank Name</th>
                                <th>Account No</th>
                                <th>IFSC Code</th>
                                <th>Basic</th>
                                <th>Dearness Relief</th>
                                <th>Pension Arrears</th>
                                <th>Total Earning</th>
                                <th>Miscellaneous Deduction</th>
                                <th>Commutation</th>
                                <th>Recovery</th>
                                <th>Total Deduction</th>
                                <th>Net Salary</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($freeze_pensions as $freeze_pension)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $freeze_pension->pension_id }}</td>
                                    <td>{{ $freeze_pension->employee_name }}</td>
                                    <td>{{ $freeze_pension->pensioner_name }}</td>
                                    @if($freeze_pension->pensioner_type == 'vol_ret')
                                        <td>Voluntary Retirement</td>
                                    @elseif($freeze_pension->pensioner_type == 'ret_med')
                                        <td>Retirement Due to Medical Reasons</td>
                                    @else
                                        <td>{{ $freeze_pension->pensioner_type }}</td>
                                    @endif
                                    <td>{{ $freeze_pension?->bank?->name }}</td>
                                    <td>{{ $freeze_pension->account_no }}</td>
                                    <td>{{ $freeze_pension->ifsc_code }}</td>
                                    <td>{{ $freeze_pension->basic }}</td>
                                    <td>{{ $freeze_pension->dearness_relief }}</td>
                                    <td>{{ $freeze_pension->pension_arrears }}</td>
                                    <td>{{ $freeze_pension->total_earning }}</td>
                                    <td>{{ $freeze_pension->miscellaneous_deduction }}</td>
                                    <td>{{ $freeze_pension->commutation }}</td>
                                    <td>{{ $freeze_pension->recovery }}</td>
                                    <td>{{ $freeze_pension->total_deduction }}</td>
                                    <td>{{ $freeze_pension->net_salary }}</td>
                                </tr>
                            @endforeach
                    </table>
                </div>

            </div>
        </div>
    </div>



</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>

    <script>
        $('#pdfAddSubmit').click(function(){
            let url = $('#addForm').serialize();
            
            // let form = $('#addForm');
            let form = document.getElementById("addForm");
            if (form.checkValidity()) {
                // alert("Form is valid!");
                window.open(
                    "{{ route('pensioner-bill-new.pdf') }}?"+url,
                    '_blank'
                );
            } else {
                form.reportValidity(); // Highlights invalid fields
            }

        });
    </script>
