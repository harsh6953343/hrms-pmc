<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Pension</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" id="DA_rate" value="{{ $da_rate->amount }}">

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_retire_after_2016">Is Employee Retire After 2016 ?<span class="text-danger">*</span></label>
                                <select class="form-select" id="emp_retire_after_2016" name="emp_retire_after_2016">
                                    <option value="">Select  Is Employee Retire After 2016 ? </option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                                <span class="text-danger invalid emp_retire_after_2016_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="is_emp_dr">Is Employee Doctor ?<span class="text-danger">*</span></label>
                                <select class="form-select" id="is_emp_dr" name="is_emp_dr">
                                    <option value="">Select Is Employee Doctor ?</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                                <span class="text-danger invalid is_emp_dr_err"></span>
                            </div>
                        </div>
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Enter Employee Name">
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Enter Designation">
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_type">Select Pay Type<span class="text-danger">*</span></label>
                                <select class="form-select pay_type" id="pay_type" name="pay_type">
                                    <option value="">Select Pay Type</option>
                                    <option value="1">6 Pay</option>
                                    <option value="2">7 Pay</option>
                                </select>
                                <span class="text-danger invalid pay_type_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="basic_salary">Basic Salary <span class="text-danger">*</span></label>
                                <input class="form-control" id="basic_salary" name="basic_salary" type="number" placeholder="Enter basic_salary">
                                <span class="text-danger invalid basic_salary_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dob">Date of Birth<span class="text-danger">*</span></label>
                                <input class="form-control" id="dob" name="dob" type="date" placeholder="Enter Date of Birth">
                                <span class="text-danger invalid dob_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="age">Age<span class="text-danger">*</span></label>
                                <input class="form-control" id="age" name="age" type="text" placeholder="Enter Age" readonly>
                                <span class="text-danger invalid age_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="retirement_date">Retirement Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="retirement_date" name="retirement_date" type="date" placeholder="Enter Retirement Date">
                                <span class="text-danger invalid retirement_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="date_of_death">Date of Death (If Applicable)<span class="text-danger"></span></label>
                                <input class="form-control" id="date_of_death" name="date_of_death" type="date" placeholder="Enter Date of Death (If Applicable)">
                                <span class="text-danger invalid date_of_death_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="account_number">Account Number<span class="text-danger">*</span></label>
                                <input class="form-control" id="account_number" name="account_number" type="text" placeholder="Enter Account Number">
                                <span class="text-danger invalid account_number_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pension_bank_id">Select Bank<span class="text-danger">*</span></label>
                                <select class="form-select pension_bank_id" id="pension_bank_id" name="pension_bank_id">
                                    <option value="">Select Bank</option>
                                    @foreach ($pension_banks as $pension_bank)
                                        <option value="{{ $pension_bank->id }}">{{ $pension_bank->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid pension_bank_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="branch_name">Branch Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="branch_name" name="branch_name" type="text" placeholder="Enter Branch Name">
                                <span class="text-danger invalid branch_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ifsc_code">IFSC Code<span class="text-danger">*</span></label>
                                <input class="form-control" id="ifsc_code" name="ifsc_code" type="text" placeholder="Enter IFSC Code">
                                <span class="text-danger invalid ifsc_code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="differance">Differance<span class="text-danger">*</span></label>
                                <input class="form-control" id="differance" name="differance" type="number" placeholder="Enter Differance">
                                <span class="text-danger invalid differance_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="deduction">Deduction<span class="text-danger">*</span></label>
                                <input class="form-control" id="deduction" name="deduction" type="number" placeholder="Enter Deduction">
                                <span class="text-danger invalid deduction_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="business_allowances">Dr. Business Allowances<span class="text-danger">*</span></label>
                                <input class="form-control" id="business_allowances" name="business_allowances" type="number" placeholder="Enter Dr. Business Allowances">
                                <span class="text-danger invalid business_allowances_err"></span>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="calculated_basic">Calculated Basic<span class="text-danger">*</span></label>
                                <input class="form-control" id="calculated_basic" name="calculated_basic" type="number" placeholder="Calculated Basic" readonly>
                                <span class="text-danger invalid calculated_basic_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="payable_pension">Payable Pension Amount<span class="text-danger">*</span></label>
                                <input class="form-control" id="payable_pension" name="payable_pension" type="number" placeholder="Payable Pension Amount" readonly>
                                <span class="text-danger invalid payable_pension_err"></span>
                            </div>

                            <div class="col-md-4">
                                <input type="hidden" id="main_da" name="main_da" >
                                <label class="col-form-label" for="da">DA<span class="text-danger">*</span></label>
                                <input class="form-control" id="da" name="da" type="number" placeholder="Dearness Allowance" readonly>
                                <span class="text-danger invalid da_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="da_applicabe">Is DA Applicable ? (Yes/No)<span class="text-danger">*</span></label>
                                <select class="form-select da_applicabe" id="da_applicabe" name="da_applicabe">
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                                <span class="text-danger invalid da_applicabe_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="sell_computation">Sell Computation (Yes/No)<span class="text-danger">*</span></label>
                                <select class="form-select sell_computation" id="sell_computation" name="sell_computation">
                                    <option value="">Select Type</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                                <span class="text-danger invalid sell_computation_err"></span>
                            </div>

                            <div class="col-md-4" id="deduct_amt_div" style="display: none;">
                                <label class="col-form-label" for="deduct_amt">Enter Amount For Deduct<span class="text-danger">*</span></label>
                                <input class="form-control" id="deduct_amt" name="deduct_amt" type="number" placeholder="Enter Amount For Deduct">
                                <span class="text-danger invalid deduct_amt_err"></span>
                            </div>

                            <div class="col-md-4" id="sell_date_div" style="display: none;" >
                                <label class="col-form-label" for="sell_date">Sell Date<span class="text-danger"></span></label>
                                <input class="form-control" id="sell_date" name="sell_date" type="date" placeholder="Enter Sell Date">
                                <span class="text-danger invalid sell_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</x-admin.layout>

<script src="{{ asset('admin/js/pension-calculation.js') }}" defer></script>

<!-- Add -->
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('pension.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('pension.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>
</script>
