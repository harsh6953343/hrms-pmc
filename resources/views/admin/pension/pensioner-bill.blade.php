
<x-admin.layout>
    <x-slot name="title">Pensioner Bill</x-slot>
    <x-slot name="heading">Pensioner Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('pensioner-bill-bank-pdf') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data" method="GET">
                @csrf

                    <div class="card-body">
                        <div class="mb-3 row">


                            <div class="col-md-3">
                                <label class="col-form-label" for="bank">Banks<span class="text-danger">*</span></label>
                                <select class="form-select" id="bank" name="bank">
                                    <option value="">Select Bank</option>
                                    <option value="all" {{ (empty($banks))?'Selected':'' }}>All</option>
                                    @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                    @endforeach
                                </select>
                                @error('bank')
                                <span class="text-danger invalid bank_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <span class="text-danger invalid month_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly>
                                @error('from_date')
                                <span class="text-danger invalid from_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly>
                                @error('to_date')
                                <span class="text-danger invalid to_date_err">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <a href="{{ route('pensioner-bill.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

            </div>
        </div>
    </div>



</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>
