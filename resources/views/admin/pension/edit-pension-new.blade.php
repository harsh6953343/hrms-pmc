<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Pension</h4>
                </header>
                <form class="theme-form" name="editForm" id="editForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="edit_model_id" id="edit_model_id" value="{{ $pensionNew->id  }}">
                    <input type="hidden" id="DA_rate" value="{{ $da_rate->amount }}">
                    <div class="card-body">

                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="pension_id">Pension Unique ID<span class="text-danger">*</span></label>
                                <input class="form-control" id="pension_id" name="pension_id" type="text" value="{{ $pensionNew->pension_id  }}" readonly>
                                <span class="text-danger invalid pension_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_fname">Employee First Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_fname" name="emp_fname" type="text" placeholder="Enter First Name" value="{{ $pensionNew->emp_fname }}">
                                <span class="text-danger invalid emp_fname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_fname_marathi">Employee First Name In Marathi<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_fname_marathi" name="emp_fname_marathi" type="text" placeholder="Employee First Name In Marathi" value="{{ $pensionNew->emp_fname_marathi }}">
                                <span class="text-danger invalid emp_fname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_mname">Employee Middle Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_mname" name="emp_mname" type="text" placeholder="Enter First Name" value="{{ $pensionNew->emp_mname }}">
                                <span class="text-danger invalid emp_mname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_mname_marathi">Employee Middle Name In Marathi<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_mname_marathi" name="emp_mname_marathi" type="text" placeholder="Employee Middle Name In Marathi" value="{{ $pensionNew->emp_mname_marathi }}">
                                <span class="text-danger invalid emp_mname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_lname">Employee Last Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_lname" name="emp_lname" type="text" placeholder="Enter Last Name" value="{{ $pensionNew->emp_lname }}">
                                <span class="text-danger invalid emp_lname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_lname_marathi">Employee Last Name In Marathi<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_lname_marathi" name="emp_lname_marathi" type="text" placeholder="Employee Last Name In Marathi" value="{{ $pensionNew->emp_lname_marathi }}">
                                <span class="text-danger invalid emp_lname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward_id">Group<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward_name" name="ward_name" type="text" value="{{ $pensionNew->group->name ?? ''}}" placeholder="Group" readonly>
                                <input id="ward_id" name="ward_id" type="hidden" readonly>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input type="hidden" name="designation_id" id="designation_id">
                                <input class="form-control" id="designation" name="designation" type="text" value="{{ $pensionNew->designation->name ?? '' }}" placeholder="Enter Designation">
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_scale_id">Select Grade<span class="text-danger"></span></label>
                                <select class="form-control pay_scale_id" id="pay_scale_id" name="pay_scale_id">
                                    <option value="">Select Grade</option>
                                    @foreach ($payScales as $payScale)
                                        <option value="{{ $payScale->id  }}" {{ ($payScale->id == $pensionNew->pay_scale_id) ? "Selected":"" }} >{{ $payScale->grade_pay_name." "."7th Pay Comission /". $payScale->pay_band_scale." /".$payScale->amount  }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid pay_scale_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="last_basic">Last Basic Salary <span class="text-danger"></span></label>
                                <input class="form-control" id="last_basic" name="last_basic" type="number" placeholder="Enter Last Basic" value="{{ $pensionNew->last_basic }}">
                                <span class="text-danger invalid last_basic_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="last_grade_pay">Last Grade Pay <span class="text-danger"></span></label>
                                <input class="form-control" id="last_grade_pay" name="last_grade_pay" type="text" placeholder="Enter Grade Pay" readonly value="{{ $pensionNew->last_grade_pay }}">
                                <span class="text-danger invalid last_grade_pay_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dob">DOB<span class="text-danger"></span></label>
                                <input class="form-control" id="dob" name="dob" type="date" placeholder="Enter Date of Birth" value="{{ $pensionNew->dob }}">
                                <span class="text-danger invalid dob_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="age">Age<span class="text-danger"></span></label>
                                <input class="form-control" id="age" name="age" type="text" placeholder="Enter Age" readonly value="{{ $pensionNew->age }}">
                                <span class="text-danger invalid age_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="doj">Joining Date<span class="text-danger"></span></label>
                                <input class="form-control" id="doj" name="doj" type="date" placeholder="Enter Joining Date" value="{{ $pensionNew->doj }}">
                                <span class="text-danger invalid doj_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dor">Retirement Date<span class="text-danger"></span></label>
                                <input class="form-control" id="dor" name="dor" type="date" placeholder="Enter Retirement Date" value="{{ $pensionNew->dor }}">
                                <span class="text-danger invalid dor_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dod">Date of Death (If Applicable)<span class="text-danger"></span></label>
                                <input class="form-control" id="dod" name="dod" type="date" placeholder="Enter Date of Death (If Applicable)" value="{{ $pensionNew->dod }}">
                                <span class="text-danger invalid dod_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="percent_share">Percent Share<span class="text-danger"></span></label>
                                <input class="form-control" id="percent_share" name="percent_share" type="text" placeholder="Enter Percent Share" value="{{ $pensionNew->percent_share }}">
                                <span class="text-danger invalid percent_share_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_title">Pensioner Title<span class="text-danger"></span></label>
                                <select class="form-select" id="pensioner_title" name="pensioner_title">
                                    <option value="">Select Pensioner Title </option>
                                    <option value="Mrs" {{($pensionNew->pensioner_title == "Mrs")? 'Selected':''  }} >Mrs</option>
                                    <option value="Mr"  {{($pensionNew->pensioner_title == "Mr")? 'Selected':''  }} >Mr</option>
                                    <option value="Ms"  {{($pensionNew->pensioner_title == "Ms")? 'Selected':''  }} >Ms</option>
                                </select>
                                <span class="text-danger invalid pensioner_title_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_type">Pensioner Type<span class="text-danger"></span></label>
                                <select class="form-select" id="pensioner_type" name="pensioner_type">
                                    <option value="">Select Pensioner Type </option>
                                    <option value="regular" {{($pensionNew->pensioner_type == "regular")? 'Selected':''  }} >Regular Retirement</option>
                                    <option value="family"  {{($pensionNew->pensioner_type == "family")? 'Selected':''  }} >Family Pension</option>
                                    <option value="inservice"  {{($pensionNew->pensioner_type == "inservice")? 'Selected':''  }} >Inservice</option>
                                    <option value="vol_ret"  {{($pensionNew->pensioner_type == "vol_ret")? 'Selected':''  }} >Voluntary Retirement</option>
                                    <option value="ret_med"  {{($pensionNew->pensioner_type == "ret_med")? 'Selected':''  }} >Retirement Due to Medical Reasons</option>
                                </select>
                                <span class="text-danger invalid pensioner_type_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_fname">Pensioner First Name<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_fname" name="pensioner_fname" type="text" placeholder="Enter Pensioner First Name" value="{{ $pensionNew->pensioner_fname }}">
                                <span class="text-danger invalid pensioner_fname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_fname_marathi">Pensioner First Name In Marathi<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_fname_marathi" name="pensioner_fname_marathi" type="text" placeholder="Employee First Name In Marathi" value="{{ $pensionNew->pensioner_fname_marathi }}">
                                <span class="text-danger invalid pensioner_fname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_mname">Pensioner Middle Name<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_mname" name="pensioner_mname" type="text" placeholder="Enter Pensioner Middle Name" value="{{ $pensionNew->pensioner_mname }}">
                                    <span class="text-danger invalid pensioner_mname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_mname_marathi">Pensioner Middle Name In Marathi<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_mname_marathi" name="pensioner_mname_marathi" type="text" placeholder="Pensioner Middle Name In Marathi" value="{{ $pensionNew->pensioner_mname_marathi }}">
                                <span class="text-danger invalid pensioner_mname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_lname">Pensioner Last Name<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_lname" name="pensioner_lname" type="text" placeholder="Enter Pensioner Last Name" value="{{ $pensionNew->pensioner_lname }}">
                                <span class="text-danger invalid pensioner_lname_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_lname_marathi">Pensioner Last Name In Marathi<span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_lname_marathi" name="pensioner_lname_marathi" type="text" placeholder="Employee Last Name In Marathi" value="{{ $pensionNew->pensioner_lname_marathi }}">
                                <span class="text-danger invalid pensioner_lname_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="mobile">Mobile No.<span class="text-danger"></span></label>
                                <input class="form-control" id="mobile" name="mobile" type="text" placeholder="Employee Mobile No." value="{{ $pensionNew->mobile }}">
                                <span class="text-danger invalid mobile_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="aadhar">Aadhaar No.<span class="text-danger"></span></label>
                                <input class="form-control" id="aadhar" name="aadhar" type="text" placeholder="Aadhaar No." value="{{ $pensionNew->aadhar }}">
                                <span class="text-danger invalid aadhar_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="address">Address<span class="text-danger"></span></label>
                                <textarea id="address" name="address" class="form-control">{{ $pensionNew->aadhar  }}</textarea>
                                <span class="text-danger invalid address_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="address_marathi">Address In Marathi<span class="text-danger"></span></label>
                                <textarea id="address_marathi" name="address_marathi" class="form-control">{{ $pensionNew->address_marathi  }}</textarea>
                                <span class="text-danger invalid address_marathi_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="city">City<span class="text-danger"></span></label>
                                <input class="form-control" id="city" name="city" type="text" placeholder="City" value="{{ $pensionNew->city  }}">
                                <span class="text-danger invalid city_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="state">State<span class="text-danger"></span></label>
                                <input class="form-control" id="state" name="state" type="text" placeholder="State" value="{{ $pensionNew->state  }}">
                                <span class="text-danger invalid state_err"></span>
                            </div>

                            <div class="col-md-4">

                                <label class="col-form-label" for="pension_bank_id">Select Bank<span class="text-danger"></span></label>
                                <select class="form-select pension_bank_id" id="pension_bank_id" name="pension_bank_id">

                                    <option value="">Select Bank</option>
                                    @foreach ($pension_banks as $pension_bank)
                                        <option value="{{ $pension_bank->id }}" {{ ($pensionNew->pension_bank_id == $pension_bank->id)? 'Selected':'' }}  >{{ $pension_bank->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid pension_bank_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="branch">Branch Name<span class="text-danger"></span></label>
                                <input class="form-control" id="branch" name="branch" type="text" placeholder="Enter Branch Name" value="{{ $pensionNew->branch  }}">
                                <span class="text-danger invalid branch_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="account_no">Account No.<span class="text-danger"></span></label>
                                <input class="form-control" id="account_no" name="account_no" type="text" placeholder="Enter Account No." value="{{ $pensionNew->account_no  }}">
                                <span class="text-danger invalid account_no_err"></span>
                            </div>

                            <div class="col-md-4">

                                <label class="col-form-label" for="ifsc_code">IFSC Code</label>
                                <input class="form-control" id="ifsc_code" name="ifsc_code" type="text" placeholder="Enter IFSC Code" value="{{ $pensionNew->ifsc_code  }}">
                                <span class="text-danger invalid ifsc_code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensionser_status">Pensioner Status</label>

                                <select class="form-select pensionser_status" id="pensionser_status" name="pensionser_status">

                                    <option value="">Select Pensioner Status</option>
                                    <option value="1" {{ ($pensionNew->pensionser_status == '1')? 'Selected':''  }}>Active</option>
                                    <option value="5" {{ ($pensionNew->pensionser_status == '5')? 'Selected':''  }}>Hold</option>
                                    <option value="2" {{ ($pensionNew->pensionser_status == '2')? 'Selected':''  }} >Stop</option>
                                    <option value="6" {{ ($pensionNew->pensionser_status == '6')? 'Selected':''  }}>Hold Release</option>
                                    <option value="4" {{ ($pensionNew->pensionser_status == '4')? 'Selected':''  }}>Deceased</option>
                                    <option value="3" {{ ($pensionNew->pensionser_status == '3')? 'Selected':''  }}>Backdated</option>

                                </select>
                                <span class="text-danger invalid pension_pensionser_status_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="status_change_date">Status Change Date <span class="text-danger"></span></label>
                                <input class="form-control" id="status_change_date" name="status_change_date" type="date" placeholder="Enter Status Change Date" value="{{ $pensionNew->status_change_date  }}">
                                <span class="text-danger invalid status_change_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="remarks">Remark <span class="text-danger"></span></label>
                                <input class="form-control" id="remarks" name="remarks" type="text" placeholder="Enter Remark" value="{{ $pensionNew->remarks  }}">
                                <span class="text-danger invalid remarks_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pension_start_date">Pension Start Date <span class="text-danger"></span></label>
                                <input class="form-control" id="pension_start_date" name="pension_start_date" type="date" placeholder="Enter Pension Start Date" value="{{ $pensionNew->pension_start_date  }}">
                                <span class="text-danger invalid pension_start_date_err"></span>
                            </div>

                            <div class="col-md-4">

                                <label class="col-form-label" for="da_applicabe">DA Applicable<span class="text-danger"></span></label>
                                <select class="form-select da_applicabe" id="da_applicabe" name="da_applicabe">

                                    <option value="">DA Applicable</option>
                                    <option value="1" {{ ($pensionNew->da_applicabe == 1)?'Selected':'';   }}  >Yes</option>
                                    <option value="2" {{ ($pensionNew->da_applicabe == 2)?'Selected':'';   }}  >No</option>
                                </select>
                                <span class="text-danger invalid pension_da_applicabe_err"></span>
                            </div>

                            <div class="col-md-4">

                                <label class="col-form-label" for="pay_commission">Pay Comission<span class="text-danger"></span></label>
                                <select class="form-select pay_commission" id="pay_commission" name="pay_commission">

                                    <option value="">Pay Comission</option>
                                    <option value="6" {{ ($pensionNew->pay_commission == 6)?'Selected':'';   }} >6th</option>
                                    <option value="7" {{ ($pensionNew->pay_commission == 7)?'Selected':'';   }} >7th</option>
                                </select>
                                <span class="text-danger invalid pension_pay_commission_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pension_end_date">Pension End Date <span class="text-danger"></span></label>
                                <input class="form-control" id="pension_end_date" name="pension_end_date" type="date" placeholder="Enter Pension End Date" value="{{ $pensionNew->pension_end_date }}">
                                <span class="text-danger invalid pension_end_date_err"></span>
                            </div>

                        </div>

                        <hr>
                        <h3>Add Pension Details</h3>

                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="basic">Basic Amount <span class="text-danger"></span></label>
                                <input class="form-control" id="basic" name="basic" type="number" placeholder="Enter Basic" value="{{ $pensionNew->basic }}">
                                <span class="text-danger invalid basic_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="basic_date">Basic Date <span class="text-danger"></span></label>
                                <input class="form-control" id="basic_date" name="basic_date" type="date" placeholder="Enter Basic Date" value="{{ $pensionNew->basic_date }}">
                                <span class="text-danger invalid basic_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="after_basic">Basic Amount (10 Years) <span class="text-danger"></span></label>
                                <input class="form-control" id="after_basic" name="after_basic" type="number" placeholder="Enter Basic" value="{{ $pensionNew->after_basic }}">
                                <span class="text-danger invalid after_basic_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="after_basic_date">Basic Date(10 Years) <span class="text-danger"></span></label>
                                <input class="form-control" id="after_basic_date" name="after_basic_date" type="date" placeholder="Enter Basic Date" value="{{ $pensionNew->after_basic_date }}">
                                <span class="text-danger invalid after_basic_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <input type="hidden" id="main_da" name="main_da" value="{{ $pensionNew->dearness_relief }}">
                                <label class="col-form-label" for="dearness_relief">DEARNESS RELIEF <span class="text-danger"></span></label>
                                <input class="form-control" id="dearness_relief" name="dearness_relief" type="number" placeholder="Enter DEARNESS RELIEF" value="{{ $pensionNew->dearness_relief }}">
                                <span class="text-danger invalid dearness_relief_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dearness_relief_date">DEARNESS RELIEF Date <span class="text-danger"></span></label>
                                <input class="form-control" id="dearness_relief_date" name="dearness_relief_date" type="date" placeholder="Enter DEARNESS RELIEF Date" value="{{ $pensionNew->dearness_relief_date }}">
                                <span class="text-danger invalid dearness_relief_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="relief_fund">RELIEF FUND <span class="text-danger"></span></label>
                                <input class="form-control" id="relief_fund" name="relief_fund" type="number" placeholder="Enter RELIEF FUND" value="{{ $pensionNew->relief_fund }}">
                                <span class="text-danger invalid relief_fund_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="relief_fund_date">RELIEF FUND Date <span class="text-danger"></span></label>
                                <input class="form-control" id="relief_fund_date" name="relief_fund_date" type="date" placeholder="Enter RELIEF FUND" value="{{ $pensionNew->relief_fund_date }}">
                                <span class="text-danger invalid relief_fund_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_arrears">PENSION ARREARS <span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_arrears" name="pensioner_arrears" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->pensioner_arrears }}">
                                <span class="text-danger invalid pensioner_arrears_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_arrears_date">PENSION ARREARS Date <span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_arrears_date" name="pensioner_arrears_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->pensioner_arrears_date }}">
                                <span class="text-danger invalid pensioner_arrears_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pensioner_arrears_to_date">PENSION ARREARS To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="pensioner_arrears_to_date" name="pensioner_arrears_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid pensioner_arrears_to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="special_allowance">SPECIAL ALLOWANCE <span class="text-danger"></span></label>
                                <input class="form-control" id="special_allowance" name="special_allowance" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->special_allowance }}">
                                <span class="text-danger invalid special_allowance_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="special_allowance_date">SPECIAL ALLOWANCE Date <span class="text-danger"></span></label>
                                <input class="form-control" id="special_allowance_date" name="special_allowance_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->special_allowance_date }}">
                                <span class="text-danger invalid special_allowance_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="special_allowance_to_date">SPECIAL ALLOWANCE To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="special_allowance_to_date" name="special_allowance_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid special_allowance_to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_deduction">MISCELLANEOUS DEDUCTION <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_deduction" name="miscellaneous_deduction" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->miscellaneous_deduction }}">
                                <span class="text-danger invalid miscellaneous_deduction_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_deduction_date">MISCELLANEOUS DEDUCTION Date <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_deduction_date" name="miscellaneous_deduction_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->miscellaneous_deduction_date }}">
                                <span class="text-danger invalid miscellaneous_deduction_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_deduction_to_date">MISCELLANEOUS DEDUCTION To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_deduction_to_date" name="miscellaneous_deduction_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid miscellaneous_deduction_to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_arrears">MISCELLANEOUS ARREARS <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_arrears" name="miscellaneous_arrears" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->miscellaneous_arrears }}">
                                <span class="text-danger invalid miscellaneous_arrears_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_arrears_date">MISCELLANEOUS ARREARS Date <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_arrears_date" name="miscellaneous_arrears_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->miscellaneous_arrears_date }}">
                                <span class="text-danger invalid miscellaneous_arrears_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="miscellaneous_arrears_to_date">MISCELLANEOUS ARREARS To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="miscellaneous_arrears_to_date" name="miscellaneous_arrears_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid miscellaneous_arrears_to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="recovery">RECOVERY <span class="text-danger"></span></label>
                                <input class="form-control" id="recovery" name="recovery" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->recovery }}">
                                <span class="text-danger invalid recovery_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="recovery_date">RECOVERY Date <span class="text-danger"></span></label>
                                <input class="form-control" id="recovery_date" name="recovery_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->recovery_date }}">
                                <span class="text-danger invalid recovery_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="recovery_to_date">RECOVERY To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="recovery_to_date" name="recovery_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid recovery_to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="commutation">COMMUTATION <span class="text-danger"></span></label>
                                <input class="form-control" id="commutation" name="commutation" type="number" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->commutation }}">
                                <span class="text-danger invalid commutation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="commutation_date">COMMUTATION Date <span class="text-danger"></span></label>
                                <input class="form-control" id="commutation_date" name="commutation_date" type="date" placeholder="Enter PENSION ARREARS" value="{{ $pensionNew->commutation_date }}">
                                <span class="text-danger invalid commutation_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="commutation_to_date">COMMUTATION To Date <span class="text-danger"></span></label>
                                <input class="form-control" id="commutation_to_date" name="commutation_to_date" type="date" placeholder="Enter PENSION ARREARS">
                                <span class="text-danger invalid commutation_to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</x-admin.layout>
<script>
$("#pay_scale_id").change(function(){
    var pay_scale_id = $('#pay_scale_id').val();

    if(pay_scale_id != '') {
        var url = "{{ route('fetch-payscale-details', ':pay_scale_id') }}";

        $.ajax({
            url: url.replace(':pay_scale_id', pay_scale_id),
            type: 'GET',
            data: {
                '_method': "GET",
                '_token': "{{ csrf_token() }}"
            },
            success: function(data) {
                if (!data.error && !data.error2) {
                    if (data.result === 1) {
                        $('#last_basic').val(data.pay_scale_details.amount);
                        $('#last_grade_pay').val(data.pay_scale_details.grade_amp);

                    } else if (data.result === 0) {
                        $('#last_basic').val('');
                        $('#last_grade_pay').val('');

                        alert("Pay Scale details not found!");
                    } else {
                        alert("Unexpected result from the server");
                    }
                }
                else {
                    alert("Error in data or unexpected response");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    } else {
        alert('Please select Pay Scale');
        $('#last_basic').val('');
        $('#last_grade_pay').val('');
    }
});

</script>

<!-- Add -->

<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('pension-new.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('pension-new.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>


<script>
    $("#dob").on("change", function() {
        var dob = new Date($(this).val());
        var today = new Date();
        var age = today.getFullYear() - dob.getFullYear();
        var m = today.getMonth() - dob.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < dob.getDate())) {
            age--;
        }
        $("#age").val(age);
    });
</script>

<script>
    function calculateValues() {
    var six_pay_basic = parseFloat($("#basic").val());
    var Da_allowns = parseFloat($("#DA_rate").val());

    // For 6 Pay
        if(six_pay_basic != '' && six_pay_basic != 0)
        {
            var basic_amt = six_pay_basic;
            var da = basic_amt * Da_allowns / 100;
            $('#dearness_relief').val(Math.round(da));
            $('#main_da').val(da);

        } else {
            alert("Please Enter Valid Amount");
        }
    }

    $(document).ready(function() {
        $("#basic, #DA_rate").on("change keyup", calculateValues);
    });

    $("#da_applicabe").on("change", function() {
        if (this.value == 1) {
            var da = $('#main_da').val();
            $('#dearness_relief').val(Math.round(da));
        }
        if (this.value == 2) {
            $('#dearness_relief').val(0);
        }
    })
</script>
