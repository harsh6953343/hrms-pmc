<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Supplementary PDF</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .page-break {
                page-break-after: always;
            }
            body {
                font-family: 'freeserif', 'normal';
            }
            .tableborder, .tableborder th, .tableborder td {
                border: 1px solid rgb(105, 102, 102);
                border-collapse: collapse;
            }
            #pdftrheading th {
                font-size: 11px;
            }
            tbody td{
                padding-right: 3px;
            }
            .tableborder tbody td{
                font-size: 13px;
            }
        </style>
    </head>
    <body>
        @php
        $monthMrName = ['1' => 'जानेवारी', '2' => 'फेब्रुवारी', '3' => 'मार्च', '4' => 'एप्रिल', '5' => 'मे', '6' => 'जून', '7' => 'जुलै', '8' => 'ऑगस्ट', '9' => 'सप्टेंबर', '10' => 'ऑक्टोबर', '11' => 'नोहेंबर', '12' => 'डिसेंबर'];
        $month = request()->month;
        $year = date('Y', strtotime(request()->from_date));
        @endphp
        <section style="border: 1px solid; height:850px; padding:0px 20px;">
            <table style="width: 100%">
                <tr>
                    <td>
                        @php
                        $logoData = file_get_contents(public_path('logo.png'));
                        $base64Logo = base64_encode($logoData);
                        @endphp
                        <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="100" width="120">
                    </td>
                    <td>
                        <p style="font-size: 55px;font-weight:900;padding-left:16%;">पनवेल महानगरपालिका</p>
                    </td>
                </tr>
            </table>

            <br><br><br><br><br>
            @if(request()->pensioner_type == "family")
            <table style="width:100%;">
                <tr align="center">
                    <td> 
                        <span style="font-weight: 900; font-size:35px;">कुटूंब निवृत्तिवेतन देयक</span><br><br>
                        <span style="font-weight: 900; font-size:35px;">माहे - {{ $monthMrName[$month] ?? '-' }}-{{ convertToMarathiNumber($year) }}</span>
                    </td>
                </tr>
            </table>            
            @else
            <table style="width:100%;">
                <tr align="center">
                    <td> 
                        <span style="font-weight: 900; font-size:35px;">नियमित निवृत्ति वेतन देयक</span><br><br>
                        <span style="font-weight: 900; font-size:35px;">माहे - {{ $monthMrName[$month] ?? '-' }}-{{ convertToMarathiNumber($year) }}</span>
                    </td>
                </tr>
            </table>
            @endif
        </section>
        <div class="page-break"></div>

        <section>
            <table style="width: 100%;" class="tableborder">
                <thead>
                    <tr>
                        <th align="center" colspan="14" style="padding:1%; font-size:26px;">पनवेल महानगरपालिका</th>
                    </tr>
                    <tr>
                        <th align="left" colspan="14" style="padding:6px;">
                            @if(request()->pensioner_type == "family")
                            कुटूंब निवृत्तिवेतन देयक - {{ $monthMrName[$month] ?? '-' }}-{{ $year }}
                            @else
                            नियमित निवृत्ति वेतन देयक माहे - {{ $monthMrName[$month] ?? '-' }}-{{ $year }}
                            @endif
                        </th>
                    </tr>
                    @if(request()->pensioner_type == "family")
                    <tr style="background: #609ce0">
                        <th style="padding:3px;">अनु. क्र.</th>
                        <th style="padding:3px;">पेन्शनर ओळख</th>
                        <th style="padding:3px;">कर्मचाऱ्यांचे नाव</th>
                        <th style="padding:3px;">निवृत्तिवेतन धारकाचे नाव</th>
                        <th style="padding:3px;">बँक खाते</th>
                        <th style="padding:3px;">सेवा निवृत्ती दिनांक</th>
                        <th style="padding:3px;">मूळ निवृत्तिवेतन</th>
                        <th style="padding:3px;">महागाई भत्ता</th>
                        <th style="padding:3px;">निवृत्तिवेतन थकबाकी</th>
                        <th style="padding:3px;">एकूण निवृत्तिवेतन</th>
                        <th style="padding:3px;">वसुली</th>
                        <th style="padding:3px;">एकूण कपात</th>
                        <th style="padding:3px;">निव्वळ निवृत्तिवेतन</th>
                    </tr>
                    @else
                    <tr style="background: #609ce0">
                        <th style="padding:3px;">अनु. क्र.</th>
                        <th style="padding:3px;">पेन्शनर ओळख</th>
                        <th style="padding:3px;">कर्मचाऱ्यांचे नाव</th>
                        <th style="padding:3px;">बैंक खाते</th>
                        <th style="padding:3px;">सेवा निवृत्ती दिनांक</th>
                        <th style="padding:3px;">मूळ निवृत्तिवेतन</th>
                        <th style="padding:3px;">महागाई भत्ता</th>
                        <th style="padding:3px;">निवृत्तिवेतन थकबाकी</th>
                        <th style="padding:3px;">एकूण निवृत्तिवेतन</th>
                        <th style="padding:3px;">मागील अंशराशीकरण</th>
                        <th style="padding:3px;">अंशराशीकरण</th>
                        <th style="padding:3px;">वसुली</th>
                        <th style="padding:3px;">एकूण कपात</th>
                        <th style="padding:3px;">निव्वळ निवृत्तिवेतन</th>
                    </tr>
                    @endif
                </thead>
                <tbody style="display: table-row-group;">
                    @php
                    $basic = 0;
                    $dearnessRelief = 0;
                    $pensionArrears = 0;
                    $totalEarning = 0;
                    $miscellaneousDeduction = 0;
                    $commutation = 0;
                    $recovery = 0;
                    $totalDeduction = 0;
                    $netSalary = 0;
                    @endphp
                    @foreach($freeze_pension_datas as $pension)
                    @php
                        $basic += $pension->basic;
                        $dearnessRelief += $pension->dearness_relief;
                        $pensionArrears += $pension->pension_arrears;
                        $totalEarning += $pension->total_earning;
                        $miscellaneousDeduction += $pension->miscellaneous_deduction;
                        $commutation += $pension->commutation;
                        $recovery += $pension->recovery;
                        $totalDeduction += $pension->total_deduction;
                        $netSalary += $pension->net_salary;
                    @endphp
                    @if(request()->pensioner_type == "family")
                        <tr style="page-break-inside: avoid;">
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($loop->iteration) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->pension_id) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ ($pension->pensionNew && ($pension->pensionNew?->emp_fname_marathi || $pension->pensionNew?->emp_mname_marathi || $pension->pensionNew?->emp_lname_marathi)) ? $pension->pensionNew?->emp_fname_marathi .' '. $pension->pensionNew?->emp_mname_marathi .' '. $pension->pensionNew?->emp_lname_marathi : $pension->employee_name }}</td>
                            <td style="padding: 5px;font-size:15px;">
                                {{ ($pension->pensionNew && ($pension->pensionNew?->pensioner_fname_marathi || $pension->pensionNew?->pensioner_mname_marathi || $pension->pensionNew?->pensioner_lname_marathi)) ? $pension->pensionNew?->pensioner_fname_marathi .' '. $pension->pensionNew?->pensioner_mname_marathi .' '. $pension->pensionNew?->pensioner_lname_marathi : $pension->pensioner_name }}
                            </td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->account_no) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ ($pension->employee?->retirement_date) ? convertToMarathiNumber(date('d-m-Y', strtotime($pension->employee?->retirement_date))) : '' }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->basic) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->dearness_relief) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->pension_arrears) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->total_earning) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->recovery) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->total_deduction) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->net_salary) }}</td>
                        </tr>
                    @else
                        <tr style="page-break-inside: avoid;">
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($loop->iteration) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->pension_id) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->employee_name) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->account_no) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ ($pension->employee?->retirement_date) ? convertToMarathiNumber(date('d-m-Y', strtotime($pension->employee?->retirement_date))) : '' }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->basic) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->dearness_relief) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->pension_arrears) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->total_earning) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->miscellaneous_deduction) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->commutation) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->recovery) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->total_deduction) }}</td>
                            <td style="padding: 5px;font-size:15px;">{{ convertToMarathiNumber($pension->net_salary) }}</td>
                        </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </section>

        <div class="page-break"></div>
        <section>
            <table style="width: 100%" class="tableborder">
                <thead style="font-size: 18px">
                    @if(request()->pensioner_type == "family")
                    <tr>
                        <th style="padding:3px;">अनु. क्र.</th>
                        <th style="padding:3px;">पेन्शनर ओळख</th>
                        <th style="padding:3px;">कर्मचाऱ्यांचे नाव</th>
                        <th style="padding:3px;">निवृत्तिवेतन धारकाचे नाव</th>
                        <th style="padding:3px;">बँक खाते</th>
                        <th style="padding:3px;">सेवा निवृत्ती दिनांक</th>
                        <th style="padding:3px;">मूळ निवृत्तिवेतन</th>
                        <th style="padding:3px;">महागाई भत्ता</th>
                        <th style="padding:3px;">निवृत्तिवेतन थकबाकी</th>
                        <th style="padding:3px;">एकूण निवृत्तिवेतन</th>
                        <th style="padding:3px;">वसुली</th>
                        <th style="padding:3px;">एकूण कपात</th>
                        <th style="padding:3px;">निव्वळ निवृत्तिवेतन</th>
                    </tr>
                    @else
                    <tr>
                        <th style="padding:3px;">अनु. क्र.</th>
                        <th style="padding:3px;">पेन्शनर ओळख</th>
                        <th style="padding:3px;">कर्मचाऱ्यांचे नाव</th>
                        <th style="padding:3px;">बैंक खाते</th>
                        <th style="padding:3px;">सेवा निवृत्ती दिनांक</th>
                        <th style="padding:3px;">मूळ निवृत्तिवेतन</th>
                        <th style="padding:3px;">महागाई भत्ता</th>
                        <th style="padding:3px;">निवृत्तिवेतन थकबाकी</th>
                        <th style="padding:3px;">एकूण निवृत्तिवेतन</th>
                        <th style="padding:3px;">मागील अंशराशीकरण</th>
                        <th style="padding:3px;">अंशराशीकरण</th>
                        <th style="padding:3px;">वसुली</th>
                        <th style="padding:3px;">एकूण कपात</th>
                        <th style="padding:3px;">निव्वळ निवृत्तिवेतन</th>
                    </tr>
                    @endif
                </thead>
                <tbody>
                    
                    @if(request()->pensioner_type == "family")
                    <tr style="font-size: 25px">
                        <td style="padding:3px;font-size:16px;">एकूण</td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($basic) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($dearnessRelief) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($pensionArrears) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($totalEarning) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($recovery) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($totalDeduction) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($netSalary) }}</td>
                    </tr>
                    @else
                    <tr>
                        <td style="padding:3px;font-size:16px;">एकूण</td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;"></td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($basic) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($dearnessRelief) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($pensionArrears) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($totalEarning) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($miscellaneousDeduction) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($commutation) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($recovery) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($totalDeduction) }}</td>
                        <td style="padding:3px;font-size:16px;">{{ convertToMarathiNumber($netSalary) }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div>
                <table style="width: 100%; align:center">
                    <thead>
                        <tr>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th style="font-size:20px; word-spacing: 2px">आस्थापना प्रमुख <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">सहा. आयुक्त (आस्थापना) <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">उपआयुक्त (मुख्यालय) <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">मुख्यलेखाअधिकारी <br> पनवेल महानगरपालिका</th>
                            <th style="font-size:20px; word-spacing: 2px">उपआयुक्त (मुख्यालय) <br> पनवेल महानगरपालिका</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                </table>
                 
            </div>
        </section>

    </body>
</html>


