<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('pension.create')
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <a href="{{ route('pension-new.create') }}" id="addToTable1" class="btn btn-primary">Add New<i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Pension Unique ID</th>
                                    <th>Pensioner Name</th>
                                    <th>Employee Name</th>
                                    <th>Pensioner Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('pension-new.list') }}",
                        method: "POST",
                        data: function(d) {
                            d._token = "{{ csrf_token() }}";
                        }
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pension_id',
                            name: 'pension_id',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pensioner_full_name',
                            name: 'pensioner_full_name',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'employee_full_name',
                            name: 'employee_full_name',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pensioner_type',
                            name: 'pensioner_type',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pension_status',
                            name: 'pension_status',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            render: function(data, type, row){
                                let editUrl = "{{ route('pension-new.edit', ':id') }}";
                                @can('pension.edit')
                                let html = `<a href="${editUrl.replace(':id', data)}" class="btn btn-warning px-2 py-1" title="Edit">
                                        Edit
                                    </a>`;
                                @endcan

                                @can('pension.delete')
                                html += `<button class="btn btn-danger rem-element px-2 py-1" title="Delete Pension" data-id="${data}">Delete </button>`;
                                @endcan

                                return html;
                            },
                        },
                    ]
                });
            })
        </script>
    @endpush

</x-admin.layout>

<!-- Delete -->
<script>
    $("#dataTable").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Pension?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('pension-new.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

</script>
