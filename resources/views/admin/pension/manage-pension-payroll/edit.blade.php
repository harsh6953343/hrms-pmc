<x-admin.layout>
    <x-slot name="title">Pensioner Details</x-slot>
    <x-slot name="heading">Pensioner Details</x-slot>

    <form method="post" action="{{ route('manage-pensioner-payroll.update', $pensionNew->id) }}">
        @method('put')
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @can('pension.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="text-center">Pensioner Details</h3>
                                <h5 class="text-center">Pensioner Id:- {{ $pensionNew->pension_id }}</h5>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-2"><b>Pensioner Name</b></div>
                                <div class="col-4"> {{ $pensionNew->pensioner_fname." ".$pensionNew->pensioner_mname." ".$pensionNew->pensioner_lname }}</div>
                                <div class="col-2"><b>Department</b></div>
                                <div class="col-4">{{ $pensionNew->department?->name }}</div>
                            </div>

                            <div class="row">
                                <div class="col-2"><b>Designation</b></div>
                                <div class="col-4">{{ $pensionNew->designation?->name }}</div>
                                <div class="col-2"><b>Joining Date</b></div>
                                <div class="col-4">{{ date('d-m-Y', strtotime($pensionNew->doj)) }}</div>
                            </div>
                            <hr>


                            <div class="row">
                                <div class="col-4">Grade</div>
                                <div class="col-2">{{ $pensionNew?->pay_scale?->grade_pay_name }}</div>
                                <div class="col-1">Level</div>
                                <div class="col-5">{{ $pensionNew?->pay_scale?->pay_band_scale }}</div>
                            </div>
                        </div>
                    </div>
                    @endcan
                    <div class="card-body">
                        <div class="mt-3">
                            <h4 class="text-center">Pensioner Payroll Details</h4>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Pay Element</th>
                                        <th>Amount</th>
                                        <th>Basis</th>
                                        <th>Based On</th>
                                        <th>Type of Pay</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Reason</th>
                                        <th>
                                            <button id="addMoreBtn" class="btn btn-primary btn-sm" type="button">Add</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyAddmore">
                                    @php
                                    $count = 0;
                                    @endphp
                                    @foreach ($pensionNew->pensionPayrolls as $payroll)
                                    <tr id="row1">
                                        <td>
                                            <input type="hidden" name="id[]" value="{{ $payroll->id }}">
                                            <input type="hidden" name="basisId[]" class="basisId" value="{{ $payroll->basis }}">
                                            <input type="hidden" name="basedOnId[]" class="basedOnId" value="{{ $payroll->based_on }}">
                                            <input type="hidden" name="typeOfPayId[]" class="typeOfPayId" value="{{ $payroll->type_of_pay }}">
                                            <select name="pension_item_master_id[]" class="form-select selectPayElement" required>
                                                <option value="">Select</option>
                                                @foreach($pensionItemMasters as $pensionItemMaster)
                                                <option @if($pensionItemMaster->id == $payroll->pension_item_master_id)selected @endif value="{{ $pensionItemMaster->id }}">{{ $pensionItemMaster->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="amount[]" value="0">
                                        </td>
                                        <td class="basicAmount">-</td>
                                        <td class="basedOnAmount">-</td>
                                        <td class="typeOfPay">-</td>
                                        <td>
                                            <input type="date" class="form-control" name="from_date[]" required>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" name="to_date[]" required>
                                        </td>
                                        <td>
                                            <textarea type="text" class="form-control" name="reason[]"></textarea>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm removeRow" data-id="1">-</button>
                                        </td>
                                    </tr>
                                    @php
                                    $count = $count + 1;
                                    @endphp
                                    @endforeach

                                    @if($count > 0)
                                    <tr id="row1">
                                        <td>
                                            <input type="hidden" name="basisId[]" class="basisId">
                                            <input type="hidden" name="basedOnId[]" class="basedOnId">
                                            <input type="hidden" name="typeOfPayId[]" class="typeOfPayId">
                                            <select name="pension_item_master_id[]" class="form-select selectPayElement" required>
                                                <option value="">Select</option>
                                                @foreach($pensionItemMasters as $pensionItemMaster)
                                                <option value="{{ $pensionItemMaster->id }}">{{ $pensionItemMaster->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="amount[]" value="0">
                                        </td>
                                        <td class="basicAmount">-</td>
                                        <td class="basedOnAmount">-</td>
                                        <td class="typeOfPay">-</td>
                                        <td>
                                            <input type="date" class="form-control" name="from_date[]" required>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" name="to_date[]" required>
                                        </td>
                                        <td>
                                            <textarea type="text" class="form-control" name="reason[]"></textarea>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm removeRow" data-id="1">-</button>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-primary">Save Change</button>
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a>
                    </div>
                </div>
            </div>
        </div>
    </form>


</x-admin.layout>

<script>
    $("body").on("change", ".selectPayElement", function(e) {
        var model_id = $(this).val();
        var url = "{{ route('pension-new.show', ':model_id') }}";
        var rowId = $(this).closest('tr').find('.removeRow').attr('data-id');
        if(model_id != ""){
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'GET',
                data: {
                    'row': rowId
                },
                success: function(data, textStatus, jqXHR) {

                    if(data.pension){
                        $('#row'+data.row).find('.basisId').val(data.pension.basis);
                        $('#row'+data.row).find('.basedOnId').val(data.pension?.based_on);
                        $('#row'+data.row).find('.typeOfPayId').val(data.pension.type_of_pay);
                        if(data.pension.basis == "1"){
                            $('#row'+data.row).find('.basicAmount').html("Percentage");
                        }else if(data.pension.basis == "2"){
                            $('#row'+data.row).find('.basicAmount').html("Amount");
                        }else if(data.pension.basis == "3"){
                            $('#row'+data.row).find('.basicAmount').html("other");
                        }

                        if(data?.pension?.pension_item_master?.name){
                            $('#row'+data.row).find('.basedOnAmount').html(data?.pension?.pension_item_master?.name);
                        }
                        else{
                            $('#row'+data.row).find('.basedOnAmount').html('-');
                        }

                        if(data.pension.type_of_pay){
                            $('#row'+data.row).find('.typeOfPay').html("Earning");
                        }else{
                            $('#row'+data.row).find('.typeOfPay').html("Deduction");
                        }
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }else{

        }
    });

    let count = 200;
    $('#addMoreBtn').on('click', function(){
        let html  = `<tr id="row${count}">
            <td>
                <input type="hidden" name="basisId[]" class="basisId">
                <input type="hidden" name="basedOnId[]" class="basedOnId">
                <input type="hidden" name="typeOfPayId[]" class="typeOfPayId">
                <select name="pension_item_master_id[]" class="form-select selectPayElement" required>
                    <option value="">Select</option>
                    @foreach($pensionItemMasters as $pensionItemMaster)
                    <option value="{{ $pensionItemMaster->id }}">{{ $pensionItemMaster->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" class="form-control" name="amount[]" value="0">
            </td>
            <td class="basicAmount">-</td>
            <td class="basedOnAmount">-</td>
            <td class="typeOfPay">-</td>
            <td>
                <input type="date" class="form-control" name="from_date[]" required>
            </td>
            <td>
                <input type="date" class="form-control" name="to_date[]" required>
            </td>
            <td>
                <textarea type="text" class="form-control" name="reason[]"></textarea>
            </td>
            <td>
                <button type="button" class="btn btn-danger btn-sm removeRow" data-id="${count}">-</button>
            </td>
        </tr>`;

        $('#tbodyAddmore').append(html);
    });

    $('body').on('click', '.removeRow', function(){
        let id = $(this).attr('data-id');

        $('#row'+id).remove();
    })
</script>
