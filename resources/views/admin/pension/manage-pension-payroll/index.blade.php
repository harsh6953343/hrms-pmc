<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('pension.create')
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <a href="{{ route('pension-new.create') }}" id="addToTable1" class="btn btn-primary">Add New<i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Pension Unique ID</th>
                                    <th>Pensioner Name</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Grade</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#dataTable').DataTable({
                    pageLength: 10,
                    info: false,
                    legth: false,
                    "autoWidth": false,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('manage-pensioner-payroll.index') }}"
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            class: 'xs-hidden',
                            orderable: false,
                            searchable: false,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pension_id',
                            name: 'pension_id',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'pensioner_full_mname',
                            name: 'pensioner_full_mname',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'employee_full_mname',
                            name: 'employee_full_mname',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'designation_name',
                            name: 'designation_name',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'department_name',
                            name: 'department_name',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'grade_pay',
                            name: 'grade_pay',
                            orderable: true,
                            searchable: true,
                            render: function(data, type, row){
                                return data;
                            },
                        },
                        {
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            render: function(data, type, row){
                                let showUrl = "{{ route('manage-pensioner-payroll.show', ':id') }}";
                                let editUrl = "{{ route('manage-pensioner-payroll.edit', ':id') }}";
                                let html = "";
                                @can('pension.delete')
                                html += `<a href="${showUrl.replace(':id', data)}" class="btn btn-primary px-2 py-1" title="Show">
                                        View
                                    </a>&nbsp;`;
                                @endcan

                                @can('pension.edit')
                                html += `<a href="${editUrl.replace(':id', data)}" class="btn btn-warning px-2 py-1" title="Edit">
                                        Edit
                                    </a>`;
                                @endcan

                                return html;
                            },
                        },
                    ]
                });
            })
        </script>
    @endpush

</x-admin.layout>

