<x-admin.layout>
    <x-slot name="title">Monthly LIC</x-slot>
    <x-slot name="heading">Monthly LIC</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_monthly_lics as $employee_monthly_lic)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_monthly_lic->from_date }}</td>
                                        <td>{{ $employee_monthly_lic->to_date }}</td>
                                        <td>
                                            @can('employee-monthly-lic.view')
                                            <a href="{{ route('employee-monthly-lic.showEmployeeLic', ['from_date' => $employee_monthly_lic->from_date, 'to_date' => $employee_monthly_lic->to_date]) }}">
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Show Lic" data-id="{{ $employee_monthly_lic->from_date }}">
                                                    <i data-feather="eye"></i>
                                                </button>
                                            </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>

