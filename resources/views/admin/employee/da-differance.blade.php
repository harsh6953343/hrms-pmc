<x-admin.layout>
    <x-slot name="title">DA Differance</x-slot>
    <x-slot name="heading">DA Differance</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add DA Differance</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" name="from_date" type="date" required>
                                <span class="text-danger invalid from_date_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" required>
                                <span class="text-danger invalid to_date_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="description">Description</label>
                                <textarea class="form-control title" id="description" name="description"></textarea>
                                <span class="text-danger invalid description_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <form action="#" method="get">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Filter DA Difference</h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="col-form-label" for="bill_no">Select Bill No</label>
                                <select required name="bill_no" class="form-select">
                                    <option value="">Select</option>
                                    @foreach($daBills as $daBill)
                                    <option value="{{ $daBill->id }}">{{ $daBill->bill_no }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 ward_div">
                                <label class="col-form-label" for="ward_id">Select Ward <span class="text-danger">*</span></label>
                                <select class="form-select ward_id" name="ward_id" required>
                                    <option value="">--Select Ward--</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-3 department_div d-none">
                                <label class="col-form-label" for="department_id">Select Department <span class="text-danger">*</span></label>
                                <select class="form-select department_id" name="department_id">
                                    <option value="">--Select Department--</option>
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-primary" name="type" value="pdf">Download Pdf</button>
                        <button class="btn btn-success" name="type" value="excel">Download Excel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>

    $(".ward_id").on("change", function(e) {
        var ward_id = $(this).val();

        if(ward_id != ''){
            var url = "{{ route('fetch-department', ':ward_id') }}";

            $.ajax({
                url: url.replace(":ward_id", ward_id),
                type: "GET",
                data: {
                    _method: "GET",
                    _token: "{{ csrf_token() }}",
                },
                success: function(data, textStatus, jqXHR) {
                    if (!data.error && !data.error2) {
                        $(".department_id").html(data.deptHtml);
                        $('.department_div').removeClass('d-none');
                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }else{
            $('.department_div').addClass('d-none');
        }
    });
</script>
