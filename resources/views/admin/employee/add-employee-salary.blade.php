<x-admin.layout>
    <x-slot name="title">Salary Structure</x-slot>
    <x-slot name="heading">Salary Structure</x-slot>

    @push('styles')
    <style>
        .size-checkbox{
            width: 1.7rem;
            height: 1.7rem;
        }
    </style>
    @endpush

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Add Employee</h4>
                </div>
                <!-- end card header -->
                <div class="card-body">
                    <form class="form-steps" autocomplete="off" id="addForm">
                        @csrf
                        <div class="step-arrow-nav mb-4">

                            <ul class="nav nav-pills custom-nav nav-justified" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="gen-info-tab" data-bs-toggle="pill" data-bs-target="#gen-info" type="button" role="tab">Basic Salary</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="allowance-tab" data-bs-toggle="pill" data-bs-target="#allowance" type="button" role="tab">Allowances</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="deduction-tab" data-bs-toggle="pill" data-bs-target="#deduction" type="button" role="tab">Deductions</button>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="gen-info" role="tabpanel">
                                <div>
                                    <!-- Employee ID field -->

                                    <input type="hidden" name="employee_id" id="employee_id">

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                                <select name="Emp_Code" id="Emp_Code" class="form-control js-example-basic-single" >
                                                    <option value="">Select Employee </option>
                                                    @foreach ($employees as $employee)
                                                        <option value="{{ $employee->employee_id }}">{{ $employee->employee_id }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid Emp_Code_err" data-error-for="Emp_Code"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- First Name, Middle Name, Last Name fields -->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                            <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                            <span class="text-danger invalid emp_name_err"></span>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                            <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                            <span class="text-danger invalid ward_err"></span>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                            <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                            <span class="text-danger invalid department_err"></span>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                            <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                            <span class="text-danger invalid designation_err"></span>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                            <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                            <span class="text-danger invalid class_err"></span>
                                        </div>

                                    </div>

                                    <hr>

                                    <div class="row">
                                        <h2>Add Salary Structure</h2>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pay_scale_id">Pay Scale<span class="text-danger">*</span></label>
                                                <select class="form-select js-example-basic-single" id="pay_scale_id" name="pay_scale_id">
                                                    <option value="">Select Pay Scale</option>
                                                    @foreach ($pay_scales as $pay_scale)
                                                        <option value="{{ $pay_scale->id }}" {{ old('pay_scale_id') == $pay_scale->id ? 'selected' : '' }}>{{ $pay_scale->pay_band." /". $pay_scale->grade_pay_name." /".$pay_scale->grade_amp." /".$pay_scale->grade_pay_name." /".$pay_scale->amount  }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid pay_scale_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="basic_salary">Basic Salary<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="basic_salary" name="basic_salary" value="{{ old('basic_salary') }}" readonly="readonly">
                                                <span class="text-danger invalid basic_salary_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="grade_pay">Grade Pay<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="grade_pay" name="grade_pay" value="{{ old('grade_pay') }}" readonly="readonly">
                                                <span class="text-danger invalid grade_pay_err"></span>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" id="personal_btn" class="btn btn-primary btn-label right ms-auto nexttab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Go to Allowance</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="allowance" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Allowance Details <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>Allowance</td>
                                                    <td>Amount</td>
                                                    <td>Type</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($allowances as $key => $allowance)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="size-checkbox" name="allowance_is_active[]" value="{{ $allowance->id }}">
                                                            <span class="text-danger invalid allowance_is_active_err"></span>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="allowance_id[]" value="{{ $allowance->id }}">
                                                            {{ $allowance->allowance }}
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="allowance_amt[]" value="{{ $allowance->amount }}">
                                                            <span class="text-danger invalid allowance_amt_err"></span>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="allowance_type[]" value="{{ $allowance->type }}">
                                                            {{ ($allowance->type == 1)?'Amount':'Percentage' }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="gen-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Basic Salary</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="deduction-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Deduction</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="deduction" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Deduction Details <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>Deduction</td>
                                                    <td>Amount</td>
                                                    <td>Type</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($deductions as $deduction)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="size-checkbox" name="deduction_is_active[]" value="{{ $deduction->id }}">
                                                            <span class="text-danger invalid deduction_is_active_err"></span>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="deduction_id[]" value="{{ $deduction->id }}">
                                                            {{ $deduction->deduction }}
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="deduction_amt[]" value="{{ $deduction->amount }}">
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="deduction_type[]" value="{{ $deduction->type }}">
                                                            {{ ($deduction->type == 1)?'Amount':'Percentage' }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="allowance-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Allowance</button>
                                    <button type="submit" class="btn btn-primary btn-label right ms-auto" id="addSubmit"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Submit</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                        </div>
                        <!-- end tab content -->
                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>

</x-admin.layout>

<script src="{{ asset('admin/js/salary-structure-validation.js') }}" defer></script>

{{-- Add --}}
<script>

    $("#Emp_Code").change(function(){
        var Emp_Code = $('#Emp_Code').val();
        if(Emp_Code != '')
        {
            var url = "{{ route('fetch-employee-details', ':Emp_Code') }}";

            $.ajax({
                url: url.replace(':Emp_Code', Emp_Code),
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data) {
                    if (!data.error && !data.error2) {
                        if (data.result === 1) {

                            var full_name = '';
                            if (data.employee_details.fname) {
                                full_name += data.employee_details.fname;
                            }
                            if (data.employee_details.mname) {
                                full_name += (full_name.length ? " " : "") + data.employee_details.mname;
                            }
                            if (data.employee_details.lname) {
                                full_name += (full_name.length ? " " : "") + data.employee_details.lname;
                            }
                            $('#emp_name').val(full_name);
                            $('#employee_id').val(data.employee_details.id);

                            $('#ward').val(data.employee_details.ward.name);
                            $('#department').val(data.employee_details.department.name);
                            $('#class').val(data.employee_details.class.name);
                            $('#designation').val(data.employee_details.designation.name);


                            $('#status_div').removeAttr('style');

                        } else if (data.result === 0) {
                            $('#status_div').hide();
                            $('#status_id').val('');
                            $('#remark').val('');
                            $('#emp_name').val('');
                            $('#ward').val('');
                            $('#department').val('');
                            $('#designation').val('');
                            $('#class').val('');

                            alert("Employee details not found!");
                        } else {
                            alert("Unexpected result from the server");
                        }
                }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        }
        else{
            alert('Please Enter Employee Id');
        }

    });


    $("#pay_scale_id").change(function(){
        var pay_scale_id = $('#pay_scale_id').val();

        if(pay_scale_id != '') {
            var url = "{{ route('fetch-payscale-details', ':pay_scale_id') }}";

            $.ajax({
                url: url.replace(':pay_scale_id', pay_scale_id),
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data) {
                    if (!data.error && !data.error2) {
                        if (data.result === 1) {
                            $('#basic_salary').val(data.pay_scale_details.amount);
                            $('#grade_pay').val(data.pay_scale_details.grade_amp);

                            $('#basic_salary').removeAttr('readonly');
                        } else if (data.result === 0) {
                            $('#basic_salary').val('');
                            $('#grade_pay').val('');

                            $('#basic_salary').attr('readonly', true);
                            alert("Pay Scale details not found!");
                        } else {
                            alert("Unexpected result from the server");
                        }
                    }
                    else {
                        alert("Error in data or unexpected response");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        } else {
            alert('Please select Pay Scale');
            $('#basic_salary').val('');
            $('#grade_pay').val('');

            $('#basic_salary').attr('readonly', true);
        }
    });

    $("#addForm").submit(function(e) {
        $("form").removeClass("was-validated");
        $("input").removeClass("is-valid");
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('employee-salary.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('employee-salary.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);

                    // Find the first input field with an error
                    var firstErrorField = Object.keys(responseObject.responseJSON.errors)[0];
                    var tabId = findTabIdForField(firstErrorField);

                    // Hide all tabs and remove active classes
                    $(".tab-content .tab-pane, .nav-pills .nav-link").removeClass("show active");

                    // Show the tab associated with the first error
                    $("#" + tabId + ", #" + tabId + "-tab").addClass("show active");

                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });

</script>
