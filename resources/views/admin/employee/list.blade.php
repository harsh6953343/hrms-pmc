<x-admin.layout>
    <x-slot name="title">Employees</x-slot>
    <x-slot name="heading">Employees</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('employee.create')
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <a href="{{ route('employee.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Class</th>
                                    <th>PF No.</th>
                                    <th>Joining Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee?->employee_id }}</td>
                                        <td>{{ $employee?->fname." ".$employee->mname." ".$employee->lname }}</td>
                                        <td>{{ $employee?->ward?->name }}</td>
                                        <td>{{ $employee?->department?->name }}</td>
                                        <td>{{ $employee?->designation?->name }}</td>
                                        <td>{{ $employee?->class?->name }}</td>
                                        <td>{{ $employee?->pf_account_no }}</td>
                                        <td>{{ \Carbon\Carbon::parse($employee->doj)->format('d-m-Y') }}</td>
                                        <td><a href="{{ route('employee-status.index') }}">{{ $employee?->status?->name }}</a></td>

                                        <td>
                                            @can('employee.edit')
                                                <a href="{{ route('employee.edit',$employee->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="Edit Employee"><i data-feather="edit"></i></a>
                                            @endcan
                                            @can('employee.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Employee" data-id="{{ $employee->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan

                                            <a href="{{ route('employee.show',$employee->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="Show Employee">
                                                <i data-feather="eye"></i>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Employee?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('employee.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

</script>
