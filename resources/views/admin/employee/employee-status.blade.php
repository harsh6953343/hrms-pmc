<x-admin.layout>
    <x-slot name="title">Employee Status</x-slot>
    <x-slot name="heading">Employee Status</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('employee_status.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <a href="{{ route('employee-status.create') }}"><button class="btn btn-primary">Add <i class="fa fa-plus"></i></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Status</th>
                                    <th>Remark</th>
                                    <th>Applicable Date</th>
                                    <th>Is Salary Applicable</th>
                                    <th>Salary Percent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($emp_statuses as $emp_status)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $emp_status->Emp_Code }}</td>
                                        <td>{{ $emp_status?->status?->name }}</td>
                                        <td>{{ $emp_status->remark }}</td>
                                        <td>{{ $emp_status->applicable_date }}</td>
                                        <td>{{ ($emp_status->is_salary_applicable == 1)?'Yes':'No' }}</td>
                                        <td>{{ $emp_status->salary_percent }} </td>
                                        <td>
                                            @can('employee_status.edit')
                                            <a href="{{ route('employee-status.edit',$emp_status->id) }}"><button class="edit-element btn btn-secondary px-2 py-1" title="Edit Employee Status" data-id="{{ $emp_status->id }}"><i data-feather="edit"></i></button></a>
                                            @endcan
                                            @can('employee_status.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Employee Status" data-id="{{ $emp_status->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Employee status?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('employee-status.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
