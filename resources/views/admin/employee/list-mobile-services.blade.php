<x-admin.mob-layout>
    <x-slot name="title">Services</x-slot>
    <x-slot name="heading">Services</x-slot>

    <div class="row">
        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-danger align-items-center justify-content-center">
                <div class="card-body" data-card='sal-slip'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3">
                                <i class="bx bx-dollar-circle text-success"></i>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex mt-4">
                        <div class="flex-grow-1">
                            <p class="fw-medium fs-4 mb-0">Salary Slip</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                    
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-info align-items-center justify-content-center">
                <div class="card-body" data-card='other'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3">
                                <i class="ri-pulse-line display-6 text-muted"></i>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-4" style="margin-left: 26%">
                        <div class="flex-grow-1">
                            <p class="text-uppercase fw-medium fs-4 mb-0">#</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-success align-items-center justify-content-center">
                <div class="card-body" data-card='other'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3">
                                <i class="ri-space-ship-line display-6 text-muted"></i>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-4" style="margin-left: 26%">
                        <div class="flex-grow-1">
                            <p class="text-uppercase fw-medium fs-4 mb-0">#</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-warning align-items-center justify-content-center">
                <div class="card-body" data-card='other'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3">
                                <i class="ri-trophy-line display-6 text-muted"></i>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-4" style="margin-left: 26%">
                        <div class="flex-grow-1">
                            <p class="text-uppercase fw-medium fs-4 mb-0">#</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-primary align-items-center justify-content-center">
                <div class="card-body" data-card='other'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3" style="color: #000">
                                <i class="ri-book-line"></i>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mt-4" style="margin-left: 26%">
                        <div class="flex-grow-1">
                            <p class="text-uppercase fw-medium fs-4 mb-0">#</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

        <div class="col-xl-3 col-md-6 col-sm-6 col-6">
            <!-- card -->
            <div class="card card-animate bg-secondary align-items-center justify-content-center">
                <div class="card-body" data-card='other'>
                    <div class="d-flex mt-4 align-items-center justify-content-center">
                        <div>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-success-subtle rounded fs-3" style="color: #000">
                                <i class="ri-numbers-line"></i>
                            </span>
                        </div>
                        </div>
                    <div class="d-flex align-items-center mt-4" style="margin-left: 26%">
                        <div class="flex-grow-1">
                            <p class="text-uppercase fw-medium fs-4 mb-0">#</p>
                        </div>
                        <div class="flex-shrink-0">

                        </div>
                    </div>
                </div><!-- end card body -->
            </div><!-- end card -->
        </div><!-- end col -->

    </div>
</x-admin.mob-layout>


<script>

    $(".card-body").click(function(){
        if($(this).attr('data-card') == 'sal-slip'){
            window.location.href = '{{ route('mob.salary-slip') }}';
        }
    });

</script>