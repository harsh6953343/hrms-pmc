<x-admin.layout>
    <x-slot name="title">Bonus</x-slot>
    <x-slot name="heading">Bonus</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Bonus</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-3">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <select name="class" id="class" class="form-select">
                                    <option value="">Select Class</option>
                                    @foreach ($class as $clas)
                                        <option value="{{ $clas->id }}">{{$clas->name  }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="bonus">Bonus Amount<span class="text-danger">*</span></label>
                                <input class="form-control title" id="bonus" name="bonus" type="text" placeholder="Enter Bonus Amount">
                                <span class="text-danger invalid bonus_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="financial_years">Financial Year<span class="text-danger">*</span></label>
                                <select class="form-select" id="financial_years" name="financial_years">
                                        <option value="">Select Financial Year</option>
                                    @foreach ($financial_years as $financial_year)
                                        <option value="{{ $financial_year->id }}">{{ $financial_year->title }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Applicable Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>



                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    {{-- <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Allowance</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Allowance (In English)<span class="text-danger">*</span></label>
                                <input class="form-control title" id="allowance" name="allowance" type="text" placeholder="Enter Allowance In English">
                                <span class="text-danger invalid allowance_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="allowance_in_marathi">Allowance (In marathi)<span class="text-danger">*</span></label>
                                <input class="form-control title" id="allowance_in_marathi" name="allowance_in_marathi" type="text" placeholder="Enter Allowance (In Marathi)">
                                <span class="text-danger invalid allowance_in_marathi_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="type">Select Type<span class="text-danger">*</span></label>
                                <select class="form-select type_class" id="edit_type" name="type">
                                    <option value="">Select Type</option>
                                </select>
                                <span class="text-danger invalid type_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="amount">Amount<span class="text-danger">*</span></label>
                                <input class="form-control title" id="edit_amount" name="amount" type="text" placeholder="Enter Amount">
                                <span class="text-danger invalid amount_err"></span>
                            </div>

                            <div class="col-md-4 mt-5">
                                <label class="col-form-check-label" for="formCheck8">
                                    Is Applicable On Suspension
                                </label>
                                <input class="form-check-input size-checkbox" type="checkbox" id="is_applicable" name="is_applicable" checked="" value="1">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="calculation">Allowance Calculation <b>(If dynamic then calculation will change as per employee attendance)</b><span class="text-danger">*</span></label>
                                <select class="form-select" id="edit_calculation" name="calculation">
                                    <option value="">Select Calculation</option>
                                </select>
                                <span class="text-danger invalid calculation_err"></span>
                            </div>


                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div> --}}


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('allowance.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Class </th>
                                    <th>Bonus</th>
                                    <th>Applicable Month</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($daDifferances as $daDifferance)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $daDifferance->DA_currentRate }}</td>
                                        <td>{{ $daDifferance->DA_newRate }}</td>
                                        <td>{{ $daDifferance->applicable_month }}</td>
                                        <td>{{ $daDifferance->given_month }}</td>
                                        <td>{{ $daDifferance->no_of_month }}</td>
                                        <td> --}}
                                            {{-- @can('allowance.edit') --}}
                                                {{-- <button class="edit-element btn btn-secondary px-2 py-1" title="Edit DA Differance" data-id="{{ $daDifferance->id }}"><i data-feather="edit"></i></button> --}}
                                            {{-- @endcan
                                            @can('allowance.delete') --}}
                                                {{-- <button class="btn btn-danger rem-element px-2 py-1" title="Delete DA Differance" data-id="{{ $daDifferance->id }}"><i data-feather="trash-2"></i> </button> --}}
                                            {{-- @endcan --}}
                                        {{-- </td>
                                    </tr>
                                @endforeach --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('allowance.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('allowance.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('allowance.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {
                    $("#editForm input[name='edit_model_id']").val(data.allowance.id);
                    $("#editForm input[name='allowance']").val(data.allowance.allowance);
                    $("#editForm input[name='amount']").val(data.allowance.amount);
                    $("#editForm input[name='allowance_in_marathi']").val(data.allowance.allowance_in_marathi);
                    $("#edit_type").html(data.typeHtml);
                    $("#edit_calculation").html(data.calculationHtml);

                    if(data.allowance.is_applicable == '1'){
                        $('#is_applicable').prop('checked', true);
                        $('.size-checkbox').val('1');
                    }else{
                        $('#is_applicable').prop('checked', false);
                        $('.size-checkbox').val('0');
                    }

                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('allowance.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('allowance.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Allowance?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('allowance.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
