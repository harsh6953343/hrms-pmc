@php
$monthMrName = ['01' => 'जानेवारी', '02' => 'फेब्रुवारी', '03' => 'मार्च', '04' => 'एप्रिल', '05' => 'मे', '06' => 'जून', '07' => 'जुलै', '08' => 'ऑगस्ट', '09' => 'सप्टेंबर', '10' => 'ऑक्टोबर', '11' => 'नोहेंबर', '12' => 'डिसेंबर'];
@endphp
    <table class="table">
        <thead>
            <tr>
                <th colspan="17" style="font-size: 30px;padding:10px 3px;text-align:center">महानगरपालिका</th>
            </tr>
            <tr>
                <th colspan="17" style="font-size: 20px;padding:10px 3px;text-align:center;">Dearness Allowance Difference Payable From {{ $monthMrName[\Carbon\Carbon::parse($daBillDate->from_date)->format('m')] ?? '' }}-{{ convertToMarathiNumber(\Carbon\Carbon::parse($daBillDate->from_date)->format('Y')) }} To {{ $monthMrName[\Carbon\Carbon::parse($daBillDate->to_date)->format('m')] ?? '' }}-{{ convertToMarathiNumber(\Carbon\Carbon::parse($daBillDate->to_date)->format('Y')) }}</th>
            </tr>
        </thead>
    </table>

    <table class="table">
        <thead>
            <tr>
                <th rowspan="2">अनु.</th>
                <th rowspan="2">कर्मचारी क्र.</th>
                <th rowspan="2">कर्मचारीचे नाव</th>
                <th rowspan="2">वेतन माहे</th>
                <th rowspan="2">अर्जित वेतन</th>
                <th rowspan="2">ग्रेड वेतन</th>
                <th colspan="3">महागाई भत्ता</th>
                <th colspan="3">घरभाडे भत्ता</th>
                <th>अंनिवेयो-मपा</th>
                <th rowspan="2">एकूण वेतन व भत्ते</th>
                <th>अंनिवेयो-क.</th>
                <th>अंनिवेयो-मपा कपात</th>
                <th rowspan="2">निव्वळ-देय</th>
            </tr>
            <tr>
                <th>देय</th>
                <th>दिलेले</th>
                <th>फरक</th>
                <th>देय</th>
                <th>दिलेले</th>
                <th>फरक</th>
                <th>फरक</th>
                <th>फरक</th>
                <th>फरक</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="17" style="padding: 10px;"></th>
            </tr>
            @php
                $count = 1;
                $totalDaPayable = $totalDaGiven = $totalDaDifference = $totalHraPayable = $totalHraGiven = $totalHraDifference = $totalCorporationAllowance = $totalTotalAllowance = $totalEmployeeDeduction = $totalCorporationDeduction = $totalNetSalary = 0;
            @endphp
            @foreach($daBillCalculates as $daBillCalculate)
                @php
                    $daPayable = $daGiven = $daDifference = $hraPayable = $hraGiven = $hraDifference = $corporationAllowance = $totalAllowance = $employeeDeduction = $corporationDeduction = $netSalary = 0;
                @endphp
                @foreach($daBillCalculate as $key => $daBill)
                <tr>
                    <td style="padding: 5px 3px;">{{ ($key == 0) ? convertToMarathiNumber($count) : '' }}</td>
                    <td>{{ ($key == 0) ? convertToMarathiNumber($daBill->emp_code) : '' }}</td>
                    <th>{{ ($key == 0) ? $daBill->employee?->fname.' '.$daBill->employee?->mname.' '.$daBill->employee?->lname : '' }}</th>
                    <th>{{ $monthMrName[\Carbon\Carbon::parse($daBill->from_date)->format('m')] ?? '' }}, {{ convertToMarathiNumber(\Carbon\Carbon::parse($daBill->from_date)->format('Y')) }}</th>
                    <td>{{ convertToMarathiNumber($daBill->basic) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->grade_pays) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->da_payable) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->da_given) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->da_difference) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->hra_payable) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->hra_given) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->hra_difference) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->dcps_corporation_allowance) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->da_difference + $daBill->hra_difference + $daBill->dcps_corporation_allowance) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->dcps_employee_deduction) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->dcps_corporation_deduction) }}</td>
                    <td>{{ convertToMarathiNumber($daBill->da_difference + $daBill->hra_difference + $daBill->dcps_corporation_allowance - $daBill->dcps_employee_deduction - $daBill->dcps_corporation_deduction) }}</td>
                </tr>
                @php
                    $daPayable += $daBill->da_payable;
                    $daGiven += $daBill->da_given;
                    $daDifference += $daBill->da_difference;
                    $hraPayable += $daBill->hra_payable;
                    $hraGiven += $daBill->hra_given;
                    $hraDifference += $daBill->hra_difference;
                    $corporationAllowance += $daBill->dcps_corporation_allowance;
                    $totalAllowance += ($daBill->da_difference + $daBill->dcps_corporation_allowance);
                    $employeeDeduction += $daBill->dcps_employee_deduction;
                    $corporationDeduction += $daBill->dcps_corporation_deduction;
                    $netSalary += ($daBill->da_difference + $daBill->dcps_corporation_allowance - $daBill->dcps_employee_deduction - $daBill->dcps_corporation_deduction);
                @endphp
                @endforeach
                <tr>
                    <td colspan="3" style="padding: 5px 3px;"></td>
                    <th>एकुण</th>
                    <td></td>
                    <td></td>
                    <th>{{ convertToMarathiNumber($daPayable) }}</th>
                    <th>{{ convertToMarathiNumber($daGiven) }}</th>
                    <th>{{ convertToMarathiNumber($daDifference) }}</th>
                    <th>{{ convertToMarathiNumber($hraPayable) }}</th>
                    <th>{{ convertToMarathiNumber($hraGiven) }}</th>
                    <th>{{ convertToMarathiNumber($hraDifference) }}</th>
                    <th>{{ convertToMarathiNumber($corporationAllowance) }}</th>
                    <th>{{ convertToMarathiNumber($totalAllowance) }}</th>
                    <th>{{ convertToMarathiNumber($employeeDeduction) }}</th>
                    <th>{{ convertToMarathiNumber($corporationDeduction) }}</th>
                    <th>{{ convertToMarathiNumber($netSalary) }}</th>
                </tr>
                <tr>
                    <th colspan="17" style="padding: 10px;"></th>
                </tr>
                @php
                    $count++;
                    $totalDaPayable += $daPayable;
                    $totalDaGiven += $daGiven;
                    $totalDaDifference += $daDifference;
                    $totalHraPayable += $hraPayable;
                    $totalHraGiven += $hraGiven;
                    $totalHraDifference += $hraDifference;
                    $totalCorporationAllowance += $corporationAllowance;
                    $totalTotalAllowance += $totalAllowance;
                    $totalEmployeeDeduction += $employeeDeduction;
                    $totalCorporationDeduction += $corporationDeduction;
                    $totalNetSalary += $netSalary;
                @endphp
            @endforeach
        </tbody>
    </table>
    <div class="page-break"></div>
    <table class="table">
        <thead>
            <tr>
                <th rowspan="2">अनु.</th>
                <th rowspan="2">कर्मचारी क्र.</th>
                <th rowspan="2">कर्मचारीचे नाव</th>
                <th rowspan="2">वेतन माहे</th>
                <th rowspan="2">अर्जित वेतन</th>
                <th rowspan="2">ग्रेड वेतन</th>
                <th colspan="3">महागाई भत्ता</th>
                <th colspan="3">घरभाडे भत्ता</th>
                <th>अंनिवेयो-मपा</th>
                <th rowspan="2">एकूण वेतन व भत्ते</th>
                <th>अंनिवेयो-क.</th>
                <th>अंनिवेयो-मपा कपात</th>
                <th rowspan="2">निव्वळ-देय</th>
            </tr>
            <tr>
                <th>देय</th>
                <th>दिलेले</th>
                <th>फरक</th>
                <th>देय</th>
                <th>दिलेले</th>
                <th>फरक</th>
                <th>फरक</th>
                <th>फरक</th>
                <th>फरक</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3"></td>
                <th>एकूण रक्कम</th>
                <td></td>
                <td></td>
                <th>{{ convertToMarathiNumber($totalDaPayable) }}</th>
                <th>{{ convertToMarathiNumber($totalDaGiven) }}</th>
                <th>{{ convertToMarathiNumber($totalDaDifference) }}</th>
                <th>{{ convertToMarathiNumber($totalHraPayable) }}</th>
                <th>{{ convertToMarathiNumber($totalHraGiven) }}</th>
                <th>{{ convertToMarathiNumber($totalHraDifference) }}</th>
                <th>{{ convertToMarathiNumber($totalCorporationAllowance) }}</th>
                <th>{{ convertToMarathiNumber($totalTotalAllowance) }}</th>
                <th>{{ convertToMarathiNumber($totalEmployeeDeduction) }}</th>
                <th>{{ convertToMarathiNumber($totalCorporationDeduction) }}</th>
                <th>{{ convertToMarathiNumber($totalNetSalary) }}</th>
            </tr>
        </tbody>
    </table>

