<x-admin.layout>
    <x-slot name="title">Delete Employee Absent Attendance</x-slot>
    <x-slot name="heading">Delete Employee Absent Attendance</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Delete Employee Absent Attendance</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" action="{{ route('import.attandance.excel.store') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label" for="month">Select Financial Year <span class="text-danger">*</span></label>
                                <select name="month" id="month" class="form-select" required>
                                    <option value="">Select month</option>
                                    @for($i=1; $i <= 12; $i++)
                                        <option value="{{ $i }}">{{ \Carbon\Carbon::create()->month($i)->format('F') }}</option>
                                    @endfor
                                </select>
                                @error('month')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label" for="financial_year_id">Select Financial Year <span class="text-danger">*</span></label>
                                <select name="financial_year_id" id="financial_year_id" class="form-select" required>
                                    <option value="">Select Financial Year</option>
                                    @foreach($financialYears as $financialYear)
                                        <option value="{{ $financialYear->id }}">{{ $financialYear->title }}</option>
                                    @endforeach
                                </select>
                                @error('financial_year_id')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <label class="col-form-label" for="import">Select File <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" name="import" required />
                                @error('import')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to import this excel.')" id="addSubmit">Import</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

</x-admin.layout>

<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>
