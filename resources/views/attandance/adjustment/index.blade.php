<x-admin.layout>
    <x-slot name="title">Attandance Adjustment</x-slot>
    <x-slot name="heading">Attandance Adjustment</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('import.attandance-adjustment.save') }}" class="theme-form" name="addForm" method="post" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month" required>
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ (request()->month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ (request()->month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ (request()->month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ (request()->month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ (request()->month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ (request()->month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ (request()->month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ (request()->month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ (request()->month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ (request()->month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ (request()->month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ (request()->month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ request()->from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ request()->to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="no_of_days">No of Days<span class="text-danger">*</span></label>
                                <input class="form-control title" id="no_of_days" name="no_of_days" type="number" placeholder="No of Days" required value="{{ request()->no_of_days }}">
                                <span class="text-danger invalid no_of_days_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="import">File<span class="text-danger">*</span></label>
                                <input class="form-control title" id="import" name="import" type="file" placeholder="No of Days" required>
                                <span class="text-danger invalid import_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>


<script>
    @if(Session::has('success'))
        swal({
            title: "Successful!",
            text: "{{ Session::get('success') }}",
            icon: "success",
            button: "OK",
        });
    @endif

    @if(Session::has('error'))
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: "error",
            button: "OK",
        });
    @endif
</script>

